# X11CP (X11 Conservancy Project)

The X11 Conservancy Project (`X11CP`) pulls together the disparate set of
programs which were being written between the very late 80s, and early 90s --
usually for Unix and Linux.

Before the establishment of code hosting services, and search engines,
applications which were being developed were largely uncoordinated.  With only
FTP servers for public file sharing, knowing which applications existed and
for which purposes became harder to discover.  Because of this, some
applications ended up being duplicated in what they did, but also, the scope
and purpose of that application ended up being niche and specific.

To help try and address this disparity, the 
[Linux Software Map](https://xteddy.org/lsm) was created to pull together a
central resource of known software for people to query and therefore use
themselves.

As the Internet expanded and Linux distributions became established, certain
FTP sites were largely used to host some of the more established programs, as
well as those found in the LSM.  Some of those providers were:

* [Sunsite](https://www.ibiblio.org/sunsite/sunsiteworld.html)
* [funet](https://www.nic.funet.fi/)
* [ibiblio](https://www.ibiblio.org/)

Now though, with the rise of search engines and software hubs, it's never been
easier to find software and know where to look.

But the early dawn of free software, especially around applications written
for X11, using Motif and XT and other widget libraries has now mostly been
consigned to obscurity.

With X11 itself now under threat of no longer being developed in favour of
Wayland, these applications are going to be harder to run and be discovered.

Hence, the `X11CP` is designed to be a central place for hosting the sources of
these applications, and to showcase their unique history and properties.  In
keeping this software active, it will help keep an important historical point
alive.

# Contributing

TODO, but:

* Search the [LSM](https://xteddy.org/lsm) for potential candidates
* Search the FTP sites above.
* `xmkmf -a`
* `Makefile` preservation
* Source-code modification.
* Screenshots

# Contact

* You can find me on `libera.chat` in `#x11cp` (`thomas_adam`)
* I'm on mastodon: `@thomasadam@bsd.network`
* Email: thomas@x11cp.org
