/* color management */


#define MASK_CTR   0
#define MASK_BLACK 1

/* plane usage:

     pixel(i)                      -> normal color i
     pixel(i) | plane_mask(0)      -> color i, but CTR-pink shaded
     pixel(i) | plane_mask(1)      -> black
*/


/* get white and black pixvalues */

unsigned long blackpix();
unsigned long whitepix();


/* get pixel values of all colors and plane masks */

unsigned long pixel (int i);
unsigned long plane_mask (int i);


/* check if extended color system is supported */

int extendedcolor();



/* initialize system */

void initcolor (GC *gc);



/* free colors */

void freecolor (GC *gc);
















