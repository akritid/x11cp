typedef struct {
  int x;
  int y;
} POINT;

typedef struct {
  POINT p1;
  POINT p2;
} LINE;



int ccw (POINT p0, POINT p1, POINT p2);
int intersect (LINE l1, LINE l2);
int ppcontains (POINT t, POINT *p, int numpoints);
