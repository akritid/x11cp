/* graphics primitives */

#include <X11/Xlib.h>


#define SCREEN     0
#define POSTSCRIPT 1

#define SOLID LineSolid
#define DASH  LineOnOffDash
#define DOT   -5

#define BLACK       0
#define WHITE       1
#define GRAY        2
#define LIGHTGRAY   3
#define NAVYBLUE    4
#define SKYBLUE     5
#define RED         6
#define PINK        7
#define LIGHTGREEN  8
#define YELLOW      9

void gp_drawline (int x1, int y1, int x2, int y2);
void gp_drawcircle (int x, int y, int radius);
void gp_setlinestyle (int width, int style);
void gp_smalltext (int x, int y, char *string);
void gp_bigtext (int x, int y, char *string);
void gp_drawrect (int x1, int y1, int x2, int y2);
void gp_drawfilledrect (int x1, int y1, int x2, int y2);
void gp_drawarea (int *x, int *y, int numpoints);
void gp_setplane (unsigned long plane);
void gp_drawplanearea (int *x, int *y, int numpoints);

