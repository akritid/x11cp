/* prototypes */

LOCATION window2internal (int x, int y);
void internal2window (LOCATION point, int *x, int *y);
void findvisibleobjects();
double dist (int x1, int y1, int x2, int y2);
char *nearobject (int x, int y);

void draw_airport (int x, int y, int heading, char *name, int numpoints,
		   LOCATION *points, RUNWAYDEF *runways);
void draw_airfield (int x, int y, int runwayheading, char *name);
void draw_specialairfield (int x, int y, int runwayheading, char *name);
void draw_glider_site (int x, int y, char *name);
void draw_airport_mil (int x, int y, int runwayheading, char *name);
void draw_airport_civ_mil (int x, int y, int runwayheading, char *name);
void draw_ndb (int x, int y, char *name, double freq);
void draw_vor (int x, int y, char *name, double freq, int northoffset);
void draw_vor_dme (int x, int y, char *name, double freq, int northoffset);
void draw_vortac (int x, int y, char *name, double freq, int northoffset);
void draw_river (int x, int y, char *name, int numpoints, LOCATION *points);
void draw_lake (char *name, int numpoints, LOCATION *points);
void draw_highway (int numpoints, LOCATION *points);
void draw_ctr (int x, int y, char *name, int numpoints, LOCATION *points);
void draw_cvfr (int x, int y, char *name, int numpoints, LOCATION *points);
void draw_village (int x, int y, char *label);
void draw_town (char *name, int numpoints, LOCATION *points);
void draw_waypoint (int x, int y, char *name, char *alias);
void objectdescription(char **lines, int *numlines, int x, int y, int *range);




