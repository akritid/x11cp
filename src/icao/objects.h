/* object data base structure */

#include "coord.h"

#define UNKNOWN -9999

/* let's see what object types we have to deal with */

/* aerodromes */

#define O_INTLAIRPORT                1
#define O_AIRPORT                    2
#define O_AIRPORT_CIV_MIL            3
#define O_AIRPORT_MIL                4
#define O_AIRFIELD                   5
#define O_SPECIAL_AIRFIELD           6
#define O_HELIPORT                   7
#define O_HELIPORT_AMB               8
#define O_GLIDER_SITE                9
#define O_HANG_GLIDER_SITE          10
#define O_PARACHUTE_JUMPING_SITE    11
#define O_FREE_BALLON_SITE          12


/* radio navigation facilities */

#define O_VOR                      101
#define O_VOR_DME                  102
#define O_VORTAC                   103
#define O_TACAN                    104
#define O_NDB                      105
#define O_MARKER_BEACON            106
#define O_BASIC_RADIO_FACILITY     107


/* misc */

#define O_OBSTRUCTION              201
#define O_GROUP_OBSTRUCTION        202
#define O_FIRED_OBSTRUCTION        203
#define O_FIRED_GROUP_OBSTRUCTION  204
#define O_AERO_GROUND_LIGHT        205



/* airspace structure */

#define O_REPORTING_POINT          301
#define O_CTR                      351
#define O_CVFR                     352


/* gliding objects */

#define O_WAYPOINT                 401


/* natural objects */

#define O_RIVER                   1001
#define O_HIGHWAY                 1002
#define O_ROAD                    1003
#define O_LAKE                    1004
#define O_VILLAGE                 1010
#define O_TOWN                    1011

#define O_ISOGONE                 2001


typedef int COLOR;
typedef double FREQ;


typedef struct {    /* runway description */
  int direction;
  int length;
  char surface;
  void *next;       /* pointer to next runway of list */
} RUNWAYDEF;




/* general object structure */

typedef struct {
  int type;              /* one of the above constants */
  char *name;            /* e.g. Grefrath-Niershorst */
  char *alias;           /* e.g. EDLF */
  LOCATION location;     /* geographic location */
  FREQ frequency;
  double msl;            /* object elev in ft above msl */
  double top_msl;        /* elev of object's top in ft above msl */
  double range;          /* range of VORs etc. in NM */ 
  RUNWAYDEF *runways;    /* list of runways */
  LOCATION *points;      /* polygon --- points describing the object */
  int numpoints;         /* number of points in above list */
  LOCATION topleft,
           bottomright;  /* bounding box if polygons are given */
  int piece;             /* number of piece rectangle if object is visible */
} OBJECT;





/* function prototypes */

int objects_init();
void new_location (LOCATION loc);
void new_elev (double elev);
void new_object (int objecttype, char *identifier);
void set_frequency (double freq);
void set_alias (char *name);
void set_topmsl (double height);
void set_range (double range);
void add_runway (int direction, int length, char surface);
void map_setscale (double scale);
void map_setorigin (LOCATION origin);
void map_setparallels (long par1, long par2);
void map_display();
char *objecttypestring (int objecttype);
OBJECT *objectfromnamelist (int listindex);   








