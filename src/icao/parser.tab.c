
/*  A Bison parser, made from parser.y  */

#define YYBISON 1  /* Identify Bison output.  */

#define	tSTRING	258
#define	tFLOAT	259
#define	tCHAR	260
#define	tLOCATION	261
#define	tLATITUDE	262
#define	tLONGITUDE	263
#define	tAT	264
#define	tSCALE	265
#define	tORIGIN	266
#define	tPARALLELS	267
#define	tZEROMERIDIAN	268
#define	tSHOW_MAP	269
#define	tDIGITIZER	270
#define	tCALIBRATE	271
#define	tOUT	272
#define	tELEV	273
#define	tFREQUENCY	274
#define	tALIAS	275
#define	tRANGE	276
#define	tISOGONE	277
#define	tINTLAIRPORT	278
#define	tAIRPORT	279
#define	tAIRPORT_CIV_MIL	280
#define	tAIRPORT_MIL	281
#define	tAIRFIELD	282
#define	tSPECIAL_AIRFIELD	283
#define	tGLIDER_SITE	284
#define	tHANG_GLIDER_SITE	285
#define	tPARACHUTE_JUMPING_SITE	286
#define	tFREE_BALLON_SITE	287
#define	tHELIPORT	288
#define	tHELIPORT_AMB	289
#define	tRIVER	290
#define	tHIGHWAY	291
#define	tROAD	292
#define	tLAKE	293
#define	tCTR	294
#define	tCVFR	295
#define	tVOR	296
#define	tVOR_DME	297
#define	tVORTAC	298
#define	tTACAN	299
#define	tNDB	300
#define	tMARKER_BEACON	301
#define	tBASIC_RADIO_FACILITY	302
#define	tOBSTRUCTION	303
#define	tGROUP_OBSTRUCTION	304
#define	tFIRED_OBSTRUCTION	305
#define	tFIRED_GROUP_OBSTRUCTION	306
#define	tREPORTING_POINT	307
#define	tWAYPOINT	308
#define	tVILLAGE	309
#define	tTOWN	310
#define	tRUNWAY	311
#define	tGRAS	312
#define	tASPHALT	313
#define	tCONCRETE	314

#line 1 "parser.y"


#include "objects.h"


LOCATION makelocation (long lat, long longi)
{
  LOCATION temp;

  temp.latitude = lat;
  temp.longitude = longi;
  return temp;
}

void map_setzero (long zero);

int currentline = 1;


#line 22 "parser.y"
typedef union {
	char *c;
        char s;
	float d;
	LOCATION l;
} YYSTYPE;

#ifndef YYLTYPE
typedef
  struct yyltype
    {
      int timestamp;
      int first_line;
      int first_column;
      int last_line;
      int last_column;
      char *text;
   }
  yyltype;

#define YYLTYPE yyltype
#endif

#include <stdio.h>

#ifndef __STDC__
#define const
#endif



#define	YYFINAL		130
#define	YYFLAG		-32768
#define	YYNTBASE	62

#define YYTRANSLATE(x) ((unsigned)(x) <= 314 ? yytranslate[x] : 73)

static const char yytranslate[] = {     0,
     2,     2,     2,     2,     2,     2,     2,     2,     2,    60,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,    61,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     1,     2,     3,     4,     5,
     6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
    16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
    26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
    36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
    46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
    56,    57,    58,    59
};

#if YYDEBUG != 0
static const short yyprhs[] = {     0,
     0,     1,     4,     6,    10,    13,    16,    18,    21,    26,
    29,    33,    35,    37,    41,    44,    47,    51,    55,    58,
    61,    64,    67,    70,    73,    76,    79,    82,    85,    88,
    91,    94,    97,   100,   103,   106,   109,   112,   115,   118,
   122,   125,   129,   132,   136,   139,   143,   146,   149,   153,
   156,   159,   163,   167,   168,   171,   172,   175,   178,   181,
   184,   189,   192,   195,   197,   199
};

#endif

static const short yyrhs[] = {    -1,
    62,    63,     0,    60,     0,    70,    66,    68,     0,    66,
    68,     0,    70,    69,     0,    64,     0,    17,     3,     0,
    10,     4,    61,     4,     0,    11,    65,     0,    12,     7,
     7,     0,    14,     0,    15,     0,    16,    65,    65,     0,
    13,     8,     0,     7,     8,     0,    23,     3,    67,     0,
    24,     3,    67,     0,    25,     3,     0,    26,     3,     0,
    27,     3,     0,    28,     3,     0,    33,     3,     0,    34,
     3,     0,    29,     3,     0,    31,     3,     0,    32,     3,
     0,    41,     3,     0,    42,     3,     0,    43,     3,     0,
    44,     3,     0,    45,     3,     0,    46,     3,     0,    47,
     3,     0,    48,     3,     0,    49,     3,     0,    50,     3,
     0,    51,     3,     0,    52,     3,     0,    35,     3,    67,
     0,    35,    67,     0,    38,     3,    67,     0,    38,    67,
     0,    22,     4,    67,     0,    36,    67,     0,    36,     3,
    67,     0,    37,    67,     0,    54,     3,     0,    55,     3,
    67,     0,    55,    67,     0,    53,     3,     0,    39,     3,
    67,     0,    40,     3,    67,     0,     0,    67,    65,     0,
     0,    68,    71,     0,    18,     4,     0,     9,    65,     0,
    19,     4,     0,    56,     4,     4,    72,     0,    20,     3,
     0,    21,     4,     0,    58,     0,    59,     0,    57,     0
};

#if YYDEBUG != 0
static const short yyrline[] = { 0,
    68,    69,    72,    73,    74,    75,    76,    79,    81,    82,
    83,    84,    85,    86,    87,    90,    95,    97,    98,    99,
   100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
   110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
   120,   121,   122,   123,   125,   126,   127,   128,   129,   130,
   131,   132,   133,   136,   137,   141,   142,   147,   150,   152,
   154,   155,   156,   159,   161,   162
};

static const char * const yytname[] = {   "$","error","$illegal.","tSTRING",
"tFLOAT","tCHAR","tLOCATION","tLATITUDE","tLONGITUDE","tAT","tSCALE","tORIGIN",
"tPARALLELS","tZEROMERIDIAN","tSHOW_MAP","tDIGITIZER","tCALIBRATE","tOUT","tELEV",
"tFREQUENCY","tALIAS","tRANGE","tISOGONE","tINTLAIRPORT","tAIRPORT","tAIRPORT_CIV_MIL",
"tAIRPORT_MIL","tAIRFIELD","tSPECIAL_AIRFIELD","tGLIDER_SITE","tHANG_GLIDER_SITE",
"tPARACHUTE_JUMPING_SITE","tFREE_BALLON_SITE","tHELIPORT","tHELIPORT_AMB","tRIVER",
"tHIGHWAY","tROAD","tLAKE","tCTR","tCVFR","tVOR","tVOR_DME","tVORTAC","tTACAN",
"tNDB","tMARKER_BEACON","tBASIC_RADIO_FACILITY","tOBSTRUCTION","tGROUP_OBSTRUCTION",
"tFIRED_OBSTRUCTION","tFIRED_GROUP_OBSTRUCTION","tREPORTING_POINT","tWAYPOINT",
"tVILLAGE","tTOWN","tRUNWAY","tGRAS","tASPHALT","tCONCRETE","'\\n'","':'","input",
"line","command","location","object","pointlist","qualities","elev","loc","quality",
"runwaytype",""
};
#endif

static const short yyr1[] = {     0,
    62,    62,    63,    63,    63,    63,    63,    64,    64,    64,
    64,    64,    64,    64,    64,    65,    66,    66,    66,    66,
    66,    66,    66,    66,    66,    66,    66,    66,    66,    66,
    66,    66,    66,    66,    66,    66,    66,    66,    66,    66,
    66,    66,    66,    66,    66,    66,    66,    66,    66,    66,
    66,    66,    66,    67,    67,    68,    68,    69,    70,    71,
    71,    71,    71,    72,    72,    72
};

static const short yyr2[] = {     0,
     0,     2,     1,     3,     2,     2,     1,     2,     4,     2,
     3,     1,     1,     3,     2,     2,     3,     3,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     3,
     2,     3,     2,     3,     2,     3,     2,     2,     3,     2,
     2,     3,     3,     0,     2,     0,     2,     2,     2,     2,
     4,     2,     2,     1,     1,     1
};

static const short yydefact[] = {     1,
     0,     0,     0,     0,     0,     0,    12,    13,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,    54,    54,    54,    54,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,    54,     3,     2,     7,    56,     0,     0,    59,
     0,    10,     0,    15,     0,     8,    54,    54,    54,    19,
    20,    21,    22,    25,    26,    27,    23,    24,    54,    41,
    54,    45,    47,    54,    43,    54,    54,    28,    29,    30,
    31,    32,    33,    34,    35,    36,    37,    38,    39,    51,
    48,    54,    50,     5,     0,    56,     6,    16,     0,    11,
    14,    44,    17,    18,    40,    55,    46,    42,    52,    53,
    49,     0,     0,     0,     0,    57,    58,     4,     9,    60,
    62,    63,     0,     0,    66,    64,    65,    61,     0,     0
};

static const short yydefgoto[] = {     1,
    45,    46,   106,    47,    70,    94,    97,    48,   116,   128
};

static const short yypact[] = {-32768,
    48,    -3,     5,    -3,     7,     8,-32768,-32768,    -3,    12,
    13,    15,    17,    18,    19,    20,    21,    22,    23,    24,
    25,    26,    27,    28,-32768,    29,    33,    34,    35,    36,
    37,    38,    39,    41,    43,    46,    51,    52,    63,    64,
    66,    75,   101,-32768,-32768,-32768,-32768,    87,    98,-32768,
   -10,-32768,   100,-32768,    -3,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,    -3,
-32768,    -3,    -3,-32768,    -3,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,    -3,   -13,   113,-32768,-32768,-32768,   139,-32768,
-32768,    -3,    -3,    -3,    -3,-32768,    -3,    -3,    -3,    -3,
    -3,   140,   142,   143,   144,-32768,-32768,   -13,-32768,-32768,
-32768,-32768,   145,   -46,-32768,-32768,-32768,-32768,   146,-32768
};

static const short yypgoto[] = {-32768,
-32768,-32768,     1,   102,   -24,    55,-32768,-32768,-32768,-32768
};


#define	YYLAST		151


static const short yytable[] = {    72,
    73,    75,    50,    49,    52,   112,   113,   114,    51,    55,
   125,   126,   127,    53,    56,    54,    57,    58,    93,    59,
    60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
    71,    74,   102,   103,   104,    76,    77,    78,    79,    80,
    81,    82,   115,    83,   105,    84,   107,   129,    85,   108,
    99,   109,   110,    86,    87,   101,     2,     3,     4,     5,
     6,     7,     8,     9,    10,    88,    89,   111,    90,    11,
    12,    13,    14,    15,    16,    17,    18,    91,    19,    20,
    21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
    31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
    41,    42,    43,    92,    95,    98,   100,    44,    11,    12,
    13,    14,    15,    16,    17,    18,   117,    19,    20,    21,
    22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
    32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
    42,    43,   119,   120,   121,   130,   122,   123,   124,    96,
   118
};

static const short yycheck[] = {    24,
    25,    26,     2,     7,     4,    19,    20,    21,     4,     9,
    57,    58,    59,     7,     3,     8,     4,     3,    43,     3,
     3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
     3,     3,    57,    58,    59,     3,     3,     3,     3,     3,
     3,     3,    56,     3,    69,     3,    71,     0,     3,    74,
    61,    76,    77,     3,     3,    55,     9,    10,    11,    12,
    13,    14,    15,    16,    17,     3,     3,    92,     3,    22,
    23,    24,    25,    26,    27,    28,    29,     3,    31,    32,
    33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
    43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
    53,    54,    55,     3,    18,     8,     7,    60,    22,    23,
    24,    25,    26,    27,    28,    29,     4,    31,    32,    33,
    34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
    44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
    54,    55,     4,     4,     3,     0,     4,     4,     4,    48,
    96
};
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
#line 3 "/usr/lib/bison.simple"

/* Skeleton output parser for bison,
   Copyright (C) 1984, 1989, 1990 Bob Corbett and Richard Stallman

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 1, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  */


#ifndef alloca
#ifdef __GNUC__
#define alloca __builtin_alloca
#else /* not GNU C.  */
#if (!defined (__STDC__) && defined (sparc)) || defined (__sparc__)
#include <alloca.h>
#else /* not sparc */
#if defined (MSDOS) && !defined (__TURBOC__)
#include <malloc.h>
#else /* not MSDOS, or __TURBOC__ */
#if defined(_AIX)
#include <malloc.h>
 #pragma alloca
#endif /* not _AIX */
#endif /* not MSDOS, or __TURBOC__ */
#endif /* not sparc.  */
#endif /* not GNU C.  */
#endif /* alloca not defined.  */

/* This is the parser code that is written into each bison parser
  when the %semantic_parser declaration is not specified in the grammar.
  It was written by Richard Stallman by simplifying the hairy parser
  used when %semantic_parser is specified.  */

/* Note: there must be only one dollar sign in this file.
   It is replaced by the list of actions, each action
   as one case of the switch.  */

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	return(0)
#define YYABORT 	return(1)
#define YYERROR		goto yyerrlab1
/* Like YYERROR except do call yyerror.
   This remains here temporarily to ease the
   transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(token, value) \
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    { yychar = (token), yylval = (value);			\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { yyerror ("syntax error: cannot back up"); YYERROR; }	\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

#ifndef YYPURE
#define YYLEX		yylex()
#endif

#ifdef YYPURE
#ifdef YYLSP_NEEDED
#define YYLEX		yylex(&yylval, &yylloc)
#else
#define YYLEX		yylex(&yylval)
#endif
#endif

/* If nonreentrant, generate the variables here */

#ifndef YYPURE

int	yychar;			/*  the lookahead symbol		*/
YYSTYPE	yylval;			/*  the semantic value of the		*/
				/*  lookahead symbol			*/

#ifdef YYLSP_NEEDED
YYLTYPE yylloc;			/*  location data for the lookahead	*/
				/*  symbol				*/
#endif

int yynerrs;			/*  number of parse errors so far       */
#endif  /* not YYPURE */

#if YYDEBUG != 0
int yydebug;			/*  nonzero means print parse trace	*/
/* Since this is uninitialized, it does not stop multiple parsers
   from coexisting.  */
#endif

/*  YYINITDEPTH indicates the initial size of the parser's stacks	*/

#ifndef	YYINITDEPTH
#define YYINITDEPTH 200
#endif

/*  YYMAXDEPTH is the maximum size the stacks can grow to
    (effective only if the built-in stack extension method is used).  */

#if YYMAXDEPTH == 0
#undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
#define YYMAXDEPTH 10000
#endif

#if __GNUC__ > 1		/* GNU C and GNU C++ define this.  */
#define __yy_bcopy(FROM,TO,COUNT)	__builtin_memcpy(TO,FROM,COUNT)
#else				/* not GNU C or C++ */
#ifndef __cplusplus

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_bcopy (from, to, count)
     char *from;
     char *to;
     int count;
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#else /* __cplusplus */

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_bcopy (char *from, char *to, int count)
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#endif
#endif

#line 169 "/usr/lib/bison.simple"
int
yyparse()
{
  register int yystate;
  register int yyn;
  register short *yyssp;
  register YYSTYPE *yyvsp;
  int yyerrstatus;	/*  number of tokens to shift before error messages enabled */
  int yychar1;		/*  lookahead token as an internal (translated) token number */

  short	yyssa[YYINITDEPTH];	/*  the state stack			*/
  YYSTYPE yyvsa[YYINITDEPTH];	/*  the semantic value stack		*/

  short *yyss = yyssa;		/*  refer to the stacks thru separate pointers */
  YYSTYPE *yyvs = yyvsa;	/*  to allow yyoverflow to reallocate them elsewhere */

#ifdef YYLSP_NEEDED
  YYLTYPE yylsa[YYINITDEPTH];	/*  the location stack			*/
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;

#define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
#define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  int yystacksize = YYINITDEPTH;

#ifdef YYPURE
  int yychar;
  YYSTYPE yylval;
  int yynerrs;
#ifdef YYLSP_NEEDED
  YYLTYPE yylloc;
#endif
#endif

  YYSTYPE yyval;		/*  the variable used to return		*/
				/*  semantic values from the action	*/
				/*  routines				*/

  int yylen;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Starting parse\n");
#endif

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.  */

  yyssp = yyss - 1;
  yyvsp = yyvs;
#ifdef YYLSP_NEEDED
  yylsp = yyls;
#endif

/* Push a new state, which is found in  yystate  .  */
/* In all cases, when you get here, the value and location stacks
   have just been pushed. so pushing a state here evens the stacks.  */
yynewstate:

  *++yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Give user a chance to reallocate the stack */
      /* Use copies of these so that the &'s don't force the real ones into memory. */
      YYSTYPE *yyvs1 = yyvs;
      short *yyss1 = yyss;
#ifdef YYLSP_NEEDED
      YYLTYPE *yyls1 = yyls;
#endif

      /* Get the current used size of the three stacks, in elements.  */
      int size = yyssp - yyss + 1;

#ifdef yyoverflow
      /* Each stack pointer address is followed by the size of
	 the data in use in that stack, in bytes.  */
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
#ifdef YYLSP_NEEDED
		 &yyls1, size * sizeof (*yylsp),
#endif
		 &yystacksize);

      yyss = yyss1; yyvs = yyvs1;
#ifdef YYLSP_NEEDED
      yyls = yyls1;
#endif
#else /* no yyoverflow */
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	{
	  yyerror("parser stack overflow");
	  return 2;
	}
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;
      yyss = (short *) alloca (yystacksize * sizeof (*yyssp));
      __yy_bcopy ((char *)yyss1, (char *)yyss, size * sizeof (*yyssp));
      yyvs = (YYSTYPE *) alloca (yystacksize * sizeof (*yyvsp));
      __yy_bcopy ((char *)yyvs1, (char *)yyvs, size * sizeof (*yyvsp));
#ifdef YYLSP_NEEDED
      yyls = (YYLTYPE *) alloca (yystacksize * sizeof (*yylsp));
      __yy_bcopy ((char *)yyls1, (char *)yyls, size * sizeof (*yylsp));
#endif
#endif /* no yyoverflow */

      yyssp = yyss + size - 1;
      yyvsp = yyvs + size - 1;
#ifdef YYLSP_NEEDED
      yylsp = yyls + size - 1;
#endif

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Stack size increased to %d\n", yystacksize);
#endif

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Entering state %d\n", yystate);
#endif

 yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Reading a token: ");
#endif
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Now at end of input.\n");
#endif
    }
  else
    {
      yychar1 = YYTRANSLATE(yychar);

#if YYDEBUG != 0
      if (yydebug)
	{
	  fprintf (stderr, "Next token is %d (%s", yychar, yytname[yychar1]);
	  /* Give the individual parser a way to print the precise meaning
	     of a token, for further debugging info.  */
#ifdef YYPRINT
	  YYPRINT (stderr, yychar, yylval);
#endif
	  fprintf (stderr, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting token %d (%s), ", yychar, yytname[yychar1]);
#endif

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  /* count tokens shifted since error; after three, turn off error status.  */
  if (yyerrstatus) yyerrstatus--;

  yystate = yyn;
  goto yynewstate;

/* Do the default action for the current state.  */
yydefault:

  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;

/* Do a reduction.  yyn is the number of a rule to reduce with.  */
yyreduce:
  yylen = yyr2[yyn];
  yyval = yyvsp[1-yylen]; /* implement default value of the action */

#if YYDEBUG != 0
  if (yydebug)
    {
      int i;

      fprintf (stderr, "Reducing via rule %d (line %d), ",
	       yyn, yyrline[yyn]);

      /* Print the symboles being reduced, and their result.  */
      for (i = yyprhs[yyn]; yyrhs[i] > 0; i++)
	fprintf (stderr, "%s ", yytname[yyrhs[i]]);
      fprintf (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif


  switch (yyn) {

case 8:
#line 80 "parser.y"
{ puts (yyvsp[0].c); ;
    break;}
case 9:
#line 81 "parser.y"
{ map_setscale (yyvsp[0].d); ;
    break;}
case 10:
#line 82 "parser.y"
{ map_setorigin (yyvsp[0].l); ;
    break;}
case 11:
#line 83 "parser.y"
{ map_setparallels (yyvsp[-1].d, yyvsp[0].d); ;
    break;}
case 12:
#line 84 "parser.y"
{ map_display(); ;
    break;}
case 13:
#line 85 "parser.y"
{ digi_input(); ;
    break;}
case 14:
#line 86 "parser.y"
{ digi_calibrate(yyvsp[-1].l, yyvsp[0].l); ;
    break;}
case 15:
#line 87 "parser.y"
{ map_setzero ((long) yyvsp[0].d); ;
    break;}
case 16:
#line 91 "parser.y"
{yyval.l = makelocation((long)yyvsp[-1].d, (long)yyvsp[0].d);;
    break;}
case 17:
#line 96 "parser.y"
{ new_object (O_INTLAIRPORT, yyvsp[-1].c); ;
    break;}
case 18:
#line 97 "parser.y"
{ new_object (O_AIRPORT, yyvsp[-1].c); ;
    break;}
case 19:
#line 98 "parser.y"
{ new_object (O_AIRPORT_CIV_MIL, yyvsp[0].c); ;
    break;}
case 20:
#line 99 "parser.y"
{ new_object (O_AIRPORT_MIL, yyvsp[0].c); ;
    break;}
case 21:
#line 100 "parser.y"
{ new_object (O_AIRFIELD, yyvsp[0].c); ;
    break;}
case 22:
#line 101 "parser.y"
{ new_object (O_SPECIAL_AIRFIELD, yyvsp[0].c); ;
    break;}
case 23:
#line 102 "parser.y"
{ new_object (O_HELIPORT, yyvsp[0].c); ;
    break;}
case 24:
#line 103 "parser.y"
{ new_object (O_HELIPORT_AMB, yyvsp[0].c); ;
    break;}
case 25:
#line 104 "parser.y"
{ new_object (O_GLIDER_SITE, yyvsp[0].c); ;
    break;}
case 26:
#line 105 "parser.y"
{ new_object (O_PARACHUTE_JUMPING_SITE, yyvsp[0].c); ;
    break;}
case 27:
#line 106 "parser.y"
{ new_object (O_FREE_BALLON_SITE, yyvsp[0].c); ;
    break;}
case 28:
#line 107 "parser.y"
{ new_object (O_VOR, yyvsp[0].c); ;
    break;}
case 29:
#line 108 "parser.y"
{ new_object (O_VOR_DME, yyvsp[0].c); ;
    break;}
case 30:
#line 109 "parser.y"
{ new_object (O_VORTAC, yyvsp[0].c); ;
    break;}
case 31:
#line 110 "parser.y"
{ new_object (O_TACAN, yyvsp[0].c); ;
    break;}
case 32:
#line 111 "parser.y"
{ new_object (O_NDB, yyvsp[0].c); ;
    break;}
case 33:
#line 112 "parser.y"
{ new_object (O_MARKER_BEACON, yyvsp[0].c); ;
    break;}
case 34:
#line 113 "parser.y"
{ new_object (O_BASIC_RADIO_FACILITY, yyvsp[0].c); ;
    break;}
case 35:
#line 114 "parser.y"
{ new_object (O_OBSTRUCTION, yyvsp[0].c); ;
    break;}
case 36:
#line 115 "parser.y"
{ new_object (O_GROUP_OBSTRUCTION, yyvsp[0].c); ;
    break;}
case 37:
#line 116 "parser.y"
{ new_object (O_FIRED_OBSTRUCTION, yyvsp[0].c); ;
    break;}
case 38:
#line 117 "parser.y"
{ new_object (O_FIRED_GROUP_OBSTRUCTION, yyvsp[0].c); ;
    break;}
case 39:
#line 118 "parser.y"
{ new_object (O_REPORTING_POINT, yyvsp[0].c); ;
    break;}
case 40:
#line 119 "parser.y"
{ new_object (O_RIVER, yyvsp[-1].c); ;
    break;}
case 41:
#line 120 "parser.y"
{ new_object (O_RIVER, ""); ;
    break;}
case 42:
#line 121 "parser.y"
{ new_object (O_LAKE, yyvsp[-1].c); ;
    break;}
case 43:
#line 122 "parser.y"
{ new_object (O_LAKE, ""); ;
    break;}
case 44:
#line 123 "parser.y"
{ new_elev (yyvsp[-1].d); 
					      new_object (O_ISOGONE, ""); ;
    break;}
case 45:
#line 125 "parser.y"
{ new_object (O_HIGHWAY, ""); ;
    break;}
case 46:
#line 126 "parser.y"
{ new_object (O_HIGHWAY, yyvsp[-1].c); ;
    break;}
case 47:
#line 127 "parser.y"
{ new_object (O_ROAD, ""); ;
    break;}
case 48:
#line 128 "parser.y"
{ new_object (O_VILLAGE, yyvsp[0].c); ;
    break;}
case 49:
#line 129 "parser.y"
{ new_object (O_TOWN, yyvsp[-1].c); ;
    break;}
case 50:
#line 130 "parser.y"
{ new_object (O_TOWN, ""); ;
    break;}
case 51:
#line 131 "parser.y"
{ new_object (O_WAYPOINT, yyvsp[0].c); ;
    break;}
case 52:
#line 132 "parser.y"
{ new_object (O_CTR, yyvsp[-1].c); ;
    break;}
case 53:
#line 133 "parser.y"
{ new_object (O_CVFR, yyvsp[-1].c); ;
    break;}
case 55:
#line 137 "parser.y"
{ add_point (yyvsp[0].l); ;
    break;}
case 58:
#line 147 "parser.y"
{ new_elev (yyvsp[0].d); ;
    break;}
case 59:
#line 150 "parser.y"
{ new_location (yyvsp[0].l); ;
    break;}
case 60:
#line 153 "parser.y"
{ set_frequency (yyvsp[0].d); ;
    break;}
case 61:
#line 154 "parser.y"
{ add_runway (yyvsp[-2].d, yyvsp[-1].d, yyvsp[0].s); ;
    break;}
case 62:
#line 155 "parser.y"
{ set_alias (yyvsp[0].c); ;
    break;}
case 63:
#line 156 "parser.y"
{ set_range (yyvsp[0].d); ;
    break;}
case 64:
#line 160 "parser.y"
{ yyval.s = 'A'; ;
    break;}
case 65:
#line 161 "parser.y"
{ yyval.s = 'C'; ;
    break;}
case 66:
#line 162 "parser.y"
{ yyval.s = 'G'; ;
    break;}
}
   /* the action file gets copied in in place of this dollarsign */
#line 440 "/usr/lib/bison.simple"

  yyvsp -= yylen;
  yyssp -= yylen;
#ifdef YYLSP_NEEDED
  yylsp -= yylen;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;

#ifdef YYLSP_NEEDED
  yylsp++;
  if (yylen == 0)
    {
      yylsp->first_line = yylloc.first_line;
      yylsp->first_column = yylloc.first_column;
      yylsp->last_line = (yylsp-1)->last_line;
      yylsp->last_column = (yylsp-1)->last_column;
      yylsp->text = 0;
    }
  else
    {
      yylsp->last_line = (yylsp+yylen-1)->last_line;
      yylsp->last_column = (yylsp+yylen-1)->last_column;
    }
#endif

  /* Now "shift" the result of the reduction.
     Determine what state that goes to,
     based on the state we popped back to
     and the rule number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;

yyerrlab:   /* here on detecting error */

  if (! yyerrstatus)
    /* If not already recovering from an error, report this error.  */
    {
      ++yynerrs;

#ifdef YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  int size = 0;
	  char *msg;
	  int x, count;

	  count = 0;
	  for (x = 0; x < (sizeof(yytname) / sizeof(char *)); x++)
	    if (yycheck[x + yyn] == x)
	      size += strlen(yytname[x]) + 15, count++;
	  msg = (char *) malloc(size + 15);
	  if (msg != 0)
	    {
	      strcpy(msg, "parse error");

	      if (count < 5)
		{
		  count = 0;
		  for (x = 0; x < (sizeof(yytname) / sizeof(char *)); x++)
		    if (yycheck[x + yyn] == x)
		      {
			strcat(msg, count == 0 ? ", expecting `" : " or `");
			strcat(msg, yytname[x]);
			strcat(msg, "'");
			count++;
		      }
		}
	      yyerror(msg);
	      free(msg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exceeded");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror("parse error");
    }

yyerrlab1:   /* here on error raised explicitly by an action */

  if (yyerrstatus == 3)
    {
      /* if just tried and failed to reuse lookahead token after an error, discard it.  */

      /* return failure if at end of input */
      if (yychar == YYEOF)
	YYABORT;

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Discarding token %d (%s).\n", yychar, yytname[yychar1]);
#endif

      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token
     after shifting the error token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;

yyerrdefault:  /* current state does not do anything special for the error token. */

#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */
  yyn = yydefact[yystate];  /* If its default is to accept any token, ok.  Otherwise pop it.*/
  if (yyn) goto yydefault;
#endif

yyerrpop:   /* pop the current state because it cannot handle the error token */

  if (yyssp == yyss) YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#ifdef YYLSP_NEEDED
  yylsp--;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "Error: state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

yyerrhandle:

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting error token, ");
#endif

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  yystate = yyn;
  goto yynewstate;
}
#line 168 "parser.y"



yyerror (char *s)
{
  printf ("%s processing line %d\n", s, currentline);
}
