typedef union {
	char *c;
        char s;
	float d;
	LOCATION l;
} YYSTYPE;
#define	tSTRING	258
#define	tFLOAT	259
#define	tCHAR	260
#define	tLOCATION	261
#define	tLATITUDE	262
#define	tLONGITUDE	263
#define	tAT	264
#define	tSCALE	265
#define	tORIGIN	266
#define	tPARALLELS	267
#define	tZEROMERIDIAN	268
#define	tSHOW_MAP	269
#define	tDIGITIZER	270
#define	tCALIBRATE	271
#define	tOUT	272
#define	tELEV	273
#define	tFREQUENCY	274
#define	tALIAS	275
#define	tRANGE	276
#define	tISOGONE	277
#define	tINTLAIRPORT	278
#define	tAIRPORT	279
#define	tAIRPORT_CIV_MIL	280
#define	tAIRPORT_MIL	281
#define	tAIRFIELD	282
#define	tSPECIAL_AIRFIELD	283
#define	tGLIDER_SITE	284
#define	tHANG_GLIDER_SITE	285
#define	tPARACHUTE_JUMPING_SITE	286
#define	tFREE_BALLON_SITE	287
#define	tHELIPORT	288
#define	tHELIPORT_AMB	289
#define	tRIVER	290
#define	tHIGHWAY	291
#define	tROAD	292
#define	tLAKE	293
#define	tCTR	294
#define	tCVFR	295
#define	tVOR	296
#define	tVOR_DME	297
#define	tVORTAC	298
#define	tTACAN	299
#define	tNDB	300
#define	tMARKER_BEACON	301
#define	tBASIC_RADIO_FACILITY	302
#define	tOBSTRUCTION	303
#define	tGROUP_OBSTRUCTION	304
#define	tFIRED_OBSTRUCTION	305
#define	tFIRED_GROUP_OBSTRUCTION	306
#define	tREPORTING_POINT	307
#define	tWAYPOINT	308
#define	tVILLAGE	309
#define	tTOWN	310
#define	tRUNWAY	311
#define	tGRAS	312
#define	tASPHALT	313
#define	tCONCRETE	314


extern YYSTYPE yylval;
