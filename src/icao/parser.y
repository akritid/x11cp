%{

#include "objects.h"


LOCATION makelocation (long lat, long longi)
{
  LOCATION temp;

  temp.latitude = lat;
  temp.longitude = longi;
  return temp;
}

void map_setzero (long zero);

int currentline = 1;

%}


%union {
	char *c;
        char s;
	float d;
	LOCATION l;
}


%token tSTRING
%token tFLOAT
%token tCHAR

%token tLOCATION
%token tLATITUDE
%token tLONGITUDE
%token tAT

%token tSCALE tORIGIN tPARALLELS tZEROMERIDIAN
%token tSHOW_MAP
%token tDIGITIZER tCALIBRATE

%type <c> tSTRING
%type <d> tFLOAT
%type <s> tCHAR
%type <l> tLOCATION

%token tOUT

%token tAT tELEV tFREQUENCY tALIAS tRANGE tISOGONE
%token tINTLAIRPORT tAIRPORT tAIRPORT_CIV_MIL tAIRPORT_MIL tAIRFIELD tSPECIAL_AIRFIELD
%token tGLIDER_SITE tHANG_GLIDER_SITE tPARACHUTE_JUMPING_SITE tFREE_BALLON_SITE
%token tHELIPORT tHELIPORT_AMB tRIVER tHIGHWAY tROAD tLAKE

%token tCTR, tCVFR

%token tVOR tVOR_DME tVORTAC tTACAN tNDB tMARKER_BEACON tBASIC_RADIO_FACILITY 

%token tOBSTRUCTION tGROUP_OBSTRUCTION tFIRED_OBSTRUCTION tFIRED_GROUP_OBSTRUCTION

%token tREPORTING_POINT, tWAYPOINT, tVILLAGE, tTOWN

%token tRUNWAY tGRAS tASPHALT tCONCRETE


%%

input:	/* empty */
	| input line
;

line:	'\n'
	| loc object qualities
	| object qualities
	| loc elev
	| command
;

command:
	tOUT tSTRING		{ puts ($<c>2); }
	| tSCALE tFLOAT ':' tFLOAT	{ map_setscale ($<d>4); }
	| tORIGIN location	{ map_setorigin ($<l>2); }
	| tPARALLELS tLATITUDE tLATITUDE  { map_setparallels ($<d>2, $<d>3); }
	| tSHOW_MAP	{ map_display(); }
	| tDIGITIZER	{ digi_input(); }
	| tCALIBRATE location location	{ digi_calibrate($<l>2, $<l>3); }
	| tZEROMERIDIAN tLONGITUDE	{ map_setzero ((long) $<d>2); }
;

location:
	tLATITUDE tLONGITUDE {$<l>$ = makelocation((long)$<d>1, (long)$<d>2);}

;

object:
	tINTLAIRPORT tSTRING pointlist      { new_object (O_INTLAIRPORT, $<c>2); }
	| tAIRPORT tSTRING pointlist        { new_object (O_AIRPORT, $<c>2); }
	| tAIRPORT_CIV_MIL         tSTRING  { new_object (O_AIRPORT_CIV_MIL, $<c>2); }
	| tAIRPORT_MIL             tSTRING  { new_object (O_AIRPORT_MIL, $<c>2); }
	| tAIRFIELD                tSTRING  { new_object (O_AIRFIELD, $<c>2); }
	| tSPECIAL_AIRFIELD        tSTRING  { new_object (O_SPECIAL_AIRFIELD, $<c>2); }
	| tHELIPORT                tSTRING  { new_object (O_HELIPORT, $<c>2); }
	| tHELIPORT_AMB            tSTRING  { new_object (O_HELIPORT_AMB, $<c>2); }
	| tGLIDER_SITE             tSTRING  { new_object (O_GLIDER_SITE, $<c>2); }
	| tPARACHUTE_JUMPING_SITE  tSTRING  { new_object (O_PARACHUTE_JUMPING_SITE, $<c>2); }
	| tFREE_BALLON_SITE        tSTRING  { new_object (O_FREE_BALLON_SITE, $<c>2); }
	| tVOR                     tSTRING  { new_object (O_VOR, $<c>2); }
	| tVOR_DME                 tSTRING  { new_object (O_VOR_DME, $<c>2); }
	| tVORTAC                  tSTRING  { new_object (O_VORTAC, $<c>2); }
	| tTACAN                   tSTRING  { new_object (O_TACAN, $<c>2); }
	| tNDB                     tSTRING  { new_object (O_NDB, $<c>2); }
	| tMARKER_BEACON           tSTRING  { new_object (O_MARKER_BEACON, $<c>2); }
	| tBASIC_RADIO_FACILITY    tSTRING  { new_object (O_BASIC_RADIO_FACILITY, $<c>2); }
	| tOBSTRUCTION             tSTRING  { new_object (O_OBSTRUCTION, $<c>2); }
	| tGROUP_OBSTRUCTION       tSTRING  { new_object (O_GROUP_OBSTRUCTION, $<c>2); }
	| tFIRED_OBSTRUCTION       tSTRING  { new_object (O_FIRED_OBSTRUCTION, $<c>2); }
	| tFIRED_GROUP_OBSTRUCTION tSTRING  { new_object (O_FIRED_GROUP_OBSTRUCTION, $<c>2); }
	| tREPORTING_POINT         tSTRING  { new_object (O_REPORTING_POINT, $<c>2); }
	| tRIVER tSTRING pointlist	    { new_object (O_RIVER, $<c>2); }
	| tRIVER pointlist		    { new_object (O_RIVER, ""); }
	| tLAKE tSTRING pointlist	    { new_object (O_LAKE, $<c>2); }
	| tLAKE pointlist	    	    { new_object (O_LAKE, ""); }
	| tISOGONE tFLOAT pointlist	    { new_elev ($<d>2); 
					      new_object (O_ISOGONE, ""); }
	| tHIGHWAY pointlist		    { new_object (O_HIGHWAY, ""); }
	| tHIGHWAY tSTRING pointlist	    { new_object (O_HIGHWAY, $<c>2); }
	| tROAD pointlist		    { new_object (O_ROAD, ""); }
	| tVILLAGE tSTRING		    { new_object (O_VILLAGE, $<c>2); }
	| tTOWN tSTRING pointlist	    { new_object (O_TOWN, $<c>2); }
	| tTOWN pointlist		    { new_object (O_TOWN, ""); }
	| tWAYPOINT tSTRING		    { new_object (O_WAYPOINT, $<c>2); }
	| tCTR tSTRING pointlist	    { new_object (O_CTR, $<c>2); }
	| tCVFR tSTRING pointlist	    { new_object (O_CVFR, $<c>2); }
;

pointlist:	/* empty */
	| pointlist location		{ add_point ($<l>2); }

;

qualities:	/* empty */
	| qualities quality

;


elev:	tELEV tFLOAT     { new_elev ($<d>2); }
;

loc:	tAT location     { new_location ($<l>2); }

quality:
	tFREQUENCY tFLOAT { set_frequency ($<d>2); }
	| tRUNWAY tFLOAT tFLOAT runwaytype { add_runway ($<d>2, $<d>3, $<s>4); }
	| tALIAS tSTRING { set_alias ($<c>2); }
	| tRANGE tFLOAT { set_range ($<d>2); }
;

runwaytype:
	tASPHALT		{ $<s>$ = 'A'; }
	| tCONCRETE		{ $<s>$ = 'C'; }
	| tGRAS			{ $<s>$ = 'G'; }
;




%%


yyerror (char *s)
{
  printf ("%s processing line %d\n", s, currentline);
}
