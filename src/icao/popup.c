/* popup windows for icao map */


#define POPUP_SRC
#include "popup.h"

#include <stdio.h>
#include <string.h>

#include <Xm/Xm.h>
#include <Xm/ScrolledW.h>
#include <Xm/PushB.h>
#include <Xm/Form.h>
#include <Xm/Label.h>
#include <Xm/DialogS.h>

#include <X11/Intrinsic.h>     /* Intrinsics Definitions*/
#include <X11/StringDefs.h>    /* Standard Name-String definitions*/
#include <X11/Xaw/Command.h>   /* Athena Command Widget */
#include <X11/Xaw/Form.h>  
#include <X11/Xaw/Viewport.h>  
#include <X11/Xaw/List.h>  
#include <X11/Shell.h>  


#define MAX_POPUP     10
#define MAX_LINES    100

Widget popupShell[MAX_POPUP];   /* top level shell widgets */
extern Widget toplevel;

char *popupstrings[MAX_LINES];



void initpopup()
{
  int i;
  
  for (i=0; i<MAX_LINES; i++)     /* allocate space for strings */
    popupstrings[i] = (char *) malloc (100);

  for (i=0; i<MAX_POPUP; i++)
    popupShell[i] = NULL;
}


void popupCB (Widget w, XtPointer client_data, XmPushButtonCallbackStruct *cbs)
{
  Widget parent = (Widget) client_data;

  XtUnmapWidget (parent);
}


/* Motif version to popup an info box */

void popupbox (int lines, char *title)
{
  Widget     shell, form, label, button, scrolled;
  char       tempstr[2000];
  char       *ptr;
  int        i, width, height;
  String     string;
  XmFontList fontlist;

  shell = XtVaCreateManagedWidget ("infodialog", xmDialogShellWidgetClass, toplevel,
				   XmNtitle,             title,
				   XmNdeleteResponse,    XmDESTROY,
				   XmNallowShellResize,  True,
				   XmNmappedWhenManaged, False,
				   NULL);
  
  form = XtVaCreateManagedWidget ("infoform", xmFormWidgetClass, shell, 
				  NULL);
  
  scrolled = XtVaCreateWidget ("infoscrolled", xmScrolledWindowWidgetClass, form,
				      XmNwidth,                  200,
				      XmNheight,                 200,
				      XmNtopAttachment,          XmATTACH_FORM,
			              XmNtopOffset,              5,
				      XmNleftAttachment,         XmATTACH_FORM,
			              XmNleftOffset,             5,
				      XmNrightAttachment,        XmATTACH_FORM,
			              XmNrightOffset,            5,
				      XmNbottomAttachment,       XmATTACH_FORM,
			              XmNbottomOffset,           5,
				      XmNbottomOffset,           45,
				      XmNscrollingPolicy,        XmAUTOMATIC,
				      NULL);

  /* make single string from text lines */
  
  ptr = tempstr;
  for (i=0; i<lines; i++)
    {
      strcpy (ptr, popupstrings[i]);
      ptr += strlen (popupstrings[i]);
      strcpy (ptr, "\n");
      ptr += strlen ("\n");
    }
  
  string = XmStringCreateLtoR (tempstr,"charset");

  label = XtVaCreateManagedWidget ("infolabel", xmLabelWidgetClass, scrolled,
				   XmNlabelString,    string,
				   XmNalignment,      XmALIGNMENT_BEGINNING,
				   NULL);

  XtVaGetValues (label,                      /* get font list */
		 XmNfontList,  &fontlist,
		 NULL);

  width  = XmStringWidth (fontlist, string) + 10;
  height = XmStringHeight (fontlist, string) + 10;
  if (height > 300)
    {
      height = 300;    /* limit vertical size of text */
      width += 25;     /* add room for scrollbar */
    }

  XtVaSetValues (scrolled,                   /* determine label size */
		 XmNwidth,  width,
		 XmNheight, height,
		 NULL);
  XmStringFree (string);

  string = XmStringCreateSimple ("Close");
  button = XtVaCreateManagedWidget ("infoclose", xmPushButtonWidgetClass, form,
				    XmNlabelString,      string,
				    XmNtopAttachment,    XmATTACH_WIDGET,
				    XmNtopWidget,        scrolled,
				    XmNtopOffset,        10,
				    XmNbottomAttachment, XmATTACH_FORM,
				    XmNbottomOffset,     10,
				    XmNleftAttachment,   XmATTACH_FORM,
				    XmNleftOffset,       10,
				    NULL);

  XtAddCallback (button, XmNactivateCallback, (XtCallbackProc) popupCB, 
		 (XtPointer) shell);
  XmStringFree (string);

  XtManageChild (scrolled);
  XtMapWidget (shell);
}


