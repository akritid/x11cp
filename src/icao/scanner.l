%{
#include <string.h>
#include "coord.h"
#include "parser.tab.h"
extern char *strsave();
extern int currentline;
static double mytmp;
%}

alpha	[a-zA-Z]
special	[\.\_-]
digit	[0-9]
exp	[Ee][-+]?{digit}+
string	"/"?{alpha}([/(),]|{alpha}|{digit}|{special}|"/")*

%p 5000
%%

[ \t]			;
[\n]			currentline++;

"/*"			skipcomments();

intlairport		return tINTLAIRPORT;
airport			return tAIRPORT;
airport_civ_mil		return tAIRPORT_CIV_MIL;
airport_mil		return tAIRPORT_MIL;
airfield		return tAIRFIELD;
special_airfield	return tSPECIAL_AIRFIELD;
heliport		return tHELIPORT;
heliport_amb		return tHELIPORT_AMB;
glider_site		return tGLIDER_SITE;
hang_glider_site	return tHANG_GLIDER_SITE;
parachute_jumping_site	return tPARACHUTE_JUMPING_SITE;
free_ballon_site	return tFREE_BALLON_SITE;

vor			return tVOR;
vor_dme			return tVOR_DME;
vortac			return tVORTAC;
tacan			return tTACAN;
ndb			return tNDB;
marker_beacon		return tMARKER_BEACON;
basic_radio_facility	return tBASIC_RADIO_FACILITY;

obstruction		return tOBSTRUCTION;
group_obstruction	return tGROUP_OBSTRUCTION;
fired_obstruction	return tFIRED_OBSTRUCTION;
fired_group_obstruction	return tFIRED_GROUP_OBSTRUCTION;
waypoint		return tWAYPOINT;

isogone			return tISOGONE;

river			return tRIVER;
lake			return tLAKE;
highway			return tHIGHWAY;
road			return tROAD;
village			return tVILLAGE;
town			return tTOWN;

ctr			return tCTR;
cvfr			return tCVFR;

reporting_point		return tREPORTING_POINT;

at			return tAT;
@			return tAT;
elev			return tELEV;
range			return tRANGE;
frequency		return tFREQUENCY;
alias			return tALIAS;
runway			return tRUNWAY;
gras			return tGRAS;
asphalt			return tASPHALT;
asph			return tASPHALT;
concrete		return tCONCRETE;
conc			return tCONCRETE;
out			return tOUT;
scale			return tSCALE;
origin			return tORIGIN;
parallels		return tPARALLELS;
show_map		return tSHOW_MAP;
digitize		return tDIGITIZER;
calibrate		return tCALIBRATE;
zeromeridian		return tZEROMERIDIAN;


{digit}{2}.{digit}{2}\047{digit}{2}\042[NS]	{yylval.d = deg2num(yytext); return tLATITUDE;}
{digit}{2}.{digit}{2}\047{digit}{2}\042[EW]	{yylval.d = deg2num(yytext); return tLONGITUDE;}
{digit}{3}.{digit}{2}\047{digit}{2}\042[EW]	{yylval.d = deg2num(yytext); return tLONGITUDE;}

{digit}+ |
{digit}+"."{digit}* |
"."{digit}+ 	    	{sscanf (yytext,"%lf",&mytmp); yylval.d=mytmp; return tFLOAT;}
{string}		{yylval.c = strsave(yytext); return tSTRING;}
.			return yytext[0];

%%


/* yywrap()
{
  return 1;
} */



/*
 * Skip over comments.
 */
skipcomments()
{
	char c;

	while (1) {
		while (input() != '*')
			;
		if ((c = input()) == '/')
			return;
		unput(c);
	}
}





char *strsave(s)
char *s;
{
	char *tmp;

	if (s == (char *)NULL)
		return (char *)NULL;

	tmp = (char *)malloc((unsigned)strlen(s) + 1);
	(void)strcpy(tmp, s);
	return tmp;
}













