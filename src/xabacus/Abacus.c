/*-
# X-BASED ABACUS
#
#  Abacus.c
#
###
#
#  Copyright (c) 1994 - 99	David Albert Bagley, bagleyd@tux.org
#
#                   All Rights Reserved
#
#  Permission to use, copy, modify, and distribute this software and
#  its documentation for any purpose and without fee is hereby granted,
#  provided that the above copyright notice appear in all copies and
#  that both that copyright notice and this permission notice appear in
#  supporting documentation, and that the name of the author not be
#  used in advertising or publicity pertaining to distribution of the
#  software without specific, written prior permission.
#
#  This program is distributed in the hope that it will be "useful",
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
*/

/* Methods file for Abacus */

#include <stdlib.h>
#include <stdio.h>
#ifdef VMS
#include <unixlib.h>
#else
#if HAVE_UNISTD_H
#include <unistd.h>
#endif
#endif
#include <X11/IntrinsicP.h>
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/CoreP.h>
#include "AbacusP.h"

static void InitializeAbacus(Widget request, Widget renew);
static void ExposeAbacus(Widget renew, XEvent * event, Region region);
static void ResizeAbacus(AbacusWidget w);
static void DestroyAbacus(Widget old);
static Boolean SetValuesAbacus(Widget current, Widget request, Widget renew);

static void QuitAbacus(AbacusWidget w, XEvent * event, char **args, int nArgs);
static void SelectAbacus(AbacusWidget w, XEvent * event, char **args, int nArgs);
static void ReleaseAbacus(AbacusWidget w, XEvent * event, char **args, int nArgs);
static void NextAbacus(AbacusWidget w, XEvent * event, char **args, int nArgs);
static void RepeatAbacus(AbacusWidget w, XEvent * event, char **args, int nArgs);
static void MoreAbacus(AbacusWidget w, XEvent * event, char **args, int nArgs);
static void ClearAbacus(AbacusWidget w, XEvent * event, char **args, int nArgs);
static void IncrementAbacus(AbacusWidget w, XEvent * event, char **args, int nArgs);
static void DecrementAbacus(AbacusWidget w, XEvent * event, char **args, int nArgs);

static void SetAllColors(AbacusWidget w, Boolean init);
static Boolean ShiftRails(AbacusWidget w, int shift);
static void ShiftedRails(AbacusWidget c, AbacusWidget w);
static int  PositionToBead(AbacusWidget w, int x, int y, int *deck, int *rail, int *j);
static void CheckBeads(AbacusWidget w);
static void ResetBeads(AbacusWidget w);
static void ClearAllBeads(AbacusWidget w);
static void MoveBeadsByPos(AbacusWidget w, int deck, int rail, int j);
static void MoveBeadsUp(AbacusWidget w, int deck, int rail, int j);
static void MoveBeadsDown(AbacusWidget w, int deck, int rail, int j);
static void DrawFrame(AbacusWidget w, GC frameGC, GC railGC);
static void DrawAllBeads(AbacusWidget w);
static void DrawBead(AbacusWidget w, int deck, int rail, int j, Boolean erase, int offset);
static void AddBead(AbacusWidget w, int deck, int p);
static void SubBead(AbacusWidget w, int deck, int p);
static Boolean EmptyCounter(AbacusWidget w);
static void SetCounter(AbacusWidget w, int deck, int rail, int number);
static void MoveBeadsByValue(AbacusWidget w, int deck, int rail, int number);

static char defaultTranslationsAbacus[] =
"<KeyPress>q: quit()\n\
   Ctrl<KeyPress>C: quit()\n\
   <Btn1Down>: select()\n\
   <Btn1Up>: release()\n\
   <KeyPress>c: clear()\n\
   <Btn3Down>: clear()\n\
   <KeyPress>i: increment()\n\
   <KeyPress>d: decrement()\n\
   <KeyPress>n: next()\n\
   <KeyPress>r: repeat()\n\
   <KeyPress>0x20: more()\n\
   <KeyPress>0x40: more()\n\
   <KeyPress>KP_Space: more()\n\
   <KeyPress>Return: more()";

/* KP_Space does not work here
   0x20 is SP (' ') in ASCII  (DP in EBCDIC)
   0x40 is SP (' ') in EBCDIC ('@' in ASCII)
 */

static XtActionsRec actionsListAbacus[] =
{
	{"quit", (XtActionProc) QuitAbacus},
	{"select", (XtActionProc) SelectAbacus},
	{"release", (XtActionProc) ReleaseAbacus},
	{"clear", (XtActionProc) ClearAbacus},
	{"increment", (XtActionProc) IncrementAbacus},
	{"decrement", (XtActionProc) DecrementAbacus},
	{"next", (XtActionProc) NextAbacus},
	{"repeat", (XtActionProc) RepeatAbacus},
	{"more", (XtActionProc) MoreAbacus}
};

static XtResource resourcesAbacus[] =
{
	{XtNwidth, XtCWidth, XtRDimension, sizeof (Dimension),
	 XtOffset(AbacusWidget, core.width), XtRString, "436"},
	{XtNheight, XtCHeight, XtRDimension, sizeof (Dimension),
	 XtOffset(AbacusWidget, core.height), XtRString, "215"},
	{XtNrails, XtCRails, XtRInt, sizeof (int),
	 XtOffset(AbacusWidget, abacus.rails), XtRString, "13"},
	{XtNbase, XtCBase, XtRInt, sizeof (int),
	 XtOffset(AbacusWidget, abacus.base), XtRString, "10"},
	{XtNspaces, XtCSpaces, XtRInt, sizeof (int),
	 XtOffset(AbacusWidget, abacus.spaces), XtRString, "2"},
	{XtNforeground, XtCForeground, XtRPixel, sizeof (Pixel),
  XtOffset(AbacusWidget, abacus.foreground), XtRString, XtDefaultForeground},
	{XtNrailColor, XtCForeground, XtRPixel, sizeof (Pixel),
   XtOffset(AbacusWidget, abacus.railColor), XtRString, XtDefaultForeground},
	{XtNbeadColor, XtCForeground, XtRPixel, sizeof (Pixel),
   XtOffset(AbacusWidget, abacus.beadColor), XtRString, XtDefaultForeground},
	{XtNbeadBorder, XtCForeground, XtRPixel, sizeof (Pixel),
 XtOffset(AbacusWidget, abacus.borderColor), XtRString, XtDefaultForeground},
	{XtNmono, XtCMono, XtRBoolean, sizeof (Boolean),
	 XtOffset(AbacusWidget, abacus.mono), XtRString, "FALSE"},
	{XtNreverse, XtCReverse, XtRBoolean, sizeof (Boolean),
	 XtOffset(AbacusWidget, abacus.reverse), XtRString, "FALSE"},
	{XtNtopNumber, XtCTopNumber, XtRInt, sizeof (int),
	 XtOffset(AbacusWidget, abacus.decks[TOP].number), XtRString, "2"},
	{XtNbottomNumber, XtCBottomNumber, XtRInt, sizeof (int),
	 XtOffset(AbacusWidget, abacus.decks[BOTTOM].number), XtRString, "5"},
	{XtNtopFactor, XtCTopFactor, XtRInt, sizeof (int),
	 XtOffset(AbacusWidget, abacus.decks[TOP].factor), XtRString, "5"},
	{XtNbottomFactor, XtCBottomFactor, XtRInt, sizeof (int),
	 XtOffset(AbacusWidget, abacus.decks[BOTTOM].factor), XtRString, "1"},
	{XtNtopOrient, XtCTopOrient, XtRBoolean, sizeof (Boolean),
   XtOffset(AbacusWidget, abacus.decks[TOP].orientation), XtRString, "TRUE"},
	{XtNbottomOrient, XtCBottomOrient, XtRBoolean, sizeof (Boolean),
XtOffset(AbacusWidget, abacus.decks[BOTTOM].orientation), XtRString, "FALSE"},
	{XtNdelay, XtCDelay, XtRInt, sizeof (int),
	 XtOffset(AbacusWidget, abacus.delay), XtRString, "100"},
	{XtNscript, XtCScript, XtRBoolean, sizeof (Boolean),
	 XtOffset(AbacusWidget, abacus.script), XtRString, "FALSE"},
	{XtNdemo, XtCDemo, XtRBoolean, sizeof (Boolean),
	 XtOffset(AbacusWidget, abacus.demo), XtRString, "FALSE"},

	{XtNdemoPath, XtCDemoPath, XtRString, sizeof (String),
	 XtOffset(AbacusWidget, abacusDemo.path), XtRString, DEMOPATH},
	{XtNdemoFont, XtCDemoFont, XtRString, sizeof (Font),
	 XtOffset(AbacusWidget, abacusDemo.font), XtRString, "-*-times-*-r-*-*-*-180-*-*-*-*"},
	{XtNdemoForeground, XtCForeground, XtRPixel, sizeof (Pixel),
	 XtOffset(AbacusWidget, abacusDemo.foreground), XtRString, XtDefaultForeground},
	{XtNdemoBackground, XtCBackground, XtRPixel, sizeof (Pixel),
	 XtOffset(AbacusWidget, abacusDemo.background), XtRString, XtDefaultBackground},

	{XtNdeck, XtCDeck, XtRInt, sizeof (int),
	 XtOffset(AbacusWidget, abacus.deck), XtRString, "-1"},
	{XtNrail, XtCRail, XtRInt, sizeof (int),
	 XtOffset(AbacusWidget, abacus.rail), XtRString, "0"},
	{XtNnumber, XtCNumber, XtRInt, sizeof (int),
	 XtOffset(AbacusWidget, abacus.number), XtRString, "0"},
	{XtNselectCallback, XtCCallback, XtRCallback, sizeof (caddr_t),
	 XtOffset(AbacusWidget, abacus.select), XtRCallback, NULL}
};

AbacusClassRec abacusClassRec =
{
	{
		(WidgetClass) & widgetClassRec,		/* superclass */
		"Abacus",	/* class name */
		sizeof (AbacusRec),	/* widget size */
		NULL,		/* class initialize */
		NULL,		/* class part initialize */
		FALSE,		/* class inited */
		(XtInitProc) InitializeAbacus,	/* initialize */
		NULL,		/* initialize hook */
		XtInheritRealize,	/* realize */
		actionsListAbacus,	/* actions */
		XtNumber(actionsListAbacus),	/* num actions */
		resourcesAbacus,	/* resources */
		XtNumber(resourcesAbacus),	/* num resources */
		NULLQUARK,	/* xrm class */
		TRUE,		/* compress motion */
		TRUE,		/* compress exposure */
		TRUE,		/* compress enterleave */
		TRUE,		/* visible interest */
		(XtWidgetProc) DestroyAbacus,	/* destroy */
		(XtWidgetProc) ResizeAbacus,	/* resize */
		(XtExposeProc) ExposeAbacus,	/* expose */
		(XtSetValuesFunc) SetValuesAbacus,	/* set values */
		NULL,		/* set values hook */
		XtInheritSetValuesAlmost,	/* set values almost */
		NULL,		/* get values hook */
		NULL,		/* accept focus */
		XtVersion,	/* version */
		NULL,		/* callback private */
		defaultTranslationsAbacus,	/* tm table */
		NULL,		/* query geometry */
		NULL,		/* display accelerator */
		NULL		/* extension */
	},
	{
		0		/* ignore */
	}
};

WidgetClass abacusWidgetClass = (WidgetClass) & abacusClassRec;

#ifndef HAVE_USLEEP
#if !defined( VMS ) || defined( XVMSUTILS ) ||  ( __VMS_VER >= 70000000 )
#ifdef USE_XVMSUTILS
#include <X11/unix_time.h>
#endif
#if HAVE_SYS_TIME_H
#include <sys/time.h>
#else
#if HAVE_SYS_SELECT_H
#include <sys/select.h>
#endif
#endif
#endif
#if defined(SYSV) || defined(SVR4)
#ifdef LESS_THAN_AIX3_2
#include <sys/poll.h>
#else /* !LESS_THAN_AIX3_2 */
#include <poll.h>
#endif /* !LESS_THAN_AIX3_2 */
#endif /* defined(SYSV) || defined(SVR4) */

static int
usleep(unsigned int usec)
{
#if (defined (SYSV) || defined(SVR4)) && !defined(__hpux)
#if defined(HAVE_NANOSLEEP)
	{
		struct timespec rqt;

		rqt.tv_nsec = 1000 * (usec % (unsigned int) 1000000);
		rqt.tv_sec = usec / (unsigned int) 1000000;
		return nanosleep(&rqt, NULL);
	}
#else
	(void) poll((void *) 0, (int) 0, usec / 1000);	/* ms resolution */
#endif
#else
#ifdef VMS
	long        timadr[2];

	if (usec != 0) {
		timadr[0] = -usec * 10;
		timadr[1] = -1;

		sys$setimr(4, &timadr, 0, 0, 0);
		sys$waitfr(4);
	}
#else
	struct timeval time_out;

#if  0
	/* (!defined(AIXV3) && !defined(__hpux)) */
	extern int  select(int, fd_set *, fd_set *, fd_set *, struct timeval *);

#endif

	time_out.tv_usec = usec % (unsigned int) 1000000;
	time_out.tv_sec = usec / (unsigned int) 1000000;
	(void) select(0, (void *) 0, (void *) 0, (void *) 0, &time_out);
#endif
#endif
	return 0;
}
#endif

static void
Sleep(unsigned int cMilliseconds)
{
	(void) usleep(cMilliseconds * 1000);
}

static void
InitializeAbacus(Widget request, Widget renew)
{
	AbacusWidget w = (AbacusWidget) renew;
	int         deck;

	for (deck = 0; deck < MAXDECKS; deck++)
		w->abacus.decks[deck].position = NULL;
	w->abacus.digits = NULL;
	CheckBeads(w);
	ResetBeads(w);
	w->abacus.depth = DefaultDepthOfScreen(XtScreen(w));
	SetAllColors(w, True);
	ResizeAbacus(w);
}

static void
DestroyAbacus(Widget old)
{
	AbacusWidget w = (AbacusWidget) old;

	XtReleaseGC(old, w->abacus.beadGC);
	XtReleaseGC(old, w->abacus.borderGC);
	XtReleaseGC(old, w->abacus.railGC);
	XtReleaseGC(old, w->abacus.frameGC);
	XtReleaseGC(old, w->abacus.inverseGC);
	XtRemoveCallbacks(old, XtNselectCallback, w->abacus.select);
}

static void
ResizeAbacus(AbacusWidget w)
{
	int         height;

	w->abacus.delta.x = 8;
	w->abacus.delta.y = 2;
	w->abacus.pos.x = MAX(((int) w->core.width - w->abacus.delta.x) /
			      w->abacus.rails, w->abacus.delta.x);
	w->abacus.pos.y = MAX(((int) w->core.height - 2 * w->abacus.delta.y - 3) /
		  (w->abacus.decks[TOP].room + w->abacus.decks[BOTTOM].room),
			      w->abacus.delta.y);
	w->abacus.width = w->abacus.pos.x * w->abacus.rails +
		w->abacus.delta.x + 2;
	w->abacus.decks[TOP].height = w->abacus.pos.y *
		w->abacus.decks[TOP].room + w->abacus.delta.y + 2;
	w->abacus.decks[BOTTOM].height = w->abacus.pos.y *
		w->abacus.decks[BOTTOM].room + w->abacus.delta.y + 2;
	height = w->abacus.decks[TOP].height + w->abacus.decks[BOTTOM].height;
	w->abacus.offset.x = ((int) w->core.width - w->abacus.width + 2) / 2;
	w->abacus.offset.y = ((int) w->core.height - height + 2) / 2 + 1;
	w->abacus.beadSize.x = w->abacus.pos.x - w->abacus.delta.x;
	w->abacus.beadSize.y = w->abacus.pos.y - w->abacus.delta.y;
}

static void
ExposeAbacus(Widget renew, XEvent * event, Region region)
{
	AbacusWidget w = (AbacusWidget) renew;

	if (w->core.visible) {
		if (w->abacus.reverse)
			XFillRectangle(XtDisplay(w), XtWindow(w),
				       w->abacus.inverseGC, 0, 0, w->core.width, w->core.height);
		DrawFrame(w, w->abacus.frameGC, w->abacus.railGC);
		DrawAllBeads(w);
	}
}

static      Boolean
SetValuesAbacus(Widget current, Widget request, Widget renew)
{
	AbacusWidget c = (AbacusWidget) current, w = (AbacusWidget) renew;
	Boolean     redraw = False;
	Boolean     redrawBeads = False;

	CheckBeads(w);
	if (w->core.background_pixel != c->core.background_pixel ||
	    w->abacus.foreground != c->abacus.foreground ||
	    w->abacus.borderColor != c->abacus.borderColor ||
	    w->abacus.beadColor != c->abacus.beadColor ||
	    w->abacus.railColor != c->abacus.railColor ||
	    w->abacus.reverse != c->abacus.reverse ||
	    w->abacus.mono != c->abacus.mono) {
		SetAllColors(w, False);
		redrawBeads = True;
	}
	if (w->abacus.decks[BOTTOM].number !=
	    c->abacus.decks[BOTTOM].number ||
	    w->abacus.decks[TOP].number != c->abacus.decks[TOP].number ||
	    w->abacus.decks[BOTTOM].factor !=
	    c->abacus.decks[BOTTOM].factor ||
	    w->abacus.decks[TOP].factor != c->abacus.decks[TOP].factor ||
	    w->abacus.base != c->abacus.base) {
		ResetBeads(w);
		redraw = True;
	}
	if (w->abacus.decks[BOTTOM].orientation !=
	    c->abacus.decks[BOTTOM].orientation ||
	    w->abacus.decks[TOP].orientation !=
	    c->abacus.decks[TOP].orientation) {
		redraw = True;
	}
	if (w->abacus.spaces != c->abacus.spaces) {
		w->abacus.decks[TOP].room =
			w->abacus.decks[TOP].number + w->abacus.spaces;
		w->abacus.decks[BOTTOM].room =
			w->abacus.decks[BOTTOM].number + w->abacus.spaces;
		redraw = True;
	}
	if (w->abacus.pos.x != c->abacus.pos.x || w->abacus.pos.y != c->abacus.pos.y)
		redrawBeads = True;
	if (redrawBeads && !redraw && XtIsRealized(renew) && renew->core.visible) {
		DrawFrame(c, c->abacus.inverseGC, c->abacus.inverseGC);
		DrawFrame(w, w->abacus.frameGC, w->abacus.railGC);
		DrawAllBeads(w);
	}
	if (w->abacus.rails != c->abacus.rails) {
		ShiftedRails(c, w);
	}
	if (w->abacus.deck == ABACUS_CLEAR) {
		w->abacus.deck = ABACUS_IGNORE;
		if (!EmptyCounter(w)) {
			ClearAllBeads(w);
		}
	} else if (w->abacus.deck != ABACUS_IGNORE) {
		MoveBeadsByValue(w, w->abacus.deck,
		     w->abacus.rail + w->abacus.rails / 2, w->abacus.number);
		w->abacus.deck = ABACUS_IGNORE;
	}
	return (redraw);
}

static void
QuitAbacus(AbacusWidget w, XEvent * event, char **args, int nArgs)
{
	XtCloseDisplay(XtDisplay(w));
	exit(0);
}

static void
SelectAbacus(AbacusWidget w, XEvent * event, char **args, int nArgs)
{
	int         deck, rail, j;

	if (w->abacus.demo) {
		ClearAllBeads(w);
		if (w->abacus.demo) {
			abacusCallbackStruct cb;

			cb.reason = ABACUS_CLEAR;
			XtCallCallbacks((Widget) w, XtNselectCallback, &cb);
		}
		w->abacus.currentDeck = -1;
	} else if (PositionToBead(w, event->xbutton.x, event->xbutton.y, &deck, &rail, &j)) {
		w->abacus.currentDeck = deck;
		w->abacus.currentRail = rail;
		w->abacus.currentPosition = j;
		DrawBead(w, deck, rail, j, False, TRUE);
	} else
		w->abacus.currentDeck = -1;
}

static void
ReleaseAbacus(AbacusWidget w, XEvent * event, char **args, int nArgs)
{
	if (w->abacus.currentDeck < 0)
		return;
	DrawBead(w, w->abacus.currentDeck, w->abacus.currentRail, w->abacus.currentPosition, True, TRUE);
	DrawBead(w, w->abacus.currentDeck, w->abacus.currentRail, w->abacus.currentPosition, False, FALSE);
	MoveBeadsByPos(w, w->abacus.currentDeck, w->abacus.currentRail,
		       w->abacus.currentPosition);
	w->abacus.currentDeck = -1;
}

static void
ClearAbacus(AbacusWidget w, XEvent * event, char **args, int nArgs)
{
#if 0
	if (EmptyCounter(w))
		return;
		/* Should check if one really wants to destroy calculations. */
#endif
	ClearAllBeads(w);
	if (w->abacus.demo) {
		abacusCallbackStruct cb;

		cb.reason = ABACUS_CLEAR;
		XtCallCallbacks((Widget) w, XtNselectCallback, &cb);
	}
}

static void
IncrementAbacus(AbacusWidget w, XEvent * event, char **args, int nArgs)
{
	if (ShiftRails(w, 1)) {
		abacusCallbackStruct cb;

		cb.reason = ABACUS_INC;
		XtCallCallbacks((Widget) w, XtNselectCallback, &cb);
	}
}

static void
DecrementAbacus(AbacusWidget w, XEvent * event, char **args, int nArgs)
{
	if (ShiftRails(w, -1)) {
		abacusCallbackStruct cb;

		cb.reason = ABACUS_DEC;
		XtCallCallbacks((Widget) w, XtNselectCallback, &cb);
	}
}

static void
NextAbacus(AbacusWidget w, XEvent * event, char **args, int nArgs)
{
	if (w->abacus.demo) {
		abacusCallbackStruct cb;

		cb.reason = ABACUS_NEXT;
		XtCallCallbacks((Widget) w, XtNselectCallback, &cb);
	}
}

static void
RepeatAbacus(AbacusWidget w, XEvent * event, char **args, int nArgs)
{
	if (w->abacus.demo) {
		abacusCallbackStruct cb;

		cb.reason = ABACUS_REPEAT;
		XtCallCallbacks((Widget) w, XtNselectCallback, &cb);
	}
}

static void
MoreAbacus(AbacusWidget w, XEvent * event, char **args, int nArgs)
{
	if (w->abacus.demo) {
		abacusCallbackStruct cb;

		cb.reason = ABACUS_MORE;
		XtCallCallbacks((Widget) w, XtNselectCallback, &cb);
	}
}

static void
SetAllColors(AbacusWidget w, Boolean init)
{
	XGCValues   values;
	XtGCMask    valueMask;

	valueMask = GCForeground | GCBackground;
	if (w->abacus.reverse) {
		values.background = w->core.background_pixel;
		values.foreground = w->abacus.foreground;
	} else {
		values.foreground = w->core.background_pixel;
		values.background = w->abacus.foreground;
	}
	if (!init)
		XtReleaseGC((Widget) w, w->abacus.inverseGC);
	w->abacus.inverseGC = XtGetGC((Widget) w, valueMask, &values);
	if (w->abacus.reverse) {
		values.background = w->abacus.foreground;
		values.foreground = w->core.background_pixel;
	} else {
		values.foreground = w->abacus.foreground;
		values.background = w->core.background_pixel;
	}
	if (!init)
		XtReleaseGC((Widget) w, w->abacus.frameGC);
	w->abacus.frameGC = XtGetGC((Widget) w, valueMask, &values);
	if (w->abacus.depth < 2 || w->abacus.mono) {
		if (w->abacus.reverse) {
			values.background = w->abacus.foreground;
			values.foreground = w->core.background_pixel;
		} else {
			values.foreground = w->abacus.foreground;
			values.background = w->core.background_pixel;
		}
		if (!init) {
			XtReleaseGC((Widget) w, w->abacus.borderGC);
			XtReleaseGC((Widget) w, w->abacus.beadGC);
			XtReleaseGC((Widget) w, w->abacus.railGC);
		}
		w->abacus.borderGC = XtGetGC((Widget) w, valueMask, &values);
		w->abacus.beadGC = XtGetGC((Widget) w, valueMask, &values);
		w->abacus.railGC = XtGetGC((Widget) w, valueMask, &values);
	} else {
		values.background = w->core.background_pixel;
		if (!init) {
			XtReleaseGC((Widget) w, w->abacus.borderGC);
			XtReleaseGC((Widget) w, w->abacus.beadGC);
			XtReleaseGC((Widget) w, w->abacus.railGC);
		}
		values.foreground = w->abacus.borderColor;
		w->abacus.borderGC = XtGetGC((Widget) w, valueMask, &values);
		values.foreground = w->abacus.beadColor;
		w->abacus.beadGC = XtGetGC((Widget) w, valueMask, &values);
		values.foreground = w->abacus.railColor;
		w->abacus.railGC = XtGetGC((Widget) w, valueMask, &values);
	}
}

static void
ShiftedRails(AbacusWidget c, AbacusWidget w)
{
	int         shift = w->abacus.rails - c->abacus.rails;

	if (shift != 0 && w->abacus.rails >= MINRAILS &&
	    (!w->abacus.demo || w->abacus.rails >= MINDEMORAILS)) {

		int         deck, rail, local_rails = 0;
		DeckPart    local_decks[MAXDECKS];

		/* Alloc space to save the rails */
		local_rails = c->abacus.rails;
		for (deck = BOTTOM; deck <= TOP; deck++) {
			if (!(local_decks[deck].position = (int *)
			      malloc(sizeof (int) * local_rails)))
				            XtError("Not enough memory, exiting.");

			for (rail = 0; rail < local_rails; rail++) {
				local_decks[deck].position[rail] =
					c->abacus.decks[deck].position[rail];
			}
		}

		ResetBeads(w);
		ResizeAbacus(w);

		XFillRectangle(XtDisplay(w), XtWindow(w),
		   w->abacus.inverseGC, 0, 0, w->core.width, w->core.height);
		DrawFrame(w, w->abacus.frameGC, w->abacus.railGC);
		DrawAllBeads(w);

		for (deck = BOTTOM; deck <= TOP; deck++) {
			for (rail = 0; rail < local_rails; rail++) {
				int         newRail = rail - local_rails / 2 + w->abacus.rails / 2;

				if ((newRail >= 0) &&
				    (newRail < w->abacus.rails)) {
					if (w->abacus.decks[deck].orientation)
						MoveBeadsByValue(w, deck,
								 newRail,
								 w->abacus.decks[deck].number - local_decks[deck].position[rail]);
					else
						MoveBeadsByValue(w, deck,
								 newRail,
								 local_decks[deck].position[rail]);
				}
			}
		}
		for (deck = BOTTOM; deck <= TOP; deck++) {
			if (local_decks[deck].position)
				(void) free((void *) local_decks[deck].position);
		}
	}
}

static      Boolean
ShiftRails(AbacusWidget w, int shift)
{
	if (shift != 0 && w->abacus.rails + shift >= MINRAILS &&
	    (!w->abacus.demo || w->abacus.rails + shift >= MINDEMORAILS)) {

		int         deck, rail, local_rails = 0;
		DeckPart    local_decks[MAXDECKS];

		/* Alloc space to save the rails */
		local_rails = w->abacus.rails;
		for (deck = BOTTOM; deck <= TOP; deck++) {
			if (!(local_decks[deck].position = (int *)
			      malloc(sizeof (int) * local_rails)))
				            XtError("Not enough memory, exiting.");

			for (rail = 0; rail < local_rails; rail++) {
				local_decks[deck].position[rail] =
					w->abacus.decks[deck].position[rail];
			}
		}

		w->abacus.rails += shift;
		ResetBeads(w);
		ResizeAbacus(w);

		XFillRectangle(XtDisplay(w), XtWindow(w),
		   w->abacus.inverseGC, 0, 0, w->core.width, w->core.height);
		DrawFrame(w, w->abacus.frameGC, w->abacus.railGC);
		DrawAllBeads(w);

		for (deck = BOTTOM; deck <= TOP; deck++) {
			for (rail = 0; rail < local_rails; rail++) {
				int         newRail = rail - local_rails / 2 + w->abacus.rails / 2;

				if ((newRail >= 0) &&
				    (newRail < w->abacus.rails)) {
					if (w->abacus.decks[deck].orientation)
						MoveBeadsByValue(w, deck,
								 newRail,
								 w->abacus.decks[deck].number - local_decks[deck].position[rail]);
					else
						MoveBeadsByValue(w, deck,
								 newRail,
								 local_decks[deck].position[rail]);
				}
			}
		}
		for (deck = BOTTOM; deck <= TOP; deck++) {
			if (local_decks[deck].position)
				(void) free((void *) local_decks[deck].position);
		}
		return True;
	}
	return False;
}

static int
PositionToBead(AbacusWidget w, int x, int y, int *deck, int *rail, int *j)
{
	x -= w->abacus.offset.x;
	y -= w->abacus.offset.y;
	if (y > w->abacus.decks[TOP].height) {
		y = y - w->abacus.decks[TOP].height;
		*deck = BOTTOM;
	} else
		*deck = TOP;
	*rail = w->abacus.rails - 1 - (x - w->abacus.delta.x / 2) / w->abacus.pos.x;
	*j = (y - w->abacus.delta.y / 2) / w->abacus.pos.y + 1;
	if (*rail < 0)
		*rail = 0;
	else if (*rail >= w->abacus.rails)
		*rail = w->abacus.rails - 1;
	if (*j < 1)
		*j = 1;
	else if (*j > w->abacus.decks[*deck].room)
		*j = w->abacus.decks[*deck].room;
	return ((*j > w->abacus.decks[*deck].position[*rail] + w->abacus.spaces) ||
		(*j <= w->abacus.decks[*deck].position[*rail]));
}

static void
CheckBeads(AbacusWidget w)
{
	char        buf[121];

	if (w->abacus.demo) {	/* Trying to keep these at a minimum... */
		if (w->abacus.base != 10) {
			w->abacus.base = 10;
			XtWarning("Base must be equal to 10 for demo");
		}
		if (w->abacus.decks[BOTTOM].number < 5) {
			w->abacus.decks[BOTTOM].number = 5;
			XtWarning("Number of beads on bottom deck must be greater than 4, for demo");
		}
		if (w->abacus.decks[TOP].number < 2) {
			w->abacus.decks[TOP].number = 2;
			XtWarning("Number of beads on top deck must be greater than 1, for demo");
		}
		if (w->abacus.decks[BOTTOM].factor != 1) {
			w->abacus.decks[BOTTOM].factor = 1;
			XtWarning("Factor for bottom deck must be equal to 1, for demo");
		}
		if (w->abacus.decks[TOP].factor != 5) {
			w->abacus.decks[TOP].factor = 5;
			XtWarning("Factor for top deck must be equal to 5, for demo");
		}
		if (w->abacus.rails < MINDEMORAILS) {
			(void) sprintf(buf,
				       "Number of Rails out of bounds, use %d..MAXINT, for demo",
				       MINDEMORAILS);
			XtWarning(buf);
			w->abacus.rails = MINDEMORAILS;
		}
#if 0
		/* Increment and decrement will get goofed up, if enabled. */
		if (!(w->abacus.rails & 1)) {
			w->abacus.rails++;
			XtWarning("Number of rails must be odd, for demo");
		}
#endif
	} else {
		if (w->abacus.base > 36) {
			/* 10 numbers + 26 letters (ASCII or EBCDIC) */
			XtWarning("Base must be less than or equal to 36");
			w->abacus.base = 10;
		} else if (w->abacus.base <= 1) {
			XtWarning("Base must be greater than 1");	/* Base 1 is rediculous :) */
			w->abacus.base = 10;
		}
		if (w->abacus.rails < MINRAILS) {
			(void) sprintf(buf,
			     "Number of Rails out of bounds, use %d..MAXINT",
				       MINRAILS);
			XtWarning(buf);
			w->abacus.rails = MINRAILS;
		}
		if (w->abacus.decks[TOP].factor < 1 ||
		    w->abacus.decks[TOP].factor > w->abacus.base) {
			(void) sprintf(buf,
			      "Factor of Top Beads out of bounds, use 1..%d",
				       w->abacus.base);
			XtWarning(buf);
			w->abacus.decks[TOP].factor = 5;
		}
		if (w->abacus.decks[BOTTOM].factor < 1 ||
		    w->abacus.decks[BOTTOM].factor > w->abacus.base) {
			(void) sprintf(buf,
			   "Factor of Bottom Beads out of bounds, use 1..%d",
				       w->abacus.base);
			XtWarning(buf);
			w->abacus.decks[BOTTOM].factor = 1;
		}
	}
	if (w->abacus.decks[TOP].number < 1 ||
	    w->abacus.decks[TOP].number > w->abacus.base) {
		(void) sprintf(buf,
			       "Number of Top Beads out of bounds, use 1..%d",
			       w->abacus.base);
		XtWarning(buf);
		w->abacus.decks[TOP].number = 2;
	}
	if (w->abacus.decks[BOTTOM].number < 1 ||
	    w->abacus.decks[BOTTOM].number > w->abacus.base) {
		(void) sprintf(buf,
			   "Number of Bottom Beads out of bounds, use 1..%d",
			       w->abacus.base);
		XtWarning(buf);
		w->abacus.decks[BOTTOM].number = 5;
	}
	if (w->abacus.spaces < 1) {
		(void) sprintf(buf, "Number of Spaces out of bounds, use 1..MAXINT");
		XtWarning(buf);
		w->abacus.spaces = 2;
	}
	if (w->abacus.delay < 0) {
		(void) sprintf(buf, "Delay out of bounds, use 0..MAXINT");
		XtWarning(buf);
		w->abacus.delay = -w->abacus.delay;
	}
}

static void
ResetBeads(AbacusWidget w)
{
	int         deck, rail;

	w->abacus.currentDeck = -1;
	w->abacus.numDigits = w->abacus.rails + CARRY + 1;
	for (deck = BOTTOM; deck <= TOP; deck++) {
		if (w->abacus.decks[deck].position)
			(void) free((void *) w->abacus.decks[deck].position);
		if (!(w->abacus.decks[deck].position = (int *)
		      malloc(sizeof (int) * w->abacus.rails)))
			            XtError("Not enough memory, exiting.");
	}
	if (w->abacus.digits)
		(void) free((void *) w->abacus.digits);
	if (!(w->abacus.digits = (char *)
	      malloc(sizeof (char) * w->abacus.numDigits)))
		            XtError("Not enough memory, exiting.");

	w->abacus.decks[TOP].room =
		w->abacus.decks[TOP].number + w->abacus.spaces;
	w->abacus.decks[BOTTOM].room =
		w->abacus.decks[BOTTOM].number + w->abacus.spaces;
	for (rail = 0; rail < w->abacus.rails; rail++)
		for (deck = BOTTOM; deck <= TOP; deck++)
			w->abacus.decks[deck].position[rail] =
				(w->abacus.decks[deck].orientation) ? w->abacus.decks[deck].number : 0;
	for (rail = 0; rail < w->abacus.numDigits - 1; rail++)
		w->abacus.digits[rail] = '0';
	w->abacus.digits[w->abacus.numDigits - 1] = '\0';
}

static void
ClearAllBeads(AbacusWidget w)
{
	int         deck, rail;

	for (rail = 0; rail < w->abacus.rails; rail++) {
		for (deck = DOWN; deck <= UP; deck++) {
			if (w->abacus.decks[deck].orientation)
				MoveBeadsUp(w, deck, rail, w->abacus.decks[deck].room);
			else	/* w->abacus.decks[deck].orientation == DOWN */
				MoveBeadsDown(w, deck, rail, 1);
		}
	}
}

static void
MoveBeadsByPos(AbacusWidget w, int deck, int rail, int pos)
{
	if (pos <= w->abacus.decks[deck].position[rail])
		MoveBeadsDown(w, deck, rail, pos);
	else
		MoveBeadsUp(w, deck, rail, pos);
}

static void
MoveBeadsUp(AbacusWidget w, int deck, int rail, int j)
{
	int         k, l;

	if (j > w->abacus.decks[deck].position[rail] + w->abacus.spaces) {
		for (l = 0; l < w->abacus.spaces; l++) {
			for (k = w->abacus.decks[deck].position[rail] + w->abacus.spaces + 1; k <= j; k++) {
				DrawBead(w, deck, rail, k - l, True, FALSE);
				DrawBead(w, deck, rail, k - l - 1, False, FALSE);
			}
			if (l + 1 != w->abacus.spaces) {
				XFlush(XtDisplay(w));
				Sleep((unsigned int) w->abacus.delay);
			}
		}
		if (w->abacus.decks[deck].orientation) {
			SubBead(w, w->abacus.decks[deck].factor *
				(j - w->abacus.spaces - w->abacus.decks[deck].position[rail]), rail);
			SetCounter(w, deck, rail, w->abacus.decks[deck].factor *
				   -(j - w->abacus.spaces - w->abacus.decks[deck].position[rail]));
		} else {	/* w->abacus.decks[deck].orientation == DOWN */
			AddBead(w, w->abacus.decks[deck].factor *
				(j - w->abacus.spaces - w->abacus.decks[deck].position[rail]), rail);
			SetCounter(w, deck, rail, w->abacus.decks[deck].factor *
				   (j - w->abacus.spaces - w->abacus.decks[deck].position[rail]));
		}
		w->abacus.decks[deck].position[rail] = j - w->abacus.spaces;
	}
}

static void
MoveBeadsDown(AbacusWidget w, int deck, int rail, int j)
{
	int         k, l;

	if (j <= w->abacus.decks[deck].position[rail]) {
		for (l = 0; l < w->abacus.spaces; l++) {
			for (k = w->abacus.decks[deck].position[rail]; k >= j; k--) {
				DrawBead(w, deck, rail, k + l, True, FALSE);
				DrawBead(w, deck, rail, k + l + 1, False, FALSE);
			}
			if (l + 1 != w->abacus.spaces) {
				XFlush(XtDisplay(w));
				Sleep((unsigned int) w->abacus.delay);
			}
		}
		if (w->abacus.decks[deck].orientation) {
			AddBead(w, w->abacus.decks[deck].factor *
			(w->abacus.decks[deck].position[rail] - j + 1), rail);
			SetCounter(w, deck, rail, w->abacus.decks[deck].factor *
			     (w->abacus.decks[deck].position[rail] - j + 1));
		} else {	/* w->abacus.decks[deck].orientation == DOWN */
			SubBead(w, w->abacus.decks[deck].factor *
			(w->abacus.decks[deck].position[rail] - j + 1), rail);
			SetCounter(w, deck, rail, w->abacus.decks[deck].factor *
			    -(w->abacus.decks[deck].position[rail] - j + 1));
		}
		w->abacus.decks[deck].position[rail] = j - 1;
	}
}

static void
DrawFrame(AbacusWidget w, GC frameGC, GC railGC)
{
	int         deck, rail, dx, dy, x, y, yOffset;

	x = w->abacus.rails * w->abacus.pos.x + w->abacus.delta.x - 1;
	/* Left */
	XFillRectangle(XtDisplay(w), XtWindow(w), frameGC,
		       0, 0, w->abacus.offset.x + 1, w->core.height);
	/* Right */
	XFillRectangle(XtDisplay(w), XtWindow(w), frameGC,
		       x + w->abacus.offset.x, 0,
		   w->core.width - (x + w->abacus.offset.x), w->core.height);
	for (deck = UP; deck >= DOWN; deck--) {
		dx = w->abacus.beadSize.x / 2 + w->abacus.delta.x + w->abacus.offset.x;
		yOffset = (deck == UP) ? 0 : w->abacus.decks[TOP].height;
		y = w->abacus.decks[deck].room * w->abacus.pos.y + w->abacus.delta.y - 1;
		dy = w->abacus.delta.y / 2 + yOffset + w->abacus.offset.y - 1;
		if (deck == UP) {
			/* Top */
			XFillRectangle(XtDisplay(w), XtWindow(w), frameGC,
				       w->abacus.offset.x + 1, yOffset - 1,
				       x - 1, w->abacus.offset.y + 1);
			/* Middle */
			XFillRectangle(XtDisplay(w), XtWindow(w), frameGC,
				       w->abacus.offset.x + 1, y + yOffset + w->abacus.offset.y,
				       x - 1, 3);
		} else {
			/* Bottom */
			XFillRectangle(XtDisplay(w), XtWindow(w), frameGC,
				       w->abacus.offset.x + 1, y + yOffset + w->abacus.offset.y,
				       x - 1, w->core.height - (y + yOffset + w->abacus.offset.y));
		}
		for (rail = 0; rail < w->abacus.rails; rail++) {
			XFillRectangle(XtDisplay(w), XtWindow(w), railGC, dx, dy, 1, y);
			dx += w->abacus.pos.x;
		}
	}
}

static void
DrawAllBeads(AbacusWidget w)
{
	int         deck, rail, j;

	for (rail = 0; rail < w->abacus.rails; rail++) {
		for (deck = DOWN; deck <= UP; deck++) {
			for (j = 1; j <= w->abacus.decks[deck].position[rail]; j++)
				DrawBead(w, deck, rail, j, False, FALSE);
			for (j = w->abacus.spaces + w->abacus.decks[deck].position[rail] + 1;
			     j <= w->abacus.decks[deck].room; j++)
				DrawBead(w, deck, rail, j, False, FALSE);
		}
	}
	SetCounter(w, 0, w->abacus.rails / 2, 0);
}

static void
XFillCircle(Display * display, Window window, GC gc, int diameter, int ctrX, int ctrY)
{
	if (diameter > 0)
		XFillArc(display, window, gc, ctrX - diameter / 2, ctrY - diameter / 2,
			 diameter, diameter, 0, CIRCLE);
}

static void
XDrawCircle(Display * display, Window window, GC gc, int diameter, int ctrX, int ctrY)
{
	if (diameter > 0)
		XDrawArc(display, window, gc, ctrX - diameter / 2, ctrY - diameter / 2,
			 diameter, diameter, 0, CIRCLE);
}

static void
DrawBead(AbacusWidget w, int deck, int rail, int j, Boolean erase, int offset)
{
	int         dx, dy, yOffset;
	GC          beadGC, borderGC;

	if (erase) {
		beadGC = w->abacus.inverseGC;
		borderGC = w->abacus.inverseGC;
	} else if (offset) {
		beadGC = w->abacus.borderGC;
		borderGC = w->abacus.beadGC;
	} else {
		beadGC = w->abacus.beadGC;
		borderGC = w->abacus.borderGC;
	}
	yOffset = (deck == UP) ? 0 : w->abacus.decks[TOP].height;
	dx = (w->abacus.rails - rail - 1) * w->abacus.pos.x + w->abacus.delta.x +
		w->abacus.offset.x;
	dy = (j - 1) * w->abacus.pos.y + w->abacus.delta.y + yOffset +
		w->abacus.offset.y - 1;
	if (!erase) {
		dx += offset;
		dy += offset;
		if (w->abacus.beadSize.x > w->abacus.beadSize.y) {
			XDrawCircle(XtDisplay(w), XtWindow(w), borderGC, w->abacus.beadSize.y - 1,
				    dx + w->abacus.beadSize.x / 2 -
			   (w->abacus.beadSize.x - w->abacus.beadSize.y) / 2,
				    dy + (w->abacus.beadSize.y - 1) / 2);
			XDrawCircle(XtDisplay(w), XtWindow(w), borderGC, w->abacus.beadSize.y - 1,
				    dx + w->abacus.beadSize.x / 2 +
			   (w->abacus.beadSize.x - w->abacus.beadSize.y) / 2,
				    dy + (w->abacus.beadSize.y - 1) / 2);
			XDrawRectangle(XtDisplay(w), XtWindow(w), borderGC,
				       dx + w->abacus.beadSize.x / 2 -
			(w->abacus.beadSize.x - w->abacus.beadSize.y) / 2, dy,
				       w->abacus.beadSize.x - w->abacus.beadSize.y, w->abacus.beadSize.y);
			XFillCircle(XtDisplay(w), XtWindow(w), beadGC, w->abacus.beadSize.y - 1,
				    dx + w->abacus.beadSize.x / 2 -
			   (w->abacus.beadSize.x - w->abacus.beadSize.y) / 2,
				    dy + (w->abacus.beadSize.y - 1) / 2);
			XFillCircle(XtDisplay(w), XtWindow(w), beadGC, w->abacus.beadSize.y - 1,
				    dx + w->abacus.beadSize.x / 2 +
			   (w->abacus.beadSize.x - w->abacus.beadSize.y) / 2,
				    dy + (w->abacus.beadSize.y - 1) / 2);
			XFillRectangle(XtDisplay(w), XtWindow(w), beadGC,
				       dx + w->abacus.beadSize.x / 2 -
			(w->abacus.beadSize.x - w->abacus.beadSize.y) / 2, dy,
				       w->abacus.beadSize.x - w->abacus.beadSize.y, w->abacus.beadSize.y);
		} else if (w->abacus.beadSize.x < w->abacus.beadSize.y) {
			XDrawCircle(XtDisplay(w), XtWindow(w), borderGC, w->abacus.beadSize.x - 1,
				    dx + (w->abacus.beadSize.x - 1) / 2,
				    dy + w->abacus.beadSize.y / 2 -
			  (w->abacus.beadSize.y - w->abacus.beadSize.x) / 2);
			XDrawCircle(XtDisplay(w), XtWindow(w), borderGC, w->abacus.beadSize.x - 1,
				    dx + (w->abacus.beadSize.x - 1) / 2,
				    dy + w->abacus.beadSize.y / 2 +
			  (w->abacus.beadSize.y - w->abacus.beadSize.x) / 2);
			XDrawRectangle(XtDisplay(w), XtWindow(w), borderGC,
				       dx, dy + w->abacus.beadSize.y / 2 -
			   (w->abacus.beadSize.y - w->abacus.beadSize.x) / 2,
				       w->abacus.beadSize.x, w->abacus.beadSize.y - w->abacus.beadSize.x);
			XFillCircle(XtDisplay(w), XtWindow(w), beadGC, w->abacus.beadSize.x - 1,
				    dx + (w->abacus.beadSize.x - 1) / 2,
				    dy + w->abacus.beadSize.y / 2 -
			  (w->abacus.beadSize.y - w->abacus.beadSize.x) / 2);
			XFillCircle(XtDisplay(w), XtWindow(w), beadGC, w->abacus.beadSize.x - 1,
				    dx + (w->abacus.beadSize.x - 1) / 2,
				    dy + w->abacus.beadSize.y / 2 +
			  (w->abacus.beadSize.y - w->abacus.beadSize.x) / 2);
			XFillRectangle(XtDisplay(w), XtWindow(w), beadGC,
				       dx, dy + w->abacus.beadSize.y / 2 -
			   (w->abacus.beadSize.y - w->abacus.beadSize.x) / 2,
				       w->abacus.beadSize.x, w->abacus.beadSize.y - w->abacus.beadSize.x);
		} else {
			XFillCircle(XtDisplay(w), XtWindow(w), beadGC, w->abacus.beadSize.y - 1,
				    dx + (w->abacus.beadSize.x - 1) / 2,
				    dy + (w->abacus.beadSize.y - 1) / 2);
			XDrawCircle(XtDisplay(w), XtWindow(w), borderGC, w->abacus.beadSize.y - 1,
				    dx + (w->abacus.beadSize.x - 1) / 2,
				    dy + (w->abacus.beadSize.y - 1) / 2);
		}
	} else {
		XFillRectangle(XtDisplay(w), XtWindow(w), beadGC,
			       dx + offset, dy + offset, w->abacus.beadSize.x + 1, w->abacus.beadSize.y + 1);
		XFillRectangle(XtDisplay(w), XtWindow(w), w->abacus.railGC,
			       dx + w->abacus.beadSize.x / 2, dy, 1, w->abacus.beadSize.y + 2);
	}
}

static void
AddBead(AbacusWidget w, int d, int p)
{
	int         position = w->abacus.numDigits - 2 - p;
	int         digit = w->abacus.digits[position] - '0';

	if (digit > 9 || digit < 0) {	/* ASCII or EBCDIC */
		digit -= ('A' - '9' - 1);
		if (digit > 18)	/* > I */
			digit -= ('J' - 'I' - 1);
		if (digit > 27)	/* > R */
			digit -= ('S' - 'R' - 1);
	}
	digit += d;

	w->abacus.digits[position] = (digit % w->abacus.base) + '0';
	if (w->abacus.digits[position] > '9') {		/* ASCII */
		w->abacus.digits[position] += ('A' - '9' - 1);
	} else if (w->abacus.digits[position] < '0') {	/* EBCDIC */
		w->abacus.digits[position] += ('A' - '9' - 1);
		if (w->abacus.digits[position] > 'I')
			w->abacus.digits[position] += ('J' - 'I' - 1);
		if (w->abacus.digits[position] > 'R')
			w->abacus.digits[position] += ('S' - 'R' - 1);
	}
	if (digit >= w->abacus.base)
		AddBead(w, digit / w->abacus.base, p + 1);
}

static void
SubBead(AbacusWidget w, int d, int p)
{
	int         position = w->abacus.numDigits - 2 - p;
	int         digit = w->abacus.digits[position] - '0';

	if (digit > 9 || digit < 0) {	/* ASCII or EBCDIC */
		digit -= ('A' - '9' - 1);
		if (digit > 18)	/* > I */
			digit -= ('J' - 'I' - 1);
		if (digit > 27)	/* > R */
			digit -= ('S' - 'R' - 1);
	}
	digit -= d;

	w->abacus.digits[position] =
		((digit + w->abacus.base) % w->abacus.base) + '0';
	if (w->abacus.digits[position] > '9') {		/* ASCII */
		w->abacus.digits[position] += ('A' - '9' - 1);
	} else if (w->abacus.digits[position] < '0') {	/* EBCDIC */
		w->abacus.digits[position] += ('A' - '9' - 1);
		if (w->abacus.digits[position] > 'I')
			w->abacus.digits[position] += ('J' - 'I' - 1);
		if (w->abacus.digits[position] > 'R')
			w->abacus.digits[position] += ('S' - 'R' - 1);
	}
	if (digit < 0)
		SubBead(w, 1 + (-1 - digit) / w->abacus.base, p + 1);
}

static      Boolean
EmptyCounter(AbacusWidget w)
{
	int         n = 0;

	while (n < w->abacus.numDigits - 2 && w->abacus.digits[n] == '0')
		n++;
	return (n == w->abacus.numDigits - 2 && w->abacus.digits[n] == '0');
}

static void
SetCounter(AbacusWidget w, int deck, int rail, int number)
{
	abacusCallbackStruct cb;
	int         n = 0;

#ifdef NO_POINT
	while (n < w->abacus.numDigits - 2 && w->abacus.digits[n] == '0')
		n++;
	if (!(cb.buffer = (char *)
	      malloc(sizeof (char) * (w->abacus.numDigits - n + 1))))
		            XtError("Not enough memory, exiting.");

#ifdef DEBUG
	(void) sprintf(cb.buffer, "%s." & (w->abacus.digits[n]));
#endif
	(void) strcpy(cb.buffer, &(w->abacus.digits[n]));
#else
	int         m = 0, half;

	while (n < w->abacus.numDigits - CARRY - w->abacus.rails / 2 &&
	       w->abacus.digits[n] == '0')
		n++;
	while (m < w->abacus.rails / 2 - 1 &&
	       w->abacus.digits[w->abacus.numDigits - CARRY - m] == '0')
		m++;
	half = w->abacus.numDigits - CARRY - w->abacus.rails / 2 - n + 1;
	if (!(cb.buffer = (char *)
	      malloc(sizeof (char) * (half + w->abacus.rails / 2 - m + 3))))
		            XtError("Not enough memory, exiting.");

	(void) strncpy(cb.buffer, &(w->abacus.digits[n]), half);
	cb.buffer[half] = '.';

	(void) strncpy(&(cb.buffer[half + 1]), &(w->abacus.digits[n + half]),
		       w->abacus.rails / 2 - m + 1);
	cb.buffer[half + w->abacus.rails / 2 - m + 1] = '\0';
#endif

	if (w->abacus.script && number != 0) {
		cb.reason = ABACUS_SCRIPT;
		cb.deck = deck;
		cb.rail = rail - w->abacus.rails / 2;
		cb.number = number;
	} else
		cb.reason = ABACUS_IGNORE;
	XtCallCallbacks((Widget) w, XtNselectCallback, &cb);
	(void) free((void *) cb.buffer);
}

static void
MoveBeadsByValue(AbacusWidget w, int deck, int rail, int number)
{
	if ((w->abacus.decks[deck].orientation && number < 0) ||
	    (!w->abacus.decks[deck].orientation && number > 0)) {
		MoveBeadsUp(w, deck, rail,
			    w->abacus.spaces + w->abacus.decks[deck].position[rail] + abs(number));
	} else if ((!w->abacus.decks[deck].orientation && number < 0) ||
		   (w->abacus.decks[deck].orientation && number > 0)) {
		MoveBeadsDown(w, deck, rail,
		     w->abacus.decks[deck].position[rail] + 1 - abs(number));
	}
}

#ifdef OLDANDINTHEWAY
/* Not used since this restricts the size of abacus to long int. */
static long int
power(int x, int n)
{				/* raise x to the nth power n >= 0 */
	int         i;
	long int    p = 1;

	for (i = 1; i <= n; ++i)
		p = p * x;
	return p;
}

/* This routine will do a XFillArc for a circle */
DiskXI(Display * display, Window window, GC gc,
       int diameter, int ctrX, int ctrY)
{
	int         x, y, p, d;

	x = 0;
	y = diameter / 2;
	p = diameter % 2;
	d = 1 - 2 * y + p;
	while (x < y) {
		DiskPointsXI(display, window, gc, ctrX, ctrY, x, y, p);
		if (d < 0)
			d = d + (4 * x) + 6;
		else {
			d = d + (4 * (x - y)) + 10;
			y--;
		}
		x++;
	}
	if (x == y)
		DiskPointsXI(display, window, gc, ctrX, ctrY, x, y, p);
}				/* DiskXI */

DiskPointsXI(Display * display, Window window, GC gc,
	     int ctrX, int ctrY, int x, int y, int p)
{
	XFillRectangle(display, window, gc,
		       ctrX - x, ctrY - y, 2 * x + p + 1, 1);
	XFillRectangle(display, window, gc,
		       ctrX - x, ctrY + y + p, 2 * x + p + 1, 1);
	XFillRectangle(display, window, gc,
		       ctrX - y, ctrY - x, 2 * y + p + 1, 1);
	XFillRectangle(display, window, gc,
		       ctrX - y, ctrY + x + p, 2 * y + p + 1, 1);
	/*XDrawLine(display, window, gc, ctrX - x, ctrY - y, ctrX + x + p, ctrY - y);
	   XDrawLine(display, window, gc,
	   ctrX - x, ctrY + y + p, ctrX + x + p, ctrY + y + p);
	   XDrawLine(display, window, gc, ctrX - y, ctrY - x, ctrX + y + p, ctrY - x);
	   XDrawLine(display, window, gc,
	   ctrX - y, ctrY + x + p, ctrX + y + p, ctrY + x + p); */
}				/* DiskPointsXI */

#endif
