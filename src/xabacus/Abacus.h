/*-
# X-BASED ABACUS
#
#  Abacus.h
#
###
#
#  Copyright (c) 1994 - 99	David Albert Bagley, bagleyd@tux.org
#
#  Abacus demo and neat pointers from
#  Copyright (c) 1991 - 98  Luis Fernandes, elf@ee.ryerson.ca
#
#                   All Rights Reserved
#
#  Permission to use, copy, modify, and distribute this software and
#  its documentation for any purpose and without fee is hereby granted,
#  provided that the above copyright notice appear in all copies and
#  that both that copyright notice and this permission notice appear in
#  supporting documentation, and that the name of the author not be
#  used in advertising or publicity pertaining to distribution of the
#  software without specific, written prior permission.
#
#  This program is distributed in the hope that it will be "useful",
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
*/

/* Public header file for Abacus */

#ifndef _XtAbacus_h
#define _XtAbacus_h

/***********************************************************************
 *
 * Abacus Widget
 *
 ***********************************************************************/

#define XtNselectCallback "selectCallback"
#define XtNrails "rails"
#define XtNspaces "spaces"
#define XtNbase "base"
#define XtNtopNumber "topNumber"
#define XtNtopOrient "topOrient"
#define XtNtopFactor "topFactor"
#define XtNbottomNumber "bottomNumber"
#define XtNbottomOrient "bottomOrient"
#define XtNbottomFactor "bottomFactor"
#define XtNrailColor "railColor"
#define XtNbeadColor "beadColor"
#define XtNbeadBorder "beadBorder"
#define XtNmono "mono"
#define XtNreverse "reverse"
#define XtNdelay "delay"
#define XtNbuffer "buffer"
#define XtNscript "script"
#define XtNdemo "demo"
#define XtNdemoPath "demoPath"
#define XtNdemoFont "demoFont"
#define XtNdemoForeground "demoForeground"
#define XtNdemoBackground "demoBackground"
#define XtNdeck "deck"
#define XtNrail "rail"
#define XtNnumber "number"
#define XtNframed "framed"
#define XtCRails "Rails"
#define XtCSpaces "Spaces"
#define XtCBase "Base"
#define XtCTopNumber "TopNumber"
#define XtCTopFactor "TopFactor"
#define XtCTopOrient "TopOrient"
#define XtCBottomNumber "BottomNumber"
#define XtCBottomFactor "BottomFactor"
#define XtCBottomOrient "BottomOrient"
#define XtCRailColor "RailColor"
#define XtCBeadColor "BeadColor"
#define XtCBeadBorder "BeadBorder"
#define XtCMono "Mono"
#define XtCReverse "Reverse"
#define XtCDelay "Delay"
#define XtCBuffer "Buffer"
#define XtCScript "Script"
#define XtCDemo "Demo"
#define XtCDemoPath "DemoPath"
#define XtCDemoFont "DemoFont"
#define XtCDemoForeground "DemoForeground"
#define XtCDemoBackground "DemoBackground"
#define XtCDeck "Deck"
#define XtCRail "Rail"
#define XtCNumber "Number"
#define XtCFramed "Framed"

#define ABACUS_MORE (-6)
#define ABACUS_REPEAT (-5)
#define ABACUS_NEXT (-4)
#define ABACUS_CLEAR (-3)
#define ABACUS_SCRIPT (-2)
#define ABACUS_IGNORE (-1)
#define ABACUS_MOVE 0
#define ABACUS_DEC 1
#define ABACUS_INC 2

typedef struct _AbacusClassRec *AbacusWidgetClass;
typedef struct _AbacusDemoClassRec *AbacusDemoWidgetClass;
typedef struct _AbacusRec *AbacusWidget;
typedef struct _AbacusRec *AbacusDemoWidget;

extern WidgetClass abacusWidgetClass;
extern WidgetClass abacusDemoWidgetClass;

typedef struct {
	XEvent     *event;
	int         reason;
	char       *buffer;
	int         deck, rail, number;
} abacusCallbackStruct;

#endif /* _XtAbacus_h */
/* DON'T ADD STUFF AFTER THIS #endif */
