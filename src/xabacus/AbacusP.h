/*-
# X-BASED ABACUS
#
#  AbacusP.h
#
###
#
#  Copyright (c) 1994 - 99	David Albert Bagley, bagleyd@tux.org
#
#  Abacus demo and neat pointers from
#  Copyright (c) 1991 - 98  Luis Fernandes, elf@ee.ryerson.ca
#
#                   All Rights Reserved
#
#  Permission to use, copy, modify, and distribute this software and
#  its documentation for any purpose and without fee is hereby granted,
#  provided that the above copyright notice appear in all copies and
#  that both that copyright notice and this permission notice appear in
#  supporting documentation, and that the name of the author not be
#  used in advertising or publicity pertaining to distribution of the
#  software without specific, written prior permission.
#
#  This program is distributed in the hope that it will be "useful",
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
*/

/* Private header file for Abacus */

#ifndef _AbacusP_h
#define _AbacusP_h

#include "Abacus.h"

#ifndef DEMOPATH
#define DEMOPATH "/usr/local/lib"
#endif

#define MINRAILS 1
#define MINDEMORAILS 3
#define MAXDECKS 2
#define UP 1
#define DOWN 0
#define TOP 1
#define BOTTOM 0
#define CARRY 2			/* I guess it could be even more if you have a weird config
				   also need a space to hold null (not included) */

#define NUM_DEGREES 360
#define MULT 64
#define CIRCLE (NUM_DEGREES*MULT)

#define ABS(a) (((a)<0)?(-a):(a))
#define SIGN(a) (((a)<0)?-1:1)
#define MIN(a,b) (((int)(a)<(int)(b))?(int)(a):(int)(b))
#define MAX(a,b) (((int)(a)>(int)(b))?(int)(a):(int)(b))

typedef struct _DeckPart {
	int         number;
	Boolean     orientation;
	int         factor;
	int        *position;
	int         room;	/* spaces + number */
	Position    height;
} DeckPart;

typedef struct _AbacusPart {
	Pixel       foreground;
	Pixel       borderColor, beadColor, railColor;
	DeckPart    decks[MAXDECKS];
	int         currentDeck, currentRail, currentPosition;
	int         rails;	/* number of columns of beads */
	int         spaces;	/* spaces between beads */
	int         base;	/* 10 usually */
	int         delay;
	Position    width;
	int         depth;
	XPoint      beadSize;
	XPoint      pos;
	XPoint      delta, offset;
	int         numDigits;
	char       *digits;
	GC          frameGC, borderGC, beadGC, railGC, inverseGC;
	Boolean     mono, reverse, script, demo;
	int         deck, rail, number;

	XtCallbackList select;
} AbacusPart;

typedef struct _AbacusDemoPart {
	char       *path;
	char       *font;
	int         lessons;
	Pixel       background, foreground;
	int         lessonLength, lessonCount;
	int         deck, rail, number, lines;
	XFontStruct *fontInfo;
	int         fontHeight;
	GC          foregroundGC;
	GC          inverseGC;
	FILE       *fp;
	Boolean     query, started, framed;

	XtCallbackList select;
} AbacusDemoPart;

typedef struct _AbacusRec {
	CorePart    core;
	AbacusPart  abacus;
	AbacusDemoPart abacusDemo;
} AbacusRec;

/* This gets around C's inability to do inheritance */
typedef struct _AbacusClassPart {
	int         ignore;
} AbacusClassPart;

typedef struct _AbacusClassRec {
	CoreClassPart core_class;
	AbacusClassPart abacus_class;
} AbacusClassRec;

extern AbacusClassRec abacusClassRec;

#endif /* _AbacusP_h */
