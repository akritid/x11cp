42
0 0 0 4
Lesson 1: Counting from 1 to 20
The beads will move at each step with an 
appropriate explanation in this window.
Press Space-bar to Begin
0 0 1 4
Lesson 1: Counting
1

Press Space-bar to Continue
0 0 1 4
Lesson 1: Counting
2

Press Space-bar to Continue
0 0 1 4
Lesson 1: Counting
3

Press Space-bar to Continue
0 0 1 4
Lesson 1: Counting
4

Press Space-bar to Continue
0 0 1 4
Lesson 1: Counting
5 
(We're not done yet...)
Press Space-bar to Continue
0 0 0 4
Lesson 1: Counting
It gets a bit tricky here. A carry to the upper-deck, and
a reset of the lower deck will complete counting 5.
Press Space-bar to Continue
1 0 1 4
Lesson 1: Counting
5 (pending operation)
Carry-over to Upper-deck...
Press Space-bar to Continue
0 0 -5 4
Lesson 1: Counting
5
...and Reset Lower-deck...
Press Space-bar
0 0 0 4
Lesson 1: Counting
This is 5. In reality, the 5th bead is never moved, the 
carry-reset operation is performed in one motion.
Press Space-bar to Continue
0 0 1 4
Lesson 1: Counting
6

Press Space-bar to Continue
0 0 1 4
Lesson 1: Counting
7

Press Space-bar to Continue
0 0 1 4
Lesson 1: Counting
8

Press Space-bar to Continue
0 0 1 4
Lesson 1: Counting
9

Press Space-bar to Continue
0 0 1 4
Lesson 1: Counting
10 
(pending operation)
Press Space-bar to Continue
1 0 1 4
Lesson 1: Counting
10 (pending operation)
Carry-over to Upper-deck...
Press Space-bar to Continue
0 0 -5 4
Lesson 1: Counting
10 (pending operation)
...Reset Lower-deck...
Press Space-bar to Continue
0 1 1 4
Lesson 1: Counting
10 
Carry-over to Previous Column
Press Space-bar
1 0 -2 4
Lesson 1: Counting
10
Reset Upper-deck...
Press Space-bar to Continue
0 0 0 4
Lesson 1: Counting
Counting 10 was similar to counting 5, with the 
addition of the extra carry to the Previous Column
Press Space-bar to Continue
0 0 1 4
Lesson 1: Counting
11

Press Space-bar to Continue
0 0 1 4
Lesson 1: Counting
12

Press Space-bar to Continue
0 0 1 4
Lesson 1: Counting
13

Press Space-bar to Continue
0 0 1 4
Lesson 1: Counting
14

Press Space-bar to Continue
0 0 -4 4
Lesson 1: Counting
15 (pending operation)
Reset Bottom-deck...
Press Space-bar to Continue
1 0 1 4
Lesson 1: Counting
15
...Carry-over to Upper-deck.
Press Space-bar to Continue
0 0 1 4
Lesson 1: Counting
16

Press Space-bar to Continue
0 0 1 4
Lesson 1: Counting
17

Press Space-bar to Continue
0 0 1 4
Lesson 1: Counting
18

Press Space-bar to Continue
0 0 1 4
Lesson 1: Counting
19

Press Space-bar to Continue
0 0 1 4
Lesson 1: Counting
20

Press Space-bar to Continue
1 0 1 4
Lesson 1: Counting
20 (pending operation)
Carry-over to the Upper-deck...
Press Space-bar to Continue
0 0 -5 4
Lesson 1: Counting
20 (pending operation)
...Reset Lower-deck...
Press Space-bar to Continue
0 1 1 4
Lesson 1: Counting
20 (pending operation)
...Carry-over To The Previous Column.
Press Space-bar to Continue
1 0 -2 4
Lesson 1: Counting
20
...And Reset Upper-deck...
Press Space-bar to Continue
0 1 -2 4
Lesson 1: End
And now, some conclusions...

Press Space-bar to Continue
0 0 0 4
Conclusion:
Each of the 2 beads on the upper-deck has a value of 5.
Each of the 5 beads on the lower-deck has a value of 1.
Press Space-bar to Continue
0 0 0 4
Conclusion: (continued)
Counting 5 beads on the lower deck requires a carry of 
1 bead to the upper-deck.
Press Space-bar to Continue
0 0 0 4
Conclusion: (continued)
Counting 2 beads on the upper deck requires a carry of 
1 bead on the lower-deck of the previous column.
Press Space-bar
0 0 0 4
Conclusion: (continued)
The one's column is at the middle of the abacus.
(Just left of center for an even number of rails).
Press Space-bar
0 0 0 4
Conclusion: (continued)
Every column to the left of one's column is an
order of magnitude greater.
Press Space-bar
0 0 0 4
Conclusion: (End)
Columns to the right of the one's column are used 
for calculations involving decimals.
Press Space-bar
