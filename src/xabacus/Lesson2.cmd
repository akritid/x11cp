16
0 0 0 4
Lesson 2: Addition 
This Lesson demonstrates 2 addition problems: 
6+5 & 23+9
Press Space-bar to Begin
1 0 1 4
Lesson 2: Addition (6+5)
We Represent 6 by Adding 5...
(pending operation)
Press Space-bar to Continue
0 0 1 4
Lesson 2: Addition (6+5)
...and 1.
(This is 6)
Press Space-bar to Continue
1 0 1 4
Lesson 2: Addition (6+5)
Adding the 5...
(Requires a Carry)... 
Press Space-bar to Continue
0 1 1 4
Lesson 2: Addition (6+5)
...So We Carry to the Previous Column...
(The ten's Column)
Press Space-bar to Continue
1 0 -2 4
Lesson 2: Addition (6+5)
...We Reset The Upper-deck and Obtain The Answer.
10 in the Previous Column + 1 in the Current= 11.
Press Space-bar to Continue
0 1 -1 4
Lesson 2: Addition
That was a Straight-forward Example...

Press Space-bar
0 0 -1 4
Lesson 2: Addition
The following one is a bit more complicated. 
So, pay attention...
Press Space-bar to Begin
0 1 2 4
Lesson 2: Addition (23+9)
20... (begin to represent 23)

Press Space-bar
0 0 3 4
Lesson 2: Addition (23+9)
...+3, represents 23.

Press Space-bar
1 0 2 4
Lesson 2: Addition (23+9)
Represent 9 as +10 -1.    So, 2 Beads on upper-deck= 10...
(pending carry to the ten's column)
Press Space-bar
0 1 1 4
Lesson 2: Addition (23+9)
...carry to ten's column...

Press Space-bar
1 0 -2 4
Lesson 2: Addition (23+9)
Reset Upper-deck of Current-column...
(Pending the `-1' of +10-1= +9)
Press Space-bar
0 0 -1 4
Lesson 2: Addition (23+9)
... -1 (Current-column) completes +9
Resulting in the answer: 32
Press Space-bar
0 0 0 4
Conclusion: (continued)
To perform addition, move beads towards the 
mid-beam; to subtract, move beads away from it.
Press Space-bar
0 0 0 4
Conclusion: (End)
To gain proficiency in using the abacus, practice !

Press Space-bar
