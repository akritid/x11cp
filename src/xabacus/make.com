$ set verify
$! cc="CC/STANDAR=VAXC"
$ if p1.eqs."DEBUG" .or. p2.eqs."DEBUG"
$ then
$!   cc=="CC/DEB/NOOPT/STANDAR=VAXC"
$   cc=="CC/DEB/NOOPT"
$   link=="LINK/DEB"
$ endif
$ if p1.nes."LINK"
$ then
$ define sys sys$library
$!
$ cc Abacus.c
$ cc xabacus.c
$ endif
$!
$ link/map xabacus,Abacus,-
       sys$library:vaxcrtl/lib,-
       sys$library:ucx$ipc/lib,-
       sys$input/opt
sys$share:decw$dxmlibshr/share
sys$share:decw$xlibshr/share
$! sys$library:ucx$ipc_shr/share
$ set noverify
$ exit
