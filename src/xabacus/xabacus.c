
/*-
# X-BASED ABACUS
#
#  xabacus.c
#
###
#
#  Copyright (c) 1994 - 99	David Albert Bagley, bagleyd@tux.org
#
#  Abacus demo and neat pointers from
#  Copyright (c) 1991 - 98  Luis Fernandes, elf@ee.ryerson.ca
#
#                   All Rights Reserved
#
#  Permission to use, copy, modify, and distribute this software and
#  its documentation for any purpose and without fee is hereby granted,
#  provided that the above copyright notice appear in all copies and
#  that both that copyright notice and this permission notice appear in
#  supporting documentation, and that the name of the author not be
#  used in advertising or publicity pertaining to distribution of the
#  software without specific, written prior permission.
#
#  This program is distributed in the hope that it will be "useful",
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
*/

/*-
  Version 4: 94/05/07 Xt
  Version 3: 93/02/03 Motif
  Version 2: 91/12/17 XView
  Version 1: 91/02/14 SunView
*/

#include <stdlib.h>
#include <stdio.h>
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <X11/cursorfont.h>
#include "Abacus.h"
#include "abacus.xbm"

#define MAXDIGITS 256		/* This limits the number of rails */
#define MAXPROGNAME 80
#define MAXNAME (MAXDIGITS+MAXPROGNAME)

static void InitializeDemo(void);
static void CallbackAbacus(Widget w, caddr_t clientData, abacusCallbackStruct * callData);
static void CallbackAbacusDemo(Widget w, caddr_t clientData, abacusCallbackStruct * callData);

static Widget abacus, abacusDemo = NULL;
static char prog[MAXPROGNAME + 2];
static char title[MAXNAME + 1];
static Arg  arg[3];

static void
Usage(void)
{
	(void) fprintf(stderr, "usage: xabacus\n");
	(void) fprintf(stderr,
	     "\t[-geometry [{width}][x{height}][{+-}{xoff}[{+-}{yoff}]]]\n");
	(void) fprintf(stderr,
	   "\t[-display [{host}]:[{vs}]] [-[no]mono] [-[no]{reverse|rv}]\n");
	(void) fprintf(stderr,
		"\t[-{foreground|fg} {color}] [-{background|bg} {color}]\n");
	(void) fprintf(stderr,
	       "\t[-{border|bd} {color}] [-rail {color}] [-bead {color}]\n");
	(void) fprintf(stderr,
		       "\t[-rails {int}] [-spaces {int}] [-base {int}]\n");
	(void) fprintf(stderr,
		   "\t[-tnumber {int}] [-bnumber {int}] [-tfactor {int}]\n");
	(void) fprintf(stderr,
		       "\t[-bfactor {int}] [-[no]torient] [-[no]borient]\n");
	(void) fprintf(stderr,
		       "\t[-delay msecs] [-[no]demo] [-[no]script]\n");
	(void) fprintf(stderr,
		       "\t[-demopath {path}] [-demofont {fontname}]\n");
	(void) fprintf(stderr,
		       "\t[-demofg {color}] [-demobg {color}]\n");
	exit(1);
}

static XrmOptionDescRec options[] =
{
	{"-mono", "*abacus.mono", XrmoptionNoArg, "TRUE"},
	{"-nomono", "*abacus.mono", XrmoptionNoArg, "FALSE"},
	{"-rv", "*abacus.reverse", XrmoptionNoArg, "TRUE"},
	{"-reverse", "*abacus.reverse", XrmoptionNoArg, "TRUE"},
	{"-norv", "*abacus.reverse", XrmoptionNoArg, "FALSE"},
	{"-noreverse", "*abacus.reverse", XrmoptionNoArg, "FALSE"},
	{"-fg", "*abacus.Foreground", XrmoptionSepArg, NULL},
	{"-foreground", "*abacus.Foreground", XrmoptionSepArg, NULL},
	{"-bg", "*Background", XrmoptionSepArg, NULL},
	{"-background", "*Background", XrmoptionSepArg, NULL},
	{"-bd", "*abacus.beadBorder", XrmoptionSepArg, NULL},
	{"-border", "*abacus.beadBorder", XrmoptionSepArg, NULL},
	{"-frame", "*abacus.Foreground", XrmoptionSepArg, NULL},
	{"-rail", "*abacus.railColor", XrmoptionSepArg, NULL},
	{"-bead", "*abacus.beadColor", XrmoptionSepArg, NULL},
	{"-rails", "*abacus.rails", XrmoptionSepArg, NULL},
	{"-spaces", "*abacus.spaces", XrmoptionSepArg, NULL},
	{"-base", "*abacus.base", XrmoptionSepArg, NULL},
	{"-tnumber", "*abacus.topNumber", XrmoptionSepArg, NULL},
	{"-bnumber", "*abacus.bottomNumber", XrmoptionSepArg, NULL},
	{"-tfactor", "*abacus.topFactor", XrmoptionSepArg, NULL},
	{"-bfactor", "*abacus.bottomFactor", XrmoptionSepArg, NULL},
	{"-torient", "*abacus.topOrient", XrmoptionNoArg, "TRUE"},
	{"-notorient", "*abacus.topOrient", XrmoptionNoArg, "FALSE"},
	{"-borient", "*abacus.bottomOrient", XrmoptionNoArg, "TRUE"},
	{"-noborient", "*abacus.bottomOrient", XrmoptionNoArg, "FALSE"},
	{"-delay", "*abacus.delay", XrmoptionSepArg, NULL},
	{"-demo", "*abacus.demo", XrmoptionNoArg, "TRUE"},
	{"-nodemo", "*abacus.demo", XrmoptionNoArg, "FALSE"},
	{"-script", "*abacus.script", XrmoptionNoArg, "TRUE"},
	{"-noscript", "*abacus.script", XrmoptionNoArg, "FALSE"},
	{"-demopath", "*abacus.demoPath", XrmoptionSepArg, NULL},
	{"-demofont", "*abacus.demoFont", XrmoptionSepArg, NULL},
	{"-demofg", "*abacus.demoForeground", XrmoptionSepArg, NULL},
	{"-demobg", "*abacus.demoBackground", XrmoptionSepArg, NULL}
};

int
main(int argc, char **argv)
{
	Widget      toplevel, shell = NULL;
	Boolean     demo;

	(void) sprintf(prog, "%s =", argv[0]);
	(void) sprintf(title, "%s%s", prog, "0");
	toplevel = XtInitialize(title, "Abacus",
				options, XtNumber(options),
				&argc, argv);
	if (argc != 1)
		Usage();

	XtSetArg(arg[0],
		 XtNiconPixmap,
		 XCreateBitmapFromData(XtDisplay(toplevel),
				       RootWindowOfScreen(XtScreen(toplevel)),
			 (char *) abacus_bits, abacus_width, abacus_height));
	XtSetArg(arg[1], XtNinput, True);
	XtSetValues(toplevel, arg, 2);

	abacus = XtCreateManagedWidget("abacus",
				       abacusWidgetClass, toplevel, NULL, 0);
	XtAddCallback(abacus,
	XtNselectCallback, (XtCallbackProc) CallbackAbacus, (XtPointer) NULL);

	XtVaGetValues(abacus, XtNdemo, &demo, NULL);
	if (demo) {
		(void) sprintf(title, "%s-demo", argv[0]);
		shell = XtCreateApplicationShell(title,
					  topLevelShellWidgetClass, NULL, 0);

		XtSetArg(arg[0],
		       XtNiconPixmap, XCreateBitmapFromData(XtDisplay(shell),
					 RootWindowOfScreen(XtScreen(shell)),
			 (char *) abacus_bits, abacus_width, abacus_height));
		XtSetArg(arg[1], XtNinput, True);
		XtSetArg(arg[2], XtNtitle, title);
		XtSetValues(shell, arg, 3);
		abacusDemo = XtCreateManagedWidget("abacus",
				      abacusDemoWidgetClass, shell, NULL, 0);
		XtAddCallback(abacusDemo,
			      XtNselectCallback, (XtCallbackProc) CallbackAbacusDemo, (XtPointer) NULL);
		InitializeDemo();
	}
	XtRealizeWidget(toplevel);
	if (demo)
		XtRealizeWidget(shell);
	XGrabButton(XtDisplay(abacus), (unsigned int) AnyButton, AnyModifier,
		    XtWindow(abacus), TRUE,
		    (unsigned int) (ButtonPressMask | ButtonMotionMask | ButtonReleaseMask),
		    GrabModeAsync, GrabModeAsync, XtWindow(abacus),
		    XCreateFontCursor(XtDisplay(abacus), XC_crosshair));
	if (demo) {
		XGrabButton(XtDisplay(abacusDemo), (unsigned int) AnyButton, AnyModifier,
			    XtWindow(abacusDemo), TRUE,
			    (unsigned int) (ButtonPressMask | ButtonMotionMask | ButtonReleaseMask),
			  GrabModeAsync, GrabModeAsync, XtWindow(abacusDemo),
			 XCreateFontCursor(XtDisplay(abacusDemo), XC_hand2));
	}
	XtMainLoop();

#ifdef VMS
	return 1;
#else
	return 0;
#endif
}

/* There's probably a better way to assure that they are the same but I do
 * not know it off hand. */
static void
InitializeDemo(void)
{
	Boolean     mono, reverse;
	Pixel       demoForeground, demoBackground;
	String      demoPath, demoFont;

	XtVaGetValues(abacus,
		      XtNmono, &mono,
		      XtNreverse, &reverse,
		      XtNdemoForeground, &demoForeground,
		      XtNdemoBackground, &demoBackground,
		      XtNdemoPath, &demoPath,
		      XtNdemoFont, &demoFont, NULL);
	XtVaSetValues(abacusDemo,
		      XtNmono, mono,
		      XtNreverse, reverse,
		      XtNdemoForeground, demoForeground,
		      XtNdemoBackground, demoBackground,
		      XtNdemoPath, demoPath,
		      XtNdemoFont, demoFont,
		      XtNframed, False, NULL);
}

static void
CallbackAbacus(Widget w, caddr_t clientData, abacusCallbackStruct * callData)
{
	if (callData->reason == ABACUS_SCRIPT || callData->reason == ABACUS_IGNORE) {
		(void) sprintf(title, "%s%s", prog, callData->buffer);
		XtSetArg(arg[0], XtNtitle, title);
		XtSetValues(XtParent(w), arg, 1);
	}
	switch (callData->reason) {
		case ABACUS_SCRIPT:
			(void) printf("%d %d %d\n",
			   callData->deck, callData->rail, callData->number);
			break;
		case ABACUS_CLEAR:
			XtSetArg(arg[0], XtNdeck, ABACUS_CLEAR);
			XtSetValues(abacusDemo, arg, 1);
			break;
		case ABACUS_NEXT:
			XtSetArg(arg[0], XtNdeck, ABACUS_NEXT);
			XtSetValues(abacusDemo, arg, 1);
			break;
		case ABACUS_REPEAT:
			XtSetArg(arg[0], XtNdeck, ABACUS_REPEAT);
			XtSetValues(abacusDemo, arg, 1);
			break;
		case ABACUS_MORE:
			XtSetArg(arg[0], XtNdeck, ABACUS_MORE);
			XtSetValues(abacusDemo, arg, 1);
			break;
	}
}

static void
CallbackAbacusDemo(Widget w, caddr_t clientData, abacusCallbackStruct * callData)
{
	if (callData->reason == ABACUS_SCRIPT || callData->reason == ABACUS_IGNORE) {
		(void) sprintf(title, "%s%s", prog, callData->buffer);
		XtSetArg(arg[0], XtNtitle, title);
		XtSetValues(XtParent(w), arg, 1);
	}
	switch (callData->reason) {
		case ABACUS_MOVE:
			XtSetArg(arg[0], XtNdeck, callData->deck);
			XtSetArg(arg[1], XtNrail, callData->rail);
			XtSetArg(arg[2], XtNnumber, callData->number);
			XtSetValues(abacus, arg, 3);
			break;
		case ABACUS_CLEAR:
			XtSetArg(arg[0], XtNdeck, ABACUS_CLEAR);
			XtSetValues(abacus, arg, 1);
			break;
	}
}
