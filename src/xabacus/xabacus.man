.\" X-BASED ABACUS
.\"
.\" xabacus.man
.\"
.\" ##
.\"
.\" Copyright (c) 1994 - 99	David Albert Bagley
.\"
.\"                   All Rights Reserved
.\"
.\" Permission to use, copy, modify, and distribute this software and
.\" its documentation for any purpose and without fee is hereby granted,
.\" provided that the above copyright notice appear in all copies and
.\" that both that copyright notice and this permission notice appear in
.\" supporting documentation, and that the name of the author not be
.\" used in advertising or publicity pertaining to distribution of the
.\" software without specific, written prior permission.
.\"
.\" This program is distributed in the hope that it will be "useful",
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
.\"
.TH ABACUS 1 "20 June 1999" "V5.5"
.SH NAME
xabacus \- Abacus X widget 
.SH SYNOPSIS
.B xabacus
 [-geometry [{width}][x{height}][{+-}{xoff}[{+-}{yoff}]]]
[-display [{host}]:[{vs}]] [-[no]mono] [-[no]{reverse|rv}]
[-{foreground|fg} {color}] [-{background|bg} {color}]
[-{border|bd} {color}] [-rail {color}] [-bead {color}]
[-rails {int}] [-spaces {int}] [-base {int}]
[-tnumber {int}] [-bnumber {int}] [-tfactor {int}]
[-bfactor {int}] [-[no]torient] [-[no]borient]
[-delay msecs] [-[no]demo] [-[no]script]
[-demopath {path}] [-demofont {fontname}]
[-demofg {color}] [-demobg {color}]
.SH DESCRIPTION
This is an implementation of the classic Chinese abacus.

The device has two decks.  Each deck, separated by a beam, normally has 13
rails on which are mounted beads.  Each rail on the top deck contains 2
beads, and each rod on the bottom deck contains 5 beads.  Each bead on the
upper deck has a value of five, while each bead on the lower deck has
value of one.  Beads are considered counted, when moved \fItowards\fP the
beam separating the decks.

There are books on how to use an abacus, but basically all it does is add
and subtract, the rest you have to do in you head.  There are techniques
like using your thumb and forefinger which does not apply with mouse entry.
Also with multiplication, one can carry out calculations on different parts
of the abacus, here it is nice to have a long abacus.

This device has two decks ("new & improved" models have auxilliary decks
stacked above the principal decks that enable multiplication, division and
square-root (honest!) computations to be performed with equal ease as
addition and subtraction).

Check out http://www.ee.ryerson.ca/~elf/abacus/ to find out more.
.SH FEATURES
Click "\fBmouse-left\fP" button on a bead you want to move.
The beads will shift themselves to vacate the row and column that was
clicked.
.LP
Click "\fBmouse-right\fP" button, or press "\fBC\fP" or "\fBc\fP" keys to
clear the abacus.
.LP
Press "\fBI\fP" or "\fBi\fP" keys to increment the number of rails.
.LP
Press "\fBD\fP" or "\fBd\fP" keys to decrement the number of rails.
.LP
Press "\fBQ\fP", "\fBq\fP", or "\fBCTRL-C\fP" keys to kill program.
.LP
The abacus may be resized.  Beads will reshape depending on the room they
have.
\fIDemo Mode:\fP
In this mode, the abacus is controlled by the program.
When started with the demo option, a second window is presented that should
be placed directly below the abacus-window. Descriptive text, and user
prompts are displayed in this window.

Pressing 'q' during the demo will quit it.  Clicking the left mouse-button
with the pointer in the window will restart the demo (beginning of current
lesson).
.SH OPTIONS
.TP 8
.B \-geometry {+|\-}\fIX\fP{+|\-}\fIY\fP
This option sets the initial position of the abacus window (resource
name "\fIgeometry\fP").
.TP 8
.B \-display \fIhost\fP:\fIdpy\fP
This option specifies the X server to contact.
.TP 8
.B \-[no]mono
This option allows you to  display on a color screen as if monochrome
(resource name "\fImono\fP").
.TP 8
.B \-[no]{reverse|rv}
This option allows you to see the abacus window in reverse video
(resource name "\fIreverse\fP").
.TP 8
.B \-{foreground|fg} \fIcolor\fP
This option specifies the foreground of the abacus window (resource name
"\fIforeground\fP").
.TP 8
.B \-{background|bg} \fIcolor\fP
This option specifies the background of the abacus window (resource name
"\fIbackground\fP").
.TP 8
.B \-{border|bd} \fIcolor\fP
This option specifies the foreground of the border of the beads
(resource name "\fIborderColor\fP").
.TP 8
.B \-bead \fIcolor\fP
This option specifies the foreground of the beads (resource name
"\fIbeadColor\fP").
.TP 8
.B \-rails \fIint\fP
This option specifies the number of rails (resource name "\fIrails\fP").
.TP 8
.B \-spaces \fIint\fP
This option specifies the number of spaces (resource name "\fIspaces\fP").
.TP 8
.B \-base \fIint\fP
This option specifies the base used (default is base 10) (resource name
"\fIbase\fP").
.TP 8
.B \-tnumber \fIint\fP
This option specifies the number of beads on top (resource name
"\fItopNumber\fP").
.TP 8
.B \-bnumber \fIint\fP
This option specifies the number of beads on bottom (resource name
"\fIbottomNumber\fP").
.TP 8
.B \-tfactor \fIint\fP
This option specifies the multiply factor for the beads on top (resource
name "\fItopFactor\fP").
.TP 8
.B \-bfactor \fIint\fP
This option specifies the multiply factor for the  beads on bottom
(resource name "\fIbottomFactor\fP").
.TP 8
.B \-[no]torient
This option specifies the orientation of the beads on top (resource name
"\fItopOrient\fP").
.TP 8
.B \-[no]borient
This option specifies the orientation of the beads on bottom (resource name
"\fIbottomOrient\fP").
.TP 8
.B \-delay \fImsecs\fP
This option specifies the number of milliseconds it takes to move a bead or
a group of beads one space (resource name "\fIdelay\fP").
.TP 8
.B \-[no]demo
This option specifies to run in demo mode.
.TP 8
.B \-[no]script
This option specifies to log application to /f\Istdout\fP, every time
the user clicks to move the beads. The output is a set of deck,
rail, and beads added or subtracted, and then this can be edited and
used to create new demos (resource name "\fIscript\fP").
.TP 8
.B \-demopath \fIpath\fP
This option specifies the path for the demo, possibly /usr/local/lib
(resource name "\fIdemoPath\fP").  It initially looks for Lesson1.cmd.
If it finds that, then looks for Lesson2.cmd, etc.   
.B \-demofont \fIfontstring\fP
This option specifies the font for the explantory text that appears in
the secondary window, during the demo.  The default font is 18 point
Times-Roman (-*-times-*-r-*-*-*-180-*). The alternate font is 8x13.
.TP 8
.B \-demofg \fIcolor\fP
This option specifies the foreground of the abacus demo window (resource
name "\fIdemoForeground\fP").
.TP 8
.B \-demobg \fIcolor\fP
This option specifies the background of the abacus demo window (resource
name "\fIdemoBackground\fP").
.SH SEE ALSO
.LP
X(1), xrubik(6), xskewb(6), xdino(6), xpyraminx(6), xoct(6), xmball(6),
xmlink(6), xpanex(6), xcubes(6), xtriangles(6), xhexagons(6)
.SH COPYRIGHTS
.LP
\*R Copyright 1994-99, David Albert Bagley
.LP
Luis Fernandes, <\fIelf@ee.ryerson.ca\fP> wrote an independent program
(xabacus 1.00) with a demo mode and postscript file.  I tried, with his
permission, to take the best features of both into one program with
xabacus-5.5.
.SH BUG REPORTS AND PROGRAM UPDATES
.LP
Send bugs (or their reports, or fixes) to the author:
.RS
David Albert Bagley,	<\fIbagleyd@tux.org\fP>
.RE
.sp
The latest version is currently at:
.RS
\fIftp://ftp.tux.org/pub/tux/bagleyd/xabacus\fP
.br
\fIftp://ftp.x.org/contrib/applications\fP

