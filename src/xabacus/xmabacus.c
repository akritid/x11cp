/*-
# MOTIF-BASED ABACUS
#
#  xmabacus.c
#
###
#
#  Copyright (c) 1993 - 99	David Albert Bagley, bagleyd@tux.org
#
#  Abacus demo and neat pointers from
#  Copyright (c) 1991 - 98  Luis Fernandes, elf@ee.ryerson.ca
#
#                   All Rights Reserved
#
#  Permission to use, copy, modify, and distribute this software and
#  its documentation for any purpose and without fee is hereby granted,
#  provided that the above copyright notice appear in all copies and
#  that both that copyright notice and this permission notice appear in
#  supporting documentation, and that the name of the author not be
#  used in advertising or publicity pertaining to distribution of the
#  software without specific, written prior permission.
#
#  This program is distributed in the hope that it will be "useful",
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
*/

/*-
  Version 5: 95/09/30 Xt/Motif
  Version 4: 94/05/07 Xt
  Version 3: 93/02/03 Motif
  Version 2: 91/12/17 XView
  Version 1: 91/02/14 SunView
*/

#include <stdlib.h>
#include <stdio.h>
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <X11/cursorfont.h>
#include <Xm/PanedW.h>
#include <Xm/RowColumn.h>
#include <Xm/Label.h>
#include <Xm/LabelG.h>
#include <Xm/Scale.h>

#include "Abacus.h"
#include "abacus.xbm"
#include "mouse-l.xbm"
#include "mouse-r.xbm"

#define MAXPROGNAME 80

/* The following is in AbacusP.h also */
#define MINRAILS 1
#define MINDEMORAILS 3
#define MAXRAILS 24		/* Totally arbitrary */

static void InitializeDemo(void);
static void CallbackAbacus(Widget w, caddr_t clientData,
			   abacusCallbackStruct * callData);
static void CallbackAbacusDemo(Widget w, caddr_t clientData,
			       abacusCallbackStruct * callData);
static void RailSlider(Widget w, XtPointer clientData,
		       XmScaleCallbackStruct * cbs);
static void motif_print(Widget w, char *text);

static Widget tracker, abacus, abacusDemo = NULL, rods;
static char title[MAXPROGNAME + 6];
static Arg  arg[3];

static void
Usage(void)
{
	(void) fprintf(stderr, "usage: xmabacus\n");
	(void) fprintf(stderr,
	     "\t[-geometry [{width}][x{height}][{+-}{xoff}[{+-}{yoff}]]]\n");
	(void) fprintf(stderr,
	   "\t[-display [{host}]:[{vs}]] [-[no]mono] [-[no]{reverse|rv}]\n");
	(void) fprintf(stderr,
		"\t[-{foreground|fg} {color}] [-{background|bg} {color}]\n");
	(void) fprintf(stderr,
	       "\t[-{border|bd} {color}] [-rail {color}] [-bead {color}]\n");
	(void) fprintf(stderr,
		       "\t[-rails {int}] [-spaces {int}] [-base {int}]\n");
	(void) fprintf(stderr,
		   "\t[-tnumber {int}] [-bnumber {int}] [-tfactor {int}]\n");
	(void) fprintf(stderr,
		       "\t[-bfactor {int}] [-[no]torient] [-[no]borient]\n");
	(void) fprintf(stderr,
		       "\t[-delay msecs] [-[no]demo] [-[no]script]\n");
	(void) fprintf(stderr,
		       "\t[-demopath {path}] [-demofont {fontname}]\n");
	(void) fprintf(stderr,
		       "\t[-demofg {color}] [-demobg {color}]\n");
	exit(1);
}

static XrmOptionDescRec options[] =
{
	{"-mono", "*abacus.mono", XrmoptionNoArg, "TRUE"},
	{"-nomono", "*abacus.mono", XrmoptionNoArg, "FALSE"},
	{"-rv", "*abacus.reverse", XrmoptionNoArg, "TRUE"},
	{"-reverse", "*abacus.reverse", XrmoptionNoArg, "TRUE"},
	{"-norv", "*abacus.reverse", XrmoptionNoArg, "FALSE"},
	{"-noreverse", "*abacus.reverse", XrmoptionNoArg, "FALSE"},
	{"-fg", "*abacus.Foreground", XrmoptionSepArg, NULL},
	{"-foreground", "*abacus.Foreground", XrmoptionSepArg, NULL},
	{"-bg", "*Background", XrmoptionSepArg, NULL},
	{"-background", "*Background", XrmoptionSepArg, NULL},
	{"-bd", "*abacus.beadBorder", XrmoptionSepArg, NULL},
	{"-border", "*abacus.beadBorder", XrmoptionSepArg, NULL},
	{"-frame", "*abacus.Foreground", XrmoptionSepArg, NULL},
	{"-rail", "*abacus.railColor", XrmoptionSepArg, NULL},
	{"-bead", "*abacus.beadColor", XrmoptionSepArg, NULL},
	{"-rails", "*abacus.rails", XrmoptionSepArg, NULL},
	{"-spaces", "*abacus.spaces", XrmoptionSepArg, NULL},
	{"-base", "*abacus.base", XrmoptionSepArg, NULL},
	{"-tnumber", "*abacus.topNumber", XrmoptionSepArg, NULL},
	{"-bnumber", "*abacus.bottomNumber", XrmoptionSepArg, NULL},
	{"-tfactor", "*abacus.topFactor", XrmoptionSepArg, NULL},
	{"-bfactor", "*abacus.bottomFactor", XrmoptionSepArg, NULL},
	{"-torient", "*abacus.topOrient", XrmoptionNoArg, "TRUE"},
	{"-notorient", "*abacus.topOrient", XrmoptionNoArg, "FALSE"},
	{"-borient", "*abacus.bottomOrient", XrmoptionNoArg, "TRUE"},
	{"-noborient", "*abacus.bottomOrient", XrmoptionNoArg, "FALSE"},
	{"-delay", "*abacus.delay", XrmoptionSepArg, NULL},
	{"-demo", "*abacus.demo", XrmoptionNoArg, "TRUE"},
	{"-nodemo", "*abacus.demo", XrmoptionNoArg, "FALSE"},
	{"-script", "*abacus.script", XrmoptionNoArg, "TRUE"},
	{"-noscript", "*abacus.script", XrmoptionNoArg, "FALSE"},
	{"-demopath", "*abacus.demoPath", XrmoptionSepArg, NULL},
	{"-demofont", "*abacus.demoFont", XrmoptionSepArg, NULL},
	{"-demofg", "*abacus.demoForeground", XrmoptionSepArg, NULL},
	{"-demobg", "*abacus.demoBackground", XrmoptionSepArg, NULL}
};

int
main(int argc, char **argv)
{
	Widget      toplevel;
	Widget      panel, panel2, rowcol, rowcol2;
	Pixmap      mouseLeftCursor, mouseRightCursor;
	Pixel       fg, bg;
	Arg         arg[2];
	int         rails;
	Boolean     demo;

	toplevel = XtInitialize(argv[0], "Abacus", options, XtNumber(options),
				&argc, argv);
	if (argc != 1)
		Usage();
	XtSetArg(arg[0],
		 XtNiconPixmap, XCreateBitmapFromData(XtDisplay(toplevel),
				      RootWindowOfScreen(XtScreen(toplevel)),
			 (char *) abacus_bits, abacus_width, abacus_height));
	XtSetArg(arg[1], XmNkeyboardFocusPolicy, XmPOINTER);	/* not XmEXPLICIT */
	XtSetValues(toplevel, arg, 2);
	panel = XtCreateManagedWidget("panel",
				xmPanedWindowWidgetClass, toplevel, NULL, 0);
	panel2 = XtVaCreateManagedWidget("panel2",
					 xmPanedWindowWidgetClass, panel,
					 XmNseparatorOn, False,
					 XmNsashWidth, 1,
					 XmNsashHeight, 1,
					 NULL);

	rowcol = XtVaCreateManagedWidget("Rowcol",
					 xmRowColumnWidgetClass, panel2,
					 XmNnumColumns, 4,
					 XmNpacking, XmPACK_COLUMN, NULL);
	XtVaGetValues(rowcol,
		      XmNforeground, &fg,
		      XmNbackground, &bg, NULL);
	mouseLeftCursor = XCreatePixmapFromBitmapData(XtDisplay(rowcol),
	      RootWindowOfScreen(XtScreen(rowcol)), (char *) mouse_left_bits,
				 mouse_left_width, mouse_left_height, fg, bg,
				     DefaultDepthOfScreen(XtScreen(rowcol)));
	mouseRightCursor = XCreatePixmapFromBitmapData(XtDisplay(rowcol),
	     RootWindowOfScreen(XtScreen(rowcol)), (char *) mouse_right_bits,
			       mouse_right_width, mouse_right_height, fg, bg,
				     DefaultDepthOfScreen(XtScreen(rowcol)));
	XtVaCreateManagedWidget("mouseLeftText",
				xmLabelGadgetClass, rowcol,
	     XtVaTypedArg, XmNlabelString, XmRString, "Move bead", 10, NULL);
	XtVaCreateManagedWidget("mouseLeft",
				xmLabelGadgetClass, rowcol,
				XmNlabelType, XmPIXMAP,
				XmNlabelPixmap, mouseLeftCursor, NULL);
	XtVaCreateManagedWidget("mouseRightText",
				xmLabelGadgetClass, rowcol,
	     XtVaTypedArg, XmNlabelString, XmRString, "    Clear", 10, NULL);
	XtVaCreateManagedWidget("mouseRight",
				xmLabelGadgetClass, rowcol,
				XmNlabelType, XmPIXMAP,
				XmNlabelPixmap, mouseRightCursor, NULL);

	rowcol2 = XtVaCreateManagedWidget("Rowcol2",
				       xmRowColumnWidgetClass, panel2, NULL);

	tracker = XtCreateManagedWidget("0",
					xmLabelWidgetClass, rowcol2, NULL, 0);

	abacus = XtCreateManagedWidget("abacus",
				       abacusWidgetClass, panel, NULL, 0);
	XtAddCallback(abacus,
	XtNselectCallback, (XtCallbackProc) CallbackAbacus, (XtPointer) NULL);

	XtVaGetValues(abacus,
		      XtNrails, &rails,
		      XtNdemo, &demo, NULL);

	rods = XtVaCreateManagedWidget("rails",
				       xmScaleWidgetClass, rowcol2,
			 XtVaTypedArg, XmNtitleString, XmRString, "Rails", 6,
				XmNminimum, (demo) ? MINDEMORAILS : MINRAILS,
				       XmNmaximum, MAXRAILS,
				       XmNvalue, rails,
				       XmNshowValue, True,
				       XmNorientation, XmHORIZONTAL, NULL);
	XtAddCallback(rods,
		      XmNvalueChangedCallback, (XtCallbackProc) RailSlider, (XtPointer) NULL);

	if (demo) {
		(void) sprintf(title, "%s-demo", argv[0]);
		abacusDemo = XtCreateManagedWidget(title,
				      abacusDemoWidgetClass, panel, NULL, 0);
		XtAddCallback(abacusDemo,
			      XtNselectCallback, (XtCallbackProc) CallbackAbacusDemo, (XtPointer) NULL);
		InitializeDemo();
	}
	XtRealizeWidget(toplevel);
	XGrabButton(XtDisplay(abacus), (unsigned int) AnyButton, AnyModifier,
		    XtWindow(abacus), TRUE,
		    (unsigned int) (ButtonPressMask | ButtonMotionMask | ButtonReleaseMask),
		    GrabModeAsync, GrabModeAsync, XtWindow(abacus),
		    XCreateFontCursor(XtDisplay(abacus), XC_crosshair));
	if (demo) {
		XGrabButton(XtDisplay(abacusDemo), (unsigned int) AnyButton, AnyModifier,
			    XtWindow(abacusDemo), TRUE,
			    (unsigned int) (ButtonPressMask | ButtonMotionMask | ButtonReleaseMask),
			  GrabModeAsync, GrabModeAsync, XtWindow(abacusDemo),
			 XCreateFontCursor(XtDisplay(abacusDemo), XC_hand2));
	}
	XtMainLoop();

#ifdef VMS
	return 1;
#else
	return 0;
#endif
}

/* There's probably a better way to assure that they are the same but I do
 * not know it off hand. */
static void
InitializeDemo(void)
{
	Boolean     mono, reverse;
	Pixel       demoForeground, demoBackground;
	String      demoPath, demoFont;

	XtVaGetValues(abacus,
		      XtNmono, &mono,
		      XtNreverse, &reverse,
		      XtNdemoForeground, &demoForeground,
		      XtNdemoBackground, &demoBackground,
		      XtNdemoPath, &demoPath,
		      XtNdemoFont, &demoFont, NULL);
	XtVaSetValues(abacusDemo,
		      XtNmono, mono,
		      XtNreverse, reverse,
		      XtNdemoForeground, demoForeground,
		      XtNdemoBackground, demoBackground,
		      XtNdemoPath, demoPath,
		      XtNdemoFont, demoFont,
		      XtNframed, True, NULL);
}

static void
CallbackAbacus(Widget w, caddr_t clientData, abacusCallbackStruct * callData)
{
	int         rails;

	if (callData->reason == ABACUS_SCRIPT || callData->reason == ABACUS_IGNORE) {
		motif_print(tracker, callData->buffer);
	}
	switch (callData->reason) {
		case ABACUS_SCRIPT:
			(void) printf("%d %d %d\n",
			   callData->deck, callData->rail, callData->number);
			break;
		case ABACUS_CLEAR:
			XtSetArg(arg[0], XtNdeck, ABACUS_CLEAR);
			XtSetValues(abacusDemo, arg, 1);
			break;
		case ABACUS_NEXT:
			XtSetArg(arg[0], XtNdeck, ABACUS_NEXT);
			XtSetValues(abacusDemo, arg, 1);
			break;
		case ABACUS_REPEAT:
			XtSetArg(arg[0], XtNdeck, ABACUS_REPEAT);
			XtSetValues(abacusDemo, arg, 1);
			break;
		case ABACUS_MORE:
			XtSetArg(arg[0], XtNdeck, ABACUS_MORE);
			XtSetValues(abacusDemo, arg, 1);
			break;
		case ABACUS_INC:
			XtVaGetValues(w,
				      XtNrails, &rails, NULL);
			if (rails <= MAXRAILS)
				XmScaleSetValue(rods, rails);
			break;
		case ABACUS_DEC:
			XtVaGetValues(w,
				      XtNrails, &rails, NULL);
			if (rails <= MAXRAILS)
				XmScaleSetValue(rods, rails);
			break;
	}
}

static void
CallbackAbacusDemo(Widget w, caddr_t clientData, abacusCallbackStruct * callData)
{
	if (callData->reason == ABACUS_SCRIPT || callData->reason == ABACUS_IGNORE) {
		motif_print(tracker, callData->buffer);
	}
	switch (callData->reason) {
		case ABACUS_MOVE:
			XtSetArg(arg[0], XtNdeck, callData->deck);
			XtSetArg(arg[1], XtNrail, callData->rail);
			XtSetArg(arg[2], XtNnumber, callData->number);
			XtSetValues(abacus, arg, 3);
			break;
		case ABACUS_CLEAR:
			XtSetArg(arg[0], XtNdeck, ABACUS_CLEAR);
			XtSetValues(abacus, arg, 1);
			break;
	}
}

static void
RailSlider(Widget w, XtPointer clientData, XmScaleCallbackStruct * cbs)
{
	int         rails = cbs->value, old;

	XtVaGetValues(abacus,
		      XtNrails, &old, NULL);
	if (old != rails) {
		XtVaSetValues(abacus,
			      XtNrails, rails, NULL);
	}
}

static void
motif_print(Widget w, char *text)
{
	Arg         wargs[1];
	XmString    xmstr;

	if (!XtIsSubclass(w, xmLabelWidgetClass))
		XtError("motif_printf() requires a Label Widget");
	xmstr = XmStringCreateLtoR(text, XmSTRING_DEFAULT_CHARSET);
	XtSetArg(wargs[0], XmNlabelString, xmstr);
	XtSetValues(w, wargs, 1);
}
