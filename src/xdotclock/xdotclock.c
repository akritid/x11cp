/*  XDotClock - A simple digital clock for X
 *  Copyright (c) 2003 Haran Shivanan <haranshivanan@yahoo.com>
 *  See the file COPYING in this directory for licensing info
 */
#include <stdlib.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <math.h>

#include <time.h>

#ifndef VERSION
	#define VERSION "???"
#endif

/* Default Radius of each particle */
#define RADIUS 5

/* Maximum number of particles that can represent a digit */
#define CLUSTER_SIZE 23

#define BLACK "#000000"
#define WHITE "#FFFFFF"
#define RED   "#FF0000"
#define GREEN "#00FF00"
#define BLUE  "#0000FF"

/* increasing this decreases the speed with which
 * color gradients shift */
#define RAMP_SIZE 500


/* number of digits to render */
#define DIGIT_COUNT 6

/* default speed of the particles */
static int speed = 1;
static int smooth = 0;

static unsigned long color_ramp[RAMP_SIZE];

/* lame hack to calculate window resizes */
static unsigned int prev_width;

static char * window_title = "XDotClock v"VERSION;

typedef struct options_t
{
	int    rv;        /* reverse video:black foreground and white background */
	char * fgc;       /* foreground color */
	char * bgc;       /* background color */
	char * bdc;       /* border color */
	int    bw;        /* border width */
	int    _24;       /* 24 hour display? */
	int    radius;    /* Radius of each particle */
	int    speed;     /* The speed with which the digits change */
	int    autospeed; /* calculate a suitable speed for the given radius - 'speed'
						 overrides this */
	int    cycle;     /* Shift the color of the digits smoothly */
	int    smooth;    /* Semi-Smooth motion of particles */
	
	char * crap; /* lazy hack */
}OPTIONS;

typedef struct particle_t PARTICLE;
typedef struct group_t
{
	PARTICLE * particles;
	size_t size;
	GC gc;
	unsigned short red, blue, green;
	int x,y;
	double z_angle;
	double radius;
}GROUP;


struct particle_t
{
	int center_x;
	int center_y;
	int x,y;
	int goal_x, goal_y;
	GROUP * group;
};



/* This is sucks up performance. A lot.
 * A solution would be to allocate writable color cells and fill them
 * using XStoreColors. Unfortunately, on my box, X has not been configured
 * to provide Visuals that give writable color cells. Or something like that.
 */
void init_color_ramp (Display * display, unsigned short red1, unsigned short green1, unsigned short blue1,
		unsigned short red2, unsigned short green2, unsigned short blue2)
{
	XColor color;
	int i;
	int rd,gd,bd;
	Colormap cm = DefaultColormap (display, 0);
	rd = (red2 - red1)/RAMP_SIZE;
	gd = (green2 - green1)/RAMP_SIZE;
	bd = (blue2 - blue1)/RAMP_SIZE;
	for (i=0;i<RAMP_SIZE;++i)
	{
		color.red = red1 + i*rd;
		color.green= green1 + i*gd;
		color.blue = blue1 + i*bd;
		color.flags = DoRed|DoGreen|DoBlue;
		XAllocColor (display, cm, &color);
		color_ramp[i] = color.pixel;
	}
	
}


/* A shrink-wrapped function to generate a smooth color gradient
 * from red->green->blue
 */
void color_ramp_span (Display * display)
{
	XColor color;
	int i;
	int rd,gd,bd;
	Colormap cm = DefaultColormap (display, 0);
	rd = ( 0-60000)/(RAMP_SIZE/2);
	gd = (60000-0)/(RAMP_SIZE/2);
	for (i=0;i<RAMP_SIZE/2;++i)
	{
		color.red = 60000 + i*rd;
		color.green= 0 + i*gd;
		color.blue = 0;
		color.flags = DoRed|DoGreen|DoBlue;
		XAllocColor (display, cm, &color);
		color_ramp[i] = color.pixel;
	}
	gd = (0-60000)/(RAMP_SIZE/2);
	bd = (60000 - 0)/(RAMP_SIZE/2);
	for (i=0;i<RAMP_SIZE/2;++i)
	{
		color.red = 0;
		color.green= 60000 + i*gd;
		color.blue = 0 + i*bd;
		color.flags = DoRed|DoGreen|DoBlue;
		XAllocColor (display, cm, &color);
		color_ramp[i+RAMP_SIZE/2] = color.pixel;
	}
	
}



/* take a string representation of a color and allocate\return a pixel
 * in the default colormap
 */
unsigned long color_from_str (Display * display, char *col)
{
	XColor result;
	Colormap cm;
	cm = DefaultColormap (display, 0);
	if (!XParseColor (display, cm, col, &result))
	{
		fprintf (stderr, "Unable to parse color:%s\n",col);
		exit (0);
	}
	XAllocColor (display, cm, &result);
	return result.pixel;
}


Window create_simple_window(Display* display, int width, int height, int x, int y,int win_border_width, 
		unsigned long bdc, unsigned long bgc)
{
  int screen_num = DefaultScreen(display);
  Window win;
  win = XCreateSimpleWindow(display, RootWindow(display, screen_num),
                            x, y, width, height, win_border_width,
							bdc,
							bgc);
  XMapWindow(display, win);
  XFlush(display);
  return win;
}


/* return a gc with the appropriate foreground color */
GC gc_color (Display * display, Window win, char * colstr)
{
	GC result;
	XColor color;
	Colormap cm;
	cm = DefaultColormap (display, 0);
	result = XCreateGC (display, win, 0, 0);
	if (!XParseColor (display, cm, colstr, &color))
	{
		fprintf (stderr, "Unable to parse color:%s\n",colstr);
		exit (0);
	}
	XAllocColor (display, cm, &color);
	XSetForeground (display, result, color.pixel);
	return result;
}


	

/* a better version of sleep() with milli-second accuracy ... sort of. */
void my_sleep(long usec)
{
	struct timeval tv;
	tv.tv_usec = usec;
	tv.tv_sec = 0;
	select(0,NULL,NULL,NULL,&tv);
}

void color_shift (Display * display, GC gc)
{

	static int index = 0;
	static int delta = 1;
	XSetForeground (display, gc, color_ramp[index]);
	if (index>=RAMP_SIZE-1)
		delta = -1,index=RAMP_SIZE-1;
	else if (index <=0)
		delta = 1,index=0;
	
	index += delta;
	
}

unsigned int calculate_delta (int offset)
{
	if (!smooth) return offset>speed?speed:1;
	if (offset > 120) return 7;
	if (offset > 50 ) return 5;
	if (offset > 18  ) return 2;
	return 1;
}

/* the actual function that renders the 'particles' on the screen */
/* TODO: Make the movement of particles more organic */
/* TODO: Reduce flicker (double buffering?) */

void draw_particle_h(Display * display, Window win, GC gc, GC rev_gc, GROUP * group, 
		 int radius)
{
	unsigned int i;
	XArc old[CLUSTER_SIZE];
	XArc new[CLUSTER_SIZE];
	int oldcount = 0, newcount = 0;

	
	/* TODO: Check size of XArc structures
	XArc * old;
	XArc * new;
	old = (XArc *)malloc(sizeof(XArc)*group->size);
	new = (XArc *)malloc(sizeof(XArc)*group->size);*/
	for (i=0;i<group->size;i++)
	{
		PARTICLE * particle = group->particles+i;
		int xo=0,yo=0;
		int xi = particle->x;
		int yi = particle->y;
		
		
		xo = particle->x - particle->group->x - particle->goal_x;
		yo = particle->y - particle->group->y - particle->goal_y;
		if (yo!=0)
		{
			/*unsigned int delta1 = (abs(yo)>delta)?delta:1;*/
			unsigned int delta1 = calculate_delta (abs(yo));
			particle->y += (yo>0)?-delta1:delta1;
		}
		if (xo!=0)
		{
			/*int delta1 = (abs(xo)>delta)?delta:1;*/
			unsigned int delta1 = calculate_delta (abs(xo));
			particle->x += (xo>0)?-delta1:delta1;
		}
		if (xi != particle->x || yi != particle->y)
		{
			old[oldcount].x = xi;
			old[oldcount].y = yi;
			old[oldcount].width = old[oldcount].height = radius;
			old[oldcount].angle1 = 0;
			old[oldcount].angle2 = 360*64;
			oldcount++;
		}
		new[newcount].x = particle->x;
		new[newcount].y = particle->y;
		new[newcount].width = new[newcount].height = radius;
		new[newcount].angle1 = 0;
		new[newcount].angle2 = 360*64;
		newcount++;
		
	}		
				

	XFillArcs (display, win, rev_gc, old, oldcount);
	XFillArcs (display, win, gc,     new, newcount);
}



int get_rand(int max)
{
	long i = random();
	return (int)((float)max*(float)((float)i/(float)RAND_MAX));
}


/* font.h contains a description of the fonts for the various digits */
#include "font.h"

/* Modify the group of particles in order to make them render a new digit*/
void particle_font (GROUP * group, char *font)
{
	PARTICLE * particles;
	unsigned int i,c,k;
	unsigned int width=FONT_WIDTH,height=FONT_HEIGHT;
	particles = group->particles;
	for (i=0,c=0;i<width*height;i++)
	{
		if (font[i]==1)
		{
			int posx, posy, c1, c2;
			posx = i % width;
			posy = i / width;
			posx = posx - 3 + 1;
			posy = -4 + posy + 1;
			c1 = particles[c].group->x + posx*10;
			c2 = particles[c].group->y + posy*10;
			particles[c].goal_x = c1 - particles[c].group->x;
			particles[c].goal_y = c2 - particles[c].group->y;
			if (++c>=group->size)
			{
				return;
			}
			
		}
	}
	for (k=c-1;c<group->size;c++)
	{
		particles[c].goal_x = particles[k].goal_x;
		particles[c].goal_y = particles[k].goal_y;
	}
}


GROUP * init_particles (PARTICLE * particles, size_t size,int xoffset, int yoffset, GC gc)
{
	unsigned int i;
	GROUP * group;
	group = (GROUP *)malloc(sizeof(GROUP));
	group->particles = particles;
	group->gc = gc;
	group->size = size;
	group->x = xoffset;
	group->y = yoffset;
	group->x = xoffset;
	group->y = yoffset;
	for (i=0;i<size;i++)
	{
		signed int offset;
		int amplitude;
		particles[i].x = particles[i].center_x = get_rand(20)+xoffset;
		particles[i].y = particles[i].center_y = get_rand(20)+yoffset;
		particles[i].goal_x = particles[i].x - xoffset;
		particles[i].goal_y = particles[i].y - yoffset;
		amplitude= get_rand(50)/5+5;
		offset = get_rand(amplitude) - (amplitude/2);
		particles[i].x+=offset;
		offset = get_rand(amplitude) - (amplitude/2);
		particles[i].y += offset;
		particles[i].group = group;
	}
	return group;
}


void print_usage()
{
	printf("%s - HS (June 2003)\n",window_title);
	printf ("usage: xdotclock [options]\n");
	printf ("Options are:\n");
	printf ("	-rv             Reverse Video (black foreground and white background)\n");
	printf ("	-bg  <color>    Background color\n");
	printf ("	-fg  <color>    Foreground color\n");
	printf ("	-bdc <color>    Border color in hex format\n");
	printf ("	-bw  <width>    Border Width\n");
	printf ("	-24             Use a 24 hour clock (default is to use a 12 hour clock)\n");
	printf ("	-radius <val>   Radius of each of the dots\n");
	printf ("	-speed  <val>   The speed with which the dots move\n");
	printf ("	-autospeed      Use a suitable speed for the radius being used\n");
	printf ("	-cycle          Causes the clock to smoothly shift colors\n");
	printf ("	-smooth         Causes the clock's motion to be semi-smooth (recommended)\n");
	printf ("	-h, --help      Print this message\n");
}

/* Horrendous macros to aid in command-line parsing*/
#define parse_arg_val(opt,struct_val); if (strcmp(*argv,opt)==0){\
				if (!(*++argv)){\
         		fprintf(stderr,"An argument should proceed the \"%s\" option\n",\
		    	*(argv-1));\
			    return NULL;}\
				result->struct_val = ((*argv));\
				argv++;\
				continue;}
											
#define parse_arg_int(opt,struct_val); if (strcmp(*argv,opt)==0){\
				if (!(*++argv)){\
         		fprintf(stderr,"An argument should proceed the \"%s\" option\n",\
		    	*(argv-1));\
			    return NULL;}\
				result->struct_val = (atoi(*argv));\
				argv++;\
				continue;}

#define parse_arg_op(opt,code); if (strcmp(*argv,opt)==0){\
				result->code = 1;\
				argv++;\
				continue;}
				
#define undefined() fprintf(stderr,"Unrecognized option:%s\n",*argv);\
					return NULL; 

/* defaults go in here */
OPTIONS * parse_cmdline (int argc, char ** argv)
{
	OPTIONS * result = (OPTIONS *)malloc(sizeof(OPTIONS));
	result->rv        = 0;
	result->fgc       = WHITE;
	result->bgc       = BLACK;
	result->bdc       = WHITE;
	result->bw        = 2;
	result->_24       = 0;
	result->radius    = RADIUS;
	result->speed     = -1;
	result->autospeed = 0;
	result->smooth    = 0;
	result->cycle = 0;
	
	argv++;
	while ((*argv)!=NULL)
	{
		if ((*argv)[0]=='-')
		{
			parse_arg_val ("-bg",  bgc);
			parse_arg_val ("-fg",  fgc);
			parse_arg_val ("-bdc", bdc);
			parse_arg_int ("-bw",  bw);
			parse_arg_int ("-radius", radius);
			parse_arg_int ("-speed", speed);
			parse_arg_op  ("-smooth",smooth);
			parse_arg_op  ("-rv",  rv);
			parse_arg_op  ("-24",  _24);
			parse_arg_op  ("-autospeed", autospeed);
			parse_arg_op  ("-cycle", cycle);
			if (!strcmp(*argv,"-h") || !strcmp(*argv,"--help")) {
				print_usage();
				return NULL;
			}
			undefined();
		}
		else
		{
			undefined();
		}
	}

	if (result->speed > 0)
		speed = result->speed;
	else if (result->autospeed){
		speed = result->radius/4 + 1;
		printf ("Speed: %i\n",speed);
	}

	smooth = result->smooth;
	return result;
}

/* try not to choke on this ugly hack */
void adjust_offset (GROUP ** groups, unsigned int width, unsigned int height)
{
	unsigned int j;
	signed int diff = ((signed int)width - (signed int)prev_width)/2;
	for (j=0;j<DIGIT_COUNT;j++) {
	  groups[j]->y = height/2;
	  groups[j]->x += diff;
	}
}

int main(int argc, char* argv[])
{
	Display* display;
	int screen_num;
	Window win;
	int counter = 0;
	int screen_height, screen_width;
	XEvent an_event;
	size_t pc_s=0;
	GROUP * timer[DIGIT_COUNT];
	int is24 = 0;
	size_t ts = DIGIT_COUNT;
	unsigned int width, height;	
	char *display_name = getenv("DISPLAY");  
	GC rev_gc, gc;
	unsigned int j;
	OPTIONS * options;
	XTextProperty title;
	
	
	options = parse_cmdline (argc, argv);
	if (!options)
		return -1;

	is24 = options->_24;

	
	display = XOpenDisplay(NULL);


	
	if (display == NULL) {
	fprintf(stderr, "%s: cannot connect to X server '%s'\n",
			argv[0], display_name);
	exit(1);
	}
	

	screen_num = DefaultScreen(display);
/* 	display_width = DisplayWidth(display, screen_num);*/
/* 	display_height = DisplayHeight(display, screen_num);*/

	screen_width  = width  = 460;
	screen_height = height =  80;
	prev_width = width;

	if (options->cycle)
		/* init_color_ramp (display,0,0,60000,0,60000,0);*/
		color_ramp_span (display);
	
#define xcolor(a) color_from_str(display,a)
	if ((!options->cycle) && options->rv)
		win = create_simple_window(display, width, height, 0, 0, options->bw,xcolor(options->bdc), xcolor(options->fgc));
	else
		win = create_simple_window(display, width, height, 0, 0, options->bw,xcolor(options->bdc), xcolor(options->bgc));

	XStringListToTextProperty (&window_title, 1, &title);
	XSetWMName (display, win, &title);	
	
	rev_gc = gc_color (display, win, options->bgc);
	gc     = gc_color (display, win, options->fgc);
	
	for (j=0;j<ts;j++)
	{
	  int x,y;
	  PARTICLE * particles;
	  particles = (PARTICLE *)malloc(sizeof(PARTICLE)*CLUSTER_SIZE);
	  x = j*85 + ((j+1)%2)*20 + 5;/* space out the particle groups */
	  y = 35;
	  timer[j] = init_particles (particles, CLUSTER_SIZE, x, y, gc);
	}

	
	XSelectInput(display, win, KeyPressMask| StructureNotifyMask);
  	pc_s = 1;
	
	
	while (1)
	{
		int restart;
		unsigned int c;
		int changed = 0;

		/* cache the time results so we don't need to draw everything in each  
		 * iteration */
		static int hc1=-1,hc2=-1,mc1=-1,mc2=-1,sc1=-1,sc2=-1;
		
		
		my_sleep(1);
		counter+=20;
		if (counter % 100 == 0) {
			time_t t1, t2;
			struct tm * tm1;
			int h1,h2,m1,m2,s1,s2;
			t1 = time(&t2);
			tm1 = localtime(&t1);
			if (!is24 && tm1->tm_hour >= 13) tm1->tm_hour -= 12;
			if (!is24 && tm1->tm_hour == 0)  tm1->tm_hour  = 12;
#define GET_DIGIT(i,j) (j==1)?i/10:i%10
			h1 = GET_DIGIT (tm1->tm_hour,1);
			h2 = GET_DIGIT (tm1->tm_hour,2);
			m1 = GET_DIGIT (tm1->tm_min ,1);
			m2 = GET_DIGIT (tm1->tm_min ,2);
			s1 = GET_DIGIT (tm1->tm_sec ,1);
			s2 = GET_DIGIT (tm1->tm_sec ,2);

			
			/* FIXME:Turns out this cache mechanism is bogus. We always draw everything
			 * because we don't check for parts of the window that are invalid and need
			 * to be redrawn. Otherwise, if the window gets covered and then uncovered
			 * the particles still won't be drawn. Checking for invalidated regions should
			 * speed things up and reduce the memory and CPU consumption (which is higher than
			 * it should be). Currently all the caching does is reduce a few calculations
			 * that need to be done */
			if (h1!=hc1) particle_font (timer[0], numbers[h1]);
			if (h1!=hc2) particle_font (timer[1], numbers[h2]);
			if (h1!=mc1) particle_font (timer[2], numbers[m1]);
			if (h1!=mc2) particle_font (timer[3], numbers[m2]);
			if (h1!=sc1) particle_font (timer[4], numbers[s1]);
			if (h1!=sc2) particle_font (timer[5], numbers[s2]);
			hc1 = h1;
			hc2 = h2;
			mc1 = m1;
			mc2 = m2;
			sc1 = s1;
			sc2 = s2;
		}
		
		if (options->cycle) 		
			color_shift(display, gc);
				
		for (c=0;c<ts;c++) {
			GROUP * g = timer[c];
			if ((!options->cycle) && options->rv)
				draw_particle_h (display, win, rev_gc, gc, g,  options->radius);
			else
				draw_particle_h (display, win, gc, rev_gc, g,  options->radius);
		}
	
		restart = 0;
		changed = 0;

		XSync (display, False);
		XFlush (display);
 		if (XPending (display)) {
			XNextEvent (display, &an_event);
			if (an_event.type == KeyPress) {
				KeySym keysym;
				XComposeStatus status;
				char buffer[10];
				int n = XLookupString (&an_event.xkey, buffer, sizeof(buffer), &keysym, &status);
				buffer[n] = 0;
				if (buffer[0]==32)
					is24 = is24==0?1:0;
			}
			if (an_event.type == ConfigureNotify) {
				screen_height = an_event.xconfigure.height;

				prev_width = screen_width;
				screen_width = an_event.xconfigure.width;
				adjust_offset (timer, screen_width, screen_height);
				restart = 1;
			}
		}
		
	}

	/* free the GCs. */
	XFreeGC(display, rev_gc);
	XFreeGC(display, gc);

	XFree (title.value);

  /* close the connection to the X server. */
	XCloseDisplay(display);
	return 1;
}
