/*****************************************************************************
 ** File          : appl.h                                                  **
 ** Purpose       :                                                         **
 ** Author        : Lionel Mallet                                           **
 ** Date          : 4th May 1992                                            **
 ** Documentation : Xedw Design Folder                                      **
 ** Related Files : iconman.c dirman.c parse.h                              **
 *****************************************************************************/

#ifndef _LM_appl_h
#define _LM_appl_h

#define TERM    (1 << 1)
#define NOTERM  (1 << 2)

#endif /* _LM_appl_h */
