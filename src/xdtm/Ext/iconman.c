/*****************************************************************************
 ** File          : iconman.c                                               **
 ** Purpose       : Icon Application Manager                                **
 ** Author        : Lionel Mallet                                           **
 ** Date          : 12th Feb 1992                                           **
 ** Documentation : Xedw Design Folder                                      **
 ** Related Files : parse.c dirman.c                                        **
 *****************************************************************************/

#include "xdtm.h"
#include "parse.h"
#include "menus.h" /* Get current_mode definition */

#include <stdio.h>

#include "Xedw/XedwList.h"
#include <X11/Xaw/SimpleMenu.h>
#include <X11/Xaw/SmeBSB.h>
#include <X11/Xaw/SmeLine.h>
#define CHAR sizeof(char)

#ifdef HAS_STRSTR
#define mystrstr(cs, ct)	strstr(cs, ct)
#else
extern char *mystrstr(char *, char *);
#endif

/* external and forward functions definitions */
#if NeedFunctionPrototypes
  public String build_args(String);
  public Widget createIconMenu(AppProgram **, Cardinal);
  extern int execute(String, String, String, Boolean, AppProgram *);
  public void ExecuteIconProgram(Widget, AppProgram*, XtPointer);
  extern String getfilename(String);
  public void PopMenu(Widget, XButtonEvent*);
  extern void selection_made(Widget, caddr_t, caddr_t);
  extern void setCursor(Cursor);
#else
  public String build_args();
  public Widget createIconMenu();
  extern int execute();
  public void ExecuteIconProgram();
  extern String getfilename();
  public void PopMenu();
  extern void selection_made();
  extern void setCursor();
#endif

extern XdtmList **icon_list;   /* List with icons and/or filenames */

/*****************************************************************************
 *                              build_args                                   *
 *****************************************************************************/
#if NeedFunctionPrototypes
public String build_args(String program)
#else
public String build_args(program)
     String program;
#endif
{
  /* Given the program with it's previous arguments, this procedure
   * will insert the highlighted file into the place specified by
   * the %[d|b|f] or on the end if none found.
   *       %d refers to the current working directory
   *       %b refers to the basename of the highlighted file
   *       %e refers to the extension of the highlighted file or NULL.
   *       %n refers to the name of the highlighted file (%b.%e).
   *       %f refers to the fullname of the highlighted file (%d[/]%n).
   *
   * - Takes the program string.
   * + Returns the resultant command line.
   */

    extern Widget directoryManager;
    extern String cwd;
    XedwListReturnStruct *list;
    Cardinal i = 0;
    String name, basename, fullname, extension, mark = "%", ext_sep = ".";
    String tmpl = program, ptr, pos, cmd = XtCalloc(1, sizeof(char));
    
    /* Get the list */
    list = XedwListShowCurrent(directoryManager);

    /* Build fullname, basename and extension from path and name */
    name = getfilename(list->string);
    basename = XtNewString(name);
    if ((strcmp(basename, ".") == 0) || (strcmp(basename, "..") == 0)) {
	/* current dir or dot dot dir */
	extension = XtNewString("");
    } else if (basename[0] == '.')  { /* file starting with dot,
					 basename = name,
					 extension = next_occurence_of_dot */
	String basewithoutdot = &basename[1]; /* jump over dot in basename */
	extension = mystrstr(basewithoutdot, ext_sep);
	if (extension) {
	    extension++;
	    basewithoutdot = strtok(basewithoutdot, ext_sep);
	} else extension = XtNewString("");
    }
    else {
	extension = mystrstr(basename, ext_sep);
	if (extension) {
	    extension++;
	    basename = strtok(basename, ext_sep);
	} else extension = XtNewString("");
    }

    fullname = XtMalloc(CHAR * (strlen(name) + strlen(cwd) + 2));
    strcpy(fullname, cwd);
    if (cwd[strlen(cwd) - 1] != '/') strcat(fullname, "/");
    strcat(fullname, name);
    
    while ((pos = mystrstr(tmpl, mark)))
    {
	cmd = XtRealloc(cmd, (pos - tmpl) + CHAR * (i + 1));
	
	for (ptr = tmpl; ptr < pos; ptr++) cmd[i++] = *ptr;
	cmd[i] = 0;
	pos++;
	switch (*pos)
	{
	case 'd':
	    cmd = XtRealloc(cmd, CHAR * (strlen(cmd) + strlen(cwd) + 1));
	    strcat(cmd, cwd); break;
	case 'b':
	    cmd = XtRealloc(cmd, CHAR * (strlen(cmd) + strlen(basename) + 1));
	    strcat(cmd, basename); break;
	case 'e':
	    cmd = XtRealloc(cmd, CHAR * (strlen(cmd) + strlen(extension) + 1));
	    strcat(cmd, extension); break;
	case 'n':
	    cmd = XtRealloc(cmd, CHAR * (strlen(cmd) + strlen(name) + 1));
	    strcat(cmd, name); break;
	case 'f':
	    cmd = XtRealloc(cmd, CHAR * (strlen(cmd) + strlen(fullname) + 1));
	    strcat(cmd, fullname); break;
	default:
	    printf("Wrong argument list in cmd!\n");
	}
	i = strlen(cmd);
	pos++;
	tmpl = pos;
    }
    if (strlen(tmpl)) {
	cmd = XtRealloc(cmd, CHAR * (strlen(cmd) + strlen(tmpl) + 1));
	strcat(cmd, tmpl);
    }
    
    XtFree(basename);
    XtFree(fullname);
    XedwListFreeCurrent(list);
    return(cmd);
}

/*****************************************************************************
 *                              ExecuteIconProgram                           *
 *****************************************************************************/
#if NeedFunctionPrototypes
public void ExecuteIconProgram(Widget w, AppProgram *prog,
			       XtPointer call_data)
#else
public void ExecuteIconProgram(w, prog, call_data)
     Widget w;
     AppProgram *prog;
     XtPointer call_data;
#endif
{
  /* Callback: when an item of the object menu is selected, builds the 
   * command to execute with the template associated to the item, and
   * executes the command.
   */

    extern Cursor busy;
    String pgm, cmd, tmpl = XtNewString(prog->program);

    setCursor(busy);
    cmd = build_args(tmpl);
    pgm = XtNewString(cmd);
    pgm = strtok(pgm, " ");

    if (pgm) execute(NULL, pgm, cmd, False, prog);
    setCursor(NULL);
    XtFree(cmd);
    XtFree(pgm);
    XtFree(tmpl);
}


/*****************************************************************************
 *                              createIconMenu                               *
 *****************************************************************************/
#if NeedFunctionPrototypes
public Widget createIconMenu(AppProgram *proglist[], Cardinal number)
#else
public Widget createIconMenu(proglist, number)
     AppProgram *proglist[];
     Cardinal number;
#endif
{
  /* Given a list of AppProgram stucture pointers, this procedure
   * creates the menu and the menu panes.
   *
   * + Return the menu created.
   */

    extern Widget directoryManager;
    Widget menu;
    Widget menuEntry;
    Cardinal i = 0, n;
    Arg arglist[3];

    XtSetArg(arglist[i], XtNlabel, "Menu");i++;
    menu = XtCreatePopupShell("iconMenu", simpleMenuWidgetClass,
			      directoryManager, arglist, i);
   
    for (n=0; n < number; n++) {
	if (!proglist[n]) { /* a separator line */
	    menuEntry = XtCreateManagedWidget("line", smeLineObjectClass,
					      menu, NULL, 0);
	} else {
	    String label = proglist[n]->string;
	    
	    i = 0;
	    XtSetArg(arglist[i], XtNlabel, label); i++;
	    menuEntry = XtCreateManagedWidget(label, smeBSBObjectClass,
					      menu, arglist, i);
	    
	    XtAddCallback(menuEntry, XtNcallback,
			  (XtCallbackProc)ExecuteIconProgram,
			  (XtPointer) proglist[n]);
	}
    }
    return(menu);
}

/*****************************************************************************
 *                              PopMenu                                      *
 *****************************************************************************/
#if NeedFunctionPrototypes
public void PopMenu(Widget w, XButtonEvent *event)
#else
public void PopMenu(w, event)
     Widget w;
     XButtonEvent *event;
#endif
{
  /* Given the directoryManager and the X Event that triggered the action,
   * selects the current object and pops the menu under mouse cursor.
   */

    XedwListReturnStruct *list;
    iconOps *ops;
    Widget menu;
    
    if (current_mode.mode == Short) return; /* No menus in Short mode */
    
    selection_made(w, (caddr_t)0, (caddr_t)0);
    /* Get the list */
    list = XedwListShowCurrent(w);
    if (list->xedwList_index != XDTM_LIST_NONE) {
	ops = (iconOps *)(icon_list[list->xedwList_index]->user_data);
    
	if (ops && (menu = ops->menu)) {
	    int menu_x, menu_y;
	    Dimension menu_width, menu_height;
	    
	    XtVaGetValues(menu, XtNwidth, &menu_width,
			        XtNheight, &menu_height, NULL);
	    
	    if ((menu_x = event->x_root) >= 0)
	    {
		int scr_width = WidthOfScreen(XtScreen(menu));
		if (menu_x + menu_width > scr_width)
		  menu_x = scr_width - menu_width;
	    }
	    
	    if ((menu_y = event->y_root) >= 0)
	    {
		int scr_height = HeightOfScreen(XtScreen(menu));
		if (menu_y + menu_height > scr_height)
		  menu_y = scr_height - menu_height;
	    }
	    XtVaSetValues(menu, XtNx, (Position)menu_x,
			        XtNy, (Position)menu_y, NULL);
	    XGrabButton( XtDisplay(w), AnyButton, AnyModifier, XtWindow(w),
			TRUE, ButtonPressMask|ButtonReleaseMask,
			GrabModeAsync, GrabModeAsync, None, None );
	    XtPopupSpringLoaded(menu);
	}
    }
    XedwListFreeCurrent(list);
}





