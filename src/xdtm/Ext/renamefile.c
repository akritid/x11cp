/*****************************************************************************
 ** File          : renamefile.c                                            **
 ** Purpose       : Initialise and Realise renamefile dialog, perform rename**
 ** Author        : Lionel Mallet                                           **
 ** Date          : April 1992                                              **
 ** Documentation : Xdtm Design Folder                                      **
 ** Related Files : dialogs.c                                               **
 *****************************************************************************/

#include "xdtm.h"

#include <errno.h>
#include <X11/Shell.h>
#include <X11/Xaw/Label.h>
#include <X11/Xaw/Command.h>
#include <X11/Xaw/AsciiText.h>
#include "Xedw/XedwForm.h"
#include "Xedw/XedwList.h"

extern void realize_dialog(Widget, XtGrabKind);

/* Widgets */

private Widget renamefilepopup;
private Widget renamefileform;
private Widget renamefilelabel;
private Widget renamefiletext;
private Widget renamefileok;
private Widget renamefilecancel;

/*****************************************************************************
 *                             init_renamefile                                  *
 *****************************************************************************/
public void init_renamefile(Widget top)
{
  /* Create Widgets for renamefile dialog */

  private void destroy_renamefile_dialog(Widget, caddr_t, caddr_t);

  Arg arglist[5];
  Cardinal i;
  XtTranslations renamefileTranslations;

  /* Translations for text widget */
  static char defaultTranslations[] = 
    "Ctrl<Key>A:        beginning-of-line() \n\
     Ctrl<Key>E:        end-of-line() \n\
     <Key>Escape:       beginning-of-line() kill-to-end-of-line() \n\
     <Key>Right: 	forward-character() \n\
     <Key>Left:         backward-character() \n\
     <Key>Delete:       delete-previous-character() \n\
     <Key>BackSpace:    delete-previous-character() \n\
     <Key>:             insert-char() \n\
     <FocusIn>:         focus-in() \n\
     <FocusOut>:        focus-out() \n\
     <BtnDown>:         select-start()";
	

  renamefilepopup  = XtCreatePopupShell("renamefilepopup",
					transientShellWidgetClass,
					top,
					NULL, 0);

  renamefileform   = XtCreateManagedWidget("renamefileform",
					   xedwFormWidgetClass,
					   renamefilepopup,
					   NULL, 0);

  /* label widget to prompt for rename filename */

  i = 0;
  XtSetArg(arglist[i], XtNfullWidth,          True); i++;
  XtSetArg(arglist[i], XtNborderWidth,           0); i++;
  XtSetArg(arglist[i], XtNjustify, XtJustifyCenter); i++;
  renamefilelabel  = XtCreateManagedWidget("renamefilelabel",
					   labelWidgetClass,
					   renamefileform,
					   arglist, i);
  
  /* rename filename text input widget */

  i = 1;
  XtSetArg(arglist[i], XtNfromVert, renamefilelabel); i++;
  XtSetArg(arglist[i], XtNeditType, XawtextEdit); i++;
  renamefiletext   = XtCreateManagedWidget("renamefiletext",
					   asciiTextWidgetClass,
					   renamefileform,
					   arglist, i);

  i = 0;
  XtSetArg(arglist[i], XtNfromVert, renamefiletext); i++;
  XtSetArg(arglist[i], XtNlabel,    "Rename"); i++;
  renamefileok    = XtCreateManagedWidget("renamefileok",
					  commandWidgetClass,
					  renamefileform,
					  arglist, i);

  i = 1;
  XtSetArg(arglist[i], XtNfromHoriz,   renamefileok); i++;
  XtSetArg(arglist[i], XtNwidthLinked, renamefileok); i++;
  XtSetArg(arglist[i], XtNlabel,          "Cancel"); i++;
  renamefilecancel = XtCreateManagedWidget("renamefilecancel",
					   commandWidgetClass,
					   renamefileform,
					   arglist, i);

  /* Add callbacks for buttons */
  XtAddCallback(renamefilecancel, XtNcallback,  destroy_renamefile_dialog, 0); 

  /* Do the translations on the text widget */
  XtUninstallTranslations(renamefiletext);
  renamefileTranslations = XtParseTranslationTable(defaultTranslations);
  XtOverrideTranslations(renamefiletext, renamefileTranslations);
}

/*****************************************************************************
 *                             renamefile_dialog                                *
 *****************************************************************************/
public void renamefile_dialog(String oldname)
{
  /* Popup the renamefile dialog on screen with the correct data 
   *
   *   oldname - The name of the file to be renamed.
   */

  private void renamefileQueryReturn(Widget, String, caddr_t);
  Arg arglist[1];
  
  XtSetArg(arglist[0], XtNlabel, "Rename file"); 
  XtSetValues(renamefilelabel, arglist, 1);

  XtSetArg(arglist[0], XtNstring, oldname);
  XtSetValues(renamefiletext, arglist, 1);

  XtAddCallback(renamefileok, XtNcallback, renamefileQueryReturn, oldname);
  realize_dialog(renamefilepopup, XtGrabNonexclusive);
}

/*****************************************************************************
 *                        destroy_renamefile_dialog                             *
 *****************************************************************************/
private void destroy_renamefile_dialog(Widget w, caddr_t dummy1,
				       caddr_t dummy2)
{
  /* Popdown and destroy callbacks for the renamefile dialog. 
   */

  XtPopdown(renamefilepopup);
  XtRemoveAllCallbacks(renamefileok, XtNcallback);
}

/*****************************************************************************
 *                            renamefileQueryReturn                             *
 *****************************************************************************/
private void renamefileQueryReturn(Widget w, String oldname, caddr_t dummy2)
{
  /* This procedure renames a directory or file with the filename
   * in the text widget renamefiletext.
   */

  extern void query_dialog(String, Boolean);
  extern Boolean directoryManagerNewDirectory(String);
  extern String cwd;
  String filename;
  Arg arglist[1];
  
  destroy_renamefile_dialog(w, 0, 0);
  
  XtSetArg(arglist[0], XtNstring, &filename);
  XtGetValues(renamefiletext, arglist, 1);

  if (rename(oldname, filename) != 0)
    query_dialog("Can't rename file!", False);
  else
    directoryManagerNewDirectory(cwd);
}

/*****************************************************************************
 *                              Rename                                       *
 *****************************************************************************/
public void Rename(Widget w, XButtonEvent *event)
{
  /* Given the directoryManager and the X Event that triggered the action,
   * selects the current object and start a rename request.
   */

    extern void selection_made(Widget, caddr_t, caddr_t);
    extern String getfilename(String);
    XedwListReturnStruct *list;
    
    selection_made(w, (caddr_t)0, (caddr_t)0);
    /* Get the list */
    list = XedwListShowCurrent(w);

    /* pop the rename dialog up */
    renamefile_dialog(getfilename(list->string));
}

