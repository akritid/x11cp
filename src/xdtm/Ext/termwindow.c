/*****************************************************************************
 ** File          : termwindow.c                                            **
 ** Purpose       : Pseudo-terminal Window Manager                          **
 ** Author        : Lionel Mallet                                           **
 ** Date          : 28th Apr 1992                                           **
 ** Documentation : Xedw Design Folder                                      **
 ** Related Files : appman.c                                                **
 *****************************************************************************/

/*********************************************************************\
|                                                                     |
|                                                                     |
|  - source code comes from the xdbx application modified by          |
|    Lionel Mallet for xdtm pseudo terminal mode (April 1992).        |
|                                                                     |
|                                                                     |
\*********************************************************************/

/*****************************************************************************
 *
 *  xdbx - X Window System interface to the dbx debugger
 *
 *  Copyright 1989 The University of Texas at Austin
 *  Copyright 1990 Microelectronics and Computer Technology Corporation
 *
 *  Permission to use, copy, modify, and distribute this software and its
 *  documentation for any purpose and without fee is hereby granted,
 *  provided that the above copyright notice appear in all copies and that
 *  both that copyright notice and this permission notice appear in
 *  supporting documentation, and that the name of The University of Texas
 *  and Microelectronics and Computer Technology Corporation (MCC) not be 
 *  used in advertising or publicity pertaining to distribution of
 *  the software without specific, written prior permission.  The
 *  University of Texas and MCC makes no representations about the 
 *  suitability of this software for any purpose.  It is provided "as is" 
 *  without express or implied warranty.
 *
 *  THE UNIVERSITY OF TEXAS AND MCC DISCLAIMS ALL WARRANTIES WITH REGARD TO
 *  THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS, IN NO EVENT SHALL THE UNIVERSITY OF TEXAS OR MCC BE LIABLE FOR
 *  ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 *  RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 *  CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 *  CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *  Author:  	Po Cheung
 *  Created:   	March 10, 1989
 *
 *****************************************************************************/

#include "xdtm.h"

#include <stdio.h>
#include <signal.h>
#include <sys/wait.h>
#include <X11/Xos.h>
#include <X11/Intrinsic.h>
#include <X11/IntrinsicP.h>
#include <X11/StringDefs.h>
#include <X11/keysym.h>
#include <X11/Xaw/Cardinals.h>
#include <X11/Xatom.h>
#include <X11/Shell.h>
#include <X11/Xaw/Label.h>
#include <X11/Xaw/Command.h>
#include <X11/Xaw/AsciiText.h>
#include <X11/Xaw/TextP.h>
#include "Xedw/XedwForm.h"

/* external and forward functions definitions */
#if NeedFunctionPrototypes
  extern void realize_dialog(Widget, Widget, XtGrabKind, XtEventHandler, XtPointer);
  extern void child_died(void);
#else
  extern void realize_dialog();
  extern void child_died();
#endif

typedef struct _ProcessEnv
{
    XtInputId inputId;
    XtIntervalId timeout;
    Widget termWindow;
    int fd;
    int pid;
    FILE *fp;
} ProcessEnv;

static unsigned long timer_interval = 1000;    
static char 	pty[11] = "/dev/pty??";	/* master side of pseudo-terminal */
static char 	tty[11] = "/dev/tty??";	/* slave side of pseudo-terminal */

#if NeedFunctionPrototypes
public int openMasterAndSlave(int *master, int *slave)
#else
public int openMasterAndSlave(master, slave)
     int *master;
     int *slave;
#endif
{
    /* This function opens a master and corresponding slave pseudo-terminals.
     *
     * + returns 0 if it succeeds, 1 otherwise.
     */
#if !defined(ibm) && !defined(SVR4)
    int  i; 
    char c;

    for (c='p'; c<='s'; c++) {
	pty[8] = c;
	for (i=0; i<16; i++) {
	    pty[9] = "0123456789abcdef"[i];
	    if ((*master = open(pty, O_RDWR)) != -1) 
	    {
		tty[8] = pty[8];
		tty[9] = pty[9];
		if ((*slave = open(tty, O_RDWR)) != -1)
		{
		    return 0;
		}
		else close(*master);
	    }
	}
    }
#else
#if defined(SVR4) /* System V R4 multiplexer device */
    extern char *ptsname(int);

#ifdef DEBUG
    fprintf(stderr,"Trying to get pty (SVR4)\n");
#endif
    if ((*master = open("/dev/ptmx", O_RDWR)) >= 0)
    {
	/* disable SIGCHLD, because grantpt() calls a setuid program to
	   change the slave tty ownership */
	signal(SIGCHLD, SIG_DFL); /* SVR4 is POSIX, so we use SIGCHLD */
        grantpt (*master);
	signal(SIGCHLD, child_died);
        unlockpt (*master);
#ifdef DEBUG
        fprintf(stderr,"name of slave tty: %s\n", ptsname(*master));
#endif
	if ((*slave = open(ptsname(*master), O_RDWR)) >= 0)
	{
            return 0;
        }
        else perror("Error opening tty slave");

    }
    else perror("Error opening tty master");

#else
    if ((*master = open("/dev/ptc", O_RDWR)) != -1)
    {
	if ((*slave = open(ttyname(*master), O_RDWR)) != -1)
	{
	    return 0;
	}
	else close(*master);
    }
#endif
#endif
    fprintf(stderr, "Xdtm: all ptys in use\n");
    return 1;
}


#if NeedFunctionPrototypes
private void SendInput(Widget w, ProcessEnv *env, XtPointer call_data)
#else
private void SendInput(w, env, call_data)
     Widget w;
     ProcessEnv *env;
     XtPointer call_data;
#endif
{
    /* This callback sends an input line to a file pointer. It is activated by
     * a CR in the text widget of the pseudo-terminal window.
     *
     * - Takes the widget into which input is read,
     *             process environment,
     *             call_data of the callback.
     */
    Widget termWindowInput;
    Arg arglist[2];
    FILE *fp = env->fp;
    String text;
    int len;

    if ((termWindowInput = XtNameToWidget(XtParent(w), "*termWindowInput")))
    {
	XtSetArg(arglist[0], XtNstring, &text);
	XtGetValues(termWindowInput, arglist, 1);
	
	len = strlen(text);
	fputs(text, fp);
	if (text && ((len < 0) || (text[len - 1] != '\n')))
	  fputs("\n", fp); /* send a CR if not in text */
	fflush(fp);

	if (text && (len > 0))
	{
	    XtSetArg(arglist[0], XtNstring, "");
	    XtSetValues(termWindowInput, arglist, 1);
	}
    }
}

#if NeedFunctionPrototypes
private void CarriageReturn(Widget w, XEvent *event,
			    String *params, Cardinal *num_params)
#else
private void CarriageReturn(w, event, params, num_params)
    Widget w;
    XEvent *event;
    String *params;
    Cardinal *num_params;
#endif
{
    /* This action activates the SendInput callback on CR or LF in the
     * input text widget of the pseudo-terminal window.
     *
     * - Takes the input text widget into which CR or LF has occured,
     *             XEvent struct,
     *             parameters of the action,
     *             number of parameters of the action.
     */
    Widget termWindowInputOK;

    if ((termWindowInputOK = XtNameToWidget(XtParent(w),"*termWindowInputOK")))
      XtCallCallbacks(termWindowInputOK, XtNcallback, NULL);
}

#if NeedFunctionPrototypes
private void DisplayMessage(ProcessEnv *env, char *buf, int nbytes)
#else
private void DisplayMessage(env, buf, nbytes)
     ProcessEnv *env;
     char *buf;
     int nbytes;
#endif
{
    /* Display contents of buf in pseudo-terminal window.
     *
     * - Takes the process environment,
     *             buffer to display,
     *             number of bytes in the buffer.
     */
    Widget w = env->termWindow;
    Widget termWindowText;
    int i;
    Arg arglist[2];
    
    if ((termWindowText = XtNameToWidget(w, "*termWindowText")))
    {
	XawTextBlock block;
	TextWidget ctx = (TextWidget) termWindowText;
	XawTextPosition lastPos = ctx->text.lastPos;
	block.firstPos = 0;
	block.length = nbytes;
	block.ptr = buf;
	
	i = 0;
	XtSetArg(arglist[i], XtNeditType, XawtextAppend); i++;
	XtSetValues(termWindowText, arglist, i);
	XawTextReplace(termWindowText, lastPos, lastPos, &block);
	i = 0;
	XtSetArg(arglist[i], XtNeditType, XawtextRead); i++;
	XtSetValues(termWindowText, arglist, i);
	
	/* scroll to end of text */
	XawTextSetInsertionPoint(termWindowText, ctx->text.lastPos);
    }
}

#if NeedFunctionPrototypes
private void ProcessIsFinished(ProcessEnv *env)
#else
private void ProcessIsFinished(env)
     ProcessEnv *env;
#endif
{
    /* Operate on pseudo-terminal window to inform that process is finished
     * for any reason
     *
     * - Takes the process environement.
     */
    static char *buf = "\n*** Terminated. ***\n";
    int nbytes;
    Widget w = env->termWindow;
    
    if (env->timeout) XtRemoveTimeOut(env->timeout);
    XtRemoveInput(env->inputId);
    
    /* process has now completed: change state of buttons and input text */
    XtSetSensitive(XtNameToWidget(w, "*termWindowQuit"), True);
    XtSetSensitive(XtNameToWidget(w, "*termWindowIntr"), False);
    XtSetSensitive(XtNameToWidget(w, "*termWindowInput"), False);
    XtSetSensitive(XtNameToWidget(w, "*termWindowInputOK"), False);
    nbytes = strlen(buf) + 1;
    DisplayMessage(env, buf, nbytes);

    /* Ibm doesn't like closing both sides of the pty, so we don't, hoping
       it will not later affect pty reuse as on Sun */
#ifndef ibm
    fclose(env->fp);
    close(env->fd);
#endif
    XtFree((char *)env);
}

#if NeedFunctionPrototypes
private void ReadStdout(ProcessEnv *env, int *fd, XtInputId *id)
#else
private void ReadStdout(env, fd, id)
     ProcessEnv *env;
     int *fd;
     XtInputId *id;
#endif
{
    /* This callback tries to read on the process stdout or stderr, and
     * either displays any text read or tells the process is terminated.
     *
     * - Takes the process environment,
     *             file descriptor where to read messages,
     *             Id of the XInput registered.
     */
    char buf[BUFSIZ];
    int nbytes;

    if (((nbytes = read(*fd, buf, BUFSIZ)) == -1) || (nbytes == 0))
      ProcessIsFinished(env);
    else DisplayMessage(env, buf, nbytes);
}

#if NeedFunctionPrototypes
private void CheckProcess(ProcessEnv *env, XtIntervalId *id)
#else
private void CheckProcess(env, id)
     ProcessEnv *env;
     XtIntervalId *id;
#endif
{
    int processStatus, waitStatus;

    env->timeout = 0;
    waitStatus = waitpid(env->pid, &processStatus, WNOHANG);

    if (waitStatus != 0) /* Process is terminated */
      ProcessIsFinished(env);
    else /* Reactivate timer */
      env->timeout = XtAppAddTimeOut(
				 XtWidgetToApplicationContext(env->termWindow),
				 timer_interval,
				 (XtTimerCallbackProc)CheckProcess,
				 (XtPointer)env);
}

#if NeedFunctionPrototypes
private void KillProcess(Widget w, ProcessEnv *env, XtPointer call_data)
#else
private void KillProcess(w, env, call_data)
     Widget w;
     ProcessEnv *env;
     XtPointer call_data;
#endif
{
    /* This callback kills the process attached to the pseudo-terminal window.
     *
     * - Takes the widget button that triggers the callback,
     *             the process environement of the process to be killed,
     *             call_data of the callback.
     */

    /* due to make behaviour, we need to kill all processes that belong
       to the process group of the started process (i.e., make) */
    kill(-(env->pid), SIGINT); /* -pid sends to the process group processes */

    /* notify process is finished */
    ProcessIsFinished(env);
}

#if NeedFunctionPrototypes
private void CloseTerm(Widget w, XtPointer client_data, XtPointer call_data)
#else
private void CloseTerm(w, client_data, call_data)
     Widget w;
     XtPointer client_data;
     XtPointer call_data;
#endif
{
    /* This callback closes a pseudo-terminal window. It removes the registered
     * input for the application.
     *
     * - Takes the widget button that closes the pseudo-terminal window,
     *             the input id to remove,
     *             call_data of the callback.
     */
    XtPopdown(XtParent(XtParent(w)));
    XtDestroyWidget(XtParent(XtParent(w)));
}

#if NeedFunctionPrototypes
private void WMCloseTerm(Widget w, XtPointer client_data,
			 XEvent *event, Boolean *dispatch)
#else
private void WMCloseTerm(w, client_data, event, dispatch)
     Widget w;
     XtPointer client_data;
     XEvent *event;
     Boolean *dispatch;
#endif
{
    extern Atom protocols[2]; /* defined in dialogs.c */

    if (event->xclient.message_type == protocols[1] &&
	event->xclient.data.l[0] == protocols[0])
    /* the widget got a kill signal */
    {
	Widget termWindowQuit = XtNameToWidget(w, "*termWindowQuit");
	Widget termWindowIntr = XtNameToWidget(w, "*termWindowIntr");

	if (XtIsSensitive(termWindowIntr))
	  /* process is not finished, kill it */
	  KillProcess(termWindowIntr, client_data, NULL);
	CloseTerm(termWindowQuit, client_data, NULL);
    }
}

#if NeedFunctionPrototypes
public void AttachProcessToTermWindow(Widget father, String fullname,
				      FILE *fp, int fd, int pid)
#else
public void AttachProcessToTermWindow(father, fullname, fp, fd, pid)
     Widget father;
     String fullname;
     FILE *fp;
     int fd;
     int pid;
#endif
{
    /* This procedure creates a pseudo-terminal window and attaches the
     * sub-process to it.
     *
     * - Takes the father of the pseudo-terminal window,
     *             fullname of the process,
     *             file pointer of sub-process stdin,
     *             file descriptor of sub-process stdout and stderr,
     *             sub-process pid.
     */
    Widget termWindow, termWindowForm, termWindowLabel, termWindowText;
    Widget termWindowInput, termWindowInputOK, termWindowIntr, termWindowQuit;
    XFontStruct *font;
    Arg arglist[12];
    Cardinal i, width, height;
    ProcessEnv *env = (ProcessEnv *)XtCalloc(1, sizeof(ProcessEnv));
    
    private String translations = "#override\n\
        <Key>Escape:    beginning-of-line() kill-to-end-of-line() \n\
 	<Key>Return:	CR()\n\
        <Key>Linefeed:  CR()\n\
    ";
    private XtActionsRec actions[] = {
	{"CR", 	(XtActionProc) CarriageReturn},
        {NULL, NULL}
    };

    /* Create popup */
    termWindow  = XtCreatePopupShell("termWindow",
				     topLevelShellWidgetClass,
				     father,
				     NULL, 0);
    
    termWindowForm   = XtCreateManagedWidget("termWindowForm",
					     xedwFormWidgetClass,
					     termWindow,
					     NULL, 0);
    i = 0;
    XtSetArg(arglist[i], XtNjustify, XtJustifyCenter); i++;
    XtSetArg(arglist[i], XtNrubberHeight,      False); i++;
    XtSetArg(arglist[i], XtNfullWidth,          True); i++;
    XtSetArg(arglist[i], XtNlabel,          fullname); i++;
    termWindowLabel  = XtCreateManagedWidget("termWindowLabel",
					     labelWidgetClass,
					     termWindowForm,
					     arglist, i);
    
    i = 0;
    XtSetArg(arglist[i], XtNfont,            app_data.term_font); i++;
    XtSetArg(arglist[i], XtNfullWidth,                     True); i++;
    XtSetArg(arglist[i], XtNresizable,                     True); i++;
    XtSetArg(arglist[i], XtNeditType,               XawtextRead); i++;
    XtSetArg(arglist[i], XtNdisplayCaret,                 False); i++;
    XtSetArg(arglist[i], XtNdisplayNonprinting,           False); i++;
    XtSetArg(arglist[i], XtNlength,                 10 * BUFSIZ); i++;
    XtSetArg(arglist[i], XtNfromVert,           termWindowLabel); i++;
    XtSetArg(arglist[i], XtNscrollVertical, XawtextScrollAlways); i++; 
    XtSetArg(arglist[i], XtNtype,                XawAsciiString); i++;
    XtSetArg(arglist[i], XtNstring,                          ""); i++;
    termWindowText   = XtCreateManagedWidget("termWindowText",
					     asciiTextWidgetClass,
					     termWindowForm,
					     arglist, i);
    
    /* Get font from text widget, set the width of the widget to the
     * maximum width of any character in that font multiplied by the
     * value in the application defaults (default 85),
     * set the height of the widget to (ascent + descent) * value in the
     * application defaults (default 10).
     * 
     * This gives a default viewing area of 85x10 characters if using a fixed 
     * width font, wider if using a variable width font.
     *
     * Note: The user may override these sizes via the application resources.
     */
    
    XtSetArg(arglist[0], XtNfont, &font);
    XtGetValues(termWindowText, arglist, 1);
    
    width =  (font->max_bounds.width * app_data.term_width);
    height = ((font->max_bounds.ascent +
	       font->max_bounds.descent) * app_data.term_height);
    
    i = 0;
    XtSetArg(arglist[i], XtNwidth,   width); i++;
    XtSetValues(termWindowLabel, arglist, i);
    XtSetArg(arglist[i], XtNheight, height); i++;
    XtSetValues(termWindowText, arglist, i);

    i = 0;
    XtSetArg(arglist[i], XtNrubberHeight,        False); i++;
    XtSetArg(arglist[i], XtNjustify,   XtJustifyCenter); i++;
    XtSetArg(arglist[i], XtNfromVert,   termWindowText); i++;
    termWindowInputOK   = XtCreateManagedWidget("termWindowInputOK",
						commandWidgetClass,
						termWindowForm,
						arglist, i);
    
    i = 0;
    XtSetArg(arglist[i], XtNfont,            app_data.term_font); i++;
    XtSetArg(arglist[i], XtNrubberHeight,                 False); i++;
    XtSetArg(arglist[i], XtNfullWidth,                     True); i++;
    XtSetArg(arglist[i], XtNeditType,               XawtextEdit); i++;
    XtSetArg(arglist[i], XtNfromVert,            termWindowText); i++;
    XtSetArg(arglist[i], XtNfromHoriz,        termWindowInputOK); i++;
    XtSetArg(arglist[i], XtNtype,                XawAsciiString); i++;
    XtSetArg(arglist[i], XtNstring,                          ""); i++;
    termWindowInput   = XtCreateManagedWidget("termWindowInput",
					      asciiTextWidgetClass,
					      termWindowForm,
					      arglist, i);
    XtOverrideTranslations(termWindowInput,
			   XtParseTranslationTable(translations));
    XtAppAddActions(XtWidgetToApplicationContext(father),
		    actions, XtNumber(actions));

    i = 0;
    XtSetArg(arglist[i], XtNrubberHeight,       False); i++;
    XtSetArg(arglist[i], XtNjustify,  XtJustifyCenter); i++;
    XtSetArg(arglist[i], XtNfromVert, termWindowInput); i++;
    termWindowIntr   = XtCreateManagedWidget("termWindowIntr",
					     commandWidgetClass,
					     termWindowForm,
					     arglist, i);
    
    i = 0;
    XtSetArg(arglist[i], XtNrubberHeight,       False); i++;
    XtSetArg(arglist[i], XtNjustify,  XtJustifyCenter); i++;
    XtSetArg(arglist[i], XtNfromVert, termWindowInput); i++;
    XtSetArg(arglist[i], XtNfromHoriz, termWindowIntr); i++;
    termWindowQuit   = XtCreateManagedWidget("termWindowQuit",
					     commandWidgetClass,
					     termWindowForm,
					     arglist, i);
    /* deactivate Quit until process has completed */
    XtSetSensitive(termWindowQuit, False);

    env->termWindow = termWindow;
    env->fd = fd;
    env->pid = pid;
    env->fp = fp;
    
    env->inputId = XtAppAddInput(XtWidgetToApplicationContext(father), fd,
				 (XtPointer)XtInputReadMask,
				 (XtInputCallbackProc)ReadStdout,
				 (XtPointer)env);
    XtAddCallback(termWindowInputOK, XtNcallback,
		  (XtCallbackProc)SendInput, env);
    XtAddCallback(termWindowQuit, XtNcallback,
		  (XtCallbackProc)CloseTerm, NULL);
    XtAddCallback(termWindowIntr, XtNcallback,
		  (XtCallbackProc)KillProcess, env);

    realize_dialog(termWindow, NULL, XtGrabNone,
		   (XtEventHandler)WMCloseTerm, (XtPointer)env);

    /* Activate timer to check out process status */
    env->timeout = XtAppAddTimeOut(XtWidgetToApplicationContext(father),
				   timer_interval,
				   (XtTimerCallbackProc)CheckProcess,
				   (XtPointer)env);
}   
 


