#ifndef _XedwForm_h
#define _XedwForm_h

#include <X11/Constraint.h>
#include <X11/Xaw/Form.h>

/***********************************************************************
 *
 * XedwForm Widget
 *
 ***********************************************************************/

/* Parameters:

 Name               Class               RepType     Default Value
 ----               -----               -------     -------------
 background         Background          Pixel       XtDefaultBackground
 border             BorderColor         Pixel       XtDefaultForeground
 borderWidth        BorderWidth         Dimension   1
 defaultDistance    Thickness           int         4
 destroyCallback    Callback            Pointer     NULL
 height             Height              Dimension   computed at realize
 mappedWhenManaged  MappedWhenManaged   Boolean     True
 sensitive          Sensitive           Boolean     True
 width              Width               Dimension   computed at realize
 x                  Position            Position    0
 y                  Position            Position    0

*/

/* Constraint parameters:

 Name               Class               RepType     Default Value
 ----               -----               -------     -------------
 bottom             Edge                XtEdgeType  XtRubber
 fromHoriz          Widget              Widget      (left edge of xedwForm)
 fromVert           Widget              Widget      (top of xedwForm)
 fullHeight         Boolean             Boolean     False
 fullWidth          Boolean             Boolean     False
 horizDistance      Thickness           int         defaultDistance
 left               Edge                XtEdgeType  XtRubber
 resizable          Boolean             Boolean     False
 right              Edge                XtEdgeType  XtRubber
 rubberHeight       Boolean             Boolean     True
 rubberWidth        Boolean             Boolean     True
 top                Edge                XtEdgeType  XtRubber
 vertDistance       Thickness           int         defaultDistance
 widthLinked        Widget              Widget      NULL
 heightLinked       Widget              Widget      NULL

*/



#define XtNfullHeight "fullHeight"
#define XtNfullWidth "fullWidth"
#define XtNrubberHeight "rubberHeight"
#define XtNrubberWidth "rubberWidth"
#define XtNwidthLinked "widthLinked"
#define XtNheightLinked "heightLinked"

typedef struct _XedwFormClassRec    *XedwFormWidgetClass;
typedef struct _XedwFormRec     *XedwFormWidget;

extern WidgetClass xedwFormWidgetClass;

extern void XedwFormDoLayout(
#if NeedFunctionPrototypes
Widget, Boolean
#endif
);     /* Boolean doit */


#endif /* _XedwForm_h */
