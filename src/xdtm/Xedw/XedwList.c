/*
 * XedwList.c - XedwList widget
 *
 * This is the XedwList widget. It is very similar to the Athena Widget List,
 * except it has the ability to display an icon with the string. Plus allows
 * multiple selections.
 * It allows the user to select one or more items in a list and notifies the
 * application through a callback function.
 *
 *  List Created:   8/13/88
 *  By:     Chris D. Peterson
 *                      MIT X Consortium
 *
 *  Modified to XedwList: 1/26/91
 *  By:     Edward Groenendaal
 *                      University of Sussex, UK.
 */

#include <stdio.h>
#include <memory.h>
#include <ctype.h>

#include <X11/IntrinsicP.h>
#include <X11/StringDefs.h>
#ifdef XPM
#include "xpm.h"
#endif
#include <X11/Xmu/Drawing.h>
#include <X11/Xaw/XawInit.h>
#include <X11/Xatom.h>
#include <X11/Xmu/Atoms.h>
#include <X11/Xmu/StdSel.h>
#include <X11/Xaw/Cardinals.h>

#include "XedwListP.h"

/*
 * Include bitmap for the default icon
 */

#ifdef XPM
#include "DefIcon.xpm"
#else
#include "DefIcon.xbm"
#endif


/*
 * Default Translation table.
 */

static char defaultTranslations[] =
  "!Shift<Btn1Down>: 	        MultipleSet()\n\
   <Btn1Down>:   	        Set()\n\
   !Shift<Btn1Down>,<Btn1Up>:   Notify()\n\
   <Btn1Down>,<Btn1Up>:     	Notify()";

/****************************************************************
 *
 * Full class record constant
 *
 ****************************************************************/

/* Private Data */

#define offset(field) XtOffset(XedwListWidget, field)

static XtResource resources[] = {
  {XtNforeground, XtCForeground, XtRPixel, sizeof(Pixel),
     offset(xedwList.foreground), XtRString, "XtDefaultForeground"},
  {XtNcursor, XtCCursor, XtRCursor, sizeof(Cursor),
     offset(simple.cursor), XtRString, "left_ptr"},
  {XtNfont,  XtCFont, XtRFontStruct, sizeof(XFontStruct *),
     offset(xedwList.font),XtRString, "XtDefaultFont"},
  {XtNxedwList, XtCXedwList, XtRPointer, sizeof(XedwList **), /* XedwList */
     offset(xedwList.xedwList), XtRString, NULL},
  {XtNdefaultColumns, XtCColumns, XtRInt,  sizeof(int),
     offset(xedwList.default_cols), XtRImmediate, (caddr_t)2},
  {XtNlongest, XtCLongest, XtRInt,  sizeof(int),
     offset(xedwList.longest), XtRImmediate, (caddr_t)0},
  {XtNnumberStrings, XtCNumberStrings, XtRInt,  sizeof(int),
     offset(xedwList.nitems), XtRImmediate, (caddr_t)0},
  {XtNpasteBuffer, XtCBoolean, XtRBoolean,  sizeof(Boolean),
     offset(xedwList.paste), XtRString, (caddr_t) "False"},
  {XtNforceColumns, XtCColumns, XtRBoolean,  sizeof(Boolean),
     offset(xedwList.force_cols), XtRString, (caddr_t) "False"},
  {XtNverticalList, XtCBoolean, XtRBoolean,  sizeof(Boolean),
     offset(xedwList.vertical_cols), XtRString, (caddr_t) "False"},
  {XtNinternalWidth, XtCWidth, XtRDimension,  sizeof(Dimension),
     offset(xedwList.internal_width), XtRImmediate, (caddr_t)4},
  {XtNinternalHeight, XtCHeight, XtRDimension, sizeof(Dimension),
     offset(xedwList.internal_height), XtRImmediate, (caddr_t)2},
  {XtNcolumnSpacing, XtCSpacing, XtRDimension,  sizeof(Dimension),
     offset(xedwList.column_space), XtRImmediate, (caddr_t)10},
  {XtNrowSpacing, XtCSpacing, XtRDimension,  sizeof(Dimension),
     offset(xedwList.row_space), XtRImmediate, (caddr_t)10},
  {XtNiconWidth, XtCWidth, XtRDimension, sizeof(Dimension),    /* IconWidth */
     offset(xedwList.icon_width), XtRImmediate, (caddr_t)32},
  {XtNiconHeight, XtCHeight, XtRDimension, sizeof(Dimension),  /* IconHeight */
     offset(xedwList.icon_height), XtRImmediate, (caddr_t)32},
  {XtNshowIcons, XtCBoolean, XtRBoolean, sizeof(Boolean),      /* ShowIcons */
     offset(xedwList.show_icons), XtRString, (caddr_t) "False"},
  {XtNmSelections, XtCBoolean, XtRBoolean, sizeof(Boolean),    /* Multiple   */
     offset(xedwList.multiple), XtRString, (caddr_t) "False"}, /* Selections */
  {XtNdefaultIcon, XtCPixmap, XtRPixmap, sizeof(Pixmap),      /* DefaultIcon */
     offset(xedwList.default_icon), XtRPixmap, (caddr_t) NULL},
  {XtNcallback, XtCCallback, XtRCallback, sizeof(caddr_t),
     offset(xedwList.callback), XtRCallback, NULL},
};

#if NeedFunctionPrototypes
static void             Initialize   (Widget, Widget, ArgList, Cardinal*);
static void             ChangeSize   (Widget, Dimension, Dimension);
static void             Resize       (Widget);
static void             Redisplay    (Widget, XEvent*, Region);
static Boolean          Layout       (Widget, Boolean, Boolean, Dimension*, Dimension*);
static XtGeometryResult PreferredGeom(Widget, XtWidgetGeometry*, XtWidgetGeometry*);
static Boolean          SetValues    (Widget, Widget, Widget, ArgList, Cardinal*);
static void             Notify       (Widget, XEvent*, String*, Cardinal*),
                        Set          (Widget, XEvent*, String*, Cardinal*), 
                        MultipleSet  (Widget, XEvent*, String*, Cardinal*), 
                        Unset        (Widget, XEvent*, String*, Cardinal*);
static Boolean 		IsHighlighted(Widget, int);
static void		AddNode	     (Widget, int);
static void		RemoveNode   (Widget, int);
static void             selection_set_buffer(Widget, String);
#else
static void             Initialize   ();
static void             ChangeSize   ();
static void             Resize       ();
static void             Redisplay    ();
static Boolean          Layout       ();
static XtGeometryResult PreferredGeom();
static Boolean          SetValues    ();
static void             Notify       (),
                        Set          (), 
                        MultipleSet  (), 
                        Unset        ();
static Boolean 		IsHighlighted();
static void		AddNode	     ();
static void		RemoveNode   ();
static void             selection_set_buffer();
#endif

static XtActionsRec actions[] = {
  {"Notify",         Notify},
  {"Set",            Set},
  {"Unset",          Unset},
  {"MultipleSet",    MultipleSet},
  {NULL,NULL}
};

XedwListClassRec xedwListClassRec = {
  {
/* core_class fields */
#define superclass                  (&simpleClassRec)
    /* superclass               */  (WidgetClass) superclass,
    /* class_name               */  "XedwList",
    /* widget_size              */  sizeof(XedwListRec),
    /* class_initialize         */  XawInitializeWidgetSet,
    /* class_part_initialize    */  NULL,
    /* class_inited             */  FALSE,
    /* initialize               */  (XtInitProc)Initialize,
    /* initialize_hook          */  NULL,
    /* realize                  */  XtInheritRealize,
    /* actions                  */  actions,
    /* num_actions              */  XtNumber(actions),
    /* resources                */  resources,
    /* num_resources            */  XtNumber(resources),
    /* xrm_class                */  NULLQUARK,
    /* compress_motion          */  TRUE,
    /* compress_exposure        */  FALSE,
    /* compress_enterleave      */  TRUE,
    /* visible_interest         */  FALSE,
    /* destroy                  */  NULL,
    /* resize                   */  (XtWidgetProc)Resize,
    /* expose                   */  (XtExposeProc)Redisplay,
    /* set_values               */  (XtSetValuesFunc)SetValues,
    /* set_values_hook          */  NULL,
    /* set_values_almost        */  XtInheritSetValuesAlmost,
    /* get_values_hook          */  NULL,
    /* accept_focus             */  NULL,
    /* version                  */  XtVersion,
    /* callback_private         */  NULL,
    /* tm_table                 */  defaultTranslations,
    /* query_geometry           */  PreferredGeom,
  },
/* Simple class fields initialization */
  {
    /* change_sensitive     */  XtInheritChangeSensitive
  }
};

WidgetClass xedwListWidgetClass = (WidgetClass)&xedwListClassRec;

/*
 * I know that this is the wrong place for this variable
 * but it avoided either an XCreateBitmapFromData or an
 * XpmCreatePixmapFromData for each default icon.
 */

static Pixmap defaultIconPixmap = (Pixmap) NULL ;
static Pixmap defaultIconPixmapMask = (Pixmap) NULL ;
static GC maskgc = (GC) NULL;

/****************************************************************
 *
 * Private Procedures
 *
 ****************************************************************/

#if NeedFunctionPrototypes
static void GetGCs(Widget w)
#else
static void GetGCs(w)
Widget w;
#endif
{

  XGCValues   values;
  XedwListWidget ilw = (XedwListWidget) w;

  if (maskgc == (GC) NULL) {
      values.foreground   = (Pixel)0;
      values.background   = (Pixel)~0;
      values.graphics_exposures = False;
#ifdef NO_CLIP
      maskgc = XtGetGC(w,
		  (unsigned) GCBackground | GCForeground | GCGraphicsExposures,
		  &values);
#else
      values.function = GXcopy;
      maskgc = XtGetGC(w,
		  (unsigned) GCBackground | GCForeground |
		       GCFunction | GCGraphicsExposures,
		  &values);
#endif
  }
  
  values.foreground   = ilw->xedwList.foreground;
  values.background   = ilw->core.background_pixel; 
  values.font         = ilw->xedwList.font->fid;
  values.graphics_exposures = False;
  ilw->xedwList.normgc = XtGetGC(w, (unsigned) GCBackground | GCForeground | GCFont | GCGraphicsExposures, &values);

  values.foreground   = ilw->core.background_pixel; 
  values.background   = ilw->xedwList.foreground;
  ilw->xedwList.revgc = XtGetGC(w, (unsigned) GCBackground | GCForeground | GCFont | GCGraphicsExposures, &values);

  values.tile       = XmuCreateStippledPixmap(XtScreen(w),
					      ilw->xedwList.foreground,
					      ilw->core.background_pixel,
					      ilw->core.depth);
  values.fill_style = FillTiled;

  ilw->xedwList.graygc = XtGetGC(w, (unsigned) GCFont | GCTile | GCFillStyle,
				 &values);
}

/*  Function Name:  ResetXedwList
 *  Description:    Resets the new xedwList when important things change.
 *  Arguments:      w - the widget.
 *                  changex, changey - allow the height or width to change?
 *  Returns: none.
 */

#if NeedFunctionPrototypes
static void ResetXedwList(Widget w, Boolean changex, Boolean changey)
#else
static void ResetXedwList(w, changex, changey)
Widget w;
Boolean changex, changey;
#endif
{
  XedwListWidget ilw = (XedwListWidget) w;
  Dimension width = w->core.width;
  Dimension height = w->core.height;
  register int i, len;

  /*
   * If xedwList is NULL then the xedwList will just be the name of the widget,
   * the icon will be set to the default icon. (defined in XedwListP.h)
   */

  if (ilw->xedwList.xedwList == NULL) {
    /* Make room for structure */
    ilw->xedwList.xedwList = XtNew(XedwList *);
    ilw->xedwList.xedwList[0] = XtNew(XedwList);
    ilw->xedwList.xedwList[0]->string = ilw->core.name;
    ilw->xedwList.xedwList[0]->icon = (Pixmap)NULL;
    ilw->xedwList.nitems = 1;
  }

  if (ilw->xedwList.nitems == 0)       /* Get number of items. */
    for ( ; ilw->xedwList.xedwList[ilw->xedwList.nitems] != NULL ; ilw->xedwList.nitems++);
  
  if (ilw->xedwList.longest == 0) /* Get column width. */
    for ( i = 0 ; i < ilw->xedwList.nitems; i++) {
      len = XTextWidth(ilw->xedwList.font, ilw->xedwList.xedwList[i]->string,
		       strlen(ilw->xedwList.xedwList[i]->string));
      if (len > ilw->xedwList.longest)
	ilw->xedwList.longest = len;
    }

  /* If longest string is less than the width of a bitmap then the longest is
   * the width of a bitmap
   */
  if (ilw->xedwList.show_icons)
    if (ilw->xedwList.longest < ilw->xedwList.icon_width)
      ilw->xedwList.longest = ilw->xedwList.icon_width;


  ilw->xedwList.col_width = ilw->xedwList.longest + ilw->xedwList.column_space;

  if (Layout(w, changex, changey, &width, &height))
    ChangeSize(w, width, height);
}

/*  Function Name: ChangeSize.
 *  Description: Laysout the widget.
 *  Arguments: w - the widget to try change the size of.
 *  Returns: none.
 */

#if NeedFunctionPrototypes
static void ChangeSize(Widget w, Dimension width, Dimension height)
#else
static void ChangeSize(w, width, height)
Widget w;
Dimension width, height;
#endif
{
  XtWidgetGeometry request, reply;

  request.request_mode = CWWidth | CWHeight;
  request.width = width;
  request.height = height;

  switch ( XtMakeGeometryRequest(w, &request, &reply) ) {
  case XtGeometryYes:
  case XtGeometryNo:
    break;
  case XtGeometryAlmost:
    Layout(w, (request.height != reply.height),
	   (request.width != reply.width),
           &(reply.width), &(reply.height));
    request = reply;
    switch (XtMakeGeometryRequest(w, &request, &reply) ) {
    case XtGeometryYes:
    case XtGeometryNo:
      break;
    case XtGeometryAlmost:
      request = reply;
      if (Layout(w, FALSE, FALSE,
		 &(request.width), &(request.height))) {
	char buf[BUFSIZ];
	sprintf(buf, "XedwList Widget: %s %s",
		"Size Changed when it shouldn't have",
		"when computing layout");
	XtAppWarning(XtWidgetToApplicationContext(w), buf);
      }
      request.request_mode = CWWidth | CWHeight;
      XtMakeGeometryRequest(w, &request, &reply);
      break;
    default:
      XtAppWarning(XtWidgetToApplicationContext(w),
		   "XedwList Widget: Unknown geometry return.");
      break;
    }
    break;
  default:
    XtAppWarning(XtWidgetToApplicationContext(w),
		 "XedwList Widget: Unknown geometry return.");
    break;
  }
}

/*  Function Name: Initialise
 *  Description: Function that initilises the widget instance.
 *  Arguments: junk - NOT USED.
 *             new  - the new widget.
 *  Returns: none
 */

#if NeedFunctionPrototypes
static void Initialize(Widget junk, Widget new, ArgList args, Cardinal *num_args)
#else
static void Initialize(junk, new, args, num_args)
Widget junk, new;
ArgList args;
Cardinal *num_args;
#endif
{
  XedwListWidget ilw = (XedwListWidget) new;
  Widget w = junk;
  Display *dpy = XtDisplay(w);
  Screen *scr = XtScreen(w);
  Window rootWindow = XRootWindowOfScreen(scr);

#ifdef XPM
  XpmAttributes pixmapAttrs;
  int pixmapReadStatus = XpmSuccess;

  pixmapAttrs.visual       = DefaultVisual(dpy, DefaultScreen(dpy));
  pixmapAttrs.colormap     = DefaultColormap(dpy, DefaultScreen(dpy));
  pixmapAttrs.depth        = DefaultDepthOfScreen(scr);
  pixmapAttrs.colorsymbols = (XpmColorSymbol *)NULL;
  pixmapAttrs.numsymbols   = 0;
  pixmapAttrs.valuemask    = XpmVisual | XpmColormap | XpmDepth |
    XpmReturnPixels;
#endif

/*
 * Initialize all private resources.
 */

  GetGCs(new);

  /* Set row height. */
  if (ilw->xedwList.show_icons)
    ilw->xedwList.row_height = ilw->xedwList.font->max_bounds.ascent
      + ilw->xedwList.font->max_bounds.descent
      + ilw->xedwList.row_space
      + ilw->xedwList.icon_height;
  else
    ilw->xedwList.row_height = ilw->xedwList.font->max_bounds.ascent
      + ilw->xedwList.font->max_bounds.descent
      + ilw->xedwList.row_space;

  ResetXedwList(new, (new->core.width == 0), (new->core.height == 0));

  ilw->xedwList.is_highlighted = (LinkedList*) XtMalloc (sizeof(LinkedList));

  ilw->xedwList.highlight = ilw->xedwList.is_highlighted->index = NO_HIGHLIGHT;
  ilw->xedwList.is_highlighted->next = NULL;

#ifdef XPM
  pixmapReadStatus =
    XpmCreatePixmapFromData(
        dpy,
	rootWindow,
	default_icon,
	&defaultIconPixmap,
	&defaultIconPixmapMask,
	&pixmapAttrs ) ;

  if (pixmapReadStatus != XpmSuccess)
    {
      defaultIconPixmap = (Pixmap) NULL ;
      defaultIconPixmapMask = (Pixmap) NULL;
    }
#else
  defaultIconPixmap = 
    XCreateBitmapFromData(
        dpy,
	rootWindow,
	default_bits, default_width, default_height);
  defaultIconPixmapMask = NULL;
#endif
} /* Initialize */

/*  Function Name: CvtToItem
 *  Description: Converts Xcoord to item number of item containing that
 *               point.
 *  Arguments: w - the xedwList widget.
 *             xloc, yloc - x location, and y location.
 *  Returns: the item number.
 */

#if NeedFunctionPrototypes
static int CvtToItem(Widget w, int xloc, int yloc, int *item)
#else
static int CvtToItem(w, xloc, yloc, item)
Widget w;
int xloc, yloc, *item;
#endif
{
  int one, another;
  XedwListWidget ilw = (XedwListWidget) w;
  int ret_val = OKAY;

  if (ilw->xedwList.vertical_cols) {
    one = ilw->xedwList.nrows * ((xloc - (int) ilw->xedwList.internal_width)
				 / ilw->xedwList.col_width);
    another = (yloc - (int) ilw->xedwList.internal_height)
      / ilw->xedwList.row_height;
    /* If out of range, return minimum possible value. */
    if (another >= ilw->xedwList.nrows) {
      another = ilw->xedwList.nrows - 1;
      ret_val = OUT_OF_RANGE;
    }
  }
  else {
    one = (ilw->xedwList.ncols * ((yloc - (int) ilw->xedwList.internal_height)
				  / ilw->xedwList.row_height)) ;
    /* If in right margin handle things right. */
    another = (xloc - (int) ilw->xedwList.internal_width) / ilw->xedwList.col_width;
    if (another >= ilw->xedwList.ncols) {
      another = ilw->xedwList.ncols - 1;
      ret_val = OUT_OF_RANGE;
    }
  }
  if ((xloc < 0) || (yloc < 0))
    ret_val = OUT_OF_RANGE;
  if (one < 0) one = 0;
  if (another < 0) another = 0;
  *item = one + another;
  if (*item >= ilw->xedwList.nitems) return(OUT_OF_RANGE);
  return(ret_val);
}

/*  Function Name: FindCornerItems.
 *  Description: Find the corners of the rectangle in item space.
 *  Arguments: w - the xedwList widget.
 *             event - the event structure that has the rectangle it it.
 *             ul_ret, lr_ret - the corners ** RETURNED **.
 *  Returns: none.
 */

#if NeedFunctionPrototypes
static void FindCornerItems(Widget w, XEvent *event, int *ul_ret, int *lr_ret)
#else
static void FindCornerItems(w, event, ul_ret, lr_ret)
Widget w;
XEvent *event;
int *ul_ret, *lr_ret;
#endif
{
  int xloc, yloc;
  
  xloc = event->xexpose.x;
  yloc = event->xexpose.y;
  CvtToItem(w, xloc, yloc, ul_ret);
  xloc += event->xexpose.width;
  yloc += event->xexpose.height;
  CvtToItem(w, xloc, yloc, lr_ret);
}

/*  Function Name: ItemInRectangle
 *  Description: returns TRUE if the item passed is in the given rectangle.
 *  Arguments: w - the xedwList widget.
 *             ul, lr - corners of the rectangle in item space.
 *             item - item to check.
 *  Returns: TRUE if the item passed is in the given rectangle.
 */

#if NeedFunctionPrototypes
static int ItemInRectangle(Widget w, int ul, int lr, int item)
#else
static int ItemInRectangle(w, ul, lr, item)
Widget w;
int ul, lr, item;
#endif
{
  XedwListWidget ilw = (XedwListWidget) w;
  register int mod_item;
  int things;

  if (item < ul || item > lr)
    return(FALSE);
  if (ilw->xedwList.vertical_cols)
    things = ilw->xedwList.nrows;
  else
    things = ilw->xedwList.ncols;
  
  mod_item = item % things;
  if ( (mod_item >= ul % things) && (mod_item <= lr % things ) )
    return(TRUE);
  return(FALSE);
}

/*  Function Name: HighlightBackground
 *  Description: paints the color of the background for the given item.
 *  Arguments: w - the widget.
 *             x, y - ul corner of the area item occupies.
 *             item - the item we are dealing with.
 *             gc - the gc that is used to paint this rectangle
 *  Returns:
 */

#if NeedFunctionPrototypes
static void HighlightBackground(Widget w, Drawable drawable, int x, int y, int item, GC gc)
#else
static void HighlightBackground(w, drawable, x, y, item, gc)
Widget w;
Drawable drawable;
int x,y,item;
GC gc;
#endif
{
  XedwListWidget ilw = (XedwListWidget) w;
  int hl_x, hl_y, width, height;

  hl_x = x - ilw->xedwList.column_space/2;
  width = XTextWidth(ilw->xedwList.font, ilw->xedwList.xedwList[item]->string,
		     strlen(ilw->xedwList.xedwList[item]->string))
    + ilw->xedwList.column_space;
  hl_y = y + ((ilw->xedwList.show_icons) ? ilw->xedwList.icon_height : 0);
  height = ilw->xedwList.row_height - ilw->xedwList.row_space -
    ((ilw->xedwList.show_icons) ? ilw->xedwList.icon_height : 0);

  XFillRectangle(XtDisplay(w), drawable, gc, hl_x, hl_y, width, height);
}

/*  Function Name: PaintItemName
 *  Description: paints the name of the item in the appropriate location.
 *  Arguments: w - the xedwList widget.

 *             item - the item to draw.
 *	       op - XedwOn, XedwOff
 *  Returns: none.
 *
 *      NOTE: no action taken on an unrealized widget.
 */

#if NeedFunctionPrototypes
static void PaintItemName(Widget w, Drawable drawable, int item, int operation)
#else
static void PaintItemName(w, drawable, item, operation)
Widget w;
Drawable drawable;
int item, operation;
#endif
{
  char * str;
  GC gc;
  unsigned normalmode, inversemode;
  int x, y, str_y, str_x;
  XedwListWidget ilw = (XedwListWidget) w;

  if (!XtIsRealized(w)) return; /* Just in case... */


  if (ilw->xedwList.vertical_cols) {
    x = ilw->xedwList.col_width * (item / ilw->xedwList.nrows)
      + ilw->xedwList.internal_width;
    y = ilw->xedwList.row_height * (item % ilw->xedwList.nrows)
      + ilw->xedwList.internal_height;
  }
  else {
    x = ilw->xedwList.col_width * (item % ilw->xedwList.ncols)
      + ilw->xedwList.internal_width;
    y = ilw->xedwList.row_height * (item / ilw->xedwList.ncols)
      + ilw->xedwList.internal_height;
  }

  
  str =  ilw->xedwList.xedwList[item]->string;
  
  str_y = y + ilw->xedwList.font->max_bounds.ascent;

  if (ilw->xedwList.show_icons)
    str_x = x + ((ilw->xedwList.longest/2) -
		 (XTextWidth(ilw->xedwList.font, str, strlen(str))/2));
  else
    str_x = x;

  if (DefaultDepthOfScreen(XtScreen(ilw)) == 1) {
    /* Monochrome */
    if (ilw->core.background_pixel == WhitePixelOfScreen(XtScreen(ilw))) { 
      normalmode = GXcopy;
      inversemode = GXcopyInverted;
    } else {
      normalmode = GXcopyInverted;	/* reverse video has been selected */
      inversemode = GXcopy;
    }
  } else {
    /* Colour */
    normalmode = GXcopy;
    inversemode = GXcopy;
  } 

  if (ilw->xedwList.is_highlighted->index != NO_HIGHLIGHT) {
    /* There are some highlighted items */
    if (ilw->xedwList.highlight == NO_HIGHLIGHT) {
      /* This is an update */
      if (operation == XedwOn) {
	if (XtIsSensitive(w)) {
	  /* Draw Inverse item */
	  gc = ilw->xedwList.revgc;
	  XSetFunction(XtDisplay(w), gc, inversemode);
	  HighlightBackground(w, drawable, str_x, y, item,
			      ilw->xedwList.normgc);
	} else {
	  /* Draw Grey item */
	}  
      } else {
	gc = ilw->xedwList.normgc;
	XSetFunction(XtDisplay(w), gc, normalmode); /* jcc */
      }
    } else {
      /* Were toggling something */
      if (operation == XedwOn) {
        if (XtIsSensitive(w)) {
          /* Draw Inverse item */
          gc = ilw->xedwList.revgc;
          XSetFunction(XtDisplay(w), gc, inversemode); 
          HighlightBackground(w, drawable, str_x, y, item,
			      ilw->xedwList.normgc);
	} else {
	  /* Draw Grey item */
	} 
      } else {
	/* Draw Normal item */
	gc = ilw->xedwList.normgc;
	XSetFunction(XtDisplay(ilw), gc, normalmode);
	HighlightBackground(w, drawable, str_x, y, item, ilw->xedwList.revgc); 
      }
    }
  } else {
    gc = ilw->xedwList.normgc;
    XSetFunction(XtDisplay(ilw), gc, normalmode);
  }

  if (ilw->xedwList.default_icon == (Pixmap)NULL) {
    ilw->xedwList.default_icon = defaultIconPixmap;
    ilw->xedwList.default_mask = defaultIconPixmapMask;
  }

  if (ilw->xedwList.show_icons){
    Pixmap icon, mask;
    if (ilw->xedwList.xedwList[item]->icon == (Pixmap)NULL &&
	ilw->xedwList.default_icon != (Pixmap)NULL) {
      icon = ilw->xedwList.default_icon;
      mask = ilw->xedwList.default_mask;
    } else {
      icon = ilw->xedwList.xedwList[item]->icon;
      mask = ilw->xedwList.xedwList[item]->mask;
    }

    if (mask == (Pixmap)NULL) {
	XCopyArea(XtDisplay(ilw), icon, drawable, gc, 0, 0,
		(unsigned) ilw->xedwList.icon_width,
		(unsigned) ilw->xedwList.icon_height,
		x + ((ilw->xedwList.longest/2) - (ilw->xedwList.icon_width/2)),
		y);
    } else {
#ifdef NO_CLIP
	XSetFunction(XtDisplay(ilw), maskgc, GXxor);
	XCopyArea(XtDisplay(ilw), icon, drawable, maskgc, 0, 0,
		(unsigned) ilw->xedwList.icon_width,
		(unsigned) ilw->xedwList.icon_height,
		x + ((ilw->xedwList.longest/2) - (ilw->xedwList.icon_width/2)),
		y);
	XSetFunction(XtDisplay(ilw), maskgc, GXand);
	XCopyPlane(XtDisplay(ilw), mask, drawable, maskgc, 0, 0,
		(unsigned) ilw->xedwList.icon_width,
		(unsigned) ilw->xedwList.icon_height,
		x + ((ilw->xedwList.longest/2) - (ilw->xedwList.icon_width/2)),
		y,
		1L);
	XSetFunction(XtDisplay(ilw), maskgc, GXxor);
	XCopyArea(XtDisplay(ilw), icon, drawable, maskgc, 0, 0,
		(unsigned) ilw->xedwList.icon_width,
		(unsigned) ilw->xedwList.icon_height,
		x + ((ilw->xedwList.longest/2) - (ilw->xedwList.icon_width/2)),
		y);
#else
	XSetClipMask(XtDisplay(ilw), maskgc, mask);
	XSetClipOrigin(XtDisplay(ilw), maskgc,
		       x + ((ilw->xedwList.longest/2) -
			    (ilw->xedwList.icon_width/2)),
		       y);
	XCopyArea(XtDisplay(ilw), icon, drawable, maskgc, 0, 0,
		(unsigned) ilw->xedwList.icon_width,
		(unsigned) ilw->xedwList.icon_height,
		x + ((ilw->xedwList.longest/2) - (ilw->xedwList.icon_width/2)),
		y);
#endif		  
    }

    /* Draw string */
    XSetFunction(XtDisplay(ilw), gc, GXcopy);
    XDrawString(XtDisplay(ilw), drawable, gc, str_x,
		str_y+ilw->xedwList.icon_height, str, strlen(str));
  } else { /* No Icons */
    XSetFunction(XtDisplay(ilw), gc, GXcopy);
    XDrawString(XtDisplay(ilw), drawable, gc, str_x, str_y, str, strlen(str));
  }
}

/*  Function Name: Redisplay
 *  Description: Repaints the widget window on expose events.
 *  Arguments: w - the xedwList widget.
 *                 event - the expose event for this repaint.
 *                 junk - NOT USED.
 *  Returns:
 */

#if NeedFunctionPrototypes
static void Redisplay(Widget w, XEvent *event, Region junk)
#else
static void Redisplay(w, event, junk)
Widget w;
XEvent *event;
Region junk;
#endif
{
  int item;           /* an item to work with. */
  int ul_item, lr_item;       /* corners of items we need to paint. */
  XedwListWidget ilw = (XedwListWidget) w;
  Display *dpy = XtDisplay(w);
  GC gc = ilw->xedwList.normgc;

  if (event == NULL) {    /* repaint all. */
    ul_item = 0;
    lr_item = ilw->xedwList.nrows * ilw->xedwList.ncols - 1;
    XClearWindow(XtDisplay(w), XtWindow(w));
  }
  else
    FindCornerItems(w, event, &ul_item, &lr_item);

  ilw->xedwList.highlight = NO_HIGHLIGHT;
  for (item = ul_item; (item <= lr_item && item < ilw->xedwList.nitems) ; item++)
    if (ItemInRectangle(w, ul_item, lr_item, item))
      if (IsHighlighted(w, item))
	PaintItemName(w, XtWindow(w), item, XedwOn);
      else
	PaintItemName(w, XtWindow(w), item, XedwOff);

}

/*  Function Name: PreferredGeom
 *  Description: This tells the parent what size we would like to be
 *                   given certain constraints.
 *  Arguments: w - the widget.
 *                 intended - what the parent intends to do with us.
 *                 requested - what we want to happen.
 *  Returns: none.
 */

#if NeedFunctionPrototypes
static XtGeometryResult PreferredGeom(Widget w, XtWidgetGeometry *intended, XtWidgetGeometry *requested)
#else
static XtGeometryResult PreferredGeom(w, intended, requested)
Widget w;
XtWidgetGeometry *intended, *requested;
#endif
{
  Dimension new_width, new_height;
  Boolean change, width_req, height_req;

  width_req = intended->request_mode & CWWidth;
  height_req = intended->request_mode & CWHeight;

  if (width_req)
    new_width = intended->width;
  else
    new_width = w->core.width;

  if (height_req)
    new_height = intended->height;
  else
    new_height = w->core.height;

  requested->request_mode = 0;

  /*
   * We only care about our height and width.
   */

  if ( !width_req && !height_req)
    return(XtGeometryYes);

  change = Layout(w, !width_req, !height_req, &new_width, &new_height);

  requested->request_mode |= CWWidth;
  requested->width = new_width;
  requested->request_mode |= CWHeight;
  requested->height = new_height;

  if (change)
    return(XtGeometryAlmost);
  return(XtGeometryYes);
}

/*  Function Name: Resize
 *  Description: resizes the widget, by changing the number of rows and
 *               columns.
 *  Arguments: w - the widget.
 *  Returns: none.
 */

#if NeedFunctionPrototypes
static void Resize(Widget w)
#else
static void Resize(w)
Widget w;
#endif
{
  Dimension width, height;

  width = w->core.width;
  height = w->core.height;

  if (Layout(w, FALSE, FALSE, &width, &height))
    XtAppWarning(XtWidgetToApplicationContext(w),
		 "XedwList Widget: Size changed when it shouldn't have when resising.");
}

/*  Function Name: Layout
 *  Description: lays out the item in the xedwList.
 *  Arguments: w - the widget.
 *             xfree, yfree - TRUE if we are free to resize the widget in
 *                            this direction.
 *             width, height - the is the current width and height that
 *                             we are going to layout the xedwList widget to,
 *                             depending on xfree and yfree of course.
 *
 *  Returns: TRUE if width or height have been changed.
 */

#if NeedFunctionPrototypes
static Boolean Layout(Widget w, Boolean xfree, Boolean yfree, Dimension *width, Dimension *height)
#else
static Boolean Layout(w, xfree, yfree, width, height)
Widget w;
Boolean xfree, yfree;
Dimension *width, *height;
#endif
{
  XedwListWidget ilw = (XedwListWidget) w;
  Boolean change = FALSE;

  /*
   * If force columns is set then always use number of columns specified
   * by default_cols.
   */

  if (ilw->xedwList.force_cols) {
    ilw->xedwList.ncols = ilw->xedwList.default_cols;
    if (ilw->xedwList.ncols <= 0) ilw->xedwList.ncols = 1;
    /* 12/3 = 4 and 10/3 = 4, but 9/3 = 3 */
    ilw->xedwList.nrows = ( ( ilw->xedwList.nitems - 1) / ilw->xedwList.ncols) + 1 ;
    if (xfree) {        /* If allowed resize width. */
      *width = ilw->xedwList.ncols * ilw->xedwList.col_width
	+ 2 * ilw->xedwList.internal_width;
      change = TRUE;
    }
    if (yfree) {        /* If allowed resize height. */
      *height = (ilw->xedwList.nrows * ilw->xedwList.row_height)
	+ 2 * ilw->xedwList.internal_height;
      change = TRUE;
    }
    return(change);
  }

  /*
   * If both width and height are free to change the use default_cols
   * to determine the number columns and set new width and height to
   * just fit the window.
   */

  if (xfree && yfree) {
    ilw->xedwList.ncols = ilw->xedwList.default_cols;
    if (ilw->xedwList.ncols <= 0) ilw->xedwList.ncols = 1;
    ilw->xedwList.nrows = ( ( ilw->xedwList.nitems - 1) / ilw->xedwList.ncols) + 1 ;
    *width = ilw->xedwList.ncols * ilw->xedwList.col_width
      + 2 * ilw->xedwList.internal_width;
    *height = (ilw->xedwList.nrows * ilw->xedwList.row_height)
      + 2 * ilw->xedwList.internal_height;
    change = TRUE;
  }
  /*
   * If the width is fixed then use it to determine the number of columns.
   * If the height is free to move (width still fixed) then resize the height
   * of the widget to fit the current xedwList exactly.
   */
  else if (!xfree) {
    ilw->xedwList.ncols = ( (*width - 2 * ilw->xedwList.internal_width)
			   / ilw->xedwList.col_width);
    if (ilw->xedwList.ncols <= 0) ilw->xedwList.ncols = 1;
    ilw->xedwList.nrows = ( ( ilw->xedwList.nitems - 1) / ilw->xedwList.ncols) + 1 ;
    if ( yfree ) {
      *height = (ilw->xedwList.nrows * ilw->xedwList.row_height)
	+ 2 * ilw->xedwList.internal_height;
      change = TRUE;
    }
  }
  /*
   * The last case is xfree and !yfree we use the height to determine
   * the number of rows and then set the width to just fit the resulting
   * number of columns.
   */
  else if (!yfree) {      /* xfree must be TRUE. */
    ilw->xedwList.nrows = (*height - 2 * ilw->xedwList.internal_height)
      / ilw->xedwList.row_height;
    if (ilw->xedwList.nrows <= 0) ilw->xedwList.nrows = 1;
    ilw->xedwList.ncols = (( ilw->xedwList.nitems - 1 ) / ilw->xedwList.nrows) + 1;
    *width = ilw->xedwList.ncols * ilw->xedwList.col_width
      + 2 * ilw->xedwList.internal_width;
    change = TRUE;
  }
  return(change);
}

/*  Function Name: Notify
 *  Description: Notifies the user that a button has been pressed, and
 *               calles the callback, if the XtNpasteBuffer resource
 *               is true then the name of the item is also put in the
 *               X cut buffer ( buf (0) ).
 *  Arguments: w - the widget that the notify occured in.
 *             event - event that caused this notification.
 *             params, num_params - not used.
 *  Returns: none.
 */

#if NeedFunctionPrototypes
static void Notify(Widget w, XEvent *event, String *params, Cardinal *num_params)
#else
static void Notify(w, event, params, num_params)
Widget w;
XEvent *event;
String *params;
Cardinal *num_params;
#endif
{
  XedwListWidget ilw = ( XedwListWidget ) w;
  int item, item_len;
  XedwListReturnStruct ret_value;

  if ( ((CvtToItem(w, event->xbutton.x, event->xbutton.y, &item))
	== OUT_OF_RANGE) || (ilw->xedwList.highlight != item) ) {
    XedwListUnhighlight(w, ilw->xedwList.highlight);
    RemoveNode(w, ilw->xedwList.highlight);
    XtCallCallbacks(w, XtNcallback, (caddr_t) 0);
    return;
  }

  /* Put the name of ALL highlighted strings into the X cut buffer, 
   * seperated by spaces.
   */

  item_len = strlen(ilw->xedwList.xedwList[item]->string);
  
  if ( ilw->xedwList.paste )   /* if XtNpasteBuffer set then paste it. */
    {
      XStoreBytes(XtDisplay(w), ilw->xedwList.xedwList[item]->string, item_len);
      {
	
	selection_set_buffer(w,ilw->xedwList.xedwList[item]->string);
      }
    }
  /*
   * Call Callback function.
   */
  
  ret_value.string = ilw->xedwList.xedwList[item]->string;
  ret_value.xedwList_index = item;
  ret_value.next = NULL;
  
  XtCallCallbacks( w, XtNcallback, (caddr_t) &ret_value);
}

/*  Function Name: Unset
 *  Description: unhighlights the current elements.
 *  Arguments: w - the widget that the event occured in.
 *                 event - not used.
 *                 params, num_params - not used.
 *  Returns: none.
 */

#if NeedFunctionPrototypes
static void Unset(Widget w, XEvent *event, String *params, Cardinal *num_params)
#else
static void Unset(w, event, params, num_params)
Widget w;
XEvent *event;
String *params;
Cardinal *num_params;
#endif
{
  XedwListUnhighlight(w, XedwAll);
}

/*  Function Name: Set
 *  Description: Highlights the current element.
 *  Arguments: w - the widget that the event occured in.
 *                 event - event that caused this notification.
 *                 params, num_params - not used.
 *  Returns: none.
 */

#if NeedFunctionPrototypes
static void Set(Widget w, XEvent *event, String *params, Cardinal *num_params)
#else
static void Set(w, event, params, num_params)
Widget w;
XEvent *event;
String *params;
Cardinal *num_params;
#endif
{
  int item;
  XedwListWidget ilw = (XedwListWidget) w;

  if ( (CvtToItem(w, event->xbutton.x, event->xbutton.y, &item))
      == OUT_OF_RANGE)
    XedwListUnhighlight(w, XedwAll);    /* Unhighlight current items. */
  else {
    if (ilw->xedwList.highlight != item || 
	ilw->xedwList.is_highlighted->next != NULL) 
      XedwListUnhighlight(w, XedwAll);	/* Unhighlight any others */
    XedwListHighlight(w, item);   	/* highlight it */
  }
}

/*  Function Name: MultipleSet
 *  Description: Highlights the current element.
 *  Arguments: w - the widget that the event occured in.
 *                 event - event that caused this notification.
 *                 params, num_params - not used.
 *  Returns: none.
 */

#if NeedFunctionPrototypes
static void MultipleSet(Widget w, XEvent *event, String *params, Cardinal *num_params)
#else
static void MultipleSet(w, event, params, num_params)
Widget w;
XEvent *event;
String *params;
Cardinal *num_params;
#endif
{
  int item;
  XedwListWidget ilw = (XedwListWidget) w;

  if (ilw->xedwList.multiple == True)
    if ( (CvtToItem(w, event->xbutton.x, event->xbutton.y, &item))
	== OUT_OF_RANGE)
      XedwListUnhighlight(w, XedwAll);     /* Unhighlight all current items. */
    else 
      if (IsHighlighted(w, item))	/* If already highlighted, */
	XedwListUnhighlight(w, item);	/* unhighlight it */
      else
	XedwListHighlight(w, item);   	/* otherwise highlight it */
  else
    XBell(XtDisplay(w), 100);
}

/* 
 * Return state of a list item 
 */
#if NeedFunctionPrototypes
static Boolean IsHighlighted(Widget w, int item)
#else
static Boolean IsHighlighted(w, item)
Widget w;
int item;
#endif
{
  XedwListWidget ilw = (XedwListWidget) w;
  LinkedList *list = ilw->xedwList.is_highlighted;
  Boolean ret_val;

  while (list != NULL && list->index != item && list->index != NO_HIGHLIGHT)
    list = list->next;
  
  if (list == NULL || list->index == NO_HIGHLIGHT)
    ret_val=FALSE;
  else
    ret_val=TRUE;
  
  return(ret_val);	/* true if item was in list false otherwise */
}

/*
 * Set specified arguments into widget
 */

#if NeedFunctionPrototypes
static Boolean SetValues(Widget current, Widget request, Widget new, ArgList args, Cardinal *num_args)
#else
static Boolean SetValues(current, request, new, args, num_args)
Widget current, request, new;
ArgList args;
Cardinal *num_args;
#endif
{
  XedwListWidget cl = (XedwListWidget) current;
  XedwListWidget rl = (XedwListWidget) request;
  XedwListWidget nl = (XedwListWidget) new;
  Boolean redraw = FALSE;

  if ((cl->xedwList.foreground != rl->xedwList.foreground) ||
      (cl->core.background_pixel != rl->core.background_pixel) ||
      (cl->xedwList.font != rl->xedwList.font) ) {
    XtDestroyGC(cl->xedwList.normgc);
    XtDestroyGC(cl->xedwList.graygc);
    XtDestroyGC(cl->xedwList.revgc);
    GetGCs(new);
    redraw = TRUE;
  }

  /* Reset row height. */
  
  if ((cl->xedwList.row_space != rl->xedwList.row_space) ||
      (cl->xedwList.font != rl->xedwList.font)           ||
      (cl->xedwList.show_icons != rl->xedwList.show_icons) ||
      (cl->xedwList.icon_width != rl->xedwList.icon_width)           ||
      (cl->xedwList.icon_height != rl->xedwList.icon_height))
    if (rl->xedwList.show_icons) 
      nl->xedwList.row_height = nl->xedwList.font->max_bounds.ascent
	+ nl->xedwList.font->max_bounds.descent
	+ nl->xedwList.row_space
        + nl->xedwList.icon_height;
    else
      nl->xedwList.row_height = nl->xedwList.font->max_bounds.ascent
	+ nl->xedwList.font->max_bounds.descent
	+ nl->xedwList.row_space;

  if ((cl->core.width != rl->core.width)                     ||
      (cl->core.height != rl->core.height)                   ||
      (cl->xedwList.internal_width != rl->xedwList.internal_width)   ||
      (cl->xedwList.internal_height != rl->xedwList.internal_height) ||
      (cl->xedwList.column_space != rl->xedwList.column_space)       ||
      (cl->xedwList.row_space != rl->xedwList.row_space)             ||
      (cl->xedwList.default_cols != rl->xedwList.default_cols)       ||
      ((cl->xedwList.force_cols != rl->xedwList.force_cols) &&
       (rl->xedwList.force_cols != rl->xedwList.ncols))           ||
      (cl->xedwList.vertical_cols != rl->xedwList.vertical_cols)     ||
      (cl->xedwList.longest != rl->xedwList.longest)                 ||
      (cl->xedwList.nitems != rl->xedwList.nitems)                   ||
      (cl->xedwList.font != rl->xedwList.font)                       ||
      (cl->xedwList.show_icons != rl->xedwList.show_icons)           ||
      (cl->xedwList.icon_width != rl->xedwList.icon_width)           ||
      (cl->xedwList.icon_height != rl->xedwList.icon_height)         ||
      (cl->xedwList.default_icon != rl->xedwList.default_icon)       ||
      (cl->xedwList.xedwList != rl->xedwList.xedwList)                        ) {
    
    ResetXedwList(new, TRUE, TRUE);
    redraw = TRUE;
  }

  if (cl->xedwList.xedwList != rl->xedwList.xedwList)
    nl->xedwList.highlight = NO_HIGHLIGHT;

  if ((cl->core.sensitive != rl->core.sensitive) ||
      (cl->core.ancestor_sensitive != rl->core.ancestor_sensitive)) {
    nl->xedwList.highlight = NO_HIGHLIGHT;
    redraw = TRUE;
  }
  
  if (!XtIsRealized(current))
    return(FALSE);

  return(redraw);
}

/* Exported Functions */

/*  Function Name: XedwListFreeCurrent
 *  Description: Frees the storage allocated by a call to XedwListShowCurrent
 *  Arguments: list - the XedwListReturnStruct list.
 *  Returns: none.
 */
#if NeedFunctionPrototypes
void XedwListFreeCurrent(XedwListReturnStruct *list)
#else
void XedwListFreeCurrent(list)
     XedwListReturnStruct *list;
#endif /* NeedFunctionPrototypes */
{
    if (list == (XedwListReturnStruct *)0) return;
    if (list->xedwList_index == XDTM_LIST_NONE)
      XtFree((char *)list);
    else {
	XedwListReturnStruct *tmplist;

	while (list != (XedwListReturnStruct *)0) {
	    tmplist = list->next;
	    XtFree((char *)list->string);
	    XtFree((char *)list);
	    list = tmplist;
	}
    }
}
    

/*  Function Name: XedwListChange.
 *  Description: Changes the xedwList being used and shown.
 *  Arguments: w - the xedwList widget.
 *                 xedwList - the new xedwList.
 *                 nitems - the number of items in the xedwList.
 *                 longest - the length (in Pixels) of the longest element
 *                           in the xedwList.
 *                 resize - if TRUE the the xedwList widget will
 *                          try to resize itself.
 *  Returns: none.
 *      NOTE:      If nitems of longest are <= 0 then they will be calculated.
 *                 If nitems is <= 0 then the xedwList needs to be NULL terminated.
 */

#if NeedFunctionPrototypes
void XedwListChange(Widget w, XedwList **xedwList, int nitems, int longest, Boolean resize_it)
#else
void XedwListChange(w, xedwList, nitems, longest, resize_it)
     Widget w;
     XedwList **xedwList;
     int nitems, longest;
     Boolean resize_it;
#endif
{
  XedwListWidget ilw = (XedwListWidget) w;

  ilw->xedwList.xedwList = xedwList;
  
  if (nitems <= 0) nitems = 0;
  ilw->xedwList.nitems = nitems;
  if (longest <= 0) longest = 0;
  ilw->xedwList.longest = longest;

  ResetXedwList(w, resize_it, resize_it);
  /* Free the memeory in the old list TODO */
  ilw->xedwList.is_highlighted->index = ilw->xedwList.highlight = NO_HIGHLIGHT;
  ilw->xedwList.is_highlighted->next = NULL;
  if ( XtIsRealized(w) )
    Redisplay(w, NULL, NULL);
}

/*  Function Name: XedwListUnhighlight
 *  Description: unlights the current highlighted element.
 *  Arguments: w - the widget.
 *  Returns: none.
 */

#if NeedFunctionPrototypes
void XedwListUnhighlight(Widget w, int item)
#else
void XedwListUnhighlight(w, item)
     Widget w;
     int item;
#endif
{
  XedwListWidget ilw = ( XedwListWidget ) w;
  LinkedList *list = ilw->xedwList.is_highlighted;
  LinkedList *temp;

  if (ilw->xedwList.is_highlighted->index != NO_HIGHLIGHT)
    if (item == XedwAll) {
      ilw->xedwList.highlight = list->index;	
      PaintItemName(w, XtWindow(w), list->index, XedwOff); /* unhighlight ALL */
      while (list->next != NULL) {		
	temp = list;
	list = list->next;
	ilw->xedwList.is_highlighted = list;
	ilw->xedwList.highlight = list->index;
	PaintItemName(w, XtWindow(w), list->index, XedwOff);
	XtFree((char *)temp);
      }
      list->index = NO_HIGHLIGHT;
    } else {
      ilw->xedwList.highlight = item;
      PaintItemName(w, XtWindow(w), item, XedwOff);
      RemoveNode(w, item);
    }
}

/*  Function Name: XedwListHighlight
 *  Description: Highlights the given item.
 *  Arguments: w - the xedwList widget.
 *             item - the item to hightlight.
 *  Returns: none.
 */

#if NeedFunctionPrototypes
void XedwListHighlight(Widget w, int item)
#else
void XedwListHighlight(w, item)
     Widget w;
     int item;
#endif
{
  XedwListWidget ilw = ( XedwListWidget ) w;
  int n;

  if (XtIsSensitive(w)) {
    if (item == XedwAll) {
      for(n=0; n < ilw->xedwList.nitems; n++) {
	ilw->xedwList.highlight = n;
	RemoveNode(w, n);
	AddNode(w, n);
	PaintItemName(w, XtWindow(w), n, XedwOn);
      }
    } else {
      ilw->xedwList.highlight = item;
      RemoveNode(w, item);
      AddNode(w, item);
      PaintItemName(w, XtWindow(w), item, XedwOn);    
    }
  }
}

/* The following stuff is stolen from xcutsel. Actually, it IS
 * xcutsel.c */
/*
 * Author:  Ralph Swick, DEC/Project Athena
 */


static String selection_value;
static Atom selection_atom=0;

#if NeedFunctionPrototypes
static Boolean ConvertSelection(Widget w, Atom *selection, Atom *target, Atom *type, XtPointer *value, unsigned long *length, int *format)
#else
static Boolean ConvertSelection(w, selection, target,
                                type, value, length, format)
     Widget w;
     Atom *selection, *target, *type;
     XtPointer *value;
     unsigned long *length;
     int *format;
#endif
{
    Display* d = XtDisplay(w);
    XSelectionRequestEvent* req =
        XtGetSelectionRequest(w, *selection, (XtRequestId)NULL);

    if (*target == XA_TARGETS(d)) {
        Atom* targetP;
        Atom* std_targets;
        unsigned long std_length;
        XmuConvertStandardSelection(w, req->time, selection, target, type,
                                   (caddr_t*)&std_targets, &std_length, format);
        *value = XtMalloc(sizeof(Atom)*(std_length + 4));
        targetP = *(Atom**)value;
        *length = std_length + 4;
        *targetP++ = XA_STRING;
        *targetP++ = XA_TEXT(d);
        *targetP++ = XA_LENGTH(d);
        *targetP++ = XA_LIST_LENGTH(d);
        memcpy((char*)targetP, (char*)std_targets, sizeof(Atom)*std_length);
        XtFree((char*)std_targets);
        *type = XA_ATOM;
        *format = 32;
        return True;
    }
    if (*target == XA_STRING || *target == XA_TEXT(d)) {
        *type = XA_STRING;
        *value = selection_value;
        *length = strlen(selection_value);
        *format = 8;
        return True;
    }
    if (*target == XA_LIST_LENGTH(d)) {
        long *temp = (long *) XtMalloc (sizeof(long));
        *temp = 1L;
        *value = (caddr_t) temp;
        *type = XA_INTEGER;
        *length = 1;
        *format = 32;
        return True;
    }
    if (*target == XA_LENGTH(d)) {
        long *temp = (long *) XtMalloc (sizeof(long));
        *temp = strlen(selection_value);
        *value = (caddr_t) temp;
        *type = XA_INTEGER;
        *length = 1;
        *format = 32;
        return True;
    }
    if (XmuConvertStandardSelection(w, req->time, selection, target, type,
                                    (caddr_t *)value, length, format))
        return True;

    /* else */
    return False;
}

#if NeedFunctionPrototypes
static void LoseSelection(Widget w, Atom *selection)
#else
static void LoseSelection(w, selection)
    Widget w;
    Atom *selection;
#endif
{
    XtFree((char *)selection_value );
    selection_value = NULL;
}

#if NeedFunctionPrototypes
static void selection_set_buffer(Widget w, String string)
#else
static void selection_set_buffer(w, string)
    Widget w;
    String string;
#endif
{
    static char * name="PRIMARY";
    if (selection_atom==0)
     XmuInternStrings( XtDisplay(w), &name, ONE,
                      &selection_atom );
    XtFree((char *)selection_value );
    selection_value=XtNewString(string);
    XtOwnSelection(w, selection_atom,
                           XtLastTimestampProcessed(XtDisplay(w)),
                            ConvertSelection, LoseSelection, NULL);
}


/*  Function Name: XedwListShowCurrent
 *  Description: returns the currently highlighted object.
 *  Arguments: w - the xedwList widget.
 *  Returns: the info about the currently highlighted object.
 */

#if NeedFunctionPrototypes
XedwListReturnStruct *XedwListShowCurrent(Widget w)
#else
XedwListReturnStruct *XedwListShowCurrent(w)
     Widget w;
#endif
{
  XedwListWidget ilw = ( XedwListWidget ) w;
  LinkedList *hl = ilw->xedwList.is_highlighted;
  XedwListReturnStruct *ret_val, *rest;

  rest = NULL;
  ret_val = NULL;
  while (hl != NULL) {
    ret_val = (XedwListReturnStruct *) XtMalloc(sizeof (XedwListReturnStruct));
    
    ret_val->xedwList_index = hl->index;
    if (ret_val->xedwList_index == XDTM_LIST_NONE)
      ret_val->string = "";
    else
      ret_val->string = 
	XtNewString(ilw->xedwList.xedwList[ret_val->xedwList_index]->string);
    hl = hl->next;
    ret_val->next = rest;
    rest = ret_val;
  }
  return ret_val;
}

#if NeedFunctionPrototypes
Boolean XedwListSelection(Widget w)
#else
Boolean XedwListSelection(w)
Widget w;
#endif
{
  XedwListWidget ilw = ( XedwListWidget ) w;
  LinkedList *hl = ilw->xedwList.is_highlighted;
  Boolean ret_val = True;
  
  if (hl->index == NO_HIGHLIGHT)
    ret_val = False;

  return(ret_val);
}

/* AddNode to LinkedList of highlighted items
 */
#if NeedFunctionPrototypes
static void AddNode(Widget w, int item)
#else
static void AddNode(w, item)
Widget w;
int item;
#endif
{
  XedwListWidget ilw = ( XedwListWidget ) w;
  LinkedList *list = ilw->xedwList.is_highlighted;
  LinkedList *temp;

  if (list->index != NO_HIGHLIGHT) {
    temp = (LinkedList*) XtMalloc (sizeof(LinkedList));
    temp->index = item;
    temp->next = list;
    ilw->xedwList.is_highlighted = temp;
  } else {
    list->index = item;    /* jcc */
    list->next = NULL;
  }
}
  

#if NeedFunctionPrototypes
static void RemoveNode(Widget w, int item)
#else
static void RemoveNode(w, item)
Widget w;
int item;
#endif
{
  XedwListWidget ilw = ( XedwListWidget ) w;
  LinkedList *list = ilw->xedwList.is_highlighted;
  LinkedList *last;                              

  last = list;
  while (list != NULL && list->index != item) {
    last = list;
    list=list->next;
  }
  if (list != NULL) {	    	/* item WAS found */
    if (last == list) {		/* it was the first item */
      if (list->next == NULL)   /* there are no more in the list */
	list->index = NO_HIGHLIGHT;
      else {
	ilw->xedwList.is_highlighted = list->next;
	XtFree((char *)list);
      }
    } else {
      last->next = list->next;
      XtFree((char *)list);
    }
  } 
}
