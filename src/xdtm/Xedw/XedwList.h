/*
 * XedwList.h - XedwList widget
 *
 * This is the XedwList widget. It is very similar to the Athena Widget List,
 * except it has the ability to display an icon with the string.
 * It allows the user to select an item in a xedwList and notifies the
 * application through a callback function.
 *
 *  List Created:   8/13/88
 *  By:     Chris D. Peterson
 *                      MIT X Consortium
 *
 *  Modified to XedwList: 1/26/91
 *  By:      Edward Groenendaal
 *                      University of Sussex, UK.
 */

#ifndef _XawXedwList_h
#define _XawXedwList_h

/***********************************************************************
 *
 * XedwList Widget
 *
 ***********************************************************************/

#include <X11/Intrinsic.h> 
#include <X11/Xaw/Simple.h>

/* Resources:

 Name               Class           RepType             Default Value
 ----               -----           -------             -------------
 background         Background      Pixel               XtDefaultBackground
 border             BorderColor     Pixel               XtDefaultForeground
 borderWidth        BorderWidth     Dimension           1
 callback           Callback        XtCallbackXedwList  NULL       **6
 columnSpacing      Spacing         Dimension           6
 cursor             Cursor          Cursor              left_ptr
 defaultColumns     Columns         int                 2          **5
 destroyCallback    Callback        Pointer             NULL
 font               Font            XFontStruct*        XtDefaultFont
 forceColumns       Columns         Boolean             False      **5
 foreground         Foreground      Pixel               XtDefaultForeground
 height             Height          Dimension           0          **1
 insensitiveBorder  Insensitive     Pixmap              Gray
 internalHeight     Height          Dimension           2
 internalWidth      Width           Dimension           4
 xedwList           XedwList        XedwList **         NULL       **2
 iconWidth          Width           Dimension           32
 iconHeight         Height          Dimension           32
 longest            Longest         int                 0          **3  **4
 mappedWhenManaged  MappedWhenManaged   Boolean         True
 mSelections	    Boolean         Boolean             False
 numberStrings      NumberStrings   int                 0          **4
 pasteBuffer        Boolean         Boolean             False
 rowSpacing         Spacing         Dimension           4
 sensitive          Sensitive       Boolean             True
 showIcons          Boolean         Boolean             False                  
 verticalList       Boolean         Boolean             False
 width              Width           Dimension           0          **1
 x                  Position        Position            0
 y                  Position        Position            0

 **1 - If the Width or Height of the xedwList widget is zero (0) then the value
       is set to the minimum size necessay to fit the entire list.

       If both Width and Height are zero then they are adjusted to fit the
       entire list that is created width the number of default columns
       specified in the defaultColumns resource.

 **2 - This is an array of strings the specify elements of the xedwList.
       This resource must be specified.

 **3 - Longest is the length of the widest string in pixels.

 **4 - If either of these values are zero (0) then the xedwList widget calculates
       the correct value.

       (This allows you to make startup faster if you already have
        this information calculated)

       NOTE: If the numberStrings value is zero the xedwList must
             be NULL terminated.

 **5 - By setting the XedwList.Columns resource you can force the application to
       have a given number of columns.

 **6 - This returns the name and index of the item selected in an
       XedwListReturnStruct that is pointed to by the client_data
       in the CallbackProc.

*/


/*
 * Value returned when there are no highlighted objects.
 */

#define XDTM_LIST_NONE -1

#define XtCXedwList "XedwList"
#define XtCSpacing "Spacing"
#define XtCColumns "Columns"
#define XtCLongest "Longest"
#define XtCNumberStrings "NumberStrings"

#define XtNcursor "cursor"
#define XtNcolumnSpacing "columnSpacing"
#define XtNdefaultColumns "defaultColumns"
#define XtNforceColumns "forceColumns"
#define XtNxedwList "xedwList"
#define XtNiconWidth "iconWidth"
#define XtNiconHeight "iconHeight"
#define XtNdefaultIcon "defaultIcon"
#define XtNlongest "longest"
#define XtNnumberStrings "numberStrings"
#define XtNpasteBuffer "pasteBuffer"
#define XtNrowSpacing "rowSpacing"
#define XtNshowIcons "showIcons"
#define XtNmSelections "mSelections"
#define XtNverticalList "verticalList"

#define XedwSingle    1
#define XedwMultiple  2

#define XedwAll			-1

#define IconBitmapWidth       32
#define IconBitmapHeight      32

/* Class record constants */

extern WidgetClass xedwListWidgetClass;

typedef struct _XedwListClassRec *XedwListWidgetClass;
typedef struct _XedwListRec      *XedwListWidget;

/* The structure of XedwList */

typedef struct _XedwList {
  String string;
  Pixmap icon;
  Pixmap mask;
} XedwList;

/* The xedwList return structure. */

typedef struct _XedwListReturnStruct {
  String   string;			/* String */
  int      xedwList_index;		/* Index into list */
  struct _XedwListReturnStruct *next;	/* Next highlighted entry */
} XedwListReturnStruct;

/******************************************************************
 *
 * Exported Functions
 *
 *****************************************************************/

/*  Function Name:  XedwListChange.
 *  Description:    Changes the xedwList being used and shown.
 *  Arguments:      w        -  the xedwList widget.
 *                  xedwList -  the new xedwList.
 *                  nitems   -  the number of items in the xedwList.
 *                  longest  -  the length (in Pixels) of the longest element
 *                              in the xedwList.
 *                  resize   -  if TRUE the the xedwList widget will
 *                              try to resize itself.
 *  Returns: none.
 *  NOTE:    If nitems of longest are <= 0 then they will be caluculated.
 *           If nitems is <= 0 then the xedwList needs to be NULL terminated.
 */

extern void XedwListChange(
#if NeedFunctionPrototypes
Widget, XedwList**,int,int,Boolean 
#endif
); 

/*  Function Name:  XedwListUnhighlight
 *  Description:    unlights the current highlighted element.
 *  Arguments:      w   -   the widget.
 *  Returns: none.
 */

extern void XedwListUnhighlight(
#if NeedFunctionPrototypes
 Widget, int
#endif
);

/*  Function Name:  XedwListHighlight
 *  Description:    Highlights the given item.
 *  Arguments:      w    -  the xedwList widget.
 *                  item -  the item to hightlight.
 *  Returns: none.
 */

extern void XedwListHighlight(
#if NeedFunctionPrototypes
Widget, int 
#endif
); 


/*  Function Name: XedwListShowCurrent
 *  Description: returns the currently highlighted objects.
 *  Arguments: w - the xedwList widget.
 *  Returns: the info about the currently highlighted object.
 */

extern XedwListReturnStruct *XedwListShowCurrent(
#if NeedFunctionPrototypes
Widget 
#endif
);

/*  Function Name: XedwListSelection
 *  Description: returns true is an object is highlighted, false otherwise.
 *  Arguments: w - the xedwList widget.
 */

extern Boolean XedwListSelection(
#if NeedFunctionPrototypes
  Widget
#endif
);

/*  Function Name: XedwListFreeCurrent
 *  Description: Frees the storage allocated by a call to XedwListShowCurrent
 *  Arguments: list - the XedwListReturnStruct list.
 *  Returns: none.
 */
extern void XedwListFreeCurrent(
#if NeedFunctionPrototypes
  XedwListReturnStruct *
#endif
);
     
#endif /* _XedwList_h */

