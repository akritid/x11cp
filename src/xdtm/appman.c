/*****************************************************************************
 ** File          : appman.c                                                **
 ** Purpose       : Application Manager                                     **
 ** Author        : Edward Groenendaal                                      **
 ** Date          : 19th Feb 1991                                           **
 ** Documentation : Xedw Design Folder                                      **
 ** Related Files : menus.c                                                 **
 ** Changes       : 28-11-91, Edward Groenendaal                            **
 **                 Merged 1.8 and 2.0b to make this file                   **
 **                 Jon added loads of process list stuff in here, mostly   **
 **                 at the end of the file. #define DEBUG for his debugging **
 **                 messages.                                               **
 **                 29-3-92, Edward Groenendaal                             **
 **                 Fixed a few bugs with the ProcessList.                  **
 **                 18-4-92, Edward Groenendaal                             **
 **                 Added all the #if NeedFunctionPrototypes stuff so non   **
 **                 ANSI C compilers can be used.                           **
 **                 June 20, 1992, Ramon Santiago                           **
 **                 Changed all XtCreate calls to XtVaCreate calls.         **
 **                 Changed all caddr_t to XtPointer.                       **
 **                 Removed some lint.                                      **
 *****************************************************************************/

#include "xdtm.h"
#include "menus.h"		/* For the use of Trash, Copy, and Move */
#include "parse.h"              /* For access to AppSelection */
#include "Ext/appl.h"

#include <X11/Xaw/MenuButton.h> /* Needed for selection menu */
#include <X11/Xaw/SimpleMenu.h>
#include <X11/Xaw/Viewport.h>
#include <X11/Xaw/Command.h>
#include <X11/Xaw/Label.h>
#include "Xedw/XedwForm.h"
#include "Xedw/XedwList.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/types.h>		/* For opening and closing file descriptors */
#include <sys/stat.h>		/* For opening and closing file descriptors */
#include <sys/wait.h>
#include <sys/time.h>
#ifdef USE_TERMIOS
#include <termios.h>
#else
#include <termio.h>
#endif

#ifdef hpux
#  define TIOCNOTTY         _IO('t', 113)	     /* void tty association */
#include <sys/bsdtty.h>
#include <sys/ioctl.h>
#endif

#ifndef USG
#include <sys/resource.h>       /* Interactive UNIX does not have this */
#endif

#ifndef TRUE_SYSV
#include <sys/file.h>		/* For access(2) */
#endif

#ifdef linux
#define FAPPEND O_APPEND
#define FNDELAY O_NDELAY
#endif

/* external and forward functions definitions */
#if NeedFunctionPrototypes
  extern void AttachProcessToTermWindow(Widget, String, FILE*, int, int);
  public String build_arguments(String, SelOptions);
  extern void button_dialog(Cardinal, XedwListReturnStruct *);
  extern void button_selected(Widget, Cardinal, XtPointer); 
  extern void changestate(Boolean);
  public void child_died(void);
  extern Cardinal count_chr(String, char);
  private void checkMissedPid(void);
  private Boolean decrement_counter(int pid);
  extern void destroy_button_dialog(void);
  extern Boolean directoryManagerNewDirectory(String);
  public int  execute(String, String, String, Boolean, AppProgram *);
  private void freeReturnStruct(void);
  extern String getfilename(String);
  private void increment_counter(AppProgram*, int, char*);
  extern void ioerr_dialog(int errno);
  extern int openMasterAndSlave(int *, int *);
  void pl_select_made(Widget, XtPointer, XtPointer);
  private int plistcmp(const void *, const void *);
  void popdown_kill(Widget, XtPointer, XtPointer);
  private void popup_kill();
  public void popup_process_list(void);
  public void popup_verify_app(AppProgram *);
  void process_list_done(Widget, XtPointer, XtPointer);
  void process_list_kill(Widget, XtPointer, XtPointer);
  private void program_selected(Widget, XtPointer, XtPointer); 
  extern void realize_dialog(Widget, Widget, XtGrabKind, XtEventHandler, XtPointer);
  private void refresh_proclist(void);
  public void run_app(AppProgram *, char *, char *, char *);
  public void selectionChange(Widget, Cardinal, XtPointer);
  extern void setCursor(Cursor);
  extern void setscroll(Widget, double);
  void va_yes(Widget, XtPointer, XtPointer);
  void va_no(Widget, XtPointer, XtPointer);
#else
  extern void AttachProcessToTermWindow();
  public String build_arguments();
  extern void button_dialog();
  extern void button_selected(); 
  extern void changestate();
  public void child_died();
  extern Cardinal count_chr();
  private void checkMissedPid();
  private Boolean decrement_counter();
  extern void destroy_button_dialog();
  extern Boolean directoryManagerNewDirectory();
  public int  execute();
  private void freeReturnStruct();
  extern String getfilename();
  private void increment_counter();
  extern void ioerr_dialog();
  extern int openMasterAndSlave();
  void pl_select_made();
  private int plistcmp();
  void popdown_kill();
  private void popup_kill();
  public void popup_process_list();
  public void popup_verify_app();
  void process_list_done();
  void process_list_kill();
  private void program_selected();
  extern void realize_dialog();
  private void refresh_proclist();
  public void run_app();
  public void selectionChange();
  extern void setCursor();
  extern void setscroll();
  void va_yes();
  void va_no();
#endif


/* Include button bitmaps */
#include "bitmaps/Trash.Button"
#include "bitmaps/Copy.Button"
#include "bitmaps/Move.Button"

/* Include Cursor bitmaps */
#include "bitmaps/Copy.Cursor"
#include "bitmaps/Copy.Mask"
#include "bitmaps/Move.Cursor"
#include "bitmaps/Move.Mask"

/* define DEBUG = 1 to get debugging messages for Jon's code. 
#define DEBUG 1
*/

#define maxPids 10
private int            missedPid[maxPids];
public  ProcessList   *process_list;
private XedwList     **proclist;
private Boolean        proclist_visible = FALSE;
private Boolean        kill_popup_done = False;

public  Mode mode;
public  Boolean buttonSensitive;
public  Widget trashButton, copyButton, moveButton;
private XedwListReturnStruct *return_list;
public  Widget appManager;
public  Widget selectionMenu;
private Widget appManagerView;
private Widget appManagerButton;
private Widget buttonForm;
private Cursor copyCursor, moveCursor;
public  Cardinal appman_selection;
private String srcdir, dstdir;
public  String *patharray;
private String path;
public  Cardinal pathsize; 

/* Jon's Widget decl's */
private Widget va_popup;  /* verify app */
private Widget va_form;
private Widget va_label_1;
private Widget va_label_2;
private Widget va_label_3;
private Widget va_command_yes;
private Widget va_command_no;
private Widget va_command_listprocs;
private Widget pl_popup;  /* process list */
private Widget pl_form;
private Widget pl_label_1;
private Widget pl_label_2;
private Widget pl_command_done;
private Widget pl_command_kill;
private Widget pl_viewport;
private Widget pl_list;
private Widget kill_popup; /* kill feedback popup */
private Widget kill_form;
private Widget kill_label_1;
private Widget kill_label_2;
private Widget kill_label_3;
private Widget kill_label_4;
private Widget kill_command_ok;

/* Input, output and stderr filenames */

static char eb_input[255], eb_output[255], eb_error[255];

/* Thank Harald Vogt for persueding me to change my version of strstr
 * to mystrstr, not that you would get a clash if compiled with the
 * correct options.. but better safe than sorry.
 */

#ifdef HAS_STRSTR
#define mystrstr(cs, ct)	strstr(cs, ct)
#else
extern char *mystrstr(
#if NeedFunctionPrototypes
    char *, char *
#endif
);
#endif

/***************************************************************************** 
 * 			createAppManagerWidgets                              *
 *****************************************************************************/
void createAppManagerWidgets(topForm)
Widget topForm;
{
  /* Create and initialise the widgets for the Application Manager and the
   * copy, move, and trash buttons below.
   *
   * - Takes a Widget which will be their parent.
   * + Returns nothing
   */

  Pixmap trash, copy, move;
  XtTranslations appManTranslations;

  /* Translations for the Application Manager.. not quite what I wanted but
   * near enough that it doesn't matter.
   */
  static char defaultTranslations[] =
    		"<Btn1Down>:    Set()\n\
		 <Btn1Up>:      Unset()\n\
	         <Btn1Up>(2):   Notify() Unset()";
  
  strcpy(eb_input, "/dev/null");
  strcpy(eb_output, eb_input);
  strcpy(eb_error, eb_input);

  /* Create Application Manager Selector */
  appManagerButton =
    XtVaCreateManagedWidget(
        "appManagerButton",
	menuButtonWidgetClass,
	topForm,
	    XtNfromVert,         menuBar,
	    XtNrubberWidth,        False,
	    XtNrubberHeight,       False,
	    XtNmenuName, "selectionMenu",
	    XtNtop,           XtChainTop,
	    XtNbottom,     XtChainBottom,
	    XtNleft,         XtChainLeft,
	    XtNright,       XtChainRight,
	    NULL ) ;

  /* Create Menu for Application Manager Selector, the panes are added after 
   * parsing the config file.
   */

  selectionMenu =
    XtVaCreatePopupShell(
        "selectionMenu",
	simpleMenuWidgetClass,
	appManagerButton,
	    NULL ) ;

  /* Create Application Manager Viewport*/

  appManagerView =
    XtVaCreateManagedWidget(
        "appManagerView",
	viewportWidgetClass,
	topForm,
	    XtNfromVert,    appManagerButton,
	    XtNforceBars,               True,
	    XtNallowVert,               True,
	    XtNrubberWidth,            False,
	    XtNwidthLinked, appManagerButton,
	    XtNtop,         XtChainTop,
	    XtNbottom,      XtChainBottom,
	    XtNleft,        XtChainLeft,
	    XtNright,       XtChainRight,
	    NULL ) ;

  /* Create Application Manager List */

  appManager =
    XtVaCreateManagedWidget(
        "appManager",
	xedwListWidgetClass,
	appManagerView,
	    XtNdefaultColumns,  1,
	    XtNforceColumns,    True,
	    XtNshowIcons,       True,
	    NULL ) ;

  /* Action Buttons */

  buttonSensitive = False; /* Start off insensitive */

  /* Create Form to hold buttons in place, and nicely spaced */

  buttonForm =
    XtVaCreateManagedWidget(
        "buttonForm",
	xedwFormWidgetClass,
	topForm,
	    XtNfromVert,    appManagerView,
	    XtNrubberWidth,          False,
	    XtNrubberHeight,         False,
	    XtNtop,          XtChainBottom,
	    XtNwidthLinked, appManagerView,
	    XtNborderWidth,              0,
	    XtNdefaultDistance,          0,
	    NULL ) ;

  /* Create the Pixmap for the trash button */
   trash = XCreateBitmapFromData(XtDisplay(topForm),
				RootWindowOfScreen(XtScreen(topForm)),
				trash_bits,
				trash_width, trash_height);

  /* Create Trash Button */

  trashButton =
    XtVaCreateManagedWidget(
        "trashButton",
	commandWidgetClass,
	buttonForm,
	    XtNhighlightThickness,       1,
	    XtNsensitive,            False,
	    XtNbitmap,               trash,
	    NULL ) ;

  /* Create the Pixmap for the move button */
  move = XCreateBitmapFromData(XtDisplay(topForm),
				RootWindowOfScreen(XtScreen(topForm)),
				move_bits,
				move_width, move_height);

  /* Create the Move Button */

  moveButton =
    XtVaCreateManagedWidget(
        "moveButton",
	commandWidgetClass,
	buttonForm,
	    XtNhighlightThickness,       1,
	    XtNsensitive,            False,
	    XtNfromHoriz,      trashButton,
	    XtNhorizDistance,            6,
	    XtNbitmap,                move,
	    NULL ) ;

  /* Create Pixmap for the copy button */
  copy = XCreateBitmapFromData(XtDisplay(topForm),
				RootWindowOfScreen(XtScreen(topForm)),
				copy_bits,
				copy_width, copy_height);

  /* Create the Copy Button */

  copyButton =
    XtVaCreateManagedWidget(
        "copyButton",
	commandWidgetClass,
	buttonForm,
	    XtNhighlightThickness,       1,
	    XtNsensitive,            False,
	    XtNfromHoriz,        moveButton,
	    XtNhorizDistance,             6,
	    XtNbitmap,                 copy,
	    NULL ) ;

  /* Add the Callbacks for the Buttons */

  XtAddCallback(trashButton, XtNcallback,
		(XtCallbackProc)button_selected, (XtPointer)Trash);
  XtAddCallback(copyButton, XtNcallback,
		(XtCallbackProc)button_selected, (XtPointer)Copy);
  XtAddCallback(moveButton, XtNcallback,
		(XtCallbackProc)button_selected, (XtPointer)Move);
  
  /* Add Callback and translations for the Application Manager */

  XtAddCallback(appManager, XtNcallback,
		(XtCallbackProc)program_selected, (XtPointer)0);
  appManTranslations = XtParseTranslationTable(defaultTranslations);
  XtUninstallTranslations(appManager);
  XtOverrideTranslations(appManager, appManTranslations);
}

/***************************************************************************** 
 * 		          	 initAppManager                              *
 *****************************************************************************/
public void initAppManager(w)
Widget w;
{
  /* Get the cursors, setup the path and signal handler for catching the
   * death of children. (Thanks to Christos Zoulas, Cornell for suggesting
   * the use of the signal handler).
   *
   * - Takes a Widget (any old widget will do!)
   * + Returns nothing
   */

  extern Widget menuBar;

  Widget       listprocmenu;
  Pixmap       cursor, mask;
  XColor       foreground, background;
  Colormap     def_cmap;
  String       pathptr;
  Cardinal     n;
  Dimension    height, space;
  XFontStruct *font;

  /* set selection in label */
  selectionChange(w, 0, NULL);

  /* get the copy and move cursors */
  def_cmap = DefaultColormapOfScreen(XtScreen(w));

  XParseColor(XtDisplay(w), def_cmap, "Black", &foreground);
  XParseColor(XtDisplay(w), def_cmap, "White", &background);
  XAllocColor(XtDisplay(w), def_cmap, &foreground);
  XAllocColor(XtDisplay(w), def_cmap, &background);

  cursor = XCreateBitmapFromData(XtDisplay(w),
				 RootWindowOfScreen(XtScreen(w)),
				 CopyC_bits,
				 CopyC_width, CopyC_height);

  mask   = XCreateBitmapFromData(XtDisplay(w),
				 RootWindowOfScreen(XtScreen(w)),
				 CopyM_bits,
				 CopyM_width, CopyM_height);

  copyCursor = XCreatePixmapCursor(XtDisplay(w), 
				   cursor, mask,
				   &foreground, &background,
				   CopyC_x_hot, CopyC_y_hot);

  XFreePixmap(XtDisplay(w), cursor);
  XFreePixmap(XtDisplay(w), mask);

  cursor = XCreateBitmapFromData(XtDisplay(w),
				       RootWindowOfScreen(XtScreen(w)),
				       MoveC_bits,
				       MoveC_width, MoveC_height);

  mask   = XCreateBitmapFromData(XtDisplay(w),
				       RootWindowOfScreen(XtScreen(w)),
				       MoveM_bits,
				       MoveM_width, MoveM_height);

  moveCursor = XCreatePixmapCursor(XtDisplay(w), 
				   cursor, mask,
				   &foreground, &background,
				   MoveC_x_hot, MoveC_y_hot);

  XFreePixmap(XtDisplay(w), cursor);
  XFreePixmap(XtDisplay(w), mask);

  mode = NormalMode; /* Start in Normal Mode */

  /* Get path and put it into patharray, for use by execute to find programs */
  if ((path = (String) getenv("PATH")) == NULL) {
    fprintf(stderr, "Warning: PATH environment variable not set\n");
  } else path = XtNewString(path);
  
  patharray = (String*) XtMalloc ((count_chr(path, ':')+1) * sizeof(String));

  /* Extract the directories from the path into the path array,
   * after many complaints the warning about non-fully qualified directories
   * has been removed!
   */
  n = 0;

  pathptr = strtok(path, ":");
  while (pathptr != NULL) {
    if (*pathptr == '/')
      patharray[n++] = pathptr;
    pathptr = strtok(NULL, ":");
  }
  
  pathsize = n;

  /* Set up the process list to keep track of our kids. */
  process_list = NULL;

  if ((listprocmenu = 
       XtNameToWidget(menuBar, "fileMenuButton.fileMenu.listprocs")) == NULL) {
    fprintf(stderr, "initAppManager: Can't find list proc menu widget\n");
    exit(2);
  }
  XtSetSensitive(listprocmenu, False);

  /* Set up our process list related widgets */
  va_popup =
    XtVaCreatePopupShell(
        "Verify",
	transientShellWidgetClass,
	w,
	    NULL ) ;

  va_form =
    XtVaCreateManagedWidget(
        "va_form",
	xedwFormWidgetClass,
	va_popup,
            NULL ) ;

  va_label_1 =
    XtVaCreateManagedWidget(
        "va_label_1",
	labelWidgetClass,
	va_form,
	    XtNborderWidth, 0,
	    XtNresizable, True,
	    NULL ) ;

  va_label_2 =
    XtVaCreateManagedWidget(
        "va_label_2",
	labelWidgetClass,
	va_form,
	    XtNborderWidth, 0,
	    XtNfromVert, va_label_1,
	    XtNresizable, True,
	    NULL ) ;

  va_label_3 =
    XtVaCreateManagedWidget(
        "va_label_3",
	labelWidgetClass,
	va_form,
	    XtNborderWidth, 0,
	    XtNfromVert, va_label_2,
	    NULL ) ;

  va_command_yes =
    XtVaCreateManagedWidget(
        "va_command_yes",
	commandWidgetClass,
	va_form,
	    XtNfromVert, va_label_3,
	    XtNlabel, "Yes",
	    NULL ) ;

  va_command_no =
    XtVaCreateManagedWidget(
        "va_command_no",
	commandWidgetClass,
	va_form,
	    XtNfromVert, va_label_3,
	    XtNlabel, "No",
	    XtNfromHoriz, va_command_yes,
	    NULL ) ;

  XtAddCallback(va_command_no, XtNcallback,
		(XtCallbackProc)va_no, (XtPointer)0);

  va_command_listprocs =
    XtVaCreateManagedWidget(
        "va_command_listprocs",
	commandWidgetClass,
	va_form,
	    XtNfromVert, va_label_3,
	    XtNlabel, "List Processes",
	    XtNfromHoriz, va_command_no,
	    NULL ) ;

  XtAddCallback(va_command_listprocs, XtNcallback,
		(XtCallbackProc)popup_process_list, (XtPointer)0);

  pl_popup =
    XtVaCreatePopupShell(
        "Process List",
	transientShellWidgetClass,
	w,
	    NULL ) ;

  pl_form =
    XtVaCreateManagedWidget(
        "pl_form",
	xedwFormWidgetClass,
	pl_popup,
	    NULL ) ;

  pl_label_1 =
    XtVaCreateManagedWidget(
        "pl_label_1",
	labelWidgetClass,
	pl_form,
	    XtNjustify, XtJustifyCenter,
	    XtNfullWidth,          True,
	    XtNborderWidth,           0,
	    XtNlabel, "Process List",
	    NULL ) ;

  pl_label_2 =
    XtVaCreateManagedWidget(
        "pl_label_2",
	labelWidgetClass,
	pl_form,
	    XtNjustify, XtJustifyCenter,
	    XtNfullWidth,          True,
	    XtNborderWidth,         0,
	    XtNlabel, "Choose process to kill",
	    XtNfromVert,   pl_label_1,
	    NULL ) ;

  pl_viewport =
    XtVaCreateManagedWidget(
        "pl_viewport",
	viewportWidgetClass,
	pl_form,
	    XtNfromVert,   pl_label_2,
	    XtNfullWidth,        True,
	    XtNforceBars,        True,
	    XtNallowVert,        True,
	    NULL ) ;

  pl_list =
    XtVaCreateManagedWidget(
        "pl_list",
	xedwListWidgetClass,
	pl_viewport,
	    XtNdefaultColumns,  1,
	    XtNforceColumns,    True,
	    XtNrowSpacing,      4,
	    XtNsensitive,       True,
	    XtNmSelections,      False,
	    NULL ) ;

  /* Get font height from filelist, then set fileview to be 5 times that
   * size.
   */

  XtVaGetValues(
      pl_list,
          XtNfont, &font,
	  XtNrowSpacing, &space,
	  NULL ) ;

  height = (font->max_bounds.ascent +
           font->max_bounds.descent +
	   space) * 5;

  XtVaSetValues(
      pl_viewport,
          XtNheight, height,
	  NULL ) ;

  pl_command_done =
    XtVaCreateManagedWidget(
        "pl_command_done",
	commandWidgetClass,
	pl_form,
	    XtNfromVert,    pl_viewport,
	    XtNjustify, XtJustifyCenter,
	    XtNlabel, "Done",
	    NULL ) ;

  pl_command_kill =
    XtVaCreateManagedWidget(
        "pl_command_kill",
	commandWidgetClass,
	pl_form,
	    XtNfromVert,    pl_viewport,
	    XtNjustify, XtJustifyCenter,
	    XtNfromHoriz,   pl_command_done,
	    XtNwidthLinked, pl_command_done,
	    XtNlabel, "Kill",
	    XtNsensitive, False,
	    NULL ) ;

  kill_popup =
    XtVaCreatePopupShell(
        "Kill Process",
	transientShellWidgetClass,
	w,
	    NULL ) ;

  kill_form =
    XtVaCreateManagedWidget(
        "kill_form",
	xedwFormWidgetClass,
	kill_popup,
	    NULL ) ;

  kill_label_1 =
    XtVaCreateManagedWidget(
        "kill_label_1",
	labelWidgetClass,
	kill_form,
	    XtNjustify, XtJustifyCenter,
	    XtNfullWidth,          True,
	    XtNborderWidth,           0,
	    XtNlabel, "Couldn't kill process",
	    NULL ) ;

  kill_label_2 =
    XtVaCreateManagedWidget(
        "kill_label_2",
        labelWidgetClass,
	kill_form,
	    XtNjustify, XtJustifyCenter,
	    XtNfullWidth,          True,
	    XtNborderWidth,         0,
	    XtNfromVert,   kill_label_1,
	    NULL ) ;

  kill_label_3 =
    XtVaCreateManagedWidget(
        "kill_label_3",
	labelWidgetClass,
	kill_form,
	    XtNjustify, XtJustifyCenter,
	    XtNfullWidth,          True,
	    XtNborderWidth,         0,
	    XtNfromVert,   kill_label_2,
	    NULL ) ;

  kill_label_4 =
    XtVaCreateManagedWidget(
        "kill_label_4",
	labelWidgetClass,
	kill_form,
	    XtNjustify, XtJustifyCenter,
	    XtNfullWidth,          True,
	    XtNborderWidth,         0,
	    XtNfromVert,   kill_label_3,
	    NULL ) ;

  kill_command_ok =
    XtVaCreateManagedWidget(
        "kill_command_ok",
	commandWidgetClass,
	kill_form,
	    XtNlabel, "Ok",
	    XtNfromVert, kill_label_4,
	    NULL ) ;

  XtAddCallback(kill_command_ok, XtNcallback,
		(XtCallbackProc)popdown_kill, (XtPointer)0);

  /* setup the signal handler for catching the childrens deaths */

#ifdef SIGCLD
  signal(SIGCLD, child_died);	/* Old */
#else
  signal(SIGCHLD, child_died);	/* New */
#endif

}

/*****************************************************************************
 *                            button_selected                                *
 *****************************************************************************/
/*ARGSUSED*/
public void button_selected(w, type, call_data)
Widget w;
Cardinal type;
XtPointer call_data;
{
  /* One of the copy, move or delete buttons has been pressed, if 
   * there are any files selected then do the stuff.
   *
   * - Takes a Widget, the Button type and call_data which is ignored 
   * + Returns nothing
   */

  extern String cwd;
  extern Cursor left_ptr;
  extern Widget directoryManager;
  extern Boolean buttonSensitive;

  static Boolean copyfirst = True; /* first time button selected */
  static Boolean movefirst = True; /* first time button selected */
  XedwListReturnStruct *tmp;
  String tmpstring;

  /* Get list of currently highlighted items */
  if (movefirst == True && copyfirst == True) {
    return_list = XedwListShowCurrent(directoryManager);
    /* extract filenames from rest of data */
    tmp = return_list;
    while ((return_list->xedwList_index != XDTM_LIST_NONE) && (tmp != NULL)) {
      /* We have to deal with a great memory leak/bug in Long mode here:
	 just copy the filename part of tmp->string, then free the whole
	 tmp->string, and make it points at the copy of filename part */
      tmpstring = XtNewString(getfilename(tmp->string));
      XtFree(tmp->string);
      tmp->string = tmpstring;
      tmp = tmp->next;
    }
  }
  
  /* if non empty call dialog */
  if (!(return_list->xedwList_index == XDTM_LIST_NONE &&
      movefirst == True && copyfirst == True)) {
    switch(type) {
    case Trash:
      changestate(False);
      button_dialog(type, return_list); /* The dialog does the rest */
      break;
    case Copy:
      if (copyfirst == True) {
	/* deselect all */
	XedwListUnhighlight(directoryManager, XedwAll);
	setCursor(copyCursor);
	mode = CopyMode;
	changestate(False);
	srcdir = XtNewString(cwd); 	/* Remember src dir */
	copyfirst = False;
      } else {				/* Second press, copy files */
	setCursor(left_ptr);
	mode = NormalMode;
	buttonSensitive = True; /* hack to force buttons insensitive */
	changestate(False);
	dstdir = XtNewString(cwd);
	button_dialog(type, return_list);
	copyfirst = True;
      }
      break;
    case Move:
      if (movefirst == True) {
	/* deselect all */
	XedwListUnhighlight(directoryManager, XedwAll);
	setCursor(moveCursor);
	mode = MoveMode;
	changestate(False);
	srcdir = XtNewString(cwd); 	/* Remember src dir */
	movefirst = False;		
      } else {
	setCursor(left_ptr);		/* Second press, move files */
	mode = NormalMode;
	buttonSensitive = True; /* hack to force buttons insensitive */
	changestate(False);
	dstdir = XtNewString(cwd);
	if (strcmp(srcdir, dstdir) != 0)
	  button_dialog(type, return_list);
	movefirst = True;
      }
      break;
    }
  }
  else
    /* Nothing selected - impossible error, alert user */
    alert_dialog("Impossible error!", "Please contact authors", NULL);

}

/*****************************************************************************
 *                           trashQueryResult                                *
 *****************************************************************************/
/*ARGSUSED*/
public void trashQueryResult(w, delete, call_data)
Widget w;
Boolean delete;
XtPointer call_data;
{
  /*   The action routine called by the Trash dialog,
   *   if the person selected delete then try to delete those files,
   *   otherwise do nothing.
   *
   * - Takes a Widget, A Boolean saying whether to delete the files, and
   *   some call_data, ignored.
   * + Returns nothing
   */
  extern Cursor busy, left_ptr;
  extern String cwd;

  String rmstring;
  int status, count = 0;
  XedwListReturnStruct *tmp;

  setCursor(busy);

  /* destroy dialog */
  destroy_button_dialog();

  if (delete == True) {
    /* delete files */

    /* I can't be bothered writing my own remove routine so I'll call
     * 'rm(1)' via execute instead.
     */
    rmstring = XtNewString("rm -fr");
    tmp = return_list;
    while (tmp != NULL) {
      rmstring = (String) XtRealloc (rmstring, sizeof(char) * 
				     (strlen(rmstring) +
				      strlen(tmp->string) + 5));
      sprintf(rmstring, "%s '%s'", rmstring, tmp->string);
      tmp = tmp->next;
      count++;
    }
    if ((status = execute(NULL, "rm", rmstring, True, NULL)) != 0) {
      if (count == 1) 
	alert_dialog("Sorry, you can't remove",  "that file", NULL); /* Remove failed */
      else
	alert_dialog("Sorry, one or more files", "could not be removed", NULL);
    }
    XtFree((char *)rmstring);
    /* Delay so that systems using 
     * NFS have time to update the directory table.
     */
    sleep(app_data.delay);
    /* refresh directory (clear highlights as a side effect) */
    directoryManagerNewDirectory(cwd);
  } else {
    /* leave list highlighted, make buttons sensitive again */
    changestate(True);
  }

  setCursor(left_ptr);

  /* free memory for list */
  freeReturnStruct();

}

/*****************************************************************************
 *                         copyQueryResult                                   *
 *****************************************************************************/
/*ARGSUSED*/
public void copyQueryResult(w, copy, call_data)
Widget w;
Boolean copy;
XtPointer call_data;
{
  /* The action routine called by the copy dialog, 
   * if the user selected the copy button, copy the files,
   * otherwise do nothing.
   *
   * - Takes a widget, copy - whether to copy files, call_data - ignored
   * + Returns Nothing
   */

  extern Cursor  busy, left_ptr;
  extern String  cwd;

  String         copystring;
  int            status, count = 0;
  Cardinal       srclen, dstlen;
  XedwListReturnStruct *tmp;

  destroy_button_dialog();

  setCursor(busy);

  if (copy == True) {
    /* copy files */
    
    /* I can't be bothered writing my own copy routine so I'll call
     * 'cp(1)' via execute instead.
     */
    srclen = strlen(srcdir);
    dstlen = strlen(dstdir);
    copystring = XtNewString("sh -c '(cd");
    copystring = (String) XtRealloc (copystring, sizeof(char) * 
				     (strlen(copystring) +
				      srclen + 12));
    sprintf(copystring, "%s %s;tar cf -", copystring, srcdir);
    tmp = return_list;
    while (tmp != NULL) {
      copystring = (String) XtRealloc (copystring, sizeof(char) * 
				       (strlen(copystring) +
					strlen(tmp->string) + 3));
      sprintf(copystring, "%s %s", copystring, tmp->string);
      tmp = tmp->next;
      count++;
    }
    copystring = (String) XtRealloc (copystring, sizeof(char) *
				     (strlen(copystring) +
				      dstlen + 70));

    sprintf(copystring, "%s)|(cd %s;f=/tmp/$$;tar xmf - 2>$f;[ ! -s $f ];s=$?;rm $f;exit $s)'", copystring, dstdir);
    if ((status = execute(NULL, "sh", copystring, True, NULL)) != 0) {
      if (count == 1)
	alert_dialog("Sorry, that file could","not be copied", NULL);
      else
	alert_dialog("Sorry, one or more files", "could not be copied", NULL);
    }
    XtFree((char *)copystring);
    /* Delay so that systems using 
     * NFS have time to update the directory table.
     */
    sleep(app_data.delay);
    /* refresh directory (clear highlights as a side effect) */
    directoryManagerNewDirectory(cwd);
  } /* else do nothing, buttons have already been turned insensitive by
       button_selected */
    
  XtFree((char *)srcdir);
  XtFree((char *)dstdir);
  setCursor(left_ptr);
  freeReturnStruct();
}
/*****************************************************************************
 *                           moveQueryResult                                 *
 *****************************************************************************/
/*ARGSUSED*/
public void moveQueryResult(w, move, call_data)
Widget w;
Boolean move;
XtPointer call_data;
{
  /* Action called when option selected in move dialog, 
   * if move was selected move the file, otherwise do nothing.
   */

  extern Cursor  busy, left_ptr;
  extern String  cwd;

  String         movestring, rmstring;
  Cardinal       srclen, dstlen;
  int            status, count = 0;
  XedwListReturnStruct *tmp;

  destroy_button_dialog(); /* Get rid of dialog */

  setCursor(busy);

  if (move == True) {
    /* move files */
    
    /* I can't be bothered writing my own move routine so I'll call
     * 'mv(1)' via execute instead.
     * NOTE: This has been changed to tar cf srcdir/file | (cd dstdir; tar xf -)
     *       so moves across file-systems will work.
     */
    srclen = strlen(srcdir);
    dstlen = strlen(dstdir);
    movestring = XtNewString("sh -c 'cd");
    rmstring = XtNewString("rm -rf");
    movestring = (String) XtRealloc (movestring, sizeof(char) *
				     (strlen(movestring) + srclen + 12));
    sprintf(movestring, "%s %s;tar cf -", movestring, srcdir);
    tmp = return_list;
    while (tmp != NULL) {
      movestring = (String) XtRealloc (movestring, sizeof(char) * 
				       (strlen(movestring) +
					strlen(tmp->string) + 3));
      sprintf(movestring, "%s %s", movestring, tmp->string);
      rmstring = (String) XtRealloc (rmstring, sizeof(char) *
				     (strlen(rmstring) + srclen +
				      strlen(tmp->string) + 3));
      sprintf(rmstring, "%s %s/%s", rmstring, srcdir, tmp->string);
      tmp = tmp->next;
      count++;
    }
    movestring = (String) XtRealloc (movestring, sizeof(char) *
				     (strlen(movestring) +
				      dstlen + 70));

    sprintf(movestring, "%s|(cd %s;f=/tmp/$$;tar xf - 2>$f;[ ! -s $f ];s=$?;rm $f;exit $s)'", movestring, dstdir);

    if ((status = execute(NULL, "sh", movestring, True, NULL)) != 0) {
      if (count == 1)
	alert_dialog("Sorry, that file could", "not be moved", NULL);
      else
	alert_dialog("Sorry, one or more files", "could not be moved", NULL);
    } else if ((status = execute(NULL, "rm", rmstring, True, NULL)) != 0) {
      if (count == 1)
	alert_dialog("Be aware that that file could not",
		     "be removed from origin directory", NULL);
      else
	alert_dialog("Be aware that one or more files could",
		     "not be removed from origin directory", NULL);
    }
    XtFree((char *)movestring);
    XtFree((char *)rmstring);
    /* Delay so that systems using 
     * NFS have time to update the directory table.
     */
    sleep(app_data.delay);
    /* refresh directory (clear highlights as a side effect) */
    directoryManagerNewDirectory(cwd);
  } /* else do nothing, buttons have already been turned insensitive by
       button_selected */
    
  XtFree((char *)srcdir);
  XtFree((char *)dstdir);
  setCursor(left_ptr);
  freeReturnStruct();
}

/*****************************************************************************
 *                           freeReturnStruct                                *
 *****************************************************************************/
private void freeReturnStruct()
{
  /* Deallocate memory for the current list return structure.
   */
  XedwListFreeCurrent(return_list);
}
  
/*****************************************************************************
 *                           selectionChange                                 *
 *****************************************************************************/
/*ARGSUSED*/
public void selectionChange(w, selection, crap)
Widget w;
Cardinal selection;
XtPointer crap;
{
  /* Action called when a selection menu pane is chosen
   * Change the current selection list to that of 'selection'
   */

  extern AppSelection **appselections;

  AppProgram          **old_list;
  XedwList            **list;
  Cardinal              i, max;

  XtVaGetValues(
      appManager,
          XtNxedwList, &list,
	  XtNnumberStrings, &max,
	  NULL ) ;

  if (list != NULL) {
	  for(i=0; i < max && list[i]; i++)
	      XtFree((char *)list[i]);
	  XtFree((char *)list);
  }
  appman_selection = selection;

  if (appselections[appman_selection] != NULL) {
      XtVaSetValues(
		    appManagerButton,
		    XtNlabel, appselections[appman_selection]->name,
		    NULL ) ;

      /* Change contents of AppManager to that of the new selection*/
      old_list = appselections[appman_selection]->list;
      max = appselections[appman_selection]->number;
      list = (XedwList **) XtMalloc((max+1) * sizeof(XedwList *));
      for(i=0; i< max; i++) {
	  list[i] = (XedwList *) XtMalloc( sizeof(XedwList));
	  list[i]->icon = old_list[i]->icon;
	  list[i]->mask = old_list[i]->mask;
	  list[i]->string = old_list[i]->string;
      }
      list[i] = NULL;
      XedwListChange(appManager, list, max, 0, True);
  }

  /* Reset scrollbar to top */
  setscroll(appManagerView, 0.0);
}

/*****************************************************************************
 *                              program_selected                             *
 *****************************************************************************/
/*ARGSUSED*/
private void program_selected(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
  /* Action called when an icon in the selection list is double clicked.
   * it executes the program.
   *
   * - All arguments ignored
   */

  extern AppSelection **appselections;

  XedwListReturnStruct *list;
  AppProgram           *node;
  Cardinal              index;

  /* Get the index of the program double clicked,
   * Thanks to David Byers for spotting a bug here in v1.0 
   */
  list = XedwListShowCurrent(w);
  if (list->xedwList_index != XDTM_LIST_NONE) {
    index = list->xedwList_index;
    node = appselections[appman_selection]->list[index];
    if (node->count) 
      popup_verify_app(node);
    else
      run_app(node, NULL, NULL, NULL);
  }
  XedwListFreeCurrent(list);
}

/*****************************************************************************
 *                               run_app                                     *
 *****************************************************************************/
public void run_app(node, input, output, error)
AppProgram *node;
char *input;
char *output;
char *error;
{
  extern Cursor busy;

  String program, filename, fullprogram;

  setCursor((Cursor)busy);

  if (input)
    strcpy(eb_input, input);
  if (output)
    strcpy(eb_output, output);
  if (error)
    strcpy(eb_error, error);

  program = XtNewString(node->program);
  
  /* extract filename from program */
  filename = XtNewString(program);
  filename = strtok(filename, " ");
  
  /* check to see if there are any highlighted files to add as arguments, 
   * if so check to see where the insertion point is, if no insertion 
   * point then append arguments.
   */
  if (node->options == IGNORE_SEL) {
    execute(program, filename, program, False, node);
  } else
    if ((fullprogram = build_arguments(program, node->options)) != NULL) {
      /* execute program */
      execute(NULL, filename, fullprogram, False, node);
      XtFree(fullprogram);
    }
  XtFree(program);
  XtFree(filename);
  setCursor((Cursor)NULL);
}

/*****************************************************************************
 *                                execute                                    *
 *****************************************************************************/
#if NeedFunctionPrototypes
public int  execute(String fullname, String filename, String fullcmd,
		    Boolean cwait, AppProgram *node)
#else
public int execute(fullname, filename, fullcmd, cwait, node)
     String fullname;
     String filename;
     String fullcmd;
     Boolean cwait;
     AppProgram *node;
#endif
{
    /* This procedure executes a program. by specifying cwait to true
     * the parent will wait for the child to terminate, then return 
     * the return value of the child.
     *
     * - Takes the full path of the program to be executed, maybe NULL.
     *             filename of the program to be executed,
     *             program argument list, e.g. "emacs filename",
     *             whether to wait for the child to terminate,
     *             wether to start the sub-process through a pseudo terminal.
     * + returns the return value from the child if cwait is set 
     *       (0 if OK, <> 0 otherwise)
     */
    
    typedef enum {ready, quote, normal, quoteready} QModes;
    
    QModes mode;
    Cardinal i, n, arglen;
#ifdef USE_TERMIOS
    struct termios Termio;
#else /* termio */
    struct termio Termio;
#endif
    int           result, pid, fd;
    int           status = 0;
    int  	  master;	       /* file descriptor of master pty */
    int  	  slave; 	       /* file descriptor of slave pty */
#ifndef USE_TERMIOS
    int	          pgrp;		       /* process group id */
#endif
    FILE         *processfp = NULL;    /* file pointer to process */
    String *newargs, args;
#if !defined(SYSV) && !defined(SVR4_0) && !defined(_POSIX_SOURCE) && !defined(__386BSD__)
    union wait       w_stat;   /* I'm not sure about the SYSV bit... */
#else
    int              w_stat;
#endif

  if (!filename) return -1;
  if (fullname == NULL || fullname[0] != '/') {
      /* Find program */
      result = -1;

      /* Note: assume that pathsize is greater than 0, otherwise segv... */
      for (i = 0; i < pathsize && result != 0; i++) {
	  /* append filename to path */
	  fullname = (String) XtRealloc (fullname, ((strlen(patharray[i])+
						     strlen(filename) + 5)
						    * sizeof(char)));
	  strcpy(fullname, patharray[i]);
	  strcat(fullname, "/");
	  strcat(fullname, filename);
	  
	  result = access(fullname, X_OK); /* Does file exist,
					      Is file executable*/
      }
	
    if (result != 0) {
      
      /* OK Let's try the filename on it's own.. */
      fullname = strcpy(fullname, filename);
      result = access(fullname, X_OK);
      
      if (result != 0) {
	fprintf(stderr, "Warning: Command '%s' not found in PATH\n", filename);
	return -1;
      }
    }
  } else {
      /* reallocate fullname to be able to free it in any case */
      fullname = XtNewString(fullname);
  }
    
    /* keep a copy of full command passed in args */
    args = XtNewString(fullcmd);
    
    /* split the args string into a NULL terminated array of strings. 
     * The number of arguments is always less than or equal to the number
     * of spaces in the args + one.
     * If a string of characters is enclosed within quotes, it is counted
     * as a single argument. 
     * Maybe not pretty - but fast.
     */
    
    newargs = (char **) XtMalloc (sizeof(char*) * (count_chr(args, ' ') + 2));
    
    n = 0;
    mode = ready;
    arglen = strlen(args);
    for (i = 0; i < arglen; i++) {
	switch (*(args+i)) {
	case '\'':
	    if (mode == normal || mode == ready) {
		/* start a new arg on the next normal chr */
		mode = quoteready;
	    } else if (mode == quote || mode == quoteready) {
		/* close current quote */
		*(args+i) = '\0';
		mode = ready;
	    }
	    break;
	case ' ':
	case '\t':
	    if (mode == normal) {
		/* terminate current arg */
		*(args+i) = '\0';
		mode = ready;
	    }
	    break;
	default:
	    if (mode == ready || mode == quoteready) {
		/* start a new arg */
		newargs[n++] = args+i;
		if (mode == ready)
		  mode = normal;
		else
		  mode = quote;
	    }
	    break;
	}
    }
    
    if (cwait)
#ifdef SIGCLD
      signal(SIGCLD,  SIG_DFL);
#else
      signal(SIGCHLD, SIG_DFL);
#endif
    
    newargs[n] = NULL;
    
    if (node && (node->termopts == TERM))
    {
	/* open pseudo-terminal */
	if (openMasterAndSlave(&master, &slave))
	    {
		alert_dialog("Error: execution failed, no more pty!", 
			     fullcmd, NULL);
		return -1;
	    }
    }
    
    if ((pid = fork()) == -1) 
      fprintf(stderr, "Warning: unable to fork\n");
    else 
      if (pid == 0) {
	  /* Child */
	  if (node && (node->termopts == TERM))
	  {
	      /* 
	       * Child : close master side of pty
	       *         redirect stdin, stdout, stderr of sub-process to pty
	       *	 unbuffer output data from sub-process
	       *	 exec sub-process with arguments
	       */
	      close(master);

	      /*
	       * Modify local and output mode of slave pty
	       */
#ifdef USE_TERMIOS
	      tcgetattr(slave, &Termio);
#else /* termio */
	      ioctl(slave, TCGETA, &Termio);
#endif
	      Termio.c_lflag &= ~ECHO;	/* No echo */
	      Termio.c_oflag &= ~ONLCR;	/* Do not map NL to CR-NL on output */
#ifdef USE_TERMIOS
	      tcsetattr(slave, TCSANOW, &Termio);
#else /* termio */
	      ioctl(slave, TCSETA, &Termio);
#endif
	      dup2(slave, 0);
	      dup2(slave, 1);
	      dup2(slave, 2);
	      if (slave > 2)
		close(slave);
	      fcntl(1, F_SETFL, FAPPEND);
	      setbuf(stdout, NULL);
	      
	      /*
	       * Set our process group to that of the terminal,
	       * so we can change the group of the terminal.
	       */
#ifdef USE_TERMIOS
	      setpgid(0, tcgetpgrp(0));
#else /* termio */
	      ioctl(0, TIOCGPGRP, &pgrp);
	      setpgid(0, pgrp);
#endif
	      
	      /*
	       * Now set the process group of the terminal and of us
	       * to our process id.  This clears us from the control
	       * of the other process group.
	       */
	      pid = getpid();

#ifdef USE_TERMIOS
	      tcsetpgrp(0, pid);
#else /* termio */
	      ioctl(0, TIOCSPGRP, &pid);
#endif
	      setpgid(0, pid);
	  }
	  else
	  {
	      /* Set out input, output and error streams to those requested
		 by the user, or /dev/null if the user didn't change them. */  
	      if (close(0) == -1) 
		fprintf(stderr,
			"Warning: can't close childs file descriptors\n");
	      else 
		if (open(eb_input, O_RDONLY, 0) == -1) {
		    fprintf(stderr,
			    "Warning: can't open %s as new input for child\n",
			    eb_input);
		    ioerr_dialog(errno);
		}
	      
	      if (close(1) == -1) 
		fprintf(stderr,
			"Warning: can't close childs file descriptors\n");
	      else 
		if (open(eb_output, O_WRONLY | O_CREAT, 0644) == -1) {
		    fprintf(stderr,
			    "Warning: can't open %s as new output for child\n",
			    eb_output);
		    ioerr_dialog(errno);
		}
	      
	      if (cwait) 
		if (close(2) == -1) 
		  fprintf(stderr,
			  "Warning: can't close childs file descriptors\n");
		else 
		  if (open(eb_error, O_WRONLY | O_CREAT, 0644) == -1) {
		      fprintf(stderr,
		      "Warning: can't open %s as new error output for child\n",
			      eb_error);
		      ioerr_dialog(errno);
		  }
	      
	      /* close all opened file descriptors (except stdin, stderr,
		 stdout) */
	      for (fd = 3; fd < 20; fd++)
		(void) close(fd); 
	  }
	  
	  execv(fullname, newargs);
	  
	  /* The exec failed, ring the bell then exit */
	  
	  XBell(XtDisplay(appManager), 100);
	  exit(0);
	  
      } else {
	  /* Parent */
	  
	  if (node && (node->termopts == TERM))
	  {
	      /* 
	       * Parent : close the slave side of pty
	       *	  close stdin and stdout
	       *	  set the sub-process file descriptor to nonblocking
	       *            mode
	       *          open file pointer with read/write access to
	       *            sub-process
	       *	  set line buffered mode
	       *	  register sub-process input with X
	       */
	      close(slave);

	      fcntl(master, F_SETFL, FNDELAY);
	      processfp = fdopen(master, "r+");
#if defined(SYSV) || defined(_AUX_SOURCE) 
	      setvbuf(processfp, NULL, _IOLBF, BUFSIZ);
#else /* I assume setlinebuf is defined on non-SYSV and non-A/UX systems */
	      setlinebuf(processfp);
#endif
	      AttachProcessToTermWindow(appManager, fullcmd,
					processfp, master, pid);
	  }
	  
	  /* If cwait is True then wait for the child to finish, then
	   * set status to it's return value.
	   */
	  if (cwait) {
	      wait(&w_stat);
	      if (WIFEXITED(w_stat))          
#ifdef WEXITSTATUS
		status = WEXITSTATUS(w_stat); /* I'm not sure which OS uses */
#else					      /* which! So here is a hack.  */
	        status = w_stat.w_retcode;     
#endif
	      else {
		  status = 1;
	      }

#ifdef SIGCLD
	      signal(SIGCLD,  child_died);
#else
	      signal(SIGCHLD, child_died);
#endif
	  } else {
	      increment_counter(node, pid, filename);
	      
	      refresh_proclist();
	  }
      }

    /* free allocated memory */
    XtFree(args);
    XtFree(fullname);
    XtFree((char *)newargs);

    return status;
}

/*****************************************************************************
 *                           build_arguments                                 *
 *****************************************************************************/
public String build_arguments(program, options)
String program;
SelOptions options;
{
  /* Given the program with it's previous arguments, this procedure
   * will insert any highlighted files into the place specified by
   * the %s or on the end if none found.
   *
   * Fixed problem with 'program' being altered. Edward, 6th June 1991
   *
   * - Takes the program string, and whether the program is allowed to
   *   take arguments, if so how many.
   * + Returns the resultant command line. Should be free'd elsewhere.
   */

  extern Widget directoryManager;

  XedwListReturnStruct *list, *tmplist, *safelist;
  String ptr, tmpptr, arguments, newprogram;
  Cardinal number = 0;

  list = XedwListShowCurrent(directoryManager);  /* Get the list */
  safelist = list;

  arguments = XtNewString(" ");
  if (options != N_SEL) {
    if ((list->xedwList_index == XDTM_LIST_NONE) && 
	(options != A_SEL) && (options != IGNORE_SEL)) {
      /* MSEL and no files selected... */
      if (!app_data.silentsel) 
	alert_dialog("This program requires files", "to be selected", "Cancel");
      newprogram = NULL;
    } else {
      while (list != NULL) {
	ptr = getfilename(list->string);
	arguments = XtRealloc (arguments, sizeof(char) * 
			       (((arguments == NULL) ? 0 : strlen(arguments)) +
				strlen(ptr) + 10));
	strcat(arguments, " ");
	strcat(arguments, ptr);
	tmplist = list;
	list = list->next;
	number++;
      }
      if (options == O_SEL && number != 1 && !app_data.silentsel) {
	alert_dialog("This program requires only",
		     "one file selected", "Cancel");
	newprogram = NULL;
      } else {
	/* insert the arguments into the program string */
	newprogram = XtMalloc (sizeof(char) * (strlen(program) + 
					       strlen(arguments) + 10));
	
	strcpy(newprogram, program);
	if ((ptr = mystrstr(newprogram, "%s")) != NULL) {
	  /* replace %s with arguments */
	  tmpptr = XtNewString(ptr+2);
	  *ptr = '\0';
	  strcat(newprogram, arguments);
	  strcat(newprogram, tmpptr);
	  XtFree((char *)tmpptr);
	} else {
	  /* append arguments to program */
	  strcat(newprogram, arguments);
	}
      }
      XtFree((char *)arguments);
    }
  } else {
    if (list->xedwList_index != XDTM_LIST_NONE && !app_data.silentsel ) {
      alert_dialog("This program does not require",
		   "any files to be selected", "Cancel");
      newprogram = NULL;
    } else 
      newprogram = XtNewString(program); 
  }

  XedwListFreeCurrent(safelist);
  XedwListUnhighlight(directoryManager, XedwAll);
  changestate(False); 

  return newprogram;
}

/*****************************************************************************
 *                             popup_verify_app                              *
 *****************************************************************************/
public void popup_verify_app(node)
AppProgram *node;
{
  char label1[80], label2[80], label3[80];

  if (node->count == 1)
    sprintf(label1, "You already have a copy");
  else 
    sprintf(label1, "You already have %d copies", node->count);

  if (node->options == IGNORE_SEL) {
    /* We've got a dirman node.  Don't show path. */
    char *c;
    c = node->string;
    while (*c++);
    while (*c != '/') c--;
    sprintf(label2, "of %s running.", ++c);
  } else sprintf(label2, "of %s running.", node->string);

  sprintf(label3, "Do you want to start another?");

  XtVaSetValues( va_label_1, XtNlabel, label1, NULL ) ;
  XtVaSetValues( va_label_2, XtNlabel, label2, NULL ) ;
  XtVaSetValues( va_label_3, XtNlabel, label3, NULL ) ;

  XtAddCallback(va_command_yes, XtNcallback, (XtCallbackProc)va_yes,
		(XtPointer)node);

  realize_dialog(va_popup, NULL, XtGrabNonexclusive, NULL, NULL);
}

/*****************************************************************************
 *                                  va_yes                                   *
 *****************************************************************************/
/*ARGSUSED*/
#if NeedFunctionPrototypes
void va_yes(Widget w, XtPointer client_data, XtPointer call_data)
#else
void va_yes(w, client_data, call_data)
     Widget w;
     XtPointer client_data;
     XtPointer call_data;
#endif
{
  XtPopdown(va_popup);
  XtRemoveAllCallbacks(va_command_yes, XtNcallback);
  run_app((AppProgram *)client_data, NULL, NULL, NULL);
}

/*****************************************************************************
 *                                   va_no                                   *
 *****************************************************************************/
/*ARGSUSED*/
#if NeedFunctionPrototypes
void va_no(Widget w, XtPointer client_data, XtPointer call_data)
#else
void va_no(w, client_data, call_data)
     Widget w;
     XtPointer client_data;
     XtPointer call_data;
#endif
{
  XtPopdown(va_popup);
}

/*****************************************************************************
 *                                 popup_kill                                *
 *****************************************************************************/
private void popup_kill()
{
  XtAppContext context;
  realize_dialog(kill_popup, NULL, XtGrabNonexclusive, NULL, NULL);
  context = XtWidgetToApplicationContext(kill_popup);
  while (kill_popup_done == False || XtAppPending(context)) {
    XtAppProcessEvent(context, XtIMAll);
  }
  kill_popup_done = False;
  XtPopdown(kill_popup);
}
/*****************************************************************************
 *                                popdown_kill                               *
 *****************************************************************************/
/*ARGSUSED*/
#if NeedFunctionPrototypes
void popdown_kill(Widget w, XtPointer client_data, XtPointer call_data)
#else
void popdown_kill(w, client_data, call_data)
     Widget w;
     XtPointer client_data;
     XtPointer call_data;
#endif
{
  kill_popup_done = True;
}

/*****************************************************************************
 *                              WM_destroy_process_list                      *
 ****************************************************************************/
#if NeedFunctionPrototypes
private void WM_destroy_process_list(Widget w,
				     XtPointer client_data,
				     XEvent *event,
				     Boolean *dispatch)
#else
private void WM_destroy_process_list(w, client_data, event, dispatch)
     Widget w;
     XtPointer client_data;
     XEvent *event;
     Boolean *dispatch;
#endif
{
    extern Atom protocols[2]; /* defined in dialogs.c */
    
    if (event->xclient.message_type == protocols[1] &&
	event->xclient.data.l[0] == protocols[0])
    /* the widget got a kill signal */
    {
	/* call callbacks on Done button */
	XtCallCallbacks(pl_command_done, XtNcallback, NULL);
    }
}

/*****************************************************************************
 *                             popup_process_list                            *
 *****************************************************************************/
public void popup_process_list()
{

  refresh_proclist();

  XtVaSetValues(
      pl_list,
          XtNlongest,             0,
	  XtNnumberStrings,       0,
	  XtNmSelections,     False,
	  XtNsensitive,        True,
	  NULL ) ;

  XtAddCallback(pl_list, XtNcallback, (XtCallbackProc)pl_select_made,
		(XtPointer)0);
  XtAddCallback(pl_command_done, XtNcallback,
		(XtCallbackProc)process_list_done, (XtPointer)0);
  XtAddCallback(pl_command_kill, XtNcallback,
		(XtCallbackProc)process_list_kill, (XtPointer)0);

  realize_dialog(pl_popup, (Widget)NULL, XtGrabNonexclusive,
		 (XtEventHandler)WM_destroy_process_list, (XtPointer)NULL);
  proclist_visible = TRUE;
}

/*****************************************************************************
 *                                 child_died                                *
 *****************************************************************************/
public void child_died()
{
  /* Signal handler for when a child dies. 
   * waits for the child (stops zombies), rings the Bell, then
   * Resets the signals.
   */
#ifdef DEBUG
#if NeedFunctionPrototypes
  private int  dumpprocess_list(char*);
#else
  private int  dumpprocess_list();
#endif
#endif
  extern String cwd;

  int pid;
#if !defined(SYSV) && !defined(SVR4_0) && !defined(_POSIX_SOURCE)
  union wait status;
#else
  int status;
#endif
 
#ifdef DEBUG
  fprintf(stderr, "child_died: entering\n");
  fflush(stderr);
#endif

#if defined(TRUE_SYSV)
  /* I shall assume that ALL SYSV OS's without BSD do NOT have wait3 */
  pid = wait(&status);
#elif defined(SVR4)            /* SVR4 definitely has waitpid() */
  pid = waitpid((pid_t)-1, &status, WNOHANG);
#else
  pid = wait3(&status, WNOHANG, 0);
#endif
  if (!pid) {
#ifdef DEBUG
    fprintf(stderr, "child_died: <yawn> false alarm, goodnight\n");
#endif
    return;
  }

  if (app_data.bellonexit) XBell(XtDisplay(appManager), 100);

  XFlush(XtDisplay(appManager));

  if (app_data.dironexit == True) 
    directoryManagerNewDirectory(cwd);

#ifdef DEBUG
  fprintf(stderr,"child_died: calling decrement_counter(%d)\n",pid);
  fflush(stderr);
#endif

  decrement_counter(pid);

#ifdef DEBUG
  fprintf(stderr,"child_died: calling dumpprocess_list()\n");
  fflush(stderr);

  dumpprocess_list("child_died: ");

  fprintf(stderr, "child_died: calling refresh_proclist()\n");
  fflush(stderr);
#endif

  refresh_proclist();

#ifdef DEBUG
  fprintf(stderr, "child_died: exiting\n");
  fflush(stderr);
#endif

#ifdef SIGCLD
  signal(SIGCLD, child_died);
#else
  signal(SIGCHLD, child_died);
#endif
}

/*****************************************************************************
 *                             increment_counter                             *
 *****************************************************************************/
private void increment_counter(node, pid, programname)
AppProgram *node;
int pid;
char *programname;
{
  /* If node is NULL then we are executing a program directly, eg via map 
   * otherwise the program is from the application manager.
   */ 

  ProcessList *ptr, *newptr;

  if (node == NULL) {
#ifdef DEBUG
    fprintf(stderr, "increment_counter: making AppProgram for %s.\n", 
	    programname);
#endif
    node = (AppProgram*) XtMalloc(sizeof(AppProgram));
    node->count = node->icon = (Pixmap)NULL;
    node->program = XtNewString(programname);
    node->string  = XtNewString(programname);
    node->options = N_SEL;
  }

  node->count++;
    
  newptr = (ProcessList *) XtMalloc(sizeof(ProcessList));
  newptr->pid = pid;
  newptr->node = node;
  newptr->next = NULL;

  if (!(ptr=process_list)) process_list = newptr;
  else {
    while (ptr->next) ptr = ptr->next;
    ptr->next = newptr;
  }
}

/*****************************************************************************
 *                             checkMissedPid                             *
 *****************************************************************************/
private void checkMissedPid()
{
  /* handle pid of child terminated before they are 
    registered by parent */
  extern Cursor left_ptr;
  int i, count = 0;

#ifdef DEBUG
fprintf(stderr, "Entering checkMissedPid\n");
#endif

  for (i = 0; i < maxPids; i++) {
    if (missedPid[i] == 0) {
      count++;
    } else if (decrement_counter(missedPid[i]) == True) {
      missedPid[i] = 0;
      count++;
    }
  }

  if (count == maxPids) {
    /* no more pid to clear, reset clock alarm,
      proclist and cursor */
    alarm(0);
    refresh_proclist();
    setCursor(left_ptr);
#ifdef DEBUG
fprintf(stderr, "Exiting checkMissedPid, no more pid uncleared\n");
#endif
  }
#ifdef DEBUG
  else
fprintf(stderr, "Exiting checkMissedPid, still %d pid uncleared\n", maxPids - count);
#endif
}

      
/*****************************************************************************
 *                             decrement_counter                             *
 *****************************************************************************/
private Boolean decrement_counter(pid)
int pid;
{
  ProcessList *ptr, *ptr2;
  int i, index = -1;

#ifdef DEBUG
  fprintf(stderr,"#################################\n");
  fprintf(stderr,"# Entering decrement_counter    #\n");
  fprintf(stderr,"#################################\n");
  fflush(stderr);
#endif

  XedwListUnhighlight(pl_list, -1);

  ptr = process_list;
  if (ptr) 
    ptr2 = ptr->next; 
  else 
    ptr2 = NULL;

  if (ptr && (ptr->pid == pid)) {
#ifdef DEBUG
    fprintf(stderr,
	   "\ndecrement_counter: *Program %s instantiation %d terminated.\n\n",
	    ptr->node->string, ptr->node->count);
    fflush(stderr);
#endif
    ptr->node->count--;
    process_list = ptr2;
    /* In case this was a non-app execute, eddyg */
    if (ptr->node->icon == (Pixmap)NULL) {
      XtFree((char *)ptr->node->string);
      XtFree((char *)ptr->node->program);
      XtFree((char *)ptr->node);
    }
    XtFree((char *)ptr);
  } else {
    while(ptr2 && (ptr2->pid != pid)) {
      ptr = ptr2;
      ptr2 = ptr2->next;
    }
    if (!ptr2) {
      for (i=0; i < maxPids; i++) 
	if (missedPid[i] == 0) {
	  if (index == -1) {
	    missedPid[i] = pid;
	    index = i;
	  }
	} else if (missedPid[i] == pid) {
	  if (index != -1) {
	    missedPid[index] = 0;
	  }
	  index = i;
	  break;
	}
	    

      if ((index == -1) && (i == maxPids))
	fprintf(stderr, "out of space in missed pid table\n");
      else {
	signal(SIGALRM, checkMissedPid);
	alarm(1);
	return False;
      }
      /*      fprintf(stderr,
	      "decrement_counter: List traversal error.  Item not found.\n"); */
    } else {
#ifdef DEBUG
      fprintf(stderr,
	      "decrement_counter: Program %s instantiation %d terminated.\n",
	      ptr2->node->string, ptr2->node->count);
#endif
      ptr2->node->count--;
      ptr->next = ptr2->next;
      if (ptr2->node->icon == (Pixmap)NULL) {
	XtFree((char *)ptr2->node->string);
	XtFree((char *)ptr2->node->program);
	XtFree((char *)ptr2->node);
      }   
      XtFree((char *)ptr2);
    }
  }
#ifdef DEBUG
  fprintf(stderr,"#################################\n");
  fprintf(stderr,"# Exiting decrement_counter     #\n");
  fprintf(stderr,"#################################\n");
  fflush(stderr);
#endif

  return True;
}

/*****************************************************************************
 *                            refresh_proclist                               *
 *****************************************************************************/
private void refresh_proclist()
{
#ifdef DEBUG
  private int dumpproclist(
#if NeedFunctionPrototypes
    char *
#endif
);
#endif

  extern ProcessList *process_list;
  extern XedwList **proclist;
  extern Widget menuBar;

  private Widget listprocmenu;
  private Boolean firsttime = True;
  ProcessList *ptr;
  char *tmpstring;
  Cardinal num, n, i, len;

#ifdef DEBUG
  fprintf(stderr,"refresh_proclist: Entering\n");
#endif

  if ((ptr = process_list) != NULL) {
    n = 1;

    while (ptr->next) {
      ptr = ptr->next;
      n++;
    }
  } else {
    n = 0; 

    XtSetSensitive(pl_command_kill, False);
  }

#ifdef DEBUG
  fprintf(stderr, "refresh_proclist: process_list counted : n = %d\n",n);
  fflush(stderr);
#endif

  /* Free our old proclist stuff here. */
  XtVaGetValues(pl_list, XtNxedwList, &proclist, XtNnumberStrings, &num, NULL);

  /* XtNxedwList is initialized with name of widget so first time DON'T FREE */
  if ((firsttime != True) && proclist) {
      for (i = 0; i < num, proclist[i]; i++) {
	  XtFree(proclist[i]->string);
	  XtFree((char *)proclist[i]);
      }
      XtFree((char *)proclist);
  }
  else firsttime = False;
  
  proclist = (XedwList **) XtMalloc(sizeof(XedwList *) * (n+1));
  
  if ((ptr = process_list) != NULL) for (i = 0; i < n; i++) {
    
#ifdef DEBUG
    fprintf(stderr,
	    "refresh_proclist: building proclist : i = %d : ptr = %d\n",i,ptr);
    fflush(stderr);
#endif 

    proclist[i] = XtNew(XedwList);
    proclist[i]->string = XtMalloc(80);
    sprintf(proclist[i]->string, "%d: ", ptr->pid);
    len = strlen(proclist[i]->string);
    tmpstring = ptr->node->string;
    if (ptr->node->options == IGNORE_SEL) {
      /* We've got a dirman node.  Let's only show the
	 name of the executable, not the path. */
      while (*tmpstring++);
      while (*tmpstring != '/') tmpstring--;
      tmpstring++;
    }
    strncpy(proclist[i]->string+len, tmpstring,
	    80 - len);
    proclist[i]->string[79] = 0;

#ifdef DEBUG
    fprintf(stderr, "                  proclist[%d]->string = %s\n",
	    i, proclist[i]->string);
#endif 

    ptr = ptr->next;
  } else i =0;

  proclist[i] = NULL;

  qsort((char *)proclist, n, sizeof(proclist[0]), plistcmp);

#ifdef DEBUG
  fprintf(stderr,"refresh_proclist: calling dumpproclist\n");
  dumpproclist("refresh_proclist: ");
#endif

  /* Reset view to top */

  setscroll(pl_viewport, 0.0);

  XtVaSetValues(
      pl_list,
          XtNxedwList,     proclist,
	  XtNnumberStrings, n,
	  NULL ) ;

  if ((listprocmenu = 
       XtNameToWidget(menuBar, "fileMenuButton.fileMenu.listprocs")) == NULL) {
    fprintf(stderr, "Signal handler: Can't find list proc menu widget\n");
    exit(2);
  }
  XtSetSensitive(listprocmenu, (n ? True : False));

#ifdef DEBUG
  fprintf(stderr, "refresh_proclist: Exiting\n");
#endif
}

/*****************************************************************************
 *                            pl_select_made                                 *
 *****************************************************************************/
/*ARGSUSED*/
#if NeedFunctionPrototypes
void pl_select_made(Widget w, XtPointer client_data, XtPointer call_data)
#else
void pl_select_made(w, client_data, call_data)
     Widget w;
     XtPointer client_data;
     XtPointer call_data;
#endif
{
  XedwListReturnStruct *highlighted;

  highlighted = XedwListShowCurrent(w);
  if (highlighted->xedwList_index != XDTM_LIST_NONE)
    XtSetSensitive(pl_command_kill, True);
  else
    XtSetSensitive(pl_command_kill, False);
  XedwListFreeCurrent(highlighted);
}

/*****************************************************************************
 *                         process_list_done                                 *
 *****************************************************************************/
/*ARGSUSED*/
#if NeedFunctionPrototypes
void process_list_done(Widget w, XtPointer client_data, XtPointer call_data)
#else
void process_list_done(w, client_data, call_data)
     Widget w;
     XtPointer client_data;
     XtPointer call_data;
#endif
{
  proclist_visible = FALSE;
  XtPopdown(pl_popup);
  XtRemoveAllCallbacks(pl_command_done, XtNcallback);
}

/*****************************************************************************
 *                                  plistcmp                                 *
 *****************************************************************************/
#if NeedFunctionPrototypes
private int plistcmp(const void *p1, const void *p2)
#else
private int plistcmp(p1, p2)
void *p1;
void *p2;
#endif
{
  return(strcmp((*(XedwList **)p1)->string, (*(XedwList **)p2)->string));
}

/*****************************************************************************
 *                         process_list_kill                                 *
 *****************************************************************************/
/*ARGSUSED*/
#if NeedFunctionPrototypes
void process_list_kill(Widget w, XtPointer client_data, XtPointer call_data)
#else
void process_list_kill(w, client_data, call_data)
     Widget w;
     XtPointer client_data;
     XtPointer call_data;
#endif
{
#ifdef DEBUG
#if NeedFunctionPrototypes
  private int dumpprocess_list(char *);
#else
  private int dumpprocess_list();
#endif
#endif

  XedwListReturnStruct *highlighted, *ptr;
  int pid;
  char dummy[80] ;

#ifdef DEBUG
  fprintf(stderr,"process_list_kill: =========================\n");
  fprintf(stderr,"process_list_kill: dumping process list\n");
  dumpprocess_list("process_list_kill: ");
#endif

  ptr = highlighted = XedwListShowCurrent(pl_list);

#ifdef DEBUG
  fprintf(stderr, "process_list_kill: killing.. \"%s\"\n",ptr->string);
  fflush(stderr);
#endif

  if ((pid = atoi(ptr->string)) > 1) {
    if(kill(pid, SIGKILL)) {
      switch(errno) {
      case ESRCH:
	strcpy(dummy, ptr->string);
	XtVaSetValues( kill_label_2, XtNlabel, dummy, NULL ) ;
	XtVaSetValues( kill_label_3, XtNlabel,
		      "This process no longer exists.", NULL ) ;
	XtVaSetValues( kill_label_4, XtNlabel, "", NULL ) ;
	popup_kill();
	/* MUST REMEMBER TO REMOVE THIS PROCESS FROM THE PROCESSLIST !!!*/
	break;
      case EPERM:
	strcpy(dummy, ptr->string);
	XtVaSetValues( kill_label_2, XtNlabel, dummy, NULL ) ;
	XtVaSetValues( kill_label_3, XtNlabel,
		  "This process was spawned SETUID or SETGID to another user.",
		      NULL ) ;
	XtVaSetValues( kill_label_4, XtNlabel,
		      "You must quit this process manually.", NULL ) ;
	popup_kill();
	break;
      }
    } else {
      
#ifdef DEBUG
      fprintf(stderr, "process_list_kill: Process %d killed\n",pid);
      fflush(stderr);
#endif
    
      /* Disable the kill button until such time as child_died
	 confirms that it is done. */

      XtSetSensitive(pl_command_kill, False);
    }
  }
#ifdef DEBUG
  fprintf(stderr,"process_list_kill: exiting\n");
#endif
  XedwListFreeCurrent(highlighted);
}

/*****************************************************************************
 *                            dumpprocess_list                               *
 *****************************************************************************/
#ifdef DEBUG
private int dumpprocess_list(str)
char *str;
{
  ProcessList *ptr;

  ptr = process_list;
  fprintf(stderr, str);
  fprintf(stderr, ":::::::::::::::::::::::::::::\n");
  while (ptr) {
    fprintf(stderr, str);
    fprintf(stderr, "pid = %d\n", ptr->pid);
    fprintf(stderr, str);
    fprintf(stderr, "node = %d\n", ptr->node);

    if (ptr->node) {
      fprintf(stderr, str);
      fprintf(stderr, "node->string = %s\n", ptr->node->string);
    }
    if (ptr->node) {
      fprintf(stderr, str);
      fprintf(stderr, "node->program = %s\n", ptr->node->program);
    }
    if (ptr->node) {
      fprintf(stderr, str);
      fprintf(stderr, "node->count = %d\n", ptr->node->count);
    }
    fprintf(stderr, str);
    fprintf(stderr, "----------------------------\n");
    ptr = ptr->next;
  }
  fprintf(stderr, str);
  fprintf(stderr, ":::::::::::::::::::::::::::::\n");
}
#endif
/*****************************************************************************
 *                            dumpproclist                                   *
 *****************************************************************************/
#ifdef DEBUG
private int dumpproclist(str)
char *str;
{
  XedwList **ptr;
  extern XedwList **proclist;
  Cardinal i = 0;
  
  ptr = proclist;
  fprintf(stderr, str);
  fprintf(stderr, "#############################\n");
  while (ptr[i]) {
    fprintf(stderr, str);
    fprintf(stderr, "proclist = %d\n", proclist);
    fprintf(stderr, str);
    fprintf(stderr, "proclist[%d] = %d\n",i,proclist[i]);
    fprintf(stderr, str);
    fprintf(stderr, "&proclist[%d]->string = %d\n", i, proclist[i]->string);
    fprintf(stderr, str);
    fprintf(stderr, "proclist[%d]->string = %s\n", i, proclist[i]->string);
    fprintf(stderr, str);
    fprintf(stderr, "----------------------------\n");
    i++;
  }
  fprintf(stderr, str);
  fprintf(stderr, "#############################\n");
}
#endif
