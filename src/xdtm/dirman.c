/*****************************************************************************
 ** File          : dirman.c                                                **
 ** Purpose       : Directory Manager                                       **
 ** Author        : Edward Groenendaal                                      **
 ** Date          : 18th Feb 1991                                           **
 ** Documentation : Xdtm Design Folder                                      **
 ** Changes       : 28-11-91, Edward Groenendaal                            **
 **                 Merged Jon's code into V1.8 to make V2.0                **
 **                 18-04-92, Edward Groenendaal                            **
 **                 Added #if NeedFunctionPrototypes stuff                  **
 **                 June 16, 1992, Ramon Santiago                           **
 **                 Changed all XtCreate calls to XtVaCreate calls.         **
 **                 Changed all caddr_t to XtPointer.                       **
 **                 June 20, 1992, Ramon Santiago                           **
 **                 removed some lint                                       **
 *****************************************************************************/

/* Include the application header file */
#include "xdtm.h"
#include "menus.h"
#include "parse.h"

/* Include the local header files */
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <limits.h>
#include <X11/Xaw/AsciiText.h>

#include "Xedw/XedwList.h"
#include "Xedw/XedwForm.h"

#define LISTSIZE    128		/* The initial size of the icon_list   */
#define LISTINCR    64		/* The steps by which it will increase */

#ifdef TRUE_SYSV
#ifndef MAXPATHLEN
#define MAXPATHLEN 512  /* jcc */
#endif
#endif

/* external and forward functions definitions */
#if NeedFunctionPrototypes
  public void changestate(Boolean);
  private void DoubleClick(Widget, XButtonEvent*);
  extern void doubleclick_dialog(String, String);
  extern void ExecuteIconProgram(Widget, AppProgram *, XtPointer);
  public String getfilename(String);
  extern Boolean getIconType(String, String, XdtmList*);
  private void GoUp(Widget, XButtonEvent*);
  private int iconcmp(const void *, const void *);
  extern void PopMenu(Widget, XButtonEvent*);
  private void Refresh(Widget, XButtonEvent*);
  private void SelectAll(Widget, XButtonEvent*);
  public void selection_made(Widget, XtPointer, XtPointer);
  extern void setCursor(Cursor);
  extern void setscroll(Widget, float);
#else
  public void changestate();
  private void DoubleClick();
  extern void doubleclick_dialog();
  extern void ExecuteIconProgram();
  public String getfilename();
  extern Boolean getIconType();
  private void GoUp();
  private int iconcmp();
  extern void PopMenu();
  private void Refresh();
  private void SelectAll();
  public void selection_made();
  extern void setCursor();
  extern void setscroll();
#endif

public Widget   directoryManager;
public String   cwd;	                   /* current working directory */
public Cardinal icon_list_size = 0;
public Cardinal icon_list_max_size = LISTSIZE;
public XdtmList **icon_list;		   /* List with icons and/or filenames */
public String   pwd_env_var;

/* local copies of the menu panes which are to be made insensitive when
 * no there is no current selection.
 */
private Widget duplicatemenu, infomenu, trashmenu, 
               copymenu, movemenu, mapmenu;


/*****************************************************************************
 *                       createDirectoryManagerWidgets                       *
 *****************************************************************************/
public void createDirectoryManagerWidgets(view)
Widget view;
{
  /* Create the directory manager widget 
   *
   * - Takes the directoryManager's parent widget
   */

  extern Icon_mode current_mode;
  XtTranslations dirManTranslations;

  /* New actions for directoryManager */

  static XtActionsRec actions[] = {
    {"DoubleClick", (XtActionProc)DoubleClick},
    {"GoUp",        (XtActionProc)GoUp},
    {"SelectAll",   (XtActionProc)SelectAll},
    {"Refresh",     (XtActionProc)Refresh},
    {"PopMenu",     (XtActionProc)PopMenu},
    {NULL, (XtActionProc)NULL}
  };

  /* New translations for directoryManager */
  static char defaultTranslations[] =
             "<Btn1Up>(2):     DoubleClick()\n\
              <Key>u:          GoUp()\n\
              <Key>a:          SelectAll()\n\
	      Ctrl<Key>L:      Refresh()\n\
              <Btn3Down>:      Set() PopMenu()\n\
              <Btn2Down>:      Set()";
  /* on peut peut-etre ajouter Info avec Btn2Up ? */
  
  /* Set the initial mode depending on the value set in the application
   * defaults. (current_mode.mode set previously to this value when 
   * creating widgets.
   */

  directoryManager =
    XtVaCreateManagedWidget(
        "directoryManager",
	xedwListWidgetClass,
	view,
	    XtNshowIcons,      (current_mode.mode == Icons) ? True : False,
	    XtNrowSpacing,     (current_mode.mode != Icons) ? 5 : 2,
	    XtNforceColumns,   (current_mode.mode == Long) ? True : False,
	    XtNdefaultColumns, (current_mode.mode == Long) ? 1 : 2,
	    XtNfont,           app_data.dm_font,
	    XtNmSelections,    True,
	    NULL ) ;

  /* Add callbacks and translations */
  XtAddCallback(directoryManager, XtNcallback,
		(XtCallbackProc)selection_made, (XtPointer)0);
  XtAppAddActions(XtWidgetToApplicationContext(view),
		  actions, XtNumber(actions));
  dirManTranslations = XtParseTranslationTable(defaultTranslations);
  XtAugmentTranslations(directoryManager, dirManTranslations);
}


/*****************************************************************************
 *                          splitPath                                        *
 *****************************************************************************/
#if NeedFunctionPrototypes
public void splitPath(String fullname, String *basename, String *path)
#else
public void splitPath(fullname, basename, path)
     String fullname;
     String *basename;
     String *path;
#endif
{
  /* split a fullname description into basename and path.
   */

    int n, len = strlen(fullname);

    n = len - 1;
    while ((n >= 0)  && (fullname[n] != '/')) n--;

    if (n < 0) /* no slash, it's a basename */
    {
	*basename = XtNewString(fullname);
	*path = XtNewString("");
	return;
    }
    else /* fullname[n] is the last '/' in fullname */
    {
	*path = (String)XtMalloc((n + 1) * sizeof(char));
	strncpy(*path, fullname, n);
	(*path)[n] = 0;
	*basename = XtNewString(&fullname[n + 1]);
	return;
    }
}

    
/*****************************************************************************
 *                     directoryManagerNewDirectory                          *
 *****************************************************************************/
public Boolean directoryManagerNewDirectory(newpath)
String newpath;
{
  /* Change the contents of the directoryManager to that of the directory
   * contained in the string newpath.
   */

  extern Cursor    busy;
  extern Widget    dirSelector;
  extern Widget    directoryManagerView;

  XdtmList         temp;
  DIR             *dirp;
  struct dirent   *dp;
  Cardinal         i = 0 ;
  Boolean          result = False; /* Whether the change of directories succeeded */

  /* create a new one */
  if ((dirp = opendir(newpath)) != NULL && chdir(newpath) == 0) {

    /* set cursor to busy */
    setCursor(busy);

    /* Emacs likes the env var PWD to find out the current directory */
    sprintf(pwd_env_var, "PWD=%s", newpath);

    if (putenv(pwd_env_var) != 0) {
      fprintf(stderr, "xdtm: Can't allocate space for environment\n");
      exit(2);
    }


    /* In the process of all this we lose our highlighted items, so
     * let's dim the menu items to reflect this. */
    changestate(False);

    /* Trash old list */
    for(i=0; i < icon_list_size; i++) {
      XtFree((char *)icon_list[i]->string);
      XtFree((char *)icon_list[i]);
    }

    /* Get icon type for each file */
    i = 0;
    while ((dp = readdir(dirp)) != NULL) {
	
      String basename = NULL, path = NULL;

      if (i == icon_list_max_size) {
	icon_list_max_size += LISTINCR;
	icon_list = (XdtmList**) XtRealloc((char *)icon_list,
					   sizeof(XdtmList*) * 
					   icon_list_max_size);
      }
	
      if (((strcmp(dp->d_name, ".") == 0) &&
	   (app_data.usedotspec == False)) ||
	  ((strcmp(dp->d_name, "..") == 0) &&
	   (app_data.usedotdotspec == False))) {

	splitPath(newpath, &basename, &path);
	if (strcmp(dp->d_name, "..") == 0) {
	  String tmppath = path;

	  XtFree(basename);
	  splitPath(tmppath, &basename, &path);
	  XtFree(tmppath);
        }
      } else {
	  basename = XtNewString(dp->d_name);
	  path = XtNewString(newpath);
      }
      
      if (getIconType(basename, path, &temp)) {
	icon_list[i] = XtNew(XdtmList);

	if (strcmp(dp->d_name, basename) != 0) {
	  /* This is either . or .. without their spec
	     replace basename by dp->d_name in temp.string */
	  int l, lbase = strlen(basename), ltemp = strlen(temp.string);
	  String ptr;

	  /* in any case (icons, short or long listing) basename is
	     always last in string, point at it */
          ptr = temp.string + (ltemp - lbase);

	  /* copy dp->d_name at ptr */
	  strcpy(ptr, dp->d_name);

	  /* adjust string length */
	  if ((l = strlen(dp->d_name) - lbase) != 0)
	    temp.string = XtRealloc(temp.string, ltemp + l + 1);
        }

	icon_list[i]->string    = temp.string;
	icon_list[i]->isdir     = temp.isdir;
	icon_list[i]->icon      = temp.icon;
	icon_list[i]->mask      = temp.mask;
	icon_list[i]->user_data = temp.user_data;
	i++;
      }
      
      XtFree(basename);
      XtFree(path);

    }
    icon_list[i] = NULL;	/* NULL terminated */
    icon_list_size = i;

    /* Sort the list */
    qsort((char*)icon_list, icon_list_size, sizeof(icon_list[0]), iconcmp);

    /* Change the contents of the directory Manager */
    XedwListChange(directoryManager, (XedwList**) icon_list, icon_list_size,
		   0, True);

    if (app_data.scrollonexit == True)
      /* Set the scrollbar of the viewport to the top */
      setscroll(directoryManagerView, 0.0);
    
    closedir(dirp);
    result = True;

    /* Change entry in the directory selector */

    XtVaSetValues(
        dirSelector,
	XtNstring, newpath,
	NULL ) ;

    /* reset cursor */
    setCursor((Cursor)NULL);
  } else 
    alert_dialog("Error: invalid directory!", newpath, NULL);

  return(result);
}

/*****************************************************************************
 *                                iconcmp                                    *
 *****************************************************************************/
#if NeedFunctionPrototypes
private int iconcmp(const void *p1, const void *p2)
#else
private int iconcmp(p1, p2)
void *p1;
void *p2;
#endif
{
  XdtmList **ip1 = (XdtmList **)p1;
  XdtmList **ip2 = (XdtmList **)p2;

  /* compare the strings of 2 XdtmList's
   * If isdir=True then put in front, otherwise put in order 
   */
    if (app_data.dirfirst == True)
    {
	if ((*ip1)->isdir == True) {
	    if ((*ip2)->isdir == True) {
		return (strcmp(getfilename((*ip1)->string), 
			       getfilename((*ip2)->string)));
	    } else {
		/* The second element is NOT a directory */
		return(-1);
	    }
	} else {
	    /* First element is a file */
	    if ((*ip2)->isdir == True) {
		/* Second element is a directory */
		return(1);
	    } else {
		/* Both elements are files */
		return (strcmp(getfilename((*ip1)->string), 
			       getfilename((*ip2)->string)));
	    }
	}
    } else return (strcmp(getfilename((*ip1)->string),
			  getfilename((*ip2)->string)));
}

/*****************************************************************************
 *                              initDirectoryManager                         *
 *****************************************************************************/
public void initDirectoryManager()
{
  /* Get the menubar pane widgets, create the initial icon_list, find 
   * out what the current directory is.
   */

  extern Boolean buttonSensitive;
  extern Widget  menuBar;
  char           tmpcwd[MAXPATHLEN];

  /* Initialise the icon list */
  icon_list = (XdtmList**) XtMalloc(sizeof(XdtmList*) * LISTSIZE);

  /* Set the starting directory */
#ifdef USE_CWD
  if (getcwd(tmpcwd, MAXPATHLEN) != NULL)
#else
  if ((char *)getwd(tmpcwd) != (char *)NULL) 
#endif
    cwd = XtNewString(tmpcwd);
  else
    if ((cwd = (char*) getenv("HOME")) == NULL) 
      cwd = XtNewString("/");
    else
      cwd = XtNewString(cwd);

  /* Get the PWD entry from the environment */

  pwd_env_var = XtMalloc(sizeof(char)*255);

  sprintf(pwd_env_var, "PWD=%s", cwd);

  if (putenv(pwd_env_var) != 0) {
    fprintf(stderr, "xdtm: Can't allocate space for environment\n");
    exit(2);
  }

  /* Get the id's of the menu widgets which are to be toggled with the
   * buttons. Note: This relies on the menus being created first, which
   * they are.
   */
  if ((duplicatemenu = 
       XtNameToWidget(menuBar, "fileMenuButton.fileMenu.duplicate")) == NULL) {
    fprintf(stderr, "Directorymanager: Can't find duplicate menu widget\n");
    exit(2);
  }
  if ((infomenu =
       XtNameToWidget(menuBar, "fileMenuButton.fileMenu.info")) == NULL) {
    fprintf(stderr, "Directorymanager: Can't find info menu widget\n");
    exit(2);
  }   
  if ((trashmenu   = 
       XtNameToWidget(menuBar, "fileMenuButton.fileMenu.trash")) == NULL) {
    fprintf(stderr, "Directorymanager: Can't find trash menu widget\n");
    exit(2);
  }
  if ((copymenu    = 
       XtNameToWidget(menuBar, "fileMenuButton.fileMenu.copy")) == NULL) {
    fprintf(stderr, "Directorymanager: Can't find copy menu widget\n");
    exit(2);
  }
  if ((movemenu    = 
       XtNameToWidget(menuBar, "fileMenuButton.fileMenu.move")) == NULL) {
    fprintf(stderr, "Directorymanager: Can't find move menu widget\n");
    exit(2);
  }
  if ((mapmenu     = 
       XtNameToWidget(menuBar,
		      "selectionMenuButton.selectionMenu.map")) == NULL) {
    fprintf(stderr, "Directorymanager: Can't find map menu widget\n");
    exit(2);
  }

  /* Add a passive grab to be able to popup spring loaded menu correctly */
  XGrabButton(XtDisplay(directoryManager), AnyButton, AnyModifier,
	      XtWindow(directoryManager), TRUE,
	      ButtonPressMask|ButtonReleaseMask,
	      GrabModeAsync, GrabModeAsync, None, None );
  
  (void) directoryManagerNewDirectory(cwd);
  buttonSensitive = True;
  changestate(False); 	   /* Insure that menus are insensitive */
}

/*****************************************************************************
 *                              selection_made                               *
 *****************************************************************************/
/*ARGSUSED*/
public void selection_made(w, client_data, call_data)
Widget w ;
XtPointer client_data ;
XtPointer call_data ;
{
  /* Someone has either selected or deselected an item.
   * If there's nothing selected then buttons are changed to 
   * not sensitive.
   */

  extern Boolean buttonSensitive;

  if (XedwListSelection(w) != buttonSensitive)
    if (buttonSensitive == False) {
      changestate(True);
    } else {
      changestate(False);
    }
}

/*****************************************************************************
 *                              DoubleClick                                  *
 *****************************************************************************/
/*ARGSUSED*/
private void DoubleClick(w, event)
Widget w;
XButtonEvent *event;
{
  /* If the file is a directory, then change to that directory -
   * if the file has an opt associated with it then run that program
   * else show the file info.
   */

  extern Icon_mode current_mode;

  struct stat           filestatus;
  String                fullname, filename, temp;
  XedwListReturnStruct *highlighted;
  Cardinal              index;
  AppProgram           *opt_pgm;

  selection_made(w, (XtPointer)0, (XtPointer)0);
  highlighted = XedwListShowCurrent(w);

  if (highlighted->xedwList_index != XDTM_LIST_NONE) {
    index = highlighted->xedwList_index;
    if ((current_mode.mode == Short) /* No userdefinable ops in Short mode */
	|| (icon_list[index] == NULL) || (icon_list[index]->user_data == NULL))
      opt_pgm = NULL;
    else
      opt_pgm = ((iconOps *)(icon_list[index]->user_data))->cmd;
    filename = getfilename(highlighted->string);
    fullname =  (String) XtMalloc((strlen(filename)+strlen(cwd)+2) *
				  sizeof(char));
    strcpy(fullname, cwd);
    if (strcmp(cwd, "/") != 0)
      strcat(fullname, "/");
    strcat(fullname, filename);

    /* Use normal stat.. follows symbolic links.. can use on SYSV */
    if (stat(fullname, &filestatus) == -1) {
      alert_dialog("Sorry, that file does", "not really exist!!", "Cancel");
    } else {
      if ((filestatus.st_mode & S_IFMT) == S_IFDIR) {

	/* The file is a directory */

	if (strcmp(filename, "..") == 0) {
	  /* Go up one level */
	  strcpy(fullname, cwd);
	  if ((temp = (char*) strrchr(fullname, '/')) == NULL) 
	    fprintf(stderr, "xdtm: impossible error (dirman.c)\n");
	  if (temp == fullname)
	    *(temp+1) = '\0';
	  else
	    *temp = '\0';
	}  
	if (strcmp(filename, ".") == 0) {
	  /* Refresh directory contents */
	  XtFree((char *)fullname);
	  fullname=XtNewString(cwd);
	}
	if (directoryManagerNewDirectory(fullname) == True) {
	  /* Normal directory - change to it */
	  XtFree((char *)cwd);
	  cwd=fullname;
	  changestate(False);
	} 
      } else {
	if (opt_pgm) {
	  /* There's a program associated with this type of file */
	  ExecuteIconProgram(NULL, opt_pgm, NULL);
	} else
	  if ((filestatus.st_mode & S_IFMT) == S_IFREG) {
	    /* Display dialog box to ask whether to display file */
	    doubleclick_dialog(filename, cwd);
	  } else alert_dialog("Cannot double click",
			      "on such file!", NULL);
	XtFree((char *)fullname);
      }
    }
  }
  XedwListFreeCurrent(highlighted);
}

/*****************************************************************************
 *                                     GoUp                                  *
 *****************************************************************************/
/*ARGSUSED*/
private void GoUp(w, event)
Widget w;
XButtonEvent *event;
{
  /* Change to the directory immediately above the current one,
   * does this by highlighting the .. directory, then calling 
   * DoubleClick to change to the highlighted directory. 
   * I used this longwinded approach because it showed on screen
   * what was actually happening.
   */

  register int i = 0 ;

  /* Find entry with .. */

  for (; i < icon_list_size && 
       (strcmp(getfilename(icon_list[i]->string), "..") != 0); i++); 

  XedwListUnhighlight(w, XedwAll);
  XedwListHighlight(w, i);  /* Call XedwListHighlight with item number */
  DoubleClick(w, (XButtonEvent*) NULL);  /* Call double click */
}

/*****************************************************************************
 *                                 SelectAll                                 *
 *****************************************************************************/
/*ARGSUSED*/
private void SelectAll(w, event)
Widget w;
XButtonEvent *event;
{
  /* Select All the files in the current directory except for . and .. */

  extern Mode    mode;
  extern Cursor  busy;

  register int i;

  /* Don't do it if in copy or move mode */
  if (mode == NormalMode) {
    setCursor(busy);
    XedwListHighlight(w, XedwAll);
    
    /* unhighlight . and .. */
    for (i=0; i < icon_list_size && 
	 (strcmp(getfilename(icon_list[i]->string), ".") != 0); i++); 
    if (i < icon_list_size) XedwListUnhighlight(w, i);
    for (i= (i < icon_list_size ? i : 0); i < icon_list_size && 
	 (strcmp(getfilename(icon_list[i]->string), "..") != 0); i++); 
    if (i < icon_list_size) XedwListUnhighlight(w, i);
    setCursor((Cursor)NULL);
  } else alert_dialog("Cannot select all", "while trying to Copy/Move", NULL);
    
  
  /* do buttons */
  if (icon_list_size > 2) {
    changestate(True);
  }
}

/*****************************************************************************
 *                                Refresh                                    *
 *****************************************************************************/
/*ARGSUSED*/
private void Refresh(w, event)
Widget w;
XButtonEvent *event;
{
  /* Refresh the current directory contents to the actual contents. */

  directoryManagerNewDirectory(cwd);
}

/*****************************************************************************
 *                               changestate                                 *
 *****************************************************************************/
#if NeedFunctionPrototypes
public void changestate(Boolean new_state)
#else
public void changestate(new_state)
Boolean new_state;
#endif
{
  /* Toggle the sensitivity of all the widgets which may not be used
   * when no files are highlighted.
   *
   * - Takes the new state for widgets.
   */

  extern Boolean buttonSensitive;
  extern Widget  trashButton, copyButton, moveButton;
  extern Mode    mode;

  if (buttonSensitive == True) {
    if (mode == MoveMode) {
      /* Turn off Trash and Copy */
      XtSetSensitive(copyButton,    False);
      XtSetSensitive(trashButton,   False);
      XtSetSensitive(copymenu,      False);
      XtSetSensitive(trashmenu,     False);
      XtSetSensitive(mapmenu,       False);
      XtSetSensitive(duplicatemenu, False);
      XtSetSensitive(infomenu,      False);
      buttonSensitive = False;
    } else if (mode == CopyMode) {
      /* Turn off Trash and Move */
      XtSetSensitive(moveButton,    False);
      XtSetSensitive(trashButton,   False);
      XtSetSensitive(movemenu,      False);
      XtSetSensitive(trashmenu,     False);
      XtSetSensitive(mapmenu,       False);
      XtSetSensitive(duplicatemenu, False);
      XtSetSensitive(infomenu,      False);
      buttonSensitive = False;
    } else if (new_state == False) {
      /* Turn off All */
      XtSetSensitive(moveButton,    False);
      XtSetSensitive(copyButton,    False);
      XtSetSensitive(trashButton,   False);
      XtSetSensitive(copymenu,      False);
      XtSetSensitive(movemenu,      False);
      XtSetSensitive(trashmenu,     False);
      XtSetSensitive(mapmenu,       False);
      XtSetSensitive(duplicatemenu, False);
      XtSetSensitive(infomenu,      False);
      buttonSensitive = False;
    }
  } else {
    /* buttonSensitive == False */

    /* If in MoveMode or CopyMode, then everything is set anyway */
    if (mode == NormalMode && new_state == True) {
      /* Set all to True */
      XtSetSensitive(trashButton,   True);
      XtSetSensitive(moveButton,    True);
      XtSetSensitive(copyButton,    True);
      XtSetSensitive(trashmenu,     True);
      XtSetSensitive(movemenu,      True);
      XtSetSensitive(copymenu,      True);
      XtSetSensitive(duplicatemenu, True);
      XtSetSensitive(mapmenu,       True);
      XtSetSensitive(infomenu,      True);
      buttonSensitive = True;
    }
  }
}

/*****************************************************************************
 *                              getfilename                                  *
 *****************************************************************************/
public String getfilename(s)
String s;
{
  /* If in Long Listing mode this function returns the filename part
   * of the string used in the directory list. If not in Long Listing
   * then return the string unmolested.
   * NOTE that the string returned when in Long Listing mode should not be
   * freed as it was never allocated.
   * NOTE also that doing string = getfilename(string) should avoided.
   */
  extern Icon_mode current_mode;

  if (current_mode.mode == Long && (strlen(s) > current_mode.length))
    return (s+current_mode.length);
  else
    return (s);
}
