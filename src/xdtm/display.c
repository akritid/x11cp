/*****************************************************************************
 ** File          : display.c                                               **
 ** Purpose       : Initialise and Realise display and query dialogs        **
 ** Author        : Edward Groenendaal                                      **
 ** Date          : July 1991                                               **
 ** Documentation : Xdtm Design Folder                                      **
 ** Related Files :                                                         **
 ** Changes       : 30.3.92 eddyg - Took out the seggie stuff around the    **
 **                 view file option.					    **
 **                 18-04-92, Edward Groenendaal                            **
 **                 Added the #if NeedFunctionPrototypes stuff              **
 **                 June 20, 1992, Ramon Santiago                           **
 **                 Changed all XtCreate calls to XtVaCreate calls.         **
 **                 Changed all caddr_t to XtPointer.                       **
 **                 Removed some lint.                                      **
 *****************************************************************************/

#include "xdtm.h"

#ifndef TRUE_SYSV
#include <sys/file.h>    /* For access(2) */
#endif

#include <X11/Shell.h>
#include <X11/Xaw/Label.h>
#include <X11/Xaw/Command.h>
#include <X11/Xaw/AsciiText.h>
#include "Xedw/XedwForm.h"

/* Structure used to pass back the id's of the display popup's */
typedef struct widgets {
  Widget popup;
  Widget save;
  Widget text;
  Widget query;
} widgets;

/* external and forward functions definitions */
extern void realize_dialog(
#if NeedFunctionPrototypes
    Widget, Widget, XtGrabKind, XtEventHandler, XtPointer
#endif
);
extern Widget topLevel;      

public String viewfile;

/*****************************************************************************
 *                              init_display                                 *
 *****************************************************************************/
/*ARGSUSED*/
public void init_display(top)
Widget top;
{
  /* This procedure has been made redundant, the popup's are created
     dynamically which is slower - but enables multiple instances. */
}

/*****************************************************************************
 *                        displaytext_callback                               *
 *****************************************************************************/
#if NeedFunctionPrototypes
void displaytext_callback(Widget w, widgets *display_widgets)
#else
void displaytext_callback(w, display_widgets)
     Widget w;
     widgets *display_widgets;
#endif
{
  XtVaSetValues(
      display_widgets->save,
          XtNsensitive, True,
	  NULL ) ;
  
  XtRemoveCallback(w, XtNcallback, (XtCallbackProc)displaytext_callback,
		   display_widgets);
}

/*****************************************************************************
 *                                destroy_query                              *
 *****************************************************************************/
/*ARGSUSED*/
private void destroy_query(w, display_widgets)
Widget w ;
widgets *display_widgets ;
{
  /* Cancel was selected.. so just destroy the query NOT the edit popup*/

  XtDestroyWidget(display_widgets->query);
  display_widgets->query = NULL;
}

/*****************************************************************************
 *                                destroy_view                               *
 *****************************************************************************/
/*ARGSUSED*/
private void destroy_view(w, destroyer)
Widget w ;
widgets *destroyer ;
{
  if (destroyer->query == NULL) {
    if (destroyer->save != NULL) {
      /* An edit widget, check to see if a save is needed, if text is NULL
       * we have already been called and the result was to DIE! 
       */
      Widget wd=NULL;

      XtVaGetValues(
          destroyer->text,
	      XtNtextSource, &wd,
	      NULL ) ;
      
      if (XawAsciiSourceChanged(wd) == True) {
	/* Make a query dialog */
	Widget querypopup;
	Widget queryform;
	Widget querylabel;
	Widget queryquit;
	Widget querycancel;
	
	querypopup =
	  XtVaCreatePopupShell(
	      "Query",
	      transientShellWidgetClass,
	      topLevel,
	          NULL ) ;

	queryform =
	  XtVaCreateManagedWidget(
	      "queryform",
	      xedwFormWidgetClass,
	      querypopup,
	          NULL ) ;

	querylabel =
	  XtVaCreateManagedWidget(
	      "querylabel",
	      labelWidgetClass,
	      queryform,
	          XtNlabel, "File not saved! Quit anyway?",
		  XtNborderWidth,                        0,
		  XtNjustify,              XtJustifyCenter,
		  XtNfullWidth,                       True,
		  NULL ) ;

	queryquit =
	  XtVaCreateManagedWidget(
	      "queryquit",
	      commandWidgetClass,
	      queryform,
	          XtNlabel,            "Quit",
		  XtNfromVert,     querylabel,
		  XtNjustify, XtJustifyCenter,
		  NULL ) ;

        querycancel =
	  XtVaCreateManagedWidget(
	      "querycancel",
	      commandWidgetClass,
	      queryform,
	          XtNlabel,          "Cancel",
		  XtNfromVert,     querylabel,
		  XtNfromHoriz,     queryquit,
		  XtNjustify, XtJustifyCenter,
		  NULL ) ;

	destroyer->query = querypopup;
	XtAddCallback(queryquit,   XtNcallback, (XtCallbackProc)destroy_view,
		      (XtPointer)destroyer);
	XtAddCallback(querycancel, XtNcallback, (XtCallbackProc)destroy_query,
		      (XtPointer)destroyer);
	
	realize_dialog(querypopup, destroyer->popup, XtGrabNonexclusive,
		       NULL, NULL);
      } else {
	XtDestroyWidget(destroyer->popup);
	XtFree((char *)destroyer);
      }
    } else {
      XtDestroyWidget(destroyer->popup);
      XtFree((char *)destroyer);
    }
  } else {
    /* We are returning after a 'are you sure you want to quit without saving'
     * dialog box. Stop editing file.
     */
    XtDestroyWidget(destroyer->query);
    XtDestroyWidget(destroyer->popup);
    XtFree((char *)destroyer);
  }
}

/*****************************************************************************
 *                              WM_destroy_view                              *
 *****************************************************************************/
#if NeedFunctionPrototypes
private void WM_destroy_view(Widget w, XtPointer client_data,
				   XEvent *event, Boolean *dispatch)
#else
private void WM_destroy_view(w, client_data, event, dispatch)
     Widget w;
     XtPointer client_data;
     XEvent *event;
     Boolean *dispatch;
#endif
{
    extern Atom protocols[2]; /* defined in dialogs.c */

    if (event->xclient.message_type == protocols[1] &&
	event->xclient.data.l[0] == protocols[0])
    /* the widget got a kill signal */
    {
	/* Directly call callback of `Quit' button */
	destroy_view(w, (Widget *)client_data);
    }
}

/*****************************************************************************
 *                              destroy_popup                                *
 *****************************************************************************/
/*ARGSUSED*/
private void destroy_popup(w, killme)
Widget w, killme;
{
  XtDestroyWidget(killme);
}

/*****************************************************************************
 *                              alert_dialog                                 *
 *****************************************************************************/
public void alert_dialog(label1, label2, buttonlabel)
String label1, label2, buttonlabel;
{    
  
  Widget alertpopup;
  Widget alertform;
  Widget alertlabel1;
  Widget alertlabel2 = (Widget) NULL;
  Widget alertbutton;

  private String QueryYes = "OK";
  
  alertpopup =
    XtVaCreatePopupShell(
        "alertDialog",
	transientShellWidgetClass,
	topLevel,
	    NULL ) ;

  alertform =
    XtVaCreateManagedWidget(
	"alertform",
	xedwFormWidgetClass,
	alertpopup,
	    NULL ) ;

  if (label1) {
    alertlabel1 =
      XtVaCreateManagedWidget(
          "alertlabel1",
	  labelWidgetClass,
	  alertform,
	      XtNlabel,            label1,
	      XtNborderWidth,           0,
	      XtNjustify, XtJustifyCenter,
	      XtNfullWidth,          True,
	      NULL ) ;
  } else {
    fprintf(stderr, "xdtm: Programmer is a forgetfull idiot\n");
    return;
  }
  
  if (label2) {
    alertlabel2 =
      XtVaCreateManagedWidget(
          "alertlabel2",
	  labelWidgetClass,
	  alertform,
	      XtNlabel,            label2,
	      XtNborderWidth,           0,
	      XtNjustify, XtJustifyCenter,
	      XtNfullWidth,          True,
	      XtNfromVert,    alertlabel1,
	      NULL ) ;
  }

  alertbutton =
    XtVaCreateManagedWidget(
        "alertbutton",
	commandWidgetClass,
	alertform,
	    XtNlabel, (buttonlabel) ? buttonlabel : QueryYes,
	    XtNfromVert,  (label2) ? alertlabel2 : alertlabel1,
	    XtNjustify, XtJustifyCenter,
	    XtNfullWidth,          True,
	    NULL ) ;

  XtAddCallback(alertbutton, XtNcallback, (XtCallbackProc)destroy_popup,
		(XtPointer)alertpopup);

  if (app_data.bellonwarn) {
      XBell(XtDisplay(alertpopup), 100);
      XBell(XtDisplay(alertpopup), 100);
  }
  
  realize_dialog(alertpopup, NULL, XtGrabNonexclusive, NULL, NULL);
}

/*****************************************************************************
 *                              displayfile                                  *
 *****************************************************************************/
public void displayfile(fullname, searchto, title, grab_kind)
String fullname, searchto, title;
XtGrabKind grab_kind;
{
  /* This procedure popup's a window with the file 'fullname' contained 
   * within a text widget. When quit is selected the window is popped down.
   *
   * - Takes the name of the file to be viewed
   * - Takes a string to position the initial view.
   * - Takes a title for the window.
   */

  Widget view_popup;
  Widget form;
  Widget quit_button;
  Widget filename_label;
  Widget view;
  widgets *view_widgets;
  int width, height;
  XFontStruct *font=app_data.view_font;

  view_popup =
    XtVaCreatePopupShell(
        "xdtm: view file",
	topLevelShellWidgetClass,
	topLevel,
	    NULL ) ;

  form =
    XtVaCreateManagedWidget(
	"form",
	xedwFormWidgetClass,
	view_popup,
            XtNbottom, XtChainBottom,
            XtNtop,       XtChainTop,
            XtNleft,     XtChainLeft,
            XtNright,   XtChainRight,
            XtNhorizDistance,      5,
            XtNvertDistance,       5,
	    NULL ) ;

  quit_button =
    XtVaCreateManagedWidget(
        "quit_button",
	commandWidgetClass,
	form,
	    XtNlabel,       "Quit",
	    XtNrubberWidth,  False,
	    XtNrubberHeight, False,
            XtNresizable,    False,
            XtNtop,     XtChainTop,
            XtNbottom,  XtChainBottom,
            XtNleft,    XtChainLeft,
            XtNright,   XtChainRight,
	    NULL ) ;

  filename_label =
    XtVaCreateManagedWidget(
        "filename_label",
	labelWidgetClass,
	form,
	    XtNlabel, ((title != NULL) ? title : fullname),
	    XtNfromHoriz, quit_button,
	    XtNfullWidth, True,
	    XtNrubberHeight, False,
            XtNtop,     XtChainTop,
            XtNbottom,  XtChainBottom,
            XtNleft,    XtChainLeft,
            XtNright,   XtChainRight,
	    NULL ) ;

  /* Work out the height and width of the text widget */

  width =  (font->max_bounds.width * app_data.view_width);
  height = ((font->max_bounds.ascent +
             font->max_bounds.descent) * app_data.view_height);

  view =
    XtVaCreateManagedWidget(
        "view",
	asciiTextWidgetClass,
	form,
	    XtNstring,           fullname,
	    XtNeditType,         XawtextRead,
	    XtNtype,             XawAsciiFile,
	    XtNscrollHorizontal, XawtextScrollWhenNeeded,
	    XtNscrollVertical,   XawtextScrollAlways,
	    XtNwidth,            width,
	    XtNheight,           height,
	    XtNfont,             app_data.view_font,
	    XtNfromVert,         filename_label,
	    XtNfullWidth,        True,
	    XtNfullHeight,       True,
            XtNtop,     XtChainTop,
            XtNbottom,  XtChainBottom,
            XtNleft,    XtChainLeft,
            XtNright,   XtChainRight,
	    NULL ) ;

  /* The search is used for displaying HELP files at the right point */
  if (searchto) {
    XawTextBlock searchfor;
    XawTextPosition foundat;
    
    searchfor.firstPos = 0;
    searchfor.ptr = searchto;
    searchfor.length = strlen(searchto);
    searchfor.format = FMT8BIT;
    foundat = XawTextSearch(view, XawsdRight, &searchfor);
    if (foundat == XawTextSearchError) {
      char uh_oh[255];
      sprintf(uh_oh, "Couldn't find section \"%s\"!", searchto);
      alert_dialog(uh_oh, "in help file", NULL);
    } else {
      XtVaSetValues(
          view,
	      XtNdisplayPosition, foundat,
	      NULL ) ;
      XawTextSetSelection(view, foundat, foundat+strlen(searchto));
    }
  }

  view_widgets = (widgets*) XtMalloc(sizeof(widgets));
  view_widgets->popup = view_popup;
  view_widgets->save  = NULL;
  view_widgets->text  = view;
  view_widgets->query = NULL;
  XtAddCallback(quit_button, XtNcallback, (XtCallbackProc)destroy_view,
		(XtPointer)view_widgets);

  realize_dialog(view_popup, NULL, grab_kind,
		 (XtEventHandler)WM_destroy_view, (XtPointer)view_widgets);

}

/*****************************************************************************
 *                               save_contents                               *
 *****************************************************************************/
/*ARGSUSED*/
private void save_contents(w, edit_widgets)
Widget w;
widgets *edit_widgets;
{
  Widget wd=0; 
  extern Cursor busy, left_ptr;

  XtVaSetValues(
      edit_widgets->popup,
          XtNcursor, busy,
	  NULL ) ;

  XtVaGetValues(
      edit_widgets->text,
          XtNtextSource,&wd,
	  NULL ) ;

  XawAsciiSave(wd);
  XtSetSensitive(edit_widgets->save, False);

  XtAddCallback(wd, XtNcallback, (XtCallbackProc)displaytext_callback, (XtPointer)edit_widgets);

  XtVaSetValues(
      edit_widgets->popup,
          XtNcursor, left_ptr,
	  NULL ) ;
}     

/*****************************************************************************
 *                              Editfile                                     *
 *****************************************************************************/
public void editfile(fullname)
String fullname;
{
  Widget edit_popup;
  Widget quit_button;
  Widget save_button;
  Widget form;
  Widget editwidget;
  Widget filename_label;
  Widget wd = NULL;
  widgets *edit_widgets;
  int width, height;
  XFontStruct *font=app_data.view_font;
  
  /* We shall assume that the file IS writable */
  
  edit_popup =
    XtVaCreatePopupShell(
        "xdtm: edit file",
	topLevelShellWidgetClass,
	topLevel,
	    NULL ) ;

  form =
    XtVaCreateManagedWidget(
	"form",
	xedwFormWidgetClass,
	edit_popup,
            XtNhorizDistance,      5,
            XtNvertDistance,       5,
            XtNbottom, XtChainBottom,
            XtNtop,       XtChainTop,
            XtNleft,     XtChainLeft,
            XtNright,   XtChainRight,
	    NULL ) ;

  quit_button =
    XtVaCreateManagedWidget(
        "quit_button",
	commandWidgetClass,
	form,
	    XtNlabel, "Quit",
	    XtNrubberWidth, False,
	    XtNrubberHeight, False,
            XtNbottom, XtChainBottom,
            XtNtop,       XtChainTop,
            XtNleft,     XtChainLeft,
            XtNright,   XtChainRight,
	    NULL ) ;

  save_button =
    XtVaCreateManagedWidget(
        "save_button",
	commandWidgetClass,
	form,
	    XtNfromHoriz,   quit_button,
	    XtNlabel,       "Save",
	    XtNrubberWidth, False,
	    XtNrubberHeight, False,
	    XtNsensitive,    False,
            XtNbottom, XtChainBottom,
            XtNtop,       XtChainTop,
            XtNleft,     XtChainLeft,
            XtNright,   XtChainRight,
	    NULL ) ;

  filename_label =
    XtVaCreateManagedWidget(
        "filename_label",
        labelWidgetClass,
	form,
	    XtNlabel,     fullname, /* Could put editing: filename here instead */
	    XtNfromHoriz, save_button,
	    XtNfullWidth, True,
	    XtNrubberHeight, False,
            XtNbottom, XtChainBottom,
            XtNtop,       XtChainTop,
            XtNleft,     XtChainLeft,
            XtNright,   XtChainRight,
	    NULL ) ;

  width =  (font->max_bounds.width * app_data.view_width);
  height = ((font->max_bounds.ascent +
             font->max_bounds.descent) * app_data.view_height);

  editwidget =
    XtVaCreateManagedWidget(
        "editwidget",
	asciiTextWidgetClass,
	form,
	    XtNstring,           fullname,
	    XtNeditType,         XawtextEdit,
	    XtNtype,             XawAsciiFile,
	    XtNscrollHorizontal, XawtextScrollWhenNeeded,
	    XtNscrollVertical,   XawtextScrollAlways,
	    XtNwidth,            width,
	    XtNheight,           height,
	    XtNfont,             app_data.view_font,
	    XtNfromVert,         filename_label,
	    XtNfullWidth,        True,
            XtNbottom, XtChainBottom,
            XtNtop,       XtChainTop,
            XtNleft,     XtChainLeft,
            XtNright,   XtChainRight,
	    NULL ) ;

  XtVaGetValues(
      editwidget,
          XtNtextSource,&wd,
	  NULL ) ;

  edit_widgets = (widgets*) XtMalloc(sizeof(widgets));
  edit_widgets->popup = edit_popup;
  edit_widgets->save  = save_button;
  edit_widgets->text  = editwidget;
  edit_widgets->query = NULL;

  XtAddCallback(save_button, XtNcallback, (XtCallbackProc)save_contents,
		(XtPointer)edit_widgets);
  XtAddCallback(quit_button, XtNcallback, (XtCallbackProc)destroy_view,
		(XtPointer)edit_widgets);
  XtAddCallback(wd, XtNcallback, (XtCallbackProc)displaytext_callback,
		(XtPointer)edit_widgets);

  realize_dialog(edit_popup, NULL, XtGrabNone,
		 (XtEventHandler)WM_destroy_view, (XtPointer)edit_widgets);
}
