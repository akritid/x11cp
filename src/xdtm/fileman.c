/*****************************************************************************
 ** File          : fileman.c                                               **
 ** Purpose       : Receive and distribute directory changes                **
 ** Author        : Edward Groenendaal                                      **
 ** Date          : 18th Feb 1991                                           **
 ** Documentation : Xdtm Design Folder                                      **
 ** Related Files : treeman.c, dirman.c, pathman.c, main.c                  **
 ** Changes       : 18-04-92, Edward Groenendaal                            **
 **                 Added #if NeedFunctionPrototypes stuff                  **
 **                 June 20, 1992, Ramon Santiago                           **
 **                 Changed all XtCreate calls to XtVaCreate calls.         **
 **                 Changed all caddr_t to XtPointer.                       **
 **                 Removed some lint.                                      **
 ****************************************************************************/

#include "xdtm.h"

#include <stdio.h>
#include <signal.h>
#include <X11/Xaw/AsciiText.h>
#include <X11/Xaw/Command.h>
#include <X11/Xaw/Paned.h>
#include <X11/Xaw/Viewport.h>
#include <X11/cursorfont.h>
#ifdef R5_ATHENA
#include <X11/Xaw/Tree.h>
#endif
#include "Xedw/XedwForm.h"

public Widget vert_bar;
public Widget dirSelector;
public Cursor busy, left_ptr;
public Widget directoryManagerView;

/* external and forward functions definitions */
#if NeedFunctionPrototypes
  extern void child_died(int);
  extern void createDirectoryManagerWidgets(Widget);
  extern Boolean directoryManagerNewDirectory(String);
  private void GotoDir(Widget, XButtonEvent*);
  private void goto_dir(Widget, XtPointer, XtPointer);
  extern void initDirectoryManager();         /* dirman.c */
#ifdef R5_ATHENA
  extern void initTreeManager();              /* treeman.c */
#endif
  public void setCursor(Cursor);
#else
  extern void child_died();
  extern void createDirectoryManagerWidgets();
  extern Boolean directoryManagerNewDirectory();
  private void GotoDir();
  private void goto_dir();
  extern void initDirectoryManager();         /* dirman.c */
#ifdef R5_ATHENA
  extern void initTreeManager();              /* treeman.c */
#endif
  public void setCursor();
#endif

/****************************************************************************
 *                       createFileManagerWidgets                           *
 ****************************************************************************/
public void createFileManagerWidgets(w)
Widget w;
{
  /* Create file manager widgets, this includes directory manager, 
   * directory selector and may in the future include the tree manager
   * which displays a tree structure representing the file system
   */
#ifdef R5_ATHENA
  Widget treeManagerView;
#endif
  Widget fileManagerPane;
  Widget dirButton;
  extern Widget menuBar;
  XtTranslations dirSelTranslations;

  static XtActionsRec actions[] = {
    {"GotoDir",  (XtActionProc)GotoDir},
    {NULL, NULL}
  };

  static XawTextSelectType no_extended_select[] = 
    {XawselectPosition, XawselectNull};

  static char defaultTranslations[] = 
    "Ctrl<Key>A:        beginning-of-line() \n\
     Ctrl<Key>E:        end-of-line() \n\
     <Key>Return:       GotoDir() \n\
     <Key>Escape:       beginning-of-line() kill-to-end-of-line() \n\
     <Key>Right: 	forward-character() \n\
     <Key>Left:         backward-character() \n\
     <Key>Delete:       delete-previous-character() \n\
     <Key>BackSpace:    delete-previous-character() \n\
     <Key>:             insert-char() \n\
     <FocusIn>:         focus-in() \n\
     <FocusOut>:        focus-out() \n\
     <BtnDown>:         select-start()";

  fileManagerPane =
    XtVaCreateManagedWidget(
        "fileManagerPane",
	panedWidgetClass,
	w,
	    XtNfromVert, menuBar,
	    XtNfullHeight, True,
	    XtNfullWidth, True,
	    XtNfromHoriz, XtNameToWidget(w, "appManagerButton"),
	    XtNtop,     XtChainTop,
	    XtNbottom,  XtChainBottom,
	    XtNleft,    XtChainLeft,
	    XtNright,   XtChainRight,
	    NULL ) ;

#ifdef R5_ATHENA
  treeManagerView =
    XtVaCreateManagedWidget(
        "treeManagerView",
	viewportWidgetClass,
	fileManagerPane,
	    XtNforceBars, True,
	    XtNallowVert, True,
	    XtNallowHoriz, True,
	    NULL ) ;

  createTreeManagerWidgets( treeManagerView ) ;
#endif

  directoryManagerView =
    XtVaCreateManagedWidget(
        "directoryManagerView",
	viewportWidgetClass,
	fileManagerPane,
	    XtNforceBars, True,
	    XtNallowVert, True,
	    NULL ) ;
  
  createDirectoryManagerWidgets( directoryManagerView ) ;

  /* Create text widget */

  dirButton =
    XtVaCreateManagedWidget(
        "dirButton",
	commandWidgetClass,
	w,
	    XtNrubberHeight,   False,
	    XtNrubberWidth,    False,
	    XtNfromHoriz,    menuBar,
	    XtNborderWidth,        0,
	    XtNlabel,        "Goto:",
	    XtNtop,     XtChainTop,
	    XtNbottom,  XtChainBottom,
	    XtNleft,    XtChainLeft,
	    XtNright,   XtChainRight,
	    NULL ) ;

  dirSelector =
    XtVaCreateManagedWidget(
        "dirSelector",
	asciiTextWidgetClass,
	w,
	    XtNrubberHeight,    False,
	    XtNfullWidth,        True,
	    XtNfromHoriz,   dirButton,
	    XtNeditType,  XawtextEdit,
	    XtNborderWidth,         1,
	    XtNvertDistance,        6,
	    XtNselectTypes, no_extended_select,
	    XtNtop,     XtChainTop,
	    XtNbottom,  XtChainBottom,
	    XtNleft,    XtChainLeft,
	    XtNright,   XtChainRight,
	    NULL ) ;

  XtAddCallback(dirButton, XtNcallback, (XtCallbackProc)goto_dir, (XtPointer)0);
  XtAppAddActions(XtWidgetToApplicationContext(w), actions, XtNumber(actions));
  XtUninstallTranslations(dirSelector);
  dirSelTranslations = XtParseTranslationTable(defaultTranslations);
  XtOverrideTranslations(dirSelector, dirSelTranslations); 
 }

/*****************************************************************************
 *                           initFileManager                                 *
 *****************************************************************************/
public void initFileManager(w)
Widget w;
{
  /* initialise file management bits */

  busy     = XCreateFontCursor(XtDisplay(w), XC_watch);
  left_ptr = XCreateFontCursor(XtDisplay(w), XC_left_ptr);

  /* Check what should be mapped, unmapping those that shouldn't */

  initDirectoryManager();
#ifdef R5_ATHENA
  initTreeManager();
#endif
}

/*****************************************************************************
 *                              GotoDir                                      *
 *****************************************************************************/
/*ARGSUSED*/
private void GotoDir(w, event)
Widget w;
XButtonEvent *event;
{
  /* Goto directory, this is an action called in reponse to the key return
   * within the directory selector. 
   */

  goto_dir(w, 0, 0);
}

/*****************************************************************************
 *                             goto_dir                                      *
 *****************************************************************************/
/*ARGSUSED*/
private void goto_dir(w, client_data, call_data)
Widget w ;
XtPointer client_data ;
XtPointer call_data ;
{
  /* Called when the goto directory button is pressed, Or when return is
   * pressed in the directory selector.
   */
  extern String cwd;

  String value, tmp;

  /* Get the contents of the dirselector */

  XtVaGetValues(
    dirSelector,
        XtNstring, &value,
	NULL ) ;

  /* If the string is empty then change to root.. avoids a seggie. */
  if (strlen(value) == 0) value = XtNewString("/");

  /* expand value using csh. The method is slow, but reliable,
     and should not harm because the goto-widget is not often used. */
  {
    int len;
    String new = NULL;
    FILE * file;
    int alloclen=0;

#ifdef SIGCLD
    signal(SIGCLD, SIG_DFL);
#else
    signal(SIGCHLD, SIG_DFL);
#endif

    /* First, create a string containing the command to expand term
       value. */
    tmp = XtMalloc((strlen(value)+30)*sizeof(char));
#ifndef ECHO_N
    sprintf(tmp, "csh -c \"/bin/echo %s \\\\\\c\"", value);
#else
    sprintf(tmp, "csh -c \"/bin/echo -n %s \"", value);
#endif
    /* Now open and read the file. As this is a pipe, we should not
       try to determine its length, instead, we will read the file
       in portions of length FILE_INC so we do not have to assume
       a max-filelength. */
    file=(FILE*)popen((char*)tmp,"r");
    XtFree((char *)tmp);
    if (file) {
      do {
#define FILE_INC 32
	if (alloclen==0)
	  new=XtMalloc(alloclen+FILE_INC);
	else
	  new=XtRealloc(new,alloclen+FILE_INC);
	len=fread(new+alloclen,sizeof(char),FILE_INC,file);
#ifndef ECHO_N
	if (len<FILE_INC) new[(alloclen+len)-1]='\0';
#else
	if (len<FILE_INC) new[(alloclen+len)]='\0';
#endif
	alloclen+=FILE_INC;
      } while (len);
#undef FILE_INC
      tmp=XtNewString(new);
      XtFree((char *)new);
      pclose(file);
    } else {
      tmp=XtNewString(value);
    }
  }
  
#ifdef SIGCLD
  signal(SIGCLD, child_died);
#else
  signal(SIGCHLD, child_died);
#endif


  /* change to the directory specified */
  if (directoryManagerNewDirectory(tmp) == False) {
    XtFree((char *)tmp);
  } else {
    cwd = tmp;
  }
}

/*****************************************************************************
 *                                setCursor                                  *
 *****************************************************************************/
public void setCursor(newcursor)
Cursor newcursor;
{

  /* This should be called with a cursor to change to, or NULL to
   * go back to saved values. 
   * If the cursor is NULL then reset cursors to their defaults in
   * widgets.
   * Otherwise..
   * Set the cursor to newcursor in the following widgets:
   *   directoryManager
   *   dirSelector
   *   
   */

  extern Widget directoryManager, appManager;

  static Cursor dirManCursor, appManCursor;

  if (newcursor == (Cursor)NULL) {

    /* Go back to saved values */

    XtVaSetValues( directoryManager, XtNcursor, dirManCursor, NULL ) ;
#ifdef R5_ATHENA
    XtVaSetValues( treeManager, XtNcursor, treeManCursor, NULL ) ;
#endif
    XtVaSetValues( appManager, XtNcursor, appManCursor, NULL ) ;

  } else {
    /* Get old values */

    XtVaGetValues( directoryManager, XtNcursor, &dirManCursor, NULL ) ;
#ifdef R5_ATHENA
    XtVaGetValues( treeManager, XtNcursor, &treeManCursor, NULL ) ;
#endif
    XtVaGetValues( appManager, XtNcursor, &appManCursor, NULL ) ;

    XtVaSetValues( directoryManager, XtNcursor, newcursor, NULL ) ;
#ifdef R5_ATHENA
    XtVaSetValues( treeManager, XtNcursor, newcursor, NULL ) ;
#endif
    XtVaSetValues( appManager,  XtNcursor, newcursor, NULL ) ;
    
  }

  XFlush(XtDisplay(directoryManager)); /* Change cursors immediately */
}

