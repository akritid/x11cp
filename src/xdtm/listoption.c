/*****************************************************************************
 ** File          : listoption.c                                            **
 ** Purpose       : Initialise and Realise long listing dialog options      **
 ** Author        : Edward Groenendaal                                      **
 ** Date          : April 1991                                              **
 ** Documentation : Xdtm Design Folder                                      **
 ** Related Files :                                                         **
 ** Changes       : 18-04-92, Edward Groenendaal                            **
 **                 Added #if NeedFunctionPrototypes stuff                  **
 **                 June 20, 1992, Ramon Santiago                           **
 **                 Changed all XtCreate calls to XtVaCreate calls.         **
 **                 Changed all caddr_t to XtPointer.                       **
 **                 Removed some lint.                                      **
 **                 Modified some widgets names, and moved language         **
 **                 specific strings to the app-defaults file.              **
 ****************************************************************************/

#include "xdtm.h"
#include "menus.h"
#include <stdio.h>
#include <X11/Shell.h>
#include <X11/Xaw/Label.h>
#include <X11/Xaw/Command.h>
#include "Xedw/XedwForm.h"

/* Note ListOption *same* order as listoptions */

typedef enum {perms, nlinks, owner, group, size, modtm, acctm} ListOption;

static String listoptions[] = {
  "rwxrwxrwx ",
  "1 ",
  "owner ",
  "group ",
  "size ",
  "modtm ",
  "acctm "
};

/* Widgets */

private Widget listOptionDialog;      
private Widget form1;
private Widget form2;
private Widget form3;
private Widget listoptionlabel;
private Widget listoptionsettings;

private Widget permsButton;
private Widget permsLabel;

private Widget linksButton;
private Widget linksLabel;

private Widget ownerButton;
private Widget ownerLabel;

private Widget groupButton;
private Widget groupLabel;

private Widget sizeButton;
private Widget sizeLabel;

private Widget acctmButton;
private Widget acctmLabel;

private Widget modtmButton;
private Widget modtmLabel;

private Widget quitButton;

#define ToggleButton(b, w)  \
{ \
  XtVaSetValues(w, XtNbitmap, (current_mode.options & b) ? tick : emptytick, NULL); \
}
	     
/* external and forward functions definitions */
#if NeedFunctionPrototypes
  private void destroy_listoption_dialog(Widget, XtPointer, XtPointer);
  extern directoryManagerNewDirectory(String);
  private String makeoptionstring();
  private void optionbutton_toggled(Widget, ListOption, XtPointer);
  extern void realize_dialog(Widget, Widget, XtGrabKind, XtEventHandler, XtPointer);
#else
  private void destroy_listoption_dialog();
  extern directoryManagerNewDirectory();
  private String makeoptionstring();
  private void optionbutton_toggled();
  extern void realize_dialog();
#endif

/*****************************************************************************
 *                          init_listoption_dialog                           *
 *****************************************************************************/
public void init_listoption(top)
Widget top;
{
  /* Initialise the long listing option dialog */

  XFontStruct *font;
  String settings;
  int width;

  static String Label = "Long Listing Options";

  /* constuct settings label */
  settings = (String) XtMalloc (sizeof(char) * 55);

  sprintf(settings, "%s%s%s%s%s%s%sfilename", 
	  listoptions[perms], 
	  listoptions[nlinks],
	  listoptions[owner],
	  listoptions[group],
	  listoptions[size],
	  listoptions[modtm],
	  listoptions[acctm]);

  listOptionDialog =
    XtVaCreatePopupShell(
        "listOptionDialog",
	transientShellWidgetClass,
	top,
	    NULL ) ;

  form1 =
    XtVaCreateManagedWidget(
        "form1",
	xedwFormWidgetClass,
	listOptionDialog,
	    NULL ) ;

  listoptionlabel =
    XtVaCreateManagedWidget(
        "listoptionlabel",
	labelWidgetClass,
	form1,
	    XtNfullWidth,          True,
	    XtNjustify, XtJustifyCenter,
	    XtNlabel,             Label,
	    XtNborderWidth,           0,
	    NULL ) ;

  listoptionsettings =
    XtVaCreateManagedWidget(
        "listoptionsettings",
	labelWidgetClass,
	form1,
	    XtNfullWidth,          True,
	    XtNfromVert, listoptionlabel,
	    XtNjustify,    XtJustifyLeft,
	    NULL ) ;

  /* Get font from widget, then use XTextWidth to find width of string,
   * then set the width of the label to this width + 20.
   */

  XtVaGetValues(
      listoptionsettings,
          XtNfont, &font,
	  NULL ) ;

  width = XTextWidth(font, settings, strlen(settings));

  XtVaSetValues(
      listoptionlabel,
          XtNwidth, width + 20,
	  NULL ) ;

  XtFree(settings);

  form2 =
    XtVaCreateManagedWidget(
        "form2",
	xedwFormWidgetClass,
	form1,
	    XtNborderWidth,               0,
	    XtNfromVert, listoptionsettings,
	    NULL ) ;

  form3 =
    XtVaCreateManagedWidget(
        "form3",
	xedwFormWidgetClass,
	form1,
	    XtNborderWidth,               0,
	    XtNfromVert, listoptionsettings,
	    XtNfromHoriz,   form2,
	    NULL ) ;

  /* First bank of buttons */

  /* Permissions */

  permsButton =
    XtVaCreateManagedWidget(
        "permsButton",
	commandWidgetClass,
	form2,
	    NULL ) ;

  permsLabel =
    XtVaCreateManagedWidget(
        "permsLabel",
	labelWidgetClass,
	form2,
	    XtNborderWidth, 0,
	    XtNfromHoriz,   permsButton,
	    NULL ) ;

  XtAddCallback(permsButton, XtNcallback, (XtCallbackProc)optionbutton_toggled,
		(XtPointer)perms);

  /* Nlinks */

  linksButton =
    XtVaCreateManagedWidget(
        "linksButton",
	commandWidgetClass,
	form2,
	    XtNfromVert,   permsButton,
	    NULL ) ;

  linksLabel =
    XtVaCreateManagedWidget(
        "linksLabel",
	labelWidgetClass,
	form2,
	    XtNfromVert,    permsButton,
	    XtNborderWidth, 0,
	    XtNfromHoriz,   linksButton,
	    NULL ) ;

  XtAddCallback(linksButton, XtNcallback, (XtCallbackProc)optionbutton_toggled,
		(XtPointer)nlinks);

  /* Owner */

  ownerButton =
    XtVaCreateManagedWidget(
        "ownerButton",
	commandWidgetClass,
	form2,
	    XtNfromVert,    linksButton,
	    NULL ) ;

  ownerLabel =
    XtVaCreateManagedWidget(
        "ownerLabel",
	labelWidgetClass,
	form2,
	    XtNfromVert,    linksButton,
	    XtNborderWidth, 0,
	    XtNfromHoriz,   ownerButton,
	    NULL ) ;

  XtAddCallback(ownerButton, XtNcallback, (XtCallbackProc)optionbutton_toggled,
		(XtPointer)owner);

  /* Second bank of buttons */

  /* Group */

  groupButton =
    XtVaCreateManagedWidget(
        "groupButton",
	commandWidgetClass,
	form3,
	    NULL ) ;

  groupLabel =
    XtVaCreateManagedWidget(
        "groupLabel",
	labelWidgetClass,
	form3,
	    XtNborderWidth, 0,
	    XtNfromHoriz,   groupButton,
	    NULL ) ;

  XtAddCallback(groupButton, XtNcallback, (XtCallbackProc)optionbutton_toggled,
		(XtPointer)group);

  /* Size */

  sizeButton =
    XtVaCreateManagedWidget(
        "sizeButton",
	commandWidgetClass,
	form3,
	    XtNfromVert, groupButton,
	    NULL ) ;

  sizeLabel =
    XtVaCreateManagedWidget(
        "sizeLabel",
	labelWidgetClass,
	form3,
	    XtNfromVert,    groupButton,
	    XtNborderWidth, 0,
	    XtNfromHoriz,   sizeButton,
	    NULL ) ;

  XtAddCallback(sizeButton, XtNcallback, (XtCallbackProc)optionbutton_toggled,
		(XtPointer)size);

  /* Modification Time */

  modtmButton =
    XtVaCreateManagedWidget(
        "modtmButton",
	commandWidgetClass,
	form3,
	    XtNfromVert, sizeButton,
	    NULL ) ;

  modtmLabel =
    XtVaCreateManagedWidget(
        "modtmLabel",
	labelWidgetClass,
	form3,
	    XtNfromVert,    sizeButton,
	    XtNborderWidth, 0,
	    XtNfromHoriz,   modtmButton,
	    NULL ) ;

  XtAddCallback(modtmButton, XtNcallback, (XtCallbackProc)optionbutton_toggled,
		(XtPointer)modtm);

  /* Access Time */  

  acctmButton =
    XtVaCreateManagedWidget(
        "acctmButton",
	commandWidgetClass,
	form3,
	    XtNfromVert, modtmButton,
	    NULL ) ;

  acctmLabel =
    XtVaCreateManagedWidget(
        "acctmLabel",
	labelWidgetClass,
	form3,
	    XtNfromVert,    modtmButton,
	    XtNborderWidth, 0,
	    XtNfromHoriz,   acctmButton,
	    NULL ) ;
  
  XtAddCallback(acctmButton, XtNcallback, (XtCallbackProc)optionbutton_toggled,
		(XtPointer)acctm);

  quitButton =
    XtVaCreateManagedWidget(
        "quitButton",
	commandWidgetClass,
	form1,
	    XtNfromVert, form2,
	    NULL ) ;

  XtAddCallback(quitButton, XtNcallback,
		(XtCallbackProc)destroy_listoption_dialog, (XtPointer)NULL);
}

/*****************************************************************************
 *                            WM_destroy_listoption_dialog                   *
 *****************************************************************************/
#if NeedFunctionPrototypes
private void WM_destroy_listoption_dialog(Widget w,
					  XtPointer client_data,
					  XEvent *event,
					  Boolean *dispatch)
#else
private void WM_destroy_listoption_dialog(w, client_data, event, dispatch)
     Widget w;
     XtPointer client_data;
     XEvent *event;
     Boolean *dispatch;
#endif
{
    extern Atom protocols[2]; /* defined in dialogs.c */
    
    if (event->xclient.message_type == protocols[1] &&
	event->xclient.data.l[0] == protocols[0])
    /* the widget got a kill signal */
    {
	/* call callbacks on Quit button */
	XtCallCallbacks(quitButton, XtNcallback, NULL);
    }
}

/*****************************************************************************
 *                            listoption_dialog                              *
 *****************************************************************************/
public void listoption_dialog()
{
  /* popup the listoption dialog. */

  String settings;

  /* set up initial button settings */

  ToggleButton(PERMS,  permsButton);
  ToggleButton(NLINKS, linksButton);
  ToggleButton(OWNER,  ownerButton);
  ToggleButton(GROUP,  groupButton);
  ToggleButton(SIZE,   sizeButton);
  ToggleButton(MODTM,  modtmButton);
  ToggleButton(ACCTM,  acctmButton);

  settings = makeoptionstring();

  XtVaSetValues(
      listoptionsettings,
          XtNlabel, settings,
	  NULL ) ;
  
  XtFree(settings);

  realize_dialog(listOptionDialog, NULL, XtGrabNonexclusive,
		 (XtEventHandler)WM_destroy_listoption_dialog, NULL);
}

/*****************************************************************************
 *                            destroy_listoption_dialog                      *
 *****************************************************************************/
/*ARGSUSED*/
private void destroy_listoption_dialog(w, client_data, call_data)
Widget w ;
XtPointer client_data ;
XtPointer call_data ;
{
  /* Popdown the listoption dialog, if we are in long listing mode then 
   * refresh the directory so we can see the changes.
   */

  extern String cwd;

  XtPopdown(listOptionDialog);
  if (current_mode.mode == Long)
    directoryManagerNewDirectory(cwd);
}

/*****************************************************************************
 *                         optionbutton_toggled                              *
 *****************************************************************************/
/*ARGSUSED*/
private void optionbutton_toggled(w, button, call_data)
Widget w;
ListOption button;
XtPointer call_data;
{
  /* One the option buttons has been pressed, the button id is contained within
   * 'button'. Toggle the current mode options with that option.
   */

  String settings;

  switch (button) {
  case perms:
    if (current_mode.options & PERMS) {
      current_mode.options &= ~PERMS;
    } else {
      current_mode.options |= PERMS;
    }
    ToggleButton(PERMS, permsButton);
    break;
  case nlinks:
    if (current_mode.options & NLINKS) {
      current_mode.options &= ~NLINKS;
    } else {
      current_mode.options |= NLINKS;
    }
    ToggleButton(NLINKS, linksButton);
    break;
  case owner:
    if (current_mode.options & OWNER) {
      current_mode.options &= ~OWNER;
    } else {
      current_mode.options |= OWNER;
    }
    ToggleButton(OWNER, ownerButton);
    break;
  case group:
    if (current_mode.options & GROUP) {
      current_mode.options &= ~GROUP;
    } else {
      current_mode.options |= GROUP;
    }
    ToggleButton(GROUP, groupButton);
    break;
  case size:
    if (current_mode.options & SIZE) {
      current_mode.options &= ~SIZE;
    } else {
      current_mode.options |= SIZE;
    }
    ToggleButton(SIZE, sizeButton);
    break;
  case modtm:
    if (current_mode.options & MODTM) {
      current_mode.options &= ~MODTM;
    } else {
      current_mode.options |= MODTM;
    }
    ToggleButton(MODTM, modtmButton);
    break;
  case acctm:
    if (current_mode.options & ACCTM) {
      current_mode.options &= ~ACCTM;
    } else {
      current_mode.options |= ACCTM;
    }
    ToggleButton(ACCTM, acctmButton);
    break;
  default:
    fprintf(stderr, "Error: Listoptions programmer error, option out of range\n");
    break;
  }

  /* Put current options in settings string */

  settings = makeoptionstring();

  /* Put new settings string in the settings label */

  XtVaSetValues(
      listoptionsettings,
          XtNlabel, settings,
	  NULL ) ;

  XtFree(settings);
}

/*****************************************************************************
 *                                                                           *
 *****************************************************************************/
private String makeoptionstring()
{
  /* construct a string representing the current options,
   * the user is responsible for free'ing it.
   */
  String permstring, nlinkstring, ownerstring, groupstring, sizestring;
  String modtmstring, acctmstring, settings;

  static String empty = "";
 
  settings = (String) XtMalloc (sizeof(char) * 55);

  if (current_mode.options & PERMS)
    permstring = listoptions[perms];
  else
    permstring = empty;
  if (current_mode.options & NLINKS)
    nlinkstring = listoptions[nlinks];
  else
    nlinkstring = empty;
  if (current_mode.options & OWNER)
    ownerstring = listoptions[owner];
  else
    ownerstring = empty;
  if (current_mode.options & GROUP)
    groupstring = listoptions[group];
  else
    groupstring = empty;
  if (current_mode.options & SIZE)
    sizestring = listoptions[size];
  else
    sizestring = empty;
  if (current_mode.options & MODTM)
    modtmstring = listoptions[modtm];
  else
    modtmstring = empty;
  if (current_mode.options & ACCTM)
    acctmstring = listoptions[acctm];
  else
    acctmstring = empty;

  /* Make display string for label of current settings*/

  sprintf(settings, "%s%s%s%s%s%s%sfilename", 
	  permstring, 
	  nlinkstring,
	  ownerstring,
	  groupstring,
	  sizestring,
	  modtmstring,
	  acctmstring);

  return(settings);
}
