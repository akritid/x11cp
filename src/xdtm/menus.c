/*****************************************************************************
 ** File          : menus.c                                                 **
 ** Purpose       : Create, and handle the pull down menus                  **
 ** Author        : Edward Groenendaal                                      **
 ** Date          : 18th Feb 1991                                           **
 ** Documentation : Xdtm Design Folder                                      **
 ** Related Files :                                                         **
 ** Changes       : 18-04-92, Edward Groenendaal                            **
 **                 Added #if NeedFunctionPrototypes stuff                  **
 **                 June 20, 1992, Ramon Santiago                           **
 **                 Changed all XtCreate calls to XtVaCreate calls.         **
 **                 Changed all caddr_t to XtPointer.                       **
 **                 Removed some lint.                                      **
 *****************************************************************************/

#include "xdtm.h"
#include "menus.h"
#include "parse.h"  /* Get type for process_list */
#include <stdio.h>
#include <unistd.h> /* for R_OK */
#include <sys/types.h>
#include <sys/stat.h>
#include <X11/Xaw/MenuButton.h>
#include <X11/Xaw/SimpleMenu.h>
#include <X11/Xaw/SmeBSB.h>
#include <X11/Xaw/SmeLine.h>

#include "Xedw/XedwList.h"
#include "Xedw/XedwForm.h"
#include "bitmaps/Tick"
#include "bitmaps/EmptyTick"

extern ProcessList *process_list;

static MenuContents fileMenuStrings[] = {
  { "about",     "About Xdtm...",  About,     noflag },
  { "help",      "Help",           Help,      noflag },
  {  LINE,        NULL,            0,         noflag },
  { "info",	 "Info",           Getinfo,   noflag },
  { "listprocs", "List Processes", Listprocs, noflag },
  {  LINE,        NULL,            0,         noflag },
  { "new",       "New File",       New,       noflag },
  { "duplicate", "Duplicate File", Duplicate, noflag },
  {  LINE,        NULL,            0,         noflag },
  { "copy",      "Copy files",     Copy,      noflag },
  { "move",      "Move files",     Move,      noflag },
  { "trash",     "Delete files",   Trash,     noflag },
  {  LINE,        NULL,            0,         noflag },
  { "reload",    "Reload config file", Reload, noflag},
  {  LINE,        NULL,            0,         noflag },
  { "quit",      "Quit Program",   Quit,      noflag },
};

static MenuContents optionMenuStrings[] = {
  { "dirfirst",       "Directories first", Dirfirst,       flagged },
#ifndef TRUE_SYSV
  { "followsymlinks", "Follow Sym Links",  FollowSymLinks, flagged },
#endif
  { "usedotspec",     "Use . Spec",        Usedotspec,     flagged },
  { "usedotdotspec",  "Use .. Spec",       Usedotdotspec,  flagged },
  {  LINE,             NULL,               0,              flagged },
  { "silentsel",      "Silent Selection",  Silentsel,      flagged },
};

static MenuContents viewMenuStrings[] = {
  { "icons",    "Show Icons",     Icons,   flagged },
  { "short",    "No Icons",       Short,   flagged },
  {  LINE,       NULL,            0,       flagged },
  { "long",     "Long Listing",   Long,    flagged },
  { "options",  "Options",        Options, flagged },
};

static MenuContents selectionMenuStrings[] = {
  { "map",      "Map Program over Files",     Map,    noflag },
  { "select",   "Select Files by Template",   Select, noflag },
};

Cardinal fileMenuSize   = sizeof(fileMenuStrings)   /
                                sizeof(fileMenuStrings[0]);
Cardinal optionMenuSize = sizeof(optionMenuStrings) /
                                sizeof(optionMenuStrings[0]);
Cardinal viewMenuSize   = sizeof(viewMenuStrings)   /
                                sizeof(viewMenuStrings[0]);
Cardinal selectionMenuSize   = sizeof(selectionMenuStrings)   /
                                sizeof(selectionMenuStrings[0]);

public Pixmap tick, emptytick;
public Widget menuBar;

public Icon_mode current_mode;

/* external and forward functions definitions */
#if NeedFunctionPrototypes
  extern void button_selected(Widget, Cardinal, XtPointer);
  extern void changestate(Boolean);
  public  void createMenu(Widget, MenuContents[], Cardinal, void (*)(Widget, Cardinal, XtPointer) );
  extern Boolean directoryManagerNewDirectory(String);
  extern void displayfile(String, String, String, XtGrabKind);
  extern void doubleclick_dialog(String, String);
  extern String getfilename(String);
  extern void listoption_dialog(void);
  extern void map_dialog(Boolean);
  private void menuSelect(Widget, Cardinal, XtPointer);
  extern void newfile_dialog(Boolean, String, Boolean);
  extern void popup_process_list(void);
  extern void loadConfig(void);
  extern void quit_dialog(void);
  extern void setCursor(Cursor);
#else
  extern void button_selected();
  extern void changestate();
  public  void createMenu();
  extern Boolean directoryManagerNewDirectory();
  extern void displayfile();
  extern void doubleclick_dialog();
  extern String getfilename();
  extern void listoption_dialog();
  extern void map_dialog();
  private void menuSelect();
  extern void newfile_dialog();
  extern void popup_process_list();
  extern void loadConfig();
  extern void quit_dialog();
  extern void setCursor();
#endif

/*****************************************************************************
 *                        createMenuWidgets                                  *
 *****************************************************************************/
public void createMenuWidgets(w)
Widget w;
{
  /* This procedure creates the widgets for the menu bar. 
   * It's 4:41 in the morning, I'm tired, but the work must go on..
   * so it's time to smile :-) Always look on the bright side of life... 
   */
 
  Widget fileMenuButton, optionMenuButton, viewMenuButton, selectionMenuButton;
  Widget fileMenu, optionMenu, viewMenu, selectionMenu;

  menuBar =
    XtVaCreateManagedWidget(
        "menuBar",
	xedwFormWidgetClass,
	w,
	    XtNrubberWidth,  False,
	    XtNrubberHeight, False,
	    XtNborderWidth,      0,
	    XtNtop,     XtChainTop,
	    XtNbottom,  XtChainBottom,
	    XtNleft,    XtChainLeft,
	    XtNright,   XtChainRight,
	    NULL ) ;

  fileMenuButton =
    XtVaCreateManagedWidget(
        "fileMenuButton",
        menuButtonWidgetClass,
	menuBar,
	    XtNhighlightThickness, 0,
	    XtNborderWidth,        0,
	    XtNmenuName,  "fileMenu",
	    XtNlabel,         "File",
	    XtNvertDistance,       0,
	    NULL ) ;

  optionMenuButton =
    XtVaCreateManagedWidget(
        "optionMenuButton",
	menuButtonWidgetClass,
	menuBar,
	    XtNhighlightThickness, 0,
	    XtNborderWidth,        0,
	    XtNmenuName,         "optionMenu",
	    XtNlabel,               "Options",
	    XtNfromHoriz,      fileMenuButton,
	    XtNvertDistance,                0,
	    NULL ) ;

  viewMenuButton =
    XtVaCreateManagedWidget(
        "viewMenuButton",
	menuButtonWidgetClass,
	menuBar,
	    XtNhighlightThickness, 0,
	    XtNborderWidth,        0,
	    XtNmenuName,        "viewMenu",
	    XtNlabel,               "View",
	    XtNfromHoriz, optionMenuButton,
	    XtNvertDistance,       0,
	    NULL ) ;

  selectionMenuButton =
    XtVaCreateManagedWidget(
        "selectionMenuButton",
	menuButtonWidgetClass,
	menuBar,
	    XtNhighlightThickness, 0,
	    XtNborderWidth,        0,
	    XtNmenuName,        "selectionMenu",
	    XtNlabel,               "Selection",
	    XtNfromHoriz, viewMenuButton,
	    XtNvertDistance,       0,
	    NULL ) ;

  fileMenu =
    XtVaCreatePopupShell(
        "fileMenu",
	simpleMenuWidgetClass,
	fileMenuButton,
	    NULL ) ;
  
  optionMenu =
    XtVaCreatePopupShell(
        "optionMenu",
	simpleMenuWidgetClass,
	optionMenuButton,
	    NULL ) ;
  
  viewMenu =
    XtVaCreatePopupShell(
        "viewMenu",
	simpleMenuWidgetClass,
	viewMenuButton,
	    NULL ) ;

  selectionMenu =
    XtVaCreatePopupShell(
        "selectionMenu",
	simpleMenuWidgetClass,
	selectionMenuButton,
	    NULL ) ;

  tick =
    XCreateBitmapFromData(
        XtDisplay(w),
	RootWindowOfScreen(XtScreen(w)),
	tick_bits, tick_width, tick_height);

  emptytick =
    XCreateBitmapFromData(
        XtDisplay(w),
	RootWindowOfScreen(XtScreen(w)),
	EmptyTick_bits, EmptyTick_width, EmptyTick_height);

  /* Default long listing options */

  current_mode.options = (char)app_data.options;

  /* create the menu panes from the arrays defined at the top of this file. */

  createMenu(fileMenu, fileMenuStrings, fileMenuSize, menuSelect);
  
  createMenu(optionMenu, optionMenuStrings, optionMenuSize, menuSelect);
  
  createMenu(viewMenu, viewMenuStrings, viewMenuSize, menuSelect);

  createMenu(selectionMenu, selectionMenuStrings, selectionMenuSize,
	     menuSelect);

}


/*****************************************************************************
 *                                createMenu                                 *
 *****************************************************************************/
public void createMenu(menu, menuStrings, menuSize, function)
Widget menu;
MenuContents menuStrings[];
Cardinal menuSize;
void (*function)();
{
  /* Given a MenuContents stucture, the number of entries and a function
   * that should be called when that pane is pressed, this procedure
   * creates the menu panes for the menu widget 'menu'
   */

  Widget menuEntry;
  Cardinal i, n;
  Arg arglist[3];

  for ( n = 0 ; n < menuSize ; n++ )
    {
      MenuContents entry ;
      String widgetname ;
      entry = menuStrings[n] ;
      widgetname = entry.paneName ;

      if (!strcmp(LINE, widgetname))
	{
	  menuEntry =
	    XtVaCreateManagedWidget(
	        widgetname,
		smeLineObjectClass,
		menu,
		    NULL ) ;
	}
      else
	{
	  i = 0;
	  XtSetArg(arglist[i], XtNlabel, entry.paneLabel); i++;
	  if (entry.set == flagged)
	    {
	      XtSetArg(arglist[i], XtNleftMargin, (tick_width*1.5)); i++;
	      if ((entry.paneNumber == current_mode.mode) ||
		  ((entry.paneNumber == Dirfirst) && app_data.dirfirst) ||
#ifndef TRUE_SYSV
		  ((entry.paneNumber == FollowSymLinks) && app_data.followsymlinks) ||
#endif
		  ((entry.paneNumber == Usedotspec) && app_data.usedotspec) ||
		  ((entry.paneNumber == Usedotdotspec) && app_data.usedotdotspec) ||
		  ((entry.paneNumber == Silentsel) && app_data.silentsel))
		{
		  XtSetArg(arglist[i], XtNleftBitmap, tick); i++; 
		}
	      else
		{
		  XtSetArg(arglist[i], XtNleftBitmap, None); i++; 
		}
	    }
	  
	  menuEntry =
	    XtCreateManagedWidget(
	        widgetname,
		smeBSBObjectClass,
		menu,
	        arglist,
		i ) ;
#if 0
	  menuEntry =
	    XtVaCreateManagedWidget(
	        widgetname,
		smeBSBObjectClass,
		menu,
		    XtNlabel, entry.paneLabel,
		    XtNleftMargin, ((entry.set == flagged) ? (tick_width*1.5) : 4),
		    XtNleftBitmap, ((entry.set == flagged) &&
				    (entry.paneNumber == current_mode.mode) ? tick : None),
		    NULL ) ;
#endif
	  if (entry.paneNumber == current_mode.mode && entry.set == flagged)
	    current_mode.w = menuEntry;

	  XtAddCallback(menuEntry, XtNcallback, function,
			(XtPointer)entry.paneNumber);
	}
    }
}

/*****************************************************************************
 *                              menuSelect                                   *
 *****************************************************************************/
private void menuSelect(w, paneNumber, rubbish)
Widget w;
Cardinal paneNumber;
XtPointer rubbish;
{
  /* This procedure is called when a pane is pressed in any of the main
   * three pull down menus.
   *
   * - Takes the paneNumber of the pane selected. Rest is discarded.
   */
  extern String cwd;
  extern Widget directoryManager;

  XedwListReturnStruct *highlighted;
  String filename, fullname, level;
  struct stat filestatus;

  switch( paneNumber )   /* Which pane was selected */
    {
      case About :       /* display about dialog */
	level = XtMalloc (sizeof(char) * 40);
	sprintf(level, "The X Desktop Manager Version %d.%d",
		RELEASE, PATCHLEVEL);
	alert_dialog(level, "bug-reports: rjs@pencom.com\n\
or         : Lionel.Mallet@sophia.inria.fr", NULL);
      break ;

      case Help :        /* If help file is readable show it */
	if (access(help_file, R_OK) == 0) 
	  displayfile(help_file, NULL, "X Desktop Manager Help", XtGrabNone);
	else
	  alert_dialog("Help file not found at", help_file, "Cancel");
      break ;

      case New :         /* Create a newfile */
	newfile_dialog(False, NULL, False);
      break ;

      case Duplicate :   /* Find out whether highlighted file is a regular file or a directory */
	highlighted = XedwListShowCurrent(directoryManager);
	if (highlighted->xedwList_index != XDTM_LIST_NONE)
	  {
	    if (highlighted->next != NULL) 
	      alert_dialog("You can only duplicate", "one file at a time",
			   "Cancel");
	    else
	      {
		filename = getfilename(highlighted->string);
		fullname=(String) XtMalloc((strlen(filename)+strlen(cwd)+3) * 
					   sizeof(char));
		strcpy(fullname, cwd);
		if (strcmp(cwd, "/") != 0)
		  strcat(fullname, "/");
		strcat(fullname, filename);
		if (stat(fullname, &filestatus) == -1)
		  {
		    fprintf(stderr,"xdtm: ARRRGGHHH stat error\n");
		  }
		else
		  {
		    if ((filestatus.st_mode & S_IFMT) == S_IFDIR) 
		      /* Is a directory */
		      newfile_dialog(True, filename, True);
		    else if ((filestatus.st_mode & S_IFMT) == S_IFREG) 
		      newfile_dialog(True, filename, False);
		    else 
		      alert_dialog("Sorry, that file can not", "be duplicated",
				   "Cancel");
		  }
		XtFree((char *)fullname);
	      }
	  }
	else
	  fprintf(stderr, "Error: Duplicate selected when should have been disabled\n");
	XedwListFreeCurrent(highlighted);
      break ;

      case Getinfo :
	highlighted = XedwListShowCurrent(directoryManager);
	if (highlighted->xedwList_index != XDTM_LIST_NONE)
	  {
	    if (highlighted->next != NULL)
	      alert_dialog("You can get info on only", "one file at a time",
			   "Cancel");
	    else
	      {
		filename = getfilename(highlighted->string);
		fullname=(String) XtMalloc((strlen(filename)+strlen(cwd)+3) * 
					   sizeof(char));
		strcpy(fullname, cwd);
		if (strcmp(cwd, "/") != 0)
		  strcat(fullname, "/");
		strcat(fullname, filename);
		if (stat(fullname, &filestatus) == -1)
		  {
		    fprintf(stderr,"xdtm: ARRRGGHHH stat error\n");
		  }
		else
		  {
		    doubleclick_dialog(filename,cwd);
		  }
		XtFree((char *)fullname);
	      }
	  }
	else
	  fprintf(stderr,
		  "Error: Info selected when should have been disabled\n");
	XedwListFreeCurrent(highlighted);
      break ;

      case Listprocs:
	popup_process_list();
      break;

      case Copy:
	/* FALL THROUGH */

      case Move:
	/* FALL THROUGH */

      case Trash:	/* Call button press with it */
	button_selected(w, paneNumber, 0);
      break;

    case Reload:  /* Reload config file */
        loadConfig();
	directoryManagerNewDirectory(cwd);
	selectionChange(w, 0, NULL);
	break;

    case Quit:    /* Quit the program.. maybe */
	if (app_data.confirmaction || process_list)
	  quit_dialog();
	else
	  exit(0);
	break ;

    case Dirfirst: /* Change to dirsfirst display option */
	app_data.dirfirst = !app_data.dirfirst;
	XtVaSetValues(w, XtNleftBitmap, (app_data.dirfirst ? tick : None),
		      NULL);
	directoryManagerNewDirectory(cwd);
	changestate(False);  /* implicitly deslecting dirman selections */
	break;
	
#ifndef TRUE_SYSV
    case FollowSymLinks: /* Change to followsymlinks display option */
	app_data.followsymlinks = !app_data.followsymlinks;
	XtVaSetValues(w, XtNleftBitmap, (app_data.followsymlinks ? tick : None),
		      NULL);
	directoryManagerNewDirectory(cwd);
	changestate(False);  /* implicitly deslecting dirman selections */
	break;
#endif 

    case Usedotspec: /* Change to usedotspec display option */
	app_data.usedotspec = !app_data.usedotspec;
	XtVaSetValues(w, XtNleftBitmap, (app_data.usedotspec ? tick : None),
		      NULL);
	directoryManagerNewDirectory(cwd);
	changestate(False);  /* implicitly deslecting dirman selections */
	break;
	
    case Usedotdotspec: /* Change to usedotdotspec display option */
	app_data.usedotdotspec = !app_data.usedotdotspec;
	XtVaSetValues(w, XtNleftBitmap, (app_data.usedotdotspec ? tick : None),
		      NULL);
	directoryManagerNewDirectory(cwd);
	changestate(False);  /* implicitly deslecting dirman selections */
	break;
	
    case Silentsel: /* Chnge to silentsel appman option */
	app_data.silentsel = !app_data.silentsel;
	XtVaSetValues(w, XtNleftBitmap, (app_data.silentsel ? tick : None),
		      NULL);
	break;
	
    case Icons:    /* Change to icon mode */
	if (current_mode.mode != Icons)
	{
	    XtVaSetValues( current_mode.w, XtNleftBitmap, None, NULL ) ;
	    XtVaSetValues( w, XtNleftBitmap, tick, NULL ) ;
	    current_mode.w = w;
	    current_mode.mode = Icons;
	    XtVaSetValues(
			  directoryManager,
			  XtNleftBitmap, tick,
			  XtNshowIcons,     True,
			  XtNrowSpacing,      10,
			  XtNforceColumns, False,
			  XtNdefaultColumns,   2,
			  NULL ) ;
	    directoryManagerNewDirectory(cwd);
	    changestate(False);  /* implicitly deslecting dirman selections */
	} 
	break;

    case Short:    /* Change to short listing mode */
	if (current_mode.mode != Short)
	{
	    XtVaSetValues( current_mode.w, XtNleftBitmap, None, NULL ) ;
	    XtVaSetValues( w, XtNleftBitmap, tick, NULL ) ;
	    current_mode.w = w;
	    current_mode.mode = Short;
	    XtVaSetValues(
			  directoryManager,
			  XtNshowIcons,    False,
			  XtNrowSpacing,       5,
			  XtNforceColumns, False,
			  XtNdefaultColumns,   2,
			  NULL ) ;
	    directoryManagerNewDirectory(cwd);	/* To be consistent */
	    changestate(False);  /* This implicitly deselects the selection
				    in the dirman. */
	} 
	break ;

    case Long:    /* change to long listing mode */
	if (current_mode.mode != Long)
	{
	    XtVaSetValues( current_mode.w, XtNleftBitmap, None, NULL ) ;
	    XtVaSetValues( w, XtNleftBitmap, tick, NULL ) ;
	    current_mode.w = w;
	    current_mode.mode = Long;
	    XtVaSetValues(
			  directoryManager,
			  XtNshowIcons,   False,
			  XtNrowSpacing,      5,
			  XtNforceColumns, True,
			  XtNdefaultColumns,  1,
			  NULL ) ;
	    directoryManagerNewDirectory(cwd);	   /* To be consistent */
	    changestate(False);  /* This was where the error was in leaving
				    things in 'selected mode' in the menus
				    and trash/copy/move. */
	}
	break ;
	
    case Options:	/* allow changes to long listing format. */
	listoption_dialog();
	break;
	
    case Map:    /* Map program over files */
	map_dialog(True);
	break;
	
    case Select: /* Select files via RE template, uses same dilaog as map,
		     except with different resources. */
	map_dialog(False);
	break;

      default:
	fprintf(stderr, "Menu option number %d not supported\n", paneNumber);
      break;
    }
}

