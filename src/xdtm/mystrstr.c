/*****************************************************************************
 ** File          : mystrstr.c                                              **
 ** Purpose       : Return pointer to first occurence of second string in   **
 **                 first string, else NULL                                 **
 ** Author        : Edward Groenendaal                                      **
 ** Date          : 11th April 1991                                         **
 ** Documentation : Xdtm Design Folder                                      **
 ** Related Files : appman.c                                                **
 ** Changes       : 18-04-92, Edward Groenendaal                            **
 **                 Used the famous nested while method instead of previous **
 **                 clumsy method.                                          **
 **                 Added #if NeedFunctionPrototypes stuff                  **
 *****************************************************************************/

#ifndef HAS_STRSTR

#include <stdio.h>  /* For NULL */

char *mystrstr(cs, ct)
     char *cs;
     char *ct;
{
  if (*ct == '\0')
    return(cs);

  while(*cs != '\0') {
    register char *cs2, *ct2;
    cs2 = cs;
    ct2 = ct;
    
    while (*ct2++ == *cs2++) /* Got start.. match rest */
      if (*ct2 == '\0')
	return(cs);
    
    cs++; /* Still looking for start */
  }

  return((char*)NULL);
}

#endif /* HAS_STRSTR */




