/*****************************************************************************
 ** File          : newfile.c                                               **
 ** Purpose       : Initialise and Realise newfile dialog                   **
 ** Author        : Edward Groenendaal                                      **
 ** Date          : April 1991                                              **
 ** Documentation : Xdtm Design Folder                                      **
 ** Related Files :                                                         **
 ** Changes       : 18-04-92, Edward Groenendaal                            **
 **                 Added #if NeedFunctionPrototypes stuff                  **
 **                 June 20, 1992, Ramon Santiago                           **
 **                 Changed all XtCreate calls to XtVaCreate calls.         **
 **                 Changed all caddr_t to XtPointer.                       **
 **                 Removed some lint.                                      **
 *****************************************************************************/

#include "xdtm.h"
#include "parse.h"

#include <math.h>
#include <string.h>
#ifdef SYSV
#include <sys/types.h>
#include <sys/stat.h>
#endif
#include <fcntl.h>

#include <X11/Shell.h>
#include <X11/Xaw/Label.h>
#include <X11/Xaw/Command.h>
#include <X11/Xaw/AsciiText.h>
#include "Xedw/XedwForm.h"

/* Widgets */

private Widget newfilepopup;
private Widget newfileform;
private Widget newfilelabel;
private Widget newfiletext;
private Widget newfilefile;
private Widget newfiledir;
private Widget newfilecancel;

typedef struct {
  String filename;
  Boolean isdir;
} Filetype;

/* external and forward functions definitions */
#if NeedFunctionPrototypes
  private void destroy_newfile_dialog(Widget, XtPointer, XtPointer);
  extern Boolean directoryManagerNewDirectory(String);
  private void duplicateQueryReturn(Widget, Filetype*, XtPointer);
  extern int execute(String, String, String, Boolean, AppProgram *);
  private void newfileQueryReturn(Widget, Boolean, XtPointer);
  extern void realize_dialog(Widget, Widget, XtGrabKind, XtEventHandler, XtPointer);
#else
  private void destroy_newfile_dialog();
  extern Boolean directoryManagerNewDirectory();
  private void duplicateQueryReturn();
  extern int execute();
  private void newfileQueryReturn();
  extern void realize_dialog();
#endif

/*****************************************************************************
 *                             init_newfile                                  *
 *****************************************************************************/
public void init_newfile(top)
Widget top;
{
  /* Create Widgets for newfile dialog */

  XtTranslations newfileTranslations;

  /* Translations for text widget */
  static char defaultTranslations[] = 
    "Ctrl<Key>A:        beginning-of-line() \n\
     Ctrl<Key>E:        end-of-line() \n\
     <Key>Escape:       beginning-of-line() kill-to-end-of-line() \n\
     <Key>Right: 	forward-character() \n\
     <Key>Left:         backward-character() \n\
     <Key>Delete:       delete-previous-character() \n\
     <Key>BackSpace:    delete-previous-character() \n\
     <Key>:             insert-char() \n\
     <FocusIn>:         focus-in() \n\
     <FocusOut>:        focus-out() \n\
     <BtnDown>:         select-start()";
	
  newfilepopup =
    XtVaCreatePopupShell(
        "New File",
	transientShellWidgetClass,
	top,
	    NULL ) ;

  newfileform =
    XtVaCreateManagedWidget(
        "newfileform",
	xedwFormWidgetClass,
	newfilepopup,
	    NULL ) ;

  /* label widget to prompt for new filename */

  newfilelabel =
    XtVaCreateManagedWidget(
        "newfilelabel",
	labelWidgetClass,
	newfileform,
	    XtNfullWidth,          True,
	    XtNborderWidth,           0,
	    XtNjustify, XtJustifyCenter,
	    NULL ) ;

  /* new filename text input widget */

  newfiletext =
    XtVaCreateManagedWidget(
        "newfiletext",
	asciiTextWidgetClass,
	newfileform,
	    XtNfullWidth,          True,
	    XtNfromVert, newfilelabel,
	    XtNeditType, XawtextEdit,
	    NULL ) ;

  newfiledir =
    XtVaCreateManagedWidget(
        "newfiledir",
	commandWidgetClass,
	newfileform,
	    XtNfromVert, newfiletext,
	    XtNlabel,    "Directory",
	    NULL ) ;

  newfilefile =
    XtVaCreateManagedWidget(
        "newfilefile",
	commandWidgetClass,
	newfileform,
	    XtNfromVert, newfiletext,
	    XtNfromHoriz,   newfiledir,
	    XtNwidthLinked, newfiledir,
	    XtNlabel,           "File",
	    NULL ) ;

  newfilecancel =
    XtVaCreateManagedWidget(
        "newfilecancel",
	commandWidgetClass,
	newfileform,
	    XtNfromVert, newfiletext,
	    XtNfromHoriz,   newfilefile,
	    XtNwidthLinked, newfilefile,
	    XtNlabel,          "Cancel",
	    NULL ) ;

  /* Add callbacks for buttons */
  XtAddCallback(newfilecancel, XtNcallback,
		(XtCallbackProc)destroy_newfile_dialog, (XtPointer)0); 

  /* Do the translations on the text widget */
  XtUninstallTranslations(newfiletext);
  newfileTranslations = XtParseTranslationTable(defaultTranslations);
  XtOverrideTranslations(newfiletext, newfileTranslations);
}

/*****************************************************************************
 *                            WM_destroy_newfile_dialog                      *
 *****************************************************************************/
#if NeedFunctionPrototypes
private void WM_destroy_newfile_dialog(Widget w,
				       XtPointer client_data,
				       XEvent *event,
				       Boolean *dispatch)
#else
private void WM_destroy_newfile_dialog(w, client_data, event, dispatch)
     Widget w;
     XtPointer client_data;
     XEvent *event;
     Boolean *dispatch;
#endif
{
    extern Atom protocols[2]; /* defined in dialogs.c */
    
    if (event->xclient.message_type == protocols[1] &&
	event->xclient.data.l[0] == protocols[0])
    /* the widget got a kill signal */
    {
	/* call callbacks on Cancel button */
	XtCallCallbacks(newfilecancel, XtNcallback, NULL);
    }
}

/*****************************************************************************
 *                             newfile_dialog                                *
 *****************************************************************************/
public void newfile_dialog(rename, newname, isdir)
Boolean rename;
String newname;
Boolean isdir;
{
  /* Popup the newfile dialog on screen with the correct data 
   *
   * - Takes a flag saying whether this is a duplication of a file, or the
   *   creation of a new file or directory.
   *   newname - The name of the file to be duplicated.
   *   isdir   - Whether the file is a directory 
   */

  String filename;
  Filetype *filetype;

  static String defaultfilename = "untitled";

  if (newname == NULL) 
    filename = defaultfilename;
  else
    filename = newname;

  if (rename == False) {

    /* Create a new file */

    XtVaSetValues( newfilelabel, XtNlabel, "Create a new file", NULL ) ;
    XtVaSetValues( newfiletext, XtNstring, filename, NULL ) ;

    /* Add callbacks for buttons */

    XtAddCallback(newfiledir,    XtNcallback,
		  (XtCallbackProc)newfileQueryReturn,  (XtPointer)True);
    XtAddCallback(newfilefile,   XtNcallback,
		  (XtCallbackProc)newfileQueryReturn, (XtPointer)False);
  } else {
    filetype = (Filetype *) XtMalloc(sizeof(Filetype));

    /* rename an existing file */
    filetype->filename = XtNewString(filename);
    filetype->isdir    = isdir;

    XtVaSetValues( newfilelabel, XtNlabel, "Duplicate file", NULL ) ;
    XtVaSetValues( newfiletext, XtNstring, filename, NULL ) ;

    if (isdir == True) 
      XtSetSensitive(newfilefile, False);
    else
      XtSetSensitive(newfiledir, False);
    XtAddCallback(newfiledir,  XtNcallback,
		  (XtCallbackProc)duplicateQueryReturn, (XtPointer)filetype);
    XtAddCallback(newfilefile, XtNcallback,
		  (XtCallbackProc)duplicateQueryReturn, (XtPointer)filetype);
  }

  realize_dialog(newfilepopup, NULL, XtGrabNonexclusive,
		 (XtEventHandler)WM_destroy_newfile_dialog, NULL);
}

/*****************************************************************************
 *                        destroy_newfile_dialog                             *
 *****************************************************************************/
/*ARGSUSED*/
private void destroy_newfile_dialog(w, client_data, call_data)
Widget w ;
XtPointer client_data ;
XtPointer call_data ;
{
  /* Popdown and destroy callbacks for the newfile dialog. Also back sure
   * that both buttons are sensitive.
   */

  XtPopdown(newfilepopup);
  
  XtSetSensitive(newfilefile, True);
  XtSetSensitive(newfiledir,  True);
  XtRemoveAllCallbacks(newfilefile, XtNcallback);
  XtRemoveAllCallbacks(newfiledir,  XtNcallback);
}

/*****************************************************************************
 *                            newfileQueryReturn                             *
 *****************************************************************************/
/*ARGSUSED*/
#if NeedFunctionPrototypes
private void newfileQueryReturn(Widget w, Boolean isdir, XtPointer dummy)
#else
private void newfileQueryReturn(w, isdir, dummy)
Widget w;
Boolean isdir;
XtPointer dummy;
#endif
{
  /* This procedure creates a new empty directory or file with the filename
   * in the text widget newfiletext.
   *
   * - Takes a flag isdir, is the new file a directory?
   */

  extern String cwd;
  String filename, fullname;
  int fd;
  
  destroy_newfile_dialog(w, 0, 0);

  XtVaGetValues( newfiletext, XtNstring, &filename, NULL ) ;

  fullname = (String) XtMalloc (sizeof(char) * (strlen(cwd) + 
						strlen(filename) + 4));
  sprintf(fullname, "%s/%s", cwd, filename);

  if (isdir == True) {
    /* Create a directory with the name 'filename' */
    if ((fd = mkdir(fullname, 0777)) == -1) 
      alert_dialog("Sorry, can't create directory", fullname, "Cancel");
    else
      directoryManagerNewDirectory(cwd);
  } else {
    /* Create a file with the name 'filename' */
    if ((fd = creat(fullname, 0666)) == -1) {
      /* Can't create fullname, maybe should look in errno to see why? */
      alert_dialog("Sorry, can't create file", fullname, "Cancel");
    } else {
      close(fd);
      directoryManagerNewDirectory(cwd);
    }
  }
  XtFree(fullname);
}

/*****************************************************************************
 *                            duplicateQueryReturn                           *
 *****************************************************************************/
/*ARGSUSED*/
private void duplicateQueryReturn(w, filetype, dummy)
Widget w;
Filetype *filetype;
XtPointer dummy;
{
  /* This procedure copies a file in the current directory to a file with
   * the name as specified within the text widget newfiletext. The orignal
   * filename and type are passed as arguments to this procedure in the
   * structure filetype.
   */

  extern String cwd;
  String filename, slash_pos, command = XtNewString("sh -c 'mkdir");
  String tmpname = XtNewString("MaNgLeD");
  int pid = getpid();
  int lenpid = (int)log10((double) pid) + 1;
  int lentmp;
  int fd;
  
  destroy_newfile_dialog(w, 0, 0);
  
  XtVaGetValues( newfiletext, XtNstring, &filename, NULL ) ;

  while ((fd = open(tmpname, O_RDONLY)) >= 0) 
  {
      /* open was successful, we cannot this name as a temporary name */
      close(fd);
      tmpname = XtRealloc(tmpname,
			  sizeof(char) * (strlen(tmpname) + lenpid +1));
      sprintf(tmpname, "%s%d", tmpname, pid);
  }
  
  /* Now tmpname is OK, we can use it to create a temporary dir like foo
     and build a command like:
   sh -c 'mkdir foo;tar cf - src|(cd foo;tar xBf -);mv foo/src dest;rmdir foo'
   where the mv should be done with care as dest can be on a different file
   system (when dest name include at least one /)
   Here, src = filetype->filename and dest = filename */

  lentmp = strlen(tmpname);
  command = XtRealloc(command, sizeof(char) * (strlen(command) + (2 * lentmp) +
				       strlen(filetype->filename) + 28));
  sprintf(command, "%s %s;tar cf - %s|(cd %s;tar xBf -)", command, tmpname,
	  filetype->filename, tmpname);
  
  if ((slash_pos = strrchr(filename, '/')) != NULL)
  {
      /* There it is, there's a / in filename so mv can be across file
	 systems. Rather do it with tar. */

      String basename = slash_pos + 1, path = filename;
      slash_pos[0] = 0; /* To seperate base and path in filename */

      command = XtRealloc(command, sizeof(char) * (strlen(command) + lentmp +
					   strlen(filetype->filename) +
					   (2 * strlen(basename)) +
					    strlen(path) + 40));
      sprintf(command, "%s;(cd %s;mv %s %s;tar cf - %s)|(cd %s;tar xBf - )",
	      command, tmpname, filetype->filename, basename, basename, path);

  } else {
      /* That's fine, filename is on the same file system as
	 filetype->filename, just move tmpname/filetype->filename into
	 filename. */

      command = XtRealloc(command, sizeof(char) * (strlen(command) + lentmp +
						   strlen(filetype->filename) +
						   strlen(filename) + 7));
      sprintf(command, "%s;mv %s/%s %s", command, tmpname, filetype->filename,
	      filename);
  }

  command = XtRealloc(command, sizeof(char) * (strlen(command) + lentmp + 10));
  sprintf(command, "%s;rm -rf %s'", command, tmpname);
  
  /* Now that command is built, try to execute it */
  if (execute(NULL, "sh", command, True, NULL) != 0) 
  {
      if (filetype->isdir == True)
	alert_dialog("Sorry, can't duplicate directory", filetype->filename,
		     "Cancel");
      else
	alert_dialog("Sorry, can't duplicate file", NULL, "Cancel");
  }
  else
    directoryManagerNewDirectory(cwd);

  XtFree(filetype->filename);
  XtFree((char *)filetype);
  XtFree(tmpname);
  XtFree(command);
}
