/*****************************************************************************
 ** File          : parse.c                                                 **
 ** Purpose       : Parse setup file, select icon to be used.               **
 ** Author        : Edward Groenendaal                                      **
 ** Date          : 19th Feb 1991, Jan 1992                                 **
 ** Documentation : Xdtm Design Folder                                      **
 ** Changes       : 18-04-92, Edward Groenendaal                            **
 **                 Added #if NeedFunctionPrototypes stuff                  **
 **                 June 20, 1992, Ramon Santiago                           **
 **                 Changed all XtCreate calls to XtVaCreate calls.         **
 **                 Changed all caddr_t to XtPointer.                       **
 **                 Removed some lint.                                      **
 ****************************************************************************/

#include "xdtm.h"
#include "parse.h"
#include "menus.h"

#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <sys/param.h>

#include <pwd.h>
#include <grp.h>

#include "Xedw/XedwList.h"

#include "bitmaps/Grey.Mask"
#include "bitmaps/folder.icon"
#include "bitmaps/file.icon"

typedef struct _BTree {
  Pixmap icon;
  Pixmap grey;
  struct _BTree *left;
  struct _BTree *right;
} BTree;

extern FILE *yyin;
extern typePrefs *prefs ;

#ifdef ESIX
typedef long uid_t;    /* jcc */
#endif

public char *preferences_filename;
public uid_t user;
public uid_t group;
private BTree *grey_tree;

/* external and forward functions definitions */
#if NeedFunctionPrototypes
  private iconPrefs *applyprefs(typePrefs*, String, String, struct stat*);
  extern Widget createIconMenu(AppProgram *[], Cardinal);
  extern void createMenu(Widget, MenuContents[], Cardinal, void (*)(Widget, Cardinal, XtPointer));
  private iconPrefs *do_iconprefs(iconPrefs*, String, String, struct stat*);
  private Pixmap grey_icon(Pixmap);
  private BTree *grey_search(Pixmap, Pixmap*, BTree*);
  public void loadConfig(void);
  private void initBitmaps(Widget);
  extern typePrefs *newTypePref(void);
  extern void selectionChange(Widget, Cardinal, XtPointer);
  extern int user_is_member(gid_t);
  extern int yyparse(void);
#else
  private iconPrefs *applyprefs();
  extern Widget createIconMenu();
  extern void createMenu();
  private iconPrefs *do_iconprefs();
  private Pixmap grey_icon();
  private BTree *grey_search();
  public void loadConfig();
  private void initBitmaps();
  extern typePrefs *newTypePref();
  extern void selectionChange();
  extern int user_is_member();
  extern int yyparse();
#endif

public GC xdtmgc;
public Pixmap foldericon, fileicon, grey_mask; /* default icons */

/*****************************************************************************
 *                               getIconType                                 *
 *****************************************************************************/
public Boolean getIconType(filename, path, element)
String filename, path;
XdtmList *element;
{
  /* Return True if file should be displayed, otherwise False. 
   * if long listing is on, expand the filename to contain the extra
   * data as well.
   */

  extern Icon_mode current_mode;

  struct stat *filestatus;
  iconPrefs *pref;
  typePrefs *myprefs = prefs;
  String fullname;
  Pixmap icon;
  
  element->string = XtNewString(filename);

  if (current_mode.mode == Short) element->icon = (Pixmap)NULL;
  else {
    filestatus = (struct stat*) XtMalloc (sizeof(struct stat));
    fullname=(String) XtMalloc((strlen(filename)+strlen(path)+4) * 
			       sizeof(char));
    strcpy(fullname, path);
    strcat(fullname, "/");
    strcat(fullname, filename);
#ifdef TRUE_SYSV
    if (stat(fullname, filestatus) == -1) {    /* jcc, no symbolic links */
#else
    if (lstat(fullname, filestatus) == -1) {
#endif
      /* maybe a link to a nonexistent file? */
      XtFree((char *)filestatus);
      XtFree(fullname);
      return(False);
    } else {
      if ((pref = applyprefs(myprefs, fullname, filename, filestatus)))
	  element->user_data = (XtPointer)pref->user_data;
      else
	  element->user_data = NULL;
      if (current_mode.mode == Icons) {
	element->icon = fileicon;
	element->mask = (Pixmap)NULL;
        element->isdir = False;
	if ((filestatus->st_mode & S_IFMT) == S_IFDIR) { 
	  element->icon = foldericon;
	  element->mask = (Pixmap)NULL;
          element->isdir = True;
        }
      
	/* try all rules in prefs */
	if (pref && ((icon = pref->icon) != None)) {/* should be
						     if (pref && ((icon = ...
						     to avoid forcing user to
						     set deficon on ANY file */
	
	  /* If the file is a directory but is not readable AND executable by 
	   * the user then grey it. If the file is not readable then grey it.
	   */

#ifdef DEBUG
	  fprintf(stderr, "getIconType : about to test icon != DUMMY \n");
	  fprintf(stderr, "filestatus->st_gid = %d\n", filestatus->st_gid);
#endif
	  if (icon != DUMMY) {
#ifdef DEBUG
	    fprintf(stderr, "icon != DUMMY\n");
#endif

	    /* if the file is not either
	       a) owner readable and owned by the user, or
	       b) group readable and in the group of the user, or
	       c) readble by others
	       then grey the icon */

	    if (!(
		  (
		   (filestatus->st_mode & S_IRUSR) &&
		   (user == filestatus->st_uid)
		  ) || (
		    (filestatus->st_mode & S_IRGRP) &&
		    user_is_member(filestatus->st_gid)
		  ) || (filestatus->st_mode & S_IROTH)
	         )) icon = grey_icon(icon);
	    else {
#ifdef DEBUG
	      fprintf(stderr, "Testing the link\n");
#endif
	      if ((filestatus->st_mode & S_IFMT) == S_IFDIR)
		if (!(( (filestatus->st_mode & S_IXUSR) &&
		        (user == filestatus->st_uid) ) ||
		      ( (filestatus->st_mode & S_IXGRP) &&
		        user_is_member(filestatus->st_gid) ) ||
		      (filestatus->st_mode & S_IXOTH))) {
		  icon = grey_icon(icon);
		}
	    }
	  }
#ifdef DEBUG
	  fprintf(stderr, "getIconType : icon type set, no crash yet\n");
#endif
	  element->icon = icon;
	  element->mask = pref->mask;

        }
      } else {
	/* Long Listing, construct the line*/
	Cardinal length;
	char cm = current_mode.options;
	String output_line;

	element->icon = None;
	element->mask = None;
	length = 0;
	output_line = XtNewString("");
	if (cm & PERMS) {
	  char type;
	  char xusr, xgrp, xoth;
	  unsigned short mode = (filestatus->st_mode);

	  output_line = (String) XtMalloc (sizeof(char) * 12);

	  /* I could use the S_IS* macros here but I prefer to see what's
	   * going on.
	   */
	  if ((mode & S_IFMT) == S_IFDIR) type = 'd';      /* Directory */
	  else if ((mode & S_IFMT) == S_IFBLK) type = 'b'; /* Block device */
	  else if ((mode & S_IFMT) == S_IFCHR) type = 'c'; /* Character device */
	  else if ((mode & S_IFMT) == S_IFREG) type = ' '; /* Plain file */
	  else if ((mode & S_IFMT) == S_IFIFO) type = 'f'; /* Fifo */
#ifndef TRUE_SYSV					   /* jcc */
	  else if ((mode & S_IFMT) == S_IFLNK) type = 'l'; /* Symbolic link */ 
	  else if ((mode & S_IFMT) == S_IFSOCK) type = 's';/* Socket */
#endif
	  else type = '?';
	  
	  if (mode & S_ISUID) 	/* Set User Id */
	    xusr = 's';
	  else if (mode & S_IXUSR)
	    xusr = 'x';
	  else 
	    xusr = '-';

	  if (mode & S_ISGID)	/* Set Group Id */
	    xgrp = 's';
	  else if (mode & S_IXGRP)
	    xgrp = 'x';
	  else
	    xgrp = '-';

	  if (mode & S_ISVTX)	/* Save Text */
	    xoth = 't';
	  else if (mode & S_IXOTH)
	    xoth = 'x';
	  else 
	    xoth = '-';

	  sprintf(output_line, 
		  "%c%c%c%c%c%c%c%c%c%c ",
		  type,
		  (mode & S_IRUSR) ? 'r' : '-',
		  (mode & S_IWUSR) ? 'w' : '-',
		  xusr,
		  (mode & S_IRGRP) ? 'r' : '-',
		  (mode & S_IWGRP) ? 'w' : '-',
		  xgrp,
		  (mode & S_IROTH) ? 'r' : '-',
		  (mode & S_IWOTH) ? 'w' : '-',
		  xoth);
	} 

	if (cm & NLINKS) {
	  output_line = XtRealloc (output_line, 
				   sizeof(char) *
				   (strlen(output_line) + 3 + 4));
	  
	  sprintf(output_line, "%s%3d ", output_line, 
		  filestatus->st_nlink);
	}
	if (cm & OWNER) {
	  struct passwd *pw;

	  output_line = XtRealloc (output_line,
				   sizeof(char) *
				  (strlen(output_line) + 8 + 4));
	  
	  if ((pw = getpwuid(filestatus->st_uid)) == NULL)
	    sprintf(output_line, "%s%-8d ", output_line, filestatus->st_uid);
	  else
	    sprintf(output_line, "%s%-8s ", output_line, pw->pw_name);

	}
	if (cm & GROUP) {
	  struct group *gr;
	  
	  output_line = XtRealloc (output_line,
				   sizeof(char) *
				  (strlen(output_line) + 8 + 4));
	  
	  if ((gr = getgrgid(filestatus->st_gid)) == NULL)
	    sprintf(output_line, "%s%-8d ", output_line, filestatus->st_gid);
	  else
	    sprintf(output_line, "%s%-8s ", output_line, gr->gr_name);

	}
	if (cm & SIZE)  {
	  unsigned short mode = (filestatus->st_mode);

	  output_line = XtRealloc (output_line, 
				   sizeof(char) *
				  (strlen(output_line) + 9 + 4));
	  if ((mode & S_IFMT) == S_IFBLK || (mode & S_IFMT) == S_IFCHR)
	    sprintf(output_line, "%s%5d,%3d ", output_line, 
		    (filestatus->st_rdev >> 8) & 0377,
		    (filestatus->st_rdev) & 0377);
	  else
	    sprintf(output_line, "%s%9d ", output_line, filestatus->st_size);
	}
	if (cm & MODTM) {
	  String time;
	  output_line = XtRealloc (output_line,
				   sizeof(char) *
				   (strlen(output_line) + 35 + 4));

	  time = (String)ctime(&(filestatus->st_mtime));
	  *(time + (strlen(time) - 1)) = '\0';
	  sprintf(output_line, "%s%s ", output_line, time);
	}
	if (cm & ACCTM) {
	  String time;
	  output_line = XtRealloc (output_line,
				   sizeof(char) *
				   (strlen(output_line) + 35 + 4));

	  time = (String)ctime(&(filestatus->st_atime));
	  *(time + (strlen(time) - 1)) = '\0';
	  sprintf(output_line, "%s%s ", output_line, time);
	}
	current_mode.length = strlen(output_line); /* length of data */
	output_line = XtRealloc (output_line,
				 sizeof(char) * 
				(strlen(output_line) + strlen(filename) + 4));
	sprintf(output_line, "%s%s", output_line, filename);
	element->string = output_line;
      }
      
    }
  
    XtFree((char *)filestatus);
    XtFree((char *)fullname);
  }
  
  if (element->icon == DUMMY)
    return(False);
  else
    return(True);
}

/*****************************************************************************
 *                                applyprefs                                 *
 *****************************************************************************/
private iconPrefs *applyprefs(tp, fullname, filename, filestatus)
typePrefs *tp;
String fullname, filename;
struct stat *filestatus;
{
  /* Recursively traverse the prefs structure until a match is found.
   * Jump out as soon as a match is found, try special files first */

  static Boolean insymlink = False;
  iconPrefs *pref = NULL;
  struct stat *newfilestatus;
  
  /* Try directories */
  if (pref == NULL && 
      tp->dir != NULL &&
      (filestatus->st_mode & S_IFMT) == S_IFDIR) {
    pref = applyprefs(tp->dir, fullname, filename, filestatus);
  }

  /* Try plain files */
  if (pref == NULL &&
      tp->file != NULL &&
      (filestatus->st_mode & S_IFMT) == S_IFREG) {
    pref = applyprefs(tp->file, fullname, filename, filestatus);
  }

  /* Try Block special devices */
  if (pref == NULL &&
      tp->block != NULL &&
      (filestatus->st_mode & S_IFMT) == S_IFBLK) {
    pref = applyprefs(tp->block, fullname, filename, filestatus);
  }

  /* Try Character special devices */
  if (pref == NULL &&
      tp->character != NULL &&
      (filestatus->st_mode & S_IFMT) == S_IFCHR) {
    pref = applyprefs(tp->character, fullname, filename, filestatus);
  }

  /* Try Fifo's */
  if (pref == NULL &&
      tp->fifo != NULL &&
      (filestatus->st_mode & S_IFMT) == S_IFIFO) {
    pref = applyprefs(tp->fifo, fullname, filename, filestatus);
  }

#if defined(macII) || !defined(TRUE_SYSV)
  /* Try Symbolic Links */
  if (app_data.followsymlinks == False) {
    if (pref == NULL &&
	tp->slink != NULL &&
	insymlink == False &&
	(filestatus->st_mode & S_IFMT) == S_IFLNK) {
	insymlink = True;
	newfilestatus = (struct stat*) XtMalloc (sizeof(struct stat));
	if (stat(fullname, newfilestatus) != -1) {
	    pref = applyprefs(tp->slink, fullname, filename, newfilestatus);
	}
	XtFree((char *)newfilestatus);
	insymlink = False;
    }

  } else {
    if (pref == NULL &&
	(filestatus->st_mode & S_IFMT) == S_IFLNK) {
	char lkfullname[MAXPATHLEN];
	char *lkname;
	int nbytes;
	
	newfilestatus = (struct stat*) XtMalloc (sizeof(struct stat));
	if (stat(fullname, newfilestatus) != -1) {
	    if ((nbytes = readlink(fullname, lkfullname, MAXPATHLEN)) >= 0) {
	      lkfullname[nbytes] = 0;
	      if ((lkname = strrchr(lkfullname, '/')) == NULL)
		lkname = lkfullname;
	      else lkname++;
	      pref = applyprefs(tp, lkfullname, lkname, newfilestatus);
	    } else if (tp->slink != NULL && insymlink == False) {
		insymlink = True;
		pref = applyprefs(tp->slink, fullname, filename, newfilestatus);
		insymlink = False;
	    }
	    
	}
	XtFree((char *)newfilestatus);
    }
  }
  
      
  

  /* Try Sockets */
  if (pref == NULL &&
      tp->socket != NULL &&
      (filestatus->st_mode & S_IFMT) == S_IFSOCK) {
    pref = applyprefs(tp->socket, fullname, filename, filestatus);
  }

#endif

  /* Try executable files */
  if (pref == NULL &&
      tp->exe != NULL &&
      (filestatus->st_mode & (S_IXUSR | S_IXGRP | S_IXOTH)) != 0) {
    pref = applyprefs(tp->exe, fullname, filename, filestatus);
  }

  /* Try Readable files */
  if (pref == NULL && 
      tp->read != NULL &&
      (filestatus->st_mode & (S_IRUSR | S_IRGRP | S_IROTH)) != 0) {
    pref = applyprefs(tp->read, fullname, filename, filestatus);
  }
  
  /* Try Writable files */
  if (pref == NULL && 
      tp->write != NULL &&
      (filestatus->st_mode & (S_IWUSR | S_IWGRP | S_IWOTH)) != 0) {
    pref = applyprefs(tp->write, fullname, filename, filestatus);
  }

  /* defaults */
  if (pref == NULL && tp->iconprefs != NULL) {
    pref = do_iconprefs(tp->iconprefs, fullname, filename, filestatus);
  }

  return(pref);
}

/*****************************************************************************
 *                                  grey_icon                                *
 *****************************************************************************/
private Pixmap grey_icon(icon)
Pixmap icon;
{
  /* Get a greyed icon for the icon 'icon'. */

  Pixmap result;
  
  grey_tree = grey_search(icon, &result, grey_tree);
  
  return(result);
}

/*****************************************************************************
 *                                grey_search                                *
 *****************************************************************************/
private BTree *grey_search(icon, new, tree)
Pixmap icon;
Pixmap *new;
BTree *tree;
{
  /* Search through the grey pixmap tree for a pixmap with the same id,
   * if none is found, make a new one by And'ing the original icon
   * with a stippled pixmap, put this into the tree, and also return it.
   * This procedure should still work even if the representation of a
   * Pixmap changes.
   */

  Pixmap newpixmap;
  BTree *result;
  extern Widget topLevel;

  if (tree == NULL) {
    int x, y;
    unsigned int width, height, bw, depth;
    Window root;

    /* Create Pixmap */
    result = (BTree*) XtMalloc (sizeof(BTree));
    result->icon = icon;
    result->left = NULL;
    result->right = NULL;

    if (!XGetGeometry(XtDisplay(topLevel), icon, &root, &x, &y,
		      &width, &height, &bw, &depth)) {
	width = 32;
	height = 32;
	depth = DefaultDepthOfScreen(XtScreen(topLevel));
    }
    
    newpixmap = XCreatePixmap(XtDisplay(topLevel),
			      RootWindowOfScreen(XtScreen(topLevel)), 
			      width, height, depth);
    XSetFunction(XtDisplay(topLevel), xdtmgc, GXcopy);
    XCopyArea(XtDisplay(topLevel), icon, newpixmap, xdtmgc,
	      0, 0, width, height, 0, 0);
    XFillRectangle(XtDisplay(topLevel), newpixmap, xdtmgc,
		   0, 0, width, height);
    
    result->grey = newpixmap; 
    *new = newpixmap;
  } else 
    if (tree->icon == icon) {
      /* Found it */
      *new = tree->grey;
      result = tree;
    } else
      if (tree->icon < icon) 
	/* Try left hand side */
	result = grey_search(icon, new, tree->left);
      else
	/* Try right hand side */
	result = grey_search(icon, new, tree->right);

  return(result);
}

/*****************************************************************************
 *                                do_iconprefs                               *
 *****************************************************************************/
private iconPrefs *do_iconprefs(ip, fullname, filename, filestatus)
iconPrefs *ip;
String fullname, filename;
struct stat *filestatus;
{
  /* traverse linked list trying to match regular expressions against 
   * the current filename 
   */
  iconPrefs *pref = NULL;
  String string;
  Boolean cmd_is_first = False;
  iconSelection *sel = NULL;

  if (ip != NULL) 
    if (ip->expbuf == NULL) {
      /* default value */
      pref = ip;
      cmd_is_first = ip->cmd_is_first;
      sel = ip->select;
    } else {
      /* try to match regular expression */
      if (ip->checkpath == True) 
	string = fullname;
      else
	string = filename;
      if (regexec(ip->expbuf, string) != 0) {
	if (ip->extra != NULL) {
	  pref = applyprefs((typePrefs*)ip->extra, fullname, filename,
			    filestatus);
	}
	if (pref == NULL) {
	  pref = ip;
	  cmd_is_first = ip->cmd_is_first;
	  sel = ip->select;
        }
      } else {
	pref = do_iconprefs(ip->next, fullname, filename, filestatus);
      }
    }

  if (pref) {
      if (pref->user_data == NULL) { /* No user_data, create an empty one */
	  pref->user_data = XtNew(iconOps);
	  pref->user_data->menu = NULL;
	  pref->user_data->cmd  = NULL;
      }
      
      /* If icon menu not already created, do it */
      if (sel && (pref->user_data->menu == NULL)) {
	  AppProgram **proglist;
	  Cardinal menusize = sel->number + 1, n = 0, i;

	  proglist = (AppProgram **)XtMalloc(menusize * sizeof(AppProgram *));

	  while (sel != NULL)
	  {
	      /* add a dummy node for selection separator */
	      proglist[n] = NULL;
	      n++;
	      
	      /* scan program list */
	      for (i = 0 ; i < sel->number; i++)
	      {
		  proglist[n] = sel->applist[i];
		  n++;
		  
	      }
	      sel = sel->next;
	      if (sel) {
		  menusize += (sel->number + 1);
		  proglist = (AppProgram **)XtRealloc((char *)proglist,
						      sizeof(AppProgram *)
						      * menusize);
	      }
	  }
	  if (n > 0)
	    pref->user_data->menu = (Widget)createIconMenu(proglist, n);
	  else pref->user_data->menu = NULL;

	  if ((n > 0) && cmd_is_first && (pref->user_data->cmd == NULL))
	    pref->user_data->cmd = proglist[1]; /* 0 is a separator */
      }
  }

  return(pref);
}

/*****************************************************************************
 *                             parsePreferences                              *
 *****************************************************************************/
public Boolean parsePreferences(w)
Widget w;
{
  /* locate config file then call parser on it */
  extern String home;

  String setupfile;
  String libsetupfile;

  initBitmaps(w);  /* Initialise default pixmaps */

  /* check to see it there is an .xdtmrc in the users home directory. 
   * If not use the system one.. or command line argument one.
   */
  
  setupfile = (String) XtMalloc ((strlen(home) + 10) * sizeof(char));
  libsetupfile = (String) XtMalloc ((strlen(app_data.systemlibdir) + 10)
				    * sizeof(char));

  user = geteuid();
  group = getegid();

  strcpy(setupfile, home);
  strcat(setupfile, "/.xdtmrc");
  strcpy(libsetupfile, app_data.systemlibdir);
  strcat(libsetupfile, "/xdtmrc");
  
  if ((app_data.cffile == 0 ||
       (((preferences_filename = app_data.cffile) == app_data.cffile) && /*try appdefaults*/
	((yyin = fopen(app_data.cffile, "r")) == NULL)))              &&
      (((preferences_filename = setupfile) == setupfile) &&  /*try home dir*/
       ((yyin = fopen(setupfile, "r")) == NULL))                      &&
      ((strcmp(app_data.systemlibdir, SYSTEM_LIB_DIR) == 0) ||
       (((preferences_filename = libsetupfile) == libsetupfile) &&  /*try system lib dir*/
	((yyin = fopen(libsetupfile, "r")) == NULL)))                 &&
      (((preferences_filename = XtNewString(SYSTEM_XDTMRC)) != NULL) &&  /*try system dir*/
       ((yyin = fopen(SYSTEM_XDTMRC, "r")) == NULL)))
    {
      fprintf(stderr,
	      "Error: No user or system xdtmrc found\n");
      fprintf(stderr,
	      "       either install an xdtmrc or specify one on the\n");
      fprintf(stderr,
	      "       command line using the -cf option\n");
      exit(2);
    } else {

      loadConfig();
    }

  XtFree((char *)setupfile);
  XtFree((char *)libsetupfile);

  return True;
}

/*****************************************************************************
 *                             loadConfig                              *
 *****************************************************************************/
public void loadConfig()
{
  /* Call the parser on the config file */

  extern AppSelection **appselections;
  extern Cardinal selectionindex;
  extern Widget selectionMenu;

  static MenuContents *selectionmenu;
  static Boolean firstTime = True;
  Cardinal i;

  /* first try to reopen the file if not called for 
     the first time */
  if (firstTime == False) {
    yyin = freopen(preferences_filename, "r", yyin);
  }

  if (yyin != NULL) {
    if (firstTime == False) {
      /* free existing structs */
      for(i = 0; i < selectionindex; i++) {
	XtDestroyWidget(XtNameToWidget(selectionMenu, 
				       selectionmenu[i].paneName));
      }

      XtFree((char *)selectionmenu);
      selectionmenu = (MenuContents *) NULL;

      for(i = 0; i < selectionindex; i++) 
	freeAppSelection(appselections[i]);
      appselections = (AppSelection **) NULL;

      freeTypePref(prefs);
      prefs = (typePrefs *) NULL;

    } else {
      firstTime = False;
    }

   /* parse the file */
    if (yyparse()) {
      fprintf(stderr, "Error parse failed. exiting\n");
      exit(1);
    }
    
    /* Create selection menu */
    selectionmenu = (MenuContents*) XtMalloc (sizeof(MenuContents) *
					      selectionindex);
    for(i = 0; i < selectionindex; i++) {
      selectionmenu[i].paneName   = appselections[i]->name;
      selectionmenu[i].paneLabel  = appselections[i]->name;
      selectionmenu[i].paneNumber = i;
      selectionmenu[i].set        = noflag;
    }
    createMenu(selectionMenu, selectionmenu, i, selectionChange);
  }
}

/*****************************************************************************
 *                               initBitmaps                                 *
 *****************************************************************************/
private void initBitmaps(w)
Widget w;
{
  /* Get default icon pixmaps + the stippled pixmap for greying icons */

  XGCValues values;
  Pixel fg, bg;
  
  fg = BlackPixelOfScreen(XtScreen(w));
  bg = WhitePixelOfScreen(XtScreen(w));
  
  foldericon  = XCreatePixmapFromBitmapData(	XtDisplay(w),
				      	RootWindowOfScreen(XtScreen(w)),
				      	folder_bits,
				     	IconBitmapWidth, IconBitmapHeight,
					fg, bg,
					DefaultDepthOfScreen(XtScreen(w)));
  fileicon    = XCreatePixmapFromBitmapData(  XtDisplay(w),
                                      	RootWindowOfScreen(XtScreen(w)),
				      	file_bits,
				      	IconBitmapWidth, IconBitmapHeight,
					fg, bg,
					DefaultDepthOfScreen(XtScreen(w)));
  grey_mask   = XCreateBitmapFromData(  XtDisplay(w),
                                      	RootWindowOfScreen(XtScreen(w)),
				      	Grey_bits,
				      	IconBitmapWidth, IconBitmapHeight);
  
  /* GC for greying pixmaps */
  if ((BlackPixelOfScreen(XtScreen(w))) == 1) {
    values.background = BlackPixelOfScreen(XtScreen(w));
    values.foreground = WhitePixelOfScreen(XtScreen(w));
  } else {
    values.background = WhitePixelOfScreen(XtScreen(w));
    values.foreground = BlackPixelOfScreen(XtScreen(w));
  }

  values.stipple = grey_mask;
  
  xdtmgc = XtGetGC(w, (unsigned) GCBackground | GCForeground | GCStipple,
		   &values);
  XSetFillStyle(XtDisplay(w), xdtmgc, FillStippled);
  
		 
  grey_tree = NULL;
}
