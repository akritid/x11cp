%{ /*-*- Mode: C -*-*/
/**************************************************************************
 ** Filename :       parser.y                                            **
 ** Author   :       Edward Groenendaal                                  **
 ** Date     :       31/1/91                                             **
 ** Purpose  :       Parse the xdtm config file, build the data          **
 **                  structures.                                         **
 ** Changes  :       18-04-92, Edward Groenendaal                        **
 **                  Added #if NeedFunctionPrototypes stuff              **
 **************************************************************************/
/* touched by Lionel Mallet - 10/2/92 */

#ifdef DEBUG_YACC
#include <X11/Intrinsic.h>
#endif

#define YYDEBUG     1	/* allow yacc debugging */
#include "xdtm.h"
#include "parse.h"
#include "Ext/appl.h"

#ifdef XPM
#include "xpm.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#ifdef HAS_STRSTR
#define mystrstr(cs, ct)	strstr(cs, ct)
#else
extern char *mystrstr(char *, char *);
#endif

#if defined(sco386) && defined(M_INTERNAT)   /* jcc */
#undef M_INTERNAT
#endif

/* structure used to hold loaded bitmaps (pixmaps), the pixmaps referenced
 * in the icon list are pointing into this structure.
 */
typedef struct _iconTree {
  String fullname;       /* index on this */
  Pixmap icon;
  Pixmap mask;
  struct _iconTree *left;
  struct _iconTree *right;
} iconTree;

/* The variable stack frame, this stack holds the stack frames for use in 
 * calculating the scope of local variables in the config language.
 */
typedef struct _varStack {
  String *defpath;		/* path in this stack */
  Cardinal dirinpath;		/* Number of dirs in path */
  Boolean checkpath;		/* Whether to match over the whole path */
  iconSelection *iconsel;       /* The level at which new iconprograms will
				 * be inserted.
				 */
  iconPrefs *iconpref;		/* The point into the main icon structure 
				 * at which this frame's results will be
				 * inserted. 
				 */
  typePrefs *current;		/* The level at which the new iconprefs
				 * will be inserted.
				 */
  struct _varStack *next;	/* Next link in stack */
} varStack;

/* Declarations of procedures defined after they have been referenced */
#if NeedFunctionPrototypes
public  typePrefs *newTypePref(void);
public  void       freeTypePref(typePrefs*);
private void       start_block(typePrefs*,iconPrefs*);
private void       end_block(void);
private String    *stringtopath(String, Cardinal*);
private int        lookupicon(String, Pixmap*, Pixmap*);
private iconPrefs *inserticonpref(iconPrefs*, iconPrefs*);
private String     strip_quotes(String);
private int        get_pixmap(String, iconTree**, Pixmap*, Pixmap*);
private void       set_path(String);
private void       set_icon(String, Boolean);
private void       set_deficon(String);
private void       set_cmd_is_first(void);
private void       newselection(String);
public  void       freeAppSelection(AppSelection*);
private void       setapp(int, String);
public  iconPrefs *newIconPref(void);
private void       freeIconPref(iconPrefs*);
private String     ExpandEnvVars(String);
private void       NewIconSelection(String);
private void       freeIconSelection(iconSelection*);
private void       NewIconProgram(String, String, int);
private void       freeAppProgram(AppProgram*);
private iconSelection *InsertIconSelection(iconSelection*, iconSelection*);
private iconSelection *InsertIconProgram(iconSelection*, AppProgram*);
private void       init_structs(void);
private void       free_structs(void);
private void       new_re(String re);
private void       new_type(int n);
private void       set_cmd(String s, int termopts);

/* imported variables/procedures */
extern int parseerror;            /* quantity of parse errors so far */
extern int yylineno;              /* line number being parsed */
extern void yyerror(char *); /* normal error printing routine */

#else
public  typePrefs *newTypePref();
public  void       freeTypePref();
private void       start_block();
private void       end_block();
private String    *stringtopath();
private int        lookupicon();
private iconPrefs *inserticonpref();
private String     strip_quotes();
private int        get_pixmap();
private void       set_path();
private void       set_icon();
private void       set_deficon();
private void       set_cmd_is_first();
private void       newselection();
public  void       freeAppSelection();
private void       setapp();
public  iconPrefs *newIconPref();
private void       freeIconPref();
private String     ExpandEnvVars();
private void       NewIconSelection();
private void       freeIconSelection();
private void       NewIconProgram();
private void       freeAppProgram();
private iconSelection *InsertIconSelection();
private iconSelection *InsertIconProgram();
private void       init_structs();
private void       free_structs();
private void       new_re();
private void       new_type();
private void       set_cmd();

/* imported variables/procedures */
extern int parseerror;            /* quantity of parse errors so far */
extern int yylineno;              /* line number being parsed */
extern void yyerror();            /* normal error printing routine */
#endif

#ifndef DEBUG_YACC
extern Widget topLevel;
#endif

/* Variable declarations */
public  AppSelection **appselections;
public  typePrefs     *prefs;          /* preferneces */
private varStack      *varframe;       /* stack frames */
private iconTree      *icontable;      /* table of pixmaps */
private AppSelection  *current_selection;
private Cardinal       listindex;      /* index into current appprogram list */
private Cardinal       currentlistmax; /* current list appprogram size */
public  Cardinal       selectionindex; /* index into appselections */

%} /* Start of YACC declarations */

%start xdtmrc	

%union                              /* valid types */
{
    int  number;                     /* number */
    char *string;                    /* string */
}

/* declare terminals with their types */

%token <string> STRING_T
%token <number> IF_T SET_T ICON_T NAME_T PATH_T DEFICON_T TYPE_T CHECKPATH_T
%token <number> CMD_T
%token <number> TRUE_T FALSE_T IGNORE_T
%token <number> DIR_T FILE_T READ_T WRITE_T EXE_T BLOCK_T CHARACTER_T SLINK_T
%token <number> SOCKET_T FIFO_T
%token <number> MSEL_T OSEL_T NSEL_T ASEL_T DEFINE_T PROG_T OPTIONS_T TERMOPT_T
%token <number> DEFAPPL_T TERM_T NOTERM_T FIRST_T
%token <number> ASSIGN_T EQUAL_T SEMIC_T COMMA_T COLON_T
%token <number> O_PAR_T C_PAR_T O_BRACE_T C_BRACE_T
%token <value>  EOFTOKEN ERRORTOKEN

/* types of non-terminals */
%type <string> identifier
%type <number> block type var option xdtmrc termopt

%%    /* Beginning of rule section */

xdtmrc       :   { init_structs(); } 
                 statements 
                 defines
                 { free_structs(); }
             ;
statements   :   /* Empty */
             |   statements statement 
             ;
statement    :   ifstatement
             |   setstatement SEMIC_T
             |   setcmdstmt
             |   defappl
             ;
defines      :   defines define
             |   define
             ;
ifstatement  :   IF_T O_PAR_T expression C_PAR_T block
             ;
setstatement :   SET_T var ASSIGN_T identifier { switch ($2) {
                                                 case PATH_T:
                                                   set_path($4);
						   break;
						 case ICON_T:
						   set_icon($4, False);
						   break;
						 case DEFICON_T:
						   set_deficon($4);
						   break;
						 }
					       }
             |   SET_T CHECKPATH_T ASSIGN_T TRUE_T 
                       { varframe->checkpath = True; }
             |   SET_T CHECKPATH_T ASSIGN_T FALSE_T 
                       { varframe->checkpath = False; }
             |   SET_T IGNORE_T
                       { set_icon("", True);}
             ;
setcmdstmt   :   SET_T CMD_T ASSIGN_T FIRST_T SEMIC_T
                       { set_cmd_is_first(); }
             |   SET_T CMD_T ASSIGN_T identifier COMMA_T termopt SEMIC_T
                       { set_cmd($4, $6);}
             |   SET_T CMD_T ASSIGN_T identifier SEMIC_T
                       { set_cmd($4, NOTERM);}
define       :   DEFINE_T identifier {newselection($2);} ASSIGN_T defineblock
             ;
defappl      :   DEFAPPL_T identifier {NewIconSelection($2);} ASSIGN_T 
                 applblock
             ;
block        :   O_BRACE_T
                 statements {end_block();} 
                 C_BRACE_T
             ;
defineblock  :   O_BRACE_T descriptions C_BRACE_T 
                 {current_selection->number = listindex;}
             ;
applblock    :   O_BRACE_T appldescs C_BRACE_T
             ;
descriptions :   /* Empty */
             |   descriptions description 
             ;
appldescs    :   /* Empty */
             |   appldescs appldesc
             ;
description  :   O_BRACE_T 

                 { /* Allocate a new AppProgram */
#ifndef DEBUG_YACC		   
		   current_selection->list[listindex] = 
		     (AppProgram*) XtMalloc (sizeof(AppProgram));
		   current_selection->list[listindex]->count = 0;
#endif
		 }

                 NAME_T ASSIGN_T identifier SEMIC_T {setapp($3, $5);}
		 ICON_T ASSIGN_T identifier SEMIC_T {setapp($8, $10);}
                 PROG_T ASSIGN_T identifier SEMIC_T {setapp($13, $15);}
                 OPTIONS_T ASSIGN_T option SEMIC_T  {setapp($18, (String)$20);}
                 optional_termopt
                 {
#ifndef DEBUG_YACC
                  /* Increment listindex, Thanks to Johan Widen for catching
		   * a bug here in version 1.0.
		   */
		   if (++listindex == currentlistmax) {
		     currentlistmax += APPPINC;
		     current_selection->list = 
		       (AppProgram**)XtRealloc((char *)current_selection->list,
						sizeof(AppProgram*) *
						currentlistmax);
		   }
#endif
		 }

                 C_BRACE_T
             ;
appldesc     :   O_BRACE_T identifier COLON_T identifier COMMA_T termopt
                    C_BRACE_T
                 {NewIconProgram($2, $4, $6);}
             |   O_BRACE_T identifier COLON_T identifier
                    C_BRACE_T
                 {NewIconProgram($2, $4, NOTERM);}
             ;
option       :   MSEL_T | OSEL_T | NSEL_T | ASEL_T
             ;
optional_termopt: /* nothing */ {setapp(TERMOPT_T, (String)NOTERM);}
             |   TERMOPT_T ASSIGN_T termopt SEMIC_T {setapp($1, (String)$3);}
             ;
termopt      :   TERM_T        { $$ = TERM; }
             |   NOTERM_T      { $$ = NOTERM; }
             ;
expression   :   NAME_T EQUAL_T identifier { new_re($3); }
             |   TYPE_T EQUAL_T type       { new_type($3); }
             ;
identifier   :   STRING_T      {  $$=strip_quotes($1);
			       }
             ;
type         :   DIR_T   | READ_T      | WRITE_T | EXE_T   | FILE_T   |
                 BLOCK_T | CHARACTER_T | FIFO_T  | SLINK_T | SOCKET_T
             ;
var          :   PATH_T | ICON_T | DEFICON_T
             ;
%%  

/*****************************************************************************
 *                               init_structs                                *
 *****************************************************************************/
private void init_structs()
{
  /* Initialise structures.. wow what a suprise :-) */

  Cardinal n;
  
#ifdef DEBUG_YACC
    XtToolkitInitialize();
#endif
  /* Icon prefs = an empty type pref */
  prefs = newTypePref();
  
  /* Icon table is empty, no icons loaded yet */
  icontable = NULL;

  /* Default top level stack frame */
  varframe = (varStack*) XtMalloc (sizeof(varStack));
  varframe->defpath = stringtopath("/usr/lib/X11/bitmaps", &n);
  varframe->dirinpath = n;
  varframe->current = prefs;
  varframe->checkpath = False;
  varframe->iconpref = NULL;
  varframe->next = NULL;

  /* initialise the Application Selection Lists Array */
  listindex = 0;	/* First entry in the new selection list */
  selectionindex = -1;  /* First selection list is 0 in the array */
  appselections = (AppSelection**) XtMalloc (sizeof(AppSelection*) * APPPSIZE);
}

/*****************************************************************************
 *                              free_structs                                 *
 *****************************************************************************/
private void free_structs()
{
  /* This procedure is called just before the parsing finishes, it free's
   * the memory allocated to the internal structures used by the parser.
   * Note: Doesn't get all of them.. but let's not get petty, what's a few
   *       bytes once only in a program of this size?
   */
  
  if (!parseerror) {
    /* Free Varstack (Should Be Only One Level Left) */
    XtFree((char *)varframe);

    selectionindex++; /* So that we know the correct number of selections */
  }
}

/*****************************************************************************
 *                                newTypePref                                *
 *****************************************************************************/
public typePrefs *newTypePref()
{
  /* Allocate the memory for a new empty typePref, I suppose this 
   * procedure is not really needed on most machines, but what if
   * a machine doesn't automatically fill all entries in a structure
   * with NULL's ?
   */

  typePrefs *tp;

  if (!parseerror) {
    tp = (typePrefs*) XtMalloc (sizeof(typePrefs));
    tp->iconprefs = NULL;
    tp->dir = NULL;
    tp->file = NULL;
    tp->block = NULL;
    tp->character = NULL;
    tp->fifo = NULL;
    tp->slink = NULL;
    tp->socket = NULL;
    tp->exe = NULL;
    tp->read = NULL;
    tp->write = NULL;
    
    return tp;
  }
  return (typePrefs *)NULL;
}

/*****************************************************************************
 *                                freeTypePref                                *
 *****************************************************************************/
public void freeTypePref(tp)
typePrefs *tp;
{
  /* Recursively free the memory for a typePref
   */
  if (tp == NULL) return;

  freeIconPref(tp->iconprefs);
  freeTypePref(tp->dir);
  freeTypePref(tp->file);
  freeTypePref(tp->block);
  freeTypePref(tp->character);
  freeTypePref(tp->fifo);
  freeTypePref(tp->slink);
  freeTypePref(tp->socket);
  freeTypePref(tp->exe);
  freeTypePref(tp->read);
  freeTypePref(tp->write);

  XtFree((char *)tp);
}

/*****************************************************************************
 *                                newIconPref                                *
 *****************************************************************************/
public iconPrefs *newIconPref()
{
  /* Allocate the memory for a new empty iconPref, I suppose this 
   * procedure is not really needed on most machines, but what if
   * a machine doesn't automatically fill all entries in a structure
   * with NULL's ?
   */

  iconPrefs *ip;

  ip = (iconPrefs*) XtMalloc (sizeof(iconPrefs));
  ip->expbuf = NULL;
  ip->checkpath = False;
  ip->extra = NULL;
  ip->icon = None;
  ip->mask = None;
  ip->user_data = NULL;
  ip->cmd_is_first = False;
  ip->select = NULL;
  ip->next = NULL;
  
  return(ip);
}

/*****************************************************************************
 *                                freeIconPref                                *
 *****************************************************************************/
private void freeIconPref(ip)
iconPrefs *ip;
{
  /* Recursively free the memory for an iconPref
   */
  if (ip == NULL) return;

  freeIconPref(ip->next);
  if (ip->expbuf != NULL) XtFree((char *)ip->expbuf);;
  freeTypePref((typePrefs *)ip->extra);
  if (ip->user_data != NULL) {
    if (ip->cmd_is_first == False) freeAppProgram(ip->user_data->cmd);
    if (ip->user_data->menu != NULL)
      XtDestroyWidget(ip->user_data->menu);
    XtFree((char *)ip->user_data);
  }
  freeIconSelection(ip->select);

  XtFree((char *)ip);
}

/*****************************************************************************
 *                                start_block                                *
 *****************************************************************************/
private void start_block(newpref, iconpref)
typePrefs *newpref;
iconPrefs *iconpref;
{
  /* A new block has been entered, create a new stack frame, inherit most
   * stuff from the previous stack frame.
   */

  /* Push new stack frame */
  varStack *newframe;
  
  if (!parseerror) {
    newframe = (varStack*) XtMalloc (sizeof(varStack));
    newframe->defpath = varframe->defpath;
    newframe->dirinpath = varframe->dirinpath;
    newframe->current = newpref;
    newframe->checkpath = varframe->checkpath;
    newframe->iconpref = iconpref;
    newframe->next = varframe;
    varframe = newframe;
  }
}

/*****************************************************************************
 *                                    end_block                              *
 *****************************************************************************/
private void end_block()
{
  /* End the current stack frame, check to see if the extra part of
   * the iconPref has been used, if not free it.
   * Pop the current stack from off the stack.
   */

  if (!parseerror) {

    iconPrefs *tmpIconPref;
    typePrefs *tmp = varframe->current;
    varStack *tmpframe = varframe->next;

    if (tmp->iconprefs == NULL && tmp->dir == NULL &&
	tmp->file == NULL      && tmp->exe == NULL &&
	tmp->read == NULL      && tmp->write == NULL) {
      XtFree((char *)tmp);
      tmp = NULL;
      tmpIconPref = varframe->iconpref;
      tmpIconPref->extra = NULL;
    }
    XtFree((char *)varframe);
    varframe = tmpframe;
  }
}

/*****************************************************************************
 *                                     new_re                                *
 *****************************************************************************/
private void new_re(re)
String re;
{
  /* Create a new iconpref, put the compiled regular expression in it then
   * insert it into the current iconprefs.
   *
   * - Takes a regular expression in the original string format. eg. "\.c$"
   */

  if (!parseerror) {
    iconPrefs *new, *tmp = varframe->current->iconprefs;

    new = newIconPref();
    new->extra = (XtPointer) newTypePref();
    new->expbuf = regcomp(re);
    new->checkpath = varframe->checkpath;
    varframe->current->iconprefs = inserticonpref(tmp, new);
    start_block((typePrefs*)new->extra, (iconPrefs*)new);
  }
}

/*****************************************************************************
 *                               new_type                                    *
 *****************************************************************************/
private void new_type(n)
int n;
{
  /* Allocate a typePref on dir, write etc. if neadbe, otherwise ignore 
   *
   * - Takes a number representing which file feature to insert the typePref
   *   on.
   */

  if (!parseerror) {
    typePrefs *typeprefs = varframe->current;
    typePrefs *newpref = NULL;

    switch(n) {
    case DIR_T:
      if (typeprefs->dir == NULL) 
	typeprefs->dir = newTypePref();
      newpref = typeprefs->dir;
      break;
    case FILE_T:
      if (typeprefs->file == NULL) 
	typeprefs->file = newTypePref();
      newpref = typeprefs->file;
      break;
    case BLOCK_T:
      if (typeprefs->block == NULL) 
	typeprefs->block = newTypePref();
      newpref = typeprefs->block;
      break;
    case CHARACTER_T:
      if (typeprefs->character == NULL) 
	typeprefs->character = newTypePref();
      newpref = typeprefs->character;
      break;
    case FIFO_T:
      if (typeprefs->fifo == NULL) 
	typeprefs->fifo = newTypePref();
      newpref = typeprefs->fifo;
      break;
    case SLINK_T:
      if (typeprefs->slink == NULL) 
	typeprefs->slink = newTypePref();
      newpref = typeprefs->slink;
      break;
    case SOCKET_T:
      if (typeprefs->socket == NULL) 
	typeprefs->socket = newTypePref();
      newpref = typeprefs->socket;
      break;
    case EXE_T:
      if (typeprefs->exe == NULL) 
	typeprefs->exe = newTypePref();
      newpref = typeprefs->exe;
      break;
    case READ_T:
      if (typeprefs->read == NULL) 
	typeprefs->read = newTypePref();
      newpref = typeprefs->read;
      break;
    case WRITE_T:
      if (typeprefs->write == NULL) 
	typeprefs->write = newTypePref();
      newpref = typeprefs->write;
      break;
    }
    start_block(newpref, NULL);
  }
}

/*****************************************************************************
 *                                   count_chr                               *
 *****************************************************************************/
public Cardinal count_chr(s, c)
String s;
char c;
{
  /* Given a String and a character this function counts the number of
   * occurences of that character in the string and returns the result.
   *
   * - Takes a string s within which to check for character c.
   */

  Cardinal count=0;

  while(*s != '\0') {
    if (*s == c) count++;
    s++;
  }
  return count;
}

/*****************************************************************************
 *                                  stringtopath                             *
 *****************************************************************************/
private String *stringtopath(s, n)
String s;
Cardinal *n;
{
  /* Split a path into directories, put these into a linked list
   * only directories starting with / or ~/ are allowed.
   *
   * - Takes a string containing the path delimited by colons, and 
   *   a pointer within which the number of directories in the
   *   path is left.
   * + Returns an array of directories.
   */
  String *result;
  String p, tmp;
#ifdef DEBUG_YACC
  String home;
#else
  extern String home;
#endif

  *n = 0;

#ifdef DEBUG_YACC
    if ((home = (String) getenv("HOME")) == NULL) {
    fprintf(stderr, "can\'t get environment variable HOME\n");
    home = XtNewString("/");
  } else 
    home = XtNewString(home);
#endif

  result = (String*) XtMalloc ((count_chr(s, ':')+3) * sizeof(String));
  s = XtNewString(s);

  /* extract the directories from path 's' */

  p = strtok(s, ":");
  while ( p != NULL) {
    if (*p == '/')
      result[(*n)++] = p;
    else 
      if (*p == '~' && *(p+1) == '/') {
	tmp = (String) XtMalloc ((strlen(home) + strlen(p) + 2) * sizeof(char));
	strcpy(tmp, home);
	strcat(tmp, p+1);
	result[(*n)++] = tmp;
      } else
	fprintf(stderr, "Warning: Directory \'%s\' not fully qualified, ignoring.\n%s",
		p, "         Must start with either / or ~/ .\n");
    p = strtok(NULL, ":");
  }
#ifdef DEBUG_YACC
  XtFree((char *)home);
#endif
  return(result);
}

/*****************************************************************************
 *                             inserticonpref                                *
 *****************************************************************************/
private iconPrefs *inserticonpref(head, item)
iconPrefs *head, *item;
{
  /* Insert item into list starting with head, insert into the last 
   * position before the default.
   *
   * - Takes the head of the iconpref list and the iconpref list entry to 
   *   be inserted.
   * + Returns the new iconpref list with the item inserted.
   */

  iconPrefs *sec = NULL, *tmp = head;

  if (head == NULL) {
    /* first element */
    head=item;
    head->next=NULL;
  } else {
    while (tmp->next != NULL) {
      sec = tmp;
      tmp = tmp->next;
    }
    if (tmp->expbuf == NULL) {
      /* Already got a default */
      if (item->expbuf != NULL) {
	/* Item is not a new default */
	item->next = tmp;
	if (sec != NULL)
	  sec->next = item;
	else 
	  head = item;
      } else { /* item->expbuf == NULL */
	  if ((tmp->icon == DUMMY) && (item->icon != DUMMY))
	    /* copy item icon into default iconPrefs */
	    tmp->icon = item->icon;
	  if (item->select != NULL) {
	      /* copy item iconSelection list into default iconPrefs */
	      iconSelection *head = tmp->select, *itemtmp = item->select ;
	      while (itemtmp != NULL) {
		  tmp->select = InsertIconSelection(head, itemtmp);
		  itemtmp = itemtmp->next;
	      }
	  }
	  if (!tmp->cmd_is_first && item->cmd_is_first)
	    tmp->cmd_is_first = True;
	  XtFree((char *)item);
      }
    } else { /* tmp->expbuf != NULL */
      /* No default */
      tmp->next = item;
      item->next = NULL;
    }
  }
  return(head);
}

/*****************************************************************************
 *                              strip_quotes                                 *
 *****************************************************************************/
private String strip_quotes(s)
String s;
{
  /* This function is for stripping the first and last characters from a 
   * string. It is used for stripping the quotes off strings.
   *
   * - Takes a String with quotes.
   * - Returns the String with quotes missing.
   */
  String new;
  
  *(s + (strlen(s)-1)) = '\0';
  s++;
  new = ExpandEnvVars(s);

  /* s is NOT free'd because it was supplied from the lexical analyzer,
   * it's up to that to free it.
   */
  return(new);
}

/*****************************************************************************
 *                                 set_path                                  *
 *****************************************************************************/
private void set_path(s)
String s;
{
  /* Given a string containing a list of directories delimited by colons
   * this procedure inserts these directories in teh form of an array of
   * strings into the current stack frame.
   */
  Cardinal n;

  varframe->defpath = stringtopath(s, &n);
  varframe->dirinpath = n;
}

/*****************************************************************************
 *                                set_icon                                   *
 *****************************************************************************/
#if NeedFunctionPrototypes
private void set_icon(String s, Boolean ignore)
#else
private void set_icon(s, ignore)
String s;
Boolean ignore;
#endif
{
  /* varframe->iconpref points to the iconpref into which this
   * icon should be inserted. If it is NULL, then create a new
   * one at varframe->current->iconprefs.
   *
   * - Takes an icon filename and/or a flag saying whether the current
   *   iconpref should be ignored.
   */

  typePrefs *frame = varframe->current;
  iconPrefs *iconpref = varframe->iconpref, 
            *head = frame->iconprefs,
            *new =  NULL;
  int status;

  if (iconpref == NULL) {
    new = newIconPref();
    if (ignore==True) {
      new->icon = DUMMY;
    } else {
      status = lookupicon(s, &(new->icon), &(new->mask));
    }
    frame->iconprefs = inserticonpref(head, new);
  } else
    if (ignore==True) {
      iconpref->icon = DUMMY;
    } else {
      status = lookupicon(s, &(iconpref->icon), &(iconpref->mask));
    }
}

/*****************************************************************************
 *                                set_deficon                                *
 *****************************************************************************/
private void set_deficon(s)
String s;
{
  /* Set the default icon in the current iconprefs list.
   *
   * - Takes the filename of the icon to be used.
   */

  typePrefs *frame = varframe->current;
  iconPrefs *iconpref, *head = frame->iconprefs;
  int status;

  iconpref = newIconPref();
  status = lookupicon(s, &(iconpref->icon), &(iconpref->mask));
  frame->iconprefs = inserticonpref(head, iconpref);

}

/*****************************************************************************
 *                                set_cmd_is_first                                   *
 *****************************************************************************/
private void set_cmd_is_first()
{
  /* varframe->iconpref points to the iconpref into which cmd_is_first
   * should be set to True. If it is NULL, then create a new
   * one at varframe->current->iconprefs.
   *
   */

  typePrefs *frame = varframe->current;
  iconPrefs *iconpref = varframe->iconpref, 
            *head = frame->iconprefs,
            *new =  NULL;

  if (iconpref == NULL) {
    new = newIconPref();
    new->cmd_is_first = True;
    frame->iconprefs = inserticonpref(head, new);
  } else 
    iconpref->cmd_is_first = True;
}

/*****************************************************************************
 *                                set_cmd                                    *
 *****************************************************************************/

private void set_cmd(s, termopts)
String s;
int termopts;
{
  /* varframe->iconpref points to the iconpref into which this
   * cmd should be inserted. If it is NULL, then create a new
   * one at varframe->current->iconprefs.
   */

  typePrefs *frame = varframe->current;
  iconPrefs *iconpref = varframe->iconpref, 
            *head = frame->iconprefs,
            *new =  NULL;
  AppProgram *app = (AppProgram *) XtMalloc(sizeof(AppProgram));
#ifdef DEBUG_YACC
    printf("DEBUG:        set_cmd %s\n", s);
#endif
    app->string = XtNewString("default");
    app->program = XtNewString(s);
    app->options = O_SEL;
    app->icon = (Pixmap)1; /* to avoid a bug in decrement_counter which uses
			      the icon field to know if node is dummy or not */
    app->termopts = termopts;
    app->count = 0;
  
  if (iconpref == NULL) {
    /* A new iconpref.. insert the command, leave the icon to be defined later
     * if it is not defined the icon will NOT be displayed.
     */
    new = newIconPref();
    new->cmd_is_first = True;
    
    frame->iconprefs = inserticonpref(head, new);
    NewIconSelection("default");
    iconpref = new;
  } else {
      iconpref->cmd_is_first = True;
      if (!iconpref->select) NewIconSelection("default");
  }
  iconpref->select = InsertIconProgram(iconpref->select, app);
}

/*****************************************************************************
 *                             lookupicon                                    *
 *****************************************************************************/
private int lookupicon(icon, pixmap, mask)
String icon;
Pixmap *pixmap, *mask;
{
  /* Return the pixmap associated with the name 'icon' if none 
   * found NULL is returned.
   */

  int status = 0;
  String *path = varframe->defpath;
  Cardinal pathsize = varframe->dirinpath;
  Cardinal n;
  String fullname = NULL;
  String tmp;
  FILE *result;

  /* Append 'icon' to each filename in current path, and check to see 
   * whether that file exists.
   */
  
  n = 0;
  do {
    tmp = (String) XtMalloc ((strlen(icon) + strlen(path[n]) + 4) * sizeof(char));
    tmp = strcpy(tmp, path[n]);
    tmp = strcat(tmp, "/");
    tmp = strcat(tmp, icon);
    if ((result = fopen(tmp, "r")) != NULL) {
      fullname = XtNewString(tmp);
      fclose(result);
      if ((status = get_pixmap(fullname, &icontable, pixmap, mask)))
      {
	  /* unable to load the pixmap, continue search in other dirs */
	  XtFree((char *)fullname);
	  fullname = NULL;
      }
    } 
    XtFree((char *)tmp);
  } while ((++n < pathsize) && (fullname == NULL));

  if (fullname == NULL) 
    fprintf(stderr,
	    "Warning: could not find/load icon '%s', default icon will be used instead.\n",
	    icon);

  return status;
}

/*****************************************************************************
 *                               get_pixmap                                  *
 *****************************************************************************/
private int get_pixmap(fullname, tree, pixmap, mask)
     String fullname;
     iconTree **tree;
     Pixmap *pixmap, *mask;
{
  /* If the item is found in the tree, then item's contents are filled 
   * with that items, otherwise the Pixmap is created and the item is 
   * inserted into the tree.
   */

  int cmp, value = 0;
  int width, height;
  int x_hot, y_hot;
#if NeedFunctionPrototypes
    extern int XdtmLocateIconFile(Screen *,char *,Pixmap *, Pixmap *,
				  int *, int *, int *, int *);
#else
    extern int XdtmLocateIconFile();
#endif
  Screen  *scr = XtScreen(topLevel);

  if (*tree == NULL) {
    /* At a leaf, insert item */
      iconTree *item = (iconTree*) XtMalloc (sizeof(iconTree));
      item->fullname = fullname;
      item->icon = (Pixmap)NULL;
      item->mask = (Pixmap)NULL;
#ifndef DEBUG_YACC
      value =
	XdtmLocateIconFile(
			   scr,
			   item->fullname,
			   &(item->icon),
			   &(item->mask),
			   &width,
			   &height,
			   &x_hot,
			   &y_hot);
#else
      item->icon = (Pixmap) DUMMY;
      item->mask = (Pixmap) DUMMY;
      printf("DEBUG: Loaded bitmap for %s\n", item->fullname);
#endif
      item->left = NULL;
      item->right = NULL;
      *tree = item;
      *pixmap = item->icon;
      *mask = item->mask;
  } else {
      if ((cmp = strcmp((*tree)->fullname, fullname)) == 0) {
	  /* Found it !!! */
	  *pixmap = (*tree)->icon;
	  *mask = (*tree)->mask;
	  XtFree((char *)fullname);
      } else 
	if (cmp < 0)
	  value = get_pixmap(fullname, &((*tree)->left), pixmap, mask);
	else 
	  value = get_pixmap(fullname, &((*tree)->right), pixmap, mask);
  }
  return value;
}

/*****************************************************************************
 *                              newselection                                 *
 *****************************************************************************/
private void newselection(selection)
String selection;
{
  /* increment selectionindex, put selection string into next 
   * appselection, increase size if appselections array if needed.
   * 
   * - Takes the name of the new selection
   */

  static selectionmax = APPPSIZE;
  
  if (++selectionindex == selectionmax) {
    selectionmax += APPPINC;
    appselections = (AppSelection **) XtRealloc ((char *)appselections, 
						 sizeof(AppSelection*) *
						 selectionmax);
  }

#ifdef DEBUG_YACC
  printf("DEBUG: New selection %s\n", selection);
#endif
  
  listindex = 0;
  current_selection = appselections[selectionindex] =
    (AppSelection*) XtMalloc (sizeof(AppSelection));
  current_selection->name = XtNewString(selection);
  current_selection->number = listindex;
  current_selection->list = (AppProgram**) XtMalloc (sizeof(AppProgram*) * 
						     APPPSIZE);
  currentlistmax = APPPSIZE;
}

/*****************************************************************************
 *                              freeAppSelection                                 *
 *****************************************************************************/
public void freeAppSelection(selection)
AppSelection *selection;
{
  if (selection == NULL) return;

  if (selection->name != NULL) XtFree(selection->name);
  if (selection->list != NULL) {
    Cardinal i;

    for (i = 0; i < selection->number; i++) 
      freeAppProgram(selection->list[i]);

    XtFree((char *)selection->list);
  }

  XtFree((char *)selection);
}

/*****************************************************************************
 *                               setapp                                      *
 *****************************************************************************/
private void setapp(type, argument)
int type;
String argument;
{
  /* Set part of the current program in the current selection list,
   * the part to be set is contained in type, the value it is to be set
   * to contained within argument.
   */

  AppProgram **proglist = current_selection->list;
  AppProgram *node = proglist[listindex];
  int status;
  
  switch(type) {
  case NAME_T:
    node->string = XtNewString(argument);
#ifdef DEBUG_YACC
    printf("String: %s\n", argument);
#endif
    break;
  case ICON_T:
    status = lookupicon(argument, &(node->icon), &(node->mask));
    break;
  case PROG_T:
    node->program = XtNewString(argument);
#ifdef DEBUG_YACC
    printf("Program: %s\n", argument);
#endif
    break;
  case OPTIONS_T:
    switch((int)argument) {
    case MSEL_T:
      node->options = M_SEL;
#ifdef DEBUG_YACC
      printf("MSEL\n");
#endif
      break;
    case OSEL_T:
      node->options = O_SEL;
#ifdef DEBUG_YACC
      printf("OSEL\n");
#endif      
      break;
    case NSEL_T:
      node->options = N_SEL;
#ifdef DEBUG_YACC
      printf("NSEL\n");
#endif
      break;
    case ASEL_T:
      node->options = A_SEL;
#ifdef DEBUG_YACC
      printf("ASEL\n");
#endif
      break;
    default:
      fprintf(stderr, "Programmer Error: Bad argument to setapp.\n");
      break;
    }
    break;
  case TERMOPT_T:
    node->termopts = (int)argument;
    break;
  default:
    fprintf(stderr, "Programmer Error: Bad argument to setapp.\n");
    break;
  }
}

/*****************************************************************************
 *                            InsertIconProgram                              *
 *****************************************************************************/
#if NeedFunctionPrototypes
private iconSelection *InsertIconProgram(iconSelection *sel, AppProgram *item)
#else
private iconSelection *InsertIconProgram(sel, item)
     iconSelection *sel;
     AppProgram *item;
#endif
{
    /* Insert item into list starting with sel->applist, insert into the first
     * position.
     *
     * - Takes the iconSelection and the iconProgram to be inserted.
     * + Return the new iconSelection with the item inserted in list.
     */

    AppProgram **tmplist = (AppProgram **)XtCalloc(sel->number + 1,
						   sizeof(AppProgram *));
/*    bcopy(sel->applist, &tmplist[1], sel->number * sizeof(AppProgram *));*/
    memcpy(&tmplist[1], sel->applist, sel->number * sizeof(AppProgram *));
    tmplist[0] = item;

    XtFree((char *)sel->applist);
    sel->applist = tmplist;
    sel->number += 1;

    return sel;
}

/*****************************************************************************
 *                            InsertIconSelection                            *
 *****************************************************************************/
#if NeedFunctionPrototypes
private iconSelection *InsertIconSelection(iconSelection *head,
					   iconSelection *item)
#else
private iconSelection *InsertIconSelection(head, item)
     iconSelection *head;
     iconSelection *item;
#endif
{
    /* Insert item ito list starting with head, insert into the last
     * position.
     *
     * - Takes the head of the iconSelection of the current iconPref and
     *   the iconSelection to be inserted.
     * + Return the new iconSelection list with the item inserted.
     */

    iconSelection *tmp = head;

    if (head == NULL) {
	/* first element */
	head = item;
	head->next = NULL;
    } else {
	while (tmp->next != NULL) tmp = tmp->next;
	tmp->next = item;
	item->next = NULL;
    }
    return(head);
}

/*****************************************************************************
 *                            ExpandEnvVars                                  *
 *****************************************************************************/
#if NeedFunctionPrototypes
private String ExpandEnvVars(String program)
#else
private String ExpandEnvVars(program)
     String program;
#endif
{
  /* Given the program with it's previous arguments, this procedure
   * will replace all ${foo} sequence by the value of the foo environment
   * variable or nothing if it doesn't exist.
   *
   * - Takes the program string.
   * + Returns the resultant command line.
   */
#define CHAR sizeof(char)

    Cardinal i = 0, j = 0;
    String var, value, tmpl = program, ptr, pos, posvar;
    String terminator = "}", mark = "${";
    String cmd = XtCalloc(1, sizeof(char));

    /* find next mark */
    while ((pos = mystrstr(tmpl, mark))) {
	/* allocate enough space */
	cmd = XtRealloc(cmd, (pos - tmpl) + CHAR * (i + 1));
	/* copy up to mark */
	for (ptr = tmpl; ptr < pos; ptr++) cmd[i++] = *ptr;
	cmd[i] = 0;
	pos += 2;
	
	/* search for env var name */
	j = 0;
	posvar = pos;
	pos = mystrstr(posvar, terminator);
	var = XtMalloc((pos - posvar) + CHAR);
	for (ptr = posvar; ptr < pos; ptr++) var[j++] = *ptr;
	var[j] = 0;
	
	if ((value = (String)getenv(var)) != NULL) {
	    cmd = XtRealloc(cmd,
			    CHAR * (strlen(cmd) + strlen(value) + 1));
	    strcat(cmd, value);
	} else {
	    fprintf(stderr,
		    "Xdtm: can\'t get environnement variable %s\n", var);
	}
	XtFree(var);
	var = NULL;
	
	i = strlen(cmd);
	pos++;
	tmpl = pos;
    }

    /* copy remaining template string */
    if (strlen(tmpl)) {
	cmd = XtRealloc(cmd, CHAR * (strlen(cmd) + strlen(tmpl) + 1));
	strcat(cmd, tmpl);
    }
    
    return cmd;
}


/*****************************************************************************
 *                            NewIconProgram                                 *
 *****************************************************************************/
#if NeedFunctionPrototypes
private void NewIconProgram(String label, String program, int options)
#else
private void NewIconProgram(label, program, options)
     String label;
     String program;
     int options;
#endif
{
    /* Create a new IconProgram struct filled with label and program,
     * insert it in the IconProgram list of the current IconSelection.
     *
     * - Takes the label and program to put in the struct.
     */

    iconSelection *iconsel = varframe->iconsel;
    AppProgram *new = (AppProgram *) XtMalloc(sizeof(AppProgram));

#ifdef DEBUG_YACC
    printf("DEBUG:        NewIconProgram %s %s\n", label, program);
#endif
    new->string = XtNewString(label);
    new->program = XtNewString(program);
    new->options = O_SEL;
    new->icon = DUMMY; /* to avoid a bug in decrement_counter which uses
			      the icon field to know if node is dummy or not */
    new->termopts = options;
    new->count = 0;

    if (iconsel->number > 0) {
	iconsel->number += 1;
	iconsel->applist = (AppProgram **)XtRealloc((char *)iconsel->applist,
						    iconsel-> number *
						    sizeof(AppProgram *));
    } else {
	iconsel->number = 1;
	iconsel->applist = (AppProgram **)XtMalloc(sizeof(AppProgram *));
    }
    
    iconsel->applist[iconsel->number - 1] = new;
}

/*****************************************************************************
 *                            freeAppProgram                                 *
 *****************************************************************************/
private void freeAppProgram(app)
AppProgram *app;
{
  if (app == NULL) return;

  if (app->string != NULL) XtFree(app->string);
  if (app->program != NULL) XtFree(app->program);

  XtFree((char *)app);
}

/*****************************************************************************
 *                            NewIconSelection                               *
 *****************************************************************************/
#if NeedFunctionPrototypes
private void NewIconSelection(String name)
#else
private void NewIconSelection(name)
     String name;
#endif
{
    /* Create a new IconSelection struct filled with name, insert it 
     * in the IconSelection list of the current iconPref.
     *
     * - Takes the name of the IconSelection to put in the struct.
     */

    typePrefs *frame = varframe->current;
    iconPrefs *iconpref = varframe->iconpref,
              *headicon = frame->iconprefs;
    iconSelection *new = (iconSelection *) XtMalloc(sizeof(iconSelection)),
                  *head;
#ifdef DEBUG_YACC
    printf("DEBUG: NewIconSelection %s\n", name);
#endif
    new->name = XtNewString(name);
    new->applist = NULL;
    new->number = 0;
    new->next = NULL;
    
    if (iconpref == NULL) {
	iconpref = newIconPref();
	iconpref->select = new;
	frame->iconprefs = inserticonpref(headicon, iconpref);
    }
    else {
	head = iconpref->select;
	iconpref->select = InsertIconSelection(head, new);
    }
    varframe->iconsel = new;
}

/*****************************************************************************
 *                            freeIconSelection                                 *
 *****************************************************************************/
private void freeIconSelection(selection)
iconSelection *selection;
{

  if (selection == NULL) return;

  if (selection->name != NULL) XtFree(selection->name);
  if (selection->applist != NULL) {
    Cardinal i;
    
    for (i=0; i < selection->number; i++) 
      freeAppProgram(selection->applist[i]);

    XtFree((char *)selection->applist);
  }
  
  freeIconSelection(selection->next);

  XtFree((char *)selection);
}

#ifdef DEBUG_YACC

/*****************************************************************************
 *                                 main                                      *
 *****************************************************************************/
main()
{
  /* main for use when the real main is not linked, because we're in debug mode.
   */

  yyparse();   /* Parse the stdin */
  return(0);   /* Hey, it probably worked */
}
#endif
