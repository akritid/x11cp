/* This file contains the current patch level. Not all patch levels are
 * released, I also use this number to keep track of my changes to xdtm,
 * so don't be suprised by jumps in this number.
 * Any new patches will say which patchlevel the patch is intended for, 
 * and should be applied only to programs of that patchlevel.
 * PATCH 1 applied - fixed bug with PWD which stopped emacs finding
 *                   out the CWD.
 * PATCH 2 applied - fixed resizing of display windows, and loads of little
 *                   problems with type's etc which X11R5 and later versions 
 *                   of gcc were fussy about.
 * PATCH 3 applied - Added all the FunctionPrototype stuff so that xdtm will
 *                   compile with K&R compilers.
 * PATCH 401 applied - Added Lionel Mallet functionalities.
 * PATCH 402 applied - fixed bug on closing both sides of pty on Ibm/RS6000.
 * PATCH 403 applied - fixed bugs in Move, Copy and setCursor policy. Better
 *                     support for Ibm Rs6000 pty handling .
 * PATCH 404 applied - fixed many bugs with memory management. Fixed a bug
 *                     with Short mode and icon menu. Fixed a bug with cursor
 *                     when doubleclick starts a default application.
 * PATCH 405 applied - fixed bug with ^Z killing the application after opening
 *                     a pseudo-terminal. Fixed many other memory bugs. Fixed
 *                     a bug with just hitting CR or clicking Input in
 *                     pseudo-terminal window.
 * PATCH 406 applied - fixed bugs with Move/Copy/Trash and Duplicate File. 
 *                     Fixed XtVa* calls to add NULL as last arg when missing.
 * PATCH 407 applied - fixed bug with Duplicate File incorrectly copying
 *                     symbolic links. Add support for HP in Imakefile.
 *                     Fixed bug with Move when moving a lot of files.
 * PATCH 408 applied - Fixed bug with frozen help windows for Map and Select.
 * PATCH 409 applied - Fixed bug with Select All when . or .. are ignored.
 *                     Fixed bug with HP wait() arg. Fixed bug with changing 
 *                     directory from "Goto" line. Fixed bug with / appended 
 *                     directory in "Goto" line when updating file information
 *                     in Info dialog. Better portability. Pseudo-terminal 
 *                     window more independent. New resources bellOnExit and
 *                     bellOnWarn to control bell.
 *                     Fixed bug with Cop/y/Move remaining sensitive after
 *                     Move/Copy is cancelled (no file selected).
 * PATCH 410 applied - Fixed bug with icon menu grabbing the server incorrectly
 *                     Fixed bug introduced in previous release causing 
 *                     Trash/Copy/Move to fail in Long mode.
 *                     Fixed bug with HP defines.
 *                     Fixed bug with map and icon program being empty.
 *                     Add handlers for WM_DELETE_WINDOW. Portability on Dec.
 *                     Fixed bug with help files not found causing exit.
 * PATCH 411 applied - Fixed bug with process list and translation table.
 *                     Fixed problem with access() include file needed.
 *                     Fixed bug with ASEL mode.
 *                     Resources added for Long listing options, dir first and
 *                     use of `.' icon.
 *                     Portability with POSIX termios.
 *                     Support of Xpm3 COLORED icons.
 *                     Polished Imakefile.
 * PATCH 501 applied - Transparency support for Xpm3 icons.
 *                     Possibility to map program in Term window.
 *                     New menu. dir first and use dot icon resources 
 *                     accessible through Options menu, map and select
 *                     under Selection menu.
 * PATCH 502 applied - Fixed code in process list management.
 *                     Added support for follow sym link.
 *                     Added support for environment variables in xdtmrc.
 *                     New resource systemLibDir to relocate help files.
 *                     Fixed bug with float resource.
 * PATCH 503 applied - Fixed bug with directory with more than 128 files 
 *                     (some files were not displayed).
 *	               Changed useDotIcon resource to useDotSpec.
 *	               Added useDotDotSpec resource likewise.
 *	               Changed parser to force setting at least one 
 *                     application in application manager.
 *	               Fixed bug with greying colored icons.
 *	               Fixed bug with default icons fallback.
 *	               Fixed bug with incorrect initialization of 
 *                     pixmaps in  parser.
 *	               Fixed bug in Imakefile to pass systemLibDir to 
 *                     compiler.
 * PATCH 504 applied - Double buffering suppressed in XedwList.c:PaintItemName.
 * PATCH 505 applied - Fixed bug in long listing
 *                     fixed bug with useDotSpec and useDotdotSpec
 *                     fixed bug with displays where black pixel is 0
 *                     fixed bug in process list dialog (not displaying pids)
 *                     alert dialog toplevel widget name changed to alertDialog
 *                     new resource and item in Options menu: silentSelection
 * PATCH 506 applied - Fixed bug with long . names without dot spec.
 * PATCH 507 applied - Fixed bug with Copy&Move due to tar not returning a reliable status
 *                     fixed process list management
 *                     symlink support suppressed only for TRUE_SYSV
 *                     added Reload config file option in File menu
 *                     polished code for ANSI compilers
 * PATCH 508 applied - Fixed bug with Reload when 
 *                     freeing preferences structs
 */
#define PATCHLEVEL 508

