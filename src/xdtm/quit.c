/*****************************************************************************
 ** File          : quit.c                                                  **
 ** Purpose       : Initialise and Realise quit dialogs                     **
 ** Author        : Edward Groenendaal                                      **
 ** Date          : April 1991                                              **
 ** Documentation : Xdtm Design Folder                                      **
 ** Related Files :                                                         **
 ** Changes       : 19-04-92, Edward Groenendaal                            **
 **                 Added #if NeedFunctionPrototypes stuff                  **
 **                 June 20, 1992, Ramon Santiago                           **
 **                 Changed all XtCreate calls to XtVaCreate calls.         **
 **                 Changed all caddr_t to XtPointer.                       **
 **                 Removed some lint.                                      **
 **                 Modified some variable names, moved language specific   **
 **                 strings to app-defaults file.                           **
 ****************************************************************************/

#include "xdtm.h"
#include "parse.h"
#include "Xedw/XedwList.h"
#include <X11/Shell.h>
#include <X11/Xaw/Label.h>
#include <X11/Xaw/Command.h>
#include "Xedw/XedwForm.h"

/* external and forward functions definitions */
#if NeedFunctionPrototypes
  extern void quitQueryResult(Widget, Boolean, XtPointer);
  void quitShowList(Widget, XtPointer, XtPointer);
  extern void realize_dialog(Widget, Widget, XtGrabKind, XtEventHandler, XtPointer);
#else
  extern void quitQueryResult();
  void quitShowList();
  extern void realize_dialog();
#endif

extern ProcessList *process_list;

/* Widgets */
private Widget quitDialog;
private Widget quitform;
private Widget quitlabel1;
private Widget quitlabel2;
private Widget quitquit;
private Widget quitcancel;
private Widget quitlistprocs;

/*****************************************************************************
 *                              init_quit                                    *
 *****************************************************************************/
public void init_quit(top)
Widget top;
{
  /* Initialise the quit dialog widgets */

  quitDialog =
    XtVaCreatePopupShell(
        "quitDialog",
	transientShellWidgetClass,
	top,
	    NULL ) ;

  quitform =
    XtVaCreateManagedWidget(
	"quitform",
	xedwFormWidgetClass,
	quitDialog,
	    NULL ) ;

  quitlabel1 =
    XtVaCreateManagedWidget(
        "quitlabel1",
	labelWidgetClass,
	quitform,
	    XtNborderWidth,           0,
	    XtNjustify, XtJustifyCenter,
	    XtNfullWidth,          True,
	    NULL ) ;

  quitlabel2 =
    XtVaCreateManagedWidget(
        "quitlabel2",
	labelWidgetClass,
	quitform,
	    XtNborderWidth,           0,
	    XtNjustify, XtJustifyCenter,
	    XtNfullWidth,          True,
	    XtNfromVert,     quitlabel1,
	    NULL ) ;

  quitquit =
    XtVaCreateManagedWidget(
        "quitquit",
	commandWidgetClass,
	quitform,
	    XtNfromVert,     quitlabel2,
	    NULL ) ;

  quitcancel =
    XtVaCreateManagedWidget(
        "quitcancel",
	commandWidgetClass,
	quitform,
	    XtNfromVert,     quitlabel2,
	    XtNjustify, XtJustifyCenter,
	    XtNfromHoriz,      quitquit,
	    NULL ) ;

  quitlistprocs =
    XtVaCreateManagedWidget(
        "quitlistprocs",
	commandWidgetClass,
	quitform,
	    XtNfromVert,     quitlabel2,
	    XtNjustify, XtJustifyCenter,
	    XtNfromHoriz,    quitcancel,
	    NULL ) ;


  XtAddCallback(quitcancel, XtNcallback, (XtCallbackProc)quitQueryResult,
		(XtPointer)False);
  XtAddCallback(quitquit,   XtNcallback, (XtCallbackProc)quitQueryResult,
		(XtPointer)True);
  XtAddCallback(quitlistprocs, XtNcallback, (XtCallbackProc)quitShowList,
		(XtPointer)0);
}

/*****************************************************************************
 *                                quit_dialog                                *
 *****************************************************************************/
public void quit_dialog()
{
  /* This procedure pops the quit dialog up on screen */

  realize_dialog(quitDialog, NULL, XtGrabNonexclusive, NULL, NULL);
}

/*****************************************************************************
 *                                                                           *
 *****************************************************************************/
public void destroy_quit_dialog()
{
  /* This procedure pop's the quit dialog down */

  XtPopdown(quitDialog);
}

/*****************************************************************************
 *                              quitShowList                                 *
 *****************************************************************************/
/*ARGSUSED*/
void quitShowList(w, client_data, call_data)
Widget w;
XtPointer client_data ;
XtPointer call_data ;
{
  extern void popup_process_list();

  popup_process_list();
}
