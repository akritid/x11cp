/*****************************************************************************
 ** File          : xdtm.h                                                  **
 ** Purpose       :                                                         **
 ** Author        : Edward Groenendaal                                      **
 ** Date          : 18th Feb 1991                                           **
 ** Documentation : Xdtm Design Folder                                      **
 ** Related Files : menus.c                                                 **
 ** Changes       : 28-11-91, Edward Groenendaal                            **
 **                 Updated to V2.0 from V1.8                               **
 **                 14-1-93, Lionel Mallet                                  **
 **                 Definition of DUMMY changed                             ** 
 *****************************************************************************/

#ifndef _xdtm_h
#define _xdtm_h

/* Current release of xdtm */
#define RELEASE       2

/* SYSTEM_LIB_DIR and SYSTEM_XDTMRC may be set automatically
 * by imake to be $(LIBDIR)/xdtm and $(LIBDIR)/xdtm/xdtmrc
 */

/* Change SYSTEM_LIB_DIR to the path of your xdtm lib dir */
#ifndef SYSTEM_LIB_DIR
#define SYSTEM_LIB_DIR   "/usr/lib/X11/xdtm"
#endif

/* Change SYSTEM_XDTMRC to the path of your system config. file */
#ifndef SYSTEM_XDTMRC
#define SYSTEM_XDTMRC "/usr/lib/X11/xdtm/xdtmrc"
#endif

#define SYSTEM_HELP "help"
#define SYSTEM_HELP_PERM "perm.help"

/* You shouldn't need to change anything below here  *
 * ================================================= */

#define DUMMY (Pixmap)~0

#ifdef TRUE_SYSV
#ifndef USE_CWD
#define USE_CWD  /* True SYSV machines use getcwd NOT getwd */
#endif
#endif

#include "patchlevel.h"

/* UNIX include files */
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

/* Standard X11 include files */
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>

#define private     	static
#define public      	/* global */

extern String help_file;
extern String perm_help_file;

typedef enum {NormalMode, CopyMode, MoveMode} Mode;

typedef struct {
  int view_width;
  int view_height;
  XFontStruct *view_font;
  int term_width;
  int term_height;
  XFontStruct *term_font;
  int delay;
  int options;
  Boolean dironexit;
  Boolean dirfirst;
  Boolean scrollonexit;
  Boolean confirmaction;
  Boolean bellonexit;
  Boolean bellonwarn;
  Boolean usedotspec;
  Boolean usedotdotspec;
  Boolean followsymlinks;
  Boolean silentsel;
  String mode;
  String cffile;
  String systemlibdir;
  XFontStruct *dm_font;
} AppData, *AppDataPtr;

extern AppData app_data;

/* Application Resources */

#define XtNviewWidth "viewWidth"
#define XtNviewHeight "viewHeight"
#define XtNviewFont "viewFont"
#define XtNtermWidth "termWidth"
#define XtNtermHeight "termHeight"
#define XtNtermFont "termFont"
#define XtNmode "mode"
#define XtNoptions "options"
#define XtNdirOnExit "dirOnExit"
#define XtNdirFirst "dirFirst"
#define XtNscrollOnExit "scrollOnExit"
#define XtNconfirmAction "confirmAction"
#define XtNbellOnExit "bellOnExit"
#define XtNbellOnWarn "bellOnWarn"
#define XtNuseDotSpec "useDotSpec"
#define XtNuseDotDotSpec "useDotDotSpec"
#define XtNfollowSymLinks "followSymLinks"
#define XtNsilentSelection "silentSelection"
#define XtNsystemLibDir "systemLibDir"
#define XtNconfigFile "configFile"
#define XtNdmFont "dmFont"
#define XtNdelay "delay"

#define XtCConfigFile "ConfigFile"
#define XtCViewWidth "ViewWidth"
#define XtCViewHeight "ViewHeight"
#define XtCTermWidth "TermWidth"
#define XtCTermHeight "TermHeight"
#define XtCMode "Mode"
#define XtCOptions "Options"
#define XtCDirOnExit "DirOnExit"
#define XtCDirFirst "DirFirst"
#define XtCScrollOnExit "ScrollOnExit"
#define XtCConfirmAction "ConfirmAction"
#define XtCBellOnExit "BellOnExit"
#define XtCBellOnWarn "BellOnWarn"
#define XtCUseDotSpec "UseDotSpec"
#define XtCUseDotDotSpec "UseDotDotSpec"
#define XtCFollowSymLinks "FollowSymLinks"
#define XtCSilentSelection "SilentSelection"
#define XtCSystemLibDir "SystemLibDir"
#define XtCDelay "Delay"

/* Local XedwList structure, has opt and isdir added. */

typedef struct _XdtmList {
  String string;           /* Must be first */
  Pixmap icon;             /* Must be second */
  Pixmap mask;
  XtPointer user_data;
  Boolean isdir;
} XdtmList;

extern void alert_dialog(/* String, String, String */);

#endif /* _xdtm_h */


