# -*- Mode: C -*-
# =======================================================================
# This is a sample xdtmrc file.
# 
# See the help file for details of the syntax.
#
# Author: Edward Groenendaal.
#         (additions by Henry Thomas)
#
# If you design any icons for use with xdtm, or have written a nice
# xdtmrc why don't you mail them to me at 'eddyg@cogs.sussex.ac.uk' and
# I'll include any nice bits in the next release of xdtm.
#
# NOTE: This file will only work with xdtm version 2.0 or later.
# =======================================================================

# Change this path to include the icon directories 
# In the release xdtmrc the string '{icon dir}' should be used, this is 
# substituted for the real icon directory automatically during installation
# by imake.
set path = "/usr/lib/X11/xdtm/icons";

# Check the whole path or just filenames when doing regular expression matches.
# checking for just filename is a LOT quicker that on the whole path
set checkpath=False;

# Default icon in case all rules fail
set deficon="file.icon";
# =======================================================================

if (type==dir) {
  set deficon="folder.icon";
  defappl "List" = {
      { "List" : "ls %f", TERM  }
      { "Emacs Dir" : "emacs %f" }
  }
  if (name=="^\.\.$") {
    set icon="dotdot.icon";
  }
  if (name=="^\.$") {
    set ignore;
  }
  if (name=="^\..*") {
    set ignore;
  }
  if (name=="^X11$") {
    set icon="x.icon";
  }
}

if (type==block) {
  set icon = "block.icon";
}

if (type==character) {
  set deficon = "character.icon";
  if (name=="^tty.*") {
    set icon="tty.icon";
  }
}

if (type==slink) {
  set deficon = "slink.icon";
  # We are now checking the destination of the link 
  if (type==dir) {
    set icon="folder.icon";
  }  
  #use the slink icon if destination NOT a directory.
}

if (type==socket) {
  set icon = "socket.icon";
}


if (type==file) {
  set deficon="file.icon";
  defappl "Info" = {
      { "Size" : "size %n", TERM }
  }
  defappl "Edit" = {
      { "Edit" : "emacs %f" }
  }
  
  # Ignore files starting with a dot
  if (name=="^%.*") {     
    set ignore;
  }

  # Ignore files starting and finishing with a #
  if (name=="^#.*#$") {
    set ignore;
  }

  # check for shared libraries
  if (name==".*\.so\..*") {
    set icon="lib.icon";
  }
  if (name==".*\.sa\..*") {
    set icon="lib.icon";
  }

  # set icons for excutable files
  if (type==exe) { 
    set deficon="prog.icon";

    # ignore executables starting with a dot
    if (name=="^\..*") {
      set ignore;
    }

    # ignore excutables starting with a %
    if (name=="^%.*") {
      set ignore;
    }

    # ignore excutables surounded by #'s (emacs auto-save file)
    if (name=="^#.*#$") {
      set ignore;
    }

    # use the xdtm icon for xdtm
    if (name=="^xdtm$") {
      set icon="xdtm.icon";
    }
  }
  
  # c source files and c++ ones
  if (name==".*\.[cC][c~]*$") {
    set icon="ccode.icon";
  }

  # object files
  if (name==".*\.o[~]*$") {
    set icon="ocode.icon";
  }

  # c header files
  if (name==".*\.h[~]*$") {
    set icon="hcode.icon";
  }

  # yacc source files
  if (name==".*\.y[~]*$") {
    set icon="ycode.icon";
  }

  # lex source files
  if (name==".*\.l[~]*$") {
    set icon="lcode.icon";
  }

  # emacs lisp files
  if (name==".*\.el[~]*$") {
    set icon="elisp.icon";
  }
  if (name=="^.emacs$") {
    set icon="elisp.icon";
  }
  if (name==".*\.elc$") {
    set icon="elispc.icon";
  }

  # libraries
  if (name==".*\.a$") {
    set cmd="nm -eT %n", TERM ;
    set icon="lib.icon";
  }

  # manual pages
  set checkpath = True;
  if (name==".*/man[1-8l]/.*") {
    set icon = "manpage.icon";
    set cmd = "xterm -title 'Manual page' -e sh -c 'nroff -man %n | less'";
  }
  set checkpath = False;

  if (name==".*\.man$") {
    set icon = "manpage.icon";
    set cmd = "xterm -title 'Manual page' -e sh -c 'nroff -man %n | less'";
  }

  # compressed files
  if (name==".*\.[ZF]$") {
    set icon="z.icon";
  }

  # text source files
  if (name==".*\.txt[~]*$") {
    set icon="text.icon";
  }
  if (name==".*\.tex[~]*$") {
    set icon="text.icon";
  }
  
  # README file
  if (name=="^[Rr][Ee][Aa][Dd][Mm][Ee]$") {
    set icon="text.icon";
  }

  # TAGS file
  if (name=="^TAGS$") {
    set icon="tag.icon";
  }
  # app-default files
  if (name=="^.*\.ad$") {
    set icon="text.icon";
    set cmd = "xrdb -merge %n";
  }
  set checkpath = True;
  if (name==".*/app-defaults/.*") {
    set icon="text.icon";
    set cmd = "xrdb -merge %n";
  }
  set checkpath = False;

  # sit, tar, shar files
  if (name==".shar$") {
    set icon="lib.icon";
  }
  if (name==".tar$") {
    set icon="lib.icon";
  }
  if (name==".sit$") {
    set icon="lib.icon";
  }

  # image files
  if (name==".p[pgmn]m$") {
    set icon="flower.icon";
  }
  if (name==".gif$") {
    set icon="flower.icon";
  }
  if (name==".ras*$") {
    set icon="flower.icon";
  }

  # sound files
  if (name==".au$") {
    set icon="mermaid.icon";
  }

  # Misc icons
  if (name=="^[Mm]ake[fF]ile[~]*$") {
      set icon="makefile.icon";
  }
  if (name=="^[Mm]ake[fF]ile\..*") {
    set icon="makefile.icon";
  }
  if (name=="^[iI][Mm]ake[Ff]ile[~]*$") {
      set icon="makefile.icon";
  }
  if (name==".*\.icon[~]*$") {
    set icon="icon.icon";
    set cmd = "bitmap %n";
  }
  if (name=="^core$") {
    set icon="core.icon";
  }

  # application icons
  if (name=="^xdtm$") {
    set icon="xdtm.icon";
  }
}

# =======================================================================
# Application manager defaults
# =======================================================================

define "programming" = {
  { name = "emacs";
    icon = "emacs.icon";
    prog = "emacs %s"; 
    options = MSEL;    
  }
  {
    name = "C Debugger";
    icon = "prog.icon";
    prog = "xterm -T 'Gnu C Debugger' -e gdb %s";
    options = OSEL;
  }
  {
    name = "Make All";
    icon = "make.icon";
    prog = "make";
    options = NSEL;
    termopt = TERM;
  }
  {
    name = "Make etags";
    icon = "make.icon";
    prog = "make etags";
    options = NSEL;
  }
  {
    name = "Make Clean";
    icon = "make.icon";
    prog = "make clean";
    options = NSEL;
  }
  { 
    name = "Make Makefile";
    icon = "make.icon";
    prog = "xmkmf";
    options = NSEL;
  }
}
# -----------------------------------------------------------------------

define "misc" = {
  { 
    name = "X Clock";
    icon = "clock.icon";
    prog = "xclock";
    options = NSEL;
  }
  {
    name = "X Bitmap";
    icon = "piccy.icon";
    prog = "bitmap -nodashed %s";
    options = OSEL;
  }
  {
    name = "Compress";
    icon = "z.icon";
    prog = "compress %s";
    options = MSEL;
  }
  {
    name = "Uncompress";
    icon = "text.icon";
    prog = "uncompress %s";
    options = MSEL;
  }
}

# -----------------------------------------------------------------------
define "games" = {
  {
    name = "Nethack";
    icon = "ghost.icon";
    prog = "xterm -T 'Nethack' -e nethack";
    options = NSEL;
  }
}

# -----------------------------------------------------------------------
define "editing" = {
  {
    name = "X Terminal";
    icon = "prog.icon";
    prog = "xterm -T 'X Terminal'";
    options = NSEL;
  }
  {
    name = "Spell Check";
    icon = "prog.icon";
    prog = "ispell %s";
    options = MSEL;
    termopt = TERM;
  }
  {
    name = "Edit : vi";
    icon = "dragon.icon";
    prog = "xterm -T 'Vi' -e vi %s";
    options = OSEL;
  }
  {
    name = "Edit : Emacs";
    icon = "emacs.icon";
    prog = "xterm -T 'Emacs' -e emacs %s";
    options = MSEL;
  }
  {
    name = "Edit : Ved";
    icon = "alien.icon";
    prog = "xterm -T 'Poplog Ved' -e ved %s";
    options = OSEL;
  }
  {
    name = "Word Count";
    icon = "wordc.icon";
    prog = "wc %s";
    options = MSEL;
    termopt = TERM;
  }
}

# -----------------------------------------------------------------------
define "remote logins" = {
  {
    name = "tsuna";
    icon = "fish.icon";
    prog = "xterm -e rlogin tsuna";
    options = NSEL;
  }
  {
    name = "syma";
    icon = "hedgehog.icon";
    prog = "xterm -e rlogin syma";
    options = NSEL;
  }
  {
    name = "cste";
    icon = "snail.icon";
    prog = "xterm -e rlogin cste";
    options = NSEL;
  }
  {
    name = "dinkum";
    icon = "prog.icon";
    prog = "xterm -e rlogin dinkum";
    options = NSEL;
  }
  {
    name = "cousteau";
    icon = "prog.icon";
    prog = "xterm -e rlogin cousteau";
    options = NSEL;
  }
  {
    name = "surya";
    icon = "prog.icon";
    prog = "xterm -e rlogin surya";
    options = NSEL;
  }
  {
    name = "lucretia";
    icon = "mermaid.icon";
    prog = "xterm -e rlogin lucretia";
    options = NSEL;
  }
  { 
    name = "pss-gate";
    icon = "pss.icon";
    prog = "xterm -e pad pss-gate";
    options = NSEL;
  }
}
# -----------------------------------------------------------------------
