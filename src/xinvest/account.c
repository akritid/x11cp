/*
  Xinvest Copyright 1995-97 Mark Buser, 
  All Rights Reserved.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.19 $ $Date: 1997/09/20 17:01:52 $
*/

#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <Xm/XmAll.h>

#include "account.h"
#include "askuser.h"
#include "calendar.h"
#include "drawing.h"
#include "graph.h"
#include "nav.h"
#include "portfolio.h"
#include "rate.h"
#include "report.h"
#include "status.h"
#include "transP.h"
#include "trans.h"
#include "util.h"


typedef struct accnt {

  /* Need tool state, blind structure might be good. state = SaveState(),
  ** SetState(account[num]->state) */

  Widget textWidget;      /* widget holding transaction listing */
  char *transText;        /* if the text is not display, its here */
  char *filename;         /* where from */
  unsigned flags;         /* modified, applied, in portfolio, writable */
  NAV  *calc_nav;         /* current date, price */
  char *title;            /* title */
  char *ticker;           /* ticker symbol */
  int  transno;           /* # transactions */
  TRANS *trans;           /* the transactions */
  PLOT_VAR  *plot;        /* the plot variables selected and attributes */
} ACCOUNT;

static int accountno = 0;
static ACCOUNT *account = NULL;
static int _activeAccount = 0;

/* Forward Declarations */
static void accountDirty();
static void makeAccountMenu();
static void setAccountMenuItem ( int );

extern Widget Transtext;  /* Transaction text widget */


/* Where the apply change button is on the account menu */
#define APPLY_BUTTON "button_2"

int activeAccount()
{
  return _activeAccount;
}

int numAccounts()
{
  return accountno;
}

/* Return true if any open account has flag set */
int accountsStatus(int flag)
{
  int i, count = 0;
  for (i=1; i <= accountno; i++)
    if ( account[i].flags & flag )
       count++;
  return (count);
}

/* Return true if 'which' account has 'flag' set */
int accountStatus(int which, unsigned flag)
{
  if ( account[which].flags & flag )
    return (True);
  else
    return (False);
}

void accountAddPortfolio ( int which )
{
  account[which].flags |= ACCOUNT_IN_PORTFOLIO;
}

void accountDelPortfolio ( int which )
{
  int i;

  if (which == ALL_ACCOUNTS) {
    for (i=1; i <= accountno; i++)
      account[i].flags &= ~ACCOUNT_IN_PORTFOLIO;
  } else
    account[which].flags &= ~ACCOUNT_IN_PORTFOLIO;
}

void accountClean ( int index )
{
  if (index) {
    extern Widget Accountmenu, Savebut;
    Widget menu;

    XtSetSensitive( Savebut, False );
    XtVaGetValues ( Savebut, XmNuserData, &menu, NULL); 
    XtSetSensitive( menu, False );
    if ( (menu = XtNameToWidget ( Accountmenu, APPLY_BUTTON )) )  
      /* the apply change button */
      XtSetSensitive( menu, False );

    account[index].flags &= ~ACCOUNT_DIRTY;
  }
}

int accountIsOpen ( char *filename )
{
  int i;

  if (accountno) {
    /* search list for this filename */
    for (i=1; i <= accountno; i++)
      if ( filename && account[i].filename)
        if (strcmp ( account[i].filename, filename ) == 0 )
          return (i);
  } 
  return (0);
}

    
int newAccount ( char *filename, char *transtext, int writable )
{
  int oldaccount = accountIsOpen ( filename );

  /* If already existed, close it */
  if (oldaccount)
    closeAccount (oldaccount); 

  /* Create new account */
  /* +2 == one for [0] (unused), one for new account */
  account = (ACCOUNT *) XtRealloc ( (char *)account, 
                                    sizeof(ACCOUNT) * (accountno+2) ); 
  if (account == NULL) {
    write_status ("Out of memory: could not create new account.", ERR);
    return(0);
  }
  accountno++;

  account[accountno].textWidget = Transtext;
  if (transtext)
    account[accountno].transText = transtext;
  else
    account[accountno].transText = NULL;

  if (filename) {
    account[accountno].filename = XtCalloc (1, strlen(filename)+1 );
    if (account[accountno].filename == NULL) {
      write_status ("Out of memory: could not create new account.", ERR);
      return(0);
    }
    strcpy(account[accountno].filename, filename);
  } else
   account[accountno].filename = NULL;

  account[accountno].flags = 0;
  if (writable)
    account[accountno].flags |= ACCOUNT_WRITABLE;

  /* calc_nav set in processAccount */
  account[accountno].calc_nav = (NAV *)XtCalloc (1, sizeof(NAV));
  if (account[accountno].calc_nav == NULL) {
    write_status ("Out of memory: could not create new account.", ERR);
    return(0);
  }
  account[accountno].title = NULL;
  account[accountno].ticker = NULL;
  account[accountno].transno = 0;
  account[accountno].trans = NULL;
  account[accountno].plot = makeGraphVar();

  return(accountno);
}

void closeAccount ( int index )
{
  /* Is valid account */
  if ( index > 0 && index <= accountno ) {

    /* Has account been modified */
    if (account[index].flags & ACCOUNT_DIRTY ) {
      char prompt[] = "Account has been changed, close anyway?";
      if ( AskUser ( account[index].textWidget, prompt, "OK", 
                     "Cancel", YES) == NO )
        return;
    }

    /* Is it in the porfolio? */
    if ( account[index].filename && 
         account[index].flags & ACCOUNT_IN_PORTFOLIO ) {
       char prompt[] = "Account is in the portfolio, close anyway?";
       if ( AskUser ( account[index].textWidget, prompt, "OK", 
                      "Cancel", YES) == NO )
         return;
       else {
         portAccount( index, PORT_DELETE);
         /* Changed portfolio, redraw pie.  It may not be on, but we don't 
         ** know that. */
         portUpdateDisplay();  
         portSense();
       }  
    }

    /* Only free the memory of closed account */
    if  (account[index].transText )
      XtFree ( account[index].transText );
    if ( account[index].filename )
      XtFree ( account[index].filename );
    if ( account[index].calc_nav )
      XtFree ( (char *)account[index].calc_nav );
    if ( account[index].title )
      XtFree ( account[index].title );
    if ( account[index].ticker )
      XtFree ( account[index].ticker );
    if (account[index].trans)
      XtFree ( (char *)account[index].trans );
    if (account[index].plot)
      XtFree ( (char *)account[index].plot );

    /* Ok slide everybody up */
    while ( index < accountno ) {
      account[index].textWidget = account[index+1].textWidget;
      account[index].transText  = account[index+1].transText;
      account[index].filename   = account[index+1].filename;
      account[index].flags      = account[index+1].flags;
      account[index].calc_nav   = account[index+1].calc_nav;
      account[index].title      = account[index+1].title;
      account[index].ticker     = account[index+1].ticker;
      account[index].transno    = account[index+1].transno;
      account[index].trans      = account[index+1].trans;
      account[index].plot       = account[index+1].plot;
      index++;
    }

    accountno--;
    account = (ACCOUNT *)XtRealloc ( (char *)account, 
                                     sizeof(ACCOUNT) * (accountno+1) ); 

    /* The above should never fail, but boy if it does! */
    if (account == NULL) {
      char prompt[] = "Memory error removing account. Account\n"
                      "pointer is corrupt. Exiting Xinvest without\n"
                      "saving changes.\n";
      AskUser ( Transtext, prompt, "OK", "Cancel", YES );
      exit(-1);
    }

    /* Display new account, don't touch the old one */
    displayAccount (-1); 
    makeAccountMenu();
  }
}

void processAccount ( int num ) 
{
  char *transtext;
  char *cp;

  char *error = NULL;
  char err_msg[132];

  if (num == 0)
    return;

  /* Copy transactions for destructive process */
  transtext = strdup( account[num].transText );
  
  /* Will last line always be processed if no trailing \n? */

  /* 
  ** Fill in transaction structures 
  */

  /* cp points to 2nd line (if any) */
  cp = strtok ( transtext, "\n" );   /* write null to end of 1st line */

  /* Always process once, even if its NULL, to clear out trans struct */
  error = rd_trans ( transtext, T_NEW );

  /* If 2nd line */
  if (cp == transtext)
    cp = strtok ( NULL, "\n" );

  /* 3rd+ line, look for newlines to feed to rd_trans */
  while ( cp && !error) {
    error = rd_trans ( cp, T_SAME );
    cp = strtok ( NULL, "\n" );
  }
 
  if (error) {
    char *name = createAccountName (num);
    sprintf( err_msg, "%s:\n%s", name, error );
    write_status ( err_msg, ERR);
    XtFree (name);
  }

  XtFree ( transtext );

  /* Save the transactions for later */
  if (account[num].trans)
    XtFree ( (char *)account[num].trans);
  account[num].trans = GetTrans();
  account[num].transno = AreTrans();

  /* Set title info */
  if (account[num].title)
    XtFree (account[num].title);
  account[num].title = GetTransTitle();

  /* Set ticker info */
  if (account[num].ticker)
    XtFree (account[num].ticker);
  account[num].ticker = GetTransTicker();

  /* We're up to date */
  account[num].flags &= ~ACCOUNT_UPDATE;

  /* Fill in structure if we have transactions */
  if ( account[num].transno ) {

    /* Set NAV info from last transaction */
    sscanf( GetTransDate( account[num].trans, account[num].transno ), 
            "%d/%d/%d", 
            &account[num].calc_nav->month, &account[num].calc_nav->date, 
            &account[num].calc_nav->year );

    account[num].calc_nav->value = GetTransNav ( account[num].trans,
                                                 account[num].transno );
  } else {
    time_t t = time ( (time_t *)0 );
    struct tm *today = localtime (&t);
    /* tm_mon is 0-11, calc_nav->month is 1-12 */
    account[num].calc_nav->month = today->tm_mon +1;
    account[num].calc_nav->date = today->tm_mday;
    account[num].calc_nav->year = today->tm_year;
    if (account[num].calc_nav->year < 1900 )
      account[num].calc_nav->year += 1900;

    account[num].calc_nav->value = 0.0;
  }

}

void displayAccount (int num)
{
  /* All the stupid widgets we need to set */
  extern Widget Translabel;
  extern Widget Graphbut, Gainbut, Savebut, Prevbut, Nextbut; 
  extern Widget Accountmenu, Filemenu;
  extern void toolCB();

  int  new, enable;
  Widget menu;
  int managedAccount = 0;        /* Last account in Transtext */

  XmString title_str;

  if ( num > accountno )
    return;

  /* Clear the screen */
  if ( num == 0 ) 
    new = 0;

  /* _activeAccount is no more, show last transaction */
  else if ( num < 0 ) {
    if ( (new = (_activeAccount -1)) < 1)
      new = accountno;
  
    XtVaGetValues ( Transtext, 
                    XmNuserData, &managedAccount, 
                    NULL);
    XtRemoveCallback( Transtext,
                      XmNvalueChangedCallback, (XtCallbackProc) accountDirty,
                      (XtPointer) managedAccount );
  }

  /* a normal variety change of focus */
  else {
    new = num;
    if (_activeAccount)
      XtVaGetValues ( account[_activeAccount].textWidget, 
                    XmNuserData, &managedAccount, 
                    NULL);

    if (managedAccount) {
      XtRemoveCallback( account[managedAccount].textWidget,
                        XmNvalueChangedCallback, (XtCallbackProc) accountDirty,
                        (XtPointer) managedAccount );
      /* Save the current text, free old if any */
      if ( account[managedAccount].transText )
        XtFree ( account[managedAccount].transText );
      account[managedAccount].transText = 
             XmTextGetString ( account[managedAccount].textWidget );

    }
  }

  _activeAccount = new;

  /* Display the title */
  if ( _activeAccount ) {
    char *title;
    char *label;
    char number[] = " [xx/xx] [RO]";

    XtSetSensitive ( Transtext, True );

    sprintf ( number, " [%d/%d]", _activeAccount, accountno);
    if ( !accountStatus(_activeAccount, ACCOUNT_WRITABLE) )
      strcat ( number, " [RO]");

    title = createAccountName (_activeAccount);
    label = XtCalloc (1, strlen(title) + strlen(number) +1);
    sprintf ( label, "%s%s", title, number );
    title_str = XmStringCreateLocalized ( label ); 
    XtFree (title);
    XtFree (label);

  } else {
    XtSetSensitive ( Transtext, False );
    title_str = XmStringCreateLocalized ( "No Open Accounts" ); 
  }

  XtVaSetValues ( Translabel, XmNlabelString, title_str, NULL);
  XmStringFree ( title_str );
  /********************/

  /* display the text */
  if (_activeAccount) {

    XmTextSetString ( account[new].textWidget, account[new].transText );
    XtAddCallback( account[new].textWidget,
                   XmNvalueChangedCallback, (XtCallbackProc) accountDirty,
                   (XtPointer)new);
    XtVaSetValues ( account[new].textWidget, XmNuserData, new, NULL);

  } else {
    XmTextSetString ( Transtext, "");
    XtVaSetValues ( Transtext, XmNuserData, num, NULL);
  }
  /********************/

  /* Set active tools */
  if ( _activeAccount && account[new].transno > 0 )
    enable = True;
  else
    enable = False;

  /* plotting */
  XtSetSensitive (Graphbut, enable );
  XtVaGetValues (Graphbut, XmNuserData, &menu, NULL); 
  XtSetSensitive (menu, enable );

  /* reports */
  XtSetSensitive (Gainbut, enable );
  XtVaGetValues (Gainbut, XmNuserData, &menu, NULL); 
  XtSetSensitive (menu, enable );
  
  /* asset allocator */
  portSense();
  
  /* Set save buttons if changed and writable and has a filename */
  if ( _activeAccount && accountStatus( new, ACCOUNT_DIRTY) && 
       accountStatus( new, ACCOUNT_WRITABLE) && account[new].filename)
    enable = True;
  else
    enable = False;
  XtSetSensitive( Savebut, enable );
  XtVaGetValues ( Savebut, XmNuserData, &menu, NULL); 
  XtSetSensitive( menu,    enable );

  /* Have we any unapplied changes? */
  if ( _activeAccount && (account[new].flags & ACCOUNT_UPDATE) )
    enable = True;
  else
    enable = False;
  if ( (menu = XtNameToWidget ( Accountmenu, APPLY_BUTTON )) )/* apply change */
    XtSetSensitive( menu, enable );

  /* If more than one open account, cycling is allowed */
  if (accountno > 1) 
    enable = True;
  else
    enable = False;
  XtSetSensitive( Prevbut, enable );
  XtSetSensitive( Nextbut, enable );
  XtVaGetValues ( Prevbut, XmNuserData, &menu, NULL); 
  XtSetSensitive( menu, enable );
  XtVaGetValues ( Nextbut, XmNuserData, &menu, NULL); 
  XtSetSensitive( menu, enable );
   

  if ( (menu = XtNameToWidget ( Filemenu, "button_2" )) ) { /* close */
    if (accountno > 0)
      XtSetSensitive( menu, True );
    else
      XtSetSensitive( menu, False );
  }
  /********************/

  /* Restore state */
  if ( _activeAccount ) {

    if ( account[new].transno > 0 ) {
      /* Set graph vars selected to this account. */
      displayGraph (new);

      /* Make report generator aware of account switch */
      displayReport(new);

      /* Asset allocator doesn't need to be told */
    } else {
      /* Pretend "about" button has been pushed.  This clears any remnants of
      ** old plot data and calls redrawDrawing. */
      toolCB ((Widget)NULL, (XtPointer)0, (XtPointer)NULL );
    }
    drawDrawingArea();

    /* Very expensive operation, only do it if we are changing the displayed 
    ** account.  This even failed to work correctly when used in 
    ** processAccount on files specified on command line (rapid new accounts
    ** created).
    */
    makeAccountMenu();
  }
  /********************/
}

void setAccount (int index, int field, void *val )
{
  if ( index >0 && index <= accountno ) {

    switch (field) {

      case ACCOUNT_FILENAME:
           if ( account[index].filename != NULL )
             XtFree (account[index].filename);
           if (val) {
             account[index].filename = XtCalloc ( 1, strlen (val) +1 );
             strcpy ( account[index].filename, (char *) val);
           } else
             account[index].filename = NULL;
          
           break;

      case ACCOUNT_PLOT:
           account[index].plot = (PLOT_VAR *)val;
           break;

      default:
          write_status ("Can't change value, no such field.", ERR);
          break;
   }

  } else
    write_status ("Can't change value, no such account index.", ERR);
}


void getAccount (int index, int field, void *val )
{
  if ( index >0 && index <= accountno ) {

    switch (field) {

      case ACCOUNT_FILENAME:
           *(char **)val = account[index].filename;
           break;

      case ACCOUNT_TITLE:
           *(char **)val = account[index].title;
           break;

      case ACCOUNT_TICKER:
           *(char **)val = account[index].ticker;
           break;

      case ACCOUNT_NAV: 
           *(NAV **)val = account[index].calc_nav;
           break;

      case ACCOUNT_NUM_TRANS:
           *(int *)val = account[index].transno;
           break;
           
      case ACCOUNT_TRANS:
           *(TRANS **)val = account[index].trans;
           break;

      case ACCOUNT_TRANSTEXT:
           *(char **)val = account[index].transText;
           break;

      case ACCOUNT_PLOT:
           *(PLOT_VAR **)val = account[index].plot;
           break;

      default:
          write_status ("Can't return value, no such field.", WARN);
          *(char **)val = NULL;
          break;
   }

  } else {
    write_status ("Can't return value, no such account index.", WARN);
    *(char **)val = NULL;
  }
}


/* Callbacks */

/* ARGSUSED */
static void accountDirty (Widget w, XtPointer client_data, 
                   XmAnyCallbackStruct *call_data)
{
  extern Widget Savebut, Accountmenu;
  Widget menu;
  
  int num = (int)client_data;

  account[num].flags |= ACCOUNT_DIRTY;
  account[num].flags |= ACCOUNT_UPDATE;

  /* Set save button if writable and has a filename */
  if (account[num].filename && (account[num].flags & ACCOUNT_WRITABLE) ) {
    XtSetSensitive( Savebut, True );
    XtVaGetValues ( Savebut, XmNuserData, &menu, NULL); 
    XtSetSensitive( menu,    True );
  }

  if ( (menu = XtNameToWidget ( Accountmenu, APPLY_BUTTON )) )/* apply change */
    XtSetSensitive( menu, True );

}

/* Is the account select box popped up */
static Widget dialog;

static void selectCB (Widget w, 
               XtPointer list, 
               XmSelectionBoxCallbackStruct *call_data )
{
  Widget listwidget = (Widget) list;
  int account;

  if (call_data->reason == XmCR_OK) {
    account = XmListItemPos ( listwidget, call_data->value);
    if ( account > 0 && account <= accountno)
      displayAccount(account);

  } else if (call_data->reason == XmCR_CANCEL) {
    XtDestroyWidget(w);
    dialog = NULL;
  }
}

/* ARGSUSED */
void accountCB (Widget w, int item_no, XmAnyCallbackStruct *call_data)
{
  Widget child;
  Arg args[1];
  int num;
  int top, visible;

  char *name;
  XmStringTable account_list;

  switch (item_no) {

    case 0: /* Prev account */
    case 1: /* Next account */
            if (accountno > 1) {
              int offset = (item_no)?1:-1;
              int next;

              /* Setting menus after destroying and creating new ones
              ** is a real race condition.  Select the menu before
              ** destroying/creating the new ones.
              */
              if (_activeAccount+offset > accountno)
                next = 1;
              else if (_activeAccount+offset < 1)
                next = accountno;
              else
                next = _activeAccount+offset;

              setAccountMenuItem (next);
              displayAccount (next);
            }
            break;

    case 2: /* Update account */
            if ( account[_activeAccount].transText )
              XtFree ( account[_activeAccount].transText );
            account[_activeAccount].transText = 
            XmTextGetString ( account[_activeAccount].textWidget );

            processAccount( _activeAccount );
            displayAccount( _activeAccount );

            break;

    case 3: /* More accounts */

            if (dialog == NULL) {
              num = 0;
              XtSetArg ( args[num], XmNautoUnmanage, False); num++;
              dialog = XmCreateSelectionDialog( Transtext, "Accountsel",
                                                args, num);

              /* I don't need these */
              child = XmSelectionBoxGetChild ( dialog, XmDIALOG_TEXT );
              XtUnmanageChild (child);
              child = XmSelectionBoxGetChild ( dialog, XmDIALOG_SELECTION_LABEL );
              XtUnmanageChild (child);
              child = XmSelectionBoxGetChild ( dialog, XmDIALOG_APPLY_BUTTON );
              XtUnmanageChild (child);
              child = XmSelectionBoxGetChild ( dialog, XmDIALOG_HELP_BUTTON );
              XtSetSensitive (child, False);

              /* Pass list widget to callback so we can get where the selection
              ** position is from it. */
              child = XmSelectionBoxGetChild ( dialog, XmDIALOG_LIST );
              XtAddCallback ( dialog, XmNokCallback, 
                              (XtCallbackProc) selectCB, (XtPointer) child);
              XtAddCallback ( dialog, XmNcancelCallback, 
                              (XtCallbackProc) selectCB, (XtPointer) child);
            }

            account_list = (XmStringTable) XtMalloc (
                                accountno * sizeof (XmString) );

            /* Create account names */
            for ( num = 1; num <= accountno; num++) {
              name = createAccountName ( num );
              if (name == NULL) {
               write_status ("Out of memory: could not create account dialog.", 
                              ERR);
               return;
              }
              account_list[num-1] = XmStringCreateLocalized ( name );
              XtFree (name);
            }

            XtVaSetValues ( dialog, XmNlistItems, account_list,
                                    XmNlistItemCount, accountno,
                            NULL);

            for ( num = 1; num <= accountno; num++)
              XmStringFree ( account_list[num-1] );
            XtFree ( (char *)account_list);

            /* Set currently displayed account as selected. */
            child = XmSelectionBoxGetChild ( dialog, XmDIALOG_LIST );
            XmListSelectPos (child, _activeAccount, True);

            /* Do we need to make the selection visible */
            XtVaGetValues ( child, XmNvisibleItemCount, &visible,
                                   XmNtopItemPosition, &top,
                            NULL);
            if ( _activeAccount >= top+visible )
              XmListSetBottomPos (child, _activeAccount);
            else if ( _activeAccount < top )
              XmListSetPos (child, _activeAccount);

            XtManageChild (dialog);
            XtPopup (XtParent(dialog), XtGrabNone);

            break;

    case 4:   /* Display account 1 */
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
    case 10:
    case 11:
    case 12:  /* Display account 9 */
            setAccountMenuItem ( item_no - 3);
            displayAccount (item_no - 3);
            break;

    default:  break;

  }

}

static void setAccountMenuItem(int which)
{
  extern Widget Accountmenu;

  WidgetList button;
  int num;

  XtVaGetValues ( Accountmenu,
                    XmNchildren, &button,
                    XmNnumChildren, &num,
                  NULL);

  /* The 3 is because there are always three extra children on this menu.
  ** Accounts start at the fourth child (cycle, apply, separator, accounts...).
  */
  if (which+3 > num)
    return;

#if XmVERSION == 1
#define XmSET   True
#define XmUNSET False
#endif

  /* ... but they are numbered (num-1)..0. */
  while (--num > 2)
    XtVaSetValues ( button[num], 
                    XmNset, ((which+2) == num)? XmSET:XmUNSET, 
                    NULL);
}


static void makeAccountMenu()
{

  extern Widget Accountmenu;
  WidgetList button;
  Widget newbutton;

  int num, item; 
  char *name;

  /* Destroy the old variable menu items */
  XtVaGetValues ( Accountmenu, XmNchildren, &button, 
                               XmNnumChildren, &num,
                  NULL);

  for ( item = 4; item < num; item++)
    XtDestroyWidget ( button[item] );

  /* Make the new variable menu items */
  for ( item = 1; item <= accountno && item < 10; item++) {
    name = createAccountName ( item );
    if (name == NULL) {
      write_status ("Out of memory: could not create account menu.", ERR);
      return;
    }

    newbutton = XtVaCreateManagedWidget ( name,  
                          xmToggleButtonGadgetClass, Accountmenu,
                          XmNindicatorType,  XmONE_OF_MANY,
                          XmNset, (item == _activeAccount)?True:False,
                          NULL );
    XtAddCallback ( newbutton, XmNvalueChangedCallback, 
                    (XtCallbackProc) accountCB, (XtPointer) (item + 3) );
    XtFree (name);
  }

  if (accountno > 9) {
    newbutton = XtVaCreateManagedWidget ( "more",
                          xmPushButtonGadgetClass, Accountmenu,
                          NULL );
    XtAddCallback ( newbutton, XmNactivateCallback,
                  (XtCallbackProc) accountCB, (XtPointer) 2 );
  }

  /* If dialog is up, update its contents */
  if (dialog)
    accountCB ( NULL, 2, NULL);
}

/* 
** Create a nice account name.  If 'which' is ALL_ACCOUNTS return "Portfolio".
** Return NULL on alloc errors.
*/ 
char *createAccountName( int which )
{
  char *name;

  if (which == ALL_ACCOUNTS) {
    name = XtNewString ("Portfolio");
  } else {
    char *title;
    getAccount ( which, ACCOUNT_TITLE, &title );
    if (title) {
      name = XtNewString (title);
    } else {
      if ( (name = XtCalloc (1, 12*sizeof(char))) )
        sprintf ( name, "Account %3d", which);
    }
  }
  return (name);
}

void saveAccount ( FILE *savefile, int index)
{
  NAV *navp;

  /* save account filename */
  fprintf ( savefile, "LOAD %d %s\n", index, account[index].filename );

  /* save last nav if different from last */
  getAccount ( index, ACCOUNT_NAV, &navp);
  if ( isNavChanged ( index, navp ) ) 
    fprintf ( savefile, "NAV %d %d/%d/%d %f\n", index, 
              account[index].calc_nav->month, account[index].calc_nav->date, 
              account[index].calc_nav->year, account[index].calc_nav->value );
}

int accountMatchTicker (char *ticker, int month, int day, int year, 
                        double nav)
{
  int accountno, found = 0;
  char *acct_ticker;

  for (accountno=1; accountno <= numAccounts(); accountno++) {
    getAccount ( accountno, ACCOUNT_TICKER, &acct_ticker);
    if (acct_ticker && (strcasecmp (ticker, acct_ticker) == 0) ) {
      found = 1;
      account[accountno].calc_nav->month = month;
      account[accountno].calc_nav->date = day;
      account[accountno].calc_nav->year = year;
      account[accountno].calc_nav->value = nav;
      if (accountno == _activeAccount)
        displayReport(accountno);    /* inform report of account change */
    }
  }
  return ((found)?accountno:0);
}

/*
** Highlight widget if passed date matches one in account's transaction
** history
*/
void accountHighlightDay (char *date, int account, Widget w)
{
  TRANS *trans;
  int numTrans, i;

  Pixel bg, fg;
  long ldate;

  ldate = strtoday (date);

  /* Reverse the foreground and background colors */
  XtVaGetValues (w, 
                 XmNbackground, &bg, 
                 XmNforeground, &fg, 
                 NULL);

  /* Is there a transaction on that date */
  getAccount (account, ACCOUNT_TRANS, &trans);
  getAccount (account, ACCOUNT_NUM_TRANS, &numTrans);
  for (i=1; i <= numTrans; i++)
    if (GetTransLDate(trans, i) == ldate) {
      XtVaSetValues (w, 
                     XmNforeground, bg, 
                     XmNbackground, fg, 
                     NULL);
      break;
    }
}
