/*
  Xinvest is copyright 1995 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.1 $ $Date: 1997/01/13 22:46:19 $
*/
#include <Xm/Xm.h>
#include <Xm/DialogS.h>
#include <Xm/MessageB.h>

#include "askuser.h"

/* ARGSUSED */
static void response ( Widget widget, 
                XtPointer client_data, 
                XtPointer call_data )
{
  int *answer = (int *) client_data;
  XmAnyCallbackStruct *cbs = (XmAnyCallbackStruct *) call_data;

  if (cbs->reason == XmCR_OK)
    *answer = YES;
  else if (cbs->reason == XmCR_CANCEL)
    *answer = NO;
}


int AskUser ( Widget parent,
              char   *question, 
              char   *ans1,
              char   *ans2,
              int    default_ans )
{
  static Widget dialog = NULL;
  XmString text, yes, no;
  static int answer;

  if (!dialog) {
    dialog = XmCreateQuestionDialog (parent, "alert", NULL, 0);
    XtVaSetValues ( dialog, XmNdialogStyle, XmDIALOG_FULL_APPLICATION_MODAL,
                    NULL );
    XtSetSensitive ( XmMessageBoxGetChild ( dialog, XmDIALOG_HELP_BUTTON),
                     False );
    XtAddCallback (dialog, XmNokCallback, response, &answer);
    XtAddCallback (dialog, XmNcancelCallback, response, &answer);
  }

  answer = 0;

  text = XmStringCreateLocalized ( question );
  yes = XmStringCreateLocalized ( ans1 );
  no = XmStringCreateLocalized ( ans2 );
  XtVaSetValues ( dialog, XmNmessageString,     text,
                          XmNokLabelString,     yes,
                          XmNcancelLabelString, no,
                          XmNdefaultButtonType, (default_ans == YES)?
                          XmDIALOG_OK_BUTTON : XmDIALOG_CANCEL_BUTTON,
                  NULL );
  XmStringFree ( text );
  XmStringFree ( yes );
  XmStringFree ( no );

  XtManageChild ( dialog );
  XtPopup ( XtParent (dialog), XtGrabNone );

  while (answer == 0)
   XtAppProcessEvent ( XtWidgetToApplicationContext(dialog), XtIMAll);

  XtUnmanageChild ( dialog );
  XtPopdown ( XtParent(dialog) );

  return answer;
}
