/*
  Xinvest Copyright 1995-97 Mark Buser,
  All Rights Reserved.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.8 $ $Date: 1997/08/09 19:37:37 $
*/

/* Change the month the calendar is displaying */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <Xm/XmAll.h>

#include "account.h"
#include "calendar.h"

/*-------------------------------------------------------------
** Forward Declarations
*/
static void monthCB (Widget, XtPointer, XtPointer);
static void yearUpDnCB (Widget, XtPointer, XtPointer);
static void dayCB (Widget, XtPointer, XtPointer);
static Widget createMonth (Widget, Calendar *);


static int mtbl[] = { 0,31,59,90,120,151,181,212,243,273,304,334,365 };

static int days_in_month( int year, int month )
{
   int days;

   days = mtbl[month] - mtbl[month - 1];
   if ( month == 2 && year % 4 == 0 && (year % 100 != 0 || year % 400 == 0) )
     days++;
  
   return days;
}

static int day_number (year, month, day )
int year, month, day;
{
  long days_ctr;

  year -= 1900;
  days_ctr = ((long)year * 365L) + ((year + 3) / 4);
  days_ctr += mtbl[month -1] + day + 6;

  year += 1900;
  if ( month > 2 && year % 4 == 0 && (year % 100 != 0 || year % 400 == 0) )
    days_ctr++;

  return (int) (days_ctr % 7L);
}

int calUserTouched (Calendar *this)
{
  if (this)
    return this->userMod;
  else
    return 0;
}

void calSetDate (Calendar *new)
{
  char year_str[5];
  Widget optmenu;

  if (new == (Calendar *)NULL || new->top == (Widget)NULL)
    return;

  /* Change year */
  sprintf (year_str, "%d", new->y);
  XmTextSetString (new->year, year_str);

  /* Change month menu */
  optmenu = XtNameToWidget (new->top, "daterow.monthoptmenu");
  if (optmenu) {
    Widget menu, cascade;
    WidgetList buttons;
    XmString label;

    /* Change menu history */
    XtVaGetValues (optmenu, XmNsubMenuId, &menu, NULL);
    XtVaGetValues (menu, XmNchildren, &buttons, NULL );
    XtVaSetValues (optmenu, XmNmenuHistory, buttons[new->m-1], NULL);

    /* Change cascade button label */
    cascade = XmOptionButtonGadget (optmenu); 
    XtVaGetValues (buttons[new->m-1], XmNlabelString, &label, NULL);
    XtVaSetValues (cascade, XmNlabelString, label, NULL);

    XmUpdateDisplay (cascade);
  }

  /* Change days */
  XtDestroyWidget (new->month);
  createMonth (new->top, new);
  XtManageChild (new->month);
}


static Widget createMonth (Widget parent, Calendar *this)
{
  char text[10];
  char *dayname[] = { "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa" };

  int i, j, m, tot, day;

  Widget dates[6][7];
  Widget day_widget = (Widget)NULL;

  /* Create the month form and dates pushbuttons */
  this->month = XtVaCreateWidget ("monthrow", 
                       xmRowColumnWidgetClass, parent,
                       XmNentryAlignment, XmALIGNMENT_CENTER,
                       XmNorientation, XmHORIZONTAL,
                       XmNnumColumns, 7,
                       XmNpacking, XmPACK_COLUMN,
                       NULL );

  for (day = 0; day < XtNumber(dayname); day++) 
    XtVaCreateManagedWidget (dayname[day], xmLabelWidgetClass, 
                             this->month, NULL);

  /* calculate dates of the month */
  m = day_number (this->y, this->m, 1);
  tot = days_in_month (this->y, this->m);

  /* protect against illegal dates on month change */
  if (this->d > tot)
    this->d = tot;

  for (day = i = 0; i < 6; i++) {
    for (j=0; j<7; j++, m += (j>m && --tot>0)) {
      char *name;
      if (j != m || tot < 1)
        name = "  ";
      else {
        sprintf (text, "%2d", ++day);
        name = text;
      }
  
      /* Set margin to two more than default so that when the shadow
      ** is incremented from 0 to 2 there is room to keep the button 
      ** the same size.  recomputeSize being false stops the button
      ** from shrinking/growing on shadowThickness change.
      */
      dates[i][j] = XtVaCreateManagedWidget (name, 
                 xmPushButtonWidgetClass, this->month,
                 XmNuserData,             this,
                 XmNsensitive,            (j % 7 == m && tot > 0),
                 XmNshadowThickness,      0,
                 XmNrecomputeSize,        False,
                 XmNmarginHeight,         4,
                 XmNmarginWidth,          4,
                 NULL );
      XtAddCallback (dates[i][j], XmNactivateCallback, 
                     (XtCallbackProc) dayCB, (XtPointer) day);
 
      if (tot > 0) {
        char date[11];

        if (day <= this->d)
          day_widget = dates[i][j];
        
        sprintf (date, "%d/%d/%d", this->m, day, this->y);
        accountHighlightDay (date, activeAccount(), dates[i][j]);
      }

    }
    m = 0;
  }

  XtVaSetValues (dates[5][6],
                 XmNshadowThickness, 2,
                 XmNmappedWhenManaged, False,
                 NULL);

  if (day_widget)
    dayCB (day_widget, (XtPointer)NULL, NULL); /* 2nd param denotes new month */

  return (this->month);
}


Widget calCreate (Widget parent, Calendar *this)
{
  Widget frame, toprow, daterow, optmenu, button;
  char ytext[5];

  if (this == (Calendar *)NULL || parent == (Widget)NULL)
    return (Widget)NULL;

  frame = XtVaCreateManagedWidget ("calframe",
                       xmFrameWidgetClass, parent, NULL );
  toprow = XtVaCreateWidget ("calrow", 
                       xmRowColumnWidgetClass, frame, NULL );
  daterow = XtVaCreateManagedWidget ("daterow", 
                       xmRowColumnWidgetClass, toprow, NULL );
  this->top = toprow;
  this->userMod = 0;

  /* Month */
  optmenu = XmVaCreateSimpleOptionMenu (daterow, "monthoptmenu",
                       NULL, (KeySym) NULL, this->m -1,
                       (XtCallbackProc) monthCB,
                       XmVaPUSHBUTTON, NULL, NULL, NULL, NULL,
                       XmVaPUSHBUTTON, NULL, NULL, NULL, NULL,
                       XmVaPUSHBUTTON, NULL, NULL, NULL, NULL,
                       XmVaPUSHBUTTON, NULL, NULL, NULL, NULL,
                       XmVaPUSHBUTTON, NULL, NULL, NULL, NULL,
                       XmVaPUSHBUTTON, NULL, NULL, NULL, NULL,
                       XmVaPUSHBUTTON, NULL, NULL, NULL, NULL,
                       XmVaPUSHBUTTON, NULL, NULL, NULL, NULL,
                       XmVaPUSHBUTTON, NULL, NULL, NULL, NULL,
                       XmVaPUSHBUTTON, NULL, NULL, NULL, NULL,
                       XmVaPUSHBUTTON, NULL, NULL, NULL, NULL,
                       XmVaPUSHBUTTON, NULL, NULL, NULL, NULL,
                       NULL);
  XtManageChild (optmenu);
  /* Set userData on parent of the actual buttons */
  optmenu = XtNameToWidget (daterow, "popup_monthoptmenu.monthoptmenu");
  if (optmenu)
    XtVaSetValues (optmenu, XmNuserData, this, NULL);

  /* Year */
  button = XtVaCreateManagedWidget ("yearDnarrow",
                       xmArrowButtonWidgetClass, daterow,
                       XmNarrowDirection, XmARROW_LEFT,
                       XmNuserData, this,
                       NULL );
  XtAddCallback (button, XmNactivateCallback,
                 (XtCallbackProc) yearUpDnCB, (XtPointer) -1);
  sprintf (ytext, "%4d", this->y);
  this->year = XtVaCreateManagedWidget ("yeartext",
                       xmTextFieldWidgetClass, daterow,
                       XmNvalue,    ytext,
                       XmNeditable, False,
                       XmNcursorPositionVisible, False,
                       XmNtraversalOn, False,
                       NULL );
  button = XtVaCreateManagedWidget ("yearUparrow",
                       xmArrowButtonWidgetClass, daterow,
                       XmNarrowDirection, XmARROW_RIGHT,
                       XmNuserData, this,
                       NULL );
  XtAddCallback (button, XmNactivateCallback,
                 (XtCallbackProc) yearUpDnCB, (XtPointer) +1);

  /* Days */
  createMonth (toprow, this);
  XtManageChild (this->month);

  XtManageChild (toprow);

  return (frame);
}


/* 
** Generic calendar callbacks 
*/

/* ARGSUSED */
static void dayCB (Widget day_widget, XtPointer client_data, 
                   XtPointer call_data )
{
  static Widget current_day = NULL;
  Calendar *this;
  int next_day = (int)client_data;

  /* next_day is only non-zero if we are dealing with the same month.
  ** In this case its safe to set current_day and this->d.  Otherwise 
  ** current_day may have been destroyed when we weren't looking.
  */ 

  /* User selection */
  if (next_day) {
    XtVaGetValues (day_widget, XmNuserData, &this, NULL);
    if (current_day != day_widget) {
      XtVaSetValues (current_day, XmNshadowThickness, 0, NULL ); 
      current_day = day_widget;
      XtVaSetValues (current_day, XmNshadowThickness, 2, NULL ); 
      this->d = next_day;
    }
    this->userMod = 1;

  /* Internal selection */
  } else {
    current_day = day_widget;
    XtVaSetValues (current_day, XmNshadowThickness, 2, NULL ); 
  }
}


/* ARGSUSED */
static void monthCB (Widget w, XtPointer client_data, XtPointer call_data)
{
  Calendar *this;
  int which = (int)client_data;

  XtVaGetValues (XtParent(w), XmNuserData, &this, NULL);
  if (this && which != (this->m-1)) {
    this->userMod = 1;                /* user touched */

    this->m = which+1;                /* new month */
    XtDestroyWidget (this->month);
    createMonth (this->top, this);
    XtManageChild (this->month);
  }
}

/* ARGSUSED */
static void yearUpDnCB (Widget w, XtPointer client_data, XtPointer call_data)
{
  char year_str[5];
  int dir = (int)client_data;
  Calendar *this;

  XtVaGetValues (w, XmNuserData, &this, NULL); 
  if (this == (Calendar *)NULL)
    return;

  if (dir == 1)       /* up */
    this->y++;
  else                /* down */
    this->y--;
  this->userMod = 1;  /* user touched */

  sprintf (year_str, "%d", this->y);
  XmTextSetString (this->year, year_str);

  /* New year, new month */
  XtDestroyWidget (this->month);
  createMonth (this->top, this);
  XtManageChild (this->month);
}

/* Conversion routines */

/*
**  calStrtoL - string into long number
**  Convert "MM/DD/YY", "MM/DD/YYYY",  or "MMDDYY" into # of days using
**  Jan 1, 1601 = day 1. REF: CACM 11, 657 (1968) - valid back to 24 Nov -4713
*/
long calStrtoL (char *dd)
{
#define MONTH (n[0])
#define DAY (n[1])
#define YEAR (n[2])

  static unsigned maxd[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
  char          *ddend;
  int           c = 0, slash = 0;
  long          jdn;
  int           n[3];
  static int    m;

    jdn = strtol(dd, &ddend, 10);               /* convert mmddyy */
    MONTH = jdn;

    if (*ddend == '/') {
        slash = 1;                              /* got mm/dd/yy or yyyy  */
        goto middle;
        }

    else if (*ddend) return -1;

    for (; c <= 2; c++) {
        if (slash) {
            n[c] = strtol(dd, &ddend, 10);      /* convert a number */
            if (*ddend != "//"[c])              /* check proper terminator */
baddy:          return -1;
middle:     dd = ddend + 1;
            }
        else {
            n[2 - c] = jdn % 100;
            jdn /= 100;
            }
        }

    if (YEAR <= 99) YEAR += 1900;       /* correct for c */
    maxd[2] = 28;
    if ((!(YEAR & 3) && (YEAR % 100)) || !(YEAR % 400))
        maxd[2] = 29;
    if (MONTH < 1 || MONTH > 12 || DAY < 1 || DAY > maxd[MONTH]) goto baddy;

    m = (MONTH <= 2) ? -1 : 0;
    YEAR += m;
    jdn = (long)DAY - 2337888L;
    jdn += 1461L * ((long)YEAR + 4800L) / 4L;
    jdn += 367L * (MONTH - 2L - (long)m * 12L) / 12L;
    jdn -= 3L * (((long)YEAR + 4900L) / 100L) / 4L;

    return (long)jdn;

#undef MONTH
#undef DAY
#undef YEAR
}


/* Convert long date to char (mm/dd/yyyy) date */
char *calLtoStr(long date)
{
  int month;
  int day;
  int year;

  time_t t = time((time_t *)0);
  struct tm *today = localtime (&t);

  static char try[20];

  /* int versions */
  month = 1;
  day =   1;
  year =  today->tm_year + 1900;

  sprintf (try, "%2.2d/%2.2d/%4.4d", month, day, year);
  while (calStrtoL(try) > date) {
    year--;
    sprintf (try, "%2.2d/%2.2d/%4.4d", month, day, year);
  }

  sprintf (try, "%2.2d/%2.2d/%4.4d", month, day, year);
  while (calStrtoL(try) <=  date && month <= 12 ) {
    month++;
    sprintf (try, "%2.2d/%2.2d/%4.4d", month, day, year);
  }

  month--;
  sprintf (try, "%2.2d/%2.2d/%4.4d", month, day, year);
  day = date - calStrtoL (try) +1;
  sprintf (try, "%2.2d/%2.2d/%4.4d", month, day, year);

  return (try);
}

/* Set date in Calendar from long parameter */
void calLtoCal (Calendar *this, long date)
{
  char *strdate = calLtoStr(date);

  sscanf (strdate, "%d/%d/%d", &(this->m), &(this->d), &(this->y));
}

/* Set date in Calendar from long parameter */
long calCaltoL (Calendar *this)
{
   char datebuf[15];

   sprintf (datebuf, "%d/%d/%d", this->m, this->d, this->y);
   return (calStrtoL(datebuf));
}


/* Convert calendar string to localized one */
char *calPrint (char *date)
{
  static char strday[20];
  struct tm day;

  /* Convert to internationalized format */
  strptime (date, "%m/%d/%Y", &day);
  strftime (strday, 20, "%x", &day);
  return (strday);
}
