/*
  Xinvest is copyright 1995 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.2 $ $Date: 1997/08/09 19:41:23 $
*/

#ifndef CALENDAR_H
#define CALENDAR_H

typedef struct _calendar {
  int  m, d, y;
  Widget top;
  Widget year;
  Widget month;
  int userMod;
} Calendar;

char *calLtoStr (long);            /* convert long to string */
long calStrtoL (char *);           /* convert string to long */
void calLtoCal (Calendar *, long); /* set date in Calendar from long. */
long calCaltoL (Calendar *);       /* return long date from Calendar */

Widget calCreate (Widget, Calendar *);
void calSetDate (Calendar *);

int calUserTouched (Calendar *);
char *calPrint (char *);           /* return localized internal date string */

#endif
