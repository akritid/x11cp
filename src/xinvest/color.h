/*
  Xinvest Copyright 1995-97 Mark Buser,
  All Rights Reserved.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.1 $ $Date: 1997/07/20 18:40:44 $
*/

#ifndef COLOR_H
#define COLOR_H

/* List of colors we allocate, names used by internal routines */
enum { 
       USER0,  /* defined in app defaults */
       USER1,
       USER2,
       USER3,
       USER4,
       USER5,
       USER6,
       USER7,
       RED,    /* allocate by name, because we always need */
       GREEN, 
       GREY,
       WHITE,
       BLACK
} Colors;
#define COLOR_NUM (USER7+1)

Colormap InitColor( Widget, Colormap, Pixel * );
Colormap NewColormap( Display *, Colormap );
Colormap GetColormap();
Pixel    GetColor( int );     /* Get named color from color table */
int isColorVisual (Display *); /* Return true if visual implies color */
int isDarkPixel (Display *, Pixel);
#endif
