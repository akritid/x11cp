#include <Xm/Xm.h>
#include <Xm/DrawingA.h>

#include "drawing.h"
#include "functions.h"
#include "graph.h"
#include "pixmap.h"
#include "portfolio.h"
#include "status.h"
#include "xutil.h"

static Widget Graph;
static int last_graph = PLOT;

void setDrawingArea (Widget w)
{
  Graph = w;
}

void setLastGraph (int which)
{
  last_graph = which;
}

/* Convenience function so we don't have to have a handle to Graph. */
void drawDrawingArea ()
{
  if ( XtIsRealized (Graph) )
    redrawDrawing( Graph, (XtPointer)NULL, (XtPointer)NULL);
}

/* ARGSUSED */
void redrawDrawing (Widget Drawing,
                    XtPointer data,
                    XtPointer cbs)

{
  static Pixmap       Drawpixmap = (Pixmap )NULL;
  static Dimension    pixheight, pixwidth;

  Dimension    height, width;
  Pixel  background, foreground;
  GC     gc;

  static firsttime = 1;

  /* This may take a while */
  setBusyCursor( Graph, True );       

  if (firsttime) {
    /* Turn off drawing area widget gravity so smaller windows generate expose
    ** events.
    */
    XSetWindowAttributes attrs;
    unsigned long mask = CWBitGravity;
    attrs.bit_gravity = ForgetGravity;
    XChangeWindowAttributes ( XtDisplay (Drawing), XtWindow (Drawing),
                              mask, &attrs );
    firsttime = 0;
  }

  XtVaSetValues (Drawing, XmNunitType, XmPIXELS, NULL);
  XtVaGetValues (Drawing, XmNheight,     &height,
                          XmNwidth,      &width,
                          XmNbackground, &background,
                          XmNforeground, &foreground,
                          XmNuserData,   &gc,
                 NULL );

  /*
  ** Make or resize pixmap if necessary
  **
  */

  /* There is no pixmap */
  if ( Drawpixmap == (Pixmap) NULL ) {
    Drawpixmap = XCreatePixmap ( XtDisplay(Drawing),
                               RootWindowOfScreen( XtScreen(Drawing)),
                               width, height,
                               DefaultDepthOfScreen( XtScreen(Drawing))
    );

  /* Pixmap has different size */
  } else if ( pixheight != height ||
              pixwidth  != width ) {
    /* destroy old pixmap */
    XFreePixmap (  XtDisplay(Drawing), Drawpixmap );

    /* make a new one at adjusted size */
    Drawpixmap = XCreatePixmap ( XtDisplay(Drawing),
                               RootWindowOfScreen( XtScreen(Drawing)),
                               width, height,
                               DefaultDepthOfScreen( XtScreen(Drawing))
    );
  }

  /* 
  ** Suppress errors on expose only. 
  */
  if (pixheight == height && pixwidth == width) /* Is this a resize? */
    suppress ( (int)data == 1 );                /* Expose or user update */

  pixheight = height; pixwidth = width;

  /*  Clear pixmap with background color */
  XSetForeground ( XtDisplay (Drawing), gc, background );
  XFillRectangle ( XtDisplay (Drawing), Drawpixmap, gc,
                   0, 0, pixwidth, pixheight );

  XSetForeground ( XtDisplay (Drawing), gc, foreground );

  XSetLineAttributes( XtDisplay(Drawing), gc, 2,
                      LineSolid, CapNotLast, JoinBevel );

  /* Plot, if it was last selected */
  if ( last_graph == PLOT && getVActive() )
    plot ( Drawing, Drawpixmap, gc );

  /* Portfolio, if it was last selected */
  else if ( last_graph == PORTFOLIO && getPActive() )
    pie ( Drawing, Drawpixmap, gc );

  else {
    /*
    ** No plots selected, they get the booby prize
    **
    */
    XSetTile ( XtDisplay (Drawing), gc, GetPixmap( PSTIPPLE, NORMAL) );
    XSetFillStyle (  XtDisplay (Drawing), gc, FillTiled );
    XFillRectangle ( XtDisplay (Drawing), Drawpixmap, gc,
                     0, 0, pixwidth, pixheight );
    XSetFillStyle (  XtDisplay (Drawing), gc, FillSolid );
  }

  if ( last_graph == FUNCCALC )
    /* Just write performance data over logo background */
    draw_funccalc ( Drawing, Drawpixmap, gc );

  /* All errors enabled */
  suppress (False);  

  XCopyArea ( XtDisplay(Drawing), Drawpixmap, XtWindow (Drawing),
              gc, 0, 0, pixwidth, pixheight, 0, 0 );

  setBusyCursor( Graph, False );
}
