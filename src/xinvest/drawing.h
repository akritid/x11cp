/*
  Xinvest is copyright 1995 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.1 $ $Date: 1996/06/04 22:53:29 $
*/

/* defines for last use of drawing area widget */
#define NOTHING       0
#define PLOT          1
#define PORTFOLIO     2
#define RETURN        3
#define FUNCCALC      4

void setLastGraph (int which);    /* Set the graph function selected */
void setDrawingArea( Widget );    /* What widget do we draw into */
void drawDrawingArea ();          /* Refresh or change what's drawn, non-CB */
/* drawing area callback */
void redrawDrawing ();            /* Refresh or change what's drawn */
