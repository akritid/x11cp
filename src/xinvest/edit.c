/*
  Xinvest is copyright 1996 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.

  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.

  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.2 $ $Date: 1997/01/13 22:46:19 $
*/

#include <Xm/PushB.h>
#include <Xm/Text.h>

#include "status.h"

/* ARGSUSED */
void editCB( Widget w, XtPointer client_data, XtPointer call_data)
{
  int result = True;
  int reason = (int) client_data;
  XEvent *event = ((XmPushButtonCallbackStruct *) call_data)->event;
  Time when;

  extern Widget Transtext;

  if (event != NULL) {
    switch (event->type) {
      case ButtonRelease:
                            when = event->xbutton.time;
                            break;
      case KeyRelease:
                            when = event->xkey.time;
                            break;
      default:
                            when = CurrentTime;
                            break;
    }
  }

  switch (reason) {
    case 0:
             result = XmTextCopy (Transtext, when);
             break;
    case 1:
             result = XmTextCut (Transtext, when);
             break;
    case 2:
             result = XmTextPaste (Transtext);
             break;
    default:
             break;
  }
  
  if (result == False)
    write_status ("Edit failed, no selection?\n"
                  "Source or destination must be\n"
                  "transaction window.", ERR);
}

/* ARGSUSED */
void editSelectionCB( Widget w, XtPointer client_data, 
                  XmAnyCallbackStruct *call_data)
{
  extern Widget Editmenu;
  Widget cut, copy;
  XmTextPosition left, right;

  int enable = False;

  switch (call_data->reason ) {
    case XmCR_GAIN_PRIMARY:
         enable = True;
         break;
    case XmCR_LOSE_PRIMARY:
         enable = False;
         break;
    case XmCR_MOVING_INSERT_CURSOR:
         if (XmTextGetSelectionPosition (w, &left, &right))
           if (left != right)
             enable = True;
         break;
  }

  cut = XtNameToWidget ( Editmenu, "button_0");
  if (cut)
    XtSetSensitive ( cut, enable );
  copy = XtNameToWidget ( Editmenu, "button_1");
  if (copy)
    XtSetSensitive ( copy, enable );
  
}
