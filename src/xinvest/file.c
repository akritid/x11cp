/*
  Xinvest is copyright 1995,1996 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.12 $ $Date: 1997/08/16 20:52:42 $
*/

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include <Xm/Xm.h>
#include <Xm/Label.h>
#include <Xm/List.h>
#include <Xm/RowColumn.h>
#include <Xm/Text.h>
#include <Xm/FileSB.h>

#include "account.h"
#include "askuser.h"
#include "drawing.h"
#include "trans.h"
#include "status.h"
#include "util.h"
#include "xutil.h"

extern Widget Filecwdlabel;
extern Widget Listfile;
extern Widget Transtext;

static char prompt[BUFSIZ];
static char err_msg[BUFSIZ];

/* forward declarations */
static void newfile ( );
       int  loadfile ( char *);
static void writefile ( char *);
static void saveAsfileCB ( Widget, XtPointer, XtPointer );


/* ARGSUSED */
static void loadfileselect ( Widget w, 
                XtPointer client_data, 
                XtPointer call_data )
{

  char *filename;
  char *directory;
  char *last;
  char *file_or_dir = NULL;
  int  account = 0;

  XmFileSelectionBoxCallbackStruct *cbf = 
     (XmFileSelectionBoxCallbackStruct *) call_data;

  XmListCallbackStruct *cbl = 
     (XmListCallbackStruct *) call_data;

  /* Determine max number of characters in a path from root and allocate 
  ** enough space to hold it. */
  int num = path_length();

  if (file_or_dir == NULL) {
     file_or_dir = XtCalloc ( num, sizeof(char) );
     if (file_or_dir == NULL) {
       write_status ("Could not allocate memory for path.\n"
                     "File not opened.", ERR);
       return;
     }
  }

  if ( XmIsFileSelectionBox(w) ) {

    /* Break into directory and filename components */

    XmStringGetLtoR ( cbf->dir, XmFONTLIST_DEFAULT_TAG, &directory );
    loadfile ( directory );
    XtFree ( directory );

    XmStringGetLtoR ( cbf->value, XmFONTLIST_DEFAULT_TAG, &filename );
    /* strip path components if any */
    if ( (last = strrchr ( filename, '/')) )
      strcpy( file_or_dir, last+1 );
    XtFree ( filename );

    XmFileSelectionDoSearch ( w, cbf->dir );
    XtUnmanageChild ( w );

  } else { 

    /* last part of path comes from list window */
    XmStringGetLtoR ( cbl->item, XmFONTLIST_DEFAULT_TAG, &filename );

#if 0
    /* remainder from the system */
    if ( getcwd( file_or_dir, BUFSIZ ) ) {
      strcat ( file_or_dir, "/" );
      strcat ( file_or_dir, filename );  
    }
#endif
    strcpy ( file_or_dir, filename );
    XtFree ( filename );
  }

  /* open file and read transactions */
  if (file_or_dir[0])
    account = loadfile ( file_or_dir ); 

  /* Process transactions, compute return, draw display */
  if ( account ) {
    processAccount ( account );
    displayAccount ( account );
    drawDrawingArea( );
  }

#if 0
  if ( XmIsFileSelectionBox(w) ) {
    XmFileSelectionDoSearch ( w, cbf->dir );
    XtUnmanageChild ( w );
  }
#endif
}

/* Compare two strings when given an indirect pointer to the strings. 
** Used for qsort below. 
*/
static int indstrcmp ( const char **a, const char **b)
{
  return ( strcasecmp ( *a, *b ) );
}

int loadfile ( char *choice )
{

  static char *path = NULL;
  FILE *file;
  char buffer[BUFSIZ];
  DIR  *dirp;
#ifdef NeXT
  struct direct *dp;
#else
  struct dirent *dp;
#endif
  struct stat   statb;

  char **dir_list = NULL;
  char **file_list = NULL;
  int  files, dirs, i;

  char *cp, *text;
  char buf_dir[80] = "File Manager ";

  XmString dir_str;

  if (stat ( choice, &statb )  != 0 ) {
    sprintf ( err_msg, "Couldn't query file:\n\"%s\"", choice);
    write_status (err_msg, ERR);
    return( (int)NULL );
  }

  if ( S_ISDIR( statb.st_mode ) ) {
    /* open directory and list choices */
    dirp = opendir( choice );
    if ( dirp == NULL ) {
      sprintf ( err_msg, "Couldn't open directory:\n\"%s\"", choice);
      write_status (err_msg, ERR);
      closedir ( dirp );
      return( (int)NULL );
    }
    if ( (chdir ( choice )) == -1) {
      sprintf ( err_msg, "Couldn't change to directory:\n\"%s\"", choice);
      write_status (err_msg, ERR);
      closedir ( dirp );
      return( (int)NULL );
    }
      
    /* Set frame title to cwd */
    cp = getcwd( buffer, BUFSIZ );
    if (cp) {
      if ( strlen(cp) > 25)  {   /* truncate from beginning of path */
        cp += strlen(cp) -22;    /* 30 - "..." */
        strcat( buf_dir, "..."); /* display this to show we truncated path */
      }
      strcat( buf_dir, cp);
      dir_str = XmStringCreateLocalized ( buf_dir );

      XtVaSetValues ( Filecwdlabel,
                      XmNlabelString, dir_str,
                      NULL );
      XmStringFree ( dir_str );
    }

    /* read directory list */
    files = 0; dirs = 0;
    for( dp = readdir(dirp); dp != NULL; dp = readdir(dirp)) {

       /* Get status, just skip if we can't stat */
       if (stat ( dp->d_name, &statb )  != 0 ) {
         sprintf ( err_msg, "Couldn't query file:\n\"%s\"", dp->d_name);
         write_status ( err_msg, WARN );
         continue;          /* skip bad files, dangling link? */
       }
       strcpy( buffer, dp->d_name);

       /* check if directory */
       if ( S_ISDIR( statb.st_mode ) )  {
         strcat( buffer, "/");
         /* Don't sort . and .. */
         if ( (strcmp ( buffer, "./") != 0) &&
               (strcmp ( buffer, "../") != 0) ) {
            dir_list = (char **)XtRealloc ( (char *)dir_list, 
                                            sizeof (char *) * (dirs+1) );
            dir_list[dirs] = XtRealloc (NULL, sizeof(char)*(strlen(buffer)+1) );
            strcpy ( dir_list[dirs++], buffer );
         }
       } else {
          file_list = (char **)XtRealloc ( (char *)file_list, 
                                           sizeof (char *) * (files+1) );
          file_list[files] = XtRealloc (NULL, sizeof(char)*(strlen(buffer)+1) );
          strcpy ( file_list[files++], buffer );
       }

    }
    closedir( dirp );

    /* Sort the list */
    qsort ( dir_list, dirs, sizeof (char *), (int (*)() )indstrcmp );

    qsort ( file_list, files, sizeof (char *), (int (*)() )indstrcmp );

    /* clear window */
    XmListDeleteAllItems( Listfile );

    /* dirs go in list first */
    dir_str = XmStringCreateLocalized ("./");
    XmListAddItemUnselected( Listfile, dir_str, 0);
    XmStringFree (dir_str);
    dir_str = XmStringCreateLocalized ("../");
    XmListAddItemUnselected( Listfile, dir_str, 0);
    XmStringFree (dir_str);

    for (i=0; i < dirs; i++) {
      XmString dir_str = XmStringCreateLocalized(dir_list[i]);
      XmListAddItemUnselected( Listfile, dir_str, 0 );
      XmStringFree ( dir_str );
      XtFree ( dir_list[i] );
    }
    XtFree ( (char *)dir_list );

    /* followed by all other files */
    for (i=0; i < files; i++) {
      XmString dir_str = XmStringCreateLocalized(file_list[i]);
      XmListAddItemUnselected( Listfile, dir_str, 0 );
      XmStringFree ( dir_str );
      XtFree ( file_list[i] );
    }
    XtFree ( (char *)file_list );

    return ( (int)NULL );

  } else { /* filename selected */

    int num = path_length();

    if (path == NULL) {
       path = XtCalloc ( num, sizeof(char) );
       if (path == NULL) {
         write_status ("Could not allocate memory for path.\n"
                       "File not added.", ERR);
         return ( (int)NULL );
       }
    }

    if ( choice[0] != '/' ) {        /* Not full path prepend CWD */
      if ( getcwd( path, num ) ) {
        strcat ( path, "/" );
        strcat ( path, choice );
      } else {
        sprintf (err_msg, "Path longer than maximum (%d),\nfile not added.", 
	         num);
        write_status ( err_msg, ERR);
        return ( (int)NULL );
      }
    } else 
      strcpy ( path, choice );

    /* Is this account already open */
    if ( (num = accountIsOpen ( path )) ) {
      sprintf ( err_msg, "File \"%s\" is already open, reload?", choice);
      if ( AskUser (GetTopShell(Transtext), err_msg, "OK", "Cancel", YES) == NO)
        return ( num );
    }
 
    /* Is the file RO? */
    if ( !(statb.st_mode & S_IWUSR) ) {
      sprintf ( err_msg, "File \"%s\" is read only.", choice);
      write_status ( err_msg, INFO );
    }

    /* Open the file */
    if ( ( file = fopen ( choice, "r") ) == NULL ) {
      sprintf( err_msg, "Couldn't open file:\n\"%s\".", choice );
      write_status ( err_msg, ERR );
      return ( (int)NULL ); 
    }

    /*  Allocate space and read transactions */
    if (!(text = XtMalloc ( (unsigned) (statb.st_size + 1) ))) {
      sprintf( err_msg, "Can't allocate memory to hold file:\n\"%s\".", 
               choice );
      write_status ( err_msg, ERR );
      fclose ( file );
      return( (int)NULL );
    }

    if (statb.st_size && !fread ( text, sizeof (char), statb.st_size, file) ) {
      sprintf( err_msg, "Did not read entire file:\n\"%s\".", choice);
      write_status ( err_msg, WARN );
    }
    text[statb.st_size] = 0;  /* NULL terminate */ 

    fclose(file);

    /* Set up new account (delete old if exists) */
    num = newAccount (path, text, statb.st_mode & S_IWUSR);
    
    return (num);

  }
}

static void writefile ( char *dir ) 
{

  FILE          *fp;
  char          *text;
  int           len;


  if ( ( fp = fopen ( dir, "w") ) == NULL ) {
    sprintf( err_msg, "Couldn't open file:\n\"%s\".", dir );
    write_status ( err_msg, ERR );
    return;
  }

  /* Write File */
  text = XmTextGetString (Transtext);
  len = XmTextGetLastPosition (Transtext);

  fwrite( text, sizeof(char), len, fp);
  if (text[len-1] != '\n')
    fputc('\n', fp);

  fclose(fp);
  XtFree(text);
}

/*
** Callback routines 
*/

#define BAKEXT ".bak"

/* Callback for file menu and quit button */
void fileCB (Widget w, int client_data, XmAnyCallbackStruct *call_data )
{
  extern void toolCB ( Widget, XtPointer, XtPointer);

  static Widget dialog;
  int item_no = (int) client_data;

  char *filename;
  char *savefilename;
  char *insert;

  /* Make a file selection dialog if we don't have one */
  if (!dialog) {
    dialog = XmCreateFileSelectionDialog (GetTopShell(Transtext), "Filesel", 
                                          NULL, 0);
    XtSetSensitive (XmFileSelectionBoxGetChild (dialog, XmDIALOG_HELP_BUTTON),
                    False);
    XtAddCallback (dialog, XmNcancelCallback, 
                   (XtCallbackProc) XtUnmanageChild, (XtPointer) NULL);
  }

  /* Remove all callbacks.  Easier than remembering what was added last. */
  XtRemoveCallback( dialog, XmNokCallback, 
                    (XtCallbackProc)saveAsfileCB, (XtPointer) NULL);
  XtRemoveCallback( dialog, XmNokCallback, 
                    (XtCallbackProc)loadfileselect, (XtPointer) NULL);

  if (item_no == 0)   /* the new button */
    newfile( );

  else if (item_no == 1) { /* Load button */

    if ( XmIsList(w) )
      loadfileselect (w, (XtPointer)client_data, (XtPointer)call_data);

    else {
      XtAddCallback( dialog, XmNokCallback, 
                     (XtCallbackProc)loadfileselect, (XtPointer) NULL);
      XtManageChild(dialog);
      XtPopup( XtParent(dialog), XtGrabNone);
    }
  }

  else if (item_no == 2) {  /* Close */
    closeAccount ( activeAccount() );

    if ( activeAccount() == 0)
      /* Pretend "about" button has been pushed.  This clears any remnants of
      ** old plot data and calls redrawDrawing.
      */
      toolCB ( (Widget)NULL, (XtPointer)0, (XtPointer)NULL );
  }

  else if (item_no == 3) {  /* Save */
    int account = activeAccount();

    if ( !accountStatus (account, ACCOUNT_WRITABLE) ) {
      write_status ("Can not save into read only file!", ERR);
      return;
    }
   
    /* Get last filename from the account */
    getAccount ( account, ACCOUNT_FILENAME, &filename );

    strcpy ( prompt, "Ok to overwrite file ");
    strcat ( prompt, filename );
    if ( AskUser (GetTopShell(Transtext), prompt, "OK", "Cancel", YES) == NO )
      return;

    /* Move orig file to .bak */
    savefilename = (char *) XtCalloc (strlen(filename) + strlen(BAKEXT) +1, 
                                      sizeof(char));
    strcpy ( savefilename, filename );
    if ( (insert = strrchr ( savefilename, '.')) != NULL ) 
      strcat ( insert, BAKEXT);
    else
      strcat ( savefilename, BAKEXT);
    if (rename ( filename, savefilename )) {
      sprintf ( err_msg, 
                "Changes not saved, couldn't create backup file:\n\"%s\"", 
                savefilename);
      write_status ( err_msg, ERR);
      XtFree ( savefilename );
      return;
    }
    XtFree ( savefilename );

    writefile ( filename ); 

    accountClean ( account );
    displayAccount ( account );

  }

  else if (item_no == 4) { /* Save As */
    XtAddCallback( dialog, XmNokCallback, 
                   (XtCallbackProc)saveAsfileCB, (XtPointer) NULL);
    XtManageChild(dialog);
    XtPopup( XtParent(dialog), XtGrabNone);
  }

  else if (item_no == 5) {     /* the quit button */
    if ( accountsStatus(ACCOUNT_DIRTY) ) {
      strcpy ( prompt, "At least one account has been changed, quit anyway?");
      if ( AskUser (GetTopShell(Transtext), prompt, "OK", "Cancel", YES) == NO )
        return;
    }

#if XtSpecificationRelease > 5
    /* close session connection before quitting */
    XtVaSetValues ( GetTopShell(w), XtNjoinSession, False, NULL);
    XtCallCallbacks ( GetTopShell(w), XtNdieCallback, NULL);
#endif

    exit (0);
  }

}
 
/* ARGSUSED */
static void saveAsfileCB ( Widget w, XtPointer client_data, 
                           XtPointer call_data )
{
  char *filename;
  char *savefilename;
  char *insert;

  struct stat   statb;
  int           status;

  int   account;

  XmFileSelectionBoxCallbackStruct *cbs = 
     (XmFileSelectionBoxCallbackStruct *) call_data; 

  /* Get filename from fileselect widget */
  if ( !XmStringGetLtoR ( cbs->value, XmFONTLIST_DEFAULT_TAG, &filename ))
    return;

  /* Check if exists */
  status = stat ( filename, &statb );

  if ( status != -1 ) {

    if ( S_ISDIR( statb.st_mode ) ) 
      /* Directory, rescan */
      XmFileSelectionDoSearch ( w, cbs->value );

    else {  /* must exist */

      if ( !(statb.st_mode & S_IWUSR) ) {
        write_status ("Can not save into read only file!", ERR);
        return;
      }
   
      /* If so, popup to confirm the write */
      strcpy ( prompt, "Ok to overwrite file ");
      strcat ( prompt, filename );
      strcat ( prompt, "?");
      if ( AskUser (GetTopShell(Transtext), prompt, "OK", "Cancel", YES) == NO )
        return;

      /* Move orig file to .bak */
      savefilename = (char *) XtCalloc (strlen(filename) + strlen(BAKEXT) +1, 
                                        sizeof (char ) );
      strcpy ( savefilename, filename );
      if ( (insert = strrchr ( savefilename, '.')) != NULL ) 
        strcat ( insert, BAKEXT);
      else
        strcat ( savefilename, BAKEXT);
      if (rename ( filename, savefilename )) {
        sprintf ( prompt, 
                  "Changes not saved, couldn't create backup file:\n\"%s\"", 
                  savefilename);
        write_status ( err_msg, ERR);
        XtFree ( savefilename );
        return;
      }
      XtFree ( savefilename );

    }
  }

  /* new file or OK to open for write */
  writefile ( filename ); 

  /* hmmm, what to do?  The original is dirty or its a new account */
  account = activeAccount();
  getAccount ( account, ACCOUNT_FILENAME, &savefilename );
  if ( savefilename == NULL) {
    /* new account */
    setAccount ( account, ACCOUNT_FILENAME, filename );
  }
  accountClean ( account );
  displayAccount ( account );

  XtUnmanageChild ( w ); 
}

#undef BAKEXT

static void newfile ( )
{
  extern void toolCB ( Widget, XtPointer, XtPointer);

  int nextaccount;
  char  newtext[] = { 
          "#title NONAME\n"
          "#ticker NONAME\n"
          "#\n"
          "#Date\t\tTransaction\tPrice\tShares\t[Fee[%]]\n" 
  };
  /* A freeable text string */
  char *text = strdup (newtext);

  
  /* No remembered filename, just transaction text, assume writable */
  nextaccount = newAccount ( NULL, text, True ); 
  if (nextaccount) {
    processAccount ( nextaccount );
    displayAccount ( nextaccount );

    /* Pretend "about" button has been pushed.  This clears any remnants of
    ** old plot data and calls redrawDrawing.
    */
    toolCB ( (Widget)NULL, (XtPointer)0, (XtPointer)NULL );
    drawDrawingArea( );
  }

}
