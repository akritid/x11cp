/*
  Xinvest is copyright 1995 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.9 $ $Date: 1997/01/13 22:46:19 $
*/

#include <stdlib.h>
#include <ctype.h>
#include <math.h>

#include <Xm/Label.h>
#include <Xm/Text.h>

#include "color.h"
#include "drawing.h"
#include "pixmap.h"
#include "status.h"
#include "xutil.h"

#define PV   0
#define FV   1
#define RATE 2
#define CMPD 3
#define PRDS 4
#define PAY  5
#define NUM_TERMS 6

#define FUNC_FV       0
#define FUNC_FVACC    1
#define FUNC_PERPAY   2
#define FUNC_PV       3
#define FUNC_RATE     4

extern Widget  Calclabels[];
extern Widget  Calctext[];

extern char  *calcparams[];

static double  num_ok[] = { 0, 0, 0};
static int     curr_func =  FUNC_FV;
static int     last_compound = 2;
static char    result_str[150];

static void update_result_text(double result, int func) {

  char *compound_str[] = {"daily", "weekly", "monthly", "quarterly", 
                          "semiannually", "annually", "180/365", "360/365" };

  /* space at end of every string for draw_funccalc to find */
  char *format_str[] = { 
    "%s at %s%% compounded %s for %s years is %s. ",
    "Installments of %s at %s%% compounded %s for %s years is %s. ",
    "The installments for %s principal at %s%% paid %s for %s years is %s. ",
    "To reach %s assuming %s%% compounded %s for %s years, requires %s now. ",
    "For %s to grow to %s compounded %s for %s years requires a %s%% "
     "rate of return. "
  };

  char *vars_str[5][4] = { 
    {"PRESENT VALUE", "RATE",         "YEARS", "RESULT"},
    {"PAYMENT",       "RATE",         "YEARS", "RESULT"},
    {"PRESENT VALUE", "RATE",         "YEARS", "RESULT"},
    {"FUTURE VALUE",  "RATE",         "YEARS", "RESULT"},
    {"PRESENT VALUE", "FUTURE VALUE", "YEARS", "RESULT"}
  };

  char var[4][15]; 

  int  i;

  /* Strings for the user params */
  for (i=0; i < 3; i++) {
    if ( num_ok[i] == 0 )
      strcpy( var[i], vars_str[func][i] );
    else {
#ifdef STRFMON
      if ( strcmp(vars_str[func][i], "RATE") == 0 ||
           strcmp(vars_str[func][i], "YEARS") == 0 )
         strfmon( var[i], 15, "%!.2n", num_ok[i] );
      else
         strfmon( var[i], 15, "%.2n", num_ok[i] );
#else
      if ( strcmp(vars_str[func][i], "RATE") == 0 ||
           strcmp(vars_str[func][i], "YEARS") == 0 )
        sprintf( var[i], "%.2f", num_ok[i] );
      else
        sprintf( var[i], "$%.2f", num_ok[i] );
#endif
    }
  }

  /* String for the result */
  if ( result == 0)
    strcpy( var[3], vars_str[func][3] );
  else
#ifdef STRFMON
    /* Equation 4 result is rate, leave off currency symbol. */
    strfmon( var[3], 15, (func==4)?"%!.2n":"%.2n", result );
#else
    sprintf( var[3], (func==4)?"%.2f":"$%.2f", result );
#endif

  sprintf ( result_str, format_str[ curr_func ], var[0], var[1], 
            compound_str[last_compound], var[2], var[3] );

  drawDrawingArea();
}

void draw_funccalc ( Widget Drawing, Pixmap Drawpixmap, GC gc)
{
  char *title[] = {"Future Value of a Lump Sum ", 
                   "Future Value of Installments ", 
                   "Periodic Payment of Principal ", 
                   "Present Value of a Future Amount ", 
                   "Rate for Present to Future Value "};

  static Pixmap graypm = (Pixmap)NULL;
#define gray_width 16
#define gray_height 16
  static unsigned char gray_bits[] = {
   0x99, 0x99, 0xAA, 0xAA, 0x66, 0x66, 0x55, 0x55,
   0x99, 0x99, 0xAA, 0xAA, 0x66, 0x66, 0x55, 0x55,
   0x99, 0x99, 0xAA, 0xAA, 0x66, 0x66, 0x55, 0x55,
   0x99, 0x99, 0xAA, 0xAA, 0x66, 0x66, 0x55, 0x55};

  Dimension width, height;
  Pixel foreground, background;
  extern XFontStruct *large;
  int x, y; 
  int boxX, boxWidth;

  Pixel top_shadow, bot_shadow, box_shadow;
  Pixel curcolor, nextcolor;
  int colorVisual = isColorVisual (XtDisplay(Drawing));

  char *nextchar;
  size_t numchars; 
  int lines, curLine, *lineChars = NULL;
  int textWidth, textHeight;

  int i, numnumbers = 0;
  int indigit = 0;

  XtVaGetValues ( Drawing, XmNwidth, &width, 
                           XmNheight, &height,
                           XmNforeground, &foreground,
                           XmNbackground, &background,
                  NULL);

  if ( colorVisual ) {
    XmGetColors ( XtScreen (Drawing), GetColormap(), background,
                  NULL, &top_shadow, &bot_shadow, NULL );
    box_shadow = bot_shadow;
  } else {
    top_shadow = background;  /* Don't care in MONO visual */
    bot_shadow = background;
    box_shadow = GetColor (BLACK);
  }

  if ( graypm == (Pixmap)NULL ) {
    graypm = XCreateBitmapFromData( XtDisplay (Drawing),
                                    RootWindowOfScreen(XtScreen( Drawing)),
                                    (char *) gray_bits,
                                    gray_width, gray_height);
    XSetStipple( XtDisplay (Drawing), gc, graypm );
  }

  /* 
  ** Define size and location of box holding text.
  */

  /* Start box half way into 25% of total margin area */
  boxX = (0.25/2)*width;
  /* Limit box to 75% of drawing area. */
  boxWidth = 0.75 * width;

  /* 
  ** How many lines of text 
  */
  XSetFont ( XtDisplay (Drawing), gc, large->fid );

  /* How many chars will fit in 95% of the box width with word wrap. */
  lines = 0;

  /* Title */
  nextchar = title[curr_func];
  numchars = strlen(nextchar);
  textWidth = XTextWidth ( large, nextchar, numchars);
  while ( (numchars && !isspace( nextchar[numchars] ) ) || 
          textWidth > 0.95*boxWidth )
    textWidth = XTextWidth ( large, nextchar, numchars-- );
  lineChars = (int *) XtRealloc ( (char *)lineChars, sizeof(int)*(lines+1));
  lineChars[lines++] = numchars;

  /* Other text */
  nextchar = result_str;
  numchars = strlen(nextchar);

  while ( numchars ) {
    textWidth = XTextWidth ( large, nextchar, numchars);
    while ( (numchars && !isspace( nextchar[numchars] ) ) || 
            textWidth > 0.95*boxWidth )
      textWidth = XTextWidth ( large, nextchar, numchars-- );

    lineChars = (int *) XtRealloc ( (char *)lineChars, sizeof(int)*(lines+1));
    lineChars[lines++] = numchars;

    nextchar += (numchars +1);
    numchars = strlen(nextchar);
  }
  textHeight = large->ascent + large->descent;

  /* Where to draw the box */
  x = boxX;
  y = (int) (height - textHeight*lines) / 2;

  /*
  ** Break up the result string into lines that will fit.  Back off
  ** so that one word does not cross lines.  Draw each character of a line
  ** one at a time.  Use apriori info to detect unspecified data (all CAPS), 
  ** specified data (digits), and the result (last digits).  Specify each
  ** in its own color to highlight.
  */

  /* For each line */
  for (curLine = 0; curLine < lines; curLine++, y+= textHeight) {

    if (curLine == 0)
      nextchar = title[curr_func];
    else if (curLine == 1)
      nextchar = result_str;
    else
      nextchar += lineChars[curLine-1] +1;

    textWidth = XTextWidth ( large, nextchar, lineChars[curLine] );
    /* Center this line in box */
    x = (boxWidth - textWidth) /2 + boxX;

    /* Draw the shadow and box */
    XSetFillStyle( XtDisplay (Drawing), gc, FillSolid );
    XSetForeground ( XtDisplay (Drawing), gc, box_shadow );
    XFillRectangle ( XtDisplay (Drawing), Drawpixmap, gc,
                     boxX +2, y +2, boxWidth, textHeight );
    XSetForeground ( XtDisplay (Drawing), gc, background );
    XFillRectangle ( XtDisplay (Drawing), Drawpixmap, gc,
                     boxX, y, boxWidth, textHeight );
    XSetForeground ( XtDisplay (Drawing), gc, foreground );
    curcolor = foreground;

    for (i = 0; i < lineChars[ curLine ]; i++) {

      /* Reset at start of each line */
      if (i == 0) {
        nextcolor = foreground;
        indigit = 0;
      }

      /* Found a specified value.  If we're the result, print in different
      ** color than other digits. */
      if ( isdigit (nextchar[i]) ) {
        indigit = 1;
        XSetFillStyle( XtDisplay (Drawing), gc, FillSolid );

        if (numnumbers == 3) 
          nextcolor = bot_shadow;      /* result in dark */
        else {
          if (colorVisual)
            nextcolor = top_shadow;    /* User supplied info in light */
          else
            nextcolor = foreground;    /* Monochrome in foreground */
        }
      }

      /* Found a space, revert to foreground */
      if ( isspace (nextchar[i]) ) {
        XSetFillStyle( XtDisplay (Drawing), gc, FillSolid );
        nextcolor = foreground;
        /* If we're a space after a number then increment number of numbers. */
        if (indigit)
          numnumbers++;
        indigit = 0;
      }

      /* Found an unspecified value - dark stipple */
      if ( isupper(nextchar[i]) && isupper(nextchar[i+1]) ) {
        indigit = 0;
        XSetFillStyle( XtDisplay (Drawing), gc, FillStippled );
        if (colorVisual)
          nextcolor = bot_shadow;
        else
          nextcolor = foreground;
      }

      /* Found a /. Compounding will be detected as a number, compensate */
      if ( nextchar[i] == '/' )
        numnumbers--;

      /* Only change GC on change of color */
      if (nextcolor != curcolor) {
        curcolor = nextcolor;
        XSetForeground ( XtDisplay (Drawing), gc, curcolor );
      }

      if (!colorVisual && indigit && numnumbers == 3) {
        /* Draw result text in reverse video */
        XSetForeground ( XtDisplay(Drawing), gc, background );
        XSetBackground ( XtDisplay(Drawing), gc, foreground );
        XDrawImageString ( XtDisplay (Drawing), Drawpixmap, gc,
                         x, y + large->ascent, nextchar+i, 1 );
        XSetForeground ( XtDisplay(Drawing), gc, foreground );
        XSetBackground ( XtDisplay (Drawing), gc, background );
      } else {
        /* No 3d if mono or we're a stipple or result */
        if (colorVisual && curcolor != bot_shadow) {
          XSetForeground ( XtDisplay (Drawing), gc, bot_shadow );
          XDrawString ( XtDisplay (Drawing), Drawpixmap, gc,
                        x +1, y + large->ascent +1, nextchar+i, 1 );
        }
        XSetForeground ( XtDisplay (Drawing), gc, curcolor );
        XDrawString ( XtDisplay (Drawing), Drawpixmap, gc,
                      x, y + large->ascent, nextchar+i, 1 );
      }

      /* There may not be a space after a digit */
      if (i == numchars-1 && indigit) 
        numnumbers++;

      x += XTextWidth ( large, nextchar+i, 1);
    }

  }

  XtFree ( (char *) lineChars );
  XSetFillStyle( XtDisplay (Drawing), gc, FillSolid );
  XSetForeground ( XtDisplay (Drawing), gc, foreground );
}

/* Given all parameters and the function type, give up the result */
static void calc_result()
{
  float  compound[] = { 365, 52, 12, 4, 2, 1, 365/180, 365/360};
  double payment, periods, pv, fv, rate, compounding;
  double result_num = 0.0;

  double r, n;

  extern int curr_func;
  extern int last_compound;

  switch (curr_func) {

    /* Operands follow the position convention in calcFuncSel */
    case FUNC_FV:      /* Compounded interest for a lump sum */
                       pv = num_ok[0];
                       rate = num_ok[1]/100.0;
                       periods = num_ok[2]; /* years */
                       compounding = compound[last_compound];
  
                       r = rate / compounding;
                       n = periods * compounding;

                       result_num = pv * pow ( 1+r, n);
       
                       break;
    case FUNC_FVACC:   /* FV of equal installment payments over time */
                       payment = num_ok[0];
                       rate =    num_ok[1]/100.0;
                       periods = num_ok[2]; /* years */
                       compounding = compound[last_compound];
  
                       r = rate / compounding;

                       /* Start at time 0 (last payment) */
                       for (n=0; n < periods*compounding; n++) {
                         result_num += payment * pow ( 1+r, n );
                       }

                       break;
    case FUNC_PERPAY:  /* your basic loan calculation */
                       pv =      num_ok[0];
                       rate =    num_ok[1]/100.0;
                       periods = num_ok[2];
                       compounding = compound[last_compound];
                     
                       r = rate/compounding;
                       n = periods*compounding;
                 
                       result_num = pv / ( (1.0 - pow ( 1+r, -n )) / r);
                  
                       break;
    case FUNC_PV:      /* Present value of a future sum */
                       payment = num_ok[0];
                       rate =    num_ok[1]/100.0;
                       periods = num_ok[2];
                       compounding = compound[last_compound];

                       r = rate/compounding;
                       n = periods * compounding;

                       result_num = payment / pow ( 1+r, n );

                       break;
    case FUNC_RATE:
                       pv = num_ok[0];
                       fv = num_ok[1];
                       periods = num_ok[2];
                       compounding = compound[last_compound];

                       n = periods*compounding;

                       /* take nth root of fv/pv */
                       result_num = pow ( fv/pv, 1.0/n );
                       result_num -= 1;

                       result_num *= 100.0; /* back to percent */

                       break;
  }

  update_result_text ( (double)result_num, curr_func );
  
}

/* ARGSUSED */
void calcCompSel ( Widget w, int client_data, XmAnyCallbackStruct *call_data )
{
  extern int last_compound;
  int i;

  last_compound = client_data;

  update_result_text ( (double)0.0, curr_func );

  for (i=0; i < 3; i++)
    if (num_ok[i] == 0)
      return;

  calc_result();
}

/* 
** We've switched functions from the option menu, change param labels if
** necessary. 
*/

/* ARGSUSED */
void calcFuncSel ( Widget w, int client_data, XmAnyCallbackStruct *call_data )
{
  XmString terms[ NUM_TERMS ];
  int i;

  extern Widget Calclabel;

  for (i=0; i < XtNumber(terms); i++) 
    terms[i] = XmStringCreateLocalized( calcparams[i] );

  switch (client_data) {

    case FUNC_FV:     /* Number of compounding terms */
                      /* Need PV, rate, periods compounding */
                      XtVaSetValues ( Calclabels[0], 
                                      XmNlabelString, terms[PV], 
                                      NULL );
                      XtVaSetValues ( Calclabels[1], 
                                      XmNlabelString, terms[RATE], 
                                      NULL );
                      XtVaSetValues ( Calclabels[2], 
                                      XmNlabelString, terms[PRDS], 
                                      NULL );
                      XtVaSetValues ( Calclabel, 
                                      XmNlabelPixmap, GetPixmap(PFV, NORMAL),
                                      NULL);
                      break;
    case FUNC_FVACC:  /* Future value of series of payments */
                      /* Need Payments, rate, payment periods */
                      XtVaSetValues ( Calclabels[0], 
                                      XmNlabelString, terms[PAY], 
                                      NULL );
                      XtVaSetValues ( Calclabels[1], 
                                      XmNlabelString, terms[RATE], 
                                      NULL );
                      XtVaSetValues ( Calclabels[2], 
                                      XmNlabelString, terms[PRDS], 
                                      NULL );
                      XtVaSetValues ( Calclabel, 
                                      XmNlabelPixmap, GetPixmap(PSFV, NORMAL),
                                      NULL);
                      break;
    case FUNC_PERPAY: /* Amount of the periodic payment of a loan */
                      /* Need PV (principal), rate, payment periods */
                      XtVaSetValues ( Calclabels[0], 
                                      XmNlabelString, terms[PV], 
                                      NULL );
                      XtVaSetValues ( Calclabels[1], 
                                      XmNlabelString, terms[RATE], 
                                      NULL );
                      XtVaSetValues ( Calclabels[2], 
                                      XmNlabelString, terms[PRDS], 
                                      NULL );
                      XtVaSetValues ( Calclabel, 
                                      XmNlabelPixmap, GetPixmap(PPER, NORMAL),
                                      NULL);
                      break;
    case FUNC_PV:     /* Present value of series of payments */
                      /* Need Payments, rate, payment periods */
                      XtVaSetValues ( Calclabels[0], 
                                      XmNlabelString, terms[FV], 
                                      NULL );
                      XtVaSetValues ( Calclabels[1], 
                                      XmNlabelString, terms[RATE], 
                                      NULL );
                      XtVaSetValues ( Calclabels[2], 
                                      XmNlabelString, terms[PRDS], 
                                      NULL );
                      XtVaSetValues ( Calclabel, 
                                      XmNlabelPixmap, GetPixmap(PPV, NORMAL),
                                      NULL);
                      break;
    case FUNC_RATE:   /* Periodic interest rate for PV to reach FV */
                      /* Need PV, FV, compunding periods */
                      XtVaSetValues ( Calclabels[0], 
                                      XmNlabelString, terms[PV], 
                                      NULL );
                      XtVaSetValues ( Calclabels[1], 
                                      XmNlabelString, terms[FV], 
                                      NULL );
                      XtVaSetValues ( Calclabels[2], 
                                      XmNlabelString, terms[PRDS], 
                                      NULL );
                      XtVaSetValues ( Calclabel, 
                                      XmNlabelPixmap, GetPixmap (PRATE, NORMAL),
                                      NULL);
                      break;
  }
  
  /* Remember last function selected */
  curr_func = client_data;

  for (i=0; i < XtNumber(terms); i++) 
    XmStringFree ( terms[i] );

  /* Clear out those text fields */
  for (i=0; i < 3; i++) {
    XmTextSetString ( Calctext[i], "" );
    num_ok[i] = 0;
  }

  /* Set traversal to first text field */
  XmProcessTraversal ( Calctext[0], XmTRAVERSE_CURRENT );

  update_result_text ( (double)0.0, curr_func );
} 


/*
** Someone's entered text into a parameter field.  Check if its a number
** and advance to next field.
*/

/* ARGSUSED */
void calcTextSel ( Widget w, int client_data, XmAnyCallbackStruct *call_data )
{

  int next = client_data +1;

  int  i;
  char *numstr, *check;
  double number;

  numstr = XmTextGetString ( w );
  number = strtod ( numstr, &check );
  if ( check >= numstr && check < (numstr + strlen(numstr)) ) {
    XmTextSetSelection ( w, 0, XmTextGetLastPosition(w), CurrentTime );
    write_status ("Please enter a valid number.\n", ERR);
    return;
  }
  XtFree (numstr);

  num_ok[client_data] = number;

  /* Set traversal to next text field */
  if (next == 3)
    next = 0;
  XmProcessTraversal ( Calctext[next], XmTRAVERSE_CURRENT );

  update_result_text ( (double)0.0, curr_func );

  for (i=0; i < 3; i++)
    if (num_ok[i] == 0)
      return;

  calc_result();
}
