/*
  Xinvest is copyright 1995,1996 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.3 $ $Date: 1996/09/17 22:36:57 $
*/

#define LEGEND_WIDTH 25
#define LEGEND_HEIGHT 15
#define LEGEND_INIT -1

void makeLegend ( Widget, int, Pixel);  /* Make a legend for a plot variable */
void displayGraph (int);         /* Show plot legends for account */
void plot ( Widget, Pixmap, GC); /* Drawing routine for plot */
int  getVActive();               /* Returns number of plots in graph */
void graphSense( int );          /* Set graph options sensitive if param != 0 */


/* Callbacks */
void  graph_pselect();                  /* graph mode selection */
void  graph_hselect();                  /* graph type selection */
void  graph_vselect();                  /* graph variable selection */
void  graph_option();                   /* Set optional feature of plot */
void  graph_slide();
void  const_change();
void  graphTraverse();                  /* TAB traverse to obscured list item */

#define PRICE         1
#define SHARE         2
#define COST          3
#define VALUE         4
#define DISTRIB       5
#define IRRRATE       6
#define TRRATE        7 
#define CONST         8
#define SHARE_CUM     9
#define COST_CUM     10
#define VALUE_CUM    11
#define DISTRIB_CUM  12 
#define PRICE_MAVG   13
#define SHARE_MAVG   14
#define COST_MAVG    15
#define DISTRIB_MAVG 16
#define RATE_MAVG    17

#define NUM_PLOTS   DISTRIB_MAVG

/* Account code needs to see this definition, since it gets to store it */
typedef struct toggle_attrib {
   unsigned valid;
   int attrib[NUM_PLOTS];
} PLOT_VAR;

PLOT_VAR *makeGraphVar ();       /* Create an initialize plot attributes */

