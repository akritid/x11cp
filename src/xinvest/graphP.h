/*
  Xinvest is copyright 1995, 1996 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.4 $ $Date: 1996/10/06 15:11:51 $
*/


#define MAVGS       (0x1F << (PRICE_MAVG-1))
#define CONSTS      (1 << (CONST-1))

#define PLOT_ACTIVE(plot) ((graphVar->valid) & (1 << (plot-1)))

#define PIXELS_PER_VDIV 20
#define PIXELS_PER_HDIV 50

#define MAX_OR_MIN(value,scale)    {      \
        if (plot->min > (value)*(scale))  \
           plot->min = (value)*(scale);   \
        if (plot->max < (value)*(scale))  \
           plot->max = (value)*(scale); }

struct line_attrib {
  Pixel color;
  char *dash;
  unsigned int width;
  int style;
};

struct attrib {
  Dimension height;
  Dimension width;
  Dimension zero;
  int       min;
  int       max;
  int       range;
  int       num_pnts;
  long      daymin;
  long      dayrange;
  int       by_trans;
  int       scale;
  int       mov_window;
};

struct trans_value {
  double value[NUM_PLOTS];
};
