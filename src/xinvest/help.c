/*
  Xinvest is copyright 1995 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.3 $ $Date: 1997/01/13 22:46:19 $
*/

#include <stdio.h>

#include <Xm/DialogS.h>
#include <Xm/Form.h>
#include <Xm/Frame.h>
#include <Xm/Text.h>
#include <Xm/List.h>
#include <Xm/PushB.h>
#include <Xm/Label.h>
#include <Xm/LabelG.h>
#include <Xm/PanedW.h>

#include "xutil.h"

static Widget help_dialog;
static int isPoppedUp = False;

static char *helpStrings[] = {
#include "help-text.h"
    NULL
};

static struct {
    int lineno;
    char *string;
} helpTopicInfo[] = {
#include "help-topic.h"
};


/* ARGSUSED */
static void loadtopic ( Widget w, XtPointer client_data, XtPointer call_data )
{
    Widget label;
    Widget text;
 
    XmString xstr;

    int    i;
    int    pos = 0;
    int    topic  = ((XmListCallbackStruct *) call_data)->item_position-1;

    /* Get the heading label and text help area widgets */
    XtVaGetValues (w, XmNuserData,  &label, NULL );
    XtVaGetValues (label, XmNuserData,  &text, NULL );

    xstr = XmStringCreateLocalized ( helpTopicInfo[topic].string );
    XtVaSetValues ( label, XmNlabelString, xstr, NULL );
    XmStringFree ( xstr );

    XmTextSetString ( text, "" );

    for ( i = helpTopicInfo[topic].lineno; helpStrings[i] != NULL; i++) {
       XmTextReplace ( text, pos, pos, helpStrings[i] );
       pos += strlen ( helpStrings[i] ); 
    }

}

/* ARGSUSED */
void helpCB ( Widget w, XtPointer client_data, XtPointer call_data ) 
{
  Widget pane, form, frame, list_w, text_w, heading, button;

  int pos = 0;
  int numHelpTopics;

  int n;
  Arg args[10];

  Dimension width, height, border;

  if (help_dialog == NULL) {
    help_dialog = XtVaCreatePopupShell ("Help", xmDialogShellWidgetClass,
                                         GetTopShell (w ),
                                         XmNdeleteResponse, XmDESTROY,
                                         NULL );

    pane = XtVaCreateWidget ( "pane", xmPanedWindowWidgetClass, help_dialog,
                               XmNsashWidth, 1,
                               XmNsashHeight, 1,
                               NULL );

    form = XtVaCreateWidget ( "form1", xmFormWidgetClass, pane, NULL );


    frame = XtVaCreateManagedWidget( "topicframe", xmFrameWidgetClass, form,
                                     XmNtopAttachment,    XmATTACH_FORM,
                                     XmNbottomAttachment, XmATTACH_FORM,
                                     XmNleftAttachment,   XmATTACH_FORM,
                                     NULL );

    XtVaCreateManagedWidget("Topics", xmLabelGadgetClass, frame,
                          XmNchildType,              XmFRAME_TITLE_CHILD,
                          XmNchildVerticalAlignment, XmALIGNMENT_CENTER,
                          NULL);

    /* scrolling list holding topics */
    n = 0;
    XtSetArg(args[n], XmNscrollVertical,   True); n++;
    XtSetArg(args[n], XmNscrollHorizontal, False); n++;
    XtSetArg(args[n], XmNselectionPolicy,  XmSINGLE_SELECT); n++;
    list_w = XmCreateScrolledList ( frame, "topiclist", args, n);
    
    XtAddCallback ( list_w, XmNdefaultActionCallback, 
                            (XtCallbackProc) loadtopic, NULL );
    XtManageChild ( list_w );

    numHelpTopics = XtNumber(helpTopicInfo)-1;
    for (n=0; n < numHelpTopics; n++) 
      XmListAddItemUnselected( list_w,
                             XmStringCreateLocalized ( helpTopicInfo[n].string),
                             0 );


    frame = XtVaCreateManagedWidget( "textframe", xmFrameWidgetClass, form,
                                  XmNtopAttachment,    XmATTACH_FORM,
                                  XmNbottomAttachment, XmATTACH_FORM,
                                  XmNleftAttachment,   XmATTACH_WIDGET,
                                  XmNleftWidget,       frame,
                                  XmNrightAttachment,  XmATTACH_FORM,
                                  NULL );

    heading = XtVaCreateManagedWidget ("heading", xmLabelGadgetClass, frame,
                                 XmNchildType,              XmFRAME_TITLE_CHILD,
                                 XmNchildVerticalAlignment, XmALIGNMENT_CENTER,
                                 XtVaTypedArg, XmNlabelString, XmRString,
                                      helpTopicInfo[0].string,
                                      strlen(helpTopicInfo[0].string),
                                 NULL );


    /* Scrolled text widget actual help text */
    n = 0;
    XtSetArg(args[n], XmNscrollVertical,        True); n++;
    XtSetArg(args[n], XmNscrollHorizontal,      False); n++;
    XtSetArg(args[n], XmNeditMode,              XmMULTI_LINE_EDIT); n++;
    XtSetArg(args[n], XmNeditable,              False); n++;
    XtSetArg(args[n], XmNcursorPositionVisible, False); n++;
    XtSetArg(args[n], XmNautoShowCursorPosition,  False); n++;
    XtSetArg(args[n], XmNwordWrap,  True); n++;
    text_w = XmCreateScrolledText( frame, "text", args, n);
  
    for ( n = helpTopicInfo[0].lineno; helpStrings[n] != NULL; n++) {
       XmTextReplace ( text_w, pos, pos, helpStrings[n] );
       pos += strlen ( helpStrings[n] ); 
    }

    XtManageChild( text_w );
    XtManageChild( form );

    /* Remember label so we can set it from selected topic */
    XtVaSetValues (list_w, XmNuserData,  heading, NULL );
    /* Remember text so we can set it from selected topic */
    XtVaSetValues (heading, XmNuserData,  text_w, NULL );

    form = XtVaCreateWidget ( "form2", xmFormWidgetClass, pane, 
                              XmNfractionBase, 5,
                              NULL );
    button = XtVaCreateManagedWidget ( "Ok", xmPushButtonWidgetClass, form,
                                        XmNtopAttachment,    XmATTACH_FORM,
                                        XmNbottomAttachment, XmATTACH_FORM,
                                        XmNleftAttachment,   XmATTACH_POSITION,
                                        XmNleftPosition,     2,
                                        XmNrightAttachment,  XmATTACH_POSITION,
                                        XmNrightPosition,    3,
                                        XmNshowAsDefault,    True,
                                        XmNdefaultButtonShadowThickness, 1,
                                        XmNuserData,         &isPoppedUp,
                                        NULL );
    XtAddCallback ( button, XmNactivateCallback, 
                            (XtCallbackProc) DestroyShell, help_dialog );
    
    XtManageChild ( form );
    XtManageChild ( pane );

    XtVaGetValues ( help_dialog, XmNwidth, &width,
                                 XmNheight, &height,
                                 XmNborderWidth, &border,
                    NULL );

    XtVaSetValues ( help_dialog, 
                    XmNminWidth,  width +  border,
                    XmNmaxWidth,  width +  border,
                    XmNminHeight, height + border,
                    XmNmaxHeight, height + border,
                    NULL );
  }
  
  XtManageChild (help_dialog);
  XtPopup(help_dialog,XtGrabNone);
  isPoppedUp = True;
}
