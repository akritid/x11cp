/*
  Xinvest is copyright 1995, 1996 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.4 $ $Date: 1996/11/08 22:43:08 $
*/

#include <stdio.h>
#include <Xm/Xm.h>

#include "account.h"
#include "nav.h"
#include "trans.h"


/* Are the current parameters the same as the last transaction? */
int isNavChanged( int account, NAV *nav)
{
  char date[11];
  long strtoday(char *);

  int numTrans;
  void *trans;

  getAccount( account, ACCOUNT_NUM_TRANS, &numTrans );
  getAccount( account, ACCOUNT_TRANS, &trans );

  if ( trans == NULL || numTrans == 0)      /* just in case */
    return False;

  sprintf( date, "%d/%d/%d", nav->month, nav->date, nav->year);

  if ( (float)(nav->value) - (float)GetTransNav (trans, numTrans) != (float)0.0 
       || strtoday(date) != GetTransLDate(trans, numTrans) )
     return True;
  else
     return False;
}

void setNav (NAV *nav, int field, void *val)
{

  if (nav) {

    switch (field) {
      case NAV_VALUE:
          nav->value = (float) *(double *)val;
          break;

      case NAV_MONTH:
          nav->month = *(int *)val;
          break;

      case NAV_DAY:
          nav->date = *(int *)val;
          break;

      case NAV_YEAR:
          nav->year = *(int *)val;
          break;

      case NAV_DATE:
          sscanf ( (char *)val, "%2d/%2d/%4d", 
                   &(nav->month), &(nav->date), &(nav->year) );
          break;
    }
  }
}

void getNav (NAV *nav, int field, void *val)
{
  if (nav) {

    switch (field) {
      case NAV_VALUE:
          *(float *)val = nav->value;
          break;

      case NAV_MONTH:
          *(int *)val = nav->month;
          break;

      case NAV_DAY:
          *(int *)val = nav->date;
          break;

      case NAV_YEAR:
          *(int *)val = nav->year;
          break;
    }
  }
}

long navToLDate ( NAV *navp )
{
  extern long strtoday (char *);
  char currentDate[11];

  sprintf( currentDate, "%2d/%2d/%2d", navp->month, navp->date, navp->year);
  return ( strtoday(currentDate) );
}
