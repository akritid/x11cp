/*
  Xinvest Copyright 1995-97 Mark Buser,
  All Rights Reserved.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.11 $ $Date: 1997/07/20 19:40:21 $
*/
#include <Xm/Xm.h>
#ifdef SHAPE
#include <X11/extensions/shape.h>
#endif
#include <xpm.h>

#include "color.h"
#include "pixmap.h"
#include "pixmapP.h"
#include "xutil.h"


static Pixmap pixmap[XtNumber(pixname)];
static Pixmap inspixmap[XtNumber(pixname)];
static Pixmap shapemask[XtNumber(pixname)];

/* forward declarations */
static Pixmap   MakeWindowsPixmap ( Widget, Pixmap );
static Pixmap   MakeGrayPixmap ( Widget, Pixmap );

Colormap InitPixmaps( Widget top, Colormap cmap )
{
  Display *dpy = XtDisplay( top );
  Window  win = RootWindowOfScreen (XtScreen( top ));
  Pixel  bg;
  Pixmap junk;  /* Unused, except to code around xpm-3.4h bug */

  XpmColorSymbol backg;
  XpmColorSymbol mask[2];
  XpmAttributes attrib;

  int code;
  int i = 0;
  int colorVisual = isColorVisual(dpy);

  XtVaGetValues( top, XmNbackground, &bg, NULL );

  /* Override background color with background color of parent */
  backg.name = "mask";
  backg.value = NULL;
  backg.pixel = bg;

  /* 
  ** Used to generate shapemask for insensitive pixmap. Set mask and light
  ** to transparent.
  */
  mask[0].name = "mask";
  mask[0].value = "none";
  mask[0].pixel = (Pixel)NULL;

  mask[1].name = "light";
  mask[1].value = "none";
  mask[1].pixel = (Pixel)NULL;

  attrib.numsymbols = 1;
  attrib.colorsymbols = &backg;
  attrib.closeness = 40000;
  attrib.colormap = cmap;
  attrib.valuemask = XpmColorSymbols | XpmCloseness | XpmColormap;

#ifdef MONO_TEST
  attrib.color_key = XPM_MONO;
  attrib.valuemask |= XpmColorKey;
#endif

  do {
     /* Make the normal (sensitive) pixmap. */
     code = XpmCreatePixmapFromData( dpy, win,
                                     pixname[i], &(pixmap[i]), &(shapemask[i]),
                                     &attrib );
     switch (code) {

         case XpmSuccess:

                if (i <= LASTINSENS) {  /* No insensitive for others */
                  /* 
                  ** Now make the insensitive pixmap.  If we're color-challenged
                  ** make the fuzzed out pixmap, otherwise get the windows
                  ** type of pixmap.
                  */

                  if ( colorVisual ) {
                    attrib.colorsymbols = &mask[0];
                    attrib.numsymbols = 2;
              
                    /* 
                    ** We assume this succeeds because the above did. If 
                    ** it didn't we'll get the XCreateInsensitive 
                    ** pixmap instead of the MakeWindowsPixmap variety.
                    */
                    code = XpmCreatePixmapFromData( dpy, win,
                                               pixname[i], 
                                               &junk, &(shapemask[i]),
                                               &attrib );
                    /* Junk is not needed, but we can't pass NULL to
                    ** XPM as the pixmap.  This is a bug in XPM. */
                    XFreePixmap ( dpy, junk);

                    attrib.colorsymbols = &backg;
                    attrib.numsymbols = 1;

                    if (shapemask[i])
                      inspixmap[i] = MakeWindowsPixmap ( top, shapemask[i]);
                    else
                      inspixmap[i] = MakeGrayPixmap( top, pixmap[i] );

                  } else  /* MONO visual */
                    inspixmap[i] = MakeGrayPixmap( top, pixmap[i] );

                  if ( inspixmap[i] == (Pixmap)NULL)
                    return (Colormap)NULL;

                } /* if pixmap needs insensitive */

                i++;
                break;

         case XpmColorFailed:
              if ( (cmap = NewColormap( dpy, cmap)) == (Colormap)NULL)
                 return (Colormap)NULL;
              attrib.colormap = cmap;
              attrib.valuemask |= XpmColormap;
              break;

         default:
              return (Colormap)NULL;
              /* break; */
    }
  } while (i <= LASTPIXMAP);

  return cmap;
}

Pixmap GetPixmap ( int which, int type)
{
  switch (type) {
    case NORMAL:  return pixmap[which];

    case INSENS:  return inspixmap[which];

    case MASK:    return shapemask[which];

    default:      return (Pixmap) XmUNSPECIFIED_PIXMAP;
  }
}

static Pixmap MakeWindowsPixmap( Widget parent, Pixmap image)
{
    static GC           graygc = (GC) NULL;

    Display             *dpy = XtDisplay (parent);
    int                 depth = DefaultDepthOfScreen( XtScreen(parent));

    Pixel               bg, shadow;
    Pixmap		new;

    int			x, y;
    Window		root;
    unsigned int	w, h, d, b;

    /* how big is the original? */
    XGetGeometry( dpy, image, &root, &x, &y, &w, &h, &b, &d );

    if (graygc == (GC) NULL) {
       graygc = XCreateGC( dpy, root, 0, 0 );
       XSetFillStyle( dpy, graygc, FillStippled );
    }

    new = XCreatePixmap( dpy, root, w, h, depth );

    XSetClipMask ( dpy, graygc, None);

    XtVaGetValues( parent, XmNbackground, &bg, 0 );
    XmGetColors ( XtScreen(parent), GetColormap(), bg, 
                  NULL, NULL, &shadow, NULL);

    /* fill pixmap in background color */
    XSetForeground ( dpy, graygc, bg ); 
    XFillRectangle( dpy, new, graygc, 0, 0, w, h );

    /* Copy mask onto new pixmap, but offset a bit and in white */
    XSetClipMask ( dpy, graygc, image);
    XSetClipOrigin ( dpy, graygc, 1, 1 );
    XSetForeground ( dpy, graygc, GetColor( WHITE) ); 

    XFillRectangle( dpy, new, graygc, 0, 0, w, h );

    /* Copy mask onto new pixmap in an appropriate dark color */
    XSetForeground ( dpy, graygc, shadow ); 
    XSetClipOrigin ( dpy, graygc, 0, 0 );
    XFillRectangle( dpy, new, graygc, 0, 0, w, h );

    return (new);

}

#define gray_width 16
#define gray_height 16
static unsigned char gray_bits[] = {
   0x99, 0x99, 0xAA, 0xAA, 0x66, 0x66, 0x55, 0x55, 
   0x99, 0x99, 0xAA, 0xAA, 0x66, 0x66, 0x55, 0x55, 
   0x99, 0x99, 0xAA, 0xAA, 0x66, 0x66, 0x55, 0x55,
   0x99, 0x99, 0xAA, 0xAA, 0x66, 0x66, 0x55, 0x55};

static	Pixmap	MakeGrayPixmap( Widget parent, Pixmap image )
{
    static Pixmap       graypm = (Pixmap) NULL;
    static GC           graygc = (GC) NULL;

    Display		*display = XtDisplay(parent);
    Pixmap		new;
    int			x, y;
    Window		root;
    unsigned int	w, h, d, b;
    Pixel		bg;

    /* how big is the original? */
    XGetGeometry( display, image, &root, &x, &y, &w, &h, &b, &d );

    /* find/create the tile */
    if ( graypm == 0 ) {
	graypm = XCreateBitmapFromData( display,
					root,
					(char *) gray_bits,
					gray_width,
					gray_height
					);
	graygc = XCreateGC( display, root, 0, 0 );
	XSetStipple( display, graygc, graypm );
	XSetFillStyle( display, graygc, FillStippled );

	XtVaGetValues( parent, XmNbackground, &bg, 0 );
	XSetForeground( display, graygc, bg );
    }

    /* create a copy and gray it out */
    new = XCreatePixmap( display, root, w, h, d );
    XCopyArea( display, image, new, graygc, 0, 0, w, h, 0, 0 );
    XFillRectangle( display, new, graygc, 0, 0, w+1, h+1 );

    return new;
}


void MakeIconWindow( Widget top )
{
   Window window, root;
   unsigned int width, height, border_width, depth;
   int x, y;
   Display *dpy = XtDisplay (top);
   Pixmap iconpix, maskpix;
   int shape_event, shape_error;

   XpmColorSymbol mask;
   XpmAttributes attrib;

   /* Set color "mask", the bg color, to NULL making it transparent */
   mask.name = "mask";
   mask.value = "none";
   mask.pixel = (Pixel)NULL;

   attrib.numsymbols = 1;
   attrib.colorsymbols = &mask;
   attrib.closeness = 40000;
   attrib.valuemask = XpmColorSymbols | XpmCloseness;

   /* Create ICON pixmap and window mask */
   if ( XpmCreatePixmapFromData( dpy, RootWindowOfScreen ( XtScreen(top) ), 
                                 pixname[PICON], &iconpix, &maskpix, &attrib ) 
        != XpmSuccess )
     return;

   /* Get current icon window */
   XtVaGetValues ( top, XmNiconWindow, &window, NULL);

   /* If we don't have one, make a simple window as large as the pixmap */
   if (!window) {
     if (!XGetGeometry ( dpy, iconpix, &root, 
                         &x, &y, &width, &height, &border_width, &depth ) ||
         !( window = XCreateSimpleWindow ( dpy, root, 0, 0, width, height,
                         (unsigned) 0, CopyFromParent, CopyFromParent ))) {

       /* If either fails, just set the icon pixmap as a fallback */
       XtVaSetValues ( top, XmNiconPixmap, iconpix, NULL);
       return;
     }

#ifdef SHAPE
     /* Set window mask to get transparent icon background */
     if ( XShapeQueryExtension ( dpy, &shape_event, &shape_error) )
        XShapeCombineMask ( dpy, window, ShapeBounding, 0, 0, 
                            maskpix, ShapeSet );
 
#endif
     XFreePixmap (dpy, iconpix);
     XFreePixmap (dpy, maskpix);

     /* Created ok, set the window */
     XtVaSetValues ( top, XmNiconWindow, window, NULL);

   }
                          
   /* Set window (icon) background to our icon pixmap */
   XSetWindowBackgroundPixmap ( dpy, window, pixmap[PICON] );

   /* Force redisplay */
   XClearWindow ( dpy, window);
}
