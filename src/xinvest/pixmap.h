/*
  Xinvest Copyright 1995-97 Mark Buser,
  All Rights Reserved.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.8 $ $Date: 1997/07/20 19:37:09 $
*/
#ifndef PIXMAP_H
#define PIXMAP_H

enum { 
  NORMAL, INSENS, MASK
} Pixtype;

#include "pixname.h"

Colormap InitPixmaps (Widget, Colormap);
Pixmap GetPixmap (int /*Pixmaps*/, int /*Pixtype*/);
void   MakeIconWindow (Widget);

#endif
