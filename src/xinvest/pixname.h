/*
  Xinvest Copyright 1997 Mark Buser,
  All Rights Reserved.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 1.3 $ $Date: 1997/08/06 23:21:49 $
*/
#ifndef PIXNAME_H
#define PIXNAME_H

enum { 
       PSTOP, PABOUT, PRETURN, PCALC, PGRAPH, PPORTFOLIO, PEDIT,
       PSAVE, PNEW, POPEN, PPREV, PNEXT, 
       PDAY, PWEEK, PMONTH, PQTR, PYEAR, PYTD, PFIRST, PPICK,
       PCHECK, PX, 
       PACCT, PACCTS, PFOLD,
       PBAR, PLINE,
       PLAST, PNEXTTRANS, PPREVTRANS,
       PINFO, PSTIPPLE, PFV, PSFV, PPER, PPV, PRATE, PICON
} Pixmaps;

#define LASTINSENS PLAST
#define LASTPIXMAP PICON
#endif
