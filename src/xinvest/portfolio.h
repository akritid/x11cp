/*
  Xinvest is copyright 1995 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.2 $ $Date: 1996/11/28 16:27:33 $
*/

typedef struct link {
   struct node *child;
   struct link *next; 
} LINK;


typedef struct node {
   char  *name;
   float value;
   char  isleaf;
   LINK  *children;
   LINK  *parent; 
} NODE;

int getPActive();               /* Returns number of slices in pie */
int pie ( Widget, Pixmap, GC);  /* Drawing routine for pie chart */
void portUpdateDisplay();       /* Redraw pie from current level */
void portSense();               /* Set portfolio buttons active or not */

#define PORT_ADD    1
#define PORT_DELETE 2
void portAccount( int, int);    /* add/remove account index (1st param) */
