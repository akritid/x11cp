/*
  Xinvest is copyright 1995,.1996 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.19 $ $Date: 1997/09/20 17:02:41 $
*/

/*  rate of return of a portfolio	*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <Xm/Xm.h>

#include "account.h"
#include "drawing.h"
#include "nav.h"
#include "rate.h"
#include "trans.h"
#include "status.h"
#include "xutil.h"

/*  
**  strtoday - string into day number 
**  Convert "MM/DD/YY", "MM/DD/YYYY",  or "MMDDYY" into # of days using 
**  Jan 1, 1601 = day 1. REF: CACM 11, 657 (1968) - valid back to 24 Nov -4713
*/
long strtoday( char *dd)
{

#define MONTH (n[0])
#define DAY (n[1])
#define YEAR (n[2])

  static unsigned maxd[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
  char		*ddend;
  int		c = 0, slash = 0;
  long		jdn;
  int		n[3];
  static int	m;

    jdn = strtol(dd, &ddend, 10);		/* convert mmddyy */
    MONTH = jdn;

    if (*ddend == '/') {
	slash = 1;				/* got mm/dd/yy or yyyy  */
	goto middle;
	}

    else if (*ddend) return -1;

    for (; c <= 2; c++) {
	if (slash) {
	    n[c] = strtol(dd, &ddend, 10);	/* convert a number */
	    if (*ddend != "//"[c])		/* check proper terminator */
baddy:		return -1;
middle:	    dd = ddend + 1;
	    }
	else {
	    n[2 - c] = jdn % 100;
	    jdn /= 100;
	    }
	}

    if (YEAR <= 99) YEAR += 1900;	/* correct for c */
    maxd[2] = 28;
    if ((!(YEAR & 3) && (YEAR % 100)) || !(YEAR % 400)) 
	maxd[2] = 29;
    if (MONTH < 1 || MONTH > 12 || DAY < 1 || DAY > maxd[MONTH]) goto baddy;

    m = (MONTH <= 2) ? -1 : 0;
    YEAR += m;
    jdn = (long)DAY - 2337888L;
    jdn += 1461L * ((long)YEAR + 4800L) / 4L;
    jdn += 367L * (MONTH - 2L - (long)m * 12L) / 12L;
    jdn -= 3L * (((long)YEAR + 4900L) / 100L) / 4L;

    return (long)jdn;

#undef MONTH
#undef DAY
#undef YEAR
}

/*
** Given we know the long and string of p, calculate the string of date.
** We know it must be later than p.
*/
char *daytostr (long date, char *p )
{
  int month;
  int day;
  int year;

  struct tm date_tm;

  char year_char[5];
  static char try[20];

  /* extract date from transaction record */
  p += 2;
  if ( *p == '/' )   /* skip if present */
    p++;
  p += 2;
  if ( *p == '/' )   /* skip if present */
    p++;

  /* Assume \0 is present in p */
  strcpy( year_char, p );

  /* get int versions */
  month = 1;
  day =   1;
  year =  atoi ( year_char );

  sprintf( try, "%2.2d/%2.2d/%2.2d", month, day, year);

  while ( strtoday ( try ) <=  date ) {
    year++;
    sprintf( try, "%2.2d/%2.2d/%2.2d", month, day, year);
  }
  year--;
  sprintf( try, "%2.2d/%2.2d/%2.2d", month, day, year);

  while ( strtoday ( try ) <=  date && month <= 12 ) {
    month++;
    sprintf( try, "%2.2d/%2.2d/%2.2d", month, day, year);
  }
  month--;
  sprintf( try, "%2.2d/%2.2d/%2.2d", month, day, year);

  day = date - strtoday( try ) +1;
  sprintf( try, "%2.2d %2.2d %2.2d", month, day, year);

  /* Convert to internationalized format */
  if (year > 1000)
    strptime ( try, "%m %d %Y", &date_tm);
  else
    strptime ( try, "%m %d %y", &date_tm);
  strftime ( try, 10, "%x", &date_tm);
  return ( try );
}

static double *deposits = NULL;   /* value of deposit */
static long *dates = NULL;        /* date of deposit */
static int  ndep = 0;             /* num deposits */
static long IRRmaxday = 0;        /* most recent transaction date */

void rateClrTrans ()
{
  XtFree ( (char *) deposits );
  deposits = NULL;
  XtFree ( (char *) dates );
  dates = NULL;
  ndep = 0;
  IRRmaxday = 0;
}

void rateAddValue(double val, long day)
{
  deposits = (double *) XtRealloc( (char *)deposits, sizeof(double)*(ndep+1));
  dates  =   (long *) XtRealloc( (char *) dates, sizeof(long)*(ndep+1));
  if ( deposits == NULL || dates == NULL) {
    write_status ( "Out of memory: can't calculate return.", ERR );
    return;
  }
  deposits[ndep] = val;
  dates[ndep] = day;
  ndep++; 
}

void rateAddTrans( TRANS *trans, int transno)
{
  int type = GetTransType (trans, transno);
  long day = GetTransLDate( trans, transno );

  if ( type & (T_BUY | T_SELL)) { 
    deposits = (double *) XtRealloc( (char *)deposits, sizeof(double)*(ndep+1));
    dates  =   (long *) XtRealloc( (char *) dates, sizeof(long)*(ndep+1));
    if ( deposits == NULL || dates == NULL) {
      write_status ( "Out of memory: can't calculate return.", ERR );
      return;
    }

    /* 
    ** Load is added on a buy because this is what it cost, shares+fee.
    */
    if (type == T_BUY) 
      deposits[ndep] = GetTransNav( trans,transno ) 
    	               * GetTransShares( trans, transno) 
		       + GetTransLoad(trans,transno);
    /* 
    ** Load is NOT subtracted here since this is all we really got out. Load on
    ** sales work into the IRR calculation via current value, which is less
    ** when there is a fee. 
    **/
    else if (type == T_SELL) 
      deposits[ndep] = GetTransNav( trans,transno )
	               * GetTransShares( trans, transno);

    dates[ndep] = day;

    ndep++;
  }

  if ( IRRmaxday < day )
    IRRmaxday = day;
}

/* Set up for a fresh round */
void rateClrAccount ()
{
  rateClrTrans();
}


/* Add this account to rate of return calculation input */
void rateAddAccount ( int account, NAV *nav)
{
  char err_msg[132];
  long endDate = navToLDate ( nav );

  int transno;
  void *trans;
  int  numTrans;

  getAccount( account, ACCOUNT_NUM_TRANS, &numTrans );
  getAccount( account, ACCOUNT_TRANS, &trans );

  /* If the NAV is too early, tell the user */
  if ( endDate < GetTransLDate(trans, 1) ) {
    if (!suppressed()) {
      char *name = createAccountName (account);
      sprintf ( err_msg, 
                "%s:\nSpecified NAV date is earlier than\nfirst transaction.",
                name );
      write_status ( err_msg, ERR );
      XtFree (name);
    }
    return;
  }

  /* Add transactions to deposits until end or we reach user specified end */
  for (transno = 1; transno <= numTrans; transno++) {
    if ( GetTransLDate(trans, transno) > endDate )
      break;
    rateAddTrans (trans, transno);
  }
}

void rate_add_account (int account, long sdate, long edate)
{
  int transno;
  void *trans;
  int  numTrans;

  long start, end;

  getAccount( account, ACCOUNT_NUM_TRANS, &numTrans );
  getAccount( account, ACCOUNT_TRANS, &trans );

  if (sdate == FIRST_DATE)
    start = GetTransLDate (trans, 1);
  else
    start = sdate;

  if (edate == LAST_DATE)
    end = GetTransLDate (trans, numTrans);
  else
    end = edate;

  if (IRRmaxday < end)
    IRRmaxday = end;            /* Calculate IRR to end date */

  if (start-1 > GetTransLDate (trans, 1)) {
    rateAddValue (rate_value (account, FIRST_DATE, start-1), start-1);
#ifdef DEBUG
    fprintf (stderr, "IRR ldate %ld lump: %.2f\n", 
             start-1, rate_value (account, FIRST_DATE, start-1));
#endif
  }

  for (transno = 1; transno <= numTrans; transno++) {
    if ( GetTransLDate (trans, transno) < start )
      continue;
    if ( GetTransLDate (trans, transno) > end )
      break;
    rateAddTrans (trans, transno);
  }

}


/* GCC 2.7.2: bad code at -O2 if this is inside rate_IRR */
static double rate; 

/* 
** Calculate IRR from previously set up input data to curtot. 
*/
double rate_IRR ( double curtot )
{
  /*  Calculate daily rate of return by binary search	*/
  if (ndep) {
    int  i, not_moving = 0;  

    double  testtot, prevtot;
    double  limit[2]; 
    int	    which;

    /* Same total for 'not_moving' tries, we're not converging */
    prevtot = 0.0;

    limit[1] = 1.1;		/* 10 percent every day	*/
    limit[0] = 0.0;		/* throw it all away	*/
    for (;;) {
      rate = (limit[1] + limit[0]) / (double)2.0;
      testtot = 0.0;
      for (i = 0; i < ndep; i++) 
        testtot += (deposits[i] * pow ( rate, (double)(IRRmaxday - dates[i])) );

      /* Bail out if too many tries */
      if ( testtot == prevtot ) {
	if (++not_moving == 20)
          return (double)0.0;
      } else
	not_moving = 0;

      which = (curtot < testtot);

      if (limit[0] >= limit[1] || limit[which] == rate) {
        rate = 100.0 * ( pow ( rate, (double)365.0) - 1.0 );
        return (rate);
      }

      limit[which] = rate;

      prevtot = testtot;
    }

  }
  return (double)0.0;
}


double account_fee( int account, NAV *nav )
{
  int i;
  double load = 0.0;
  long maxday = 0;

  int numTrans;
  void *trans;

  getAccount( account, ACCOUNT_NUM_TRANS, &numTrans );
  getAccount( account, ACCOUNT_TRANS, &trans );

  if ( nav != NULL )
    maxday = navToLDate( nav );
  else
    maxday = GetTransLDate(trans, numTrans);

  for ( i=1; i <= numTrans; i++) {
    if ( GetTransLDate(trans, i) > maxday ) 
      /* user specified date less then some number of transactions */
      break;

    load += GetTransLoad (trans, i);
  }

  return (load);
}

double rate_fee( int account, long sdate, long edate )
{
  int i;
  double load = 0.0;

  int numTrans;
  void *trans;

  long start, end;

  getAccount( account, ACCOUNT_NUM_TRANS, &numTrans );
  getAccount( account, ACCOUNT_TRANS, &trans );

  if (sdate == FIRST_DATE)
    start = GetTransLDate( trans, 1 );
  else
    start = sdate;

  if (edate == LAST_DATE)
    end = GetTransLDate( trans, numTrans );
  else
    end = edate;

  for ( i=1; i <= numTrans; i++) {

    if ( GetTransLDate(trans, i) < start ) 
      continue;
    if ( GetTransLDate(trans, i) > end ) 
      break;

    load += GetTransLoad (trans, i);
  }

  return (load);
}


/* How many shares are in the account, taking into consideration dividend
** reinvestment (or not), load on sales, and splits. */
double account_shares( int account, NAV *nav )
{
  int i;
  double shares = 0.0;
  long	 maxday = 0;

  int numTrans;
  void *trans;
  int type;
  int reinvest;

  getAccount( account, ACCOUNT_NUM_TRANS, &numTrans );
  getAccount( account, ACCOUNT_TRANS, &trans );

  if ( nav != NULL )
    maxday = navToLDate( nav );
  else
    maxday = GetTransLDate(trans, numTrans);

  for ( i=1; i <= numTrans; i++) {
    if ( GetTransLDate(trans, i) > maxday ) 
      /* user specified date less then some number of transactions */
      break;

    /* add up all shares owned. */
    type = GetTransType(trans,i);
    reinvest = GetTransFlag ( trans, i, TRANS_REINVEST );

    /* shares are neg for T_SELL. divs only if reinvest on */
    if ( type != T_DIST || reinvest )
      shares += GetTransShares(trans, i);

    /* possible load on sale */
    if ( type == T_SELL )
      shares -= GetTransLoad(trans,i) / GetTransNav(trans,i);

    if ( type == T_SPLIT )
      shares = ( shares * GetTransSplit(trans,i,TRANS_MAJOR)) 
	       / GetTransSplit(trans,i,TRANS_MINOR);
  }

  return ( shares );
}

double rate_shares( int account, long sdate, long edate )
{
  int i;
  double shares = 0.0;

  int numTrans;
  void *trans;
  int type;
  int reinvest;
  long start, end;

  getAccount( account, ACCOUNT_NUM_TRANS, &numTrans );
  getAccount( account, ACCOUNT_TRANS, &trans );

  if (sdate == FIRST_DATE)
    start = GetTransLDate( trans, 1 );
  else
    start = sdate;

  if (edate == LAST_DATE)
    end = GetTransLDate( trans, numTrans );
  else
    end = edate;

  for ( i=1; i <= numTrans; i++) {

    if ( GetTransLDate(trans, i) < start ) 
      continue;
    
    if ( GetTransLDate(trans, i) > end ) 
      break;

    /* add up all shares owned. */
    type = GetTransType(trans,i);
    reinvest = GetTransFlag ( trans, i, TRANS_REINVEST );

    /* shares are neg for T_SELL. divs only if reinvest on */
    if ( type != T_DIST || reinvest )
      shares += GetTransShares(trans, i);

    /* possible load on sale */
    if ( type == T_SELL )
      shares -= GetTransLoad(trans,i) / GetTransNav(trans,i);

    if ( type == T_SPLIT )
      shares = ( shares * GetTransSplit(trans,i,TRANS_MAJOR)) 
	       / GetTransSplit(trans,i,TRANS_MINOR);
  }

  return ( shares );
}


/* What are the current shares worth at current price */
double account_value ( int account, NAV *nav )
{
  double  curtot = 0.0;
  int     numTrans;
  void    *trans;

  getAccount( account, ACCOUNT_NUM_TRANS, &numTrans );
  getAccount( account, ACCOUNT_TRANS, &trans );

  curtot = account_shares ( account, nav );

  if ( nav != NULL )
    curtot *= nav->value;
  else
    curtot *= GetTransNav(trans, numTrans);

  return (curtot);
}

double rate_value ( int account, long sdate, long edate )
{
  double  curtot = 0.0;
  int     numTrans;
  void    *trans;

  int     i;
  long    latestDate = 0;

  long    start, end;

  getAccount( account, ACCOUNT_NUM_TRANS, &numTrans );
  getAccount( account, ACCOUNT_TRANS, &trans );

  if (sdate == FIRST_DATE)
    start = GetTransLDate( trans, 1 );
  else
    start = sdate;

  if (edate == LAST_DATE)
    end = GetTransLDate( trans, numTrans );
  else
    end = edate;

  curtot = rate_shares (account, start, end);

  /* Find last trans before end date */
  for (i=1; i <= numTrans; i++) {
    long nextDate = GetTransLDate (trans, i);

    if (nextDate < start) 
      continue;
    
    if (nextDate > end) 
      break;

    if (nextDate == latestDate)
      continue;

    latestDate = nextDate;
  }

  /* Do we have a pending price point? If not use last NAV within date range. */
  {
    NAV *navp;
    float val = 0.0;
    getAccount (account, ACCOUNT_NAV, &navp);
    /* 
    ** Price point must be later than any existing trans and no later than
    ** end date.
    */
    if (navp && navToLDate(navp) > latestDate && navToLDate(navp) <= end) { 
      getNav (navp, NAV_VALUE, &val);
      curtot *= (double)val;
    } else if (i-1 > 0)
      curtot *= GetTransNav (trans, i-1);
  }

  return (curtot);
}


/* How much have we spent on shares including load */
double account_cost ( int account, NAV *nav )
{
  long	  maxday = 0;
  double  curtot = 0.0;
  int	  i;

  int     numTrans;
  void    *trans;

  getAccount( account, ACCOUNT_NUM_TRANS, &numTrans );
  getAccount( account, ACCOUNT_TRANS, &trans );

  if ( nav != NULL )
    maxday = navToLDate( nav );
  else
    maxday = GetTransLDate(trans, numTrans);

  for (i = 1; i <= numTrans; i++) {

    /* user specified date less then some number of transactions */
    if ( GetTransLDate(trans, i) > maxday)
      break;

    /* add up all shares owned for current cost, factor in load  */
    if ( GetTransType(trans, i) == T_BUY)
      curtot += (GetTransShares(trans, i) * GetTransNav(trans, i)
                + GetTransLoad (trans, i)); 
  }

  return (curtot);
}

double rate_cost ( int account, long sdate, long edate )
{
  double  curtot = 0.0;
  int	  i;

  int     numTrans;
  void    *trans;

  long    start, end;

  getAccount( account, ACCOUNT_NUM_TRANS, &numTrans );
  getAccount( account, ACCOUNT_TRANS, &trans );

  if (sdate == FIRST_DATE)
    start = GetTransLDate( trans, 1 );
  else
    start = sdate;

  if (edate == LAST_DATE)
    end = GetTransLDate( trans, numTrans );
  else
    end = edate;


  for (i = 1; i <= numTrans; i++) {

    if ( GetTransLDate(trans, i) < start)
      continue;

    if ( GetTransLDate(trans, i) > end)
      break;

    /* add up all shares owned for current cost, factor in load  */
    if ( GetTransType(trans, i) == T_BUY)
      curtot += (GetTransShares(trans, i) * GetTransNav(trans, i)
                + GetTransLoad (trans, i)); 
  }

  return (curtot);
}

/* How much money came out of the account not including load which just
** evaporated. */
double account_withdrawal( int account, NAV *nav )
{
  int i;
  double withdraw = 0.0;
  long	  maxday = 0;

  int numTrans;
  void *trans;
  int reinvest;

  getAccount( account, ACCOUNT_NUM_TRANS, &numTrans );
  getAccount( account, ACCOUNT_TRANS, &trans );

  if ( nav != NULL )
    maxday = navToLDate( nav );
  else
    maxday = GetTransLDate(trans, numTrans);

  for ( i=1; i <= numTrans; i++) {
    if ( GetTransLDate(trans, i) > maxday ) 
      /* user specified date less then some number of transactions */
      break;

    if ( GetTransType(trans, i) == T_SELL ) 
      /* - since T_SELL shares are negative. */
      withdraw += (-GetTransShares(trans, i) * GetTransNav(trans, i));

    /* divs only if reinvest off. */
    reinvest = GetTransFlag (trans, i, TRANS_REINVEST);
    if ( GetTransType(trans,i) == T_DIST && !reinvest )
      withdraw += GetTransShares(trans, i) * GetTransNav(trans, i);
  }

  /* withdraw is positive for any sales or div distributions */
  return ( withdraw );
}

double rate_withdrawal( int account, long sdate, long edate )
{
  int i;
  double withdraw = 0.0;

  int numTrans;
  void *trans;
  int reinvest;

  long start, end;

  getAccount( account, ACCOUNT_NUM_TRANS, &numTrans );
  getAccount( account, ACCOUNT_TRANS, &trans );

  if (sdate == FIRST_DATE)
    start = GetTransLDate( trans, 1 );
  else
    start = sdate;

  if (edate == LAST_DATE)
    end = GetTransLDate( trans, numTrans );
  else
    end = edate;

  for ( i=1; i <= numTrans; i++) {

    if ( GetTransLDate(trans, i) < start ) 
      continue;
    if ( GetTransLDate(trans, i) > end ) 
      break;

    if ( GetTransType(trans, i) == T_SELL ) 
      /* - since T_SELL shares are negative. */
      withdraw += (-GetTransShares(trans, i) * GetTransNav(trans, i));

    /* divs only if reinvest off. */
    reinvest = GetTransFlag (trans, i, TRANS_REINVEST);
    if ( GetTransType(trans,i) == T_DIST && !reinvest )
      withdraw += GetTransShares(trans, i) * GetTransNav(trans, i);
  }

  /* withdraw is positive for any sales or div distributions */
  return ( withdraw );
}


/* How much gain came out of the account not including load which just
** evaporated. */
double account_distrib( int account, NAV *nav )
{
  int i;
  double withdraw = 0.0;
  long maxday = 0;

  int numTrans;
  int reinvest;
  void *trans;

  getAccount( account, ACCOUNT_NUM_TRANS, &numTrans );
  getAccount( account, ACCOUNT_TRANS, &trans );

  if ( nav != NULL )
    maxday = navToLDate( nav );
  else
    maxday = GetTransLDate( trans, numTrans );

  for ( i=1; i <= numTrans; i++) {
    if ( GetTransLDate(trans, i) > maxday ) 
      /* user specified date less then some number of transactions */
      break;

    /* divs only if reinvest off. */
    reinvest = GetTransFlag (trans, i, TRANS_REINVEST);
    if ( GetTransType(trans,i) == T_DIST && !reinvest )
      withdraw += GetTransShares(trans, i) * GetTransNav(trans, i);
  }

  /* withdraw is positive for any sales or div distributions */
  return ( withdraw );
}

double rate_distrib( int account, long sdate, long edate )
{
  int i;
  double withdraw = 0.0;

  int numTrans;
  int reinvest;
  void *trans;

  long start, end;

  getAccount( account, ACCOUNT_NUM_TRANS, &numTrans );
  getAccount( account, ACCOUNT_TRANS, &trans );

  if (sdate == FIRST_DATE)
    start = GetTransLDate( trans, 1 );
  else
    start = sdate;

  if (edate == LAST_DATE)
    end = GetTransLDate( trans, numTrans );
  else
    end = edate;

  for ( i=1; i <= numTrans; i++) {

    if ( GetTransLDate(trans, i) < start ) 
      continue;
    if ( GetTransLDate(trans, i) > end ) 
      break;

    /* divs only if reinvest off. */
    reinvest = GetTransFlag (trans, i, TRANS_REINVEST);
    if ( GetTransType(trans,i) == T_DIST && !reinvest )
      withdraw += GetTransShares(trans, i) * GetTransNav(trans, i);
  }

  /* withdraw is positive for any sales or div distributions */
  return ( withdraw );
}


/* 
** Calculate yield between start and end dates.  Return more info if 'result'
** non-NULL. 
*/
double rate_yield( int account, long sdate, long edate, YIELD *result )
{
  int i;

  double totYield = 0.0;
  double yield = 0.0;
  double shares = 0.0;

  double value = 0.0;
  double div = 0.0;

  int numTrans;
  void *trans;

  long start, end;

  getAccount( account, ACCOUNT_NUM_TRANS, &numTrans );
  getAccount( account, ACCOUNT_TRANS, &trans );

  if (sdate == FIRST_DATE)
    start = GetTransLDate( trans, 1 );
  else
    start = sdate;

  if (edate == LAST_DATE)
    end = GetTransLDate( trans, numTrans );
  else
    end = edate;

  for ( i=1; i <= numTrans; i++) {

    if ( GetTransLDate(trans, i) < start ) 
      continue;
    if ( GetTransLDate(trans, i) > end ) 
      break;

    /* Yield = dividend value/start shares/start price */
    if (GetTransType(trans,i) == T_DIST) {

      /* Dividend value/start price = shares. */
      yield = GetTransShares(trans, i);

      /* / start shares */
      shares = rate_shares (account, FIRST_DATE, GetTransLDate(trans, i-1));
      if (shares > 0.0) {

        /* Portfolio yield */
        value += (shares * GetTransNav (trans, i));
        div += (yield * GetTransNav (trans, i));

        /* Account yield */
        yield /= shares;
        totYield += yield;
#ifdef DEBUG
        fprintf (stderr, "YIELD partial %.4f\n", yield);
#endif
      }
    }
  }

  if (result) {
    double years = (double)(end - start) / 365.0;
    result->div = div;
    result->value = value;

    result->yield = totYield * 100.0;
    result->annYield = (pow (1.0+totYield, 1.0/years) - 1.0) * 100.0;
  }

#ifdef DEBUG
  fprintf (stderr, "YIELD final %.4f\n", totYield);
#endif
  return (totYield);
}
