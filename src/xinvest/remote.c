/*
  Xinvest Copyright 1997 Mark Buser,
  All Rights Reserved.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 1.2 $ $Date: 1997/05/25 23:07:40 $
*/

#include <stdio.h>

#include <X11/Intrinsic.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>

#include "account.h"

#define XINVEST_VERSION_PROP   "_XINVEST_VERSION"
#define XINVEST_COMMAND_PROP   "_XINVEST_COMMAND"
#define XINVEST_RESPONSE_PROP  "_XINVEST_RESPONSE"

static Atom XA_XINVEST_VERSION  = 0;
static Atom XA_XINVEST_COMMAND  = 0;
static Atom XA_XINVEST_RESPONSE = 0;

/* ARGSUSED */
static void remoteCommand (Widget shell, XtPointer client_data, 
                           XPropertyEvent *event)
{
  Display *dpy = XtDisplay(shell);
  Window win = XtWindow(shell);
  int status;
  Atom actual_type;
  int actual_format;
  unsigned long nitems, bytes_after;
  unsigned char *data = 0;

  char response[80];

  if (event->atom != XA_XINVEST_COMMAND)
    return;

  strcpy ( response, "500 Invalid command."); 

  status = XGetWindowProperty (dpy, win, XA_XINVEST_COMMAND,
                                   0, (65536 / sizeof (long)),
                                   True, /* delete */
                                   XA_STRING,
                                   &actual_type, &actual_format,
                                   &nitems, &bytes_after,
                                   &data);
  if (status == Success && data && *data) {
    char ticker[12];
    int month, day, year;
    double nav;

    /* "NAV ticker mm/dd/yyyy dd.dd" */
    status = sscanf ( (char *)data, "NAV %11s %d/%d/%d %lf", 
                      &(ticker[0]), &month, &day, &year, &nav);
    if (status == 5)
      strcpy ( response, "200 Ok."); 

    /* Do something useful, not found ticker is ok. */
    if ( !accountMatchTicker( ticker, month, day, year, nav) )
      strcpy ( response, "201 Ticker not found."); 

    XChangeProperty (dpy, win, XA_XINVEST_RESPONSE, XA_STRING, 8,
                     PropModeReplace, (unsigned char *) response,
                     strlen (response));
  }
}


void remoteInit (Widget Toplevel)
{
  Display *dpy = XtDisplay (Toplevel);
  Window win = XtWindow (Toplevel);
  char *version = "2.4";

  if (! XA_XINVEST_VERSION)
    XA_XINVEST_VERSION = XInternAtom (dpy, XINVEST_VERSION_PROP, False);
  if (! XA_XINVEST_COMMAND)
    XA_XINVEST_COMMAND = XInternAtom (dpy, XINVEST_COMMAND_PROP, False);
  if (! XA_XINVEST_RESPONSE)
    XA_XINVEST_RESPONSE = XInternAtom (dpy, XINVEST_RESPONSE_PROP, False);

  XtAddEventHandler (Toplevel, PropertyChangeMask, False, 
                     (XtEventHandler)remoteCommand, NULL);

  XChangeProperty (dpy, win, XA_XINVEST_VERSION, XA_STRING, 8,
                   PropModeReplace, (unsigned char *) version,
                   strlen (version));
}
