/*
  Xinvest Copyright 1997 Mark Buser,
  All Rights Reserved.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 1.9 $ $Date: 1997/09/22 22:48:39 $
*/

#include <math.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <Xm/XmAll.h>

#include "account.h"
#include "calendar.h"
#include "color.h"
#include "nav.h"
#include "pixmap.h"
#include "rate.h"
#include "status.h"
#include "tooltips.h"
#include "trans.h"
#include "xutil.h"

#define HEADER_HEIGHT         64
#define HEADER_WIDTH          650

#define REPORT_ACTIVE_ACCOUNT 1
#define REPORT_ALL_ACCOUNTS   2
#define REPORT_PORTFOLIO      4

#define REPORT_DETAIL_ON      1
#define REPORT_ANN_ON         2
#define REPORT_SINGLEWIN_ON   4

#define REPORT_PORTFOLIO_ACCOUNT      -1

#define REPORT_DETAILS        4
#define REPORT_FIELDS         7
#define REPORT_TIMES          8

/*
** Globals (all static)
*/
static Widget reportDialog = (Widget)NULL;
static Widget reportDrawing = (Widget)NULL;

static int reportAccount = 0;
static int reportTime = 0;
static int reportValue = 0;

static char datestr[40];

static struct daterange {
  long start;
  long end;
} reportDateRange[REPORT_TIMES];

static Calendar reportCal;        /* Current calendar */
static long Customstart;          /* Custom start and end dates. Copied into */
static long Customend;            /* reportDateRange in reportGenerate() */

/* 
** Forwards
*/
static void reportDraw (Widget, XtPointer, XtPointer);

/* Convert FIRST_DATE, LAST_DATE, and CURRENT_DATE into real dates */
static long convertDate (int account, long date)
{
  void *trans;
  int numTrans;

  getAccount (account, ACCOUNT_TRANS, &trans);
  getAccount (account, ACCOUNT_NUM_TRANS, &numTrans);

  if (trans == NULL)
    return 0L;        /* at least we'll notice this */

  if (date == LAST_DATE) 
    return GetTransLDate (trans, numTrans); 

  else if (date == FIRST_DATE)
    return GetTransLDate (trans, 1); 

  else if (date == CURRENT_DATE) {
    char datebuf[20];
    time_t t = time ((time_t *)NULL);
    struct tm *today = localtime (&t);

    strftime (datebuf, 20, "%m/%d/%Y", today);
    return (calStrtoL (datebuf));

  } else
    return date;
}


/* Initialize calendar */
static void reportInit()
{
  Customstart = convertDate(activeAccount(), FIRST_DATE);
  Customend = convertDate(activeAccount(), CURRENT_DATE);
}


/* ARGSUSED */
void reportSetTime (Widget w, int which, XmToggleButtonCallbackStruct *calldata)
{
  static Boolean first = True;
  Widget ok;

  if (calldata->set == XmSET) {
    reportTime |= (1 << which);
    if (which == REPORT_TIMES-1) {
      XtVaGetValues (w, XmNuserData, &ok, NULL);
      XtSetSensitive (ok, True);

      if (first) {
        reportInit();
        first = False;
      }
    }
  } else {
    reportTime &= ~(1 << which);
    if (which == REPORT_TIMES-1) {
      XtVaGetValues (w, XmNuserData, &ok, NULL);
      XtSetSensitive (ok, False);
    }
  }

  XtVaGetValues (XtParent(w), XmNuserData, &ok, NULL);
  if (ok && reportAccount && reportTime)
    XtSetSensitive (ok, True);
  else
    XtSetSensitive (ok, False);

}

/* ARGSUSED */
void reportSetValue (Widget w, int which, 
                     XmToggleButtonCallbackStruct *calldata)
{
  if (calldata->set == XmSET)
    reportValue |= (1 << which);
  else
    reportValue &= ~(1 << which);
}

/* ARGSUSED */
void reportSetAccount (Widget w, int which, 
                       XmToggleButtonCallbackStruct *calldata)
{
  Widget button;
  char name[10];

  if (calldata->set == XmSET) {
    reportAccount |= (1 << which);

    /* Can't have 'this account' AND 'all accounts' set simultaneously */
    if (which == 0 || which == 1) {
      reportAccount &= ~(1 << (1-which));
      sprintf (name, "button_%d", 1-which);
      if ((button = XtNameToWidget (XtParent(w), name)) != (Widget)NULL)
        XmToggleButtonSetState (button, False, False);
    }
  } else {
    reportAccount &= ~(1 << which);
  }

  XtVaGetValues (XtParent(w), XmNuserData, &button, NULL);
  if (button && reportAccount && reportTime)
    XtSetSensitive (button, True);
  else
    XtSetSensitive (button, False);
}


/* Redisplay the custom calendar and/or report.  Useful on account switch */
void displayReport (int account)
{
  void *trans;

  /* If user hasn't touched calendar, change default dates to new account */
  if (!calUserTouched(&reportCal)) {
    getAccount (account, ACCOUNT_TRANS, &trans);
    if (trans) {
      if (Customstart == -1)   /* Displaying start */
        calLtoCal (&reportCal, GetTransLDate (trans, 1));
      else                     /* Displaying end */
        calLtoCal (&reportCal, convertDate( account, CURRENT_DATE));
    }
    reportInit();
  }
  calSetDate (&reportCal);

  /* Force new drawing if in single window mode */
  if (reportDrawing && reportValue & REPORT_SINGLEWIN_ON)
    reportDraw (reportDrawing, (XtPointer)NULL, (XtPointer)NULL);
}

void reportCustomStartEnd (Widget w, XtPointer client_data, 
                           XmAnyCallbackStruct *call_data)
{
  long date = calCaltoL (&reportCal);

  if ((int)client_data == 0) {
    Customend = date;                     /* Save end */
    calLtoCal (&reportCal, Customstart);  /* Display start */
    Customstart = -1L;                    /* Start is in reportCal */
  } else {
    Customstart = date;
    calLtoCal (&reportCal, Customend);
    Customend = -1L;
  }
   
  calSetDate (&reportCal);
}

static void reportCustomWarp (Widget w, XtPointer client_data,
                       XmPushButtonCallbackStruct *call_data)
{
  int account = activeAccount();
  void *trans;
  int numTrans, i;

  long currentDate = calCaltoL (&reportCal);
  long nextDate;

  getAccount (account, ACCOUNT_TRANS, &trans);
  getAccount (account, ACCOUNT_NUM_TRANS, &numTrans);

  if (trans == NULL)
    return;

  switch ((int) client_data) {
  
  case 0: /* previous transaction */
          for (i=numTrans; i >= 1; i--) {
            nextDate = GetTransLDate (trans, i);
            if (nextDate < currentDate)
              break;
          }
          break;
  case 1: /* next transaction */
          for (i=1; i <= numTrans; i++) {
            nextDate = GetTransLDate (trans, i);
            if (nextDate > currentDate)
              break;
          }
          break;
  case 2: /* first transaction */
          nextDate = GetTransLDate (trans, 1);
          break;
  case 3: /* last transaction */
          nextDate = GetTransLDate (trans, numTrans);
          break;
  default: break;
  }

  if (nextDate && nextDate != currentDate) {
    calLtoCal (&reportCal, nextDate);
    calSetDate (&reportCal);
  }
}

void reportCustomTime (Widget w, XtPointer client_data, 
                       XmPushButtonCallbackStruct *call_data)
{
  static Widget form = (Widget)NULL;
  Widget calendar, optmenu, button1, button2;
  Widget reportform = (Widget)client_data;
  Boolean mapped;

  if (form == (Widget)NULL) {

    /* Starting date */
    calLtoCal (&reportCal, convertDate (activeAccount(), 
                                        reportDateRange[REPORT_TIMES-1].start));

    /* Same size as parent */
    form = XtVaCreateWidget ("reportform2", 
                       xmFormWidgetClass, XtParent(reportform),
                       XmNmappedWhenManaged, False,
                       XmNtopAttachment, XmATTACH_FORM,
                       XmNleftAttachment, XmATTACH_FORM,
                       XmNrightAttachment, XmATTACH_FORM,
                       XmNbottomAttachment, XmATTACH_FORM,
                       NULL);
    calendar = calCreate (form, &reportCal);
    XtVaSetValues (calendar, 
                       XmNtopAttachment, XmATTACH_FORM,
                       XmNleftAttachment, XmATTACH_FORM,
                   NULL);
    XtManageChild (calendar);
    optmenu = XmVaCreateSimpleOptionMenu (form, "reportoptmenu",
                       NULL, (KeySym) NULL, 0,
                       (XtCallbackProc) reportCustomStartEnd,
                       XmVaPUSHBUTTON, NULL, NULL, NULL, NULL,
                       XmVaPUSHBUTTON, NULL, NULL, NULL, NULL,
                       XmNtopAttachment, XmATTACH_FORM,
                       XmNleftAttachment, XmATTACH_WIDGET,
                       XmNleftWidget, calendar,
                       NULL);
    XtDestroyWidget(XmOptionLabelGadget(optmenu));
    XtManageChild (optmenu);
    /* previous transaction button */
    button1 =  XtVaCreateManagedWidget ("prevTrans",
         xmPushButtonWidgetClass, form,
         XmNlabelType,       XmPIXMAP,
         XmNlabelPixmap,     GetPixmap(PPREVTRANS, NORMAL),
         XmNlabelInsensitivePixmap,   GetPixmap(PPREVTRANS, NORMAL),
         XmNsensitive,       True,
         XmNtopAttachment,   XmATTACH_WIDGET,
         XmNtopWidget,       optmenu,
         XmNleftAttachment,  XmATTACH_WIDGET,
         XmNleftWidget,      calendar,
         NULL);
    XtAddCallback (button1, XmNactivateCallback, 
                   (XtCallbackProc)reportCustomWarp, (XtPointer)0 );
    installToolTips (button1);
    /* next transaction button */
    button2 = XtVaCreateManagedWidget ("nextTrans",
         xmPushButtonWidgetClass,  form,
         XmNlabelType,             XmPIXMAP,
         XmNlabelPixmap,           GetPixmap(PNEXTTRANS, NORMAL),
         XmNlabelInsensitivePixmap,   GetPixmap(PNEXTTRANS, NORMAL),
         XmNsensitive,             True,
         XmNtopAttachment,         XmATTACH_WIDGET,
         XmNtopWidget,             optmenu,
         XmNleftAttachment,        XmATTACH_WIDGET,
         XmNleftWidget,            button1,
         NULL);
    XtAddCallback (button2, XmNactivateCallback, 
                   (XtCallbackProc)reportCustomWarp, (XtPointer)1 );
    installToolTips (button2);
    /* 1st transaction button */ 
    button1 =  XtVaCreateManagedWidget ("firstTrans",
         xmPushButtonWidgetClass, form,
         XmNlabelType,            XmPIXMAP,
         XmNlabelPixmap,          GetPixmap(PFIRST, NORMAL),
         XmNlabelInsensitivePixmap,   GetPixmap(PFIRST, NORMAL),
         XmNsensitive,            True,
         XmNtopAttachment,        XmATTACH_WIDGET,
         XmNtopWidget,            button1,
         XmNleftAttachment,       XmATTACH_WIDGET,
         XmNleftWidget,           calendar,
         NULL);
    XtAddCallback (button1, XmNactivateCallback, 
                   (XtCallbackProc)reportCustomWarp, (XtPointer)2 );
    installToolTips (button1);
    /* last transaction button */
    button2 =  XtVaCreateManagedWidget ("lastTrans",
         xmPushButtonWidgetClass,  form,
         XmNlabelType,             XmPIXMAP,
         XmNlabelPixmap,           GetPixmap(PLAST, NORMAL),
         XmNlabelInsensitivePixmap,   GetPixmap(PLAST, NORMAL),
         XmNsensitive,             True,
         XmNtopAttachment,         XmATTACH_OPPOSITE_WIDGET,
         XmNtopWidget,             button1,
         XmNleftAttachment,        XmATTACH_WIDGET,
         XmNleftWidget,            button1,
         NULL);
    XtAddCallback (button2, XmNactivateCallback, 
                   (XtCallbackProc)reportCustomWarp, (XtPointer)3 );
    installToolTips (button2);

    /* Page return button */
    button1 = XtVaCreateManagedWidget (
                "Screenbut", xmPushButtonWidgetClass, form,
                XmNlabelType,    XmPIXMAP,
                XmNlabelPixmap,  GetPixmap(PPREV,NORMAL),
                XmNlabelInsensitivePixmap, GetPixmap(PPREV,INSENS),
                XmNrightAttachment, XmATTACH_FORM,
                XmNbottomAttachment, XmATTACH_FORM,
                NULL);
    installToolTips (button1);
    XtAddCallback (button1, XmNactivateCallback, 
                   (XtCallbackProc)reportCustomTime, (XtPointer)reportform);

    XtManageChild (form);

    XtVaSetValues (form, XmNnoResize, True, NULL);
    CenterWidget (XtParent(form), form);

  }

  XtVaGetValues (form, XmNmappedWhenManaged, &mapped, NULL);
  if (mapped == False) {
    /* Turn on second page */
    XtSetMappedWhenManaged (reportform, False);
    XtSetMappedWhenManaged (form, True);
  } else {
    /* Return to 1st page */
    XtSetMappedWhenManaged (reportform, True);
    XtSetMappedWhenManaged (form, False);
  }
}

struct report_attrib {
 Display *dpy;
 XFontStruct *font;
 GC gc;
 Pixmap pix;
 int x, y;
 Pixel fg, bg, shad, hilite;
 int shadThick;
} report_attrib;



static void reportDrawCircle (int neg, struct report_attrib *attrib)
{
 int symbol_border = (int)(0.2*(attrib->font->ascent+attrib->font->descent));
 int symbol_size =   (int)(0.6*(attrib->font->ascent+attrib->font->descent));

 XSetForeground (attrib->dpy, attrib->gc, attrib->shad );
 XFillArc (attrib->dpy, attrib->pix, attrib->gc,
           attrib->x - symbol_size -symbol_border +1, 
           attrib->y + symbol_border +1,
           symbol_size, symbol_size, 0, 360*64 );

 if (neg)
   XSetForeground (attrib->dpy, attrib->gc, GetColor(RED) );
 else
   XSetForeground (attrib->dpy, attrib->gc, GetColor(GREEN) );
 XFillArc (attrib->dpy, attrib->pix, attrib->gc,
           attrib->x - symbol_size - symbol_border, attrib->y + symbol_border,
           symbol_size, symbol_size, 0, 360*64 );

 XSetForeground (attrib->dpy, attrib->gc, attrib->fg );
}

static void reportDrawArrow (int neg, struct report_attrib *attrib)
{
  int one_third, one_half, two_third;
  int symbol_border = (int)(0.1*(attrib->font->ascent+attrib->font->descent));
  int symbol_size =   (int)(0.8*(attrib->font->ascent+attrib->font->descent));
  XPoint point[7]; 
 
  one_third = symbol_size/3;
  two_third = one_third *2;
  one_half = symbol_size/2;
 
  /******************
           ^6
         /   \
      0--1   4--5
         |___|
         2   3
  *******************/
  point[0].x = attrib->x - symbol_size - symbol_border +1;
  point[0].y = (neg) ? (attrib->y + symbol_border + one_third +1):
                       (attrib->y + symbol_border + two_third +1);
  point[1].x = one_third;
  point[1].y = 0;
  point[2].x = 0;
  point[2].y = (neg)?-one_third:one_third;
  point[3].x = one_third;
  point[3].y = 0;
  point[4].x = 0;
  point[4].y = -point[2].y;
  point[5].x = point[1].x;
  point[5].y = 0;
  point[6].x = -one_half;
  point[6].y = (neg)?two_third:-two_third;

  XSetForeground (attrib->dpy, attrib->gc, attrib->shad );
  XFillPolygon (attrib->dpy, attrib->pix, attrib->gc,
                point, 7, Convex, CoordModePrevious );

  point[0].x--;
  point[0].y--;
  if (neg)
    XSetForeground (attrib->dpy, attrib->gc, GetColor(RED) );
  else
    XSetForeground (attrib->dpy, attrib->gc, GetColor(GREEN) );
  XFillPolygon (attrib->dpy, attrib->pix, attrib->gc,
                point, 7, Convex, CoordModePrevious );

  XSetForeground (attrib->dpy, attrib->gc, attrib->fg );
}


static void reportTimeVal (int account, struct report_attrib *attrib)
{
  extern Widget Timeform;
  Widget w;

  int i, j, k;
  char line[80];

  double   value = 0.0, cost = 0.0, debit = 0.0, fee = 0.0, distrib = 0.0;
  double   prevValue = 0.0, prevCost = 0.0, prevDebit = 0.0, prevFee = 0.0; 
  double   prevDistrib = 0.0;

  long     daymin, daymax;

  Pixel color;

  XSetFont (attrib->dpy, attrib->gc, attrib->font->fid);

  /* 
  ** Generate strings 
  */

  /* Draw title */
  attrib->x = 0.1 * HEADER_WIDTH;
  color = attrib->hilite;
  if (account == REPORT_PORTFOLIO_ACCOUNT) {
    strcpy (line, "Open Account(s)");
  } else {
    char *name = createAccountName (account);
    sprintf (line, "%.80s", name);
    XtFree (name);
  }
  XSetForeground (attrib->dpy, attrib->gc, attrib->shad );
  XDrawString (attrib->dpy, attrib->pix, attrib->gc,
               attrib->x + attrib->shadThick, 
               attrib->y + attrib->font->ascent + attrib->shadThick,
               line, strlen (line));
  XSetForeground (attrib->dpy, attrib->gc, color );
  XDrawString (attrib->dpy, attrib->pix, attrib->gc,
               attrib->x, attrib->y + attrib->font->ascent,
               line, strlen (line));
  attrib->x += XTextWidth (attrib->font, line, strlen(line)); 

  /* Draw date */
  XSetForeground (attrib->dpy, attrib->gc, attrib->shad);
  XDrawString (attrib->dpy, attrib->pix, attrib->gc,
               attrib->x + attrib->shadThick, 
               attrib->y + attrib->font->ascent + attrib->shadThick,
               datestr, strlen(datestr));
  XSetForeground (attrib->dpy, attrib->gc, attrib->fg);
  XDrawString (attrib->dpy, attrib->pix, attrib->gc,
               attrib->x, attrib->y + attrib->font->ascent,
               datestr, strlen (datestr));


  attrib->y += attrib->font->ascent + attrib->font->descent;

  /* Draw separator */
  attrib->x = 0.1 * HEADER_WIDTH;
  attrib->y += attrib->shadThick;
  XSetForeground (attrib->dpy, attrib->gc, attrib->shad);
  XDrawLine (attrib->dpy, attrib->pix, attrib->gc, 
             attrib->x-5, attrib->y, 0.9*HEADER_WIDTH, attrib->y);
  attrib->y -= attrib->shadThick;
  XSetForeground (attrib->dpy, attrib->gc, attrib->fg);
  XDrawLine (attrib->dpy, attrib->pix, attrib->gc, 
             attrib->x-5, attrib->y, 0.9*HEADER_WIDTH, attrib->y);

  attrib->y += (attrib->font->descent - 2*attrib->shadThick);

  /* Draw date, price, shares */
  color = attrib->fg;
  {
    NAV *navp;
    float  p = 0.0;
    double v = 0.0;
    char strval[40];
    char strprice[40];
    char strshares[40];

    if (account == REPORT_PORTFOLIO_ACCOUNT) {
      strcpy (strprice, "N/A");
      strcpy (strshares,"N/A");
      for (k=1; k <= numAccounts(); k++) {
        getAccount( k, ACCOUNT_NAV, &navp );
        v += rate_value (k, FIRST_DATE, navToLDate(navp));
      }
#ifdef STRFMON
      strfmon (strval, 40, "%.2n", v);
#else
      sprintf (strval, "$%.2f", v);
#endif
    } else {
      getAccount( account, ACCOUNT_NAV, &navp );
      getNav( navp, NAV_VALUE, &p);
      v = rate_value (account, FIRST_DATE, navToLDate(navp));
#ifdef STRFMON
      strfmon (strprice, 40, "%.4n", (double)p);
      strfmon (strval, 40, "%.2n", v);
#else
      sprintf (strprice, "$%.4f", (double)p );
      sprintf (strval, "$%.2f", v);
#endif
      sprintf (strshares, "%.3f", rate_shares (account, FIRST_DATE, LAST_DATE));
    }

    sprintf (line, "Value: %-15.15s Shares: %-15.15s Price: %-15.15s", 
             strval, strshares, strprice);
  }
  XSetForeground (attrib->dpy, attrib->gc, attrib->shad );
  XDrawString (attrib->dpy, attrib->pix, attrib->gc,
               attrib->x + attrib->shadThick, 
               attrib->y + attrib->font->ascent + attrib->shadThick,
               line, strlen (line) );
  XSetForeground ( attrib->dpy, attrib->gc, color );
  XDrawString ( attrib->dpy, attrib->pix, attrib->gc,
                attrib->x, attrib->y + attrib->font->ascent,
                line, strlen (line));
  attrib->y += attrib->font->ascent + attrib->font->descent;

  /* Draw selected times */
  for (i=0; i < REPORT_TIMES; i++) {
    if (reportTime & (1 << i)) {
      char timename[20];
      XmString timeframe;
      char *timestr;

      attrib->x = 0.1*HEADER_WIDTH;

      sprintf (timename, "button_%d", i);
      if ((w = XtNameToWidget (Timeform, timename))) {

        /* Calculate values  */
        value = 0.0;   prevValue = 0.0;
        cost = 0.0;    prevCost = 0.0;
        debit = 0.0;   prevDebit = 0.0;
        fee = 0.0;     prevFee = 0.0;
        distrib = 0.0; prevDistrib = 0.0;
        daymin = LONG_MAX; daymax = 0L;

        if (account == REPORT_PORTFOLIO_ACCOUNT) {
          for (k=1; k <= numAccounts(); k++) {
            if (convertDate (k, reportDateRange[i].end) > daymax)
              daymax = convertDate (k, reportDateRange[i].end);
            if (convertDate(k, reportDateRange[i].start) < daymin)
              daymin = convertDate(k, reportDateRange[i].start);
            value     += rate_value (k, FIRST_DATE, reportDateRange[i].end );
            prevValue += rate_value (k, FIRST_DATE, 
                                 convertDate (k, reportDateRange[i].start) -1L);
            cost     += rate_cost (k, FIRST_DATE, reportDateRange[i].end );
            prevCost += rate_cost (k, FIRST_DATE, 
                                 convertDate (k, reportDateRange[i].start) -1L);
            debit += rate_withdrawal (k, FIRST_DATE, reportDateRange[i].end);
            prevDebit += rate_withdrawal (k, FIRST_DATE, 
                                 convertDate (k, reportDateRange[i].start) -1L);
            fee     += rate_fee (k, FIRST_DATE, reportDateRange[i].end); 
            prevFee += rate_fee (k, FIRST_DATE, 
                                 convertDate (k, reportDateRange[i].start) -1L);
            distrib += rate_distrib (k, FIRST_DATE, reportDateRange[i].end);
            prevDistrib += rate_distrib( k, FIRST_DATE, 
                                 convertDate (k, reportDateRange[i].start) -1L);
          }
        } else {
          daymax = convertDate (account, reportDateRange[i].end);
          daymin = convertDate (account, reportDateRange[i].start);
          value   = rate_value (account, FIRST_DATE, reportDateRange[i].end);
          prevValue = rate_value (account, FIRST_DATE, convertDate (account, 
                                                 reportDateRange[i].start) -1L);
          cost     = rate_cost (account, FIRST_DATE, reportDateRange[i].end);
          prevCost = rate_cost (account, FIRST_DATE, 
                           convertDate (account, reportDateRange[i].start) -1L);
          debit     = rate_withdrawal (account, FIRST_DATE,
                                       reportDateRange[i].end); 
          prevDebit = rate_withdrawal (account, FIRST_DATE, 
                           convertDate (account, reportDateRange[i].start) -1L);
          fee     = rate_fee (account, FIRST_DATE, reportDateRange[i].end); 
          prevFee = rate_fee (account, FIRST_DATE, 
                           convertDate (account, reportDateRange[i].start) -1L);
          distrib     = rate_distrib (account, FIRST_DATE,
                                      reportDateRange[i].end);
          prevDistrib = rate_distrib (account, FIRST_DATE, 
                           convertDate (account, reportDateRange[i].start) -1L);
        }

#ifdef DEBUG
        fprintf( stderr, 
                 "daymax -> %ld\n"
                 "daymin -> %ld\n"
                 "value     -> %f\n"
                 "prevalue  -> %f\n"
                 "cost      -> %f\n"
                 "precost   -> %f\n"
                 "debit     -> %f\n"
                 "predebit  -> %f\n"
                 "fee       -> %f\n"
                 "prefee    -> %f\n"
                 "distrib   -> %f\n"
                 "predistrib-> %f\n",
                 daymax, daymin, value, prevValue, cost, prevCost, debit,
                 prevDebit, fee, prevFee, distrib, prevDistrib );
#endif

        /* Draw time frame */
        XtVaGetValues (w, XmNlabelString, &timeframe, NULL);
        XmStringGetLtoR (timeframe, XmFONTLIST_DEFAULT_TAG, &timestr);

        if (i == REPORT_TIMES-1) {        /* Custom */
          char *start;                    
          start = XtNewString (calPrint (calLtoStr(reportDateRange[i].start)));
          /* 
          ** Can't call calLtoStr, etc twice; it returns pointer to static.
          */
          sprintf (line, "%.19s  (%.20s - %.20s)", timestr, start,
                   calPrint (calLtoStr(reportDateRange[i].end)) );
          XtFree (start);
        } else if (i == REPORT_TIMES-2)     /* YTD */
          sprintf (line, "%.19s  (%.20s)", timestr, 
                   calPrint(calLtoStr(daymin)));
        else
          sprintf (line, "%.19s", timestr);
        XtFree (timestr);

        XSetForeground (attrib->dpy, attrib->gc, attrib->shad );
        XDrawString (attrib->dpy, attrib->pix, attrib->gc,
                     attrib->x + attrib->shadThick, 
                     attrib->y + attrib->font->ascent + attrib->shadThick,
                     line, strlen (line) );

        XSetForeground ( attrib->dpy, attrib->gc, color );
        XDrawString (attrib->dpy, attrib->pix, attrib->gc,
                     attrib->x, attrib->y + attrib->font->ascent,
                       line, strlen (line) );
        attrib->y += attrib->font->ascent + attrib->font->descent;

        /* Draw selected values */
        attrib->x = 0.2*HEADER_WIDTH;
        for (j=0; j < REPORT_FIELDS; j++) {
          char *vallabel;
          char *valformats[] = { "%.2f%%", ">1000%%",
                              "%.2f%% (%.2f%% Annualized)",
                              ">1000%% (%.2f%% Annualized)",
                              "%.2f%% (>1000%% Annualized)",
                              ">1000%% (>1000%% Annualized)"};
          char valval[40];

          int n, textWidth;

          attrib->x = 0.2*HEADER_WIDTH;

          /* If details not on skip them */
          if ((reportValue & 0x1) == 0 && j < 4)
            continue;

          switch (j) {
            case 0:  /* VALUE */
                     vallabel = "Value";
                     reportDrawArrow (value-prevValue<0.0, attrib);
#ifdef STRFMON
                     strfmon (valval, 50, "%.2n", value-prevValue);
#else
                     sprintf (valval, "$%.2f", value-prevValue);
#endif
                     break;

            case 1:  /* DEBIT during period */
                     vallabel = "Debit";
                     reportDrawCircle ((debit-prevDebit<0.0), attrib);
#ifdef STRFMON
                     strfmon ( valval, 40,"%.2n", debit-prevDebit);
#else
                     sprintf ( valval, "$%.2f", debit-prevDebit);
#endif
                     break;

            case 2:  /* COST */
                     {
                       double realCost = cost;
                       double realPCost = prevCost;

                       vallabel = "Cost";
                       reportDrawCircle ((realCost-realPCost)>0.0, attrib);
#ifdef STRFMON
                       strfmon (valval, 40,"%.2n", realCost-realPCost);
#else
                       sprintf (valval, "$%.2f", realCost-realPCost);
#endif
                     }
                     break;

            case 3:  /* FEE during period */
                     if (fee > 0.0)
                       fee = -fee;
                     if (prevFee > 0.0)
                       prevFee = -prevFee;

                     vallabel = "Fee";
                     reportDrawCircle ((fee-prevFee<0.0), attrib);
#ifdef STRFMON
                     strfmon ( valval, 40,"%.2n", fee-prevFee);
#else
                     sprintf ( valval, "$%.2f", fee-prevFee);
#endif
                     break;

            case 4:  /* TR */
                     {
                       double rate, annRate;
                       double years = (double)(daymax - daymin) / 365.0;
                       double realCost = cost - prevCost;
                       double realValue = (value+debit) - (prevValue+prevDebit);

                       if ( realCost == (double)0.0) {
                         if (realValue == (double)0.0)
                           rate = annRate = 0;
                         else
                           rate = annRate = 1001.0; /* trigger inf. format */
                       } else {
                         rate = (realValue - realCost) / realCost;
                         annRate = (pow (1.0+rate, 1.0/years) - 1.0) * 100.0;
                         rate *= 100;
                       }
                       {
                         int index = 0;
                         if (reportValue & 2) 
                           index = 2 + 2*(annRate> 1000.0);
                         index += (rate > 1000.0);
                         
                         vallabel = "TR";
                         reportDrawArrow ((rate<0.0), attrib);
                         sprintf (valval, valformats[index],
                                  rate, annRate);
                       }
                     }
                     break;

            case 5:  /* IRR */
                     {
                       double rate;

                       rateClrAccount();

                       if (account != REPORT_PORTFOLIO_ACCOUNT) {
                         rate_add_account (account, 
                                           reportDateRange[i].start,
                                           reportDateRange[i].end);
                       } else {
                         for (k=1; k<=numAccounts(); k++) {
                           rate_add_account (k, 
                                             reportDateRange[i].start,
                                             reportDateRange[i].end);
                         }
                       }
                       rate = rate_IRR (value + distrib);
                       {
                         int index = (rate > 1000.0);
                         
                         vallabel = "IRR";
                         reportDrawArrow ((rate<0.0), attrib);
                         sprintf( valval, valformats[index], rate);
                       }
                     }
                     break;

            case 6:  /* YIELD */
                     {
                       YIELD result = {0.0, 0.0, 0.0, 0.0};
                       if (account != REPORT_PORTFOLIO_ACCOUNT) {
                         rate_yield (account, reportDateRange[i].start,
                                     reportDateRange[i].end, &result);
                       } else {
                         YIELD acct;
                         double years = (double)(daymax - daymin) / 365.0;
                         for (k = 1; k <= numAccounts(); k++) {
                           rate_yield (k, reportDateRange[i].start,
                                       reportDateRange[i].end, &acct);
                           result.div += acct.div;
                           result.value += acct.value;
                         }
                         /* Guard against divide by zero */
                         if (result.value == 0.0) result.value = 1.0;
                         result.yield = result.div/result.value;
                         result.annYield = 
                            (pow (1.0+result.yield, 1.0/years) - 1.0) * 100.0;
                         result.yield *= 100.0;
                       }
                       {
                         int index = 0;
                         if (reportValue & 2) 
                           index = 2 + 2*(result.annYield> 1000.0);
                         index += (result.yield > 1000.0);
                         
                         vallabel = "Yield";
                         reportDrawArrow (result.yield<0.0, attrib);
                         sprintf (valval, valformats[index],
                                  result.yield, result.annYield);
                       }
                     }
                     break;

            default:
                     vallabel = "Unknown Field";
                     sprintf(valval, "%d", j);
                     break;
          }

          /* No tabs in Xlib */
          n = 0;
          do {
            sprintf (line, "%s%*s%s", vallabel, n++, " ", valval);
            textWidth = XTextWidth (attrib->font, line, strlen(line));
          } while (strlen(line) < 79 && textWidth < 0.5*HEADER_WIDTH);

          XSetForeground (attrib->dpy, attrib->gc, attrib->shad );
          XDrawString (attrib->dpy, attrib->pix, attrib->gc,
                       attrib->x + attrib->shadThick, 
                       attrib->y + attrib->font->ascent + attrib->shadThick,
                       line, strlen (line) );

          XSetForeground ( attrib->dpy, attrib->gc, color );
          XDrawString (attrib->dpy, attrib->pix, attrib->gc,
                       attrib->x, attrib->y + attrib->font->ascent,
                       line, strlen (line) );
          attrib->y += attrib->font->ascent + attrib->font->descent;
        } /* for vals */
      } /* if widget found */
    } /* if time on */
  } /* for time */
  attrib->y += attrib->font->ascent;  /* realign on even font height boundary */

}


/* ARGSUSED */
static void reportRedraw (Widget w, XtPointer client_data, XtPointer call_data)
{
  Dimension width, height;
  struct report_attrib *attrib;

  XtVaGetValues ( w, XmNwidth, &width,
                     XmNheight, &height,
                     XmNuserData, &attrib,
                  NULL);
  if (attrib && attrib->pix && attrib->gc)
    XCopyArea (XtDisplay(w), attrib->pix, XtWindow(w), attrib->gc,
               0, 0, width, height, 0, 0 );
}


/* ARGSUSED */
static void reportDraw (Widget w, XtPointer client_data, XtPointer call_data)
{
  int accounts = 0, values = 0, times = 0;
  int lines = 0;

  int i;

  Display *dpy = XtDisplay(w);
  GC gc;
  extern XFontStruct *large;

  Pixmap draw;
  Dimension height, width;

  struct report_attrib *attrib;
  attrib = (struct report_attrib *) XtCalloc (1, sizeof(struct report_attrib));

  /* 
  ** How many lines 
  */
  if (reportAccount & REPORT_ALL_ACCOUNTS)
    accounts += numAccounts();
  if (reportAccount & REPORT_ACTIVE_ACCOUNT)
    accounts++;
  if (reportAccount & REPORT_PORTFOLIO)
    accounts++;

  for (i=0; i < REPORT_TIMES; i++) {
   if (reportTime & (1 << i))
     times++;
  }

  values = REPORT_FIELDS - REPORT_DETAILS;
  if (reportValue & 1)
    values += REPORT_DETAILS;

  if (accounts == 0 || times == 0) {
    write_status ("No account(s) and/or time frame(s)\n"
                  "selected, report not generated.", ERR);
    return;
  }

  lines = accounts*3 + accounts * times * (values +1);

  setBusyCursor (w, True);

  /* Create pixmap */
  height = 2*HEADER_HEIGHT + lines*(large->ascent+large->descent);
  width = HEADER_WIDTH;
  draw = XCreatePixmap (dpy, RootWindowOfScreen (XtScreen(w)),
                        width, height, DefaultDepthOfScreen (XtScreen(w)));

  /* Clear background, copy header */
  XtVaGetValues (w, 
                 XmNbackground, &attrib->bg,
                 XmNforeground, &attrib->fg,
                 XmNhighlightColor, &attrib->hilite,
                 NULL);
  if (attrib->shadThick == 0)
    attrib->shadThick = 1;

  if (isColorVisual(dpy)) {
    Pixel top, bot;

    XmGetColors (XtScreen(w), GetColormap(), attrib->bg,
                 NULL, &top, &bot, NULL);
    if (isDarkPixel (dpy, attrib->fg))
      attrib->shad = top;
    else
      attrib->shad = bot;
  } else
    attrib->shad = attrib->bg;

  gc = XCreateGC (dpy, RootWindowOfScreen(XtScreen(w)), 0, NULL);
  XSetLineAttributes (dpy, gc, attrib->shadThick, 
                      LineSolid, CapNotLast, JoinBevel);

  XSetTile (dpy, gc, GetPixmap(PSTIPPLE, NORMAL));
  XSetFillStyle (dpy, gc, FillTiled );
  XFillRectangle (dpy, draw, gc, 0, 0, width, height);
  XSetFillStyle (dpy, gc, FillSolid);

  XSetForeground (dpy, gc, attrib->bg);
  XFillRectangle (dpy, draw, gc, 
	         (int)(0.1*HEADER_WIDTH-5), (int)(HEADER_HEIGHT-5), 
                 (unsigned)(0.8*width+5), (unsigned)(height-2*HEADER_HEIGHT+5)); 

  XSetForeground (dpy, gc, attrib->shad);
  XDrawRectangle (dpy, draw, gc, 
	         (int)(0.1*HEADER_WIDTH-5+1), (int)(HEADER_HEIGHT-5+1), 
                 (unsigned)(0.8*width+5), (unsigned)(height-2*HEADER_HEIGHT+5)); 
  XSetForeground (dpy, gc, attrib->fg);
  XDrawRectangle (dpy, draw, gc, 
		 (int)(0.1*HEADER_WIDTH-5), (int)(HEADER_HEIGHT-5), 
                 (unsigned)(0.8*width+5), (unsigned)(height-2*HEADER_HEIGHT+5)); 

  XSetLineAttributes (dpy, gc, attrib->shadThick, 
                      LineSolid, CapNotLast, JoinBevel);
  
  attrib->dpy = dpy;
  attrib->gc = gc;
  attrib->pix = draw;
  attrib->font = large;
  attrib->x = 0.1*HEADER_WIDTH;
  attrib->y = HEADER_HEIGHT;

  /* Write report */
  if (reportAccount & REPORT_ACTIVE_ACCOUNT)
    reportTimeVal (activeAccount(), attrib);
  if (reportAccount & REPORT_ALL_ACCOUNTS) {
    for (i=1; i <= numAccounts(); i++)
      reportTimeVal (i, attrib);
  }
  if (reportAccount & REPORT_PORTFOLIO)
    reportTimeVal (REPORT_PORTFOLIO_ACCOUNT, attrib);

  /* Write to screen */
  XtVaSetValues ( w, XmNwidth, width,
                     XmNheight, height,
                  NULL);
  XCopyArea (dpy, draw, XtWindow(w), gc,
             0, 0, width, height, 0, 0 );

  /* Save for redraw */
  XtVaSetValues (w, XmNuserData, attrib, NULL);

  /* Remove initializer, replace with copier */
  XtRemoveCallback (w, XmNexposeCallback, reportDraw, (XtPointer)NULL);
  XtAddCallback (w, XmNexposeCallback, 
                 (XtCallbackProc)reportRedraw, (XtPointer)NULL);

  setBusyCursor (w, False);
}


/* ARGSUSED */
void reportDestroy (Widget w, Widget client_data, XtPointer call_data)
{
  struct report_attrib *attrib;

  XtVaGetValues (client_data, XmNuserData, &attrib, NULL);

  if (attrib) {
    if (attrib->pix)
      XFreePixmap (XtDisplay(w), attrib->pix);
    if (attrib && attrib->gc)
      XFreeGC (XtDisplay(w), attrib->gc);
    XtFree ( (char *)attrib);
  }
  XtDestroyWidget (GetTopShell(w));
  reportDialog = (Widget)NULL;
  reportDrawing = (Widget)NULL;
}


/* ARGSUSED */
void reportGenerate (Widget w, XtPointer client_data, XtPointer calldata)
{
  Widget scroll;
  Widget pane, form, button;

  int i;

  time_t t = time ((time_t *)NULL);
  struct tm *today = localtime (&t);
  char datebuf[40];
  long ldate;

  /*
  ** Set time ranges 
  */
  strftime (datebuf, 20, "%m/%d/%Y", today);
  ldate = calStrtoL (datebuf);
  strftime (datestr, 40, " - %x", today);

  for (i=0; i < REPORT_TIMES; i++) {
    reportDateRange[i].end = ldate;
 
    switch (i) {
      case 0:  reportDateRange[i].start = ldate - 1L; break;
      case 1:  reportDateRange[i].start = ldate - 7L; break;
      case 2:  reportDateRange[i].start = ldate - 30L; break;
      case 3:  reportDateRange[i].start = ldate - (365L/4L); break;
      case 4:  reportDateRange[i].start = ldate - (long) (today->tm_yday); 
               break;
      case 5:  reportDateRange[i].start = ldate - 365L; break;
      case 6:  reportDateRange[i].start = FIRST_DATE; break;
      case 7:  if (Customstart == -1L) {
                 reportDateRange[i].start = calCaltoL(&reportCal);
                 reportDateRange[i].end = Customend;
               } else if (Customend == -1L) {
                 reportDateRange[i].start = Customstart;
                 reportDateRange[i].end = calCaltoL(&reportCal);
               } else {
                 reportDateRange[i].start = Customstart;
                 reportDateRange[i].end = Customend;
               }
               break;
    }
  }

  /* Check custom time for errors if enabled */
  i = REPORT_TIMES-1;
  if (reportTime & (1 << i)) {
    if (reportDateRange[i].start >= reportDateRange[i].end) {
      write_status ("Custom report period end date\n"
                    "must be later than start date.",
                    ERR);
      return;
    }
  }

  /* 
  ** Create scrolled window dialog if in single window mode and none exists,
  ** or multi-window mode. 
  */
  if (((reportValue & REPORT_SINGLEWIN_ON) && reportDialog == (Widget)NULL)
      || !(reportValue & REPORT_SINGLEWIN_ON)) {
    reportDialog = XtVaCreatePopupShell ("Report", xmDialogShellWidgetClass,
                            GetTopShell (w), NULL );
    pane = XtVaCreateWidget ("ReportPane", 
                           xmPanedWindowWidgetClass, reportDialog,
                           XmNsashWidth, 1,
                           XmNsashHeight, 1,
                           NULL ); 
    scroll = XtVaCreateManagedWidget ("ReportScroll", 
                           xmScrolledWindowWidgetClass, pane,
                           XmNscrollingPolicy, XmAUTOMATIC,
                           XmNvisualPolicy,    XmCONSTANT,
                           NULL);
    reportDrawing = XtVaCreateManagedWidget ("ReportDrawing", 
                           xmDrawingAreaWidgetClass, scroll,
                           NULL );
    XtAddCallback( reportDrawing, XmNexposeCallback,
                 (XtCallbackProc) reportDraw, (XtPointer) NULL);

    /* Dialog control */
    form = XtVaCreateWidget ("form", xmFormWidgetClass, pane,
                           XmNfractionBase, 5,
                           XmNskipAdjust, True,
                           NULL );
    button = XtVaCreateManagedWidget ("Ok", xmPushButtonWidgetClass, form,
                           XmNtopAttachment,    XmATTACH_FORM,
                           XmNbottomAttachment, XmATTACH_FORM,
                           XmNleftAttachment,   XmATTACH_POSITION,
                           XmNleftPosition,     2,
                           XmNrightAttachment,  XmATTACH_POSITION,
                           XmNrightPosition,    3,
                           XmNshowAsDefault,    True,
                           XmNdefaultButtonShadowThickness, 1,
                           NULL );
    XtAddCallback (button, XmNactivateCallback,
                 (XtCallbackProc) reportDestroy, reportDrawing);

    XtManageChild (form);
    XtManageChild (pane);
  }

  XtManageChild (reportDialog);
  XtPopup (reportDialog, XtGrabNone);

  /* Force new drawing if in single window mode */
  if (reportDrawing && reportValue & REPORT_SINGLEWIN_ON)
    reportDraw (reportDrawing, (XtPointer)NULL, (XtPointer)NULL);
}
