/*
  Xinvest Copyright 1997 Mark Buser,
  All Rights Reserved.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 1.2 $ $Date: 1997/09/20 17:01:21 $
*/
#ifndef report_h
#define report_h

/* Report refresh */
void displayReport (int);

/* Report callbacks */
void reportSetTime ();
void reportSetValue ();
void reportSetAccount ();
void reportCustomTime ();
void reportGenerate ();

#endif
