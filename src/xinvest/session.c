/*
  Xinvest is copyright 1996-97 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.5 $ $Date: 1997/01/13 22:46:19 $
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <Xm/Xm.h>
#include <Xm/Protocols.h>
#if XtSpecificationRelease > 5
#include <X11/SM/SMlib.h>
#endif

#include "account.h"
#include "file.h"
#include "nav.h"
#include "portfolio.h"
#include "status.h"
#include "util.h"

/* Application defined options that we don't want to save in state file */
static XrmOptionDescRec kill_options[] = {
  {
     "-restore", "restore", XrmoptionSepArg, NULL
  }
};

static  char errmsg[1024];

int restoreState( char *from )
{
  FILE *savefile;
  char line[80];
  float version;     /* Xinvest state file rev */
  int xrev;          /* X11Rx revision */

  char *param;
  int account;
  int updatePort = False;

  /* Open session file */
  savefile = fopen ( from, "r");

  if (savefile == NULL) {
    sprintf ( errmsg, 
              "Could not open state file '%s',\nstate not restored.",
              from );
    write_status ( errmsg, ERR);
    return (False);
  }

  /* Read save area version number */
  if ( fgets ( line, 80, savefile) )
    sscanf ( line, "VERSION %f (X11R%d)\n", &version, &xrev);
  else {
    sprintf ( errmsg, 
              "Could not read state file '%s',\nstate not restored.",
              from );
    write_status ( errmsg, ERR);
    return (False);
  }
  if (version != 1.0) {
    sprintf ( errmsg, 
              "Expected version 1.0, not '%f'\n"
              "from state file '%s',\nstate not restored.",
              version, from );
    write_status ( errmsg, ERR);
    return (False);
  }

  /* Odds are we have a good state file, take note */
  sprintf ( errmsg, "Restoring state saved under X11R%d protocol.", xrev);
  write_status ( errmsg, LOG);

  while ( fgets(line, 80, savefile) ) {
    param = strrchr (line, '\n');  /* strip newline */
    if (param)
      *param = '\0';
    param = strrchr (line, ' ');   /* param is after last space */

    switch (line[0]) {

            case 'C': /* "CWD path" */
                      if (param)
                        loadfile (++param);
                      break;

            case 'D': /* "DISPLAY account" */
                      if (param) {
                        sscanf ( param, "%d", &account );
                        displayAccount ( account );
                      }
                      break;

            case 'L': /* "LOAD account path" */
                      if (param) {
                        account = loadfile (++param);
                        if (account)
                          processAccount (account);
                      }
                      break;

            case 'N': /* "NAV account mm/dd/yyyy n.n" */
                      {
                        NAV *navp;
                        char date[11];
                        double value;

                        sscanf ( line, "NAV %d %s %lf", &account, date, &value);
                        getAccount ( account, ACCOUNT_NAV, &navp );
                        setNav ( navp, NAV_DATE, date);
                        setNav ( navp, NAV_VALUE, &value);
                      }
                      break;

            case 'P': /* "PORTADD account" */
                      if (param) {
                        sscanf ( param, "%d", &account );
                        portAccount ( account, PORT_ADD );
                        updatePort = True;
                      }
                      break;

            default:  break;
    }
  }
  if (updatePort)         /* If any PORTADD, draw in the portfolio display */
    portUpdateDisplay();

  fclose (savefile);
  return (True);
}


static int saveState (char *to)
{
  FILE *savefile;

  char *cwdpath;
  int pathlen = path_length();
  int account;

  static int log = True;

  if (log) {
    sprintf ( errmsg, "Saving state under X11R%d protocol.", 
              XtSpecificationRelease);
    write_status ( errmsg, LOG);
    log = False;
  }

  /* 
  ** Create session file 
  */
  if ( (savefile = fopen ( to, "w" )) == NULL) {
    sprintf ( errmsg, 
              "Could not open state file '%s',\nstate not saved.", 
              to );
    write_status ( errmsg, ERR);
    return ( False );
  }

  /* 
  ** Save save area version number 
  */
  fprintf ( savefile, "VERSION 1.0 (X11R%d)\n", XtSpecificationRelease);

  /* 
  ** Save CWD 
  */
  cwdpath = XtCalloc ( pathlen, sizeof(char) );
  if (cwdpath == NULL) {
    write_status ("Memory allocate error, save\n"
                  "state file is not complete.",
                  ERR );
    fclose (savefile);
    return ( False );
  }

  /* Get full path */
  if ( getcwd( cwdpath, pathlen )  ) 
    fprintf ( savefile, "CWD %s\n", cwdpath);
  else {
    write_status ("CWD length greater than maximum,\n"
                  "save state file is not complete.",
                  ERR );
    fclose (savefile);
    return ( False );
  }
  XtFree (cwdpath);

  for (account = 1; account <= numAccounts(); account++) {
    /* 
    ** Save account info 
    */
    saveAccount ( savefile, account );

    /* 
    ** Save what's in the portfolio 
    */
    if (accountStatus ( account, ACCOUNT_IN_PORTFOLIO) )
      fprintf (savefile, "PORTADD %d\n", account);
  }

  if (numAccounts())
    fprintf (savefile, "DISPLAY %d\n", activeAccount() );

  /* 
   * Things we don't save:
   * 1. Menu settings - zoom, et al
   * 2. Tool settings - graph selections, et al
   */

   fclose (savefile);
   return (True);
}

static char *sessSaveFile()
{
  char *savepathname;
  char savefilename[] = "xinvXXXXXX";
  char *fullpathname;

  /*
  ** Determine session save area and filename.
  */
  if ( (savepathname = getenv ("SM_SAVE_DIR")) == NULL)
    savepathname = getenv ("HOME");

#ifndef X_NOT_POSIX
  fullpathname = XtMalloc ( strlen (savepathname) + strlen (savefilename) +2 );
  if (fullpathname == NULL) {
    write_status ("Could not allocate memory for path.\n", ERR);
    return (NULL);
  }
  strcpy ( fullpathname, savepathname);
  strcat ( fullpathname, "/" );  
  strcat ( fullpathname, savefilename);
  fullpathname = mktemp ( fullpathname );
#else
  fullpathname = tempnam ( savepathname, "xinv" );
#endif

  return (fullpathname);
}

#if XtSpecificationRelease > 5

/* ARGSUSED */
void sessSave ( Widget shell, XtPointer client_data, XtPointer call_data)
{
  XtCheckpointToken token = (XtCheckpointToken) call_data;
  String saveFile = sessSaveFile();

  char **argv;
  int  argc;

  String *discard = (String *)NULL;
  String *restart = (String *)NULL;
  int i;

  if (token->shutdown || token->interact_style != SmInteractStyleNone)
    XtSetSensitive ( shell, False);

  if ( (token->save_success = saveState(saveFile)) ) {
    /* 
    ** Update restore command 
    */

    XtVaGetValues ( shell, XtNrestartCommand, &argv, NULL);
    argc = 0;     /* How many arguments */
    if (argv) {
      XrmDatabase database = (XrmDatabase) NULL;

      while ( argv[argc++] )
        ;
      if (argc) argc--;       /* don't count NULL last arg */

      /* Remove arguments we don't want to save */
      removeFileOption ( &argc, argv );
      XrmParseCommand ( &database, kill_options, XtNumber(kill_options),
                        "Xinvest", &argc, argv );
      XrmDestroyDatabase (database);

      restart = (char **) XtMalloc ((argc+3) * sizeof(String));
      if (restart) {
        for (i=0; i < argc; i++)
          restart[i] = XtNewString ( argv[i] );
        restart[argc++] = XtNewString("-restore");
        restart[argc++] = XtNewString(saveFile);
        restart[argc]   = NULL;
  
        XtVaSetValues ( shell, XtNrestartCommand, restart, NULL);
      }
    }

    /* 
    ** Update discard command 
    */
    i = 0;
    discard = (String *) XtMalloc ( 2 * sizeof(String) );
    if (discard) {
      discard[i] = XtMalloc ( strlen(saveFile) + strlen ("rm ") +1 );
      if (discard[i])
        sprintf ( discard[i++], "rm %s", saveFile );
      discard[i] = NULL;

      XtVaSetValues ( shell, XtNdiscardCommand, discard, NULL);
    }
  }

  XtFree (saveFile);
}
#else

/* ARGSUSED */
void sessSave ( Widget shell, XtPointer client_data, XtPointer call_data)
{
  char *saveFile = sessSaveFile();

  char **argv;
  int argc;
  char restore[] = "-restore";

  XGetCommand ( XtDisplay (shell), XtWindow (shell), &argv, &argc );

  if ( saveState (saveFile) ) {
    XrmDatabase database = (XrmDatabase) NULL;

    /* Remove arguments we don't want to save (-f, -restore) */
    removeFileOption ( &argc, argv );
    XrmParseCommand ( &database, kill_options, XtNumber(kill_options),
                      "Xinvest", &argc, argv );
    XrmDestroyDatabase (database);

    /* Add new -restore to read state file */
    argv = (char **) XtRealloc ( (char *)argv, (argc+2) * sizeof(char *));
    argv[argc++] = restore;
    argv[argc++] = saveFile;
    XSetCommand ( XtDisplay (shell), XtWindow (shell), argv, argc );

  } else
    XSetCommand ( XtDisplay (shell), XtWindow (shell), argv, argc );

  XtFree (saveFile);
  XFreeStringList ( argv );
}
#endif

/* ARGSUSED */
void sessResume ( Widget shell, XtPointer client_data, XtPointer call_data)
{
  XtSetSensitive ( shell, True);
}

/* ARGSUSED */
void sessDie ( Widget shell, XtPointer client_data, XtPointer call_data)
{
  XtDestroyApplicationContext (XtWidgetToApplicationContext(shell));
  exit(0);
}
