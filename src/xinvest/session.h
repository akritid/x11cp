/*
  Xinvest is copyright 1996 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.0 $ $Date: 1996/11/21 01:31:56 $
*/

void sessDie ( Widget, XtPointer, XtPointer call_data);
void sessSave ( Widget, XtPointer, XtPointer call_data);
void sessResume ( Widget, XtPointer, XtPointer call_data);
int restoreState ( char *);
