/*
  Xinvest is copyright 1995, 1996 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.1 $ $Date: 1996/12/22 19:18:05 $
*/

#ifndef status_h
#define status_h

#include <Xm/Xm.h>

/* levels for write_status */
#define WARN XmDIALOG_WARNING
#define ERR  XmDIALOG_ERROR
#define INFO XmDIALOG_INFORMATION
#define LOG  XmDIALOG_MESSAGE

void write_status (char *info, int level);

int bound_ok ( int, int, int *, int );
int suppressed ();
void suppress ( int );
#endif
