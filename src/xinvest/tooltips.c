/*
  Xinvest Copyright 1995-97 Mark Buser,
  All Rights Reserved.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.

  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.

  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 1.2 $ $Date: 1997/07/03 21:47:21 $
*/

#include <Xm/XmAll.h>

#include "color.h"

#define UP   0
#define DOWN 1

#define UP_TIME 750L        /* pause time before popup 750 mS */
#define DOWN_TIME 4000L     /* pause time before popdown 4 S */

static Widget popup = (Widget) NULL;
static int pause = True;    /* pause before popup? */

static void popToolTips (int way)
{
  if (popup) {
    if (way == UP)
      XtRealizeWidget (popup);
    else
      XtUnrealizeWidget (popup);
  }
}

/* ARGSUSED */
static void downTime (int client_data, XtIntervalId timer)
{
  popToolTips (DOWN);
  pause = True;
}

/* ARGSUSED */
static void upTime (XtAppContext appContext, XtIntervalId timer)
{
  static XtIntervalId downTimer = (XtIntervalId) NULL;

  popToolTips (UP);

  if (downTimer)
    XtRemoveTimeOut (downTimer);
  downTimer = XtAppAddTimeOut (appContext, DOWN_TIME, 
                      (XtTimerCallbackProc)downTime, (XtPointer)NULL);

}

/* ARGSUSED */
static void pauseTime (XtAppContext appContext, XtIntervalId timer)
{
  pause = True;
}

/* ARGSUSED */
static void downCB (Widget w, XtPointer client_data, XtPointer call_data)
{
  popToolTips (DOWN);
  pause = True;
}

/* ARGSUSED */
void showToolTips ( Widget w, XtPointer client_data, XEvent *event)
{
  static Widget label;
  Widget frame;

  XmFontList font_list;
  XmString label_str;
  Dimension label_width, label_height, height;

  Position root_x, root_y;

  static XtIntervalId upTimer = (XtIntervalId) NULL;
  XtAppContext appContext = XtWidgetToApplicationContext (w);

  if (event->type == EnterNotify) {

    if (popup == (Widget)NULL) {
       popup = XtVaAppCreateShell ("TipShell", "TipClass",
                                   applicationShellWidgetClass,
                                   XtDisplayOfObject(w),
                                   XmNoverrideRedirect, True,
                                   XmNcolormap, GetColormap(),
                                   NULL);
       frame = XtVaCreateManagedWidget ("tipFrame", 
                                xmFrameWidgetClass, popup, 
                                NULL);
       label = XtVaCreateManagedWidget("tipLabel", 
                                       xmLabelGadgetClass, frame,
                                       NULL);
    }

    XtVaGetValues( label, XmNfontList, &font_list, NULL);
    XtVaGetValues( w,     XmNlabelString, &label_str, NULL);

    label_width = XmStringWidth( font_list, label_str);
    label_height = XmStringHeight( font_list, label_str);

    XtVaSetValues( label, XmNlabelString, label_str, NULL);
    XmStringFree( label_str );

    XtTranslateCoords( w, (Position)0, (Position)0, &root_x, &root_y);
    XtVaGetValues( w, XmNheight, &height, NULL);

#define BORDER 2

    XtVaSetValues (popup, XmNx, root_x,
                          XmNy, root_y+height,
                          XmNwidth, label_width + (BORDER*2),
                          XmNheight, label_height + (BORDER*2),
                   NULL);
#undef BORDER

    if (upTimer) {
      XtRemoveTimeOut (upTimer);
      upTimer = (XtIntervalId) NULL;
    }

    if (pause == True)
      upTimer = XtAppAddTimeOut (appContext, UP_TIME, 
                        (XtTimerCallbackProc)upTime, (XtPointer)appContext);
    else
      upTime (appContext, (XtIntervalId) NULL);

  } else if (event->type == LeaveNotify && popup) {

    if (upTimer) {
      XtRemoveTimeOut (upTimer);
      upTimer = (XtIntervalId) NULL;
    }

    if (XtIsRealized (popup) ) {
      popToolTips (DOWN);

      pause = False;
      upTimer = XtAppAddTimeOut (appContext, UP_TIME, 
	                (XtTimerCallbackProc)pauseTime, (XtPointer)appContext);
    }
  }
}

/* 
** Install tooltips event handlers and callbacks.  Only works for 
** XmPushButton or XmToggleButton classes.
*/
void installToolTips (Widget w)
{
  XtAddEventHandler (w, LeaveWindowMask | EnterWindowMask, False,
		     (XtEventHandler) showToolTips, (XtPointer) 0);
  if (XmIsPushButton(w) || XmIsPushButtonGadget(w))
    XtAddCallback (w, XmNactivateCallback, downCB, (XtPointer)NULL);
  if (XmIsToggleButton(w) || XmIsToggleButtonGadget(w))
    XtAddCallback (w, XmNvalueChangedCallback, downCB, (XtPointer)NULL);
}
