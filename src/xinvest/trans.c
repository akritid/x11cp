/*
  Xinvest Copyright 1995-97 Mark Buser,
  All Rights Reserved.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.12 $ $Date: 1997/08/10 16:02:45 $
*/

#include <ctype.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <Xm/Xm.h>

#include "rate.h"
#include "trans.h"
#include "transP.h"
#include "status.h"
#include "util.h"

/* globals */
static int lineno = 0;
static int transno = 0;
static int transalloc = 0;
static TRANS *trans;
static char *title = NULL;
static char *ticker = NULL;

/* Are there any transactions, returns how many */
int AreTrans() {
  return (transno);
}

/* Set transaction record to none */
static void ClrTrans()
{
  lineno = 0;
  title = NULL;
  ticker = NULL;
  transno = 0;
  transalloc = 0;
  trans = NULL;
}

/* Return pointer to trans structure */
void *GetTrans()
{
  return (trans);
}

/* Return pointer to transaction title, if there is one */
char *GetTransTitle()
{
  return title;
}

/* Return pointer to ticker tape symbol, if there is one */
char *GetTransTicker()
{
  return ticker;
}

/* 
** Return date of transaction "num"
*/
char *GetTransDate( void *trans, int num )
{
  return (((TRANS *)trans)[num].t_date);
}

long GetTransLDate( void *trans, int num )
{
  return (((TRANS *)trans)[num].t_ldate);
}

/* 
** Return NAV of transaction "num"
*/
double GetTransNav( void *trans, int num )
{
  return ( ((TRANS *)trans)[num].t_nav );
}

/* 
** Return type of transaction "num"
*/
int GetTransType( void *trans, int num )
{
  return ( ((TRANS *)trans)[num].t_type );
}

/* 
** Return load of transaction "num"
*/
double GetTransLoad( void *trans, int num )
{
  return ( ((TRANS *)trans)[num].t_load );
}

/* 
** Return number of shares of transaction "num"
*/
double GetTransShares( void *trans, int num )
{
  return (((TRANS *)trans)[num].t_shares);
}

/*
** Return value of indicated flag value at transaction num 
*/
int GetTransFlag( void *trans, int num, int flag )
{
  if (flag == TRANS_REINVEST)
    return (((TRANS *)trans)[num].t_flag & flag);
  else
    return 0;
}

/*
** Return value of split  major:minor
*/
int GetTransSplit( void *trans, int num, int which )
{
  /* Check flag range? */
  if (which == TRANS_MAJOR)
    return (((TRANS *)trans)[num].t_major);
  if (which == TRANS_MINOR)
    return (((TRANS *)trans)[num].t_minor);
  else
    return 1;
}

/*
** Read in a transaction from trans, reset to 1st line on new file.
** Return 0 on ok, -1 on error.
*/
char *rd_trans( char *transline, int new )
{
    TRANS *tp;                  /* ptr to slots in trans[] array */

    char datebuf[20];           /* store date here */
    int  type;                  /* T_BUY, T_SELL, or T_DIST */
    double nav;                 /* buy price, sell price, reinvest price */
    double shares = 0.0;        /* number of shares buy or sell */
    double load = 0.0;          /* transaction cost */
    char loadpercent[2] = " ";  /* is load in percentage */
    int  major = 1, minor = 1;  /* split major:minor */

    static int reinvest = True; /* reinvest gains or not */

    char directive[11], param[11];
    int dirnum;
    int num_read, expect;

    char typebuf[20];
     
    static char err_msg[80];

    if (new) {
      ClrTrans();
      reinvest = True;
    }

    if ( (transline == NULL) || strlen(transline) == 0 )
      return (0);

    lineno++;
     
    /* 
    ** Process the non-transaction lines 
    */

    /* skip comment lines */
    if ( !( (*transline == '#') || 
            (*transline == '*') || 
            isspace(*transline) ) ) {
     
      /* 10 is fine for directives, param is not used */
      num_read = sscanf (transline, "%10s %n %10s", directive, &dirnum, param);

      /* 
      ** Percent processing (skip)
      */
      if ( strncasecmp( "percent", directive, strlen("percent")) == 0 )
        return (0);

      /* 
      ** Title processing 
      */

      /* skip title directive except for saving it */
      if (strncasecmp( "title", directive, strlen("title")) == 0) {

        /* Should be at least the directive and one other word */
        if ( num_read == 1) {
          sprintf (err_msg, "Line %d: missing title text in title directive.",
		   lineno);
          return (err_msg);
        }

        /* free the old title if there was one */
        if (title)
          XtFree (title);

        /* Save the title for other uses */
        title = XtNewString (transline+dirnum);
        if (title  == NULL) {
          strcpy (err_msg, "Couldn't allocate space for title directive.");
          return (err_msg);
        } else
          return (NULL);
      }

      /* 
      ** Ticker symbol processing
      */
      if (strncasecmp ("ticker", directive, strlen("ticker")) == 0) {

        /* Should be at least the directive and one other word */
        if ( num_read == 1) {
          sprintf (err_msg, 
		  "Line %d: missing parameter to ticker directive.",
		  lineno);
          return (err_msg);
        }

        /* Remove previous tick, if present */
        if (ticker)
          XtFree (ticker);

        /* Save the tick for other uses */
        ticker = XtNewString (transline+dirnum);
        if ( ticker == NULL) {
          strcpy (err_msg, "Couldn't allocate space for ticker directive.");
          return (err_msg);
        } else
          return (NULL);
      }

      /* 
      ** Reinvest gain processing 
      */
      if (strncasecmp ("reinvest", directive, strlen("reinvest")) == 0) {
        char *tmp;

        /* Should be at least the directive and one other word */
        if ( num_read == 1) {
          sprintf (err_msg, 
		  "Line %d: missing parameter to reinvest directive.",
		  lineno);
          return (err_msg);
        }

        /* tmp points to first char past directive. This should never be NULL.*/
        tmp = strstr( transline, param );

	/* See what the parameter to reinvest is */
	if (strcasecmp (transline+dirnum, "all") == 0)
	  reinvest = True;
	else if (strcasecmp (transline+dirnum, "none") == 0)
	  reinvest = False;
	else {
          sprintf (err_msg, 
		   "Line %d: invalid parameter '%s' in reinvest directive.",
	           lineno, transline+dirnum);
	  return (err_msg);
	}

        return (0);
      }

      /*   
      ** Ok got a real transaction 
      */

      /* Param is just to catch any unexpected stuff at end of line */
      if ( strncasecmp( param, "split", strlen("split")) == 0)
        num_read = sscanf ( transline, "%s %s %lf %d:%d %s", 
                            datebuf, typebuf, &nav, &major, &minor, param );
      else
        num_read = sscanf ( transline, "%s %s %lf %lf %lf %1s %s", 
                            datebuf, typebuf, &nav, &shares, &load, loadpercent,
                            param );


      /* check for valid date */
      if ( !strchr( datebuf, '/') || strtoday( datebuf ) == -1 ) {
         sprintf( err_msg, 
                  "Line %d: invalid date '%s'.\n" 
                  "See help \"Transaction Record\".",
                  lineno, datebuf );
         return (err_msg);
      }

      /* check for consecutive dates */
      if ( transno > 0 && ( strtoday ( datebuf ) < 
                            strtoday ( trans[transno].t_date ) ) ) {
         sprintf( err_msg, 
                  "Line %d: '%s' is earlier\n"
		  "than previous date '%s'.", 
                  lineno, datebuf, trans[transno].t_date );
         return (err_msg);
      }

      /* check for allowable type */
      if ( strncasecmp( "buy", typebuf, 3) == 0 ||
           strncasecmp( "exchange", typebuf, 8) == 0 )
        type = T_BUY;
      else if ( strncasecmp( "sell", typebuf, 4) == 0) {
        type = T_SELL;
        if ( shares > 0 )    /* Sell shares are always negative */
           shares = -shares;
      }
      else if ( strncasecmp( "div",  typebuf, 3) == 0 ||
                strncasecmp( "stcg", typebuf, 4) == 0 ||
                strncasecmp( "ltcg", typebuf, 4) == 0 )
        type = T_DIST;
      else if ( strncasecmp( "price", typebuf, 5) == 0 )
        type = T_PRICE;
      else if ( strncasecmp( "split", typebuf, 5) == 0 )
        type = T_SPLIT;
      else {
        sprintf( err_msg, 
                 "Line %d: Unknown transaction type: '%s'", 
                 lineno, typebuf );
        return (err_msg);
      }

      /* Check for in range splits */
      if (type == T_SPLIT) {

	if (major <= 0 || major > 255 || minor <= 0 || minor > 255) {
          sprintf( err_msg, 
                   "Line %d, split '%d:%d' out \n"
		   "of valid range of 1 to 255.", 
                   lineno, major, minor );
          return (err_msg);
        }

      }

      /* If load is percentage calculate, otherwise its a straight value */
      if (type == T_BUY || type == T_SELL || type == T_DIST)
        if (num_read == 6 && loadpercent[0] == '%') {
          /* load is present and expressed as percentage of purchase */
          load = fabs(shares) * nav * load / 100.0;
        }

      /* Check for a reasonable load */
      if (load < 0.0) {
        sprintf( err_msg, "Line %d, specified load '%.2f' is negative.\n",
                 lineno, load );
        return (err_msg);
      } else if (shares > 0.0 && load > (shares * nav)) {
        sprintf( err_msg, 
                 "Line %d, specified load '%.2f' is not\n"
                 "in range 0 to 100%% of transaction value.\n",
                 lineno, load );
        return (err_msg);
      }

      /* check for appropriate number of fields */
      switch (type) {

        case T_PRICE:
                      expect = 3;              /* date command price */
                      break;

        case T_BUY:
        case T_SELL:
        case T_DIST:
                      if (loadpercent[0] == '%')
                        expect = 6;
                      else if (num_read > 4)   /* straight load  */
                        expect = 5;
                      else                     /* no load */
                        expect = 4;
                      break;
	case T_SPLIT:
		      expect = 5;              /* date command price m:m */
		      break;
      }
      if ( num_read != expect ) {
        sprintf( err_msg, 
		 "Line %d: expected %d fields, processed %d.", 
                 lineno, expect, num_read );
        return (err_msg);
      }

      /* Seems ok, fill in new transaction structure */
      if ( ++transno >= transalloc ) {
        trans = (TRANS *)XtRealloc ( (char *)trans, 
                            (transalloc+TRANS_ALLOC)*sizeof(TRANS) );
        if ( trans == NULL) {
          sprintf( err_msg, "Out of memory: loaded transactions up to %d",
                   transno-1);
          return (err_msg);
        }
        transalloc += TRANS_ALLOC;
      }
      
      /* Adjust short hand dates, so we don't have to do it anywhere else */
      { /* Adjust 50+ to 1950+, 49- to 2049- */
        int m, d, y;
        sscanf ( datebuf, "%d/%d/%d", &m, &d, &y); 
        if ( y < 50 )
          y += 2000;
        else if ( y < 100)
          y += 1900;
        sprintf ( datebuf, "%2d/%2d/%4d", m, d, y);
      }

      tp = &trans[transno];
      tp -> t_type = type;
      /* Save expanded year, not the possibly shorthand one */
      tp -> t_date = strdup( datebuf );
      tp -> t_ldate = strtoday( datebuf );
      tp -> t_shares = shares;
      tp -> t_nav = nav;
      tp -> t_load = load;
      tp -> t_major = major;
      tp -> t_minor = minor;
      if (reinvest == True)
	tp -> t_flag |= TRANS_REINVEST;
      else
	tp -> t_flag &= ~TRANS_REINVEST;
    }

    return ( NULL );
}
