/*
  Xinvest is copyright 1995,1996 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.4 $ $Date: 1997/01/11 00:55:30 $
*/
#ifndef TRANSP_H
#define TRANSP_H

typedef struct transaction {
    /* from the user input */
    unsigned char  t_type;   /* type of transaction */
    unsigned char  t_flag;   /* reinvest on/off */
    unsigned char  t_major;  /* split 1st digit */
    unsigned char  t_minor;  /* split 2nd digit */
    char       *t_date;      /* date of transaction */
    long       t_ldate;      /* date of transaction */
    double     t_shares;     /* number of shares bought or remaining */
    double     t_nav;        /* net asset value = price per share */
    double     t_load;       /* transaction cost */
} TRANS;

#define TRANS_ALLOC 10
#endif
