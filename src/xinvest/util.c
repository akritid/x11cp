/*
  Xinvest is copyright 1995,1996 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 1.7 $ $Date: 1997/04/13 12:49:45 $
*/
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <Xm/Xm.h>

#include "account.h"
#include "file.h"
#include "status.h"
#include "util.h"

void syntax(int argc, char **argv)
{
  if (argc != 1)
    fprintf( stderr, "%s: unknown option \"%s\"\n", argv[0], argv[1]);
  fprintf( stderr, "usage: %s [options ...]\n", argv[0]);
  fprintf( stderr, "       where options include:\n\n");
  fprintf( stderr, "       -f filename(s)  to load transaction file(s),\n");
  fprintf( stderr, "       -help           to print this message,\n");
  fprintf( stderr, "       -install        to install a private colormap,\n");
  fprintf( stderr, "       or any valid X toolkit option.\n\n");
}


/* All I want is the lousy max path length, why is this a good thing? */
#ifdef PATH_MAX
static int pathmax = PATH_MAX;
#else
static int pathmax = 0;
#endif

#define PATH_MAX_GUESS 1024

int path_length ()
{
   if (pathmax == 0) {
     if ((pathmax = pathconf ("/", _PC_PATH_MAX)) < 0) 
       pathmax = PATH_MAX_GUESS;
     else
       pathmax++;
   }

   return (pathmax);
}


/* Look at an argc/argv pair for the -f filename[s] option.  Return argc/argv
** with this option removed.
*/
void removeFileOption ( int *argc, char **argv )
{
  int i, j;
  int start = -1;
  int end = -1;

  for (i = 0; i < *argc; i++) {
    /*
    ** Find -f option and the next option after it, if any.  No filenames
    ** are allowed to start with '-'.
    */
    if ( argv[i] && strcmp ( "-f", argv[i] ) == 0) {
      for (start = i, j = i+1; j < *argc; j++)
        if ( argv[j] && strncmp ("-", argv[j], strlen("-")) == 0) {
          end = j;
          break;
        }
    }

    if ( start > 0 ) {          /* Found -f option. */

      if ( end > 0 ) {          /* Option(s) after it, move up to discard -f. */
        int newargc = *argc - (end - start);

        for ( j = end; j < *argc; j++)
          argv[start++] = argv[j];

        if (end < *argc-1) {    /* There may be more -f's, look again */
          *argc = newargc;
          removeFileOption (argc, argv);
        } else {
          *argc = newargc;
        }
        break;

      } else if ( end == -1 ) { /* No options after it, just decrement argc */
        *argc = start;
        break;
      }
    }

    /* else no -f found, no changes required */
  }

  return;
}

/* Look at an argc/argv pair for -f filename[s] options. Process the option
** by loading and processing the files.
*/
void processFileOption ( int argc, char **argv)
{
  int account = 0;
  /*
  ** If any options left, they must be data files beginning with -f.
  ** There may be more than one -f option and each can have zero or more
  ** filenames [-f filename(s) -f filename(s) ...].
  */
  argv++; argc--;                          /* skip executable name */
  while (argc) {

    if ( strcmp ( "-f", *argv) != 0 ) {    /* bad option */
      char errmsg[132];
      sprintf ( errmsg,
                "Unknown option '%s' ignored,\nexpected '-f filename(s)'.",
                *argv );
      write_status ( errmsg, ERR);
      argv++; argc--;                      /* skip -f */

    } else {                               /* -f option, as expected */
      argv++; argc--; 

      while ( argc ) {                     /* any number of filenames */
        if (strncmp ( "-", *argv, strlen("-")) == 0)
          break;                           /* found option, go back to top */


        if ( (account = loadfile( *argv )) )
          processAccount( account );

        argv++; argc--;
      }

    }

  }
  if (account)
    displayAccount( account );
}


/*
** Host Specific Support
*/

/* Supplied by Cube-X */
#ifdef NeXT

extern char *getwd (char *pathname);

char *getcwd ( char *buf, size_t size)
{
  char realBuffer[MAXPATHLEN+1];
  int length;

  if (getwd(realBuffer) == NULL) {
    errno = EACCES;
    return NULL;
  }
  length = strlen(realBuffer);
  if (length >= size) {
    errno = ERANGE;
    return NULL;
  }
  strcpy (buf, realBuffer);
  return buf;
}

char *strdup ( const char *src)
{
  char *copy = (char *) malloc(strlen(src)+1);
  if (copy)
    strcpy(copy, src);
  return copy;
}
#endif


/* Unixware needs this. Supplied by Thanh Ma. */
#ifdef NEED_STRCASECMP

int
strncasecmp(char *s1, char *s2,int n)
{
    register int c1, c2,l=0;

    while (*s1 && *s2 && l<n) {
    c1 = tolower(*s1);
    c2 = tolower(*s2);
    if (c1 != c2)
        return (c1 - c2);
    s1++;
    s2++;
    l++;
    }
    return (int) (0);
}

int
strcasecmp(char *s1, char *s2)
{
    register int c1, c2;

    while (*s1 && *s2) {
    c1 = tolower(*s1);
    c2 = tolower(*s2);
    if (c1 != c2)
        return (c1 - c2);
    s1++;
    s2++;
    }
    return (int) (*s1 - *s2);
}
#endif


#if defined(__FreeBSD__) || defined (NEED_STRPTIME)

/* Weird, FreeBSD and IRIX 5 don't have this, but do have strftime. 
** This is a crippled implementation, only supporting the formats
** actually used in the rest of Xinvest.  Note that the return _value_
** does not conform to the man page either.
*/
char *strptime ( char* buf, char *fmt, struct tm *tm )
{
  char err_msg[128];

  if ( strcmp( fmt, "%m/%d/%y" ) == 0 ) 
    sscanf ( buf, "%d/%d/%d", &tm->tm_mon, &tm->tm_mday, &tm->tm_year );

  else if (strcmp ( fmt, "%m/%d/%Y" ) == 0 ) {
    sscanf ( buf, "%d/%d/%d", &tm->tm_mon, &tm->tm_mday, &tm->tm_year );
    tm->tm_year -= 1900;

  } else if (strcmp ( fmt, "%m %d %y" ) == 0 ) 
    sscanf ( buf, "%d %d %d", &tm->tm_mon, &tm->tm_mday, &tm->tm_year );

  else {
    sprintf ( err_msg, "Xinvest internal error: strptime.  Unsupported\n"
                      "format: \"%s\".  See Reporting Bugs.", fmt);
    write_status( err_msg, ERR);
  }

  tm->tm_mon--;  /* Months are from 0-11, mdays from 1-31, years since 1900 */

  return NULL;  /* completely bogus return value, but I don't use it */
}
#endif
