/*
  Xinvest is copyright 1995,1996 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.2 $ $Date: 1997/02/11 01:19:41 $
*/
#ifndef util_h
#define util_h

void syntax(int argc, char **argv);
int path_length();

void removeFileOption ( int *, char **);  /* Remove -f from passed argc/argv */
void processFileOption ( int, char **);   /* Load -f files from argc/argv */

#ifdef NeXT
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include <strings.h>
#include <sys/dir.h>
#include <sys/param.h>
#define S_ISDIR(mode) (((mode) & 0170000) == 0040000)

char *getcwd (char *, size_t);
char *strdup( const char *);
#endif

#ifdef NEED_STRCASECMP
int strcasecmp (char *, char *);
int strncasecmp (char *, char *, int);
#endif 

#if defined(__FreeBSD__) || defined (__sgi)
char *strptime ( char *, char *, struct tm *);
#endif

#endif
