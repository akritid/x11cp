/*
  Xinvest Copyright 1995-97 Mark Buser,
  All Rights Reserved.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.4 $ $Date: 1997/08/09 19:41:54 $
*/

/* Application resources */
typedef struct {
  XmFontList graph_font_list;
  Boolean    install;
  Boolean    help;
  Pixel      color0;
  Pixel      color1;
  Pixel      color2;
  Pixel      color3;
  Pixel      color4;
  Pixel      color5;
  Pixel      color6;
  Pixel      color7;
  char       *restore;
}  AppData;

static XtResource resources [] = {
  {
     XmNfontList,
     XmCFontList,
     XmRFontList,
     sizeof( XmFontList ), 
     XtOffsetOf ( AppData, graph_font_list),
     XtRString,
     XmNdefaultFontList 
  },
  {
     "install", 
     "Install", 
     XmRBoolean,
     sizeof(Boolean),
     XtOffsetOf ( AppData, install),
     XmRImmediate, 
     False 
  },
  {
     "help", 
     "Help", 
     XmRBoolean,
     sizeof(Boolean),
     XtOffsetOf ( AppData, help),
     XmRImmediate, 
     False 
  },
  {
     "color0", 
     "Color0", 
     XmRPixel,
     sizeof(Pixel),
     XtOffsetOf ( AppData, color0),
     XmRImmediate, 
     False 
  },
  {
     "color1", 
     "Color1", 
     XmRPixel,
     sizeof(Pixel),
     XtOffsetOf ( AppData, color1),
     XmRImmediate, 
     False 
  },
  {
     "color2", 
     "Color2", 
     XmRPixel,
     sizeof(Pixel),
     XtOffsetOf ( AppData, color2),
     XmRImmediate, 
     False 
  },
  {
     "color3", 
     "Color3", 
     XmRPixel,
     sizeof(Pixel),
     XtOffsetOf ( AppData, color3),
     XmRImmediate, 
     False 
  },
  {
     "color4", 
     "Color4", 
     XmRPixel,
     sizeof(Pixel),
     XtOffsetOf ( AppData, color4),
     XmRImmediate, 
     False 
  },
  {
     "color5", 
     "Color5", 
     XmRPixel,
     sizeof(Pixel),
     XtOffsetOf ( AppData, color5),
     XmRImmediate, 
     False 
  },
  {
     "color6", 
     "Color6", 
     XmRPixel,
     sizeof(Pixel),
     XtOffsetOf ( AppData, color6),
     XmRImmediate, 
     False 
  },
  {
     "color7", 
     "Color7", 
     XmRPixel,
     sizeof(Pixel),
     XtOffsetOf ( AppData, color7),
     XmRImmediate, 
     False 
  },
  {
     "restore", 
     "Restore", 
     XmRString,
     sizeof(char *),
     XtOffsetOf ( AppData, restore),
     XmRImmediate, 
     NULL 
  },
};

/* Application defined options */
static XrmOptionDescRec options [] = {
  {
     "-install", "install", XrmoptionNoArg, "True"
  },
  {
     "-help", "help", XrmoptionNoArg, "True" 
  },
  {
     "-restore", "restore", XrmoptionSepArg, NULL
  }
};
