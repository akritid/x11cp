/*
  Xinvest is copyright 1996-97 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.8 $ $Date: 1997/09/12 00:58:05 $
*/

#include <stdio.h>

#include <Xm/XmAll.h>

#include "tooltips.h"
#include "xutil.h"

/* Get top most widget in the tree containing w */
Widget GetTopShell (Widget w)
{
  while (w && !XtIsWMShell (w) )
    w = XtParent (w);
  return w;
}

/* ARGSUSED */
void DestroyShell ( Widget w, XtPointer client_data, XtPointer call_data )
{
  Widget shell = (Widget) client_data;
  int *isPopped;

  /* Set is popped flag false, so we can know when this shell is no more */
  XtVaGetValues ( w, XmNuserData, &isPopped, NULL );
  if (isPopped)
    *isPopped = False;

  XtUnmanageChild (shell);
  XtPopdown (shell);
}

void CenterWidget ( Widget parent, Widget child)
{
  Dimension ph, pw, cw, ch;
  int       x, y;
 
  XtVaGetValues( parent, XmNwidth, &pw,
                         XmNheight, &ph,
                 NULL );
  XtVaGetValues( child, XmNwidth, &cw,
                        XmNheight, &ch,
                 NULL );
  x = (int)(pw - cw) / 2;
  if (x < 0) x = 0;

  y = (int)(ph - ch) / 2;
  if (y < 0) y = 0;
 
  XtVaSetValues ( child, XmNleftOffset, x,
                         XmNtopOffset, y,
                  NULL );
}


Widget makeMenuPulldown ( Widget parent, char *name, int pos, 
                          MENUITEMS *items, int numItems)
{
  int i, n, numbuttons;
  Widget menu, cascade, child;
  WidgetClass class;
  String callbackname;

  char cascadename[10];    /* 99 buttons should be good */

  Arg arg[2];

  numbuttons = -1;

  sprintf (cascadename, "button_%d", pos);
  cascade = XtNameToWidget (parent, cascadename);
  
  /* the pulldown menu or popup menu */
  if (cascade)
    menu = XmCreatePulldownMenu (parent, name, NULL, 0);
  else
    menu = XmCreatePopupMenu (parent, name, NULL, 0);

  /* the menu items */
  for (i=0; i < numItems; i++) {

    if ( strcasecmp ( items[i].class, XmVaPUSHBUTTON ) == 0) {
      class = xmPushButtonGadgetClass;
      callbackname = XmNactivateCallback;
      numbuttons++;
      n = 0;

    } else if (  strcasecmp ( items[i].class, XmVaRADIOBUTTON ) == 0) {
      class = xmToggleButtonGadgetClass;
      callbackname = XmNvalueChangedCallback;
      numbuttons++;
      n = 0;
      XtSetArg ( arg[n], XmNindicatorType, XmONE_OF_MANY); n++;
      if (i==0) {
        XtSetArg ( arg[n], XmNset, XmSET); n++;
      }

    } else if (  strcasecmp ( items[i].class, XmVaCHECKBUTTON ) == 0) {
      class = xmToggleButtonGadgetClass;
      callbackname = XmNvalueChangedCallback;
      numbuttons++;
      XtSetArg ( arg[0], XmNindicatorType, XmN_OF_MANY);
      n = 1;

    } else if (  strcasecmp ( items[i].class, XmVaTITLE ) == 0) {
      class = xmLabelGadgetClass;
      callbackname = NULL;
      n = 0;

    } else if (  strcasecmp ( items[i].class, XmVaSEPARATOR ) == 0) {
      class = xmSeparatorGadgetClass;
      callbackname = NULL;
      n = 0;
    }

    child = XtCreateManagedWidget ( items[i].name, class, menu, arg, n);
    if (items[i].callbackproc)
       XtAddCallback ( child, callbackname, 
                       (XtCallbackProc)items[i].callbackproc, 
                       (XtPointer) numbuttons );
  }

  /* Attach pulldown to cascade button */
  if (cascade)
    XtVaSetValues ( cascade, XmNsubMenuId, menu, NULL);

  /* Attach pulldown to cascade button */
  XtVaSetValues ( cascade, XmNsubMenuId, menu, NULL);

  return menu;
}

Widget makeButtonbar ( Widget parent, char *name, int direction, 
                       BUTTONITEMS *items, int numItems)
{
  Widget butform, child;
  WidgetClass class;
  String callbackname;
  Arg arg[8];
  int n;
  int i;
  
  butform = XtVaCreateWidget ( name, xmFormWidgetClass, parent, NULL);

  for (i=0; i < numItems; i++) {

    /* Set up arguments */
    n = 0;

    /* Button class: toggle or pushbutton */

    if ( strcasecmp ( items[i].class, XmVaTOGGLEBUTTON ) == 0) {
      class = xmToggleButtonWidgetClass;
      callbackname = XmNvalueChangedCallback;
      n = 0;
    } else {
      class = xmPushButtonWidgetClass;
      callbackname = XmNactivateCallback;
      n = 0;
    }

    /* Pixmaps */
    if (items[i].sens != (Pixmap) NULL) {
      XtSetArg ( arg[n], XmNlabelPixmap, items[i].sens); n++;
      if (items[i].insens == (Pixmap) NULL) {
        XtSetArg ( arg[n], XmNlabelInsensitivePixmap, items[i].sens); n++;
      }
    }
    if (items[i].insens != (Pixmap) NULL) {
      XtSetArg ( arg[n], XmNlabelInsensitivePixmap, items[i].insens); n++;
      XtSetArg ( arg[n], XmNsensitive, False); n++;
    }
    if (items[i].sel != (Pixmap) NULL) {
      XtSetArg ( arg[n], XmNselectPixmap, items[i].sel); n++;
    }

    if (n > 0) {
      XtSetArg ( arg[n], XmNlabelType, XmPIXMAP ); n++;
    }

    /* Form posiion*/
    if (direction == XmVERTICAL) {
      XtSetArg ( arg[n], XmNtopAttachment, XmATTACH_POSITION); n++;
      XtSetArg ( arg[n], XmNleftAttachment, XmATTACH_FORM); n++;
      XtSetArg ( arg[n], XmNrightAttachment, XmATTACH_FORM); n++;
    }
    if (direction == XmHORIZONTAL) {
      XtSetArg ( arg[n], XmNleftAttachment, XmATTACH_POSITION); n++;
      XtSetArg ( arg[n], XmNtopAttachment, XmATTACH_FORM); n++;
      XtSetArg ( arg[n], XmNbottomAttachment, XmATTACH_FORM); n++;
    }

    /* User data */
    XtSetArg ( arg[n], XmNuserData, items[i].userdata); n++;

    child = XtCreateManagedWidget ( items[i].name, class, butform, arg, n);

    /* Tool tips only for buttons with pixmaps */
    if (items[i].sens != (Pixmap)NULL)
      installToolTips (child);

    /* Event procedure */
    if (items[i].eventproc)
      XtAddEventHandler ( child, EnterWindowMask | LeaveWindowMask, False,
                         (XtEventHandler) items[i].eventproc, (XtPointer) NULL);

    /* Activate callback */
    if (items[i].activecallbackproc)
       XtAddCallback ( child, callbackname, 
                       (XtCallbackProc)items[i].activecallbackproc, 
                       (XtPointer) i );

    /* Arm callback */
    if (items[i].armcallbackproc) 
       XtAddCallback ( child, XmNarmCallback, 
                       (XtCallbackProc)items[i].armcallbackproc, 
                       (XtPointer) i );
#if 0
    if (items[i].armcallbackproc) 
       XtAddCallback ( child, XmNdisarmCallback, 
                       (XtCallbackProc)items[i].armcallbackproc, 
                       (XtPointer) i );
#endif
  }

  return (butform);
}

#include <X11/cursorfont.h>

void setBusyCursor(Widget w, int state)
{
  Widget top = GetTopShell (w);
  /* create a watch cursor and display it, if state is true */
  static Cursor watch = (Cursor)NULL;
  XSetWindowAttributes attrs;
  Display *dsp;
  Window win;
 
  if ( XtIsRealized (top) ) {
    dsp = XtDisplay (top);
    win = XtWindow (top);

    if (!watch)
      watch = XCreateFontCursor ( dsp, XC_watch );

    if ( state )
      attrs.cursor = watch;
    else
      attrs.cursor = (Cursor)NULL;

    XChangeWindowAttributes ( dsp, win, CWCursor, &attrs);
    XmUpdateDisplay ( top );
  }
}

/* Return true if it seems we're running under CDE */
int isCDE()
{
  Display *dpy = XOpenDisplay (NULL);
  Window root = DefaultRootWindow (dpy);
  Atom atom, type;
  int  format;
  unsigned long nitems, remaining;
  unsigned char *prop;

  if (dpy) {
    atom = XInternAtom( dpy, "_DT_SM_WINDOW_INFO", True );
    if (atom != None) {
      if (XGetWindowProperty (dpy, root, atom,
                              0, (65536 / sizeof (long)),
                              False,
                              XA_STRING,
                              &type, &format,
                              &nitems, &remaining,
                              &prop)
          == Success)
        return True;
    }

    atom = XInternAtom( dpy, "_VUE_SM_WINDOW_INFO", True );
    if (atom != None) {
      if (XGetWindowProperty (dpy, root, atom,
                              0, (65536 / sizeof (long)),
                              False,
                              XA_STRING,
                              &type, &format,
                              &nitems, &remaining,
                              &prop)
          == Success)
        return True;
    }
  }
  return False;
}

/* Return fallback resources. Values based on whether we're running under
** CDE or not.
*/
String *getFallbackResource()
{
  static String common[] = {
#include "resCommon.h"
          NULL
  };
  static String nonCDE[] = {
#include "resCommon.h"
#include "resColor.h"
          NULL
  };

  if (isCDE())
    return (common);
  else
    return(nonCDE);

}

#ifdef XQUOTE
/* Allow internal changes to force top level to change size. */
void AllowShellResize (Widget w, int command)
{
  static int state = UNLOCK;

  if ( (state == UNLOCK) || (command & UNLOCK) ) {
    if (command & ALLOW) {
      XtVaSetValues( GetTopShell(w), XmNallowShellResize, True, NULL );
      XSync( XtDisplay(w), False );
    } else if (command & DISALLOW) {
      XSync( XtDisplay(w), False );
      XtVaSetValues( GetTopShell(w), XmNallowShellResize, False, NULL );
    }
  }

  if (command & LOCK)
    state = LOCK;
  else if (command & UNLOCK)
    state = UNLOCK;
}

/* Handle popup menu event */
/* ARGSUSED */
void PostIt ( Widget w, XtPointer client_data, XEvent *event)
{
  Widget popup = (Widget) client_data;
  XButtonPressedEvent *bevent = (XButtonPressedEvent *)event;

  /* Should only be button 3, but if we're in a CDE front panel, we'll
  ** never see it; how to allow button 2 as well? */
  if (bevent->button != 3)
    return;
  XmMenuPosition (popup, bevent);
  XtManageChild (popup);
}

/*
** Progress Bar Sweep
*/
static int running = False;

/* ARGSUSED */
static void cylon ( int pos, XtIntervalId timer)
{
  extern Widget ProgressBar;
  static int dir = 0;

  XmScrollBarSetValues ( ProgressBar, pos, 10, 0, 0, False);

  if (dir == 0) {
    pos += 2;
    if (pos > 90) {
      pos = 90;
      dir = 1;
    }
  } else {
    pos -= 2;
    if (pos < 0) {
      pos = 0;
      dir = 0;
    }
  }

  if (running)
    XtAppAddTimeOut ( XtWidgetToApplicationContext(ProgressBar),
                      50L, (XtTimerCallbackProc)cylon,
                      (XtPointer) pos );
  else
    XmScrollBarSetValues ( ProgressBar, 0, 100, 0, 0, False);
}

void cylonStart( int control)
{
  extern Widget ProgressBar;

  if (ProgressBar == (Widget)NULL)
    return;

  if (control == True && !running) {
    running = True;
    cylon ( 0, (XtIntervalId) NULL);
  } else
    running = False;
}
#endif
