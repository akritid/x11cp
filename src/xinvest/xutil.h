/*
  Xinvest is copyright 1996 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.8 $ $Date: 1997/08/16 20:59:12 $
*/
#ifndef XUTIL_H
#define XUTIL_H

#if XmVERSION == 1
/* Motif 1.x toggle button does not have XmSET, only True or False */
#define XmSET   True
#define XmUNSET False
#endif

typedef struct {
       char *name;                  /* "button_0", "separator_0", etc */
       char *class;                 /* XmVaPUSHBUTTON, etc */
       XtCallbackProc callbackproc; /* NULL if none */
} MENUITEMS;

typedef struct {
  char *name;                  /* "button_0", "separator_0", etc */
  char *class;                 /* XmVaPUSHBUTTON, etc */
  Pixmap sens, insens, sel;    /* Pixmap, NULL if not used */
  XtPointer userdata;          /* XmNuserData */
  XtCallbackProc eventproc;    /* proc for Enter/Leave Window, NULL if none */
  XtCallbackProc activecallbackproc; /* activate callback, NULL if none */
  XtCallbackProc armcallbackproc;    /* arm callback, NULL if none */
} BUTTONITEMS;

Widget GetTopShell (Widget); /* Get top most widget in the tree containing w */
void   DestroyShell (Widget, XtPointer, XtPointer);

void CenterWidget ( Widget, Widget); /* Center 2nd param in 1st param parent */

Widget makeMenuPulldown (Widget, char *, int, MENUITEMS *, int);
Widget makeButtonbar (Widget, char *, int, BUTTONITEMS *, int);

void   setBusyCursor (Widget, int);
int    isCDE();                      /* Return true if running under CDE */

String *getFallbackResource();

#ifdef XQUOTE
void AllowShellResize (Widget, int);
void PostIt (Widget, XtPointer, XEvent *);
void cylonStart (int);
#endif

#endif
