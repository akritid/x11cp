#include "toolbar.h"
#include <string.h>
#include <unistd.h>

Widget file_dialog, select_dialog, description_dialog;

extern int childs_open;
extern Widget add_new_button(Widget, Widget, int);

void pixmap_selection(Widget);

void app_ok(Widget file, XtPointer client_data, XtPointer
		      call_data)
{
    char *filename;
    Widget curr_button;
    
    XmFileSelectionBoxCallbackStruct *cbs =
	(XmFileSelectionBoxCallbackStruct *) call_data;

    childs_open--;
    if(!XmStringGetLtoR(cbs->value, XmFONTLIST_DEFAULT_TAG, &filename))
	return;

    toolbar = start;
    curr_button = XtParent(XtParent(file));
    
    while((toolbar->button != curr_button) && (toolbar->next != NULL))
	toolbar = toolbar->next;
    if(toolbar)
	strcpy(toolbar->app_path, filename);

    XtFree(filename);
    XtDestroyWidget(file);
    file_dialog = NULL;
}

void app_cancel(Widget file, XtPointer client_data, XtPointer
		call_data)
{
    XtDestroyWidget(file);

    childs_open--;

    if(file == file_dialog)
      file_dialog = NULL;
    if(file == select_dialog)
      select_dialog = NULL;
    if(file == description_dialog)
      description_dialog = NULL;

}

void get_application(Widget button)
{
    char path[MAXPATH];
    int counter;

    if(file_dialog)
      return;

    childs_open++;

    toolbar = start;
    while(toolbar->button != button)
	toolbar = toolbar->next;
    
    strcpy(path, toolbar->app_path);
    counter = strlen(path);

    while((path[counter--] != '/') && (counter > 0));
    path[++counter] = '\0';

    file_dialog = XmCreateFileSelectionDialog(button,
					      "File",
					      NULL,
					      0);
    XtVaSetValues(file_dialog,
		  XmNdialogStyle,
		  XmDIALOG_PRIMARY_APPLICATION_MODAL,
		  XmNdialogTitle,
		  XmStringCreateLocalized("File Selection"),
		  XmNpattern,
		  XmStringCreateLocalized("*"),
		  XmNdirectory,
		  XmStringCreateLocalized(path),
		  XmNresizePolicy,
		  XmRESIZE_ANY,
		  NULL);
    
    XtAddCallback(file_dialog, XmNcancelCallback,
		  app_cancel, NULL);
    XtAddCallback(file_dialog, XmNokCallback, app_ok, NULL);

    XtManageChild(file_dialog);
}

void app_callback( Widget button, XtPointer client_data, XtPointer
			 call_data)
{
  int pid, counter;
  char tmp[MAXPARAM];
  
  
  toolbar = start;
  while(toolbar->button != button)
    toolbar = toolbar->next;
  
  if(!toolbar)
    return;
  
  if(toolbar->type == SUB)
    {
      XtUnmanageChild(XtParent(toolbar->rowcol));
      childs_open--;
    }

  if((pid = fork()) < 0)
    {
      fprintf(stderr, "xmtoolbar: can't fork\n");
    }
  else
    if(pid == 0)
      {
	char *args[MAXPARAM];
	counter = 0;
	strcpy(tmp, toolbar->parameters);
	args[counter] = malloc(strlen(toolbar->app_path));
	strcpy(args[counter], toolbar->app_path);
	counter++;
	if(strcmp("", toolbar->parameters))
	  {
	    args[counter]=strtok(tmp, " \t"); 
	    counter++; 
	    while((args[counter] = strtok(NULL, " \t")) != NULL)
	      counter++; 
	  }    
	args[counter] = NULL;
	if(execvp(toolbar->app_path, args) < 0)
	  {
	    fprintf(stderr, "xmtoolbar's child: exec failed\n");
	    exit(1);
	  }
      }
}

void delete_group(void)
{
  tool_button *help, *help_2;

  help = toolbar->next;
  while((help->next)&&(help->next->type == SUB) )
    help = help->next;
  help_2 = toolbar->next;
  if(help)
    toolbar->next = help->next;
  else
    toolbar->next = NULL;
  help->next = NULL;
  while(help_2) 
    {
      XtDestroyWidget(help_2->button);
      if(help_2->type == GROUP)
	XtDestroyWidget(XtParent(help_2->rowcol));
      help = help_2;
      help_2 = help_2->next;
      free(help);
    }
}

void delete_ok(Widget parent, XtPointer client_data, XtPointer
	       call_data)
{
  Dimension bx, by;
  Position x_pos, y_pos;

  delete_group();
  XtUnmanageChild(parent);

  XtVaGetValues(start->button,
		XmNheight,
		&button_y,
		XmNwidth,
		&button_x,
		NULL);

  bx = button_x;
  by = button_y;
  
  if(((orientation == XmHORIZONTAL) && (position == BOTTOMRIGHT)) || 
     ((orientation == XmVERTICAL) && (position == BOTTOMRIGHT)) ||
     ((orientation == XmHORIZONTAL) && (position == TOPRIGHT)) ||
     ((orientation == XmVERTICAL) && (position == BOTTOMLEFT)))
    {
      XtVaGetValues(toplevel,
		    XmNx,
		    &x_pos,
		    XmNy,
		    &y_pos,
		    NULL);
      
      if(orientation == XmHORIZONTAL)
	by = 0;
      else
	bx = 0;
      
      XtVaSetValues(toplevel,
		    XmNx,
		    x_pos + bx,
		    XmNy,
		    y_pos + by,
		    NULL);
    }
}
 
void ask_for_delete(void)
{
  if((!toolbar->next->next) || (toolbar->next->next->type != SUB))
    delete_group();
  else
    {
      Widget question;

      question = XmCreateQuestionDialog(toplevel,
					"Question",
					NULL,
					0);

      
      XtVaSetValues(question,
		    XmNdialogStyle,
		    XmDIALOG_PRIMARY_APPLICATION_MODAL,
		    XmNdialogTitle,
		    XmStringCreateLocalized("Are you sure?"),
		    XmNmessageString,
		    XmStringCreateLocalized("Group not empty - delete it?"),
		    NULL);

      XtAddCallback(question, XmNokCallback, delete_ok, NULL);
      XtDestroyWidget(XmMessageBoxGetChild(question, XmDIALOG_HELP_BUTTON)); 
      XtManageChild(question);
    }
}

void delete_button(Widget button)
{
    tool_button *help;
    Dimension bx, by;
    Position x_pos, y_pos;
    int type;

    toolbar = start;
    while(toolbar)
      {
	if(toolbar->next->button == button)
	  {
	    type = toolbar->next->type;

	    if(toolbar->next->type != SUB)
	      button_num--;
		
	    if(toolbar->next->type == GROUP)
	      {
		ask_for_delete();
		break;
	      }
	    else
	      {
		help = toolbar->next;
		if(toolbar->next->next)
		  toolbar->next = toolbar->next->next;
		else
		  toolbar->next = NULL;
		XtDestroyWidget(help->button);
		if(help->type == SUB)
		  XtUnmanageChild(XtParent(XtParent(help->button)));
		free(help);
		break;
	      }
	  }
	toolbar = toolbar->next;
      }

    if((type == SUB) || (type == GROUP))
      return;

    XtVaGetValues(start->button,
		  XmNheight,
		  &button_y,
		  XmNwidth,
		  &button_x,
		  NULL);

    bx = button_x;
    by = button_y;

    if(((orientation == XmHORIZONTAL) && (position == BOTTOMRIGHT)) || 
       ((orientation == XmVERTICAL) && (position == BOTTOMRIGHT)) ||
       ((orientation == XmHORIZONTAL) && (position == TOPRIGHT)) ||
       ((orientation == XmVERTICAL) && (position == BOTTOMLEFT)))
      {
	XtVaGetValues(toplevel,
		      XmNx,
		      &x_pos,
		      XmNy,
		      &y_pos,
		      NULL);

	if(orientation == XmHORIZONTAL)
	  by = 0;
	else
	  bx = 0;

	XtVaSetValues(toplevel,
		      XmNx,
		      x_pos + bx,
		      XmNy,
		      y_pos + by,
		      NULL);
      }
}

void set_desc(Widget button, XtPointer client_data, XtPointer
		call_data)
{  
  char *text;
  XmSelectionBoxCallbackStruct *cbs = 
    (XmSelectionBoxCallbackStruct *) call_data;
  
  childs_open--;

  toolbar = start;
  while(toolbar->button != XtParent(XtParent(button)))
    toolbar = toolbar->next;  
  if((!toolbar) || (!XmStringGetLtoR(cbs->value,
				     XmFONTLIST_DEFAULT_TAG, &text)))
    return;
  strcpy(toolbar->description, text);
  XtFree(text);
  XtDestroyWidget(button);
  description_dialog = NULL;  

}

/* get the parameters and store them into the param-string */
void set_params(Widget button, XtPointer client_data, XtPointer
		call_data)
{
  char *text;
  XmSelectionBoxCallbackStruct *cbs = 
    (XmSelectionBoxCallbackStruct *) call_data;

  childs_open++;

  toolbar = start;
  while(toolbar->button != XtParent(XtParent(button)))
    toolbar = toolbar->next;

  if((!toolbar) || (!XmStringGetLtoR(cbs->value,
				     XmFONTLIST_DEFAULT_TAG, &text)))
    return;

  strcpy(toolbar->parameters, text);
  XtFree(text);

  XtDestroyWidget(button);
  select_dialog = NULL;
}

/* open a prompt dilog to enter the applications parameters */
void get_params(Widget button)
{

  if(select_dialog)
    return;

  toolbar = start;
  while(toolbar->button != button)
    toolbar = toolbar->next;

  if(!toolbar)
    return;

  childs_open++;

  select_dialog = XmCreatePromptDialog(button, 
				       "Prompt",
				       NULL,
				       0);
  XtVaSetValues(select_dialog,
		XmNdialogStyle,
		XmDIALOG_PRIMARY_APPLICATION_MODAL,
		XmNdialogTitle,
		XmStringCreateLocalized("Parameters"),
		XmNselectionLabelString,
		XmStringCreateLocalized("Application Parameters"),
		XmNtextString,
		XmStringCreateLocalized(toolbar->parameters),
		NULL);

  XtAddCallback(select_dialog, XmNokCallback, set_params, NULL);
  XtAddCallback(select_dialog, XmNcancelCallback, app_cancel, NULL);
  XtSetSensitive(XmSelectionBoxGetChild(select_dialog, XmDIALOG_HELP_BUTTON),
		 False);
  
  XtManageChild(select_dialog);
}

void enter_description(Widget button)
{

  if(description_dialog)
    return;

  toolbar = start;
  while(toolbar->button != button)
    toolbar = toolbar->next;

  if(!toolbar)
    return;

  childs_open++;

  description_dialog = XmCreatePromptDialog(button, 
					    "description",
					    NULL,
					    0);
  XtVaSetValues(description_dialog,		  
		XmNdialogStyle,
		XmDIALOG_PRIMARY_APPLICATION_MODAL,
		XmNdialogTitle,
		XmStringCreateLocalized("Description"),
		XmNselectionLabelString,
		XmStringCreateLocalized("Enter Description"),
		XmNtextString,
		XmStringCreateLocalized(toolbar->description),
		NULL);

  XtAddCallback(description_dialog, XmNokCallback, set_desc, NULL);
  XtAddCallback(description_dialog, XmNcancelCallback, app_cancel, NULL);
  XtSetSensitive(XmSelectionBoxGetChild(description_dialog, XmDIALOG_HELP_BUTTON),
		 False);
  
  XtManageChild(description_dialog);
}

void group_callback(Widget parent, XtPointer client_data, XtPointer
		     call_data)
{
  int layout;
  Position x, y, top_x, top_y;
  int off_x, off_y; 
  Dimension width, height;

  toolbar = start;
  while(toolbar->button != parent)
    toolbar = toolbar->next;
    
  if(!toolbar)
    return;

  XtVaGetValues(toplevel,
		XmNx,
		&top_x,
		XmNy,
		&top_y,
		NULL);
		
  if(XtIsManaged(XtParent(toolbar->rowcol)))
    {
      childs_open--;
      XtUnmanageChild(XtParent(toolbar->rowcol));
      return;
    }

  childs_open++;
  
  if(orientation == XmVERTICAL)
    layout = XmHORIZONTAL;
  else
    layout = XmVERTICAL;

  XtVaSetValues(toolbar->rowcol,
		XmNorientation,
		layout,
		NULL);

  XtManageChild(XtParent(toolbar->rowcol));
  XtVaGetValues(XtParent(toolbar->rowcol),
		XmNwidth,
		&width,
		XmNheight,
		&height,
		NULL);
		
  XtUnmanageChild(XtParent(toolbar->rowcol));

  XtVaGetValues(parent,
		XmNwidth,
		&button_x,
		XmNheight,
		&button_y,
		NULL);
		

  if(orientation == XmHORIZONTAL)
    {
      if(top_y > (screen_height/2))
	off_y = (-1) * height;
      else
	off_y = button_y;	
      off_x = 0;
    }
  else
    {
      if(top_x > (screen_width/2))
	off_x = (-1)*width;
      else
	off_x = button_x;
      off_y = 0;
    }

  XtVaGetValues(parent,
		XmNx,
		&x,
		XmNy,
		&y,
		NULL);

  XtVaSetValues(XtParent(toolbar->rowcol),
		XmNx,
		top_x + x + off_x,
		XmNy,
		top_y + y + off_y,
		NULL);
  
  XtManageChild(XtParent(toolbar->rowcol));    
  XRaiseWindow(XtDisplay(XtParent(toolbar->rowcol)), XtWindow(XtParent(toolbar->rowcol)));
}

void group_popup_callback(Widget parent, XtPointer client_data, XtPointer
		     call_data)
{
  int selected = (int) client_data;  
  Widget button = XtParent(XtParent(XtParent(parent)));

  toolbar = start;
  while(toolbar->button != button)
    toolbar = toolbar->next;

  if(!toolbar)
    return;

  switch(selected)
    {
    case 0:
      pixmap_selection(button); 
      break;      
    case 1:
      add_new_button(toolbar->rowcol, button, SUB);
      break;
    case 2:
      enter_description(button);
      break;
    case 3:
      delete_button(button);
      break;
    }

}

void new_popup_callback( Widget button, XtPointer client_data, XtPointer
			 call_data)
{
    int selected = (int) client_data;
    Widget tool_button = XtParent(XtParent(XtParent(button)));

    switch(selected)
	{
	case 0:
	  /* select the pixmap for the button */
	  pixmap_selection(tool_button);
	  break;
	case 1:
	  /* assign an application to the button */
	  get_application(tool_button);
	  break;
	case 2:
	  get_params(tool_button);
	  break;
	case 3:
	  enter_description(tool_button);
	  break;
	case 4:
	  /* remove the button from the toolbar */
	  delete_button(tool_button);
	  break;
	}

}


















