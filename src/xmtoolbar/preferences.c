#include "toolbar.h"
#include "pixmap.h"

#include <Xm/ToggleBG.h>
#include <Xm/ToggleB.h>
#include <Xm/SelectioB.h>
#include <Xm/Separator.h>

Widget dialog, save_toggle, ontop_toggle, quick_info_toggle, auto_hide_toggle;
Widget radio_button[4], orientation_button[2];

void change_orientation(Widget rowcol)
{

    XtVaSetValues(rowcol,
		  XmNorientation,
		  orientation,
		  NULL);
    
    XtVaGetValues(toplevel,
		  XmNheight,
		  &window_height,
		  XmNwidth,
		  &window_width,
		  NULL);

}

void cancel_callback(Widget button, XtPointer client_data, XtPointer
		     call_data)
{
    XtDestroyWidget(dialog);
    dialog = NULL;
}

void ok_callback(Widget button, XtPointer client_data, XtPointer
	     call_data)
{
    int x, y, old;
    Widget default_button = start->button;
    
    old = orientation;

    if(XmToggleButtonGetState(orientation_button[0]))
	orientation = XmHORIZONTAL;
    else
	orientation = XmVERTICAL;

    if(old != orientation)
      change_orientation(rowcol);

    if(expand)
      {
	x = button_x;
	y = button_y;
      }
    else
      {
	toolbar = start;
	while(toolbar)
	  {
	    if((toolbar->type == GROUP) &&
	       XtIsManaged(XtParent(toolbar->rowcol)))
	      XtUnmanageChild(XtParent(toolbar->rowcol));
	    toolbar = toolbar->next;
	  }
	x = window_width;
	y = window_height;
      }

    if(XmToggleButtonGetState(radio_button[0]))
	{
	    position = TOPLEFT;
	    XtVaSetValues(toplevel,
			  XmNx,
			  0,
			  XmNy,
			  0,
			  NULL);
	    XtVaSetValues(default_button,
			  XmNpositionIndex,
			  0,
			  NULL);
	}

    if(XmToggleButtonGetState(radio_button[1]))
	{
	    position = TOPRIGHT;
	    XtVaSetValues(toplevel,
			  XmNx,
			  screen_width - x,
			  XmNy,
			  0,
			  NULL);
	    if((!expand) && (orientation == XmHORIZONTAL))
		XtVaSetValues(default_button,
			      XmNpositionIndex,
			      XmLAST_POSITION,
			      NULL);
	    else
		if(!expand)
		    XtVaSetValues(default_button,
				  XmNpositionIndex,
				  0,
				  NULL);
	}

    if(XmToggleButtonGetState(radio_button[2]))
	{
	    position = BOTTOMLEFT;
	    XtVaSetValues(toplevel,
			  XmNx,
			  0,
			  XmNy,
			  screen_height - y,
			  NULL);
	    if((!expand) && (orientation == XmVERTICAL))
		XtVaSetValues(default_button,
			      XmNpositionIndex,
			      XmLAST_POSITION,
			      NULL);
	    else
		if(!expand)
		    XtVaSetValues(default_button,
				  XmNpositionIndex,
				  0,
				  NULL);

	}

    if(XmToggleButtonGetState(radio_button[3]))
	{
	    position = BOTTOMRIGHT;
	    XtVaSetValues(toplevel,
			  XmNx,
			  screen_width - x,
			  XmNy,
			  screen_height - y,
			  NULL);
	    if(!expand)
		XtVaSetValues(default_button,
			      XmNpositionIndex,
			      XmLAST_POSITION,
			      NULL);
	}

    XtVaSetValues(toplevel,
		  XmNwidth,
		  x,
		  XmNheight,
		  y,
		  NULL);

    if(XmToggleButtonGetState(ontop_toggle))
      {
	stay_on_top = True;
	XRaiseWindow(XtDisplay(toplevel), XtWindow(toplevel));
      }
    else
      {
	stay_on_top = False;
	XLowerWindow(XtDisplay(toplevel), XtWindow(toplevel));
      }

    if(XmToggleButtonGetState(save_toggle))
	save_on_exit = True;
    else
	save_on_exit = False;

    if(XmToggleButtonGetState(quick_info_toggle))
      quick_info = True;
    else
      quick_info = False;

    if(XmToggleButtonGetState(auto_hide_toggle))
      auto_hide = True;
    else
      auto_hide = False;

    XtDestroyWidget(dialog);
    dialog = NULL;   
}

void preferences_dialog()
{
    
    Widget
      form, 
      position_frame, 
      position_box, 
      orientation_box,
      orientation_frame,
      ok_button,
      separator,
      cancel_button;
    
    Pixmap pixmap;

    if(dialog)
	return;

    dialog = XtVaCreatePopupShell("Preferences",
				  xmDialogShellWidgetClass,
				  toplevel,
				  /*XmNallowShellResize,
				  True,*/
				  NULL);

    form = XtVaCreateManagedWidget("Form",
				   xmFormWidgetClass,
				   dialog,
				   XmNheight,
				   190,
				   XmNwidth,
				   250,
				   XmNfractionBase,
				   100,
				   NULL);
    
    position_frame = XtVaCreateManagedWidget("Frame",
					     xmFrameWidgetClass,
					     form,
					     NULL);

    position_box = XmCreateRadioBox(position_frame,
				    "Position",
				    NULL,
				    0);

    orientation_frame = XtVaCreateManagedWidget("Frame",
						xmFrameWidgetClass,
						form,
						NULL);

    orientation_box = XmCreateRadioBox(orientation_frame,
				       "Orientation",
				       NULL,
				       0);

    XtVaSetValues(position_box,
		  XmNorientation,
		  XmHORIZONTAL,
		  XmNnumColumns,
		  2,
		  XmNpacking,
		  XmPACK_COLUMN,
		  NULL);

    XtVaSetValues(position_frame,
		  XmNtopAttachment,
		  XmATTACH_POSITION,
		  XmNtopPosition,
		  2,
		  XmNleftAttachment,
		  XmATTACH_POSITION,
		  XmNleftPosition,
		  2,
		  XmNrightAttachment,
		  XmATTACH_POSITION,
		  XmNrightPosition,
		  55,
		  XmNbottomAttachment,
		  XmATTACH_POSITION,
		  XmNbottomPosition,
		  50,		  
		  NULL);

    XtVaSetValues(orientation_frame,
		  XmNtopAttachment,
		  XmATTACH_POSITION,
		  XmNtopPosition,
		  2,
		  XmNleftAttachment,
		  XmATTACH_POSITION,
		  XmNleftPosition,
		  57,
		  XmNrightAttachment,
		  XmATTACH_POSITION,
		  XmNrightPosition,
		  98,
		  XmNbottomAttachment,
		  XmATTACH_POSITION,
		  XmNbottomPosition,
		  50,
		  NULL);

    XtVaCreateManagedWidget("Position:",
			    xmLabelGadgetClass,
			    position_frame,
			    XmNchildType,
			    XmFRAME_TITLE_CHILD,
			    NULL);



    XtVaCreateManagedWidget("Orientation:",
			    xmLabelGadgetClass,
			    orientation_frame,
			    XmNchildType,
			    XmFRAME_TITLE_CHILD,
			    NULL);

    XtVaSetValues(orientation_box,
		  XmNorientation,
		  XmVERTICAL,
		  XmNpacking,
		  XmPACK_COLUMN,
		  NULL);

    ok_button = XtVaCreateManagedWidget("OK",
					xmPushButtonWidgetClass,
					form,		
					XmNtopAttachment,
					XmATTACH_POSITION,
					XmNtopPosition,
					84,
					XmNleftAttachment,
					XmATTACH_POSITION,
					XmNleftPosition,
					10,
					XmNrightAttachment,
					XmATTACH_POSITION,
					XmNrightPosition,
					49,
					XmNbottomAttachment,
					XmATTACH_POSITION,
					XmNbottomPosition,
					98,
					NULL);

    cancel_button = XtVaCreateManagedWidget("Cancel",
					    xmPushButtonWidgetClass,
					    form,
					    XmNtopAttachment,
					    XmATTACH_POSITION,
					    XmNtopPosition,
					    84,
					    XmNleftAttachment,
					    XmATTACH_POSITION,
					    XmNleftPosition,
					    51,
					    XmNrightAttachment,
					    XmATTACH_POSITION,
					    XmNrightPosition,
					    90,
					    XmNbottomAttachment,
					    XmATTACH_POSITION,
					    XmNbottomPosition,
					    98,
					    NULL);
 

    save_toggle = XtVaCreateManagedWidget("Save-on-exit",
					  xmToggleButtonGadgetClass,
					  form,
					  XmNtopAttachment,
					  XmATTACH_POSITION,
					  XmNtopPosition,
					  52,
					  XmNleftAttachment,
					  XmATTACH_POSITION,
					  XmNleftPosition,
					  5,
#if XmVERSION >= 2
 					  XmNindicatorOn,
					  XmINDICATOR_CHECK_BOX,
					  XmNenableToggleVisual,
					  True,
#endif
					  NULL);

    ontop_toggle = XtVaCreateManagedWidget("Stay-on-top",
					   xmToggleButtonGadgetClass,
					   form,
					   XmNtopAttachment,
					   XmATTACH_POSITION,
					   XmNtopPosition,
					   52,
					   XmNleftAttachment,
					   XmATTACH_POSITION,
					   XmNleftPosition,
					   60,
#if XmVERSION >= 2
					   XmNindicatorOn,
					   XmINDICATOR_CHECK_BOX,
					   XmNenableToggleVisual,
					   True,
#endif
					   NULL);

    quick_info_toggle = XtVaCreateManagedWidget("Quick-Info",
						xmToggleButtonGadgetClass,
						form,
						XmNtopAttachment,
						XmATTACH_POSITION,
						XmNtopPosition,
						65,
						XmNleftAttachment,
						XmATTACH_POSITION,
						XmNleftPosition,
						5,
#if XmVERSION >= 2
						XmNindicatorOn,
						XmINDICATOR_CHECK_BOX,
						XmNenableToggleVisual,
						True,
#endif
						NULL);

     auto_hide_toggle = XtVaCreateManagedWidget("Auto-Hide",
						xmToggleButtonGadgetClass,
						form,
						XmNtopAttachment,
						XmATTACH_POSITION,
						XmNtopPosition,
						65,
						XmNleftAttachment,
						XmATTACH_POSITION,
						XmNleftPosition,
						60,
#if XmVERSION >= 2
						XmNindicatorOn,
						XmINDICATOR_CHECK_BOX,
						XmNenableToggleVisual,
						True,
#endif
						NULL);
   
    separator = XtVaCreateManagedWidget("Separator",
					xmSeparatorWidgetClass,
					form,
					XmNtopAttachment,
					XmATTACH_POSITION,
					XmNtopPosition,
					80,
					XmNleftAttachment,
					XmATTACH_FORM,
					XmNrightAttachment,
					XmATTACH_FORM,
					NULL);

    pixmap = XCreatePixmapFromBitmapData(display,
					 XRootWindowOfScreen(screen), 
					 topleft_bits,
					 topleft_width, 
					 topleft_height,
					 0,
					 0xffffff,
					 screen_depth);

    radio_button[0] = XtVaCreateManagedWidget("Top-Left",
					      xmToggleButtonGadgetClass,
					      position_box,
					      XmNlabelType,
					      XmPIXMAP,
					      XmNlabelPixmap,
					      pixmap,
					      NULL);

    pixmap = XCreatePixmapFromBitmapData(display,
					 XRootWindowOfScreen(screen), 
					 topright_bits,
					 topright_width, 
					 topright_height,
					 0,
					 0xffffff,
					 screen_depth);

    radio_button[1] = XtVaCreateManagedWidget("Top-Right",
					      xmToggleButtonGadgetClass,
					      position_box,
					      XmNlabelType,
					      XmPIXMAP,
					      XmNlabelPixmap,
					      pixmap,
					      NULL);

    pixmap = XCreatePixmapFromBitmapData(display,
					 XRootWindowOfScreen(screen), 
					 bottomleft_bits,
					 bottomleft_width, 
					 bottomleft_height,
					 0,
					 0xffffff,
					 screen_depth);


    radio_button[2] = XtVaCreateManagedWidget("Bottom-Left",
					      xmToggleButtonGadgetClass,
					      position_box,
					      XmNlabelType,
					      XmPIXMAP,
					      XmNlabelPixmap,
					      pixmap,
					      NULL);

    pixmap = XCreatePixmapFromBitmapData(display,
					 XRootWindowOfScreen(screen), 
					 bottomright_bits,
					 bottomright_width, 
					 bottomright_height,
					 0,
					 0xffffff,
					 screen_depth);				    

    radio_button[3] = XtVaCreateManagedWidget("Bottom-Right",
					      xmToggleButtonGadgetClass,
					      position_box,
					      XmNlabelType,
					      XmPIXMAP,
					      XmNlabelPixmap,
					      pixmap,
					      NULL);

    orientation_button[0] = XtVaCreateManagedWidget("Horizontal",
						    xmToggleButtonGadgetClass,
						    orientation_box,
						    NULL);    
    orientation_button[1] = XtVaCreateManagedWidget("Vertical",
						    xmToggleButtonGadgetClass,
						    orientation_box,
						    NULL);            

    if(orientation == XmHORIZONTAL)
	XmToggleButtonSetState(orientation_button[0], True,
			       True);
    else
	XmToggleButtonSetState(orientation_button[1], True,
			       True);

    XtAddCallback(cancel_button, XmNactivateCallback, cancel_callback, NULL);
    XtAddCallback(ok_button, XmNactivateCallback,
		  ok_callback, NULL);

    if(stay_on_top)
	XmToggleButtonSetState(ontop_toggle, True, True);
    else
	XmToggleButtonSetState(ontop_toggle, False, True);   

    if(save_on_exit)
	XmToggleButtonSetState(save_toggle, True, True);
    else
	XmToggleButtonSetState(save_toggle, False, True);

    if(quick_info)
	XmToggleButtonSetState(quick_info_toggle, True, True);
    else
	XmToggleButtonSetState(quick_info_toggle, False, True);

    if(auto_hide)
 	XmToggleButtonSetState(auto_hide_toggle, True, True);
    else
	XmToggleButtonSetState(auto_hide_toggle, False, True);
     
    switch(position)
	{
	case TOPLEFT:
	    XmToggleButtonSetState(radio_button[0], True, True);
	    break;
	case TOPRIGHT:
	    XmToggleButtonSetState(radio_button[1], True, True);
	    break;	    
	case BOTTOMLEFT:
	    XmToggleButtonSetState(radio_button[2], True, True);
	    break;
	case BOTTOMRIGHT:
	    XmToggleButtonSetState(radio_button[3], True, True);
	    break;
	}

    XtManageChild(dialog);
    XtManageChild(form);
    XtManageChild(position_box);
    XtManageChild(orientation_box);
    XtManageChild(save_toggle);
    XtManageChild(ontop_toggle);
    XtManageChild(auto_hide_toggle);
    XtManageChild(separator);
}





