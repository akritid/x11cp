#include "toolbar.h"

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <assert.h>   
#include <getopt.h>

extern void group_popup_callback();
extern void group_callback();
extern void show_help();
extern void delete_help();


void get_def_pixmap(Widget parent);
void button_event(Widget, XtPointer, XEvent);
void popup_callback(Widget, XtPointer, XtPointer);
void new_popup_callback(Widget, XtPointer, XtPointer);
void show_bar(Widget, XtPointer, XEvent *);
void hide_bar(Widget, XtPointer, XEvent *);

Screen *screen;
Display *display;
Widget toplevel, rowcol;
int screen_depth,
    screen_height,
    screen_width;
Dimension button_x, button_y;
Dimension window_width,
          window_height;
int button_num = 0;
int expand = 0;
int orientation = XmVERTICAL;
int position = TOPLEFT;
int save_on_exit = False;
int stay_on_top = True;
int group = NORMAL;
int quick_info = True;
int auto_hide = True;
int childs_open = 0;

Position old_y;

int not_again = True;

extern char *optarg;
extern int optind, opterr, optopt;

/* If config file is specified on the command line, we use it; else
** the default config file $HOME/.toolbar is used
*/
char TOOLBAR_CONFIG_FILE[255];

/* Coordinates for the toolbar, specified on command line or read
** from the config file
*/
int START_X=0;
int START_Y=0;


char def_pix[255]=DEFAULT_PIX;

tool_button *toolbar, *start;

extern void app_callback();

/* this function expands or shrinks the application when pressing the */
/* default button */
void button_pressed(Widget button, XtPointer client_data, XtPointer
		     call_data)
{
    Position x_pos, y_pos;
    int pos_index;

    if( !expand )
	{
	  /* save the current values */
	  XtVaGetValues(toplevel,
			XmNheight,
			&window_height,
			XmNwidth,
			&window_width,
			NULL);

	  
	  if(position == TOPRIGHT)
	    XtVaSetValues(toplevel,
			  XmNx,
			  screen_width - button_x,
			  XmNy,
			  0,
			  NULL);

	  if(position == BOTTOMRIGHT)
	    XtVaSetValues(toplevel,
			  XmNx,
			  screen_width - button_x,
			  XmNy,
			  screen_height - button_y,
			  NULL);	

	  if(position == BOTTOMLEFT)
	    XtVaSetValues(toplevel,
			  XmNx,
			  0,
			  XmNy,
			  screen_height - button_y,
			  NULL);	

 	  if(position == TOPLEFT)
	    XtVaSetValues(toplevel,
			  XmNx,
			  0,
			  XmNy,
			  0,
			  NULL);

	  XtVaGetValues(button,
			XmNpositionIndex,
			&pos_index,
			NULL);

 	  if(position == CUSTOM)
	    {
	      /*if((pos_index == XmLAST_POSITION) && (orientation ==
						    XmHORIZONTAL))*/
		XtVaSetValues(toplevel,
			      XmNx,
			      START_X,
			      XmNy,
			      START_Y,
			      NULL);
		/*else
		XtVaSetValues(toplevel,
			      XmNx,
			      START_X + window_width - button_x,
			      XmNy,
			      START_Y,
			      NULL);*/
	    }

	  XtVaSetValues(button,
			XmNpositionIndex,
			0,
			NULL);
	  
	  XtVaGetValues(start->button,
			XmNwidth,
			&button_x,
			XmNheight,
			&button_y,
			NULL);

	  XtVaSetValues(toplevel,  
			XmNheight, 
			button_y,
			XmNwidth, 
			button_x,
			NULL );
	  
	  toolbar = start;
	  while(toolbar != NULL)
	    {
	      if((toolbar->type == GROUP) &&
		 (XtIsManaged(XtParent(toolbar->rowcol))))
		XtUnmanageChild(XtParent(toolbar->rowcol));
	      toolbar = toolbar->next;
	    }
	  childs_open = 0;
	  expand = 1;
	}
    else
	{
	    if(((orientation == XmHORIZONTAL) && (position == BOTTOMRIGHT)) || 
	       ((orientation == XmVERTICAL) && (position == BOTTOMRIGHT)) ||
	       ((orientation == XmHORIZONTAL) && (position == TOPRIGHT)) ||
	       ((orientation == XmVERTICAL) && (position == BOTTOMLEFT)))
		XtVaSetValues(button,
			      XmNpositionIndex,
			      XmLAST_POSITION,
			      NULL);


	    XtVaGetValues(toplevel,
			  XmNx,
			  &x_pos,
			  XmNy,
			  &y_pos,
			  NULL);

	    if((x_pos > (screen_width/2)) &&
	       (orientation==XmHORIZONTAL))
	      {
		XtVaSetValues(button,
			      XmNpositionIndex,
			      XmLAST_POSITION,
			      NULL);
		x_pos = START_X - window_width + button_x;
	      }


	    if(((y_pos+window_height) > screen_height)&&(orientation==XmVERTICAL)) 
 		y_pos = screen_height-window_height; 
 	    if(((x_pos+window_width) > screen_width)&&(orientation==XmHORIZONTAL)) 
 		x_pos = screen_width-window_width; 
		

	    XtVaSetValues(toplevel,  
			  XmNheight, 
			  window_height,
			  XmNwidth, 
			  window_width, 
			  XmNx, 
 			  x_pos, 
 			  XmNy, 
 			  y_pos, 
			  NULL ); 

	    expand = 0;
	}

    if(stay_on_top)
      XRaiseWindow(XtDisplay(toplevel), XtWindow(toplevel));
    else
      XLowerWindow(XtDisplay(toplevel), XtWindow(toplevel));

    XmUpdateDisplay(toplevel);
}

void set_path(Widget parent, XtPointer client_data, XtPointer
		     call_data)
{
  char *filename;
  Pixmap button_pixmap;

  XmFileSelectionBoxCallbackStruct *cbs =
    (XmFileSelectionBoxCallbackStruct *) call_data;

  if(!XmStringGetLtoR(cbs->value, XmFONTLIST_DEFAULT_TAG, &filename))
    exit(1);

  strcpy(def_pix, filename);
  strcpy(start->pixmap_path, filename);
  XtDestroyWidget(XtParent(parent));
  button_pixmap = xmtGetPixmapByDepth(screen, def_pix, 0, 0, screen_depth); /* andre */
  if (button_pixmap == XmUNSPECIFIED_PIXMAP)
    get_def_pixmap(toplevel);
  else
    XtVaSetValues(start->button,
		  XmNlabelPixmap,
		  button_pixmap,
		  NULL);
  
}
void get_path(Widget parent, XtPointer client_data, XtPointer
		     call_data)
{
  Widget file_select;

  file_select = XmCreateFileSelectionDialog(parent,
					    "get_pix",
					    NULL,
					    0);
  XtAddCallback(file_select, XmNcancelCallback, (XtCallbackProc)exit,
		NULL);
  XtAddCallback(file_select, XmNokCallback, set_path, NULL);
  XtManageChild(file_select);
}

void get_def_pixmap(Widget parent)
{
  Widget message_dialog;
  XmString string;

  message_dialog = XmCreateErrorDialog(parent,
				       "Error",
				       NULL,
				       0); 
  string = XmStringLtoRCreate("Can not find \"default.xpm\"\nPress \"Ok\" to choose another pixmap as default\nor \"Cancel\" to exit program", XmSTRING_DEFAULT_CHARSET);
  XtVaSetValues(message_dialog,
		XmNmessageString,
		string,
		XmNdialogTitle,
		XmStringCreateLocalized("Default Pixmap Error"),
		XmNx,
		screen_width/2,
		XmNy,
		screen_height/2,
		NULL);

  XtManageChild(message_dialog);
  XtAddCallback(message_dialog, XmNcancelCallback,
		(XtCallbackProc)exit, NULL);
  XtAddCallback(message_dialog, XmNokCallback, get_path, NULL);

  XtPopup(XtParent(message_dialog), XtGrabNone);
  XmStringFree(string);

}

Widget add_button(Widget row_col, tool_button *button)
{
    Widget new_button, popup;
    Pixmap pixmap;
    int pos_index;
    Position x_pos, y_pos;

    pixmap = xmtGetPixmapByDepth(screen, button->pixmap_path, 0, 0, screen_depth); /* andre */
    if (pixmap == XmUNSPECIFIED_PIXMAP)
      {
	if((pixmap = xmtGetPixmapByDepth(screen, def_pix, 0, 0, /* andre */
					screen_depth)) == XmUNSPECIFIED_PIXMAP)
	  {
	    fprintf(stderr, "add_button: pixmap read failed\n" );
	    if(button == start)
		get_def_pixmap(toplevel);
	  }
      }
    
    expand = 0;
    if(button->type != SUB)
      button_num++;

    button->rowcol = row_col;
    
    if((((orientation == XmHORIZONTAL) && (position == BOTTOMRIGHT)) || 
	((orientation == XmVERTICAL) && (position == BOTTOMRIGHT)) ||
	((orientation == XmHORIZONTAL) && (position == TOPRIGHT)) ||
	((orientation == XmVERTICAL) && (position == BOTTOMLEFT)) ||
	((orientation == XmHORIZONTAL) && (position == CUSTOM))) &&
       (START_X > (screen_width/2)))
      pos_index = 0;
    else
      pos_index = XmLAST_POSITION;
    
    
    new_button = XtVaCreateManagedWidget("Button",
					 xmPushButtonWidgetClass,
					 row_col,
					 XmNlabelType, 
					 XmPIXMAP, 
					 XmNlabelPixmap,
					 pixmap,
					 XmNentryAlignment,
					 XmALIGNMENT_CENTER, 
					 XmNpositionIndex,
					 pos_index,
					 NULL);
    
    button->button = new_button;
    if((button != start) && ((button->type == NORMAL) ||
			     (button->type == SUB))) 
      popup = XmVaCreateSimplePopupMenu(new_button, "test", new_popup_callback,
					XmVaTITLE,
					XmStringCreateLocalized("Button Menu"),
					XmVaDOUBLE_SEPARATOR,
					XmVaPUSHBUTTON,
					XmStringCreateLocalized("Pixmap"), 
					'P',
					NULL, NULL,
					XmVaPUSHBUTTON,
					XmStringCreateLocalized("Application"), 
					'A',
					NULL, NULL,
					XmVaPUSHBUTTON,
					XmStringCreateLocalized("App Parameters"), 
					'r',
					NULL, NULL,
					XmVaPUSHBUTTON,
					XmStringCreateLocalized("Enter Description"), 
					'E',
					NULL, NULL,
					XmVaSEPARATOR,
					XmVaPUSHBUTTON,
					XmStringCreateLocalized("Delete Button"),
					'D',
					NULL, NULL,
					NULL);
    else
      if((button != start) && (button->type == GROUP))
	popup = XmVaCreateSimplePopupMenu(new_button, "test", group_popup_callback,
					  XmVaTITLE,
					  XmStringCreateLocalized("Group Menu"),
					  XmVaDOUBLE_SEPARATOR,
					  XmVaPUSHBUTTON,
					  XmStringCreateLocalized("Pixmap"), 
					  'P',
					  NULL, NULL,
					  XmVaPUSHBUTTON,
					  XmStringCreateLocalized("Add Button"), 
					  'A',
					  NULL, NULL,
					  XmVaPUSHBUTTON,
					  XmStringCreateLocalized("Enter Description"), 
					  'E',
					  NULL, NULL,
					  XmVaSEPARATOR,
					  XmVaPUSHBUTTON,
					  XmStringCreateLocalized("Delete Group"),
					  'D',
					  NULL, NULL,
					  NULL);
      else
	popup = XmVaCreateSimplePopupMenu(new_button, "test",
					  popup_callback,
					  XmVaTITLE,
					  XmStringCreateLocalized("Main Menu"),
					  XmVaDOUBLE_SEPARATOR,
					  XmVaPUSHBUTTON,
					  XmStringCreateLocalized("New Button"), 
					  'B',
					  NULL, NULL,
					  XmVaPUSHBUTTON,
					  XmStringCreateLocalized("New Group"), 
					  'G',
					  NULL, NULL,
					  XmVaSEPARATOR,
					  XmVaPUSHBUTTON,
					  XmStringCreateLocalized("Pixmap"), 
					  'P',
					  NULL, NULL,
					  XmVaPUSHBUTTON,
					  XmStringCreateLocalized("Save config"), 
					  'S',
					  NULL, NULL,
					  XmVaPUSHBUTTON,
					  XmStringCreateLocalized("Preferences"), 
					  'r',
					  NULL, NULL,
					  XmVaPUSHBUTTON,
					  XmStringCreateLocalized("Export config"), 
					  'E',
					  NULL, NULL,     	      
					  XmVaSEPARATOR,
					  XmVaPUSHBUTTON,
					  XmStringCreateLocalized("Info"), 
					  'I',
					  NULL, NULL,
					  XmVaPUSHBUTTON,
					  XmStringCreateLocalized("Help"), 
					  'H',
					  NULL, NULL,
					  XmVaSEPARATOR,
					  XmVaSEPARATOR,
					  XmVaPUSHBUTTON,
					  XmStringCreateLocalized("Exit"),
					  'x',
					  NULL, NULL,
					  NULL);
    
    
    XtAddEventHandler(new_button, ButtonPressMask, False, (XtEventHandler)button_event, popup);    
    XtManageChild(new_button);
    
    if(button->type != SUB)
      {
	XtVaGetValues(new_button, 
		      XmNwidth,
		      &button_x,
		      XmNheight,
		      &button_y,
		      NULL);
      }
    
    XtVaGetValues(toplevel, 
		  XmNx, 
		  &x_pos, 
		  XmNy, 
		  &y_pos,
		  XmNheight,
		  &window_height,
		  XmNwidth,
		  &window_width,
		  NULL);
    
    if(((y_pos+window_height) > screen_height)&&(orientation==XmVERTICAL)) 
      y_pos = screen_height-window_height; 
    if(((x_pos+window_width) > screen_width)&&(orientation==XmHORIZONTAL)) 
      x_pos = screen_width-window_width;   
    
    XtVaSetValues(toplevel,  
		  XmNx,
		  x_pos,
		  XmNy,
		  y_pos,
		  NULL );
    
    if(button == start)
      XtAddCallback(new_button, XmNactivateCallback,
		    button_pressed, NULL);
    else
      {
	if(button->type != GROUP)
	  XtAddCallback(new_button, XmNactivateCallback,
			app_callback, NULL);
	else
	  XtAddCallback(new_button, XmNactivateCallback,
			group_callback, NULL);	
	/*strcpy(button->description, "No description");*/
      }
    
    XtAddEventHandler(new_button, EnterWindowMask, False,
		      (XtEventHandler) show_help, NULL);
    XtAddEventHandler(new_button, LeaveWindowMask, False,
		      (XtEventHandler) delete_help, NULL);    
    return new_button;
}

/* this function reads the config file if it exists else it returns */
/* NULL and we are starting with a new toolbar */
int read_config(void)
{
    struct stat file;
    tool_button *button, *help = NULL;
    int config;
    char filename[MAXPATH];
    int counter = 0, pos;

    strcpy(filename, getenv("HOME"));
    strcat(filename, "/");
    strcat(filename, TOOLBAR_CONFIG_FILE);
  
    toolbar = malloc(sizeof(tool_button));
    start = toolbar;
    toolbar->app_path[0] = '\0'; 
    toolbar->next = NULL;
    toolbar->parameters[0] ='\0';
    toolbar->description[0] = '\0';

    if(stat(filename, &file) && (errno==ENOENT))
      {
	if( getenv("TOOLBAR"))
	  strcpy(filename, getenv("TOOLBAR"));
	else
	  return -1;

	strcat(filename, "/xmtoolbar");
	if(stat(filename, &file) && (errno==ENOENT))
	  return -1;
      }

    if((config = open(filename, O_RDONLY)) < 0)
	{
	    fprintf(stderr, "read_config: error opening config file\n");
	    return -1;
	}

    /* read the number of elements */
    /* the preferences settins are on the end of the file */
    /* so we are able to read old configuration files with a newer
       version of xmtoolbar as long as the layout of the toolbar
       structure remains unchanged */ 
    if(read(config, (char *)&counter, sizeof(int)) != sizeof(int))
      {
	fprintf(stderr, "read_config: read counter failed\n");
	exit(1);
      }

    while(counter > 0)
      {
	if(read(config, toolbar, sizeof(tool_button)) !=
	   sizeof(tool_button))
	  fprintf(stderr,"read button failed\n");
	button = malloc(sizeof(tool_button));
	help = toolbar;
	toolbar->next = button;
	toolbar = button;
	counter--;
      }
    
    if(read(config, (char *)&orientation, sizeof(int)) != sizeof(int))
      fprintf(stderr, "read orientation failed\n");
    if(read(config, (char *)&pos, sizeof(int)) != sizeof(int))
      fprintf(stderr, "read position failed\n");
    if(read(config, (char *)&save_on_exit, sizeof(int)) != sizeof(int))
      fprintf(stderr, "read save failed\n");
    if(read(config, (char *)&stay_on_top, sizeof(int)) != sizeof(int))
      fprintf(stderr, "read stay failed\n");
    if(read(config, (char *)&quick_info, sizeof(int)) != sizeof(int))
      fprintf(stderr, "read quick info failed\n");

    if(position != CUSTOM) {
      position = pos;
      if(read(config, (char *)&START_X, sizeof(int)) != sizeof(int))
	fprintf(stderr, "read START_X failed\n");
      if(read(config, (char *)&START_Y, sizeof(int)) != sizeof(int))
	fprintf(stderr, "read START_Y failed\n");  
    }

    if(read(config, (char *)&auto_hide, sizeof(int)) != sizeof(int))
      fprintf(stderr, "read auto hide failed\n");  

    strcpy(def_pix, start->pixmap_path);
    
    toolbar = help;
    toolbar->next = NULL;

    close(config);
    return 0;
}

#if XmVERSION < 2 /* andre */
void handler( String msg )
{
  if( msg )
    if( strcmp( msg,
                "\n"
                "    Name: Rowcol\n"
                "    Class: XmRowColumn\n"
                "    Attempt to add wrong type child to a homogeneous RowColumn widget\n" ) != 0 )
      {
        fprintf( stderr, "Warning:" );
        fprintf( stderr, "%s\n", msg );
      }
}
#endif

int main(int argc, char *argv[])
{

    Widget group, row_col;
    XtAppContext app;
    int config, counter, layout;
    Position x, y;
    int c, flag=0;

    button_x = 0;
    button_y = 0;
    
    XtSetLanguageProc(NULL, NULL, NULL);

    strcpy(def_pix, DEFAULT_PIX);
    
    toplevel = XtVaAppInitialize(&app,
				 "xmtoolbar",
				 NULL,
				 0,
				 &argc,
				 argv,
				 NULL,
				 NULL);


    signal(SIGCHLD, SIG_IGN);

#if XmVERSION < 2 /* andre */
    XtAppSetWarningHandler( app, handler );
#endif

    XtVaSetValues(toplevel, 
  		  /*XmNmwmDecorations,   
  		  MWM_DECOR_BORDER,*/
		  XmNshadowThickness,
		  0,
		  XmNresizePolicy,
		  XmRESIZE_ANY,
		  XmNallowShellResize,
		  True,
		  XmNoverrideRedirect, 
		  True,
		  NULL); 

    rowcol = XtVaCreateManagedWidget("Rowcol",
				     xmRowColumnWidgetClass,
				     toplevel,
				     XmNpacking,
				     XmPACK_COLUMN,
				     XmNentryAlignment,
				     XmALIGNMENT_CENTER,
				     XmNentryVerticalAlignment,
				     XmALIGNMENT_CENTER,
				     XmNmarginHeight,
				     0,
				     XmNmarginWidth,
				     0,
				     XmNrowColumnType, 
				     XmMENU_BAR, 
				     XmNshadowThickness,
				     0,
				     XmNadjustLast,
				     True,
				     XmNresizePolicy,
				     XmRESIZE_ANY,
				     NULL);


    /* get the screen parameters */
    display = XtDisplay(toplevel);
    screen = XtScreen(toplevel);
    screen_depth = XDefaultDepthOfScreen(screen);
    screen_height = HeightOfScreen(screen);
    screen_width = WidthOfScreen(screen);

    strcpy(TOOLBAR_CONFIG_FILE, ".toolbar");
    while( (c = getopt( argc, argv, "c:x:y:" )) !=EOF )
      {
	switch(c)
	  {
	  case 'c' : 
	    strcpy( TOOLBAR_CONFIG_FILE, optarg );
	    break;
	  case 'x' : 
	    START_X = atoi(optarg);
	    flag++; 
	    break;
	  case 'y' : 
	    START_Y = atoi(optarg);
	    flag++;
	    break;
	    
	  default : 
	    fprintf(stderr,"usage: xmtoolbar [-c file] [-x xpos -y ypos]\n");
	    exit(1);
	  }
    }

    if(flag == 2)
      position = CUSTOM;

    config = read_config() ;
    if(config)
	fprintf(stderr,"xmtoolbar: Found no config file - starting from scratch\n");

    XtVaSetValues(rowcol,
		  XmNorientation,
		  orientation,
		  NULL);

    if(config)
	strcpy(toolbar->pixmap_path, DEFAULT_PIX);

    counter = 0;
    toolbar = start;
    while( toolbar != NULL)
      {
	switch(toolbar->type)
	  {
	  case GROUP:
	    XtVaGetValues(toplevel,
			  XmNx,
			  &x,
			  XmNy,
			  &y,
			  NULL);
	    
	    if(orientation == XmHORIZONTAL)
	      {
		x += counter * button_x;
		y += button_y;
		layout = XmVERTICAL;
	      }
	    else
	      {
		x += button_x;
		y += counter * button_y; 
		layout = XmHORIZONTAL;
	      }
	    group = XtVaCreatePopupShell("Popup",
					 topLevelShellWidgetClass,
					 rowcol,
					 XmNoverrideRedirect, 
					 True,
					 XmNallowShellResize,
					 True,
					 XmNresizePolicy,
					 XmRESIZE_ANY,
					 XmNx,
					 x,
					 XmNy,
					 y,
					 NULL);
	    
	    row_col = XtVaCreateManagedWidget("rowcol",
					      xmRowColumnWidgetClass,
					      group,
					      XmNorientation,
					      layout,
					      XmNpacking,
					      XmPACK_COLUMN,
					      XmNentryAlignment,
					      XmALIGNMENT_CENTER,
					      XmNentryVerticalAlignment,
					      XmALIGNMENT_CENTER,
					      XmNmarginHeight,
					      0,
					      XmNmarginWidth,
					      0,
					      XmNrowColumnType, 
					      XmMENU_BAR, 
					      XmNshadowThickness,
					      0,
					      NULL);
	    add_button(rowcol, toolbar);
	    toolbar->rowcol = row_col;
	    counter++;
	    break;
	  case SUB:
	    add_button(row_col, toolbar);
	    toolbar->rowcol = row_col;
	    break;
	  default:
	    counter++;
	    add_button(rowcol, toolbar);
	  }
	toolbar = toolbar->next;
      }

    XtVaGetValues(start->button,
		  XmNwidth,
		  &button_x,
		  XmNheight,
		  &button_y,
		  NULL);

    
    XtRealizeWidget(toplevel);


    XtVaGetValues(toplevel,
		  XmNwidth,
		  &window_width,
		  XmNheight,
		  &window_height,
		  NULL);

    if(position == TOPLEFT) 
      XtVaSetValues(toplevel,
		    XmNx,
		    0,
		    XmNy,
		    0,
		    NULL);
    if(position == TOPRIGHT) 
      XtVaSetValues(toplevel,
		    XmNx,
		    screen_width - window_width,
		    XmNy,
		    0,
		    NULL);

    if(position == BOTTOMLEFT) 
      XtVaSetValues(toplevel,
		    XmNx,
		    0,
		    XmNy,
		    screen_height - window_height,
		    NULL);

    if(position == BOTTOMRIGHT) 
      XtVaSetValues(toplevel,
		    XmNx,
		    screen_width - window_width,
		    XmNy,
		    screen_height - window_height,
		    NULL);

    if(position == CUSTOM) 
      {
	if((orientation == XmHORIZONTAL) && (START_X >
					     (screen_width/2)))
	        XtVaSetValues(toplevel,
			      XmNx,
			      START_X - window_width + button_x,
			      XmNy,
			      START_Y,
			      NULL);
	else
	  XtVaSetValues(toplevel,
			XmNx,
			START_X,
			XmNy,
			START_Y,
			NULL);
      }

    XtAddEventHandler(toplevel, EnterWindowMask, False,
		      (XtEventHandler) show_bar, NULL);
    XtAddEventHandler(toplevel, LeaveWindowMask, False,
		      (XtEventHandler) hide_bar, NULL);
    
    if(stay_on_top)
      XRaiseWindow(XtDisplay(toplevel), XtWindow(toplevel));
    else
      XLowerWindow(XtDisplay(toplevel), XtWindow(toplevel));

    XtAppMainLoop(app);
    return 0;
}

/* this function moves the toolbar out of the screen except of 3
   pixels so it can be activated again */
void hide_bar(Widget button, XtPointer client_data, XEvent *event)
{
  Position y_pos;

  XtVaGetValues(toplevel,
		XmNy,
		&old_y,
		NULL);

  if(!not_again)
    return;

  if(auto_hide == False)
    return;

  /* auto hide only when displayed at the screen borders */
  if(((position == CUSTOM) && (old_y != 0)) &&
     ((position == CUSTOM) && (old_y != screen_height)))
    return;

  /* if any group button displays other buttons then don't hide
     until they are all closed */
  if(childs_open > 0)
    return;

  not_again = False;
  y_pos = old_y;
  
  if(y_pos > (screen_height/2))
    while(y_pos != (screen_height - 3))
      {
	y_pos++;
	XtVaSetValues(toplevel,
		      XmNy,
		      y_pos,
		      NULL);
	XmUpdateDisplay(toplevel);
      }
  else
    {
      if(orientation == XmHORIZONTAL)
	y_pos += button_y;
      else
	y_pos += window_height;
      while(y_pos > 3)
	{
	  y_pos--;
	  if(orientation == XmHORIZONTAL)
	    XtVaSetValues(toplevel,
			  XmNy,
			  y_pos - button_y,
			  NULL);
	  else
	    XtVaSetValues(toplevel,
			  XmNy,
			  y_pos - window_height,
			  NULL);	    
	  XmUpdateDisplay(toplevel);
	}
    }
}

void show_bar(Widget button, XtPointer client_data, XEvent *event)
{
  Position y_pos;

  
  if(not_again)
    return;

  not_again = True;

  XtVaGetValues(toplevel,
		XmNy,
		&y_pos,
		NULL);

  if(old_y > (screen_height/2))
    while(y_pos != old_y)
      {
	y_pos--;
	XtVaSetValues(toplevel,
		      XmNy,
		      y_pos,
		      NULL);
	XmUpdateDisplay(toplevel);
      }
  else
    while(y_pos < old_y)
      {
	y_pos++;
	XtVaSetValues(toplevel,
		      XmNy,
		      y_pos,
		      NULL);
	XmUpdateDisplay(toplevel);
      }

}

Pixmap xmtGetPixmapByDepth( Screen *screen,                          /* andre */
                                   char *image_name,
                                   Pixel foreground,
                                   Pixel background,
                                   int depth)
{
  Pixmap pixmap;

#if XmVERSION < 2 

#include <X11/xpm.h>

  static Boolean first = True;

  SubstitutionRec substitutions[] = { { 'B', NULL },
                                      { 'T', "bitmaps" },
                                      { 'S', NULL } };

  static char *search_path;

  char *image_path;

  XpmAttributes attributes;
  int status;


  substitutions[0].substitution = image_name;

  if( first )
    {
      char *xbmlangpath = getenv( "XBMLANGPATH" );

      first = False;

      if( xbmlangpath )
        search_path = xbmlangpath;
      else
        {
          char *home = getenv( "HOME" );
          char *xapplresdir = getenv( "XAPPLRESDIR" );

          if( xapplresdir )
            {
              search_path = malloc( 9999 );

              search_path[0] = '\0';

              strcat( search_path, "%B:" );

              strcat( search_path, xapplresdir ); strcat( search_path, "/%L/bitmaps/%N/%B:" );
              strcat( search_path, xapplresdir ); strcat( search_path, "/%l/bitmaps/%N/%B:" );
              strcat( search_path, xapplresdir ); strcat( search_path, "/bitmaps/%N/%B:" );
              strcat( search_path, xapplresdir ); strcat( search_path, "/%L/bitmaps/%B:" );
              strcat( search_path, xapplresdir ); strcat( search_path, "/%l/bitmaps/%B:" );
              strcat( search_path, xapplresdir ); strcat( search_path, "/bitmaps/%B:" );

              strcat( search_path, home ); strcat( search_path, "/bitmaps/%B:" );
              strcat( search_path, home ); strcat( search_path, "/%B:" );

              strcat( search_path, "/usr/lib/X11/%L/bitmaps/%N/%B:"
                                   "/usr/lib/X11/%l/bitmaps/%N/%B:"
                                   "/usr/lib/X11/bitmaps/%N/%B:"
                                   "/usr/lib/X11/%L/bitmaps/%B:"
                                   "/usr/lib/X11/%l/bitmaps/%B:"
                                   "/usr/lib/X11/bitmaps/%B:"
                                   "/usr/include/X11/bitmaps/%B" );

              search_path = realloc( search_path, strlen(search_path)+1 );
            }
          else
            {
              search_path = malloc( 9999 );

              search_path[0] = '\0';

              strcat( search_path, "%B:" );

              strcat( search_path, home ); strcat( search_path, "/%L/bitmaps/%N/%B:" );
              strcat( search_path, home ); strcat( search_path, "/%l/bitmaps/%N/%B:" );
              strcat( search_path, home ); strcat( search_path, "/bitmaps/%N/%B:" );
              strcat( search_path, home ); strcat( search_path, "/%L/bitmaps/%B:" );
              strcat( search_path, home ); strcat( search_path, "/%l/bitmaps/%B:" );
              strcat( search_path, home ); strcat( search_path, "/bitmaps/%B:" );
              strcat( search_path, home ); strcat( search_path, "/%B:" );

              strcat( search_path, "/usr/lib/X11/%L/bitmaps/%N/%B:"
                                   "/usr/lib/X11/%l/bitmaps/%N/%B:"
                                   "/usr/lib/X11/bitmaps/%N/%B:"
                                   "/usr/lib/X11/%L/bitmaps/%B:"
                                   "/usr/lib/X11/%l/bitmaps/%B:"
                                   "/usr/lib/X11/bitmaps/%B:"
                                   "/usr/include/X11/bitmaps/%B" );

              search_path = realloc( search_path, strlen(search_path)+1 );
            }
        }
    }

  image_path = XtResolvePathname( DisplayOfScreen(screen),
                                  "", image_name, "",
                                  search_path,
                                  substitutions, XtNumber(substitutions),
                                  NULL );

  if( image_path )
    {

      attributes.closeness = 65535;
      attributes.exactColors = False;
      attributes.valuemask = XpmExactColors | XpmCloseness;

      status = XpmReadFileToPixmap( DisplayOfScreen(screen),
                                    RootWindowOfScreen(screen),
                                    image_path,
                                    &pixmap,
                                    NULL,
                                    &attributes );

      if( status != XpmSuccess )
        pixmap = XmUNSPECIFIED_PIXMAP;

      free( image_path );
    }
  else
    {
      pixmap = XmUNSPECIFIED_PIXMAP;
    }

#else
  pixmap = XmGetPixmapByDepth( screen,
                               image_name,
                               foreground,
                               background,
                               depth);
#endif

  return( pixmap );
}
