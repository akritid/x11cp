#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include <Xm/PushB.h>
#include <Xm/MwmUtil.h>
#include <Xm/MessageB.h>
#include <Xm/Label.h>
#include <Xm/LabelG.h>
#include <Xm/List.h>
#include <Xm/Separator.h>
#include <Xm/Frame.h>
#include <Xm/Form.h>
#include <Xm/FileSB.h>
#include <Xm/RowColumn.h>
#include <Xm/DialogS.h>
#include <Xm/CascadeB.h>
#include <Xm/SelectioB.h>

#define DEFAULT_PIX     "default.xpm"

#define HORIZONTAL  0
#define VERTICAL    1

#define TOPLEFT     200
#define TOPRIGHT    201
#define BOTTOMLEFT  202
#define BOTTOMRIGHT 203
#define CUSTOM      204

#define MAXPATH  255
#define MAXPARAM 255

#define NORMAL 0
#define GROUP  1
#define SUB    2

typedef struct tool_button {
  Widget button;
  Widget rowcol;
  int type;
  char pixmap_path[MAXPATH];
  int width;
  int height;
  char app_path[MAXPATH];
  char parameters[MAXPARAM];
  char description[255];
  struct tool_button *next;
}tool_button;
    
extern Screen *screen;
extern Display *display;
extern Widget toplevel, rowcol;
extern int screen_depth;
extern int screen_width;
extern int screen_height;
extern Dimension button_x, button_y;
extern Dimension window_width, window_height;
extern int button_num;
extern int expand;
extern int orientation;
extern int position;
extern int save_on_exit;
extern int stay_on_top;
extern int quick_info;
extern int auto_hide;

extern char def_pix[255];
extern tool_button *toolbar, *start;



extern Pixmap xmtGetPixmapByDepth( Screen *screen,            /* andre */
                                   char *image_name,
                                   Pixel foreground,
                                   Pixel background,
                                   int depth);


