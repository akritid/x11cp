/**** help.c ****/

/**********************************************************************
*  Copyright (c) 1991, 1992 Iris Computing Laboratories.
*
*  This software is provided for demonstration purposes only.  As
*  freely-distributed, modifiable source code, this software carries
*  absolutely no warranty.  Iris Computing Laboratories disclaims
*  all warranties for this software, including any implied warranties
*  of merchantability and fitness, and shall not be liable for
*  damages of any type resulting from its use.
*  Permission to use, copy, modify, and distribute this source code
*  for any purpose and without fee is hereby granted, provided that
*  the above copyright and this permission notice appear in all copies
*  and supporting documentation, and provided that Iris Computing
*  Laboratories not be named in advertising or publicity pertaining
*  to the redistribution of this software without specific, written
*  prior permission.
**********************************************************************/


/*******************************************************************
Currently, there is no help object; that is, this module simply
provides functions for creating and activating the help system.
*******************************************************************/


#include "help.h"


/*
Private callbacks:
*/

static void HelpDismiss();


/*
Private globals:
*/

static Widget helpShell, helpText;
static char **help_text;


/*
Public functions:
*/

/*
help_create_dialog() creates the dialog box
used for displaying the help text.
*/

void help_create_dialog(ref_wid, app_class, rows, columns, char_set)
Widget ref_wid;
char *app_class;
int rows, columns;
XmStringCharSet char_set;
{
	Arg args[10];
	int i;
	Dimension height, margin_height;
	XmString string;
	Widget helpOuterBox, helpDismissButton, helpButtonBox, helpWindow;

	i = 0;
	XtSetArg(args[i], XmNallowShellResize, (XtArgVal) True); i++;
	XtSetArg(args[i], XmNmappedWhenManaged, (XtArgVal) False); i++;
	helpShell = XtAppCreateShell(NULL, app_class,
		topLevelShellWidgetClass, XtDisplay(ref_wid), args, i);
	i = 0;
	XtSetArg(args[i], XmNsashWidth, (XtArgVal) 1); i++;
	XtSetArg(args[i], XmNsashHeight, (XtArgVal) 1); i++;
	helpOuterBox = XtCreateManagedWidget("helpOuterBox",
		xmPanedWindowWidgetClass, helpShell, args, i);
	i = 0;
	helpWindow = XtCreateManagedWidget("helpWindow",
		xmScrolledWindowWidgetClass, helpOuterBox, args, i);
	i = 0;
	XtSetArg(args[i], XmNeditable, (XtArgVal) False); i++;
	XtSetArg(args[i], XmNeditMode, (XtArgVal) XmMULTI_LINE_EDIT); i++;
	if (rows > 0) {
		XtSetArg(args[i], XmNrows, (XtArgVal) rows); i++;
	}
	if (columns > 0) {
		XtSetArg(args[i], XmNcolumns, (XtArgVal) columns); i++;
	}
	XtSetArg(args[i], XmNvalue, (XtArgVal) " ");
	XtSetArg(args[i], XmNautoShowCursorPosition, (XtArgVal) False);
	helpText = XtCreateManagedWidget("helpText",
		xmTextWidgetClass, helpWindow, args, i);
	i = 0;
	XtSetArg(args[i], XmNadjustLast, (XtArgVal) False); i++;
	XtSetArg(args[i], XmNborderWidth, (XtArgVal) 0); i++;
	XtSetArg(args[i], XmNorientation, (XtArgVal) XmHORIZONTAL); i++;
	helpButtonBox = XtCreateManagedWidget("helpButtonBox",
		xmRowColumnWidgetClass, helpOuterBox, args, i);
	i = 0;
	string = XmStringCreateLtoR("Dismiss", char_set);
	XtSetArg(args[i], XmNlabelString, string); i++;
	helpDismissButton = XtCreateManagedWidget("helpDismissButton",
		xmPushButtonWidgetClass, helpButtonBox, args, i);
	XmStringFree(string);
	XtAddCallback(helpDismissButton,
		XmNactivateCallback, HelpDismiss, NULL);
	i = 0;
	XtSetArg(args[i], XmNmarginHeight, &margin_height); i++;
	XtGetValues(helpButtonBox, args, i);
	i = 0;
	XtSetArg(args[i], XmNheight, &height); i++;
	XtGetValues(helpDismissButton, args, i);
	i = 0;
	XtSetArg(args[i], XmNpaneMinimum,
		(XtArgVal) (height + (margin_height * 2))); i++;
	XtSetArg(args[i], XmNpaneMaximum,
		(XtArgVal) (height + (margin_height * 2))); i++;
	XtSetValues(helpButtonBox, args, i);
	remove_sash_traversal(helpOuterBox);
}	/* help_create_dialog */


/*
help_realize() does the realization.  At present,
there is no argument, since there is no "Help" object.
*/

void help_realize()
{
	XtRealizeWidget(helpShell);
}	/* help_realize */


/*
help_shell() returns the shell widget
for the help dialog.
*/

Widget help_shell()
{
	return helpShell;
}	/* help_shell */


/*
help_create_pulldown_menu() adds the "Help" pull-down
menu to the end of the menu bar.
*/

void help_create_pulldown_menu(mainMenuBar, help_menu, help_data, char_set)
Widget mainMenuBar;
menu_entry help_menu[];
char *help_data[];
XmStringCharSet char_set;
{
	Arg args[5];
	int i;
	XmString string;
	Widget menuHelpPane, menuHelpButton;

	help_text = help_data;
	menuHelpPane = XmCreatePulldownMenu(mainMenuBar, "menuHelpPane", NULL, 0);
	i = 0;
	XtSetArg(args[i], XmNsubMenuId, (XtArgVal) menuHelpPane); i++;
	string = XmStringCreateLtoR("Help", char_set);
	XtSetArg(args[i], XmNlabelString, string); i++;
	menuHelpButton = XtCreateManagedWidget("menuHelpButton",
		xmCascadeButtonWidgetClass, mainMenuBar, args, i);
	XmStringFree(string);
	i = 0;
	XtSetArg(args[i], XmNmenuHelpWidget, menuHelpButton); i++;
	XtSetValues(mainMenuBar, args, i);
	create_menus(menuHelpPane, help_menu, char_set);
}	/* help_create_pulldown_menu */


/*
Public callbacks:
*/

/*
Help() is a dispatcher for the help text.
*/
/*ARGSUSED*/
void Help(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	if (!XtIsRealized(helpShell))
		return;
	XmTextSetString(helpText, help_text[(int) client_data]);
	XtMapWidget(helpShell);
}	/* Help */


/*
Private callbacks:
*/

/*
HelpDismiss() simply removes the help window.
*/
/*ARGSUSED*/
static void HelpDismiss(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	XtUnmapWidget(helpShell);
}	/* HelpDismiss */

