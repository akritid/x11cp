/**** xrolo.c ****/

/**********************************************************************
*  Copyright (c) 1991, 1992 Iris Computing Laboratories.
*
*  This software is provided for demonstration purposes only.  As
*  freely-distributed, modifiable source code, this software carries
*  absolutely no warranty.  Iris Computing Laboratories disclaims
*  all warranties for this software, including any implied warranties
*  of merchantability and fitness, and shall not be liable for
*  damages of any type resulting from its use.
*  Permission to use, copy, modify, and distribute this source code
*  for any purpose and without fee is hereby granted, provided that
*  the above copyright and this permission notice appear in all copies
*  and supporting documentation, and provided that Iris Computing
*  Laboratories not be named in advertising or publicity pertaining
*  to the redistribution of this software without specific, written
*  prior permission.
**********************************************************************/


/********************************************************************
`xrolodex' implements a simple rolodex application.  It is composed
of the following modules:
	`xrolo.c'		-- widget creation for the top-level interface,
					   plus callbacks and support functions.
	`xrolo.db.c'	-- rolodex entry manipulation functions and
					   the state of the database.
	`xrolo.help.t'	-- a module for providing help system text.
	`xrolo.index.c'	-- the rolodex index manipulation functions.

	`streamdb.c'	-- a stream- or byte-oriented database object.
	`strstr.c'		-- a string search module.
	`ctrlpanel.c'	-- a control panel pseudo-widget.
	`editor.c'		-- a self-contained editor pseudo-widget.
	`listshell.c'	-- a list selection box pseudo-widget.
	`dialog.c'		-- a dialog box pseudo-widget.
	`help.c'		-- a generalized help system.
	`motif.c'		-- OSF/Motif miscellaneous functions.

See these modules for more details.

Note that this implementation contains source code specifically
designed to provide conversation, sometimes debate, as well as
examples that motivate discussion of certain techniques in both
lecture and laboratory settings.  For example, we use one
dialog box, `messageDialog' to display different messages
(efficient), but in another case, we use two dialog boxes in a
nonmodal situation simply to distinguish between the context in
which their callbacks are activated (not as efficient).
********************************************************************/

#include <stdlib.h>

#include "xrolo.h"


/*
Private functions:
*/

static Widget create_main_menu_bar();
static CtrlPanel create_entry_control_panel();
static void create_dialogs();
static int check_for_unsaved_changes();
static void load_next_rolodex_entry();
static void load_previous_rolodex_entry();
static void new_entry();
static int save_current_text();
static void insert_new_db_entry();
static void save_current_db_entry();
static void find_entry_containing_text_forward();
static void find_entry_containing_text_reverse();
static void open_rolodex_file(), save_as_rolodex_file();
static void do_save_as_operation();
static void save_first_time();
static void update_current_entry();
static int is_rolodex_active();
static void initialize_critical_variables();
static void cleanup_and_exit();
static void user_message();
static void initialize_shell_icons();
static void handle_close_buttons();



/*
Private callbacks:
*/

static void Open(), Save(), SaveAs(), Quit(), Exit();
static void Next(), Previous(), First(), Last();
static void Index(), Find(), Sort();
static void NewIns(), NewAdd(), Copy(), Delete(), Undelete();
static void EntryModified(), IndexSelection();
static void InsensitiveSearch(), SensitiveSearch();
static void FileSelectionOK(), FileSelectionCancel();
static void OverwriteOK(), OverwriteCancel();
static void SaveUnsavedChanges(), DiscardUnsavedChanges(),
	CancelUnsavedChanges();
static void FindForward(), FindReverse(), FindDismiss();
static void Close(), UnmanageWindow(), UnmapWindow(), DialogCancel();


/*
Private globals (can be modified):
*/

static char *delimiter;					 /* see resources */
static char *save_as_filename = NULL;
static Pixmap icon_pixmap;


/*
Private globals (global for callback convenience only -- never modified):
*/

static CtrlPanel entryPanel;
static Editor entryEditWindow;
static ListShell entryIndex;
static Dialog openUnsavedDialog, quitUnsavedDialog, overwriteDialog,
	messageDialog;
static Widget databaseLabel, fileSelectionDialog;
static Widget findShell, findText;


/*
Private globals (these variables maintain the state of rolodex entries):
*/

static int editing_new_entry = FALSE;
static EntryDB current_entry = NULL; /* opaque usage as indicator variable */
static EntryDB temp_entry = NULL;    /* opaque usage as indicator variable */
static int entry_modified = FALSE;
static int first_time = TRUE;
static int case_sensitive_search = FALSE;


/*
Private character set, passed to other modules:
*/

static XmStringCharSet char_set = (XmStringCharSet) XmSTRING_DEFAULT_CHARSET;


/*
Private resource globals (set during start-up):
*/

static XtResource resources[] = {
	{XtNviewportMenuBar, XtCViewportMenuBar, XtRBoolean, sizeof(Boolean),
		XtOffset(ApplicationDataPtr, viewport_menubar), XtRImmediate,
		(XtPointer) FALSE},
	{XtNviewportRows, XtCViewportRows, XtRInt, sizeof(int),
		XtOffset(ApplicationDataPtr, viewport_rows), XtRImmediate,
		(XtPointer) xrolo_DEFAULT_VIEWPORT_ROWS},
	{XtNviewportColumns, XtCViewportColumns, XtRInt, sizeof(int),
		XtOffset(ApplicationDataPtr, viewport_columns), XtRImmediate,
		(XtPointer) xrolo_DEFAULT_VIEWPORT_COLUMNS},
	{XtNindexRows, XtCIndexRows, XtRInt, sizeof(int),
		XtOffset(ApplicationDataPtr, index_rows), XtRImmediate,
		(XtPointer) xrolo_DEFAULT_INDEX_ROWS},
	{XtNindexColumns, XtCIndexColumns, XtRInt, sizeof(int),
		XtOffset(ApplicationDataPtr, index_columns), XtRImmediate,
		(XtPointer) xrolo_DEFAULT_INDEX_COLUMNS},
	{XtNcenterDialogs, XtCCenterDialogs, XtRBoolean, sizeof(Boolean),
		XtOffset(ApplicationDataPtr, center_dialogs), XtRImmediate,
		(XtPointer) TRUE},
	{XtNentryDelimiter, XtCEntryDelimiter, XtRString, sizeof(String),
		XtOffset(ApplicationDataPtr, delimiter),
		XtRString, xrolo_DEFAULT_DELIMITER},
	{XtNdirectoryMask, XtCDirectoryMask, XtRString, sizeof(String),
		XtOffset(ApplicationDataPtr, dir_mask),
		XtRString, ""},	/* default is home directory; see create_dialogs() */
};


/*
main() creates a top-level window from a system of manager
widgets, using several pseudo-widgets for the control panel,
the edit area for rolodex entries, and the index facility.
*/

void main(argc, argv)
int argc;
char *argv[];
{
	Arg args[10];
	int i;
	XtAppContext app;
	Widget topLevel, mainWindow, mainMenuBar;
	static XtCallbackRec text_mod_cb_list[] = {
		{EntryModified, (XtPointer) NULL},
		{(XtCallbackProc) NULL, (XtPointer) NULL},
	};
	ApplicationData resource_data;
	char *icon_name;

	if ((argc == 2 && strcmp(argv[1], "-help") == 0)) {
		printf("\nusage:  xrolodex [<filename>] [-iconName <icon name>]\n\n");
		exit(0);
	}
	topLevel = XtAppInitialize(&app, xrolo_APP_CLASS,
		(XrmOptionDescList) NULL, 0,
		&argc, argv, (String *) NULL, (ArgList) NULL, 0);
	if (argc > 4) {
		printf("\nusage:  xrolodex [<filename>] [-iconName <icon name>]\n");
		exit(0);
	}
	XtGetApplicationResources(topLevel, &resource_data, resources,
		XtNumber(resources), NULL, 0);
	delimiter = resource_data.delimiter;	/* set the global */
	i = 0;
	XtSetArg(args[i], XmNhorizontalSpacing, (XtArgVal) 5); i++;
	XtSetArg(args[i], XmNverticalSpacing, (XtArgVal) 5); i++;
	mainWindow = XtCreateManagedWidget("mainWindow",
		xmFormWidgetClass, topLevel, args, i);

	mainMenuBar = create_main_menu_bar(mainWindow);

	i = 0;
	XtSetArg(args[i], XmNleftAttachment, (XtArgVal) XmATTACH_FORM); i++;
	XtSetArg(args[i], XmNrightAttachment, (XtArgVal) XmATTACH_FORM); i++;
	XtSetArg(args[i], XmNtopAttachment, (XtArgVal) XmATTACH_WIDGET); i++;
	XtSetArg(args[i], XmNtopWidget, (XtArgVal) mainMenuBar); i++;
	XtSetArg(args[i], XmNlabelString,
		XmStringCreateLtoR("  ", char_set)); i++;
	databaseLabel = XtCreateManagedWidget("databaseLabel",
		xmLabelWidgetClass, mainWindow, args, i);

	entryPanel = create_entry_control_panel(mainWindow);
	i = 0;
	XtSetArg(args[i], XmNleftAttachment, (XtArgVal) XmATTACH_FORM); i++;
	XtSetArg(args[i], XmNtopAttachment, (XtArgVal) XmATTACH_WIDGET); i++;
	XtSetArg(args[i], XmNtopWidget, (XtArgVal) databaseLabel); i++;
	XtSetArg(args[i], XmNbottomAttachment, (XtArgVal) XmATTACH_FORM); i++;
	XtSetValues(ctrlPanel_instance(entryPanel), args, i);

	entryEditWindow =
		editor_create(mainWindow, xrolo_APP_CLASS, "entryEditWindow",
		resource_data.viewport_rows, resource_data.viewport_columns, 40,
		editor_SCROLL_DEFAULT, "", char_set, "xrolodex",
		"Editor/Viewport Search and Replace", editor_WIN_MGR_DECOR,
		resource_data.viewport_menubar ? editor_MENU_BAR : editor_POPUP_MENU);
	i = 0;
	XtSetArg(args[i], XmNleftAttachment, (XtArgVal) XmATTACH_WIDGET); i++;
	XtSetArg(args[i], XmNleftWidget, (XtArgVal) ctrlPanel_instance(entryPanel)); i++;
	XtSetArg(args[i], XmNrightAttachment, (XtArgVal) XmATTACH_FORM); i++;
	XtSetArg(args[i], XmNtopAttachment, (XtArgVal) XmATTACH_WIDGET); i++;
	XtSetArg(args[i], XmNtopWidget, (XtArgVal) databaseLabel); i++;
	XtSetArg(args[i], XmNbottomAttachment, (XtArgVal) XmATTACH_FORM); i++;
	XtSetValues(editor_instance(entryEditWindow), args, i);
	i = 0;
	XtSetArg(args[i], XmNmodifyVerifyCallback, text_mod_cb_list); i++;
	XtSetValues(editor_edit(entryEditWindow), args, i);

	/*
	the list selection box's items will be added later:
	*/
	entryIndex = listShell_create(topLevel, xrolo_APP_CLASS, "entryIndex",
		NULL, char_set,
		resource_data.index_columns, resource_data.index_rows,
		listShell_NO_STAY_UP, listShell_WIN_MGR_DECOR);
	listShell_add_callback(entryIndex, IndexSelection, 0);
	xrolo_index_create(entryIndex);

	create_dialogs(app, topLevel, &resource_data);
	help_create_dialog(topLevel, xrolo_APP_CLASS,
		xrolo_help_rows, xrolo_help_columns, char_set);

	if (argc == 4 && strcmp(argv[2], "-iconName") == 0)
		icon_name = argv[3];
	else if (argc == 3 && strcmp(argv[1], "-iconName") == 0)
		icon_name = argv[2];
	else
		icon_name = NULL;
	initialize_shell_icons(topLevel, icon_name);
	XtRealizeWidget(topLevel);
	XtRealizeWidget(findShell);
	help_realize();
	editor_realize(entryEditWindow);
	listShell_realize(entryIndex);
	dialog_realize(openUnsavedDialog);
	dialog_realize(quitUnsavedDialog);
	dialog_realize(overwriteDialog);
	dialog_realize(messageDialog);
	handle_close_buttons(topLevel);
	if (argc > 1 && strcmp(argv[1], "-iconName") != 0 && strlen(argv[1]) > 0)
		open_rolodex_file(argv[1]);
#ifdef SCREEN_SHOT
	while (True) {
		XEvent event;

		XtAppNextEvent(app, &event);
		if (event.type == ButtonPress && event.xbutton.button == 2) {
			XUngrabPointer(XtDisplay(topLevel), CurrentTime);
			XUngrabKeyboard(XtDisplay(topLevel), CurrentTime);
		}
		XtDispatchEvent(&event);
	}
#else
	XtAppMainLoop(app);
#endif
}	/* main */


/*
create_main_menu_bar() creates the application-level menu bar,
including pull-down menus and menu entries.
*/

static Widget create_main_menu_bar(parent)
Widget parent;
{
	static menu_entry file_menu[] = {
		{menu_ENTRY, "Open...", "menuOpenButton", Open, NULL, NULL, NULL},
		{menu_ENTRY, "Save", "menuSaveButton", Save, NULL, NULL, NULL},
		{menu_ENTRY, "Save As...", "menuSaveAsButton", SaveAs, NULL,
			NULL, NULL},
		{menu_ENTRY, "Quit", "menuQuitButton", Quit, NULL, NULL, NULL},
		{menu_ENTRY, "Exit", "menuExitButton", Exit, NULL, NULL, NULL},
		{menu_END, NULL, NULL, NULL, NULL, NULL, NULL},
	};
	static menu_entry edit_menu[] = {
		{menu_ENTRY, "New/Ins", "menuInsButton", NewIns, NULL, NULL, NULL},
		{menu_ENTRY, "New/Add", "menuAddButton", NewAdd, NULL, NULL, NULL},
		{menu_ENTRY, "Copy", "menuCopyButton", Copy, NULL, NULL, NULL},
		{menu_ENTRY, "Delete", "menuDeleteButton", Delete, NULL,
			NULL, NULL},
		{menu_ENTRY, "Undelete", "menuUndeleteButton", Undelete, NULL,
			NULL, NULL},
		{menu_END, NULL, NULL, NULL, NULL, NULL, NULL},
	};
	static menu_entry find_menu[] = {
		{menu_ENTRY, "Find Entry...", "menuFindEntryButton", Find, NULL,
			NULL, NULL},
		{menu_ENTRY, "Index...", "menuIndexButton", Index, NULL,
			NULL, NULL},
		{menu_END, NULL, NULL, NULL, NULL, NULL, NULL},
	};
	static menu_entry sort_menu[] = {
		{menu_ENTRY, "Ascending", "menuAscendingButton", Sort,
			(XtPointer) xrolo_ASCEND, NULL, NULL},
		{menu_ENTRY, "Descending", "menuDescendingButton", Sort,
			(XtPointer) xrolo_DESCEND, NULL, NULL},
		{menu_END, NULL, NULL, NULL, NULL, NULL, NULL},
	};
	static menu_entry menus[] = {
		{menu_SUBMENU, "File", "menuFileButton", NULL, NULL, file_menu, NULL},
		{menu_SUBMENU, "Edit", "menuEditButton", NULL, NULL, edit_menu, NULL},
		{menu_SUBMENU, "Find", "menuFindButton", NULL, NULL, find_menu, NULL},
		{menu_SUBMENU, "Sort", "menuSortButton", NULL, NULL, sort_menu, NULL},
		{menu_END, NULL, NULL, NULL, NULL, NULL, NULL},
	};
	static menu_entry help_menu[] = {
		{menu_ENTRY, "General...", "menuGenHelpButton",
			Help, (XtPointer) 0, NULL, NULL},
		{menu_ENTRY, "Files...", "menuFileHelpButton",
			Help, (XtPointer) 1, NULL, NULL},
		{menu_ENTRY, "Edit Entries...", "menuEditHelpButton",
			Help, (XtPointer) 2, NULL, NULL},
		{menu_ENTRY, "Find Entries...", "menuFindHelpButton",
			Help, (XtPointer) 3, NULL, NULL},
		{menu_ENTRY, "Sort Entries...", "menuSortHelpButton",
			Help, (XtPointer) 4, NULL, NULL},
		{menu_ENTRY, "Control Panel...", "menuPanelHelpButton",
			Help, (XtPointer) 5, NULL, NULL},
		{menu_ENTRY, "Editor...", "menuEditorHelpButton",
			Help, (XtPointer) 6, NULL, NULL},
		{menu_ENTRY, "Info...", "menuInfoHelpButton",
			Help, (XtPointer) 7, NULL, NULL},
		{menu_END, NULL, NULL, NULL, NULL, NULL, NULL},
	};
	Arg args[10];
	int i;
	Widget mainMenuBar;

	i = 0;
	XtSetArg(args[i], XmNleftAttachment, (XtArgVal) XmATTACH_FORM); i++;
	XtSetArg(args[i], XmNrightAttachment, (XtArgVal) XmATTACH_FORM); i++;
	XtSetArg(args[i], XmNtopAttachment, (XtArgVal) XmATTACH_FORM); i++;
	mainMenuBar = XmCreateMenuBar(parent, "mainMenuBar", args, i);
	XtManageChild(mainMenuBar);
	create_menus(mainMenuBar, menus, char_set);
	help_create_pulldown_menu(mainMenuBar, help_menu, xrolo_help_data, char_set);
	return mainMenuBar;
}	/* create_main_menu_bar */


/*
create_entry_control_panel() creates the control panel that's
used to manipulate rolodex entries in the rolodex window.
*/

static CtrlPanel create_entry_control_panel(parent)
Widget parent;
{
	static CtrlPanelItem items[] = {
		{"First", NULL, First, NULL},
		{"Last", NULL, Last, NULL},
		{"", NULL, NULL, NULL},				/* separator */
		{"Previous", NULL, Previous, NULL},
		{"Next", NULL, Next, NULL},
		{"", NULL, NULL, NULL},				/* separator */
		{"New/Ins", NULL, NewIns, NULL},
		{"Copy", NULL, Copy, NULL},
		{"", NULL, NULL, NULL},				/* separator */
		{"Index...", NULL, Index, NULL},
		{NULL, NULL, NULL, NULL},
	};
	CtrlPanel entryControlPanel;

	entryControlPanel = ctrlPanel_create(parent, "entryControlPanel",
		items, char_set, ctrlPanel_VERTICAL);
	return entryControlPanel;
}	/* create_entry_control_panel */


/*
create_dialogs() creates the dialog boxes used for prompting
for filenames, for prompts related to unsaved changes to
the rolodex file, and the find-entry window.
*/

static void create_dialogs(app, parent, resource_data)
XtAppContext app;
Widget parent;
ApplicationData *resource_data;
{
	static DialogButtonItem quit_unsaved_items[] = {
		{"Save", SaveUnsavedChanges, (XtPointer) xrolo_QUIT_WARNING},
		{"Discard", DiscardUnsavedChanges, (XtPointer) xrolo_QUIT_WARNING},
		{"Cancel", CancelUnsavedChanges, (XtPointer) xrolo_QUIT_WARNING},
		{NULL, NULL, NULL},
	};
	static DialogButtonItem open_warning_items[] = {
		{"Save", SaveUnsavedChanges, (XtPointer) xrolo_OPEN_WARNING},
		{"Discard", DiscardUnsavedChanges, (XtPointer) xrolo_OPEN_WARNING},
		{"Cancel", CancelUnsavedChanges, (XtPointer) xrolo_OPEN_WARNING},
		{NULL, NULL, NULL},
	};
	static DialogButtonItem overwrite_items[] = {
		{"Overwrite", OverwriteOK, NULL},
		{"Cancel", OverwriteCancel, NULL},
		{NULL, NULL, NULL},
	};
	static DialogButtonItem message_items[] = {
		{"  OK  ", NULL, NULL},
		{NULL, NULL, NULL},
	};
	static menu_entry toggle_menu[] = {
		{menu_TOG_ENTRY, "Case insensitive", "findInsensitiveButton",
			InsensitiveSearch, NULL, NULL, NULL},
		{menu_TOG_ENTRY, "Case sensitive", "findSensitiveButton",
			SensitiveSearch, NULL, NULL, NULL},
		{menu_END, NULL, NULL, NULL, NULL, NULL, NULL},
	};
	char dir_mask[xrolo_DIR_MASK_MAX_LEN + 2];
	char *home;
	Arg args[10];
	int i;
	Dimension height, margin_height;
	Widget findPane, findBox, findLabel, findButtonBox,
		findForwardButton, findReverseButton, findDismissButton,
		findRadioBox;


	if (*resource_data->dir_mask)
		strncpy(dir_mask, resource_data->dir_mask, xrolo_DIR_MASK_MAX_LEN);
	else if ((home = (char *) getenv("HOME")) == NULL)
		strcpy(dir_mask, xrolo_DIR_MASK);
	else {
		strncpy(dir_mask, home, xrolo_DIR_MASK_MAX_LEN);
		strncat(dir_mask, "/", xrolo_DIR_MASK_MAX_LEN - strlen(home));
		//strncat(dir_mask, xrolo_DIR_MASK, xrolo_DIR_MASK_MAX_LEN - strlen(home) - 1);
	}
	i = 0;
	XtSetArg(args[i], XmNdialogTitle,
		XmStringCreateLtoR("xrolodex", char_set)); i++;
	XtSetArg(args[i], XmNdirMask,
		XmStringCreateLtoR(dir_mask, char_set)); i++;
	fileSelectionDialog = (Widget) XmCreateFileSelectionDialog(parent,
		"fileSelectionDialog", args, i);
	//XtAddCallback(fileSelectionDialog, XmNokCallback,
	//	FileSelectionOK, NULL);
	//XtAddCallback(fileSelectionDialog, XmNcancelCallback,
	//	FileSelectionCancel, NULL);
	//XtUnmanageChild((Widget) XmFileSelectionBoxGetChild(fileSelectionDialog,
	//	XmDIALOG_HELP_BUTTON));

/***********************************************************************
	dialog_create(app, parent, instance_name, items, num_columns,
		title, prompt, char_set, dialog_position, modal_dialog,
		max_win_mgr, string_input, auto_popdown, default_button);
***********************************************************************/
	quitUnsavedDialog = dialog_create(app, parent, "quitUnsavedDialog",
		quit_unsaved_items, 0, "xrolodex", xrolo_QUIT_WARNING_MSG, char_set,
		(resource_data->center_dialogs) ? dialog_CENTER : dialog_DEFAULT,
		FALSE, FALSE, FALSE, FALSE, 0);
	openUnsavedDialog = dialog_create(app, parent, "openUnsavedDialog",
		open_warning_items, 0, "xrolodex", xrolo_OPEN_WARNING_MSG, char_set,
		(resource_data->center_dialogs) ? dialog_CENTER : dialog_DEFAULT,
		FALSE, FALSE, FALSE, FALSE, 0);
	overwriteDialog = dialog_create(app, parent, "overwriteDialog",
		overwrite_items, 0, "xrolodex", "File already exists!", char_set,
		(resource_data->center_dialogs) ? dialog_CENTER : dialog_DEFAULT,
		FALSE, FALSE, FALSE, TRUE, 0);
	messageDialog = dialog_create(app, parent, "messageDialog",
		message_items, 0, "xrolodex", "", char_set,
		(resource_data->center_dialogs) ? dialog_CENTER : dialog_DEFAULT,
		FALSE, FALSE, FALSE, TRUE, 0);

	i = 0;
	XtSetArg(args[i], XmNallowShellResize, (XtArgVal) True); i++;
	XtSetArg(args[i], XmNmappedWhenManaged, (XtArgVal) False); i++;
	findShell = XtAppCreateShell(NULL, xrolo_APP_CLASS,
		topLevelShellWidgetClass, XtDisplay(parent), args, i);
	i = 0;
	XtSetArg(args[i], XmNsashWidth, (XtArgVal) 1); i++;
	XtSetArg(args[i], XmNsashHeight, (XtArgVal) 1); i++;
	findPane = XtCreateManagedWidget("findPane",
		xmPanedWindowWidgetClass, findShell, args, i);
	i = 0;
	XtSetArg(args[i], XmNhorizontalSpacing, (XtArgVal) 5); i++;
	XtSetArg(args[i], XmNverticalSpacing, (XtArgVal) 5); i++;
	findBox = XtCreateManagedWidget("findBox",
		xmFormWidgetClass, findPane, args, i);
	i = 0;
	XtSetArg(args[i], XmNleftAttachment, (XtArgVal) XmATTACH_FORM); i++;
	XtSetArg(args[i], XmNrightAttachment, (XtArgVal) XmATTACH_FORM); i++;
	XtSetArg(args[i], XmNtopAttachment, (XtArgVal) XmATTACH_FORM); i++;
	XtSetArg(args[i], XmNlabelString,
		XmStringCreateLtoR("Search for the next entry containing:",
			char_set)); i++;
	XtSetArg(args[i], XmNalignment, (XtArgVal) XmALIGNMENT_CENTER); i++;
	findLabel = XtCreateManagedWidget("findLabel",
		xmLabelWidgetClass, findBox, args, i);
	i = 0;
	XtSetArg(args[i], XmNleftAttachment, (XtArgVal) XmATTACH_FORM); i++;
	XtSetArg(args[i], XmNrightAttachment, (XtArgVal) XmATTACH_FORM); i++;
	XtSetArg(args[i], XmNtopAttachment, (XtArgVal) XmATTACH_WIDGET); i++;
	XtSetArg(args[i], XmNtopWidget, (XtArgVal) findLabel); i++;
	XtSetArg(args[i], XmNcolumns, (XtArgVal) 40); i++;
	findText = XtCreateManagedWidget("findText",
		xmTextWidgetClass, findBox, args, i);
	XtAddCallback(findText, XmNactivateCallback, FindForward, NULL);
	i = 0;
	XtSetArg(args[i], XmNleftAttachment, (XtArgVal) XmATTACH_FORM); i++;
	XtSetArg(args[i], XmNrightAttachment, (XtArgVal) XmATTACH_FORM); i++;
	XtSetArg(args[i], XmNtopAttachment, (XtArgVal) XmATTACH_WIDGET); i++;
	XtSetArg(args[i], XmNtopWidget, (XtArgVal) findText); i++;
	XtSetArg(args[i], XmNborderWidth, (XtArgVal) 0); i++;
	XtSetArg(args[i], XmNorientation, (XtArgVal) XmHORIZONTAL); i++;
	XtSetArg(args[i], XmNradioBehavior, (XtArgVal) True); i++;
	XtSetArg(args[i], XmNradioAlwaysOne, (XtArgVal) True); i++;
	findRadioBox = XtCreateManagedWidget("findRadioBox",
		xmRowColumnWidgetClass, findBox, args, i);
	create_menus(findRadioBox, toggle_menu, char_set);
	i = 0;
	XtSetArg(args[i], XmNset, (XtArgVal) True); i++;
	XtSetValues(toggle_menu[0].id, args, i);
	i = 0;
	XtSetArg(args[i], XmNborderWidth, (XtArgVal) 0); i++;
	XtSetArg(args[i], XmNorientation, (XtArgVal) XmHORIZONTAL); i++;
	findButtonBox = XtCreateManagedWidget("findButtonBox",
		xmRowColumnWidgetClass, findPane, args, i);
	i = 0;
	XtSetArg(args[i], XmNlabelString,
		XmStringCreateLtoR("Forward", char_set)); i++;
	findForwardButton = XtCreateManagedWidget("findForwardButton",
		xmPushButtonWidgetClass, findButtonBox, args, i);
	XtAddCallback(findForwardButton,
		XmNactivateCallback, FindForward, NULL);
	i = 0;
	XtSetArg(args[i], XmNlabelString,
		XmStringCreateLtoR("Reverse", char_set)); i++;
	findReverseButton = XtCreateManagedWidget("findReverseButton",
		xmPushButtonWidgetClass, findButtonBox, args, i);
	XtAddCallback(findReverseButton,
		XmNactivateCallback, FindReverse, NULL);
	i = 0;
	XtSetArg(args[i], XmNlabelString,
		XmStringCreateLtoR("Dismiss", char_set)); i++;
	findDismissButton = XtCreateManagedWidget("findDismissButton",
		xmPushButtonWidgetClass, findButtonBox, args, i);
	XtAddCallback(findDismissButton,
		XmNactivateCallback, FindDismiss, NULL);
	i = 0;
	XtSetArg(args[i], XmNmarginHeight, &margin_height); i++;
	XtGetValues(findButtonBox, args, i);
	i = 0;
	XtSetArg(args[i], XmNheight, &height); i++;
	XtGetValues(findDismissButton, args, i);
	i = 0;
	XtSetArg(args[i], XmNpaneMinimum,
		(XtArgVal) (height + (margin_height * 2))); i++;
	XtSetArg(args[i], XmNpaneMaximum,
		(XtArgVal) (height + (margin_height * 2))); i++;
	XtSetValues(findButtonBox, args, i);
	remove_sash_traversal(findPane);
}	/* create_dialogs */


/*
Private callback functions:
*/

/*
Close() terminates the application after freeing dynamic
resources and processing unsaved changes.
*/
/*ARGSUSED*/
static void Close(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	if (check_for_unsaved_changes(quitUnsavedDialog))
		return;
	cleanup_and_exit((Widget) client_data);
}	/* Close */


/*
UnmapWindow() provides "Close" button functionality for pop-ups.
*/
/*ARGSUSED*/
static void UnmapWindow(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	XtUnmapWidget((Widget) client_data);
}	/* UnmapWindow */


/*
UnmanageWindow() provides "Close" button functionality for Motif dialogs.
*/
/*ARGSUSED*/
static void UnmanageWindow(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	XtUnmanageChild((Widget) client_data);
}	/* UnmanageWindow */


/*
DialogCancel() provides "Close" button functionality for
`Dialog' dialogs.  Because `xrolodex' does NOT interpret
data associated with any of its dialogs' buttons, it
doesn't matter which button we interpret the "Close"
operation as being equivalent to; thus, the 0th button
is used; see dialog_cancel() in `dialog.c'.
*/
/*ARGSUSED*/
static void DialogCancel(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	dialog_cancel((Dialog) client_data, 0);
}	/* DialogCancel */


/*
Quit() terminates the application after freeing dynamic
resources.  It first checks with the user via a dialog box.
*/
/*ARGSUSED*/
static void Quit(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	if (check_for_unsaved_changes(quitUnsavedDialog))
		return;
	cleanup_and_exit(w);
}	/* Quit */


/*
Exit() terminates the application after saving changes.
*/
/*ARGSUSED*/
static void Exit(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	if (!is_rolodex_active())
		return;
	save_first_time();
	save_current_text();
	xrolo_db_save();
	cleanup_and_exit(w);
}	/* Exit */


/*
Open() prompts regarding unsaved changes and then
activates the file selection box for loading a rolodex
file.  FileSelectionOK() manages the actual open/load
operation for the rolodex file.
*/
/*ARGSUSED*/
static void Open(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	Arg arg;
	static XtCallbackRec open_cb_list[] = {
		{FileSelectionOK, (XtPointer) xrolo_OPEN},	/* ptr warning */
		{(XtCallbackProc) NULL, (XtPointer) NULL},
	};

	if (check_for_unsaved_changes(openUnsavedDialog))
		return;
	XtSetArg(arg, XmNokCallback, (XtArgVal) open_cb_list);
	//XtSetValues(fileSelectionDialog, &arg, 1);
	//XtManageChild(fileSelectionDialog);
}	/* Open */


/*
Save() saves changes to the currently open rolodex database.
*/
/*ARGSUSED*/
static void Save(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	if (!is_rolodex_active())
		return;
	save_first_time();
	save_current_text();
	xrolo_db_save();
}	/* Save */


/*
SaveAs() activates the file selection box for specifying
a rolodex file.  FileSelectionOK() manages the actual save
operation for the rolodex file.
*/
/*ARGSUSED*/
static void SaveAs(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	Arg arg;
	static XtCallbackRec save_as_cb_list[] = {
		{FileSelectionOK, (XtPointer) xrolo_SAVE_AS},	/* ptr warning */
		{(XtCallbackProc) NULL, (XtPointer) NULL},
	};

	if (!is_rolodex_active())
		return;
	XtSetArg(arg, XmNokCallback, (XtArgVal) save_as_cb_list);
	//XtSetValues(fileSelectionDialog, &arg, 1);
	//XtManageChild(fileSelectionDialog);
}	/* SaveAs */


/*
EntryModified() signals modifications to the rolodex entry viewport.
It is registered with the editor pseudo-widget's modify callback.
*/
/*ARGSUSED*/
static void EntryModified(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	if (!is_rolodex_active())
		return;
	entry_modified = TRUE;
}	/* EntryModified */


/*
InsensitiveSearch() signals modifications to the rolodex entry
search case sensitivity.  It is registered with the toggle
button's value-changed callback.
*/
/*ARGSUSED*/
static void InsensitiveSearch(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	case_sensitive_search = FALSE;
}	/* InsensitiveSearch */


/*
SensitiveSearch() signals modifications to the rolodex entry
search case sensitivity.  It is registered with the toggle
button's value-changed callback.
*/
/*ARGSUSED*/
static void SensitiveSearch(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	case_sensitive_search = TRUE;
}	/* SensitiveSearch */


/*
FileSelectionOK() responds to a user's file selection.  It
must handle rolodex file "Open" and "SaveAs" operations.
*/
/*ARGSUSED*/
static void FileSelectionOK(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	Arg args[5];
	int i;
	char *filename;
	XmString xmstring;

	i = 0;
	XtSetArg(args[i], XmNtextString, &xmstring); i++;
	//XtGetValues(fileSelectionDialog, args, i);
	if (!XmStringGetLtoR(xmstring, char_set, &filename))
		return;
	//XtUnmanageChild(fileSelectionDialog);
	if ((int) client_data == xrolo_OPEN)
		open_rolodex_file(filename);
	else if ((int) client_data == xrolo_SAVE_AS) {
		/*
		save_as_rolodex_file() *may* activate a nonmodal dialog box
		and return with it still pending, so create a copy of the local
		filename before freeing it.  Since callbacks are involved, there
		is no point in passing it as arguments to procedures and setting
		(and resetting) callback client-data values:
		*/
		if (save_as_filename)
/*			free(save_as_filename);*/
			XtFree(save_as_filename);
		if ((save_as_filename =
/*		(char *) malloc((unsigned) (strlen(filename) + 1))) != NULL) {
			strcpy(save_as_filename, filename);*/
		(char *) XtNewString((String) filename)) != NULL) {
			save_as_rolodex_file();
		}
	}
	XmStringFree(xmstring);
	XtFree(filename);
}	/* FileSelectionOK */


/*
FileSelectionCancel() simply unmaps the file selection box.
*/
/*ARGSUSED*/
static void FileSelectionCancel(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	//XtUnmanageChild(fileSelectionDialog);
}	/* FileSelectionCancel */


/*
SaveUnsavedChanges() handles the appropriate action if a user
chooses the "OK" button, depending on application context.
*/
/*ARGSUSED*/
static void SaveUnsavedChanges(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	if (!is_rolodex_active())
		return;
	save_first_time();
	save_current_text();
	xrolo_db_save();
	if ((int) client_data == xrolo_QUIT_WARNING) {
		dialog_deactivate(quitUnsavedDialog);
		cleanup_and_exit(w);
	}
	else if ((int) client_data == xrolo_OPEN_WARNING) {
		dialog_deactivate(openUnsavedDialog);
		//XtManageChild(fileSelectionDialog);
	}
}	/* SaveUnsavedChanges */


/*
DiscardUnsavedChanges() handles the appropriate action if a user
chooses the "Discard" button, depending on application context.
*/
/*ARGSUSED*/
static void DiscardUnsavedChanges(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	if ((int) client_data == xrolo_QUIT_WARNING) {
		dialog_deactivate(quitUnsavedDialog);
		cleanup_and_exit(w);
	}
	else if ((int) client_data == xrolo_OPEN_WARNING) {
		dialog_deactivate(openUnsavedDialog);
		//XtManageChild(fileSelectionDialog);
	}
}	/* DiscardUnsavedChanges */


/*
CancelUnsavedChanges() handles the appropriate action if a user
chooses the "Cancel" button, depending on application context.
*/
/*ARGSUSED*/
static void CancelUnsavedChanges(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	if ((int) client_data == xrolo_QUIT_WARNING)
		dialog_deactivate(quitUnsavedDialog);
	else if ((int) client_data == xrolo_OPEN_WARNING)
		dialog_deactivate(openUnsavedDialog);
}	/* CancelUnsavedChanges */


/*
OverwriteOK() handles the appropriate action if a user
chooses the "OK" button during a "Save As" operation
that leads to the file-overwrite warning.
*/
/*ARGSUSED*/
static void OverwriteOK(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	do_save_as_operation(save_as_filename);
}	/* OverwriteOK */


/*
OverwriteCancel() handles the appropriate action if a user
chooses the "Cancel" button during a "Save As" operation
that leads to the file-overwrite warning.
*/
/*ARGSUSED*/
static void OverwriteCancel(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	/* currently, nothing */
}	/* OverwriteCancel */


/*
FindForward() handles the search in the forward direction for an
entry containing the specified text.
*/
/*ARGSUSED*/
static void FindForward(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	if (!is_rolodex_active())
		return;
	find_entry_containing_text_forward();
}	/* FindForward */


/*
FindReverse() handles the search in the reverse direction for an
entry containing the specified text.
*/
/*ARGSUSED*/
static void FindReverse(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	if (!is_rolodex_active())
		return;
	find_entry_containing_text_reverse();
}	/* FindReverse */


/*
FindDismiss() simply removes the "Find" dialog.
*/
/*ARGSUSED*/
static void FindDismiss(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	XtUnmapWidget(findShell);
}	/* FindDismiss */


/*
First() advances to the first entry in the rolodex database.
*/
/*ARGSUSED*/
static void First(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	if (!is_rolodex_active())
		return;
	save_current_text();
	if (xrolo_db_current_entry_is_first_entry())
		return;
	if ((temp_entry = xrolo_db_first_entry()) != NULL)
		update_current_entry(temp_entry);
}	/* First */


/*
Last() advances to the last entry in the rolodex database.
*/
/*ARGSUSED*/
static void Last(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	if (!is_rolodex_active())
		return;
	save_current_text();
	if (xrolo_db_current_entry_is_last_entry())
		return;
	if ((temp_entry = xrolo_db_last_entry()) != NULL)
		update_current_entry(temp_entry);
}	/* Last */


/*
Next() advances to the next entry in the rolodex database.
*/
/*ARGSUSED*/
static void Next(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	if (!is_rolodex_active())
		return;
	load_next_rolodex_entry();
}	/* Next */


/*
Previous() advances to the previous entry in the rolodex database.
*/
/*ARGSUSED*/
static void Previous(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	if (!is_rolodex_active())
		return;
	load_previous_rolodex_entry();
}	/* Previous */


/*
Index() invokes the rolodex index system for selecting a
specific rolodex entry.
*/
/*ARGSUSED*/
static void Index(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	if (!is_rolodex_active())
		return;
	save_current_text();
	if (!xrolo_index_build(char_set))
		return;
	listShell_activate(entryIndex);
}	/* Index */


/*
Find() locates the next occurence of the specified text
across rolodex entries, beginning with the next entry.
*/
/*ARGSUSED*/
static void Find(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	if (!XtIsRealized(findShell))
		return;
	XtMapWidget(findShell);
}	/* Find */


/*
NewIns() clears the rolodex entry area for new text.
*/
/*ARGSUSED*/
static void NewIns(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	if (!is_rolodex_active())
		return;
	new_entry(xrolo_INSERT);
}	/* NewIns */


/*
NewAdd() clears the rolodex entry area for new text.
*/
/*ARGSUSED*/
static void NewAdd(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	if (!is_rolodex_active())
		return;
	new_entry(xrolo_ADD);
}	/* NewAdd */


/*
Copy() inserts a new rolodex entry into the rolodex file.
*/
/*ARGSUSED*/
static void Copy(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	if (!is_rolodex_active())
		return;
	if (!current_entry) {
		user_message("Nothing to copy.");
		return;
	}
	save_current_text();
	user_message("Inserting a duplicate entry.");
	insert_new_db_entry();
}	/* Copy */


/*
Delete() deletes the current rolodex entry from the rolodex file.
*/
/*ARGSUSED*/
static void Delete(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	if (!is_rolodex_active())
		return;
	listShell_deactivate(entryIndex);	/* must be rebuilt */
	if (editing_new_entry) {
		if (!current_entry)
			editor_set_text(entryEditWindow, "");
		else {
			current_entry = xrolo_db_current_entry();
			editor_set_text(entryEditWindow, xrolo_db_get_text(current_entry));
		}
		editing_new_entry = entry_modified = FALSE;
	}
	else {
		/*
		it's possible to delete all entries:
		*/
		if ((current_entry = xrolo_db_delete_current_entry()) == NULL)
			editor_set_text(entryEditWindow, "");
		else
			editor_set_text(entryEditWindow, xrolo_db_get_text(current_entry));
		entry_modified = FALSE;
	}
}	/* Delete */


/*
Undelete() recovers the most recent rolodex entry deletion.
*/
/*ARGSUSED*/
static void Undelete(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	if (!is_rolodex_active())
		return;
	save_current_text();
	if ((temp_entry = xrolo_db_recover_deleted_entry()) != NULL)
		update_current_entry(temp_entry);
}	/* Undelete */


/*
IndexSelection() chooses a new current rolodex entry from
the rolodex file based on the user's selection in the item
selection box.
*/
/*ARGSUSED*/
static void IndexSelection(w, client_data, call_data)
Widget w;
XtPointer client_data;
XmListCallbackStruct *call_data;
{
	/* already saved entry in Index() */
	temp_entry = (!xrolo_db_current_entry_is_first_entry()) ?
		xrolo_db_first_entry() : current_entry;
	if (!temp_entry)
		return;
	if ((temp_entry = xrolo_db_nth_entry(call_data->item_position - 1)) != NULL)
		update_current_entry(temp_entry);
	else {
		if ((temp_entry = xrolo_db_first_entry()) != NULL)
			update_current_entry(temp_entry);
	}
}	/* IndexSelection */


/*
Sort() sorts the rolodex database entries.
*/
/*ARGSUSED*/
static void Sort(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	if (!is_rolodex_active())
		return;
	if (!current_entry) {
		user_message("Nothing to sort.");
		return;
	}
	save_current_text();
	temp_entry = ((int) client_data == xrolo_ASCEND) ?
		xrolo_db_sort_ascending() : xrolo_db_sort_descending();
	if (temp_entry)
		update_current_entry(temp_entry);
}	/* Sort */


/*
Private support functions:
*/

/*
check_for_unsaved_changes() raises the dialog box that signals
unsaved changes, if there are pending modifications to the
rolodex file.
*/

static int check_for_unsaved_changes(dialog_box)
Dialog dialog_box;
{
	if (!entry_modified && !xrolo_db_is_modified())
		return FALSE;
	dialog_activate(dialog_box);
	return TRUE;
}	/* check_for_unsaved_changes */


/*
load_next_rolodex_entry() simply requests that the next rolodex
entry be loaded, checking for several special conditions.
*/

static void load_next_rolodex_entry()
{
	save_current_text();
	if (xrolo_db_current_entry_is_last_entry())
		return;
	if ((temp_entry = xrolo_db_next_entry()) != NULL)
		update_current_entry(temp_entry);
}	/* load_next_rolodex_entry */


/*
load_previous_rolodex_entry() simply requests that the previous
rolodex entry be loaded, checking for several special conditions.
*/

static void load_previous_rolodex_entry()
{
	save_current_text();
	if (xrolo_db_current_entry_is_first_entry())
		return;
	if ((temp_entry = xrolo_db_previous_entry()) != NULL)
		update_current_entry(temp_entry);
}	/* load_previous_rolodex_entry */


/*
new_entry() clears the rolodex viewport and sets the
insertion point.
*/

static void new_entry(operation)
int operation;
{
	if (!entry_modified && editing_new_entry) {
		user_message("Already editing new entry.");
		return;
	}
	save_current_text();
	editing_new_entry = TRUE;
	editor_set_text(entryEditWindow, "");
	entry_modified = FALSE;	/* saved it before setting text */
	/*
	want to add the new entry after current entry?
	*/
	if (operation == xrolo_ADD) {
		/*
		if already at the end, point to end of the database:
		*/
		if ((temp_entry = xrolo_db_next_entry()) == NULL) {
			current_entry = xrolo_db_past_last_entry();
		}
		else
			current_entry = temp_entry;
	}
}	/* new_entry */


/*
save_current_text() handles both (1) text entered in the
viewport after pressing "New/{Ins,Add}", and (2) changes
to the current/existing entry.  It is called by every
module that initiates a high-level operation.  It imposes
the following implicit policy:  If the user has pressed
"New/{Ins,Add}", but doesn't enter any text, an empty
entry is NOT added to the database.
*/

static int save_current_text()
{
	listShell_deactivate(entryIndex);	/* must be rebuilt */
	if (!entry_modified) {
		editing_new_entry = FALSE; /* may have been set, but no text entered */
		return FALSE;
	}
	if (editing_new_entry || !current_entry)
		insert_new_db_entry();
	else
		save_current_db_entry();
	editing_new_entry = FALSE;
	return TRUE;
}	/* save_current_text */


/*
insert_new_db_entry() inserts the text from the viewport into the
rolodex file WITHOUT updating the entry viewport--useful after
adding new text with "New".
*/

static void insert_new_db_entry()
{
	if ((temp_entry =
	xrolo_db_insert_new_entry(editor_get_text(entryEditWindow))) == NULL) {
		fprintf(stderr, "xrolodex: can't insert new entry!\n");
		user_message("Can't insert new entry!");
	}
	else {
		current_entry = temp_entry;
		entry_modified = FALSE;
	}
	editing_new_entry = FALSE;
}	/* insert_new_db_entry */


/*
save_current_db_entry() saves the current viewport text by deleting
the current entry from the rolodex file and then inserting the text
from the viewport.  xrolo_db_delete_current_entry_no_undo() does NOT
affect the deletion history.
*/

static void save_current_db_entry()
{
	current_entry = xrolo_db_delete_current_entry_no_undo();
	insert_new_db_entry();
}	/* save_current_db_entry */


/*
find_entry_containing_text_forward() searches forward through the
rolodex entries for the first entry containing the specified text.
*/

static void find_entry_containing_text_forward()
{
	char *search_text = XmTextGetString(findText);

	save_current_text();
	if (xrolo_db_current_entry_is_last_entry()) {
		user_message("Search forward:  currently at last entry.");
		XtFree(search_text);
		return;
	}
	if ((temp_entry =
	xrolo_db_find_entry_forward(search_text, case_sensitive_search)) != NULL)
		update_current_entry(temp_entry);
	else
		user_message("Search forward:  no match.");
	XtFree(search_text);
}	/* find_entry_containing_text_forward */


/*
find_entry_containing_text_reverse() searches backward through the
rolodex entries for the first entry containing the specified text.
*/

static void find_entry_containing_text_reverse()
{
	char *search_text = XmTextGetString(findText);

	save_current_text();
	if (xrolo_db_current_entry_is_first_entry()) {
		user_message("Search backward:  currently at first entry.");
		XtFree(search_text);
		return;
	}
	if ((temp_entry =
	xrolo_db_find_entry_reverse(search_text, case_sensitive_search)) != NULL)
		update_current_entry(temp_entry);
	else
		user_message("Search backward:  no match.");
	XtFree(search_text);
}	/* find_entry_containing_text_reverse */


/*
open_rolodex_file() opens the file and loads the first entry if
the file exists; otherwise, it clears the current rolodex display.
*/

static void open_rolodex_file(filename)
char *filename;
{
	Arg args[3];
	int i;

	initialize_critical_variables();
	i = 0;
	XtSetArg(args[i], XmNalignment, (XtArgVal) XmALIGNMENT_CENTER); i++;
	XtSetArg(args[i], XmNlabelString,
		XmStringCreateLtoR(filename, char_set)); i++;
	XtSetValues(databaseLabel, args, i);
	current_entry = xrolo_db_create(filename, delimiter);
	if (current_entry)
		editor_set_text(entryEditWindow, xrolo_db_get_text(current_entry));
	else
		editor_set_text(entryEditWindow, "");	/* file doesn't yet exist */
	entry_modified = FALSE;
}	/* open_rolodex_file */


/*
save_as_rolodex_file() manages the save-as process for saving
the rolodex file under a new filename.
*/
static void save_as_rolodex_file()
{
	if (file_exists(save_as_filename))
		dialog_activate(overwriteDialog);
	else
		do_save_as_operation();
}	/* save_as_rolodex_file */


/*
do_save_as_operation() completes the save-as task, if there
is no file-write error, and updates the filename label.
*/

static void do_save_as_operation()
{
	Arg args[3];
	int i;

	if (!xrolo_db_save_as(save_as_filename)) {
		user_message("Can't save file.");
		return;
	}
	initialize_critical_variables();
	i = 0;
	XtSetArg(args[i], XmNalignment, (XtArgVal) XmALIGNMENT_CENTER); i++;
	XtSetArg(args[i], XmNlabelString,
		XmStringCreateLtoR(save_as_filename, char_set)); i++;
	XtSetValues(databaseLabel, args, i);
}	/* do_save_as_operation */


/*
save_first_time() is called before updating
the rolodex file to ensure that a back-up
is created.
*/

static void save_first_time()
{
	if (first_time) {
		first_time = FALSE;
		xrolo_db_save_backup();
	}
}	/* save_first_time */


/*
update_current_entry() sets up the new current entry.
*/

static void update_current_entry(new_entry)
EntryDB new_entry;
{
	current_entry = new_entry;
	editor_set_text(entryEditWindow, xrolo_db_get_text(current_entry));
	entry_modified = FALSE;	/* saved it before setting text */
}	/* update_current_entry */


/*
is_rolodex_active() is called before every operation that leads
to rolodex modification.
*/

static int is_rolodex_active()
{
	if (!xrolo_db_is_active()) {
		user_message(
		"First, use \"Open\" to establish a (new or existing) rolodex file.");
		return FALSE;
	}
	return TRUE;
}	/* is_rolodex_active */


/*
initialize_critical_variables() is called before opening
a new rolodex file.
*/

static void initialize_critical_variables()
{
	editing_new_entry = FALSE;
	current_entry = NULL;
	entry_modified = FALSE;
	first_time = TRUE;
	listShell_deactivate(entryIndex);
}	/* initialize_critical_variables */


/*
cleanup_and_exit() is called by several functions
when the application terminates.
*/

static void cleanup_and_exit(w)
Widget w;
{
	if (save_as_filename)
/*		free(save_as_filename);*/
		XtFree(save_as_filename);
	xrolo_index_destroy();
	xrolo_db_destroy();
	ctrlPanel_destroy(entryPanel);
	editor_destroy(entryEditWindow);
	listShell_destroy(entryIndex);
	XFreePixmap(XtDisplay(w), icon_pixmap);
	exit(0);
}	/* cleanup_and_exit */


/*
user_message() sets the message dialog box to
have the specified message and then maps
the dialog box.
*/

static void user_message(msg)
char *msg;
{
	dialog_set_prompt(messageDialog, msg);
	dialog_activate(messageDialog);
}	/* user_message */


/*
initialize_shell_icons() sets the icon for top-level windows.
*/

static void initialize_shell_icons(w, icon_name)
Widget w;
char *icon_name;
{

#include "xrolo.icon"

	Arg args[3];
	int num_args = 1;

	icon_pixmap = XCreateBitmapFromData(XtDisplay(w),
		RootWindowOfScreen(XtScreen(w)), xrolo_bits,
		xrolo_width, xrolo_height);
	XtSetArg(args[0], XtNiconPixmap, (XtArgVal) icon_pixmap);
	if (icon_name) {
		XtSetArg(args[1], XtNiconName, (XtArgVal) icon_name);
		num_args++;
	}
	XtSetValues(w, args, num_args);
	XtSetValues(listShell_shell(entryIndex), args, num_args);
	XtSetValues(editor_replaceShell(entryEditWindow), args, num_args);
	XtSetValues(findShell, args, num_args);
	XtSetValues(help_shell(), args, num_args);
}	/* initialize_shell_icons */


/*
handle_close_buttons() accommodates the "Close" buttons.  The actions
should be hardcoded -- the user should not be allowed to override the
XmUNMAP default for pop-ups.
*/

static void handle_close_buttons(w)
Widget w;
{
	Atom xrolo_DELETE_WINDOW;
	Arg arg;

	xrolo_DELETE_WINDOW = XmInternAtom(XtDisplay(w),
		"WM_DELETE_WINDOW", False);
	XtSetArg(arg, XmNdeleteResponse, (XtArgVal) XmDO_NOTHING);
	XtSetValues(w, &arg, 1);
	XtSetValues(findShell, &arg, 1);
	XtSetValues(help_shell(), &arg, 1);
	XtSetValues(listShell_shell(entryIndex), &arg, 1);
	XtSetValues(editor_replaceShell(entryEditWindow), &arg, 1);
	XtSetValues(dialog_dialogShell(messageDialog), &arg, 1);
	XtSetValues(dialog_dialogShell(overwriteDialog), &arg, 1);
	XtSetValues(dialog_dialogShell(openUnsavedDialog), &arg, 1);
	XtSetValues(dialog_dialogShell(quitUnsavedDialog), &arg, 1);
	//XtSetValues(XtParent(fileSelectionDialog), &arg, 1);
	XmAddWMProtocolCallback(w, xrolo_DELETE_WINDOW,
		Close, (caddr_t) w);
	XmAddWMProtocolCallback(findShell, xrolo_DELETE_WINDOW,
		UnmapWindow, (caddr_t) findShell);
	XmAddWMProtocolCallback(help_shell(), xrolo_DELETE_WINDOW,
		UnmapWindow, (caddr_t) help_shell());
	XmAddWMProtocolCallback(listShell_shell(entryIndex), xrolo_DELETE_WINDOW,
		UnmapWindow, (caddr_t) listShell_shell(entryIndex));
	XmAddWMProtocolCallback(editor_replaceShell(entryEditWindow),
		xrolo_DELETE_WINDOW, UnmapWindow,
		(caddr_t) editor_replaceShell(entryEditWindow));
	XmAddWMProtocolCallback(dialog_dialogShell(messageDialog),
		xrolo_DELETE_WINDOW, DialogCancel, (caddr_t) messageDialog);
	XmAddWMProtocolCallback(dialog_dialogShell(overwriteDialog),
		xrolo_DELETE_WINDOW, DialogCancel, (caddr_t) overwriteDialog);
	XmAddWMProtocolCallback(dialog_dialogShell(openUnsavedDialog),
		xrolo_DELETE_WINDOW, DialogCancel, (caddr_t) openUnsavedDialog);
	XmAddWMProtocolCallback(dialog_dialogShell(quitUnsavedDialog),
		xrolo_DELETE_WINDOW, DialogCancel, (caddr_t) quitUnsavedDialog);
	//XmAddWMProtocolCallback(XtParent(fileSelectionDialog), xrolo_DELETE_WINDOW,
	//	UnmanageWindow, (caddr_t) fileSelectionDialog);
}	/* handle_close_buttons */

