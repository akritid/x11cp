#### xrolo.make ####

# WIDGET_ACCESS is in `editor.[ch]'
# MANUAL_CLIP is in `editor.c'
#
CFLAGS = -I/usr/include/Xm -D_NO_PROTO -DWIDGET_ACCESS -DMANUAL_CLIP

LDFLAGS = 

XTLIB = -lXt

# If it's not `Xt', name your Xt library:
#XTLIB = -lXtm


xrolodex:	xrolo.o xrolo.db.o xrolo.index.o help.o streamdb.o \
			ctrlpanel.o editor.o listshell.o dialog.o strstr.o motif.o
			cc $(LDFLAGS) -o xrolodex xrolo.o xrolo.db.o xrolo.index.o \
			help.o streamdb.o ctrlpanel.o editor.o \
			listshell.o dialog.o strstr.o motif.o -lXm $(XTLIB) -lX11

