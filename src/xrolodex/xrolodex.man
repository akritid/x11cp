.TH XROLODEX l "10 January 1992"
.SH NAME
xrolodex \- X Window System Rolodex(R) Application
.SH SYNOPSIS
.B xrolodex [\-help]
[
.I <filename>
]
\-help
.SH DESCRIPTION
.LP
.I xrolodex
is a small, rolodex-like application for X Window System
environments.  It has a Motif-based user interface.  Each button
and menu for
.I xrolodex
is described in detail in the help system.
.SH AUTHOR
.PP
Jerry Smith, Iris Computing Laboratories
.PP
Copyright 1992, Iris Computing Laboratories
