typedef char * String;
#ifndef INCLUDE_FALLBACKS

String FALLBACKS[] = { 0 } ;

#else

/*Resource file for xtartan v2.2 23-APR-95 */
/*XTartan.width: 400 */
/*XTartan.height: 400 */
String FALLBACKS[] = { "XTartan.geometry : 400x400",
"XTartan.showMinimum : False",
"XTartan.translations: \
	 :<Key>x : xpm()\\n\
	 :<Key>g : gif()\\n\
	 :<Key>d : dark()\\n\
	 :<Key>a : fade()\\n\
	 :<Key>? : help()\\n\
      Ctrl<Key>C : exit()\\n\
	 :<Key>h : help()\\n\
	 :<Key>f : setTartan(0)\\n\
	 :<Key>n : setTartan(+)\\n\
	 :<Key>space : setTartan(+)\\n\
	 :<Key>m : minimum()\\n\
	 :<Key>N : name()\\n\
	 :<Key>p : setTartan(-)\\n\
	 :<Key>P : sett()\\n\
	 :<Key>q : exit()\\n\
	 :<Key>t : info()\\n\
	 :<Key>r : root()\\n\
	 :<Key>* : scale(+1)\\n\
	 :<Key>/ : scale(-1)\\n\
	 :<Key>+ : lineWidth(+1)\\n\
	 :<Key>- : lineWidth(-1)\\n\
	 :<Key>= : scale() lineWidth()\\n\
	 :<Key>$ : dump()",
"XTartan.helpString: \
Default bindings:\\n\
h or ?   this help message\\n\
f  go to the first tartan\\n\
m  toggle displaying minimum size vs. fixed box\\n\
n  circulate through tartans (next)\\n\
N  print name of current tartan\\n\
p  circulate through tartans in reverse (previous)\\n\
P  print name and sett of current tartan\\n\
q or ^C  quit\\n\
t  print name and sizes of current tartan\\n\
x  generate xpm file of current tartan\\n\
g  generate gif file of current tartan\\n\
d  toggle between ancient/modern colors\\n\
a  toggle between faded/normal colors (the fadeValue cant not be changed)\\n\
*  increase scale factor by 1\\n\
/  decrease scale factor by 1\\n\
+  increase line width by 1\\n\
-  decrease line width by 1\\n\
=  print current scale factor and line width\\n\
r  install current tartan to root window and exit\\n\
$  dump tartan and color info (for debugging)\\n",

/* dark mode -- default is False */
"*dark : False",

/* fade mode -- default is False, with 2 as fadeValue */
"*fade : False",
"*fadeValue : 2",

/*Color code definitions. */
/*The various sources used different color names; I have tried to condense these */
/*into a single set of colors without losing too much information. */

/*These are the colors that are widely used in the setts */
/*Azure */
"XTartan*colorCode.AZ: Sky Blue",
"XTartan*colorCode.B: MediumBlue",
"XTartan*colorCode.BK: Black",
"XTartan*colorCode.BR: Brown",
/*Crimson */
"XTartan*colorCode.CR: Firebrick",
/*Dark Blue */
"XTartan*colorCode.DB: Navy",
"XTartan*colorCode.G: ForestGreen",
"XTartan*colorCode.GY: Grey",
/*Light Green */
"XTartan*colorCode.LG: Pale Green",
/*Purple */
"XTartan*colorCode.PU: Plum",
"XTartan*colorCode.R: Red",
"XTartan*colorCode.W: White",
"XTartan*colorCode.Y: Yellow",

/*more colors from (W)(Lyon Court); these are used in only a few places */
/*Lilac - used only in MacThomas_LC */
"XTartan*colorCode.Lil: Orchid",
/*Lavender used only in Priest */
"XTartan*colorCode.Lv: Lavender",
/*Magenta used only in MacThomas_LC */
"XTartan*colorCode.Ma: Magenta",
/*Maroon (Mn) used only in Royal Canadien Air Force */
"XTartan*colorCode.Mn: Maroon",
/*Orange used only in Cumming_L and Skene_N */
"XTartan*colorCode.Or: Orange",

/*used in tests at end */
"XTartan*colorCode.Cy: Cyan",
"XTartan*colorCode.Cor: Coral",
"XTartan*colorCode.ForGr: ForestGreen",
"XTartan*colorCode.SlB: Slate Blue",
"XTartan*colorCode.Glr: Goldenrod",
"XTartan*colorCode.Mar: Maroon",
"XTartan*colorCode.Trq: Turquoise",
"XTartan*colorCode.Wh: Wheat",

/*The tartan sett definitions */
/*%a means sett is asymmetric (no pivots) */
/*%b means divide all thread counts by two when parsed into memory */
/*%p means last two colors are double pivot (normal symmetry is single pivot */
/*   on first and last colors) */
/*%v means V stripes are different from H stripes; v stripes follow */
/*() in sett descriptions are comments */
/*By default, a sett is assumed symmetric and with H and V stripes the same. */
/*Some setts have a color which alternates between two colors in every other */
/*sett; these are listed below with a double-size sett, with comments indicating */
/*which colors are the alternates and where the pivot is. */
/*Sources for sett info are indicated as leading comment as follows: */
/*(W): "The Tartan Waevers Guide" by James D. Scarlett, published 1985 by */
/*     Shepheard-Walwyn (Publishers) Ltd., London */
/*(D): "The Official Tartan map of tartans approved by clan chiefs, */
/*     the standing council of Scottish chiefs, or the Lord Lyon King of Arms" */
/*     by Dunbar and Pottinger, published by Elm Tree Books in 1976. */
/*(C): "The Clans and Tartans of Scotland" by Robert Bain, published by Collins, */
/*     London and Glasgow, in 1950 (first published 1938) */
/*     This source has no thread counts, only photos; I have estimated the thread */
/*     counts and colors, but the accuracy is limited by the quality of the */
/*     photographic plates, which is sometimes not particularly good. */
/*Where there were multiple tartans with the same name, I have appended one or */
/*two uppercase characters indicating the source: */
/*_C for source (C) (where photo appears substantially different from the */
/*	other sources). */
/*_D for source (D). */
/*_LC where source (W) indicates the source as the Lyon Court. */
/*_LO where source (W) attributes it to Logan, "The Clans of the  */
/*	Scottish Highlanders". */
/*_P where source (W) shows a photo substantially different from printed info. */
/*_SM where source (W) attributes it to William and Andrew Smith, "Authenticated */
/*	Tartans of the Clans and Families of Scotland" 1850. */
/*_VS where source (W) indicates the source as the Vestiarium Scoticum. */
/*_WI where source (W) indicates the source as the William Wilson and Son's */
/*	1819 Key Pattern Book. _WI1 and _WI2 are also Wilson. */
/*_nnnn (a year) where source (W) has given a year but no other authority. */
/*Please remember that this information has been manually transcribed from */
/*printed material, and there are bound to be errors.  Don't consider these */
/*definitions as authority - if you are serious about getting the proper */
/*definition of a sett, please do a bit of research and find some real */
/*authorities.  The references listed here are a good start, or you can */
/*look up "tartans" at your local library. */
"XTartan.firstTartan: Abercrombie",
"XTartan*Abercrombie.sett: (W) %b G28 W2 G14 BK14 B4 BK4 B4 BK4 B14",
"XTartan*Abercrombie.nextTartan: Abercrombie_D",
"XTartan*Abercrombie_D.sett: (D) %b G28 W2 G14 BK14 B4 BK4 B4 BK4 B28",
"XTartan*Abercrombie_D.nextTartan: Anderson",
"XTartan*Anderson.sett: (W)(D) %b R6 AZ12 R2 BK4 R2 AZ36 BK6 W6 BK6 Y2 BK2 \
	Y2 BK8 R2 B8 R6 G12 R4 G12 R8",
"XTartan*Anderson.nextTartan: Anderson_P",
"XTartan*Anderson_P.sett: (W) %b R6 AZ12 BK2 R4 BK2 AZ36 BK6 W6 BK6 Y2 BK2 \
	Y2 BK8 R2 B8 G12 BK2 R4 BK2 G12 R6",
/*The photo (W) gives is not the same as the thread count it lists; */
/*this is the photo. */
"XTartan*Anderson_P.nextTartan: Arbuthnott",
"XTartan*Arbuthnott.sett: (D) %b B8 BK2 B2 BK2 B2 BK8 G4 W2 G4 B4 G4 W2 \
	G4 BK8 B10 BK2 B2",
"XTartan*Arbuthnott.nextTartan: Armstrong",
"XTartan*Armstrong.sett: (W)(D) %b G4 BK2 G60 BK24 B4 BK2 B2 BK2 B24 R6",
"XTartan*Armstrong.nextTartan: Austin",
"XTartan*Austin.sett: (W) %b BK4 G18 B8 BK8 B8",
"XTartan*Austin.nextTartan: Austin_WI",
"XTartan*Austin_WI.sett: (W) %b BK4 G12 PU6 BK6 PU6",
"XTartan*Austin_WI.nextTartan: Baird",
"XTartan*Baird.sett: (W)(D) %b PU6 G2 PU2 G16 BK16 B16 BK4 B6",
"XTartan*Baird.nextTartan: Balmoral",
"XTartan*Balmoral.sett: (C) %b R2 W2 BK2 W4 GY8 W2 GY2 W2 BK4 GY4 W22 R4 W8",
"XTartan*Balmoral.nextTartan: Barclay_Dress",
"XTartan*Barclay_Dress.sett: (W)(D) %b W2 Y12 BK12 Y2",
"XTartan*Barclay_Dress.nextTartan: Barclay_Hunting",
"XTartan*Barclay_Hunting.sett: (W) %b R2 G32 B32 G2",
"XTartan*Barclay_Hunting.nextTartan: Black_Watch",
"XTartan*Black_Watch.sett: (W) %b B22 BK2 B2 BK2 B2 BK16 G16 BK2 G16 BK16 B16 BK2 B2 \
	G10 BK2 G10 BK8 DB9 BK1 DB1",
/*(W) lists Black Watch under Campbell */
"XTartan*Black_Watch.nextTartan: Borthwick",
"XTartan*Borthwick.sett: (W) %b G34 BK2 CR32 BK4 GY28 BK38 GY28 BK4 CR12",
"XTartan*Borthwick.nextTartan: Borthwick_D",
"XTartan*Borthwick_D.sett: (D) %b CR8 BK4 GY20 BK28 GY20 BK4 CR20 BK2 G24",
"XTartan*Borthwick_D.nextTartan: Boyd",
"XTartan*Boyd.sett: (D)(W) %b Y10 G44 BK4 B4 BK4 B4 BK20 R76 G10 R8 BK8 W10",
"XTartan*Boyd.nextTartan: Breadalbane_Fencibles",
"XTartan*Breadalbane_Fencibles.sett: (W) %b B16 BK2 B2 BK2 B2 BK16 Y2 G28 Y2 \
	BK16 B16 BK2 B2",
/*(W) lists this under Campbell */
"XTartan*Breadalbane_Fencibles.nextTartan: Brodie",
"XTartan*Brodie.sett: (W) %b R96 W8 B8 BK8 R24 B8 R2 Y8",
"XTartan*Brodie.nextTartan: Brodie_Dress",
"XTartan*Brodie_Dress.sett: (D)(W) %b BK4 R32 BK16 Y2 BK16 R4",
"XTartan*Brodie_Dress.nextTartan: Brodie_Hunting",
"XTartan*Brodie_Hunting.sett: (W) %b R4 B16 G16 BK16 Y2 BK16 R4",
"XTartan*Brodie_Hunting.nextTartan: Brodie_1850",
"XTartan*Brodie_1850.sett: (W) %b R96 W8 B8 BK8 R24 B8 R2 Y8",
/*(W) just says this was used around 1850, but does not give an authority */
"XTartan*Brodie_1850.nextTartan: Bruce",
"XTartan*Bruce.sett: (D)(W) %b W2(alternate Y) R16 G4 R4 G12 R2(pivot) \
	G12 R4 G4 R16 Y2(alternate W)",
"XTartan*Bruce.nextTartan: Buchan",
"XTartan*Buchan.sett: (D) %b %p BK4 R4 G54 R4 BK12 B4 R12 G12 R4 BK48 R4 B4 BK4",
"XTartan*Buchan.nextTartan: Buchanan",
"XTartan*Buchanan.sett: (W) %a %b BK2 Y12 BK2 B8 BK2 G12 B8 G12 BK2 B8 BK2 R16 \
	W2 R16 BK2 B8 BK2 Y12",
"XTartan*Buchanan.nextTartan: Buchanan_D",
"XTartan*Buchanan_D.sett: (D) %b AZ4 G32 BK2 AZ4 BK2 Y8 BK2 Y8 BK2 AZ4 BK2 R32 W4",
"XTartan*Buchanan_D.nextTartan: Buchanan_VS",
"XTartan*Buchanan_VS.sett: (W) %b BK2 W18 CR8 W4 CR8 W4",
"XTartan*Buchanan_VS.nextTartan: Cameron",
"XTartan*Cameron.sett: (W) %b Y2 R32 G12 R4 G12 R4",
"XTartan*Cameron.nextTartan: Cameron_Clan_D",
"XTartan*Cameron_Clan_D.sett: (D) %b Y2 R30 G12 R2 G12 R2",
"XTartan*Cameron_Clan_D.nextTartan: Cameron_Hunting",
"XTartan*Cameron_Hunting.sett: (W) %b Y4 G6 B32 G28 R6 G20 R6",
"XTartan*Cameron_Hunting.nextTartan: Cameron_of_Erracht",
"XTartan*Cameron_of_Erracht.sett: (W) %b G16 R2 G2 R6 G32 BK32 R2 B32 R6 B16 Y4",
"XTartan*Cameron_of_Erracht.nextTartan: Cameron_of_Locheil",
"XTartan*Cameron_of_Locheil.sett: (W) %b R12 G6 R12 B2 W2 B2 R4 B16 R8",
"XTartan*Cameron_of_Locheil.nextTartan: Campbell",
"XTartan*Campbell.sett: (W) %b B22 BK2 B2 BK2 B2 BK16 G16 BK2 G16 BK16 B16 BK2 B2",
/*This is the Black Watch tartan (also listed above under Black Watch) */
/*(C) lists the above as Campbell of Argyll */
"XTartan*Campbell.nextTartan: Campbell_of_Argyll",
"XTartan*Campbell_of_Argyll.sett: (D)(W) %b B2 BK2 B16 BK16 G16 BK2 W4(alternate Y) \
	BK2 G16 BK16 B2 BK2 B2 BK2 B16(pivot) \
	BK2 B2 BK2 B2 BK16 G16 BK2 Y4(alternate W) \
	BK2 G16 BK16 B16 BK2 B2",
"XTartan*Campbell_of_Argyll.nextTartan: Campbell_of_Breadalbane",
"XTartan*Campbell_of_Breadalbane.sett: (D)(W) %b BK6 B18 BK18 G18 Y4 G18 BK18",
"XTartan*Campbell_of_Breadalbane.nextTartan: Campbell_of_Cawdor",
"XTartan*Campbell_of_Cawdor.sett: (D)(W) %b AZ4 BK2 G16 BK16 B16 BK2 R4",
"XTartan*Campbell_of_Cawdor.nextTartan: Campbell_of_Loudon",
"XTartan*Campbell_of_Loudon.sett: (D)(W) %b Y4(alternate W) BK2 G24 BK24 B24 BK2 \
	B4(pivot) BK2 B24 BK24 G24 BK2 W4(alternate Y)",
"XTartan*Campbell_of_Loudon.nextTartan: Carnegie",
"XTartan*Carnegie.sett: (D)(W) %b Y2 G4 R2 G2 R4 G12 BK12 R2 B12 R4 B2 R2 B6",
"XTartan*Carnegie.nextTartan: Chisholm",
"XTartan*Chisholm.sett: (W) %b R24 PU4 W2 PU4 R6 G16 R6 PU2",
"XTartan*Chisholm.nextTartan: Chisholm_D",
"XTartan*Chisholm_D.sett: (D) %b R12 W2 R48 PU12 G4 PU2 G4 PU2 G24 R2",
"XTartan*Chisholm_D.nextTartan: Chisholm_VS",
"XTartan*Chisholm_VS.sett: (W) %b R12 W2 R48 B12 G4 BK2 G4 BK2 G24 R2",
/*(W) notes that the two BK stripes are usually woven in B */
"XTartan*Chisholm_VS.nextTartan: Clan_Chattan",
"XTartan*Clan_Chattan.sett: (W) %b R244 BK8 W4 G64 W8 Y14 R14 BK4 R14 Y14 W8 \
	AZ64 BK16 R16 Y24 W8",
"XTartan*Clan_Chattan.nextTartan: Clan_Chattan_D",
"XTartan*Clan_Chattan_D.sett: (D) %b R120 BK4 W2 G32 W4 Y6 R6 BK2 R6 Y6 W4 \
	AZ32 BK8 R8 Y12 W4",
"XTartan*Clan_Chattan_D.nextTartan: Clark",
"XTartan*Clark.sett: (W) %b AZ24 BK8 G8 BK8 R24",
"XTartan*Clark.nextTartan: Clergy",
"XTartan*Clergy.sett: (W) %b W2 BK10 W2 GY8 W2 BK52 W2 BK20 GY10 BK4 GY10 BK20 W2 \
	GY8 W2 BK10 W2",
/*(C) shows a tartan that it says is made with dark blue, light blue, and black */
/*stripes, but the photo is not clear enough to get thread counts from */
"XTartan*Clergy.nextTartan: Cochrane",
"XTartan*Cochrane.sett: (W) %b G32 R4 G4 R2 G6 R2 G4 R4 G24 BK24 R2 B32 R4 B16 Y4",
"XTartan*Cochrane.nextTartan: Cochrane_LC",
"XTartan*Cochrane_LC.sett: (W) %b G44 R8 G4 R4 G4 R8 G24 BK24 R4 B20 R8 B8 Y6",
"XTartan*Cochrane_LC.nextTartan: Cockburn",
"XTartan*Cockburn.sett: (D)(W) %b G72 BK2 G2 BK2 G2 BK2 B24 BK2 W2 BK2 B24 \
	BK2 Y2 BK2 G24 BK4 R4",
"XTartan*Cockburn.nextTartan: Colquhoun",
"XTartan*Colquhoun.sett: (D)(W) %b B2 BK2 B16 BK16 W2 G16 R4",
"XTartan*Colquhoun.nextTartan: Colqhoun_VS",
"XTartan*Colqhoun_VS.sett: (W) %b B8 BK4 B32 W2 BK16 G48 R8",
"XTartan*Colqhoun_VS.nextTartan: Comyn",
"XTartan*Comyn.sett: (C) %b R2 G8 W2 G8 R4 G4 R18 BK2",
"XTartan*Comyn.nextTartan: Crawford",
"XTartan*Crawford.sett: (D)(W) %b CR12 W4 CR60 G24 CR6 G24 CR6",
"XTartan*Crawford.nextTartan: Cumming",
"XTartan*Cumming.sett: (D)(W) %b %p BK2 R2 G16 R2 BK12 B2 R12 G12 R2 BK16 R2 B2 BK2",
"XTartan*Cumming.nextTartan: Cumming_SM",
"XTartan*Cumming_SM.sett: (W) %b BK4 R36 G12 R6 G18 W2 G18 R6",
"XTartan*Cumming_SM.nextTartan: Cumming_LO",
"XTartan*Cumming_LO.sett: (W) %b AZ8 BK4 AZ8 BK20 Or2 G20 R8 W2 R8",
"XTartan*Cumming_LO.nextTartan: Cumming_VS",
"XTartan*Cumming_VS.sett: (W) %b BK4 R48 G8 R4 G8 R8 G16 W2 G16 R8",
"XTartan*Cumming_VS.nextTartan: Cunningham",
"XTartan*Cunningham.sett: (W) %b W6 R2 B2 R56 BK60 R2 BK6",
"XTartan*Cunningham.nextTartan: Cunningham_D",
"XTartan*Cunningham_D.sett: (D) %b W6 R2 BK2 R56 BK60 R2 BK6",
"XTartan*Cunningham_D.nextTartan: Dalzell",
"XTartan*Dalzell.sett: (D)(W) %b G12 CR6 G4 R64 B4 W2 R8 B12 R8 W2 B4 R8 G64 \
	R8 B4 W2 R48",
/*(W) gives Dalziel identical to (D)'s Dalzell */
"XTartan*Dalzell.nextTartan: Davidson",
"XTartan*Davidson.sett: (D)(W) %b R2 B12 G2 B2 G16 BK2 G16 BK2 G2 BK12 R2",
"XTartan*Davidson.nextTartan: Davidson_Double",
"XTartan*Davidson_Double.sett: (W) %b BK6 W4 BK6 G16 BK16 B16 R4 B6",
/*(W) actually calls it Double Davidson */
"XTartan*Davidson_Double.nextTartan: Davidson_of_Tulloch",
"XTartan*Davidson_of_Tulloch.sett: (W) %b W2 G12 BK6 B12 R2",
"XTartan*Davidson_of_Tulloch.nextTartan: Douglas",
"XTartan*Douglas.sett: (W) %b BK4 AZ4 G16 B16 W2",
"XTartan*Douglas.nextTartan: Dougles_Green",
"XTartan*Dougles_Green.sett: (D) %b BK8 AZ4 G16 B16 W2",
"XTartan*Dougles_Green.nextTartan: Douglas_VS",
"XTartan*Douglas_VS.sett: (W) %b GY4 BK2 GY32 BK16 GY2 BK2 GY2 BK32",
/*(C) lists this as Douglas Grey */
"XTartan*Douglas_VS.nextTartan: Drummond_of_Perth",
"XTartan*Drummond_of_Perth.sett: (D)(W) %b R72 W2 B6 Y2 G32 R16 B6 AZ4 W2",
"XTartan*Drummond_of_Perth.nextTartan: Drummond_C",
"XTartan*Drummond_C.sett: (C) %b R6 BK2 R2 G12 R2 G2 R2 BK4 R2 W2 R12 B2 R2 B2 R6",
"XTartan*Drummond_C.nextTartan: Drummond_VS",
"XTartan*Drummond_VS.sett: (W) %b G8 R2 G2 R56 G16 BK2 G2 BK2 G36 R2 G2 R8",
"XTartan*Drummond_VS.nextTartan: Dunbar",
/*(W) lists "Prince Charles' cloak" under Drummond */
"XTartan*Dunbar.sett: (D)(W) %b R8 BK2 R56 BK16 G42 R12",
"XTartan*Dunbar.nextTartan: Duncan",
"XTartan*Duncan.sett: (D)(W) %b BK8 G42 W6 G42 B42 R8",
"XTartan*Duncan.nextTartan: Dundas",
"XTartan*Dundas.sett: (D)(W) %b BK4 G4 R2 G48 BK24 B32 BK8",
"XTartan*Dundas.nextTartan: Eglinton",
"XTartan*Eglinton.sett: (W) %b BK6 G6 BK6 AZ32 BK6 R6 BK6",
/*(W) lists this under Montgomery */
"XTartan*Eglinton.nextTartan: Elliott",
"XTartan*Elliott.sett: (D)(W) %b R2 B6 BR8 B32",
/*(W) uses Magenta stripe instead of Brown */
"XTartan*Elliott.nextTartan: Erskine",
"XTartan*Erskine.sett: (D)(W) %b R8 G2 R56 G48 R2 G12",
"XTartan*Erskine.nextTartan: Farquharson",
"XTartan*Farquharson.sett: (D)(W) %b R4 B8 BK2 B2 BK2 B2 BK16 G16 Y4 G16 BK16 \
	B16 BK2 R4",
"XTartan*Farquharson.nextTartan: Fergusson",
"XTartan*Fergusson.sett: (D)(W) %b B48 BK16 G16 R4 G16 BK2 W4",
/*(W) lists this Fergusson as Ferguson of Athol */
"XTartan*Fergusson.nextTartan: Ferguson_of_Balquhidder",
"XTartan*Ferguson_of_Balquhidder.sett: (W) %b G4 B24 R2 BK24 G24 BK4",
"XTartan*Ferguson_of_Balquhidder.nextTartan: Fletcher",
"XTartan*Fletcher.sett: (D) %b B12 BK2 B12 BK16 R2 G16 BK4",
"XTartan*Fletcher.nextTartan: Fletcher_C",
"XTartan*Fletcher_C.sett: (C) %b B12 BK2 B12 BK16 R2 G16 R4",
"XTartan*Fletcher_C.nextTartan: Forbes",
"XTartan*Forbes.sett: (D)(W) %b B16 BK2 B4 BK2 B4 BK12 G16 BK2 W4 BK2 G16 BK12 \
	B16 BK2 B4",
"XTartan*Forbes.nextTartan: Forbes_LC",
"XTartan*Forbes_LC.sett: (W) %b B2 BK12 B12 BK12 G12 BK2 W2",
"XTartan*Forbes_LC.nextTartan: Forbes_VS",
"XTartan*Forbes_VS.sett: (W) %b R2 G32 BK16 G6 BK8 Y2",
"XTartan*Forbes_VS.nextTartan: Fraser",
"XTartan*Fraser.sett: (W) %b B32 R2 B2 R2 G24 R32 G4 R32 G24 B24 R2 B2",
"XTartan*Fraser.nextTartan: Fraser_VS",
"XTartan*Fraser_VS.sett: (D)(W) %b W2 R24 G12 R2 B12 R2",
"XTartan*Fraser_VS.nextTartan: Gordon",
"XTartan*Gordon.sett: (D)(W) %b B24 BK4 B4 BK4 B4 BK24 G24 Y4 G24 BK24 B22 BK4 B4",
"XTartan*Gordon.nextTartan: Gordon_VS",
"XTartan*Gordon_VS.sett: (W) %b B56 BK2 B2 BK2 B6 BK24 G48 Y2 G2 Y4 G2 Y2 G48 BK24 \
	B36 BK2 B8",
"XTartan*Gordon_VS.nextTartan: Gow",
"XTartan*Gow.sett: (D)(W) %b R8 G8 R2 B8 R8",
"XTartan*Gow.nextTartan: Graham_of_Menteith",
"XTartan*Graham_of_Menteith.sett: (D)(W) %b BK2 B24 BK24 G2 AZ4 G32",
/*(W) also notes an alternative using PU24 instead of B24 for second stripe */
"XTartan*Graham_of_Menteith.nextTartan: Graham_of_Montrose",
"XTartan*Graham_of_Montrose.sett: (W) %b BK2 B8 BK8 G8 W2 G8 BK8",
"XTartan*Graham_of_Montrose.nextTartan: Graham_W",
"XTartan*Graham_W.sett: (W) %b BK6 PU28 BK34 G8 W4 G42",
/*(W) lists this as Wilson's No. 158 */
"XTartan*Graham_W.nextTartan: Grant",
"XTartan*Grant.sett: (W) %b R12 B2 R4 B4 R64 AZ2 R4 B16 R4 G4 R4 G48 R4 B4 R12",
"XTartan*Grant.nextTartan: Grant_D",
"XTartan*Grant_D.sett: (D) %b R12 B4 R4 B4 R48 AZ4 R4 B12 R4 G4 R4 G40 R4 B4 R12",
"XTartan*Grant_D.nextTartan: Grant_VS",
"XTartan*Grant_VS.sett: (W) %b R8 B4 R4 B4 R112 B32 R8 G2 R8 G72 R6 G2 R8",
"XTartan*Grant_VS.nextTartan: Grant_of_Monymusk",
"XTartan*Grant_of_Monymusk.sett: (W) %b R24 G6 R24 B16 R10 B16 R8 G28 R8 G28 \
	R8 G28 R24",
"XTartan*Grant_of_Monymusk.nextTartan: Gunn",
"XTartan*Gunn.sett: (D)(W) %b R4 G24 BK24 G2 B24 G4",
"XTartan*Gunn.nextTartan: Gunn_VS",
"XTartan*Gunn_VS.sett: (W) %b R4 G60 BK32 G4 BK32 G4",
"XTartan*Gunn_VS.nextTartan: Hamilton",
"XTartan*Hamilton.sett: (W)(D) %b W2 R18 B12 R2 B12",
/*(D) gives Hamilton as above but with AZ insted of B for both stripes */
"XTartan*Hamilton.nextTartan: Hay",
"XTartan*Hay.sett: (D)(W) %b W12 R4 BK2 R4 G8 R96 G24 R4 G4 R4 G72 Y4 G8 R12",
"XTartan*Hay.nextTartan: Hay_and_Leith",
"XTartan*Hay_and_Leith.sett: (W) %b BK6 R2 Y2 BK4 R32 G4 R2 Y2 R4 G30 W2 BK30 R2 PU30 \
	R4 Y2 R2 PU4 R32 BK4 Y2 R2 BK6",
"XTartan*Hay_and_Leith.nextTartan: Henderson",
"XTartan*Henderson.sett: (D)(W) %b Y2 BK12 G8 BK2 G32 B2 G8 B12 W2",
/*(C) lists this as Henderson or MacKendrick */
"XTartan*Henderson.nextTartan: Home",
"XTartan*Home.sett: (D)(W) %b B6 G4 B48 BK16 R2 BK4 R2 BK56",
"XTartan*Home.nextTartan: Hunter",
"XTartan*Hunter.sett: (D) %b G16 BK2 G16 BK16 R2 B16 W2 B16 R2 BK16",
"XTartan*Hunter.nextTartan: Huntly_Old",
"XTartan*Huntly_Old.sett: (W) %b PU32 W4 CR14 W4 BK28 AZ12 W4 PU30 W4 G34 \
	AZ12 G12 R16 BK12 R16 BK4",
/*(W) actually calls it Old Huntly (pre 1800), the cover pattern for the book */
"XTartan*Huntly_Old.nextTartan: Innes",
"XTartan*Innes.sett: (W) %b AZ14 BK48 R8 BK8 R8 BK8 R48 Y8 R12 B24 R12 BK8 \
	G40 BK8 R12 W8",
/*(C) lists this as Innes or MacInnes */
"XTartan*Innes.nextTartan: Innes_D",
"XTartan*Innes_D.sett: (D) %b AZ6 BK24 R4 BK4 R4 BK4 R24 Y4 R6 B12 R6 BK4 G20 BK4 R6 W4",
"XTartan*Innes_D.nextTartan: Johnston",
"XTartan*Johnston.sett: (D)(W) %b Y6 G4 BK2 G60 B48 BK4 B4 BK4",
"XTartan*Johnston.nextTartan: Keith_and_Austin",
"XTartan*Keith_and_Austin.sett: (D) %b BK4 G18 B8 BK8 B8",
/*This Keith is identical to Austin listed above */
"XTartan*Keith_and_Austin.nextTartan: Kennedy",
"XTartan*Kennedy.sett: (W) %b R4 G48 B8 BK6 B6 BK6 B6 BK6 B8 G24 CR2 G4 \
	CR2 G6 Y2 G4 BK4",
/*(D) gives Kennedy same as above but with PU instead of CR stripes */
"XTartan*Kennedy.nextTartan: Kerr",
"XTartan*Kerr.sett: (D)(W) %b BK8 R4 BK2 R56 BK28 G6 BK2 G4 BK2 G40",
"XTartan*Kerr.nextTartan: Kerr_Hunting",
"XTartan*Kerr_Hunting.sett: (W) %b BK8 B4 BK2 B56 BK28 G6 BK2 G4 BK2 G40",
"XTartan*Kerr_Hunting.nextTartan: Kincaid",
"XTartan*Kincaid.sett: (D) %b BK22 G34 R6",
"XTartan*Kincaid.nextTartan: Lamont",
"XTartan*Lamont.sett: (D)(W) %b B6 BK2 B2 BK2 B2 BK8 G8 W2 G8 BK8 B8 BK2 B2",
"XTartan*Lamont.nextTartan: Leslie_Dress",
"XTartan*Leslie_Dress.sett: (D)(W) %b BK2 R64 B32 R8 BK12 Y2 BK12 R8",
"XTartan*Leslie_Dress.nextTartan: Leslie_Hunting",
"XTartan*Leslie_Hunting.sett: (W) %b R4 B16 BK16 W2 G16 BK2",
"XTartan*Leslie_Hunting.nextTartan: Lindsay",
"XTartan*Lindsay.sett: (D)(W) %b CR6 B4 CR48 B16 G4 B4 G4 B4 G40",
"XTartan*Lindsay.nextTartan: Livingston",
"XTartan*Livingston.sett: (D)(W) %b R16 G4 R40 G32 R8 BK2 R4 BK2 R8 G24",
"XTartan*Livingston.nextTartan: Logan_and_MacLennan",
"XTartan*Logan_and_MacLennan.sett: (D)(W) %b Y4 BK2 R2 G32 BK24 B32 R4 B4 R4 B6 R12",
/*(W) also lists another Logan which is also called Skene (as listed under S) */
"XTartan*Logan_and_MacLennan.nextTartan: MacAlister",
"XTartan*MacAlister.sett: (D)(W) %b R16 LG2 G4 R4 AZ2 R2 W2 R2 AZ2 R4 G6 R2 W2 R12 \
	AZ2 R2 G24 R2 AZ2 R32 AZ2 R2 G24 R2 AZ2 R12 W2 R2 B8 R2 W2 R4 G6 \
	LG2 R4 LG2 G6 R6 W2 R2 B4 R2 W2 R16",
"XTartan*MacAlister.nextTartan: MacAlister_CC",
"XTartan*MacAlister_CC.sett: (W) %b R8 AZ2 R2 G12 R24 AZ2 R2 G36 R2 AZ2 R64 AZ2 R2 \
	G36 R2 AZ2 R24 B16 R16 G16 R8 G16 R64",
"XTartan*MacAlister_CC.nextTartan: MacAlpine",
"XTartan*MacAlpine.sett: (W) %a %b G2 BK8 Y2 BK8 G2 BK2 G12 BK2 G12 BK2 G2 BK8 W2 BK8",
"XTartan*MacAlpine.nextTartan: MacAlpine_D",
"XTartan*MacAlpine_D.sett: (D) %a %b G2 BK8 Y2 B8 G2 B2 G12 BK2 G12 BK2 G2 BK8 W2 BK8",
"XTartan*MacAlpine_D.nextTartan: MacArthur",
"XTartan*MacArthur.sett: (D)(W) %b Y6 G60 BK24 G12 BK64",
"XTartan*MacArthur.nextTartan: MacAulay",
"XTartan*MacAulay.sett: (D)(W) %b BK4 R32 G12 R6 G16 W2",
"XTartan*MacAulay.nextTartan: MacAulay_Hunting",
"XTartan*MacAulay_Hunting.sett: (W) %b R4 G24 BK8 G16 BK32 W2 BK32 G12",
"XTartan*MacAulay_Hunting.nextTartan: MacBain",
"XTartan*MacBain.sett: (D)(W) %b R120 W4 AZ10 BK4 W4 BK4 AZ10 W4 BK4 G24 BK4 W4 \
	R10 CR10 G4 CR10 R10 W4 G20",
/*(W) incorrectly lists the thread count with the last two stripes reversed; */
/*the photo shows the proper order, as listed here. */
"XTartan*MacBain.nextTartan: MacBean",
"XTartan*MacBean.sett: (W) %b R48 W2 B4 AZ2 W2 AZ2 B4 W2 BK2 G12 BK2 W2 R4 CR4 G2 \
	CR4 R4 W2 G6",
"XTartan*MacBean.nextTartan: MacBeth",
"XTartan*MacBeth.sett: (D)(W) %b B72 Y8 BK12 W2 BK2 W2 BK2 G16 R12 BK2 R6 W2",
/*(W) incorrectly lists the first W stripe as Y; the photo shows it correctly */
/*as white. */
"XTartan*MacBeth.nextTartan: MacCallum",
"XTartan*MacCallum.sett: (D)(W) %b BK2 B12 BK12 G8 AZ2 BK4 G16",
/*(C) lists this as MacCallum Ancient */
"XTartan*MacCallum.nextTartan: MacCallum_W",
"XTartan*MacCallum_W.sett: (W) %b BK2 B12 BK12 G12 R2 G12 BK12",
"XTartan*MacCallum_W.nextTartan: MacColl",
"XTartan*MacColl.sett: (D)(W) %b G8 R2 G2 R24 B2 R2 B6 R2 B2 R4 G16 R2 B2 R24",
"XTartan*MacColl.nextTartan: MacDonald",
"XTartan*MacDonald.sett: (D)(W) %b G16 R2 G4 R6 G24 BK24 R2 B24 R6 B4 R2 B16",
"XTartan*MacDonald.nextTartan: MacDonald_of_Clanranald",
"XTartan*MacDonald_of_Clanranald.sett: (W) %b G16 R2 G4 R6 G24 W2 BK24 R2 B24 \
	R6 B4 R2 B16",
"XTartan*MacDonald_of_Clanranald.nextTartan: MacDonald_of_Clanranald_D",
"XTartan*MacDonald_of_Clanranald_D.sett: (D) %b G12 R4 G4 R6 G22 W4 BK22 R4 B24 \
	R6 B4 R4 B12",
"XTartan*MacDonald_of_Clanranald_D.nextTartan: MacDonald_of_Sleat",
"XTartan*MacDonald_of_Sleat.sett: (W) %b BK4 R36 G4 R10 G32",
"XTartan*MacDonald_of_Sleat.nextTartan: MacDonald_of_Staffa",
"XTartan*MacDonald_of_Staffa.sett: (C) %b Y2 R12 B2 R6 B2 R2 G4 W2 G4 R6 Y2 R6 \
	BK4 R2 B2 R6 B2 R2 G8 R6 B2 R2 B2 R2 B2 R2 B2 R30",
"XTartan*MacDonald_of_Staffa.nextTartan: MacDonald_Lord_of_the_Isles",
"XTartan*MacDonald_Lord_of_the_Isles.sett: (W) %b R76 G4 R10 G32",
/*(C) gives this as MacDonald of Sleat */
"XTartan*MacDonald_Lord_of_the_Isles.nextTartan: MacDonald_Lord_of_the_Isles_Hunting",
"XTartan*MacDonald_Lord_of_the_Isles_Hunting.sett: (W) %b G48 W2 G4 W4 B4 W2 \
	B24 W2 B4 W4 B4 W2 B24",
"XTartan*MacDonald_Lord_of_the_Isles_Hunting.nextTartan: MacDonell_of_Glengarry",
"XTartan*MacDonell_of_Glengarry.sett: (W) %b W2 G8 R2 G4 R6 G24 BK24 R2 B24 R8 B16",
"XTartan*MacDonell_of_Glengarry.nextTartan: MacDonell_of_Glengarry_D",
"XTartan*MacDonell_of_Glengarry_D.sett: (D) %b B16 R2 B4 R6 B24 R2 BK24 G24 R6 \
	G4 R2 G8 W2",
"XTartan*MacDonell_of_Glengarry_D.nextTartan: MacDonell_of_Keppoch",
"XTartan*MacDonell_of_Keppoch.sett: (D)(W)(W gives it reversed from D) \
	%b G4 R4 B2 R48 AZ2 B12 R6 G24 R8 B2",
/*(W) says this is also called MacKillop */
"XTartan*MacDonell_of_Keppoch.nextTartan: MacDonnald_of_ye_Ylis",
"XTartan*MacDonnald_of_ye_Ylis.sett: (W) %b W8 G60 BK2 G2 BK2 G6 BK24 B20 R6",
/*(C) gives this as MacDonald of the Isles */
"XTartan*MacDonnald_of_ye_Ylis.nextTartan: MacDougall",
"XTartan*MacDougall.sett: (W) %b AZ2 R8 CR12 R144 B8 R16 G36 R36 G36 CR24 R8 CR24 \
	B36 R16 G8 R16 G144 R8 CR24 AZ4",
"XTartan*MacDougall.nextTartan: MacDougall_D",
"XTartan*MacDougall_D.sett: (D) %b W2 PU6 R4 G60 R8 G4 R8 B20 PU8 R4 PU8 G20 \
	R20 G20 R4 B4 R60 PU6 R4 W2 R4 PU6 R60 B4 R4 G20 R10",
"XTartan*MacDougall_D.nextTartan: MacDougall_VS",
"XTartan*MacDougall_VS.sett: (W) %b PU8 G16 B12 PU16 R12 G4 R4 G4 R48 G2 R6",
"XTartan*MacDougall_VS.nextTartan: MacDuff",
"XTartan*MacDuff.sett: (D)(W) %b R8 BK2 R8 G12 BK8 B6 R16",
/*(W) lists MacDuff as above but with AZ6 instead of B6 as 2nd to last stripe */
"XTartan*MacDuff.nextTartan: MacEwan",
"XTartan*MacEwan.sett: (D)(W) %b Y4(alternate R) BK2 G24 BK24 B24 BK2 B4(pivot) \
	BK2 B24 BK24 G24 BK2 R4(alternate Y)",
"XTartan*MacEwan.nextTartan: MacFarlane",
"XTartan*MacFarlane.sett: (D)(W) %b R84 BK2 G24 W4 R6 BK2 R6 W4 G4 PU24 BK8 R6 W8 G6",
"XTartan*MacFarlane.nextTartan: MacFarlane_VS",
"XTartan*MacFarlane_VS.sett: (W) %b BK14 W12 BK2 W12",
"XTartan*MacFarlane_VS.nextTartan: MacFie",
"XTartan*MacFie.sett: (D) %b W2(alternate Y) R24 G4 R2 G32(pivot) \
	R2 G4 R24 Y2(alternate W)",
"XTartan*MacFie.nextTartan: MacGillivray",
"XTartan*MacGillivray.sett: (D)(W) %b R8 AZ2 B2 R64 AZ4 R4 B24 R4 G32 R8 AZ2 R8 B4",
"XTartan*MacGillivray.nextTartan: MacGregor",
"XTartan*MacGregor.sett: (D)(W) %b R72 G36 R8 G12 BK2 W4",
"XTartan*MacGregor.nextTartan: MacIain",
"XTartan*MacIain.sett: (D) %b Y4 BK2 R24 BK16 R8 BK16 R8",
"XTartan*MacIain.nextTartan: MacInnes",
"XTartan*MacInnes.sett: (D)(W) %b Y4 BK24 G4 BK4 G4 BK4 G32 BK6 AZ6 BK6 B24 G12 R4",
/*(C) gives this as MacInnes Hunting */
"XTartan*MacInnes.nextTartan: MacIntyre",
"XTartan*MacIntyre.sett: (W) %b AZ2 R4 G4 R8 B32 R4 G2 R8 B2 R4 G32 R8 B4 R4 AZ2",
"XTartan*MacIntyre.nextTartan: MacIntyre_LC",
"XTartan*MacIntyre_LC.sett: (D)(W) %b W8 G64 B24 R6 B24 G8",
"XTartan*MacIntyre_LC.nextTartan: MacIver",
"XTartan*MacIver.sett: (D)(W) %b W2(alternate Y) R24 BK4 R4 BK32(pivot) \
	R4 BK4 R24 Y2(alternate W)",
"XTartan*MacIver.nextTartan: MacKay",
"XTartan*MacKay.sett: (D)(W) %b BK6 G28 BK28 G4 B28 G6",
/*(W) lists MacKay as above but with PU28 for the 2nd to last stripe */
"XTartan*MacKay.nextTartan: MacKay_VS",
"XTartan*MacKay_VS.sett: (W) %b R2 B32 BK12 B4 BK12 B4",
"XTartan*MacKay_VS.nextTartan: MacKeane",
"XTartan*MacKeane.sett: (W) %b Y4 BK2 R24 BK16 R8 BK16 R8",
/*(W) lists this under MacQueen */
"XTartan*MacKeane.nextTartan: MacKenzie",
"XTartan*MacKenzie.sett: (D)(W) %b B24 BK4 B4 BK4 B4 BK24 G24 BK2 W4 BK2 G24 \
	BK24 B24 BK2 R4",
"XTartan*MacKenzie.nextTartan: MacKinlay",
"XTartan*MacKinlay.sett: (D)(W) %b B4 BK4 B16 BK12 G16 BK2 R4 BK2 G16 BK12 B4 \
	BK4 B4 BK4 B12",
/*(W) lists the stripes in reverse order from the above */
"XTartan*MacKinlay.nextTartan: MacKinnon",
"XTartan*MacKinnon.sett: (D)(W) %b W4 R8 PU4 G16 R32 G4 B8 R4 G32 R12 B4 G4 R6 PU4",
/*(W) says the purple lines are white, pink, rose, and black in the various */
/*references it has used. */
"XTartan*MacKinnon.nextTartan: MacKinnon_Hunting",
"XTartan*MacKinnon_Hunting.sett: (W) %b W4 BR32 G32 R4 G32 BR32 G4",
"XTartan*MacKinnon_Hunting.nextTartan: MacKintosh",
"XTartan*MacKintosh.sett: (W) %b R48 B12 R6 G24 R8 B2",
"XTartan*MacKintosh.nextTartan: MacKintosh_Hunting",
"XTartan*MacKintosh_Hunting.sett: (W) %b Y4 G24 B12 R6 G24 R8 B2",
"XTartan*MacKintosh_Hunting.nextTartan: MacKintosh_D",
"XTartan*MacKintosh_D.sett: (D) %b R44 B10 R4 G22 R6 B2",
"XTartan*MacKintosh_D.nextTartan: MacLachlan",
"XTartan*MacLachlan.sett: (D)(W) %b R32 BK4 R4 BK4 R4 BK32 B32 G6 B32 BK32 R32 BK4 R4",
"XTartan*MacLachlan.nextTartan: MacLachlan_VS",
"XTartan*MacLachlan_VS.sett: (W) %b Y12 BK4 Y48 BK12 Y4 BK42 Y4 BK12",
"XTartan*MacLachlan_VS.nextTartan: MacLachlan_W",
"XTartan*MacLachlan_W.sett: (W) %b Y6 W4 BK32 G32 Y6 W4 R48",
"XTartan*MacLachlan_W.nextTartan: MacLaine_of_Lochbuie",
"XTartan*MacLaine_of_Lochbuie.sett: (D)(W) %b R64 G16 AZ8 Y2",
"XTartan*MacLaine_of_Lochbuie.nextTartan: MacLaine_of_Lochbuie_Hunting",
"XTartan*MacLaine_of_Lochbuie_Hunting.sett: (W) %b B64 R6 B8 BK2 Y6",
"XTartan*MacLaine_of_Lochbuie_Hunting.nextTartan: MacLaren",
"XTartan*MacLaren.sett: (D)(W) %b B48 BK16 G16 R4 G16 BK2 Y4",
"XTartan*MacLaren.nextTartan: MacLean",
"XTartan*MacLean.sett: (D)(W) %b BK2 R4 AZ2 R24 G16 BK2 W2 BK2 Y2 BK6 AZ2 B8",
/*also called MacLean of Duart */
"XTartan*MacLean.nextTartan: MacLean_VS",
"XTartan*MacLean_VS.sett: (W) %b BK2 G32 BK4 G4 BK12 W2 BK12 G6",
/*(C) calls this MacLean Hunting */
"XTartan*MacLean_VS.nextTartan: MacLeod",
"XTartan*MacLeod.sett: (D)(W) %b R6 BK4 G30 BK20 B40 BK4 Y4",
/*(W) lists the above as MacLeod (Logan) */
/*(C) gives the above as MacLeod of Lewis */
"XTartan*MacLeod.nextTartan: MacLeod_of_Lewis",
"XTartan*MacLeod_of_Lewis.sett: (D)(W) %b R2 Y24 BK16 Y2 BK16",
/*(W) lists the above as MacLeod (Vest Scot) */
/*(C) gives the above as MacLeod */
"XTartan*MacLeod_of_Lewis.nextTartan: MacMillan",
"XTartan*MacMillan.sett: (D)(W) %b R2 Y16 R4 Y16 R6 Y4 R24 Y2 R6",
"XTartan*MacMillan.nextTartan: MacMillan_Ancient",
"XTartan*MacMillan_Ancient.sett: (W) %a %b G4 BK2 G36 BK2 G4 BK2 CR24 G8 \
	Y12 BK2 Y12 BK2",
"XTartan*MacMillan_Ancient.nextTartan: MacMillan_Hunting",
"XTartan*MacMillan_Hunting.sett: (W) %b R2 G16 R4 G16 BK8 Y4 BK8 B24 Y2 B6",
"XTartan*MacMillan_Hunting.nextTartan: MacNab",
"XTartan*MacNab.sett: (D)(W) %b G16 CR2 G2 CR2 G2 CR12 R16 CR2 R16 CR12 G14 CR2 G2",
"XTartan*MacNab.nextTartan: MacNab_VS",
"XTartan*MacNab_VS.sett: (W) %b BK2 R24 CR4 G8 CR4 R4 G12",
"XTartan*MacNab_VS.nextTartan: MacNab_WI1",
"XTartan*MacNab_WI1.sett: (W) %b CR48 G2 AZ2 G4 R48",
"XTartan*MacNab_WI1.nextTartan: MacNab_WI2",
"XTartan*MacNab_WI2.sett: (W) %b AZ4 PU22 R6 G30",
"XTartan*MacNab_WI2.nextTartan: MacNaughten",
"XTartan*MacNaughten.sett: (D)(W) %b BK2 AZ2 R32 AZ16 BK24 G32 R32 AZ2 BK2",
"XTartan*MacNaughten.nextTartan: MacNeil",
"XTartan*MacNeil.sett: (W) %b Y2 BK6 G30 BK28 B32 R4 W2",
"XTartan*MacNeil.nextTartan: MacNiel_of_Barra",
"XTartan*MacNiel_of_Barra.sett: (D)(W) %b Y6 BK4 G24 BK24 B28 W6",
/*(D) lists the above as MacNiel */
"XTartan*MacNiel_of_Barra.nextTartan: MacNiel_of_Colonsay",
"XTartan*MacNiel_of_Colonsay.sett: (W) %b BK4 B12 BK12 G12 W2 G12 B8",
"XTartan*MacNiel_of_Colonsay.nextTartan: MacNicol",
"XTartan*MacNicol.sett: (W) %b R12 G2 R12 BK8 R2 AZ2 R2 G16 R12 BK2 R12 G2",
"XTartan*MacNicol.nextTartan: MacNicol_D",
"XTartan*MacNicol_D.sett: (D) %b BK4 R20 G4 R20 BK16 AZ4 BK12 R4 G26 R20 G4 R20 BK4",
"XTartan*MacNicol_D.nextTartan: MacNicol_Hunting",
"XTartan*MacNicol_Hunting.sett: (W) %b B20 BK2 G2 BK2 G2 BK2 B20 R6 BK20 R6 G20 \
	BK2 Y2 BK2 W2 BK2 G20",
"XTartan*MacNicol_Hunting.nextTartan: MacPherson",
"XTartan*MacPherson.sett: (W) %b R2 BK2 W2 R24 AZ8 BK2 AZ2 BK2 AZ8 BK12 Y2 \
	G16 R24 AZ4 R24",
"XTartan*MacPherson.nextTartan: MacPherson_Dress",
"XTartan*MacPherson_Dress.sett: (W) %b W6 R2 W60 BK40 W6 BK18 Y2",
"XTartan*MacPherson_Dress.nextTartan: MacPherson_Hunting",
"XTartan*MacPherson_Hunting.sett: (D)(W) %b B2 R2 GY16 R2 B2 R2 BK16 R2 B2",
"XTartan*MacPherson_Hunting.nextTartan: MacPherson_Of_Cluny",
"XTartan*MacPherson_Of_Cluny.sett: (W) %b R10 PU4 R4 G84 R10 PU72 R140 PU4 Y4 R14 G4",
"XTartan*MacPherson_Of_Cluny.nextTartan: MacPhie",
"XTartan*MacPhie.sett: (W) %b W2(alternate Y) R24 G4 R2 G32(pivot) \
	R2 G4 R24 Y2(alternate W)",
"XTartan*MacPhie.nextTartan: MacQuarie",
"XTartan*MacQuarie.sett: (D)(W) %b R32 G2 R2 G2 R8 G24",
/*(W) names the above MacQuarrie */
"XTartan*MacQuarie.nextTartan: MacQuarrie_1815",
"XTartan*MacQuarrie_1815.sett: (W) %b G4 R6 G4 R104 G4 R4 B56 R4 G84 R4 G4 R6 G4",
/*(W) does not give an authority for this, just says "collected about 1815" */
"XTartan*MacQuarrie_1815.nextTartan: MacQuarrie_LO",
"XTartan*MacQuarrie_LO.sett: (W) %b R4 AZ2 R32 B24 R8 G32 R12",
"XTartan*MacQuarrie_LO.nextTartan: MacQuarrie_SM",
"XTartan*MacQuarrie_SM.sett: (W) %b R2 AZ2 R12 B6 R2 G12 R2 G12 R12 B2 R2 AZ2",
"XTartan*MacQuarrie_SM.nextTartan: MacQueen",
"XTartan*MacQueen.sett: (D)(W) %b Y2 BK24 R12 BK4 R12 BK4",
"XTartan*MacQueen.nextTartan: MacRae",
"XTartan*MacRae.sett: (D)(W) %b G8 R2 G8 R8 B2 R2 B2 R2 B2 R8 B2 R2 B2 R2 B2 R8 \
	W2 R2 B8 R2 B8 R2 W2 R8 G2 R2 G2 R8 G8 R2 G8",
"XTartan*MacRae.nextTartan: MacTavish",
"XTartan*MacTavish.sett: (D)(W) %b AZ4 R24 B4 AZ12 BK12 AZ2",
"XTartan*MacTavish.nextTartan: MacThomas",
"XTartan*MacThomas.sett: (D) %b B6 CR4 B42 BK22 G42 CR4 G6",
"XTartan*MacThomas.nextTartan: MacThomas_LC",
"XTartan*MacThomas_LC.sett: (W) %b G10 Lil6 G64 BK32 B64 Ma6 B10",
"XTartan*MacThomas_LC.nextTartan: Maitland",
"XTartan*Maitland.sett: (D)(W) %b R4 Y4 B4 Y4 G18 BK8 G6 B16 G6",
"XTartan*Maitland.nextTartan: Malcolm",
"XTartan*Malcolm.sett: (D)(W) %a %b B2 R2 B12 BK12 G12 BK2 Y2 BK2 AZ2 BK2 \
	G12 BK12 B12 R2",
"XTartan*Malcolm.nextTartan: Matheson",
"XTartan*Matheson.sett: (D)(W) %b G16 R8 G2 R2 G2 R48 B16 G8 R2 G2 R2 G8 R16 G2 \
	R2 G2 R2 B16 G16 R4 G8",
"XTartan*Matheson.nextTartan: Matheson_N",
"XTartan*Matheson_N.sett: (W) %b R4 G4 R24 B20 AZ6 G20 R4 G4 R4 G20 R24 G4 R4",
"XTartan*Matheson_N.nextTartan: Maxwell",
"XTartan*Maxwell.sett: (D)(W) %b R6 G2 R56 BK12 R8 G32 R6",
"XTartan*Maxwell.nextTartan: Melville",
"XTartan*Melville.sett: (W) %b BK10 W4 G36 BK34 PU32 BK6",
/*(W) lists this under Oliphant */
"XTartan*Melville.nextTartan: Menzies",
"XTartan*Menzies.sett: (W) %b R10 AZ4 W2 G10 R10",
"XTartan*Menzies.nextTartan: Menzies_Dress",
"XTartan*Menzies_Dress.sett: (D) %b R72 W8 R6 W8 R12 W4 R2 W24",
/*(C) gives this as Menzies, and shows a Menzies Black and White which is the */
/*same except for using Black instead of Red. */
"XTartan*Menzies_Dress.nextTartan: Menzies_Hunting",
"XTartan*Menzies_Hunting.sett: (W) %b G96 R8 G4 R8 G12 R4 G6 R18",
"XTartan*Menzies_Hunting.nextTartan: Menzies_VS",
"XTartan*Menzies_VS.sett: (W) %b W8 R2 W4 R6 W48 CR10 R6 CR2 R2 CR2 R40 W4",
"XTartan*Menzies_VS.nextTartan: Merchiston_Castle_School",
"XTartan*Merchiston_Castle_School.sett: (W) %b R10 B72 BK24 W10 BK10 W36 R10 \
	W16 BK16 W16 BK36",
/*(W) lists this under Napier */
"XTartan*Merchiston_Castle_School.nextTartan: Mitchell",
"XTartan*Mitchell.sett: (W) %b BK4 G24 BK24 R2 B24 W4",
"XTartan*Mitchell.nextTartan: Moncreiffe",
"XTartan*Moncreiffe.sett: (W) R1 G1",
"XTartan*Moncreiffe.nextTartan: Moncreiffe_D",
"XTartan*Moncreiffe_D.sett: (D) %b R2 G2 R2",
"XTartan*Moncreiffe_D.nextTartan: Montgomery",
"XTartan*Montgomery.sett: (D)(W) %b BK8 G10 BK8 PU56 BK8 R10 BK8",
/*(D) lists it as Montgomerie */
"XTartan*Montgomery.nextTartan: Montrose",
"XTartan*Montrose.sett: (W) %b AZ4 BK4 R28 AZ14 BK16 G30 R28 BK4 AZ4",
"XTartan*Montrose.nextTartan: Morrison",
"XTartan*Morrison.sett: (W) %b BK6 G28 BK28 G4 B28 R6",
"XTartan*Morrison.nextTartan: Morrison_LC",
"XTartan*Morrison_LC.sett: (D)(W) %b G10 R64 BK10 R14 BK10 R34 G30 W8 G18",
"XTartan*Morrison_LC.nextTartan: Mowat",
"XTartan*Mowat.sett: (D) %b B36 BK2 B4 BK36 Y4 G32 BK32",
"XTartan*Mowat.nextTartan: Munro",
"XTartan*Munro.sett: (D)(W) %b G4 CR4 G4 R32 B2 Y2 R6 B12 R6 Y2 B2 R6 G32 R6 B2 Y2 R48",
"XTartan*Munro.nextTartan: Munro_VS",
"XTartan*Munro_VS.sett: (W) %b W6 R64 BK36 R8 BK36",
"XTartan*Munro_VS.nextTartan: Murray",
"XTartan*Murray.sett: (W) %b B12 BK2 B2 BK2 B2 BK12 G12 R4 G12 BK12 B12 BK2 B4",
"XTartan*Murray.nextTartan: Murray_of_Atholl",
"XTartan*Murray_of_Atholl.sett: (D)(W) %b B24 BK4 B4 BK4 B4 BK24 G24 R6 G24 BK24 \
	B24 BK2 R6",
"XTartan*Murray_of_Atholl.nextTartan: Murray_of_Tullibardine",
"XTartan*Murray_of_Tullibardine.sett: (W) %b B4 R2 B2 R4 B8 R4 B2 R2 BK4 R2 B2 \
	R48 B24 R4 G4 R16 G24 R8 B4 R4 BK2",
"XTartan*Murray_of_Tullibardine.nextTartan: Napier",
"XTartan*Napier.sett: (D)(W) %b W2 B24 BK8 W4 BK4 W8 BK4 W4 BK4 W4 BK8",
"XTartan*Napier.nextTartan: Ogilvy",
"XTartan*Ogilvy.sett: (W) %b W4 AZ10 Y4 PU4 R12 W4 R12 W4 R12 BK4 Y10 AZ20 R6 AZ20",
"XTartan*Ogilvy.nextTartan: Ogilvy_Hunting",
"XTartan*Ogilvy_Hunting.sett: (W) %b R8 G6 BK2 G6 BK2 G32 BK16 Y4 B48",
"XTartan*Ogilvy_Hunting.nextTartan: Ogilvy_D",
"XTartan*Ogilvy_D.sett: (D) %b W2 AZ8 Y2 BK2 R12 W2 R8 W2 R12 BK2 Y2 AZ8 R2 AZ8",
"XTartan*Ogilvy_D.nextTartan: Ogilvy_VS",
"XTartan*Ogilvy_VS.sett: (W) %b R6 G4 BK2 G48 BK32 B4 Y2 B56",
"XTartan*Ogilvy_VS.nextTartan: Oliphant",
"XTartan*Oliphant.sett: (D)(W) %b G4 W2 G64 B48 BK8 B8",
"XTartan*Oliphant.nextTartan: Priest",
"XTartan*Priest.sett: (W) %b AZ4 BK16 AZ4 Lv8 AZ4 BK32 AZ4 BK32 Lv28 AZ4 BK4",
"XTartan*Priest.nextTartan: Prince_Charles_Cloak",
"XTartan*Prince_Charles_Cloak.sett: (W) %b R96 B6 Y2 G28 R16 B6 AZ8 W2",
"XTartan*Prince_Charles_Cloak.nextTartan: Ramsay",
"XTartan*Ramsay.sett: (D)(W) %b R6 PU2 R60 BK56 W4 BK8",
/*(W) gives CR2 for the second stripe */
"XTartan*Ramsay.nextTartan: Rattay",
"XTartan*Rattay.sett: (D) %b G142 BK8 R8 B18 R8 B8 R72 B8 W8",
"XTartan*Rattay.nextTartan: Robertson",
"XTartan*Robertson.sett: (W) %b R2 G2 R18 B2 R2 G18 R2 B18 R2 G2 R18 G2 R2",
"XTartan*Robertson.nextTartan: Robertson_of_Kindeace",
"XTartan*Robertson_of_Kindeace.sett: (W) %b W6 BK2 B24 BK24 G32 BK2 R6 BK2 G32 BK24 \
	B4 BK4 B4 BK4 B24",
"XTartan*Robertson_of_Kindeace.nextTartan: Robertson_D",
"XTartan*Robertson_D.sett: (D) %b R10 G4 R60 B6 R6 G60 R6 B60 R6 B6 R60 G4 R10",
"XTartan*Robertson_D.nextTartan: Rob_Roy",
"XTartan*Rob_Roy.sett: (C) %b BK8 R8",
"XTartan*Rob_Roy.nextTartan: Royal_Canadian_Air_Force",
"XTartan*Royal_Canadian_Air_Force.sett: (W) Mn4 AZ6 BK1 Mn2 BK1 AZ14 BK3 W3 BK2 Mn2 \
	BK2 Mn2 B4 Mn2 B6 Mn2 B6 Mn3",
"XTartan*Royal_Canadian_Air_Force.nextTartan: Rose",
"XTartan*Rose.sett: (W) G4 R56 B12 R10 B4 R4 B4 R22 W4",
"XTartan*Rose.nextTartan: Rose_Hunting",
"XTartan*Rose_Hunting.sett: (D)(W) %b BK8 W2 G20 BK20 B20 R4",
"XTartan*Rose_Hunting.nextTartan: Rose_VS",
"XTartan*Rose_VS.sett: (W) %b G8 R64 PU18 CR12 PU4 CR6 PU4 CR24 W6",
"XTartan*Rose_VS.nextTartan: Ross",
"XTartan*Ross.sett: (D)(W) %b G36 R4 G36 R36 G4 R8 G4 R36 B36 R4 B36 R36 B2 R2 \
	B4 R2 B2 R36 B2 R2 B4 R2 B2 R36 G36 R4 G36",
"XTartan*Ross.nextTartan: Ross_Hunting",
"XTartan*Ross_Hunting.sett: (W) %b R2 G4 R2 G24 BK4 G4 BK4 G6 LG2 G2 LG2 G4 LG8 G4",
"XTartan*Ross_Hunting.nextTartan: Ruthven",
"XTartan*Ruthven.sett: (D) %b R4 G2 R60 B36 G30 W6",
"XTartan*Ruthven.nextTartan: Scott",
"XTartan*Scott.sett: (D)(W) %b G8 R6 BK2 R56 G28 R8 G8 W6 G8 R8",
"XTartan*Scott.nextTartan: Scrymgeour",
"XTartan*Scrymgeour.sett: (D) %b Y90 BK6 R12 B18 Y12 BK6 R90",
"XTartan*Scrymgeour.nextTartan: Seton",
"XTartan*Seton.sett: (D)(W) %b R4 G2 R64 BK8 R4 B8 R8 G24 W2 G12",
"XTartan*Seton.nextTartan: Shaw",
"XTartan*Shaw.sett: (D)(W) %b AZ10 BK2 R60 PU30 R16 G60 R16 PU4",
/*(W) uses scarlett instead of red, light blue instead of azure */
/*(D) calls it Shaw_Red */
"XTartan*Shaw.nextTartan: Shepherd",
"XTartan*Shepherd.sett: (C) BK1 W1",
"XTartan*Shepherd.nextTartan: Sinclair",
"XTartan*Sinclair.sett: (D) %b R60 G24 BK10 W4 AZ12 R60",
"XTartan*Sinclair.nextTartan: Sinclair_Dress",
"XTartan*Sinclair_Dress.sett: (W) %b R56 G32 BK8 W2 AZ12 R56",
"XTartan*Sinclair_Dress.nextTartan: Sinclair_Hunting",
"XTartan*Sinclair_Hunting.sett: (W) %b G4 R2 G60 BK32 W2 B32 R4",
"XTartan*Sinclair_Hunting.nextTartan: Skene",
"XTartan*Skene.sett: (W) %b B2 R6 G18 R6 B2 R6 B18",
"XTartan*Skene.nextTartan: Skene_D",
"XTartan*Skene_D.sett: (D) %b G4 R12 G36 R12 G4 R12 B18",
"XTartan*Skene_D.nextTartan: Skene_N",
"XTartan*Skene_N.sett: (W) %b BK8 R6 G48 BK8 Or6 BK8 G48 BK8 R6 BK8 B48 BK8",
"XTartan*Skene_N.nextTartan: Stewart_Dress",
"XTartan*Stewart_Dress.sett: (D) %b W72 B8 BK12 Y2 BK2 W2 BK2 G16 R8 BK2 R4 W2",
/*(W) shows a picture of Stewart Dress but gives no thread count! */
"XTartan*Stewart_Dress.nextTartan: Stewart_Hunting",
"XTartan*Stewart_Hunting.sett: (W) %b \
	G4 B14 BK2 B2 BK2 B2 BK6 G22 R4(alternate Y) G22 BK6 G4 BK12 G4(pivot) \
	BK12 G4 BK6 G22 Y4(alternate R) G22 BK6 B2 BK2 B2 BK2 B14 G4",
"XTartan*Stewart_Hunting.nextTartan: Stewart_Hunting_D",
"XTartan*Stewart_Hunting_D.sett: (D) %b \
	G4 B6 BK2 B2 BK2 B2 BK8 G24 R4(alternate Y) G24 BK6 G4 BK12 G4(pivot) \
	BK12 G4 BK6 G24 Y4(alternate R) G24 BK8 B2 BK2 B2 BK2 B6 G4",
"XTartan*Stewart_Hunting_D.nextTartan: Stewart_Hunting_Early",
"XTartan*Stewart_Hunting_Early.sett: (W) %b \
	G8 B18 BK6 B6 BK16 G54 R8(alternate Y) G54 BK16 G10 BK26 G8(pivot) \
	BK26 G10 BK16 G54 Y8(alternate R) G54 BK16 B6 BK6 B18 G8",
"XTartan*Stewart_Hunting_Early.nextTartan: Stewart_Old",
"XTartan*Stewart_Old.sett: (W) %b G24 BK2 B4 BK2 G24 R4 BK24 R2 BK24 R4 B24 \
	BK2 G4 BK2 G4 BK2 B24",
"XTartan*Stewart_Old.nextTartan: Stewart_Royal",
"XTartan*Stewart_Royal.sett: (D) %b R72 B8 BK12 Y2 BK2 W2 BK2 G16 R8 BK2 R4 W2",
"XTartan*Stewart_Royal.nextTartan: Stewart_of_Appin",
"XTartan*Stewart_of_Appin.sett: (D)(W) %b G4 R4 AZ2 B4 R48 G4 R4 B16 R4 G4 R8 \
	G48 R4 AZ2 B4 R6",
"XTartan*Stewart_of_Appin.nextTartan: Stewart_of_Atholl",
"XTartan*Stewart_of_Atholl.sett: (D)(W) %b R12 BK2 R40 BK16 G6 BK2 G4 BK2 G44",
"XTartan*Stewart_of_Atholl.nextTartan: Stuart_of_Bute",
"XTartan*Stuart_of_Bute.sett: (D)(W) %b W4 R48 BK12 G2 BK2 G4 BK2 G12 R24",
"XTartan*Stuart_of_Bute.nextTartan: Sutherland",
"XTartan*Sutherland.sett: (D)(W) %b G12 W4 G48 BK24 B6 BK4 B4 BK4 B24 R2 B2 R6",
"XTartan*Sutherland.nextTartan: Urquhart",
"XTartan*Urquhart.sett: (W) %b R4 G6 BK6 G96 BK48 B16 BK6 B6 BK6 B48 W4 B8",
"XTartan*Urquhart.nextTartan: Urquhart_D",
"XTartan*Urquhart_D.sett: (D)(W) %b BK4 G18 BK12 B2 BK2 B2 BK2 B12 R6",
"XTartan*Urquhart_D.nextTartan: Urquhart_L",
"XTartan*Urquhart_L.sett: (W) %b G2 BK2 G16 BK16 B16 R2 B16 BK16 G2 BK2 G2 BK2 G16",
"XTartan*Urquhart_L.nextTartan: Wallace",
"XTartan*Wallace.sett: (D)(W) %b BK2 R16 BK16 Y2",
"XTartan*Wallace.nextTartan: Wallace_Hunting",
"XTartan*Wallace_Hunting.sett: (D)(W) %b BK2 G16 BK16 Y2",
"XTartan*Wallace_Hunting.nextTartan: Wallace_Blue",
"XTartan*Wallace_Blue.sett: (D)(W) %b BK2 B16 BK16 Y2",
"XTartan*Wallace_Blue.nextTartan: Wemyss",
"XTartan*Wemyss.sett: (D)(W) %b R8 G2 R48 BK8 R8 BK24 W2 BK24 R8",
"XTartan*Wemyss.nextTartan: Rainbow1",
/*and a few test patterns */
"XTartan*Rainbow1.sett: %a BK4 R4 G4 B4 Y4 PU4 Cy4 W4",
"XTartan*Rainbow1.nextTartan: Rainbow2",
"XTartan*Rainbow2.sett: %a BK4 R4 G4 B4 Y4 PU4 Cy4 W4 \
	%v %a LG4 Cor4 ForGr4 SlB4 Glr4 Mar4 Trq4 Wh4",
"XTartan*Rainbow2.nextTartan: Usa",
"XTartan*Usa.sett: %a R4 W4 B4 %v R2 W2 B2 Y2",
"XTartan*Usa.nextTartan : Mine",
"XTartan*colorCode.StB: Steel Blue",
"XTartan*colorCode.OL:  OldLace",
"XTartan*Mine.sett : (Experience)\
   OL28 Wh2  OL2 Wh2  OL2 Wh4  Mn2 Wh2  Mn2 Wh2  Mn4 StB2 Mn2 StB2 Mn2 StB8 BK40 \
%v OL28 StB2 OL2 StB2 OL2 StB4 Mn2 StB2 Mn2 StB2 Mn4 StB2 Mn2 StB2 Mn2 StB8 BK40",

0,

};

/*End XTartan.ad */

#endif
