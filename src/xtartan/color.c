/* color.c - functions dealing with colors
 * by Jim McBeath (jimmc@hisoft.uucp)
 *
 *  7.Jan.88  jimmc  Initial definition (X10)
 * 24.Oct.89  jimmc  Convert to X11, Xt; general restructuring
 *  9.Jan.91  jimmc  v2.0: Use resources to do color translations
 */

#include <stdio.h>
#include <X11/Intrinsic.h>
#include "tartan.h"		/* for dark resource from Rdata */
typedef struct _cinfo {		/* info about one color */
	struct _cinfo *next;
	char *name;
	char *longname;
	int pixel;
	int darkpixel;
	int fadepixel;
	int fadedarkpixel;
} Cinfo;

extern char *GetSubResource();

extern Display *TDisplay;
extern Widget TTopWidget;
extern Colormap TColormap;

Cinfo *colorinfo;

#define FADE(x) ((x / Rdata.fadeValue) + ((Rdata.fadeValue-1)*65535/Rdata.fadeValue))

int			/* returns the pixel value */
colorPixel(color)
char *color;		/* the color code */
{
	Cinfo *p;
	char *tcolor;
	int t,pixel,dpixel, fpixel, fdpixel;
	XColor xdef, sdef;
	XColor fdef, fddef; /* fade definition, fade dark definition */

	/* see if we have already translated this color */
	for (p=colorinfo; p; p=p->next) {
		if (strcmp(p->name,color)==0)
			return (Rdata.dark ? 
				(Rdata.fade ? p->fadedarkpixel : p->darkpixel) :
				(Rdata.fade ? p->fadepixel : p->pixel));
	}
	/* we have not seen it before, make the translation */
	pixel = 0;	/* we use 0 on any errors */
	tcolor = GetSubResource(TTopWidget,"colorCode",color);
	if (!tcolor) {
		Warn("No color translation for code %s",color);
		tcolor = "<No translation>";
	} else {
		t = XLookupColor(TDisplay,TColormap,tcolor,&xdef,&sdef);
		if (t==0) {
			Warn("Error getting definition for color %s, code %s",
				tcolor, color);
		} else {
			t = XAllocColor(TDisplay,TColormap,&xdef);
			if (t==0) {
				Warn("Can't allocate color %s, code %s",
					tcolor, color);
			} else {
				pixel = xdef.pixel;
			}
			/* darken whites/greys */
			if (xdef.red == xdef.blue && xdef.blue == xdef.green)
			{
				fdef.red = FADE(xdef.red);
				fdef.blue = FADE(xdef.blue);
				fdef.green = FADE(xdef.green);

				xdef.red = (xdef.red * 2) / 3;
				xdef.blue = (xdef.blue * 2) / 3;
				xdef.green = (xdef.green * 2) / 3;
				fddef.red = FADE(xdef.red);
				fddef.blue = FADE(xdef.blue);
				fddef.green = FADE(xdef.green);
			}
			/* darken yellows */
			else if (xdef.red == xdef.green && xdef.blue == 0)
			{
				fdef.red = FADE(xdef.red);
				fdef.blue = FADE(xdef.blue);
				fdef.green = FADE(xdef.green);
				xdef.red = (xdef.red * 2) / 3;
				xdef.green = (xdef.green * 2) / 3;
				fddef.red = FADE(xdef.red);
				fddef.blue = FADE(xdef.blue);
				fddef.green = FADE(xdef.green);
			}
			/* darken reds */
			else if (xdef.blue == 0 && xdef.green == 0)
			{
				fdef.red = FADE(xdef.red);
				fdef.blue = FADE(xdef.blue);
				fdef.green = FADE(xdef.green);
				xdef.red = (xdef.red * 2) / 3;
				fddef.red = FADE(xdef.red);
				fddef.blue = FADE(xdef.blue);
				fddef.green = FADE(xdef.green);
			}
			else
			/* darken others */
			{
				fdef.red = FADE(xdef.red);
				fdef.blue = FADE(xdef.blue);
				fdef.green = FADE(xdef.green);
				xdef.red /= 2;
				xdef.blue = (xdef.blue * 2) / 5;
				xdef.green /= 2;
				fddef.red = FADE(xdef.red);
				fddef.blue = FADE(xdef.blue);
				fddef.green = FADE(xdef.green);
			}
			fdef.pixel = 0;
			xdef.pixel = 0;
			fddef.pixel = 0;
			t = XAllocColor(TDisplay,TColormap,&xdef);
			if (t==0) {
				Warn("Can't allocate dark color %s, code %s",
					tcolor, color);
			} else {
				dpixel = xdef.pixel;
			}

			t = XAllocColor(TDisplay,TColormap,&fdef);
			if (t==0) {
				Warn("Can't allocate fade color %s, code %s",
					tcolor, color);
			} else {
				fpixel = fdef.pixel;
			}

			t = XAllocColor(TDisplay,TColormap,&fddef);
			if (t==0) {
				Warn("Can't allocate faded dark color %s, code %s",
					tcolor, color);
			} else {
				fdpixel = fddef.pixel;
			}
		}
	}
	p = (Cinfo *)XtMalloc(sizeof(Cinfo));
	p->name = XtMalloc(strlen(color)+1);
	(void)strcpy(p->name,color);
	p->longname = XtMalloc(strlen(tcolor)+1);
	(void)strcpy(p->longname,tcolor);
	p->pixel = pixel;
	p->darkpixel = dpixel;
	p->fadepixel = fpixel;
	p->fadedarkpixel = fdpixel;
	p->next = colorinfo;
	colorinfo = p;
	return (Rdata.dark ? 
		(Rdata.fade ? p->fadedarkpixel : p->darkpixel) :
		(Rdata.fade ? p->fadepixel : p->pixel));
}

char *
colorLongname(color)
char *color;		/* the color code */
{
	Cinfo *p;

	(void)colorPixel(color);	/* load it if not already loaded */
	for (p=colorinfo; p; p=p->next) {
		if (strcmp(p->name,color)==0)
			return p->longname;
	}
	return "<no translation>";	/* should never happen */
}

void dumpColors()
{
	Cinfo *p;

	for (p=colorinfo; p; p=p->next) {
		(void)printf("%s: %s\n", p->name, p->longname);
	}
}

/* end */
