#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <stdio.h>

#ifdef HAS_GD
#include "gd.h"

/* CreateImageFromPixmap */
/* flagrantly stolen from the Xpm library, subject to its copyright below */
/* XPM copyright:
 * Copyright (C) 1989-95 GROUPE BULL
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * GROUPE BULL BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name of GROUPE BULL shall not be
 * used in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization from GROUPE BULL.
*/
void
CreateImageFromPixmap(display, pixmap, ximage_return, width, height)
    Display *display;
    Pixmap pixmap;
    XImage **ximage_return;
    unsigned int *width;
    unsigned int *height;
{
    unsigned int dum;
    int dummy;
    Window win;

    if (*width == 0 && *height == 0)
        XGetGeometry(display, pixmap, &win, &dummy, &dummy,
                     width, height, &dum, &dum);

    *ximage_return = XGetImage(display, pixmap, 0, 0, *width, *height,
                               AllPlanes, ZPixmap);
}

void CreateGifFromXImage(char *filename, Display *display, 
	Colormap cmap, XImage *ximage)
{
	FILE *fd;
	XColor cols[256];
	int gdallocated[256];
	int w,h,x,y,i;
	unsigned char p;
	gdImagePtr gd;

	fd = fopen(filename,"wb");
	if (!fd) return;
	
	/* init cmap request */
	/* built for pseudocolor -- may fail with truecolor/directcolor */
	for (i = 0; i < 256; i++) 
	{
		cols[i].pixel = i;
		gdallocated[i] = -1;
	}
	XQueryColors(display,cmap,cols,256);

	w = ximage->width;
	h = ximage->height;

	/* now create the gif */
	gd = gdImageCreate(w,h);

        for (y = 0; y < h; y++)
	for (x = 0; x < (w-1); x++)
	  {
		i = (y * ximage->bytes_per_line) + x;
		p = ximage->data[i]; /* returns between 0 and 256 */
	  	if (gdallocated[p] == -1) /* not allocated */
	  	    gdallocated[p] = gdImageColorAllocate(gd,
	  	    	cols[p].red / 256,
	  	    	cols[p].green / 256,
	  	    	cols[p].blue / 256);
		gdImageSetPixel(gd,x,y,gdallocated[p]);
	  }
	gdImageGif(gd,fd);
	fclose(fd);
	gdImageDestroy(gd);
}
#endif
