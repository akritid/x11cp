/* main.c - main module for xtartan
 * by Jim McBeath (jimmc@hisoft.uucp, jimmc@netcom.com)
 *
 *  7.Jan.88  jimmc  Initial definition (X10)
 * 24.Oct.89  jimmc  Convert to X11, Xt; general restructuring
 *  9.Jan.91  jimmc  V2.0: use XTartan resource file
 */

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Xatom.h>

#ifdef INCLUDE_VROOT
#include <X11/vroot.h>
#endif

#include <stdio.h>
#include "tartan.h"
#include "patchlevel.h"

char *Progname;
Display *TDisplay;
Screen *TScreen;
GC Tgc;
Colormap TColormap;
Window TWindow, TRootWindow;
XtAppContext TAppCtx;
Widget TTopWidget;
Drawable TDrawable;
char *NamePattern;
char *SettPattern;

Dimension origWidth, origHeight;

#include "tartcursor.h"	/* define our cursor */

static char *version=VERSION;	/* from patchlevel.h */

/* define global resource data declared in tartan.h */
RInfo Rdata;

static XrmOptionDescRec options[] = {
  {"-draw",	"action",	XrmoptionNoArg,  "draw"},
  {"-list",	"action",	XrmoptionNoArg,  "list"},
  {"-print",	"action",	XrmoptionNoArg,  "print"},
  {"-version",	"action",	XrmoptionNoArg,  "version"},
  {"-r",		"useRoot",	XrmoptionNoArg,  "TRUE" },
  {"-s",		"scale",	XrmoptionSepArg, NULL },
  {"-t",		"tartan",	XrmoptionSepArg, NULL },
  {"-w",		"lineWidth",	XrmoptionSepArg, NULL },
  {"-np",		"namePattern",	XrmoptionSepArg, NULL },
  {"-sp",		"settPattern",	XrmoptionSepArg, NULL },
  {"-dark",	"dark",		XrmoptionNoArg, "TRUE" },
  {"-modern",	"dark",		XrmoptionNoArg, "TRUE" },
  {"-min",	"showMinimum",	XrmoptionNoArg, "TRUE" },
  {"-minimum",	"showMinimum",	XrmoptionNoArg, "TRUE" },
  {"-fade",	"fade",		XrmoptionNoArg, "TRUE" },
  {"-fv",		"fadeValue",	XrmoptionSepArg, NULL },
};

static XtResource resources[] = {
  {"action", "Action", XtRString, sizeof(String),
   XtOffset(RInfoPtr,action), XtRString, "draw"},
  {"lineWidth", "LineWidth", XtRInt,    sizeof(int),
   XtOffset(RInfoPtr,lwidth), XtRImmediate, (caddr_t)2},
  {"scale", "Scale", XtRInt,    sizeof(int),
   XtOffset(RInfoPtr,scale), XtRImmediate, (caddr_t)2},
  {"tartan", "Tartan", XtRString, sizeof(String),
   XtOffset(RInfoPtr,tartanname), XtRString, "MacBeth"},
  {"useRoot", "UseRoot", XtRBool, sizeof(Bool),
   XtOffset(RInfoPtr,useroot), XtRBool, 0},
  {"dark", "Dark", XtRBool, sizeof(Bool),
   XtOffset(RInfoPtr,dark), XtRBool, 0},
  {"fade", "Fade", XtRBool, sizeof(Bool),
   XtOffset(RInfoPtr,fade), XtRBool, 0},
  {"fadeValue", "FadeValue", XtRInt, sizeof(int),
   XtOffset(RInfoPtr,fadeValue), XtRImmediate, (XtPointer)2},
  {"namePattern", "NamePattern", XtRString, sizeof(String),
   XtOffset(RInfoPtr,namepattern), XtRString, ""},
  {"settPattern", "SettPattern", XtRString, sizeof(String),
   XtOffset(RInfoPtr,settpattern), XtRString, ""},
  {"showMinimum", "ShowMinimum", XtRBool, sizeof(Bool),
   XtOffset(RInfoPtr,minimum), XtRBool, 0},
};

extern String FALLBACKS[];

int main(int argc,char *argv[])
{
  char buf[1000];

  Progname = strrchr(argv[0],'/');
  if (Progname) Progname++; 
  else Progname=argv[0];

  TTopWidget = XtAppInitialize(&TAppCtx,"XTartan",options,XtNumber(options),
			       &argc, argv, FALLBACKS, NULL, 0);

  XtVaGetValues(TTopWidget,
		XtNwidth, &origWidth,
		XtNheight, &origHeight,
		NULL);

  if (argc!=1) Usage(1);
  TDisplay = XtDisplay(TTopWidget);

  initActions(TAppCtx);

  XtGetApplicationResources(TTopWidget,(char *)&Rdata,
			    resources,XtNumber(resources),
			    (ArgList)NULL,(Cardinal)0);

  if (Rdata.fadeValue <= 1) Rdata.fadeValue = 2;

  NamePattern = Rdata.namepattern;
  SettPattern = Rdata.settpattern;

  readTartans();		/* load up all the tartan info */

  if (!anyTartanMatches()) {
    Fatal("No tartans matching specified patterns\n");
    exit(1);
  }

  if (!setTartan(Rdata.tartanname)) {
    Warn("Can't find tartan %s",Rdata.tartanname);
    ListTartans();
    exit(1);
  }
  setScaleWidth(Rdata.scale,Rdata.lwidth);

  if (strcmp(Rdata.action,"version")==0) {
    (void)printf("%s\n",version);
    exit(0);
  }

  if (strcmp(Rdata.action,"list")==0) {
    ListTartans();
    exit(0);
  }

  if (strcmp(Rdata.action,"print")==0) {
    PrintTartan();
    exit(0);
  }

  if (strcmp(Rdata.action,"draw")!=0) {
    Fatal("Bad action %s",Rdata.action);
  }

  (void)sprintf(buf,"%s %s",Progname,Rdata.tartanname);
  initDisplay(TTopWidget,buf,Rdata.useroot);

  (void)setTartan("");	/* update now that we have the window */

  if (Rdata.useroot) {
    setrootbg(TDisplay,TScreen,TRootWindow,TDrawable);
  } else {
    /* for non-root window, let user enter commands */
    XtAppMainLoop(TAppCtx);
    /* NOTREACHED */
  }
  XCloseDisplay(TDisplay);
  exit(0);
  return 1;
}

void initDisplay(top,winname,rflag)
     Widget top;
     char *winname;
     int rflag;
{
  Pixmap bm,mbm;
  Cursor tart_cursor;
  XColor fgc, bgc;
  XWMHints hints;

  TScreen = DefaultScreenOfDisplay(TDisplay);
  TColormap = DefaultColormapOfScreen(TScreen);
  Tgc = DefaultGCOfScreen(TScreen);
  TRootWindow = DefaultRootWindow(TDisplay);
  if (rflag) {
    TWindow = TRootWindow;
    return;
  }
  XtSetMappedWhenManaged(top,0);
  XtRealizeWidget(top);
  TWindow = XtWindow(top);

  /* get the window manager to accept input here */
  hints.flags = InputHint;
  hints.input = True;
  XSetWMHints(TDisplay, TWindow, &hints);

  XStoreName(TDisplay,TWindow,winname);
  bm = XCreateBitmapFromData(TDisplay,TWindow,tartcursor_bits,
			     tartcursor_width,tartcursor_height);
  mbm = XCreateBitmapFromData(TDisplay,TWindow,tartmask_bits,
			      tartmask_width,tartmask_height);
  fgc.pixel = BlackPixelOfScreen(TScreen);
  bgc.pixel = WhitePixelOfScreen(TScreen);
  XQueryColor(TDisplay,TColormap,&fgc);
  XQueryColor(TDisplay,TColormap,&bgc);
  tart_cursor = XCreatePixmapCursor(TDisplay,bm,mbm,&fgc,&bgc,
				    tartcursor_x_hot,tartcursor_y_hot);
  if (!tart_cursor) Warn("can't make cursor");
  else XDefineCursor(TDisplay,TWindow,tart_cursor);
  XtMapWidget(top);
  XtSetMappedWhenManaged(top,1);
}

void Usage(x)
     int x;		/* exit code (-1 means return) */
{
  (void)printf("usage: %s  [-draw] [-list] [-print] [std Xt args]\n"
	       "[-r] [-s scale] [-t tartanname] [-w linewidth]\n"
	       "[-np namepattern] [-sp settpattern] [-dark] [-fade]\n",
	       Progname);
  (void)printf("-draw     draw the specified tartan (default)\n");
  (void)printf("-list     print a list of available tartans\n");
  (void)printf("-print    print out info about the specified tartan\n");
  (void)printf("-r  use root window\n");
  (void)printf("-s  provide an expansion scale factor\n");
  (void)printf("-t  specify the tartan name to use\n");
  (void)printf("-w  provide a line width factor\n");
  (void)printf("-np   use only tartans with specified pattern in name\n");
  (void)printf("-sp   use only tartans with specified pattern in sett\n");
  (void)printf("-dark  display the darker modern-style colors\n");
  (void)printf("-fade  display faded colors for backgrounds (e.g., WWW)\n");
  (void)printf("-fv provide a fade factor ( > 0 ; default 2)\n");
  if (x<0) return;
  exit(x);
}

/* end */

