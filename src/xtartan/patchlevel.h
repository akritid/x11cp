/* patchlevel.h for xtartan
 */

#define VERSION "xtartan version 2.3  April 27, 1996"
/* by Jim McBeath (jimmc@hisoft.uucp, jimmc@netcom.com) */
/* modifications added by Joe Shelby (jshelby@autometric.com) */
/*
 * v1 posted November 1989
 *
 * patch1, version 1.1, December 1989:
 *   add extra braces to TI*.h files to conform to ANSI C;
 *   change Imakefile to "ComplexProgramTarget".
 *
 * version 2.0, February 1991:
 *   Use XTartan resource file for sizes, translations, color mappings, and
 *   tartan (sett) definitions.  There is no longer any sett info compiled
 *   into the program, so you can add new tartans (or replace exising tartans)
 *   without recompiling.
 *
 * version 2.1, January 1995:
 *  now accepts input when using window managers other than mwm
 *  supports xpm file format
 *    new action "x" dumps xpm file of current tartan (name = tartan name)
 *  now has "dark" mode for "modern" darker colors as opposed to the default
 *    "ancient" colors.
 *    2 new command line options "-dark" and "-modern" to enable
 *    new actions "d" to toggle
 *    xpm file of dark tartan named by tartan name + "_dark"
 *
 * version 2.2, March 1995
 *  showMinimum resource and action added
 *  faded colors for www pages
 *  better line-width algorithm
 *  new naming scheme for .xpm files.
 *
 * version 2.3, April 1996
 *  gif file support using GD library.
 *   new action "g" to dump gif file, using same naming scheme as xpm
 *  XTartan.c -- app-defaults now installed as fallbacks
 *   eliminates need to install app-default file.
 *  Interactive Root-Window action
 *  
 *
 */

/* end */
