/* setrootbg.c - makes the current root background permanent.
 * A modularized version of the function from the xsetroot program.
 *
 * 24.Oct.89  jimmc  Initial definition (borrowed from xsetroot)
 *  9.Jan.91  jimmc  Pass dpy info as args instead of globals
 */

#include <X11/Xlib.h>
#include <X11/Xatom.h>

#define Dynamic 1

void setrootbg(TDisplay,TScreen,TRootWindow,TDrawable)
Display *TDisplay;
Screen *TScreen;
Window TRootWindow;
Drawable TDrawable;
{
	Atom prop,type;
	int format;
	unsigned long length,after;
	unsigned char *data;

	/* for root window, get rid of old root pixmap and colors
	 * and save new pixmap and colors.
	 * (this code borrowed from xsetroot)
	 */
	if (DefaultVisualOfScreen(TScreen)->class & Dynamic) {
		prop = XInternAtom(TDisplay,"_XSETROOT_ID",False);
		XGetWindowProperty(TDisplay,TRootWindow,prop,0L,1L,True,
			AnyPropertyType,
			&type,&format,&length,&after,&data);
		if ((type == XA_PIXMAP) && (format == 32) &&
		    (length == 1) && (after == 0))
			XKillClient(TDisplay, *((Pixmap *)data));
				/* zap old colors */
		else if (type != None)
			Warn("_XSETROOT_ID property is garbage");
		/* save colors used in pixmap */
		XChangeProperty(TDisplay,TRootWindow,prop,XA_PIXMAP,32,
			PropModeReplace,(unsigned char *)&TDrawable,1);
		XSetCloseDownMode(TDisplay, RetainPermanent);
	}
}

/* end */
