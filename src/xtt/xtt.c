/*
 *	xtt.c -	simple timetable display program
 *      
 * This software comes with NO warranty whatsoever. I therefore take no
 * responsibility for any damages, losses or problems caused through use
 * or misuse of this program.
 *
 * I hereby grant permission for this program to be freely copied and
 * distributed by any means, provided that this and all other copyright
 * notices remain unchanged. However, this program and all its code still
 * belong to me, and I reserve the right to be identified as the author of
 * it.
 *
 * Jan 1995.
 * Matthew Chapman, csuoq@csv.warwick.ac.uk.
 *
 */

/*#define DEBUG*/

#include <stdio.h>

#ifdef DOLPHIN
#include <sys/types.h>
#endif

#include <sys/file.h>
#include <time.h>
#include <string.h>

#ifdef DOLPHIN
#include <unistd.h>
#endif

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <X11/Xaw/Command.h>
#include <X11/Xaw/Form.h>
#include <X11/Xaw/Label.h>
#include <X11/Xatom.h>
#include <X11/cursorfont.h>
#include <X11/xpm.h>

extern getenv();

#define SETBG(x) XtVaTypedArg,XtNbackground,XtRString,x,strlen((char *)x)+1

void alloc_colours();
void load_pixmap(char **dname, Pixmap *xpm,Widget top);
void set_icon_pixmap(char **dname,Widget top);
void acceptQuitPre(Widget top);
void acceptQuitPost(Widget top);
void fit_on_screen(Widget top,Position *x,Position *y, int w, int h,
        int horizB,int vertB);

/* location of timetable file, relative to $HOME/ */
#define XTTFILE ".xtt"

/* xpms for icon and background */
#include "xtt.xpm"
#define ICON xtt_xpm
#include "wood.xpm"
#define BGXPM wood_xpm
#include "about.xpm"
#define ABOUTXPM about_xpm

#define DAYS 5
#define MAXSLOTS 20
#define ENTRY_LEN 40
#define SEPARATOR "/"
#define EMPTY '-'
#define DEFAULT "sandybrown"
#define CHECK_DELAY 1000*60*60  /* 1 hour in milliseconds */

static char *daynames[] = { "Monday","Tuesday","Wednesday","Thursday","Friday" };

void readtable();
void DoDay();
int WhatsTheDay();
void StartCheck();
void HourlyCheck();
void AboutPopup();
void PopDown();
void Prev();
void Next();
void Quit();

struct AppData {
	int slots,hour,minutes,warning;
	Boolean clock12,nowarn,mono;
	String file;
	Pixel bcol;
} app_data;

/* resource options */
static XrmOptionDescRec options[] =
{
	{ "-slots",		"*slots",		XrmoptionSepArg, NULL },
	{ "-hour",		"*hour",		XrmoptionSepArg, NULL },
	{ "-minutes",	"*minutes",		XrmoptionSepArg, NULL },
	{ "-12",		"*12",			XrmoptionNoArg,"True" },
	{ "-nowarn",	"*nowarn",		XrmoptionNoArg,"True" },
	{ "-warning",	"*warning",		XrmoptionSepArg, NULL },
	{ "-file",		"*file",		XrmoptionSepArg, NULL },
	{ "-mono",		"*mono",		XrmoptionNoArg,"True" },
	{ "-bcol",		"*bcol",		XrmoptionSepArg, NULL },
};

/* option types and default values */
static XtResource resources[] =
{
	{"slots","Slots",XtRInt,sizeof(int),XtOffsetOf(struct AppData,slots),
		XtRImmediate,(XtPointer) 9},
	{"hour","Hour",XtRInt,sizeof(int),XtOffsetOf(struct AppData,hour),
		XtRImmediate,(XtPointer) 9},
	{"minutes","Minutes",XtRInt,sizeof(int),XtOffsetOf(struct AppData,minutes),
		XtRImmediate,(XtPointer) 5},
	{"12","12",XtRBoolean,sizeof(Boolean),XtOffsetOf(struct AppData,clock12),
		XtRImmediate,(XtPointer) False},
	{"nowarn","Nowarn",XtRBoolean,sizeof(Boolean),XtOffsetOf(struct AppData,nowarn),
		XtRImmediate,(XtPointer) False},
	{"warning","Warning",XtRInt,sizeof(int),XtOffsetOf(struct AppData,warning),
		XtRImmediate,(XtPointer) 10},
	{"file","File",XtRString,sizeof(String),XtOffsetOf(struct AppData,file),
		XtRImmediate,(XtPointer) ""},
	{"mono","Mono",XtRBoolean,sizeof(Boolean),XtOffsetOf(struct AppData,mono),
		XtRImmediate,(XtPointer) False},
	{"bcol","Bcol",XtRPixel,sizeof(Pixel),XtOffsetOf(struct AppData,bcol),
		XtRString,"chocolate"},
};

/* fallback resources - the fonts may have to be changed */
String fallback_resources[] = {
	"*background:          white",
	"*font:                -b&h-lucidatypewriter-bold-r-*-*-12-*",
	"*title.justify:       left",
	"*title.font:          -*-times-*-r-*-*-20-*-*-*-*-*-*-*",
	"*title.horizDistance: 0",
	NULL,
};

static char course[DAYS][MAXSLOTS][ENTRY_LEN+1];
static char locate[DAYS][MAXSLOTS][ENTRY_LEN+1];
static char colour[DAYS][MAXSLOTS][ENTRY_LEN+1];
XtAppContext app_context;
Widget topLevel,box,title,next,prev,quit,about;
Widget wtime[MAXSLOTS],wtext[MAXSLOTS],wlocn[MAXSLOTS];
Widget pshell,pform,ptitle,ptime,ptext,plocn,dismiss;
Widget ashell,aform,adismiss,aboutpic;
int curr_day,today,maxtextlen=0,maxlocnlen=0;
Pixmap bgxpm,aboutxpm;

int main(int argc, char **argv)
{
	int i,d;

	topLevel = XtVaAppInitialize(&app_context,"XTT",options,XtNumber(options),
		&argc,argv,fallback_resources,NULL);

	XtVaGetApplicationResources(topLevel,&app_data,resources,
    			XtNumber(resources),NULL);

	if (argc != 1)
	{
		fprintf(stderr,"Usage: %s [toolkit options] [options]\n",argv[0]);
		fprintf(stderr,"  Options are:\n");
		fprintf(stderr,"    [-slots <number>]  number of entries per day (default: 9)\n");
		fprintf(stderr,"    [-hour <hour>]     hour of first slot (default: 9)\n");
		fprintf(stderr,"    [-minute <mins>]   offset from the hour of each slot (default: 5)\n");
		fprintf(stderr,"    [-12]              switch to 12 hour clock (default: 24 hour clock)\n");
		fprintf(stderr,"    [-nowarn]          disable warnings before lectures (default: enabled)\n");
		fprintf(stderr,"    [-warning <mins>]  time gap between warning and lecture (default: 10)\n");
		fprintf(stderr,"    [-file <xtt file>] alternative file to ~/.xtt, to view other timetables\n");
		fprintf(stderr,"    [-mono]            reduced colour mode - automatically set if screen depth\n                       is 1 - i.e. a black and white display\n");
		fprintf(stderr,"    [-bcol <colour>]   colour of buttons - automatically set to white if\n                       the -mono option is used\n");
		return(1);
	}
	if ((app_data.slots<=0) || (app_data.hour<0) || (app_data.minutes<0))
	{
		fprintf(stderr,"Error: Out of range option\n");
		return(1);
	}
	if (app_data.slots>MAXSLOTS)
	{
		fprintf(stderr,"Error: The maximum number of slots is %d.\n",MAXSLOTS);
		return(1);
	}
	if (app_data.hour>23)
	{
		fprintf(stderr,"Error: The maximum allowable hour is 23.\n");
		return(1);
	}
	if (app_data.minutes>=60)
	{
		fprintf(stderr,"Error: The maximum number of minutes is 59.\n");
		return(1);
	}

	if (DefaultDepthOfScreen(XtScreen(topLevel))==1)
		app_data.mono=True;

	readtable();
	alloc_colours();
	if (app_data.mono)
		app_data.bcol = WhitePixelOfScreen(XtScreen(topLevel));
	
	box = XtVaCreateManagedWidget("box",formWidgetClass,topLevel,NULL);
	title = XtVaCreateManagedWidget("title",labelWidgetClass,box,XtNborderWidth,0,
		XtNlabel,"Wednesday",NULL); /* label to set to widest day name */	

	for (i=0; i<app_data.slots; i++)
	{
		wtime[i] = XtVaCreateManagedWidget("wtime",labelWidgetClass,box,NULL);
		wtext[i] = XtVaCreateManagedWidget("wtext",labelWidgetClass,box,
			XtNfromHoriz,wtime[i],XtNhorizDistance,-1,NULL);
		wlocn[i] = XtVaCreateManagedWidget("wlocn",labelWidgetClass,box,
			XtNfromHoriz,wtext[i],XtNhorizDistance,-1,NULL);
		if (i>0)
		{
			XtVaSetValues(wtime[i],XtNfromVert,wtime[i-1],XtNvertDistance,-1,
				NULL);
			XtVaSetValues(wtext[i],XtNfromVert,wtext[i-1],XtNvertDistance,-1,
				NULL);
			XtVaSetValues(wlocn[i],XtNfromVert,wlocn[i-1],XtNvertDistance,-1,
				NULL);
		}
		else
		{
			XtVaSetValues(wtime[i],XtNfromVert,title,NULL);
			XtVaSetValues(wtext[i],XtNfromVert,title,NULL);
			XtVaSetValues(wlocn[i],XtNfromVert,title,NULL);
		}
	}

	prev = XtVaCreateManagedWidget("Prev",commandWidgetClass,box,	
		XtNfromVert,wtime[app_data.slots-1],XtNbackground,app_data.bcol,NULL);
	next = XtVaCreateManagedWidget("Next",commandWidgetClass,box,
		XtNfromVert,wtime[app_data.slots-1],XtNfromHoriz,prev,
		XtNbackground,app_data.bcol,NULL);	
	about = XtVaCreateManagedWidget("About",commandWidgetClass,box,
		XtNfromVert,wtime[app_data.slots-1],XtNfromHoriz,next,
		XtNbackground,app_data.bcol,NULL);
	quit = XtVaCreateManagedWidget("Quit",commandWidgetClass,box,
		XtNfromVert,wtime[app_data.slots-1],XtNfromHoriz,about,
		XtNbackground,app_data.bcol,NULL);	

	XtAddCallback(prev,XtNcallback,Prev,topLevel);
	XtAddCallback(next,XtNcallback,Next,topLevel);
	XtAddCallback(about,XtNcallback,AboutPopup,NULL);
	XtAddCallback(quit,XtNcallback,Quit,topLevel);

	/* the notify popup */
	pshell = XtVaCreatePopupShell("pshell",transientShellWidgetClass,topLevel,
		NULL);
	pform = XtVaCreateManagedWidget("pform",formWidgetClass,pshell,NULL);
	ptitle = XtVaCreateManagedWidget("ptitle",labelWidgetClass,pform,NULL);
	ptime = XtVaCreateManagedWidget("ptime",labelWidgetClass,pform,
		XtNfromVert,ptitle,NULL);
	ptext = XtVaCreateManagedWidget("ptext",labelWidgetClass,pform,
		XtNfromHoriz,ptime,XtNhorizDistance,-1,XtNfromVert,ptitle,NULL);
	plocn = XtVaCreateManagedWidget("plocn",labelWidgetClass,pform,
		XtNfromHoriz,ptext,XtNhorizDistance,-1,XtNfromVert,ptitle,NULL);
	dismiss = XtVaCreateManagedWidget("Dismiss",commandWidgetClass,pform,
		XtNfromVert,ptime,XtNbackground,app_data.bcol,NULL);
	XtAddCallback(dismiss,XtNcallback,PopDown,NULL);

	/* the about popup */
	{
		ashell = XtVaCreatePopupShell("ashell",transientShellWidgetClass,topLevel,
			NULL);
		aform = XtVaCreateManagedWidget("aform",formWidgetClass,ashell,NULL);
		aboutpic = XtVaCreateManagedWidget("aboutpic",labelWidgetClass,aform,
			XtNlabel,"",XtNwidth,189,XtNheight,136,NULL);
		adismiss = XtVaCreateManagedWidget("Dismiss",commandWidgetClass,aform,
			XtNfromVert,aboutpic,XtNbackground,app_data.bcol,NULL);
		XtAddCallback(adismiss,XtNcallback,PopDown,NULL);
	}
	
	curr_day = WhatsTheDay();	
	
	for (d=0; d<DAYS; d++)
		for (i=0; i<app_data.slots; i++)
			if (strlen(course[d][i]) > maxtextlen)
				maxtextlen=strlen(course[d][i]);	
	for (i=0; i<app_data.slots; i++)
		XtVaSetValues(wtext[i],XtNwidth,maxtextlen*8+5,NULL);

	for (d=0; d<DAYS; d++)
		for (i=0; i<app_data.slots; i++)
			if (strlen(locate[d][i]) > maxlocnlen)
				maxlocnlen=strlen(locate[d][i]);	
	for (i=0; i<app_data.slots; i++)
		XtVaSetValues(wlocn[i],XtNwidth,maxlocnlen*8+5,NULL);

	if (!app_data.mono)
	{
		set_icon_pixmap(ICON,topLevel);
		load_pixmap(BGXPM,&bgxpm,topLevel);
		XtVaSetValues(box,XtNbackgroundPixmap,(XtArgVal)bgxpm,NULL);
		XtVaSetValues(title,XtNbackgroundPixmap,(XtArgVal)bgxpm,NULL);
		XtVaSetValues(pform,XtNbackgroundPixmap,(XtArgVal)bgxpm,NULL);
		XtVaSetValues(aform,XtNbackgroundPixmap,(XtArgVal)bgxpm,NULL);
	}
	load_pixmap(ABOUTXPM,&aboutxpm,topLevel);
	XtVaSetValues(aboutpic,XtNbackgroundPixmap,(XtArgVal)aboutxpm,NULL);
	
	acceptQuitPre(topLevel);
	XtRealizeWidget(topLevel);
	acceptQuitPost(topLevel);

	DoDay(curr_day);
	if (!app_data.nowarn)
		StartCheck();
	
	XtAppMainLoop(app_context);

	return(0);
}

void DoDay()
{
	int i;
	int hour=app_data.hour,min=app_data.minutes;
	char txt[ENTRY_LEN+1],txt2[ENTRY_LEN+1],timestr[20];
	
	XtVaSetValues(title,XtNlabel,daynames[curr_day],NULL);
	for (i=0; i<app_data.slots; i++)
	{
		strncpy(txt,course[curr_day][i],ENTRY_LEN);
		txt[ENTRY_LEN]='\0';
#ifdef DEBUG
		printf("txt=%s.\n",txt);
#endif
		strncpy(txt2,locate[curr_day][i],ENTRY_LEN);
		txt2[ENTRY_LEN]='\0';
		sprintf(timestr,"%2d.%02d",hour,min);
		hour++;
		if ((hour==13) && (app_data.clock12))
			hour=1;
		XtVaSetValues(wtime[i],XtNlabel,timestr,SETBG(colour[curr_day][i]),NULL);
		XtVaSetValues(wtext[i],XtNlabel,txt,SETBG(colour[curr_day][i]),NULL);
		XtVaSetValues(wlocn[i],XtNlabel,txt2,SETBG(colour[curr_day][i]),NULL);
	}
}

void StartCheck()
{
	int sec=app_data.minutes*60,secnow;
	time_t now = time((time_t *)NULL);
	struct tm *tnow;
	unsigned long delay;

	sec-=app_data.warning*60;
	if (sec<0) sec+=3600;
	
	tnow = localtime(&now);
	secnow = (tnow->tm_min)*60 + (tnow->tm_sec);
	if (sec > secnow)
		delay=(sec-secnow)*1000;
	else
		delay=(sec - secnow + 60*60)*1000;
#ifdef DEBUG
	printf("Delay: %ds\n",(int)(delay/1000));
#endif
	XtAppAddTimeOut(app_context,delay,HourlyCheck,NULL);
}

void HourlyCheck()
{
	int int_x=200,int_y=200,junk2,junk3;
	Window junk1;
	char txt[ENTRY_LEN+1],txt2[ENTRY_LEN+1],timestr[20];
	int i,hour;
	time_t now = time((time_t *)NULL);
	struct tm *tnow;
		
	XtAppAddTimeOut(app_context,CHECK_DELAY,HourlyCheck,NULL);
	curr_day = WhatsTheDay();
	tnow = localtime(&now);
	/* calculate the hour we're interested in */
	hour = tnow->tm_hour + (int)(((tnow->tm_min)+app_data.warning)/60);
	i = hour-app_data.hour;
#ifdef DEBUG
	printf("hour: %d i: %d today: %d\n",hour,i,today);
#endif
	if ((today<DAYS) && (today>=0) && (i>=0) && (i<app_data.slots) && (course[today][i][0] != EMPTY))
	{
		char buff[1024];
#ifdef DEBUG
		printf("Lecture Warning.\n");
#endif
		sprintf(buff,"%d min Lecture Warning: ",app_data.warning);
		XtVaSetValues(ptitle,XtNlabel,buff,
			SETBG((app_data.mono==True) ? "white" : "red"),NULL);
		strncpy(txt,course[today][i],ENTRY_LEN);
		strncpy(txt2,locate[today][i],ENTRY_LEN);
		sprintf(timestr,"%2d.%02d",hour,app_data.minutes);
		XtVaSetValues(ptime,XtNlabel,timestr,SETBG(colour[today][i]),NULL);
		XtVaSetValues(ptext,XtNlabel,txt,SETBG(colour[today][i]),NULL);
		XtVaSetValues(plocn,XtNlabel,txt2,SETBG(colour[today][i]),NULL);

		XtVaSetValues(ptext,XtNwidth,maxtextlen*8+5,NULL);
		XtVaSetValues(plocn,XtNwidth,maxlocnlen*8+5,NULL);

		XQueryPointer(XtDisplay(topLevel),DefaultRootWindow(XtDisplay(topLevel)),
			&junk1,&junk1,&int_x,&int_y,&junk2,&junk2,&junk3);
		XtVaSetValues(pshell,XtNx,int_x,XtNy,int_y,XtNtitle,"xtt",NULL);
		XtPopup(pshell,XtGrabNonexclusive);
	}
}

void AboutPopup()
{
	Window junk1;
	int int_x=200,int_y=200,junk2,junk3;
	Position x,y;
	const int WIDTH = 199;
	const int HEIGHT = 169;
	
	XQueryPointer(XtDisplay(topLevel),DefaultRootWindow(XtDisplay(topLevel)),
		&junk1,&junk1,&int_x,&int_y,&junk2,&junk2,&junk3);
	x=(Position)int_x;
	y=(Position)int_y;
	fit_on_screen(topLevel,&x,&y,WIDTH,HEIGHT,30,50);
	XtVaSetValues(ashell,XtNx,x,XtNy,y,XtNtitle,"xtt",XtNwidth,WIDTH,
		XtNheight,HEIGHT,XtNmaxWidth,WIDTH,XtNminWidth,WIDTH,
		XtNmaxHeight,HEIGHT,XtNminHeight,HEIGHT,NULL);

	XtPopup(ashell,XtGrabNonexclusive);
}

void PopDown(Widget w, XtPointer client_data, XtPointer call_data)
{
	/* chose between notify popup and about popup */
	if (w==dismiss)
		XtPopdown(pshell);
	else
		XtPopdown(ashell);
}

void Prev()
{
	curr_day--;
	if (curr_day < 0)
		curr_day=DAYS-1;
	DoDay();
}

void Next()
{
	curr_day++;
	if (curr_day==DAYS)
		curr_day=0;
	DoDay();
}

int WhatsTheDay()
{
	time_t now = time((time_t *)NULL);
	struct tm *tnow;
	int day;
	
	tnow = localtime(&now);
	day = (tnow->tm_wday) - 1;
	today = day;
#ifdef DEBUG
	printf("Today: %d\n",today);
#endif
	/* if at weekend, advance to Monday */
	if ((day<0) || (day>=DAYS))
		day=0;
	return(day);
}

/* read table from $HOME/XTTFILE */
void readtable()
{
	FILE *fp;
	char bfile[512],buff[1024];
	char *buffs;
	int day=0, slot=0;
	int at,s,d;
	Boolean found;

	if (strlen(app_data.file)>0)
		strcpy(bfile,app_data.file);
	else
		sprintf(bfile,"%s/%s",(char *)getenv("HOME"),XTTFILE);

	fp = fopen(bfile,"r");
	if (fp == NULL)
	{
	        fprintf(stderr,"Error: couldn't open file %s\n",bfile);
			exit(1);
	}

	while (!feof(fp))
	{
	    fscanf(fp,"%1024[^\n]\n",(char *)buff);
		if (buff[0] != '#')
		{
			if (day==DAYS)
			{
				fprintf(stderr,"Error: Too many lines in %s\n",bfile);
				fprintf(stderr,"Should have only %d lines.\n",DAYS*app_data.slots);
				exit(1);					
			}
			if ((at=strcspn(buff,SEPARATOR)) != strlen(buff))
			{
				strncpy(course[day][slot],buff,(at<ENTRY_LEN) ? at : ENTRY_LEN);
				buffs = &buff[at+strlen(SEPARATOR)];
				if ((at=strcspn(buffs,SEPARATOR)) != strlen(buffs))
				{
					strncpy(locate[day][slot],buffs,(at<ENTRY_LEN) ? at : ENTRY_LEN);
					strncpy(colour[day][slot],&buffs[at+strlen(SEPARATOR)],ENTRY_LEN);
				}
				else
				{
					/* course and location given, but no colour */
					strncpy(locate[day][slot],buffs,ENTRY_LEN);
					/* search for same course in previous entries */
					strncpy(colour[day][slot],DEFAULT,ENTRY_LEN);
					found=False;
					for (d=day; (d>=0) && !found; d--)
						for (s=app_data.slots-1; (s>=0) && !found; s--)
							if (((d<day) || (s<slot)) && (!strcmp(course[d][s],course[day][slot])))
							{
								strncpy(colour[day][slot],colour[d][s],ENTRY_LEN);
								found=True;
							}
				}
			}
			else
			{
				strncpy(course[day][slot],buff,ENTRY_LEN);
				strcpy(locate[day][slot],"");
				strncpy(colour[day][slot],DEFAULT,ENTRY_LEN);
			}
			if (++slot==app_data.slots)
			{
				slot=0;
				day++;
			}
		}
	}
	if ((day!=DAYS) || (slot!=0))
	{
		fprintf(stderr,"Error: Too few lines in %s\n",bfile);
		fprintf(stderr,"Got %d lines. Need %d.\n",day*app_data.slots+slot,
			DAYS*app_data.slots);
		exit(1);					
	}		
}

void alloc_colours(void)
{
	int d,i;
	Display *display=XtDisplay(topLevel);
	XColor junk1,junk2;
	Boolean got_all=True;

	for (d=0; d<DAYS; d++)
		for (i=0; i<app_data.slots; i++)
		{
			if (app_data.mono)
				strcpy(colour[d][i],"white");
			else
			{
				if (!(XAllocNamedColor(display,DefaultColormap(display,0),colour[d][i],
						&junk1,&junk2)))
				{
					got_all=False;
					strcpy(colour[d][i],"white");
				}
			}
		}
	if (!got_all)
		fprintf(stderr,"Warning: Cannot allocate all colours - using white instead.\n");
}

void Quit()
{
	if (!app_data.mono)
	{
		XFreePixmap(XtDisplay(topLevel),bgxpm);
		XFreePixmap(XtDisplay(topLevel),aboutxpm);
	}
	XtDestroyApplicationContext(app_context);
	exit(0);
}

void load_pixmap(char **dname, Pixmap *xpm,Widget top)
{
	/* returns a Pixmap, loaded from the given data, relative to the
		display of the given Widget. The closest colours are used. Exit
		is called given an error */

	int status;
	XpmAttributes attr;
	Pixmap mask;

	attr.valuemask = XpmReturnPixels | XpmCloseness;
	attr.closeness = 20000;
	status = XpmCreatePixmapFromData(XtDisplay(top),
		DefaultRootWindow(XtDisplay(top)),dname,xpm,&mask,&attr);
	if (status != XpmSuccess)
	{
			fprintf(stderr,"Pixmap error: %s\n",XpmGetErrorString(status));
			exit(1);
	}
}

void set_icon_pixmap(char **dname,Widget top)
{
	/* sets the icon of the top level Widget to the Pixmap in the given
		filename. Exit is called given an error */
	Pixmap icon_pixmap;
	
	load_pixmap(dname,&icon_pixmap,top);
	XtVaSetValues(top,XtNiconPixmap,icon_pixmap, NULL);
}

void acceptQuitPre(Widget top)
{
	/* perform quit action on delete window signal - before XtRealizeWidget.
	Calls a function called Quit */

	XtActionsRec Actions[] = { {"quit",Quit } };
	
    XtAppAddActions(XtWidgetToApplicationContext(top),Actions,
    		XtNumber(Actions));
    XtOverrideTranslations(top,
    		XtParseTranslationTable("<Message>WM_PROTOCOLS: quit()\n"));
}

void acceptQuitPost(Widget top)
{
	/* perform quit action on delete window signal - after XtRealizeWidget */

	Atom wm_delete_window = XInternAtom(XtDisplay(top),"WM_DELETE_WINDOW",False);
	XSetWMProtocols(XtDisplay(top),XtWindow(top),&wm_delete_window,1);
}

void fit_on_screen(Widget top,Position *x,Position *y, int w, int h,
        int horizB,int vertB)
{
        /* scales x and y to fit (w by h) window on screen, allowing for 
                borders of size horizB by vertB */

        Display *dpy = XtDisplay(top);
        int width=DisplayWidth(dpy,DefaultScreen(dpy));
        int height=DisplayHeight(dpy,DefaultScreen(dpy));

        if (*x>width-w-horizB)
                *x=width-w-horizB;
        if (*y>height-h-vertB)
                *y=height-h-vertB;
}
