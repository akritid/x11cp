.\" @(#)xtt.man	1.0 94/11/02; By - Matt Chapman
.TH XTT 1 "13 Jan 1995" "xtt"
.SH NAME
xtt \- XTimeTable
.SH SYNOPSIS
xtt [X-Toolkit Options...] [XTT Options]
.SH DESCRIPTION
This program is mainly for students, like me, that have to try to drag
themselves away from a computer in time for lectures. It reads in a
timetable from a file (~/.xtt) and displays it in a window a day at a
time. A warning is popped up a number of minutes before a lecture or appointment
is due to start.
.SH OPTIONS
Xtt accepts all of the standard X Toolkit command line options
along with the additional options listed below. See \fIX\fP(1).
.TP 8
.B \-slots \fInumber\fP
This sets the number of slots or entries for each day (default: 9).
The tabletable file must contain exactly 5 times this number of entries
- one for each day of the week.
.TP 8
.B \-hour \fIhour\fP
This is the hour digit of the time the first slot of each day starts (default: 9).
.TP 8
.B \-minute \fIminutes\fP
This is the offset in minutes of every slot from the hour (default: 5 -
all slots start at 5 minutes past the hour).
.TP 8
.B \-12
.br
Switch to 12 hour clock notation, instead of the default 24.
.TP 8
.B \-nowarn
.br
This disables the warnings which popup a certain time before each non-empty
slot is due to start.
.TP 8
.B \-warning \fIminutes\fP
This defines the length of time before non-empty slots that a warning is
given, if not disabled (default: 10).
.TP 8
.B \-file \fIxtt_file\fP
This specifies a different file to be read as the timetable file, instead of
the default ~/.xtt. This is useful to see if a collegue is at a lecture - if
their .xtt file is readable by you, do: xtt ~usercode/.xtt.
.TP 8
.B \-mono
.br
This activates reduced colour mode which is entirely black and white, apart
from the about popup. This option is automatically set if the program is run
on a black and white display.
.TP 8
.B \-bcol \fIcolour\fP
This option can be used to change the colour of all buttons. The default setting
is "chocolate", or "white" if the -mono option is set.
.SH FILE FORMAT
The file format for the timetable file is as follows:
.br
Lines beginning with '#' and blank lines are ignored.
There must be an entry for every slot of every weekday, one per line - which
is 5 * (number of slots in a day) lines of entries. An entry beginning with '-'
indicates an empty slot - no warning for it is given. All entries are of the
form \fIdescription/location/colour.\fP The two rightmost sections can be
omitted. The first two sections, \fIdescription\fP and \fIlocation\fP are printed
in two columns, for each slot. The \fIcolour\fP section specifies the background
colour of that slot. To aid colouring by subject, if no colour is
specified, all previous entries are searched for the same \fIdescription\fP section,
and the colour of that is used. Therefore the colour of a subject only
needs to be given once, for its first ocurrence in the file.
.SH LIMITATIONS
It is intended for a specific purpose, and is therefore fairly inflexible.
The offset from the hour of the lectures, the number and start time of
lectures can be changed, but they can only be on weekdays, and all last
one hour. If you want a flexible appointment system then go somewhere
else.
.SH FILES
.br
~/\fB.\fPxtt     file containing the timetable to be displayed
.SH AUTHOR
Matt Chapman
.br
csuoq@dcs.warwick.ac.uk
.br
http://www.csv.warwick.ac.uk/~csuoq
.SH COPYRIGHT
IMPORTANT:
This software comes with NO warranty whatsoever. I therefore take no
responsibility for any damages, losses or problems caused through use or
misuse of this program.

I hereby grant permission for this program to be freely copied and
distributed by any means, provided that this and all other copyright notices
remain unchanged. However, this program and all its code still belong to me,
and I reserve the right to be identified as the author of it.

Matthew Chapman. Jan 1995.
