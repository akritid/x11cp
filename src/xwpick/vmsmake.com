$!**********************************************************************
$!                                                                     *
$! Name: VMSMAKE.COM                                 Date:    05.09.94 *
$! Author: Evgeni Chernyaev (chernaev@mx.ihep.su)    Revised:          *
$!                                                                     *
$! Function: Build XWPICK under OpenVMS (VAX/ALPHA)                    *
$!                                                                     *
$!**********************************************************************
$ SET VERIFY
$!
$!                Set logical names
$!
$ IF (F$SEARCH("SYS$SYSTEM:DECC$COMPILER.EXE").NES."")
$ THEN
$   CC := cc/decc/prefix=ALL/standard=VAXC/nodebug/optimize
$ ELSE
$   CC := cc/nodebug/optimize
$ ENDIF
$ DEFINE/NOLOG X11 DECW$INCLUDE
$!
$!                Compilation
$!
$ CC xwpick.c
$ CC ImgToolKit.c
$ CC PSencode.c
$ CC PS_LZWencode.c
$ CC PCXencode.c
$ CC GIFencode.c
$ CC PICTencode.c
$ CC PPMencode.c
$!
$!                Create executable
$!
$ LINK xwpick,-
       ImgToolKit,-
       PSencode,-
       PS_LZWencode,-
       PCXencode,-
       GIFencode,-
       PICTencode,-
       PPMencode,-
       SYS$INPUT/OPT
SYS$LIBRARY:DECW$XLIBSHR/SHARE
$!
$!                Create help library
$!
$ LIBRARY/HELP/CREATE=BLOCK:0 XWPICK.HLB XWPICK.HLP
$!
$!                Cleaning
$!
$ DELETE/NOLOG *.obj;*
$ PURGE/NOLOG  xwpick.exe,xwpick.hlb
$ SET NOVERIFY
$ EXIT
